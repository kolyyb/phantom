import mysql.connector as mysql
from mysql.connector import cursor

conn = mysql.connect(host='localhost', user='root', password='pass', database='phantom_data')


def store_items(item_name = None, id_parent = None,type = None):
    if type == None:
        type = "geographic"
    cur = conn.cursor(dictionary=True)
    query = """INSERT INTO items(item_parent, item_name, item_type) VALUES (%s, %s, %s);"""
    cur.execute(query, [id_parent, item_name, type])
    conn.commit()
    return cur.lastrowid

def get_item_by_name(item_name):
    cur = conn.cursor(dictionary=True)
    query = """SELECT * FROM items WHERE item_name = %s;"""
    cur.execute(query, [item_name])
    item = cur.fetchone()
    return item


