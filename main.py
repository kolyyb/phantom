import os
import requests
from bs4 import BeautifulSoup
from db import store_items, get_item_by_name


# Récupérer la page html et l'enregistrer en local

def get_h3_items():
    pass


def get_h4_items():
    pass


def get_url_page(page_name):
    if not os.path.exists("pages"):
        os.mkdir("pages")
        print("Created")
    file_name = page_name.split("/")[-1]
    html = ""

    if not os.path.isfile("pages/" + page_name):
        url = "https://www.paranormaldatabase.com/" + page_name
        response = requests.get(url)
        response.encoding = response.apparent_encoding

        if response.status_code == 200:
            print("pages scrappées: " + page_name)
            html = response.text
            # print(html)

            f = open("pages/" + file_name, "w")
            f.write(html)
            f.close()
    else:
        print("Lecture page existante: " + page_name)
        f = open("pages/" + file_name, "r")
        html = f.read()
        f.close()

    soup = BeautifulSoup(html, "html5lib")
    return soup


# item_list = []
urlLinksList = []


# Recuperation des éléments de la page
def get_items_page(soup):
    # Récupération des h3 == Région (parent)
    h3_list = soup.find_all("h3")

    # Récupération de la div englobant l'ensemble des parents et enfants
    for h3 in h3_list:
        if h3.text != "© Paranormal Database 2023":
            print("Parent: " + h3.text)
            parent_element = h3.parent

            # enregistrer H3 dans la bdd et stocker l'id (parent)
            if_item_exist = get_item_by_name(h3.text)
            if not if_item_exist:
                id_parent = store_items(h3.text)
            else:
                id_parent = if_item_exist["id"]

            # Récupération des titres h4 == enfant
            if parent_element:
                h4_list = parent_element.find_all("h4")
                for h4 in h4_list:
                    print("  == children: " + h4.text)
                    if_item_exist = get_item_by_name(h4.text)
                    if not if_item_exist:
                        store_items(h4.text, id_parent=id_parent)

                # Recupération des urls des pages
                a_link_list = parent_element.find_all("a")
                for a_link in a_link_list:
                    href = a_link.get("href")
                    if href is not None:
                        print("a href " + a_link.get("href"))
                        urlLinksList.append(a_link.get("href"))


def get_home_page():
    # Récupération de l'ensemble des éléments de la page
    soup = get_url_page("index.html")
    get_items_page(soup)

    # Si une liste de lien vers des sous-pages existe
    # Parcourir chaque sous_page
    # de la même façon que la page parente
    if urlLinksList:
        for urlLink in urlLinksList:
            soup = get_url_page(urlLink)
            get_items_page(soup)


# England > South East England > Counties > Berkshire > cases (https://www.paranormaldatabase.com/berkshire/berkdata.php)
def get_cases_page():
    soup = get_url_page("berkdata.php")


# Utiliser le concept de *.parent pour recuperer les contenus des cases (title, location, type, date, comment)
# Ou utiliser la methode d'Anthony qui consiste à utiliser la fonction soup *.content et des dictionnaires
# Contenu englobant du bloc case : w3-border-left w3-border-top  w3-left-align
# Suprimer les <p></P>, <br></br>
# Malheureusement les contenus spécifiques : titre, location, date, type, comment ne sont pas tagés

def get_cases():
    pass


if __name__ == "__main__":
    get_home_page()
