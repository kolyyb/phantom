DROP DATABASE IF EXISTS phantom_data;
CREATE DATABASE phantom_data;
USE phantom_data;


CREATE TABLE items
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    item_parent INT,
    item_name   VARCHAR(65),
    item_type   ENUM ('geographic', 'reports')
);

CREATE TABLE cases
(
    id            INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    case_title    VARCHAR(65),
    case_location VARCHAR(65),
    case_type     VARCHAR(65),
    case_date     VARCHAR(65),
    case_comment  TEXT,
    month         INT,
    weather       BOOL
);

CREATE TABLE cases_items
(
    case_id INT,
    item_id INT,
    FOREIGN KEY (case_id) references cases (id),
    FOREIGN KEY (item_id) references items (id)
);
