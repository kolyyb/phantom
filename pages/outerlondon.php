
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Church Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > Outer & Other</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>London (Outer & Other) Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2281.jpg' class="w3-card" title='Underground London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Underground London.</small></span>                      <p><h4><span class="w3-border-bottom">London Subterraneans</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Other London - No fixed location<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Current<br>
              <span class="w3-border-bottom">Further Comments:</span> This legend, which seemingly 'does the rounds' every few years, states that a group of Londoners began to live underground in the late nineteenth century, and now they have mutated. These subterraneans live off the junk food we discard in the underground tube stations and the odd commuter if they are discovered alone on the train.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon3713.jpg' class="w3-card" title='Sky above London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Sky above London.</small></span>                      <p><h4><span class="w3-border-bottom">Fiery Sword</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Other London - Skies over the city<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1832<br>
              <span class="w3-border-bottom">Further Comments:</span> The start of the great Cholera Epidemic in 1832 was accompanied by a vision witnessed by thousands, of a burning fiercely sword hanging over the city.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon3203a.jpg' class="w3-card" title='The sky over the City of London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The sky over the City of London.</small></span>                      <p><h4><span class="w3-border-bottom">Darkness</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Other London - Skies over the city<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 August 1763<br>
              <span class="w3-border-bottom">Further Comments:</span> London was briefly covered by a darkness during the day, blacker than an eclipse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Edward Benson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Addington - Addington Palace<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This former Archbishop still drifts around the building where he listens to the church music. It is said he is only seen by choirboys.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">German Pilot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Addington - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Dying after his aircraft crashed nearby, this the Second World War pilot is still occasionally seen walking around this former village.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Digger Harry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Addington - Wooded area near Courtwood Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1932<br>
              <span class="w3-border-bottom">Further Comments:</span> Harry was so upset when his wife died that he buried her close to his cottage without telling anyone. The authorities, believing Harry had killed her, locked him up for several months before releasing him because of his age. During his incarceration, Harry forgot where his wife was buried, and spent the remainder of his days trying to find her. His tragic phantom was said to continue to roam the area carrying a shovel.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Capybara</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Barnet - Darlands Lake<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 September 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Normally found in South America, a capybara was spotted by actor David Dayan Fisher and his nephew and niece while walking through this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Dick_turpin_jumping.jpg' class="w3-card" title='An old illustration of Dick Turpin on horseback.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old illustration of Dick Turpin on horseback.</small></span>                      <p><h4><span class="w3-border-bottom">Turpin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Barnet - Finchley Common (no longer exists, now suburbs)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The common was said to be one of the homes to the ghostly form of Dick Turpin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Maidservant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Barnet - Red Lion Public House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This woman died after falling down the stairs - her shade remains earthbound.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Johnson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Barnet - Road between Barnet and Enfield Chase<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A man by the name of Johnson was said to have been murdered along this road, and the spot where the crime occurred was thought to be haunted for many years after the event. The same road was also said to be haunted by another man, a Mr Danby, who on at least one occasion, caused a horse pulling a carriage with two occupants to bolt. The ghost continued to run alongside the panicked horse until stopping by a gate - it was only then that the horse calmed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Airmen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bexley - A2 London-bound between Heath turn-off and Black Prince turn-off<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s, and 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> Several witnesses were said to have seen a couple of airmen standing on the hard shoulder of the A2. One wore a leather flying helmet while the other appeared to have a medal or piece of metal near his throat. A local tale says that a Heinkel bomber crashed nearby, killing the crew who could not bail out in time, and it has been speculated that the figures belong to this aircraft.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Beast of Bexley</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bexley - Bexleyheath Golf Club<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 22:30h on 12 July 2005, and February 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A teenager reported seeing a giant black cat trotting across the grass here - police were called but could find nothing. The creature was photographed by a woman from the bottom of her garden in Feb 2006.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bright Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bexley - Foots Cray Meadows<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This bright white female form is reported to walk the meadows scattering flowers for her husband who died during wartime. She is said to have killed herself soon after hearing the news in the same area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Constance</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bexley - Hall Place<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Ghosts unknown, poltergeist 1950<br>
              <span class="w3-border-bottom">Further Comments:</span> One of three ghosts reported here, Lady Constance watched her husband die as a stag gored him, and later killed herself. Her pale white shade now frequents the grounds. Edward, the Black Prince, has also been observed in the area, while the spirit of a servant haunts the loft. A poltergeist outbreak in 1950 resulted in furniture being tossed around.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Brentford - Banks of the Grand Union Canal<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1935<br>
              <span class="w3-border-bottom">Further Comments:</span> A torso of a female murder victim was discovered here, with her legs found in a train carriage at Waterloo. Her ghost remained in the area for a while, manifesting as a tall woman wearing a white dress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Playful Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Brentford - Clitheroe Lock, Grand Union Canal<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A young boy with a connection to Boston Manor was said to have drowned here during the 1820s. Always 'felt' but never seen, the entity is said to be playful and can assist (or hinder, depending on his mood) with the lock gates as boats pass through.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walking Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bromley - Bromley North Line<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s?<br>
              <span class="w3-border-bottom">Further Comments:</span> This small stretch of line is reputedly haunted by a woman struck by a train.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bromley - Chislehurst Caves<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1961<br>
              <span class="w3-border-bottom">Further Comments:</span> Many ghosts are said to haunt the caves, including a woman drowned in a pool who was seen in 1961 - the witness knocked himself out on the cave roof while trying to escape the apparition. Other 'people' have been seen within, and the sounds of children and sometimes horses are reported. One myth says that a priest who spent the night at the site was found dead, the cause of death - fright!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Michael Jackson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bromley - Churchill Theatre<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 10 May 2014<br>
              <span class="w3-border-bottom">Further Comments:</span> Several news outlets reported on the photograph of 'Michael Jackson' manifesting near an impersonator which was snapped by a fourteen year old boy, although the entity is more likely to be lens flare.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Severed Arm</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bromley - Downe Court Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Several shades reside here, though the scariest is the ghostly severed arm that once appeared, placing itself by the side of a woman while she was in bed. A young girl dripping wet (she drowned in a lake) haunts the butler's room, and finally, many people have reported feeling unnaturally terrified while standing at the top of the staircase where once a man hanged himself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dark Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bromley - Orpington Priory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 22 February 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> A drone operator caught a figure in black standing within the priory grounds, although there is little to suggest that the figure is not 'real'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Charlotte</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bromley - Queensway Village Bakery (aka Plaxtol Village Bakery), Petts Wood<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> This bakery hit the headlines after it was reported to be haunted by an entity named Charlotte. She was said to be harmless, although she was blamed for throwing baking trays and bread, flicking lights on and spelling the first three letters of her name in spilt caster sugar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sea Captain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bromley - United Services Club<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> An old sea captain reputedly haunts this site, possibly from the building's time as a post First World War military hospital.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unknown Male</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Bromley - Viola, Crescent Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The spectral image of a man was seen several times in a bedroom in his house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Royal Messenger</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Carshalton - Convent of the Daughters of the Cross, Pound Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed by a fall down the stairs (he was pushed after delivering bad news), this former courier now haunts the steps on which he died. Two more ghosts haunt the long galley; a butler murdered a housemaid in the area, and their earthbound souls have been watched re-enacting the scene.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 92</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=92"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=92"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Main London Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Reports: People</h5>
     <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View tReports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2024</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
