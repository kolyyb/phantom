

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in the Isle of Man, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/otherregions.html">Other Regions</a> > Isle of Man</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Isle of Man Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Secret Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Andreas - Shan Cashtal barrow<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The fairies here once travelled to and from the barrow using underground passageways leading to a nearby churchyard (at Maughold).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/211110hauntedbeach.jpg' class="w3-card" title='The Haunted Beach from Apparitions; or, The Mystery of Ghosts, Hobgoblins, and Haunted Houses Developed, by Joseph Taylor.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Haunted Beach from Apparitions; or, The Mystery of Ghosts, Hobgoblins, and Haunted Houses Developed, by Joseph Taylor.</small></span>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baldrine - Garwick Beach<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> If alone on the shingle beach, the faint sound of footsteps can be heard in the stones.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse and Cart</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baldrine - Pack Horse Lane<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of a ghostly horse and cart moving along the lane is said to be a warning that something bad will soon happen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Piece of Paper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballafletcher - Stone cross  in the area (current status not known)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A stone cross could not be moved by man or team of horses, and while a large group stood around the monument debating how to try to shift it, an old man told a young boy to reach under the rock. The boy did so, flipped the stone over, and pulled out a piece of paper which instructed the group to fear God and obey priests. Copies of the text were shared over the island and were said to bestow great fortune and protection from witches.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phynnodderee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballasalla - Rushen Abbey<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This fairy creature would be observed digging around the ruins, although when the area was examined, there was never any evidence of soil disturbance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sprites & Pixies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballona - Ballona Bridge<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> On this day, the fairy population comes out in force. If they are not acknowledged by anyone crossing the bridge, the fairy folk have no hesitation in making their presence felt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beary Mountain (aka Bearey Mountain) - Upper part of the site<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1800s<br>
              <span class="w3-border-bottom">Further Comments:</span> A group of fairies carrying household items were observed walking up the mountain, disappearing into the mists which circled the top. It was thought they moved home to escape the sound of a newly built mill.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Six Rowers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Black Head - Cave<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A cave, once said to contain pirate treasure, only had one entrance which could be accessed by the sea. One local man watched six figures in a small boat row into the cave. Curious to know what they were up to, the man followed the figures, but they, and their boat, had vanished without trace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Glashtin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Braddan - Church<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A short phantom with a hairy and evil face, this entity would haunt the churchyard. It could only be safely passed if bowed to three times.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Home</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Braddan - 'Real' Fairy Bridge<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Bridge still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This is another site said to be home to the fairies, and it is considered unlucky not to say hello or wave to them as you cross the bridge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mer Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Calf of Man - Rocky area, exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1810<br>
              <span class="w3-border-bottom">Further Comments:</span> Three men from Douglas found two merchildren on the rocks. One was already dead, but they saved the second child and took it to their town. The creature was described as around 60.3 centimetres in length and brown in colour, although the scales around its tail were slightly violet, and the hair on its head light green.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with Spinning Wheel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carraghan - Side of the mountain<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a woman carrying a spinning wheel and appearing to be in distress would be seen sitting on the side of the mountain. She was considered an ill omen, although after four people tried to capture her, she vanished into a stream and was never seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smoky Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castletown - 77 Malew Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn 1957<br>
              <span class="w3-border-bottom">Further Comments:</span> Alasdair Alpin MacGregor wrote that a smoky female apparition appeared to the owner of this cottage. The entity was blue/grey in colour and watched the owner before disappearing through a wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Writing on the Wall</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castletown - Building once used as staff accommodation<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1995<br>
              <span class="w3-border-bottom">Further Comments:</span> A former occupier of this building reported that the upper part of the building had an evil feeling, and some new staff would not spend more than a night there. One witness, alone in the building, took a shower. Returning to the bathroom a short time later to pick up their watch, they found vile words written in shaving foam on the walls. A priest called to help said that he felt a presence, but it would not hurt anyone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-70.jpg' class="w3-card" title='An old postcard of Castle Rushen on the Isle of Man.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Castle Rushen on the Isle of Man.</small></span>                      <p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castletown - Castle Rushen, Eagle Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> Watched by four boys, this phantom female figure wore a head dress and had dark hands. Local legend says that the woman was once executed for murdering her child. Another ghost that haunts the castle, this time in the basement, has been seen sleeping with a huge sword by his side. Finally, a legend says that the castle has miles of tunnels beneath, leading to a country full of giants.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lighting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castletown - Compton House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An unseen hand is said to flick lights on and off in a room here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Angry Voices</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castletown - Hill Fort<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The area is said to be haunted by the sound of angry voices, a large shuck-like dog, and a cleric.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cregneish - Meayll Stone Circle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom army comprising of soldiers and their horses are said to haunt the area round these ancient burial chambers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Buggane</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crosby - St Trinian's Church<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This monster, an ogre-like entity, was stirred by the ringing of the bells of this church - it tore off the roof while trying to silence the peals. Another story says that the roof is missing because demons would throw it to the ground after each repair until either the locals eventually gave up trying to fix it, or local tailor Timothy Clucas tricked the Buggane into exploding its own head.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/talking-gef.jpg' class="w3-card" title='Gef, from Harry Price&#039;s 1936 &#039;The Haunting of Cashen&#039;s Gap&#039;.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Gef, from Harry Price&#039;s 1936 &#039;The Haunting of Cashen&#039;s Gap&#039;.</small></span>                      <p><h4><span class="w3-border-bottom">Gef the Talking Mongoose</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dalby - Doarlish Cashen farm<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1931-1935<br>
              <span class="w3-border-bottom">Further Comments:</span> Part poltergeist, part cryptozoological wonder, Gef attached itself to the nine-year-old Mary who lived in the farmhouse. Over a four-year period, the creature (if that was it was) created raps, manifested voices which communicated with the family, issued advice for the Grand National, and took the form of a small ferret-like animal, with fingers instead of claws.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dhoon Glen - Inneen Vooar waterfall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local tale says a young girl drowned in the pool under the waterfall and that her ghost remains.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Douglas - Douglas Head Hotel (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1975<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom woman reputed to haunt this site was very active, generating poltergeist-like effects (moving chairs and ashtrays, and opening doors) and manifesting on the second floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Douglas-Falcan-Cliff-Hotel.jpg' class="w3-card" title='A phantom falling figure.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom falling figure.</small></span>                      <p><h4><span class="w3-border-bottom">Falling Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Douglas - Falcan Cliff Hotel (no longer operational and converted into offices)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A German soldier who jumped off the tower was once occasionally seen repeating his fall. It was also said that animals disliked entering the tower.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in seat B14</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Douglas - Gaiety Theatre and Opera House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly lady in seat B14 is said to have been widowed during the Great War and now quietly watches performances - the seat tends to be left empty during performances, just in case she needs to use it. Other ghosts on the site include a woman in black, a man who lurks in one of the boxes, and helpful entity who helps thespians and stage crew.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Worried Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Douglas - Original Quids Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former owner reported that his dog would refuse to enter the cellar, and that he himself felt a presence that evoked inexplicable fear.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 69</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=69"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=69"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/otherregions.html">Return to Other Regions</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Types </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report2.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
