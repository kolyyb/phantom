

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Northumberland Ghosts, Folklore and Strange Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northeastandyorks.html">North East and Yorkshire</a> > Northumberland</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Northumberland Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Acklington-General-area.jpg' class="w3-card" title='A witch standing in snowy woodland.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A witch standing in snowy woodland.</small></span>                      <p><h4><span class="w3-border-bottom">Witchy Neighbourhood</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acklington - General area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The people from this area had a reputation as being practitioners of witchcraft.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Akeld - Small piece of woodland between village and Yeavering (may no longer stand)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A small strip of woodland was said to be home to a white female phantom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alnmouth - Schooner Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> According to local press, the hotel was named the county's most haunted hotel by the 'Poltergeist Society', although the society appears never to have existed. Putting this potential marketing exercise to one side, the building is reputedly home to a phantom soldier, a small child, a chicken, ghostly whispers, and the sounds of someone screaming.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Creature from the Tomb</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alnwick - Alnwick Castle<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> 1100s<br>
              <span class="w3-border-bottom">Further Comments:</span> The vampire that once frequented this castle, a former lord of the estate, lived under the site and would emerge at night to attack the local villagers. An outbreak of plague was also attributed to the unholy creature, and this resulted in the villagers digging the monster up from its shallow grave and burning it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dancing within Limits</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alnwick - Fairy ring at Chathill Farm (ring may no longer exist)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Children would dance around a fairy circle here, although it was said if they circled the site more than nine times, fairies would spirit the offending child away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lancelot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alnwick - Joyous Gard, possibly Alnwick Castle<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Lancelot was laid to rest at his castle, Joyous Gard. Some believe that this fort later become known as Alnwick Castle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wishing Well</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alnwick - Pin Well (was in parkland, not known if it still exists)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Tradition said that your wishes would come true if you walk three times around the well before leaping across it and dropping in a pin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Bottles</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alnwick - Ye Olde Cross public house<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> A collection of bottles on display in the pub are reportedly cursed - the last person to touch them died soon after, and now they are covered up to avoid further mishaps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Handless Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alwinton - Moorland in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s, 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> This cowled entity possessed no visible hands, feet or face. During the 1980s the figure may have twice been seen in a home just outside of Alwinton.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">School Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alwinton - Private residence outside of village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A young boy, around six years old and wearing a school uniform, was seen in the garden by someone at home here. The child looked solid but disappeared near the garden gate. A local legend says that a young schoolboy had drowned in a nearby river.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Leaping Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amble - Cliffs close to Cliff House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly woman leaps from these cliffs, fading away before she hits the sea below.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Short Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashington - Private house along Newbiggin Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1960s or early 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> A girl reported seeing a short woman in her parent's bedroom, dressed in ancient Egyptian clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/north1415.jpg' class="w3-card" title='An old drawing of Bamburgh Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old drawing of Bamburgh Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Olde Knight</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bamburgh - Bamburgh Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This armour clad knight is also often heard stomping about the castle, sometimes rattling his chains. A ghostly woman in a green cloak is sometimes seen falling from the top of the castle, vanishing before she hits the ground.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Free Money</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bamburgh - Rocky outcrop on which castle is built<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Lucky people are said to be able to find money which has been left by the fairies, but if they failed to add a coin of their own to the hoard they had found, everything would slip away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/laidly-worm.jpg' class="w3-card" title='The Laidly Worm, from the 1895 book English Folk and Fairy Tales by Joseph Jacobs.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Laidly Worm, from the 1895 book English Folk and Fairy Tales by Joseph Jacobs.</small></span>                      <p><h4><span class="w3-border-bottom">The Laidly Worm</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bamburgh - Spindlestone Heugh (aka  Spindlestone Crag)<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Sixth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This area was, for a brief time, home to a large slimy serpent - it turned out to be the daughter of the king, transformed by a curse from her evil stepmother. The king's son discovered the truth, saved his sister, and turned his mother into a toad. Another variant of the story says a dragon held the king's daughter hostage until she was saved by a knight who came from overseas.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Robber</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bardon Mill - Hardriding Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity is heard engaged in a struggle during which several pieces of furnishings are knocked over - when the sounds are investigated, all rooms appear calm and undisturbed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown Objects</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barrasford - Stones (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Last stone removed nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Before being removed by farmers, the standing stones which stood around the area were thought to have been thrown by giants engaged in combat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Home</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beanley - Moors in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The moorland in his region was considered a place where the little people could hide.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bellingham - Brownrigg school (no longer operating)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1984<br>
              <span class="w3-border-bottom">Further Comments:</span> Three girls unable to sleep in their dorm one night all heard the thunderous sound of horses and the clanking of metal pass through the room. One witness said they were so terrified they did not dare open their eyes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">RAF Pilot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belsay - A696, just outside of village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> Media outlets reported that Rob Davies and Chris Felton had filmed a figure which resembled a 'RAF pilot' standing on the roadside late at night. Although the footage was poor, the witnesses said the figure was dressed all in beige and held his arm out as if hitchhiking. When they turned the car around and drove back for another look, the figure had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Waters of Luck</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bertley - Village well (may no longer exist)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Miners would visit this well during the early hours of New Year's Day to fill their flasks, as it was believed any household which housed the water throughout the year would have luck bestowed upon it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walking Dead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berwick on Tweed - Exact area(s) unknown<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former wealthy man who died was said to have returned from the grave and walked the streets at night. A pack of dogs were said to escort him. Finally, a group of local men dug up the grave and incinerated the body, though most of the men are said to have died soon after from an unknown disease.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Green Shark</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berwick on Tweed - River Tweed<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 16 September 1757<br>
              <span class="w3-border-bottom">Further Comments:</span> A 1.8 metre long shark was discovered in this river, green in colour and no doubt a long way from home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Scottish Piper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berwick upon Tweed - Berwick castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly Scot plays his pipes while pacing up and down one of the ruined battlements.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berwick upon Tweed - Churchyard, Tweedmouth<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 1904<br>
              <span class="w3-border-bottom">Further Comments:</span> Several locals reported seeing a phantom woman with ginger hair, dressed in white or grey, in the early hours of the morning. It was said to have followed people as they left the area, while one other witness observed it reading headstones.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 158</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=158"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=158"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northeastandyorks.html">Return to North East and Yorkshire</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North West England</h5>
   <a href="/regions/northwest.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/northwest.jpg"                   alt="View North West records" style="width:100%" title="View North West records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
