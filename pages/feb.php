
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in February">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > February</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>February - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec37778.jpg' class="w3-card" title='Salt Tower at the Tower of London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Salt Tower at the Tower of London.</small></span>                      <p><h4><span class="w3-border-bottom">Lady Jane</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 (Greater London) - Tower of London, Salt Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 February (reoccurring), but least seen 1957<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom of Lady Jane Grey once haunted this tower, though it is now over fifty years since last being spotted standing on the roof.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/scot3993.jpg' class="w3-card" title='Glencoe, Scotland.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Glencoe, Scotland.</small></span>                      <p><h4><span class="w3-border-bottom">Clan MacDonald</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glencoe (Argyll and Bute) - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 February (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered in cold blood by the clan Campbell, these 38 MacDonald family members return to the scene of the crime.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Lady Lovibond</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Deal (Kent) - Goodwin Sands<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 February, every 50 years from 1748<br>
              <span class="w3-border-bottom">Further Comments:</span> A jealous sailor deliberately ran the Lady Lovibond onto the Goodwin Sands, a treacherous area off the coast of Deal, wreaking the ship and killing all on board. The craft is now said to re-enact the crash, though there are no reports of any witnesses in 1998. Two other ghostly craft are said to frequent the area - the liner Montrose, and a man of war named Shrewsbury.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Something Down There</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northrepps (Norfolk) - Shrieking Pit, along a lane off Hungry Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 February (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Some say that the entity that appears in the pit is the spirit of a woman who took her life, while others believe it is something much darker that lures the unwary to their deaths.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf912bnds.jpg' class="w3-card" title='St Mary&#039;s Churchyard, Bury St Edmunds.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> St Mary&#039;s Churchyard, Bury St Edmunds.</small></span>                      <p><h4><span class="w3-border-bottom">Maude Carew</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bury St Edmunds (Suffolk) - St Mary's Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 February, 23:00h (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Cursed by a monk after Maude murdered the Duke of Gloucester in 1447, her shade is now said to appear once a year. The alleged ghost often brings dozens of people to the area in the hope of seeing her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Christopher Wren</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hampton Court (Surrey) - The Old Court House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 26 February (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Dying in the front room of this building, Wren's footsteps can be heard on the anniversary of his death, pacing up and down the staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Charlotte-Bronte.jpg' class="w3-card" title='Portrait of Charlotte Bronte, by George Richmond.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Portrait of Charlotte Bronte, by George Richmond.</small></span>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hathersage (Derbyshire) - Vicarage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 28 February (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Even though the phantom white lady has not been observed for around one hundred years, she still opens a single door on one night per annum. A 1927 newspaper report claimed the figure to be Charlotte Bronte and that the appearance upset the family's dog, although the vicar denied the story.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Skater of Hickling Broad</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Potter Heigham (Norfolk) - River Thurne<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> In love with a rich man's daughter, a drummer boy would skate across the frozen river to secretly rendezvous with his forbidden girlfriend. On one winter's night the ice cracked and the boy drowned - leaving his ghost to continue the drumming. A phantom female rower has also been seen on the river.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Mary_Queen_of_Scots002.jpg' class="w3-card" title='Mary Queen of Scots.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Mary Queen of Scots.</small></span>                      <p><h4><span class="w3-border-bottom">Mary Queen of Scots</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Oundle (Northamptonshire) - The Talbot Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This hotel contains the staircase from Fotheringay Castle, Mary's place of execution. Her ghost is rumoured to have come with the staircase; pictures move on their own accord, one guest was awoken by 'something' sitting on their bed, and the gaunt face of a woman can be seen staring from out the hotel's windows.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor5326.jpg' class="w3-card" title='Millhams Lane, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Millhams Lane, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth (Dorset) - Millhams Lane, and surrounding Longham area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February (reoccurring); last confirmed in 1970s, but legends say she continues<br>
              <span class="w3-border-bottom">Further Comments:</span> The area around the A348 bridge is reportedly haunted by a ghostly white (or grey) woman, killed when hit by a horse and trap along the road. One story says she tries to entice men to jump off the bridge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Healing Waters</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Loch Monar (Highland) - Waters of the loch<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: first Mondays of February, May, August and November<br>
              <span class="w3-border-bottom">Further Comments:</span> If entered on certain days of the year, the waters of this Loch would heal the sick on the proviso that the Loch was entered three times at midnight and the water then sipped, before a coin was thrown in. The sick would then have to leave the site before the sun came up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Long Coat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buxhall (Suffolk) - Rattlesden Road area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: frosty nights or when snow is on the ground in February<br>
              <span class="w3-border-bottom">Further Comments:</span> The silhouette of a man in a long coat has been spotted walking around this road, carrying a lantern at waist height. The same witness reports his dog behaves strangely when taken past the woods close to Cockerells Hall Drive.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 12 of 12</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
