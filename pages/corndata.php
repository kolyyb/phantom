
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Cornish Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Cornwall</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Cornish Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Peggy Bray</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Altarnun - King's Head public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former landlady of the pub, Peggy has been known to return to ensure all is running smoothly.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mary the Housekeeper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Altarnun - Penhallow Vicarage (now a private house)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Mary had an affair with the Revenant Tripp - she ended the relationship by drowning herself in the nearby river. Her grey dressed ghost has been seen standing by the back door and in one of the bedrooms, while the phantom footsteps heard pacing around late at night are thought to belong to Tripp.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-23.jpg' class="w3-card" title='An old postcard of Bedruthan, Cornwall.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Bedruthan, Cornwall.</small></span>                      <p><h4><span class="w3-border-bottom">Knockers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedruthan - Beach, cliff top, & Bedruthan Steps<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Some believe these creatures to resemble mutant dwarfs (two foot high, large eyes with deformed and twisted heads), while others call them the ghosts of those killed while being forced to work for the Romans... Either way, they can be heard mining and picking away at the rocks underground in these areas.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grunting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin - Bodmin Jail (no longer a prison)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2003, 2005, 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> After spending a night at the jail in 2005, a ghost hunting group reported recording EVP containing a strange grunt. Members of the organisation also reported having stones thrown at them, even though they were alone. A couple of years prior to this, a woman had a feeling of great uneasiness when visiting the site and left perturbed when a door slammed shut in front of her with no visible reason for closing. A 2013 visitor reported a loud grunt in their left ear, also heard by another witness just over a metre away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Coffin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin - Cairns, Carburrow Tor<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Protected by a flock of birds, two golden coffins were believed to be buried under the cairns.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/corn11392.jpg' class="w3-card" title='Isolated Bodmin Moor.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Isolated Bodmin Moor.</small></span>                      <p><h4><span class="w3-border-bottom">Beast of Bodmin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin - General area<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> Ongoing<br>
              <span class="w3-border-bottom">Further Comments:</span> Another famous cryptid, the Beast of Bodmin is thought by some to be a large black cat, while others believe the creature is more paranormal in nature. Regardless, dozens of sightings span years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin - Lanhydrock House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity haunts the gallery and the drawing room, the only parts of the original house that still stand (a fire in the nineteenth century destroyed the rest of the house). The building is also said to be haunted by a man hanged by Royalists during the Civil War.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/corn939.jpg' class="w3-card" title='Dozmary Pool, Bodmin Moor.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Dozmary Pool, Bodmin Moor.</small></span>                      <p><h4><span class="w3-border-bottom">Jan Tregeagle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin Moor - Dozmary Pool<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> As punishment for killing his wife and children, Jan's ghost returns to the pool every night, attempting to empty the contents with a broken shell. A pack of ghostly wild dogs watch over him should he try to escape. One version of the same tale states Tregeagle did escape, but now he sweeps the sand from Porthcurno Cove to Mill Bay; he can be heard screaming as the sea washes the sand back again. Locals would refuse to say Tregeagle's name in case the ghost manifested. The Pool is also the supposed location where Sir Bedivere returned Excalibur.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Kobolds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin Moor - Goonzion Down mine<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One tin mine shaft was nicknamed the Roaring Shaft after the many bangs and thuds heard emanating from the empty tunnel. Kobolds or Bucca were held accountable.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Loud Footfalls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin Moor - Manor House inn, Rilla Mill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Heard coming from upstairs, these footsteps are always tracked down to the same empty bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Cup</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin Moor - Rillaton Barrow<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Locals told of a ghostly druid that walked this area offering all he came across a drink from a golden cup that could never be fully emptied. When the barrow was excavated in 1818, a gold beaker was recovered next to a skeleton, both thought to be at least 2000 years old.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Charlotte Dymond</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin Moor - Roughtor (aka Rough Tor), near a memorial stone to Charlotte<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 April (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> While Charlotte's boyfriend was hanged for her murder, it is widely believed he was innocent; this could be the reason why Charlotte has returned, dressed in a gown and silk bonnet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin Moor - St Nectan's Glen<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This area of woodland has produced an abundance of supernatural sightings over the years, the majority pertaining to ghostly hooded monks. A stooping old man has been observed standing by the water. In July 1981, one witness saw a burning human skull in a tree after hearing many whispering voices, although being alone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Treasure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolenowe Carn Moor - Uncle Ned's Rock<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Who Uncle Ned was is unknown, but his name is remembered as a local legend says he buried his treasure somewhere near the rock which bears his name.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/corn2071.jpg' class="w3-card" title='The famous Jamaica Inn, Bolventor.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The famous Jamaica Inn, Bolventor.</small></span>                      <p><h4><span class="w3-border-bottom">Drinking Sailor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolventor - Jamaica Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mostly unknown, large dog in April 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> A sailor who was murdered nearby reportedly returns to the public house to finish his last drink; he has been seen sitting on the wall outside the building. There have also been reports of a ghostly floating cloak, while outside there was a report of a phantom man hanging by his neck from a nearby tree. In 2015 a witness reported seeing a semi-transparent phantom dog, the size of an Irish Wolfhound, running thirty centimetres above the ground.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/corn4528.jpg' class="w3-card" title='Road between Bolventor and Five Lanes.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Road between Bolventor and Five Lanes.</small></span>                      <p><h4><span class="w3-border-bottom">Green Car</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolventor - Road between town and Five Lanes<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by at least two independent witnesses at different times, this old fashioned car contains four men who appear to be laughing, before the vehicle vanishes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Spinning Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bosava - Trewoof Manor House (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This shade was said to have been laid in a room which was subsequently bricked up, though released many years later by a curious night worker who could hear noises coming from behind the brickwork...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Devil and the Cobbler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bosava (?) - House no longer standing, location not known<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Built by a cobbler who made a deal with Old Nick and went back on his word, the house became haunted by the man and the devil. A miller who lived nearby was losing money due to his proximity to the location and asked Parson Corker to banish both entities. The parson brought some friends, set up his magic circle and after a mighty battle (involving a storm which levelled most trees in the area) banished the Devil. The ghost proved more difficult to banish, and in the end the house was demolished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-112.jpg' class="w3-card" title='An old postcard of Boscastle Harbour.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Boscastle Harbour.</small></span>                      <p><h4><span class="w3-border-bottom">Man in Tweed</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boscastle - Harbour<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Fishermen were reported to have encountered a strange man dressed in tweed who would stare at them before vanishing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Plucker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boscastle - Napoleon Inn<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Nicknamed Plucker, this poltergeist moves small items, causes pictures to fall off walls, and tugs at people's clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-21.jpg' class="w3-card" title='An old postcard of Boscastle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Boscastle.</small></span>                      <p><h4><span class="w3-border-bottom">Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boscastle - Off coast, close to Forrabury Church?<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Stormy weather<br>
              <span class="w3-border-bottom">Further Comments:</span> Bells made for this church were cast and blessed, but as the ship carrying them approached the church, the captain claimed that he, not God, had transported the bells safely. A storm suddenly blew in, sinking the ship and taking the bells to the bottom of the sea.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Colonel Hawker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boscastle - Penally House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Colonel unknown, horse on calm nights (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Footsteps belonging to this officer are said to wander around the property, while a figure has been seen passing by windows. The road outside the house is haunted by a horse, and a piece of local folklore claims secret tunnels connect the building to the beach.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tolling Bell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boscastle - Smugglers Cottage<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Last heard late twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The tolling of a phantom bell said to either foretell a shipwreck in the area, or the sound itself comes from an old sunken ship.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coachman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boscastle - Wellington Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure, complete with frilly shirt and ponytail, has been watched as he disappears though a wall near reception. The ghost of a young lady who committed suicide by leaping from a tower attached to the building has also been seen drifting forlornly in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Magical Giant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bosporthennis - Croft<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Said to have taken place every 01 August (when giant was alive)<br>
              <span class="w3-border-bottom">Further Comments:</span> A giant who lived nearby was said to use this site once a year for magical rites and was happy for local people to watch.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 333</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=333"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=13&totalRows_paradata=333"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
