
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in July">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > July</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>July - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rahere</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 (Greater London) - St Bartholomew's church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 July, 07:00h (Rahere) (reoccurring), others early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This pious monk, who doubled as a jester in Henry I's court, is thought to be the man who built the church and has been reported standing by the altar. He quickly disappears if seen. Another figure was observed in the pulpit by a former Rector, who said the man wore clothing from the Reformation period. A different witness spotted a woman in a white dress, while others have claimed shuffling footsteps were heard around the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Six Thousand Horsemen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Yarnton (Oxfordshire) - Road through village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 04 July<br>
              <span class="w3-border-bottom">Further Comments:</span> King Charles I and six thousand horsemen passed through Yarnton to avoid the siege of Oxford. Amazingly, they managed to reach Worcester undetected and now, on the anniversary of their escape, make the journey through Yarnton again in ghostly form.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tristram Dillington</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Knighton Gorges (Isle of Wight) - Knighton Shute<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 04 July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Former owner of the demolished Knighton Manor, Tristram Dillington drives a coach and four horses on the anniversary of his death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Dick of the North</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dewsbury (Yorkshire) - The Temple, aka Black Dick's Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 05 July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Known locally as Black Dick's Tower, this building is said to be the home of a phantom highwayman once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Westonzoyland (Somerset) - Sedgemoor Battlefield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 06 July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The spirits here reportedly include voices that call out to witnesses over the nearby River Carey, and horsemen who gallop silently along. People have also reported pockets of cold air, and watching a white woman glide over the marsh, near where her lover was murdered by Royalists. On the anniversary of the battle, sounds of fighting have been heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hymns</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgwater (Somerset) - Road from Bridgwater to Weston Zoyland<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 06 July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> As the Duke of Monmouth retreated after their defeat, his troops sang hymns to try to boost their morale. Their song can still be heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> East Grinstead (Sussex) - Ashdown House, Forest Row<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 06 July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year these footfalls are heard as they descend the staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Richard Fitzgeorge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lyndhurst (Hampshire) - Glasshayes Mansion (scheduled for demolition in 2016)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 07 July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The first Duc de Stacpoole is said to throw a party for the dead in July, the music from which can be heard. At other times of the year, people have reported faces at the windows of empty parts of the building or experiencing the sensation of being watched.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bride in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chagford (Devon) - Whiddon Park Guest House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 10 July (reoccurring), last seen 1971<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered by a former suitor on her wedding day, this bride now returns on that fateful date. Dressed in black, she has been seen standing in the doorway of a bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Duke of Monmouth</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Horton (Dorset) - Woodlands - Monmouth Ash Tree<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 16 July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Even though the Duke was executed at the Tower of London, his ghost now frequents the area he was captured after his failed rebellion. The ghost is said to carry his head in his hands.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bagpipes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lochranza, Island Of Arran (Ayrshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1700s onwards, calm summer evenings (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A bagpiper who died in the nearby hills now walks into village on calm summer evenings, the only indication of his presence the sound of his pipes. It is said that one tourist followed the sound of the pipes, only for the piping to suddenly cease when the holiday maker approached where they thought the music was coming from.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-51.jpg' class="w3-card" title='An old postcard of Horning village in Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Horning village in Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">Morphing Village</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Horning (Norfolk) - General area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 July, every five years  (next in 2021) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once every five years the village is said to revert to how it looked several hundred years ago.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Pass-of-Killiecrankie.jpg' class="w3-card" title='An old postcard showing Pass of Killiecrankie.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Pass of Killiecrankie.</small></span>                      <p><h4><span class="w3-border-bottom">Battlefield</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Pitlochry (Perth and Kinross) - Pass of Killiecrankie<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The bloody battleground between English Government and Jacobite Highlanders, the anniversary of the battle is marked by the ground turning blood red. Reports of soldiers in English and Scottish regalia, both lying dead and others fighting, have also been recorded. As if this was not enough, the area is also haunted by the drifting head of a woman murdered on the site during the seventeenth century, and the tall white spectre who tries to grab people passing along the roads; anyone the ghost touches dies within the year...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borley (Essex) - Site of Rectory, Nun's Walk<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 28 July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a regular apparition on the lawn of Borley Rectory, the figure still appears where the garden once existed. Borley was called the most haunted house in Britain, though the chief ghost hunter in the case appears to have been over enthusiastic in his analysis.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> East Carlton (Northamptonshire) - East Carlton House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early in Summer Evenings (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> One storyteller reported that this building was haunted by a lady that had jumped to her death from a balcony after being jilted by her lover.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Duke of Monmouth</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgwater (Somerset) - Castle ruins<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early July (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The Duke of Monmouth is said to have stayed at the castle just prior to the battle of Sedgemoor, his phantom possibly remaining in regret of defeat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jack</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cowes (Isle of Wight) - Model Railway Exhibition, Marine Court (no longer operating)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early summer (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen sitting near the shop, is it unknown whether this ghostly Jack Russell will return now the exhibition is closed. However, it has been reported that this story started as a joke to fool visitors, but quickly became part of local folklore.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter (Devon) - Exeter Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> July, 7pm (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen only around this time, the nun quickly disappears once spotted. A monk has also been reported in the area surrounding the cathedral, while a small group of witnesses heard music coming from within the building although it was dark and empty at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Middridge-Hill.jpg' class="w3-card" title='A gathering of fairies.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A gathering of fairies.</small></span>                      <p><h4><span class="w3-border-bottom">People in Green</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Middridge (Durham) - Hill near the village<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Seventeenth century, summer nights<br>
              <span class="w3-border-bottom">Further Comments:</span> Tiny people dressed in green would be briefly seen by locals visiting the site during the summer months. If alone. to catch a glance of the figures would be considered lucky, but if in a larger party, any witnesses to the fairies would be struck with ill-fortune. When the hill was being cut by railway engineers, the banks would collapse at night. Fairies were blamed for the disruption and navvies requested additional money in dealing with the paranormal problem (which was granted).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-109.jpg' class="w3-card" title='An old postcard of Whitby Abbey in Yorkshire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Whitby Abbey in Yorkshire.</small></span>                      <p><h4><span class="w3-border-bottom">Petrified Snakes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Whitby (Yorkshire) - Whitby Abbey<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Snakes unknown, St Hilda's ghost during summer months 10:00h - 12:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> The abbey was besieged by snakes, until St Hilda turned up and drove them all off the edge of the cliff, where they turned to stone on the beach below. Her ghost is also reported to still gaze from the windows of the abbey. A few people have heard a choir singing in the empty building and one local legend says a team of headless horses pulling a hearse was once spotted by a man and boy walking in the area. Finally, treasure hidden in the abbey's grounds is protected by a headless figure in white.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Priest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lartington (Durham) - Priest's Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> An Elizabethan priest is said to haunt this knoll, the site he was executed by soldiers for administrating forbidden rites.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Aberdovey.jpg' class="w3-card" title='An old postcard showing Aberdovey in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Aberdovey in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Lost Town</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdovey (Gwynedd) - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Swallowed by the sea, church bells belonging to a town lost off the coast here can still be heard on quiet evenings. They say that at low tides one can see sunken tree trunks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dancing Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gatcombe (Isle of Wight) - St Olave's  Church<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer (reoccurring) (Dog), young girl unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The carved wooden dog at the feet of the effigy of Edward Estur is said to come alive during summer nights and dance around the church standing on hind legs. The churchyard is said to be haunted by a ghostly young girl with blonde hair in a pale dress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Isabella of Valois</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sonning (Berkshire) - Site of Bishop's Palace (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer evenings (last seen late eighteenth/early nineteen century?)<br>
              <span class="w3-border-bottom">Further Comments:</span> Wife of Richard II, Queen Isabella' drifts around the field where the palace once stood. As the phantom grey woman moves, ghostly music emerges from the ground. George III's organist lived nearby and may have been the last person to witness the phantom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Waiting Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isfield (Sussex) - Lavender Line railway station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer evenings (reoccurring), last seen 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> This railway museum is haunted by a woman in white who is said to wait for a lover who never returns home. This Victorian woman later took her own life by throwing herself under a train.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 35</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=35"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=35"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
