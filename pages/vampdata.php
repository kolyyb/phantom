



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Vampires and Werewolves - Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Vampires/Werewolves</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Sightings of Vampires & Werewolves across the Isles</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Foaming Wolf</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A1067 (Norfolk) - Road between Fakenham and Norwich<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver on the way back from the cinema in Norwich encountered a large black wolf eating a carcass along this road. The creature was described as standing around a metre at the withers, with yellow eyes and black matted hair. The driver slowed and the wolf briefly looked up before continuing to eat. The witness continued home, shaken.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Creature from the Tomb</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alnwick (Northumberland) - Alnwick Castle<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> 1100s<br>
              <span class="w3-border-bottom">Further Comments:</span> The vampire that once frequented this castle, a former lord of the estate, lived under the site and would emerge at night to attack the local villagers. An outbreak of plague was also attributed to the unholy creature, and this resulted in the villagers digging the monster up from its shallow grave and burning it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Female Spectre</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beaumaris (Gwynedd) - Baron Hall<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local tale says that this old manor house is home to a female vampire, though many believe the entity to be ghostly in nature.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walking Dead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berwick on Tweed (Northumberland) - Exact area(s) unknown<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former wealthy man who died was said to have returned from the grave and walked the streets at night. A pack of dogs were said to escort him. Finally, a group of local men dug up the grave and incinerated the body, though most of the men are said to have died soon after from an unknown disease.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vampire Attacks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham (West Midlands) - Glen Park Road (Ward End) and Saltley, Small Heath and Alum Rock areas<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> A gentleman, described as black and in his twenties, bit another man walking along the street before pouncing on neighbours who came to the victim's aid. One woman present was said to have had a chunk bitten out of her hand. Police, however, stated that they had received no reports of such an incident and dismissed the story as an urban myth.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blood Sucker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blair Atholl (Perth and Kinross) - Glen Tilt, north of the town<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Two poachers shacking up in a bothy reported being attacked by a creature that drank blood from one of them. The pair managed to fight the creature off, and it flew away. The story is very similar to the vampire tale that happened at Fealaar, Aberdeenshire.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Doggett</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blandford Forum (Dorset) - Area near Eastbury Park (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A corrupt manservant who stole thousands of pounds from his employer, Doggett finally killed himself, and now drives his phantom horse and carriage along this area. One local story says he returned as a vampire; after his body was exhumed many years after his death (from St Mary's Church in Tarrant Gunville) it was found to be uncorrupted, with a rosy tint to the cheeks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Speed Wolf</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buxton (Derbyshire) - A6 northeast of Buxton<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Although this creature is reported to resemble a normal wolf, it moves at fantastic speeds and covers great distances in a single bound. It is unclear whether the wolf is a physical entity, but the nearby village of Wormhill claims to be the location where the last wolf in England was killed in the sixteenth century.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vampire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Croglin (Cumbria) - Croglin Low Hall<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> 1875<br>
              <span class="w3-border-bottom">Further Comments:</span> One of the few vampire reports in the UK, this creature attacked a young girl in this building. One of her brothers witnessed the attack and shot the monster in the leg. The blood trail enabled them to track it to the village graveyard, where it was dug up and burnt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/werewolf.gif' class="w3-card" title='An old woodcut of a man being attacked by a wolf.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a man being attacked by a wolf.</small></span>                      <p><h4><span class="w3-border-bottom">Welsh Wolf</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Denbigh (Clwyd) - Area between Denbigh and Wrexham<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Reported to be the size of a horse, this huge wolf would create mayhem in the region, feeding on livestock, dogs and the flesh of men.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wolf-headed Skeleton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dogdyke (Lincolnshire) - Langrick Fen, but exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, pre-1926<br>
              <span class="w3-border-bottom">Further Comments:</span> A local archaeologist digging in the peat discovered a human skeleton but with a wolf's head. The man took his discovery home, but during the night he found his house besieged by a werewolf. The archaeologist spent the night barricaded in the kitchen as the beast tried to gain entrance to his house, and as the sun came up and the werewolf left, the man took the skeleton and reburied the bones where they were found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coffin Carrying Returners</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drakelow (Derbyshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1090<br>
              <span class="w3-border-bottom">Further Comments:</span> Several servants fell ill and died after taking seed corn from the Abbey barns. The servants soon returned, their coffins carried on their backs while running across fields and knocking on doors. More villagers fell sick, and the pestilence only ceased when the dead were dug up and burned. The survivors fled the village and left it to fall into ruin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Suicidal Lycanthrope</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eccleshall (Staffordshire) - Village crossroads<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> April 1975<br>
              <span class="w3-border-bottom">Further Comments:</span> Andrew, a teenager from the village, was found dead at the crossroads having killed himself with a penknife. One of his friends reported that Andrew had called him just before midnight, saying that his skin was changing and that he was turning into a wolf.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Edale-General-area.jpg' class="w3-card" title='Werewolf or just a large dog?'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Werewolf or just a large dog?</small></span>                      <p><h4><span class="w3-border-bottom">Howl like a Foghorn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edale (Derbyshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> 1925<br>
              <span class="w3-border-bottom">Further Comments:</span> A large black creature of unknown origin caused havoc here in the 1920s when it killed dozens of sheep. Though rarely seen, the creature was said to have a howl like a foghorn. Locals allocated the blame on a lycanthrope.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Werecat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edmondthorpe (Leicestershire) - St Michael's Church<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> Effigies still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir Roger Smith and his two wives have their tomb within this church, with the effigy of Lady Ann possessing a dark mark on one whist. A local legend says that Ann was a witch who could change into a cat - the mark on her statue shows where a butler hit Ann in cat form with a small axe.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-68.jpg' class="w3-card" title='An old postcard of the Doone Valley in Exmoor.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of the Doone Valley in Exmoor.</small></span>                      <p><h4><span class="w3-border-bottom">Wolf Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exmoor (Devon) - Doone Valley<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman walking home after dark reported seeing a grey man with a wolf's head, apparently stalking a large rabbit. The werewolf vanished when disturbed by a stag that ran out from a nearby wooded area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vampire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fealaar (Aberdeenshire) - Hut hidden away in a wooded area<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> Two hunters were said to have been attacked by a vampire while spending the night in the bothy. This story is very similar to the vampire tale that happened at Blair Atholl, Lowlands, and is likely to be derived from it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stinker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Flixton (Yorkshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> There are stories that the area around Flixton is home to a werewolf, which has glowing red eyes and a particularly bad body odour.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/GlasgowNecropolis.jpg' class="w3-card" title='The Necropolis, likely taken between 1858-1872.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Necropolis, likely taken between 1858-1872.</small></span>                      <p><h4><span class="w3-border-bottom">Gorbals Vampire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow (Lanarkshire) - Southern Necropolis<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s (vampire, others unknown)<br>
              <span class="w3-border-bottom">Further Comments:</span> An urban myth circled the area during the 1950s that the graveyard was home to a vampire with iron teeth. At the story's height, it is said that hundreds of children patrolled the site, looking for the monster which, they thought, had kidnapped and eaten two local children. Another story to emerge from the Necropolis is that of the white woman - a feminine statue that sits on top of the grave of three people killed in a tramcar accident is said to turns its head and watch visitors as they pass.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Abhartach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glenullen (County Derry / County Londonderry) - General area, Abhartach's final resting place marked by a large stone with a thorn tree growing up from the side<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> Fifth century<br>
              <span class="w3-border-bottom">Further Comments:</span> In a battle for the throne, the warrior Cathrain killed his rival Abhartach. Abhartach, however, returned from the grave two days later and preyed on the locals. Cathrain once again killed the undead fiend, but two days later, Abhartach came back and continued his attacks. Cathrain discussed the problem with druids, who told him to stab his adversary through the heart with a weapon made of yew. This he did, and Abhartach never returned.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/two-witches.jpg' class="w3-card" title='A white-faced witch meeting a black-faced witch with a great beast. Woodcut, 1720.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A white-faced witch meeting a black-faced witch with a great beast. Woodcut, 1720.</small></span>                      <p><h4><span class="w3-border-bottom">Witch</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Holywell (Clwyd) - Basingwerk Abbey, and forests of Longdendale<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A witch who could change into a wolf was unable to return to human form after the Abbot of Basingwerk asked heaven to stop the witch from killing innocents. The witch/wolf ran into King Henry II's hunting party and after a vicious fight, the party slayed the creature.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wolf Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kingdom of Ossory (County Kilkenny) - Ossory was a sub-kingdom, most of which was in County Kilkenny<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> Sometime before 1100<br>
              <span class="w3-border-bottom">Further Comments:</span> It was said that men from this kingdom could transform into wolves whenever they pleased. However, while in wolf form their human bodies would be left at home. If moved, the person would be trapped as a wolf for the rest of their life.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Loch-Langavat-Lewis.jpg' class="w3-card" title='Werewolf on the beach.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Werewolf on the beach.</small></span>                      <p><h4><span class="w3-border-bottom">Werewolves</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Loch Langavat, Isle of Lewis (Outer Hebrides) - Exact area unknown<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The island was once home to a colony of wolfmen - even though they are extinct, disturbing their graves is reported to release their spirits.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hooded Dead Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lochmaben (Dumfries and Galloway) - Woodland around Lochmaben Castle<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> 1991 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Tom Robertson investigated the woods after hearing stories that animals had been found drained of their blood. He encountered a tall figure dressed in sacking with a hood over its head, which black eyes and grey face. The creature leapt into a tree and swung away. Eight years later Robertson went looking for the creature again, finding it and taking a couple of photographs.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Strange Wolf</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lynton (Devon) - Valley of the Rocks<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> This area was reportedly the stomping ground for a werewolf - sightings were reported up until the 1990s.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 36</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=36"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=36"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
