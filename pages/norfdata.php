

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Norfolk Ghosts, East Anglian Folklore and Strange Places in Norfolk, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Norfolk</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Norfolk Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Foaming Wolf</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A1067 - Road between Fakenham and Norwich<br>
              <span class="w3-border-bottom">Type:</span> Werewolf<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver on the way back from the cinema in Norwich encountered a large black wolf eating a carcass along this road. The creature was described as standing around a metre at the withers, with yellow eyes and black matted hair. The driver slowed and the wolf briefly looked up before continuing to eat. The witness continued home, shaken.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shaggy Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A1075 - Between Thetford and East Wretham, half a mile from the level crossing<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> June 1986, and 22 December 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> Driving towards Thetford from the direction of Watton, this witness spotted a large, long haired creature coloured greyish white. It had small ears, a long snout, large eyes, and stood on four legs. The witness drove past the beast three times in total, ensuring a good look at it; on the third pass the creature rose on its hind legs, taking an almost human form, and stood between six to eight feet tall. The driver quickly returned home, only returning two days later to look at the area again - the creature had gone, but there were tracks at the site suggesting something had been dragged. Another witness, a similar creature in December 2007, but said it was greyer in colour with black patches.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Male Silhouette</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A11 - Road from Norwich, heading towards London<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Between 06:00h-07:00h, January and February<br>
              <span class="w3-border-bottom">Further Comments:</span> On two occasions, a DJ returning to London encountered a lone figure on the road. On the first encounter during a light snow shower, the driver swerved to avoid the silhouette of a man in hat, though the figure vanished as the driver neared. A similar event happened again the following month, although the snow was replaced by mist.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Faceless Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acle - A47 (aka Acle straight)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2002<br>
              <span class="w3-border-bottom">Further Comments:</span> A figure wearing green with no face stood on the side of the road. Other motorists have reportedly had to brake hard to avoid hitting a figure which quickly and mysteriously vanishes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burge's Pooling Blood</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acle - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 07 April (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Josiah Burge was murdered by a man seeking revenge for the death of his sister. Josiah's blood is now reported to pool at the location on the day of the fatal attack. The stories state that Burge's ghost appeared only once - returning to kill his murderer.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Middle Aged Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acle - Heading west into Acle from Halvergate turnoff<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 March 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Passing the turn off to Halvergate towards Acle, a driver watched as a middle aged man walked out into the middle of the road (from the right hand side) in the path of their car. The figure turned to look at the driver, and the car passed straight through him. The area is traditionally known to be haunted by a horse and cart.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Heard's Lantern</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alderfen Broad - Marshland between Alderfen and Neatishead, known as Heard's Holde<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Misty nights. Event occurred pre-1890s<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Heard, a man who accidentally drowned after committing heinous crimes, would wave his lantern on misty evenings, trying to lure the unwary into the marshes. The phantom was finally trapped in a bottle by three men reading scripture and a boy with two pigeons which distracted the spirit.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadowy Forms</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashmanaugh - Council house along Church Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> This house is haunted by a female form that has been heard talking and seen only fleetingly.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Magic Oak Tree</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashwellthorpe - Ashwellthorp Hall<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 16th Century<br>
              <span class="w3-border-bottom">Further Comments:</span> In an act of Christmas kindness, Sir Thomas Knyvet welcomed a stranger into his home. The visitor used a magic acorn to grow a huge oak tree in the great hall in front of his Christmas guests. They chopped down the tree but were unable to move the fallen trunk - the stranger had to summon two goslings who picked the tree up and took it into the grounds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Feline</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Attleborough - A11, near Attleborough<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> February 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> A large unknown cat was seen by both motorists and a policeman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylmerton - Circular Hollows (local name Shrieking Pits)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1700s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The White Lady has been observed looking into each pit, crying out in pity. Her origin is unknown, but she could just be a story concocted by local smugglers. The sound of the screams heard in the pits, which local folklore attributes to the woman's phantom, is more likely to be the wind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Richard Andrews</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylsham - Black Boys Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Generally credited as being the first innkeeper, Andrews was killed during a fight with a member of the New Model Army. He was buried on the site and is said to haunt it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Thomas_Boleyn.jpg' class="w3-card" title='Thomas Boleyn, as pictured on his tomb.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Thomas Boleyn, as pictured on his tomb.</small></span>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylsham - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sentry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baconsthorpe - Baconsthorpe Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Soldier unknown, EVP July 2018<br>
              <span class="w3-border-bottom">Further Comments:</span> The former soldier who still patrols the battlements is blamed for the stones that are occasionally thrown into the moat, breaking the normally still waters. A visitor who recorded audio in the gatehouse (empty at the time) caught a snippet conversation between a man and woman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Henrietta Nelson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banningham - Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Henrietta Nelson accompanied her portrait, and when the painting was moved here by Bryan Hall, Miss Nelson came along. The face of the painting was said to change, sometimes changing from happy to sad in a matter of moments. The portrait is now back at Yaxley Hall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footmen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barford - Watton Road, between Barford and Kimberly<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 September 2013, 01:15h<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver had to swerve to avoid three figures standing in the road. One wore a long red cloak, while the other two were dressed as eighteenth century footmen. The figures paid no attention to the car. The driver later contacted nearby Kimberly Hall to see if they had a fancy dress event on that evening - they had not.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Big Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnham - Barnham Common<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 April 1985<br>
              <span class="w3-border-bottom">Further Comments:</span> An unidentified large cat was seen in the woods in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Beautiful Face in the Lake</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barton Turf - Barton Broad<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Accidentally killed by her father (or so the story goes), the woman in question intended to run away with a knight. Her spirit haunts the water and is said to be a blessing to any who see her face.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with Red Hair sitting by the Fire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barton Turf - Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Only appearing if the chair is placed by a fire, the red headed woman is believed to have burned to death in an abbey fire.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Miss Nelson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barton Turf - Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1850s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Miss Nelson is reported to accompany her portrait. Once haunting Cobb Hall, Yaxley, the ghost moved to Barton Turf when the painting was bought by the current owner. The painting was stolen in 1995 - the ghost vanished with it...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blue Lights Dance</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bawburgh - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 May (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Blue lights are said to appear and dance over the church well.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Digging Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beechamwell - Hangour Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Hill still present<br>
              <span class="w3-border-bottom">Further Comments:</span> As Old Nick dug out the nearby dyke, Hangour Hill was created with the discarded earth.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Entity</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beeston Regis - Old footpath which ran through the grounds of Beeston Priory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Between 1938 and 1943?<br>
              <span class="w3-border-bottom">Further Comments:</span> A hooded grey ghost would hide behind two boulders which once stood here, leaping out at passers-by at sunset. A local farmer grew tired of this and requested that one of the large stones be placed on his grave when he died, so that the phantom had nowhere in future to hide. The farmer died in the late 1940s - one boulder was used to cover his grave and the ghost was never seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belaugh - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Richard Slater</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belaugh - River<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Legend says entity appears nightly, but date of last sighting unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Slater worked at the rectory; he stole money, gems and other valuables from the church and buried them in the garden, waiting for a convenient time to recover the items to safely fence. Slater chose the time to do so badly, as he was caught by the rector as he dug the items up. In his haste to escape, Slater then drowned in the nearby river, weighed down by the stolen goods.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 404</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=404"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=16&totalRows_paradata=404"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
