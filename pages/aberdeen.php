
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Aberdeen Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Aberdeen</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Aberdeen Ghosts, Folklore and Forteana</h3>

 


                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bell Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Aberdeen Central Library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> The East of Scotland Paranormal Society investigated this building and claimed to have heard footsteps, whispering, and the sound of a bell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Aberdeen-Amatola-Hotel.jpg' class="w3-card" title='A phantom Victorian.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom Victorian.</small></span>                      <p><h4><span class="w3-border-bottom">Victorian Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Amatola Hotel (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantasmal female figure observed on the lower landing of the hotel may have been the daughter of one of the former owners of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Ardoe House Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990?<br>
              <span class="w3-border-bottom">Further Comments:</span> The daughter of a previous owner, this shade is thought to have returned after the girl's suicide. She is said to walk down the staircase dressed in nightwear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Arts Centre, 31 Kings Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> The centre is said to be haunted by a maid who still loiters around the old top floor flat and along corridors, while a floating face haunts the Green Room. The Aberdeen Ghost Hunters group investigated the site and recorded electrical interference and EVP.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Earth-Hound.jpg' class="w3-card" title='An overdramatic imagining of an Earth Hound.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An overdramatic imagining of an Earth Hound.</small></span>                      <p><h4><span class="w3-border-bottom">Earth Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Churchyard, Mastrick<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1915<br>
              <span class="w3-border-bottom">Further Comments:</span> Earth hounds are said to be the size of a rat, but with a dog-like head and feet of a mole. They live in churchyards and eat the recently buried. A dead earth hound may have been ploughed up in the Mastrick churchyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Walker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Cocket Hat public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1973 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> First spotted by barmaid Agnes McInnes, the ghost of the former landlord Walker is more commonly fleetingly seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pensioner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Constitution Street<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 February 1888<br>
              <span class="w3-border-bottom">Further Comments:</span> The remains of an aging soldier were discovered in a barn along this road; much of the body had been reduced to ash, though the face could be recognised. The bales of straw surrounding him failed to catch fire.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pacing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Former Halls of Residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1990s and 2002<br>
              <span class="w3-border-bottom">Further Comments:</span> One student reported hearing pacing along a corridor. When investigated, no one could be found in the area. The same student also awoke to hear someone repeating their name, although the room was unoccupied by anyone else at the time. Several years later, one girl encountered a tall dark figure which vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banging</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Gordon Place<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 06 January 1920<br>
              <span class="w3-border-bottom">Further Comments:</span> The house shook due to unexplained bangs and crashes that emerged from walls and floors. Police on the scene were also unable to track down the source of the sounds. Some blamed the disturbance on the ghost of a blacksmith, who had owned a property under the house and had taken his own life many years previous, although visiting spiritualists claimed the owner's father who died a few years earlier made the sounds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hooded Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Grassy area near a bus stop on the Kincorthland Estate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 January 1970, 05:45h<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman walking to the bus stop on her way to work in the early hours of the morning spotted a cloaked, hooded and masked phantom figure. The woman felt an overwhelming urge to approach the entity, which dissolved like mist and vanished into the grass.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sitting Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Hillhead Halls of Residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A student woke up to discover a man sitting on a chair next to her bed. The student screamed and the figure vanished. Another student in the room next door also claimed to have seen something dark move past while looking in a mirror.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jake</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - His Majesty's Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1982, and 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A former stagehand who lost his head in an accident at the theatre prior to the Second World War, Jake disappeared after building work ended in 1982. However, when renovation work started again, contractors began to report the sound of someone in high heels walking around empty areas of the theatre.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Officer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - King Street, former Barracks (currently Grampian Transport?)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A Captain with the Gordon Highlanders, this officer lost his bottle prior to being shipped overseas and took his own life. His ghost remains trapped within the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Watching</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Ma Cameron's public house, Little Belmont Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> People have reported feelings of being watched in the bar when the area is empty. One story says a decorator fled the site after hearing a series of knocks emerging from an empty room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Silhouette</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Old Fire Station, King Street (now student accommodation)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A former student who stayed here recalled how a silhouetted figure walked towards him as he stood in the shower - the witness jumped out of the way, injuring his heal. A couple of months later someone told the student that the building had a haunted reputation. It is said that several fire fighters based at the site when operational encountered a figure in Victorian uniform, said to be a man who died while saving a child.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Orange Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Over city<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A handful of reports concerning strange lights were recorded during 2006. One family observed six orange lights hovering in the sky before appearing to disintegrate.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Naked Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Palace Hotel<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This unclothed ghost hammered at the windows of the hotel for the period of a week. When finally spotted and approached, he floated away leaving no footprints in freshly laid snow.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Friendly Presence</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Private residence, Chaplains' Court, Chanonry<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970 - 1994<br>
              <span class="w3-border-bottom">Further Comments:</span> Author Agnes Short claimed to have seen a ghostly woman in her home, both gliding along a corridor and sewing while sitting. A small group of phantom men were also spotted sitting around a table. Latter owners said they could feel a friendly presence on the site but never encountered a ghost.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Purple Fingers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - St Machar Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Three schoolchildren reported seeing three strange purple fingers with long black nails, unnaturally long, emerging from the cathedral door. They ran.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Going Down</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - St Nicholas House<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> July 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> Two council workers watched a dark haired lady enter a lift, but when they also stepped inside a few seconds later, she had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Clencher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - Union Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Many people have reported feeling this entity grab their arms or legs, holding on for several seconds before letting go. It is believed to be a little girl who died violently nearby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Indian Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen - White Dove Hotel (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This young Indian girl manifested and stood by a guest dying of fever in the hotel - the nurse who saw the ghost said it had terrible injuries on the neck, so severe that it caused the nurse to faint. When the nurse regained consciousness, the strange little girl had gone and the patient passed away.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 22 of 22</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
