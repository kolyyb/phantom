
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Going Underground - Ghosts, Folklore and Strange Places on the London Underground, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > London Underground</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of the London Underground</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Faceless Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E11 - Beacontree Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1992<br>
              <span class="w3-border-bottom">Further Comments:</span> A station employee working alone heard the door to his office rattle several times. Unnerved, the man began to climb upstairs to find a colleague but felt he was being watched. Turning around, he saw a woman standing there with long blond hair but no face - her features were completely smooth. Talking to his colleague a short time later, the employee discovered that he was not the only person to have seen her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grinning Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E15 - Channelsea Depot, Stratford<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> June - December 1994<br>
              <span class="w3-border-bottom">Further Comments:</span> A former British Rail employee reported seeing a tall man wearing a cape and top hat standing by a hangar. He had a terrible grin and a mouth full of white teeth, and immediately vanished, leaving the witness very cold and apprehensive. A few months later, in the same area, the witness felt a strong tug at her bag that almost pulled her over; she spun around expecting to see a colleague, but no one was in sight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Workman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E17 - Blackhorse Road Underground Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The station is said to be haunted by the ghost of a workman who died during the construction. A figure has been observed on the platform late at night, and his footsteps heard in other parts of the station.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cries and Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E2 - Bethnal Green Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1981<br>
              <span class="w3-border-bottom">Further Comments:</span> A station master working alone in the station office late at night heard the soft sounds of children crying. As time went by, the cries grew louder and were joined by the screams of women. He ran from the office. One hundred and seventy-three people died in the station in a single accident during the Second World War, the vast majority being women and children.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anne Naylor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Farringdon Underground Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The odd screams that have been heard in this area are attributed to Naylor, thirteen year old girl murdered on this site in 1758. She is now referred to as 'the Screaming Spectre'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec2_1022.jpg' class="w3-card" title='The Central Line of Bank Underground Station, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Central Line of Bank Underground Station, London.</small></span>                      <p><h4><span class="w3-border-bottom">Sarah Whitehead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC2 - Bank Station, Central Line<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Sarah seen late twentieth century, knocking in 1982<br>
              <span class="w3-border-bottom">Further Comments:</span> Possibly the same figure that haunts the Bank of England; in life this poor girl could not handle news that her brother had died and returned daily to his office to meet him. Dressed in black clothing, she is affectionately called 'the Black Nun'. A worker once chased what he thought was an old lady locked in the station during the early hours of the morning, but she vanished down a corridor with no possible exit. In addition, at least one employee has reported something knocking on an empty lift door from the inside, way after normal closing time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Lon2139.jpg' class="w3-card" title='Liverpool Street Station, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Liverpool Street Station, London.</small></span>                      <p><h4><span class="w3-border-bottom">Rebecca Griffiths</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC2 - Liverpool Street Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Rebecca 1780-1812, man circa 2000<br>
              <span class="w3-border-bottom">Further Comments:</span> Once the site of the first Hospital of the Star of Bethlehem, an asylum for the insane, the area was haunted by the screams of Griffiths who was buried without a coin she compulsively held on to when locked away here. Her ghost also had the habit of exciting other inmates by peering through their cell windows. More recently there have been reports by underground staff of a man in white overalls on the platforms that can only be seen on CCTV.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Aldgate Underground Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-to-late-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This old woman was seen by an engineer as it stroked his friend's hair, seconds before the co-worker touched a live wire which sent 20,000 volts through his body. Remarkably, he survived. Phantom footfalls have also been reported coming from down the tunnel, abruptly finishing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Silhouette</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - King William Tunnel, Under London Bridge (disused underground tunnel)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s?<br>
              <span class="w3-border-bottom">Further Comments:</span> An image taken by a photographer shows what appears to be a silhouetted figure along this tunnel, though no one else was there at the time. A medium called to the location claimed that the ghost was that of a man who died while breaking up a fight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ashen-faced Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC4 - Northern Line into St Paul's tube station<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> On four consecutive days, a commuter on an early train sat opposite a girl whose long black hair covered her face, with only the tip of her nose visible. On the fourth day, as the commuter left the carriage, he realised that the ashen-faced girl was looking straight at him. The commuter never saw the girl again but was convinced that she was a ghost.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Steam Train</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N2 - East Finchley to Wellington Sidings underground<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This stretch of the Northern Line is reputed to be haunted by a spectral steam engine.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sounds of a Train</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N6 - Highgate High Level Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Started during the Second World War, the station was never finished, though locals sometimes reported the sound of a steam train along where the track was supposed to have been laid. One rational explanation put forward is that the sounds of the trains came from nearby stations which were active until the 1970.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nw1_3613.jpg' class="w3-card" title='Platform 10 of King&#039;s Cross Station, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Platform 10 of King&#039;s Cross Station, London.</small></span>                      <p><h4><span class="w3-border-bottom">Boudica's Grave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - King's Cross Station, Platform 10<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> The final resting ground of the warrior queen is reported to be under this busy platform.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Distressed Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - King's Cross underground station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness spotted a woman in her twenties with long brown hair, wearing jeans and t-shirt. The figure was kneeling at the side of the corridor with her arms outstretched, and appeared distressed and crying. Someone walking in the opposite direction then walked through the woman. The witness said that upon reflection, it was like watching a repeating piece of film.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bruno Hauptmann</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 (possible) - Bakerloo Line<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1983<br>
              <span class="w3-border-bottom">Further Comments:</span> A photograph of a ghostly figure taken by Karen Collett while travelling on the Bakerloo Line later transpired to be the waxwork figure of Bruno Hauptmann, although it is still unclear how Hauptmann's form (with added bolts of lightning) managed to manifest on the photograph.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2281.jpg' class="w3-card" title='Underground London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Underground London.</small></span>                      <p><h4><span class="w3-border-bottom">London Subterraneans</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Other London - No fixed location<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Current<br>
              <span class="w3-border-bottom">Further Comments:</span> This legend, which seemingly 'does the rounds' every few years, states that a group of Londoners began to live underground in the late nineteenth century, and now they have mutated. These subterraneans live off the junk food we discard in the underground tube stations and the odd commuter if they are discovered alone on the train.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Churchill</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Queensway - Queensway Station, Central Line<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> Witnessed waiting on the platform, Sir Winston Churchill once lived quite close to the station.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with a Red Scarf</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Uxbridge - Ickenham Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter evenings (reoccurring), 1950s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly figure stands at the end of the platform, close to where she fell and was electrocuted. She sometimes waves to attract attention before vanishing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon1023.jpg' class="w3-card" title='The Northern Line platform of the Elephant &amp; Castle underground station.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Northern Line platform of the Elephant &amp; Castle underground station.</small></span>                      <p><h4><span class="w3-border-bottom">Vanishing Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Elephant & Castle Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Northern Line unknown, Bakerloo woman seen twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Footfalls and rapping echo around the Northern Line in the station after it has closed - on investigation, no sound source can be found. Another story says the last train of the night on the Bakerloo Line is haunted by a lone girl who walks from the last carriage to the tip of the train, vanishing as she reaches the front carriage.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - London Road Depot (Bakerloo Line)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This area is thought to be haunted by a nun who is connected to a nearby Roman Catholic school.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Slamming Doors</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE11 - Kennington Loop<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> All passengers disembark at Kennington and the carriages are checked just prior to trains turning in the loop. However, as the train drivers sit waiting in the dark loop tunnel, at least two have reported hearing the connecting carriage doors open and close as if someone is moving from the rear of the train towards the driving compartment.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE11 - Side street by Kennington underground station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1984, around 03:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two employees at the station, while smoking outside and sitting on a wall, heard a woman sobbing no further than three or four yards (2.7 - 3.6 metres) away, although no one was visible. As the crying moved away, one of the workers followed the sound, which travelled down the road and into a side street behind the station, where it ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cries of the Trapped</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE13 - Lewisham Station, & St Johns area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 04 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A crash in 1957, caused partly by fog, killed ninety people and injured over one hundred. It is their cries which can be heard on the anniversary on the accident. Another version of the story says the phantom screams date to the Second World War.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Reflection</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE17 - Bakerloo line, Elephant & Castle and other stations along the line<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It is reported that occasionally, while travelling northbound, some passengers can see the reflection of someone sitting next to them, even though there is no one in the seat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tube Traveller</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE17 - Elephant & Castle Underground Station, Bakerloo line<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Woman seen late twentieth century, photograph taken in 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by both staff and commuters, this young woman enters the train's carriages, but is never seen leaving. Some also allocate the blame on the same entity when invisible footfalls create loud echoing around the station after hours.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 42</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=42"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=42"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Greater London</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View Greater London records" style="width:100%" title="View Greater London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
