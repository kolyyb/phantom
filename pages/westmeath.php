
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Co Westmeath Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Westmeath</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Westmeath Ghosts, Folklore and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse and Rider</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Athlone - Clonfinlough Stone<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This stone is carved with scenes which could depict an ancient battle. One legend says that at certain times of the year a horseman manifests and gallops around it. Another story states that a man named Michael will one day find two pots of gold under the stone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blue Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Athlone - Portlick Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The blue lady is said to have been seen a few times (although the dates are not stated) moving around close to the staircase of the castle. A phantom prisoner is also said to haunt the basement.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jacky Dalton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clonmellon - Killua Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> With wild yellow hair, Jacky's ghost still moves around the castle at night. Jacky was a land steward to Sir Benjamin Chapman, though he turned to drink and finally suicide after his employer's death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Delvin-Killough.jpg' class="w3-card" title='A strange little man in red.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A strange little man in red.</small></span>                      <p><h4><span class="w3-border-bottom">Small Man in Red</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Delvin - Area around Killough<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 1908<br>
              <span class="w3-border-bottom">Further Comments:</span> A little man dressed in red was spotted by children around the area, mysteriously vanishing from the sight of some of the witnesses while remaining visible to others. The local press at the time claimed the entity could have been an escaped monkey, although other people suspected a leprechaun.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hidden Cave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Delvin - Unknown hill in the area, (off the road to Ballinlough?)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A secret entrance on the hill leads to a hidden cave which contains treasure, so the story goes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cavalry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kilbeg - Hill in the area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Cavalrymen sleep upon their horses under this hill. It is said that firing a gun, which is also hidden within the hill, will wake them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rochfort's Blood</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kilbride - Dunboden Park Manor House<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 1797 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Lieutenant Commander Robert Rochfort was murdered on the doorstep of his home, possibly in retaliation after executing an innocent man. The bloodstain on the doorstep was said to be impossible to remove.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert Rochfort</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kilbride - Roads around Dunboden Park, and Lough Ennell<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1797<br>
              <span class="w3-border-bottom">Further Comments:</span> After being murdered at his home, Lieutenant Commander Robert Rochfort took to haunting the roads around his former home. A priest by the name of Father Timothy Shanley encountered the ghost, and after a brief but fierce battle that included holy water and brimstone, Rochfort was banished into Lough Ennell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Killucan-Knock-Shee-Ban.jpg' class="w3-card" title='A ghostly figure in the grass.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly figure in the grass.</small></span>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killucan - Knock Shee Ban hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The hill was once thought to be haunted by either a woman in white or a fairy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Frightful Beast</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lough Ree - Waters of the lake<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 800 AD and twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> One very old story says that a swimmer was torn to pieces by a monster after trying to leave a small island in this body of water. A creature was seen in 1954 by several people while out fishing. One of the witnesses, Georgina Carberry, suffered nightmares weeks after the event. Possibly because of this sighting, Captain Lionel Leslie set out to capture the beast using explosives in the first instance and a long net in the second, but with no success. In 1959, two English fishermen claimed their boat had been pulled around the lough by a creature (they escaped by cutting their fishing line) and in 1960 three clergymen observed a serpent-like beast. More recently, it has been speculated that the lake is home to a giant eel.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stone Thrower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Muckanagh - Unknown property and nearby bridge<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1939<br>
              <span class="w3-border-bottom">Further Comments:</span> Despite being warned by neighbours, a man took sand and stones from a nearby pit which contained the bodies of soldiers and used the material in his garden. The following day, the stones he had removed were thrown at his house by unseen hands. A holy man was summoned and banished the spirit to underneath a nearby bridge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Prisoners</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Mullingar - Arts Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Post 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> The prison that once stood on this site had a violent past, and ghosthunters have suggested that this is the source of the strange occurrences reported in the arts centre. Footsteps echo in empty places, doors open and close unaided, and a man (who can only be seen by some people) spotted in the auditorium.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Face</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Mullingar - House along Mount Street (currently Golden Hill restaurant)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Post 1846<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly face would be seen gazing from a window, looking towards the site where Brian Seery was hanged for assault - a crime it would later transpire Seery did not commit. The window is now blocked off. Another version of the story says the window turned black to prevent the hanging from being watched.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rail Employee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Mullingar - Scoutail Bridge (bridge over railway line)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local story says that the bridge is haunted by a former station employee killed in an accident.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Scarecrow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Mullingar - St Bridget's Ward at St Loman's Hospital<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> December 2014<br>
              <span class="w3-border-bottom">Further Comments:</span> A photograph published by the local press that showed a strange face looking out from a window was later identified as a scarecrow left in the room by previous visitors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Healing Well</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Mullingar - St Bridget's Well, Ash Road<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May - 09 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> It was once believed that drinking from the well for these nine consecutive days in May would result in any disease being cured.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Opening Doors</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Mullingar - Westmeath Examiner Newspaper Offices<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> These offices are supposed to be home to figures spotted in empty rooms and doors which open and close unaided.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knocking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Not known - Emo House (now demolished)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> John Seymour wrote of Emo House in this county being haunted by knocking on the front door and the periodic opening and closing of doors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Guaire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Rathwire - Hillfort<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Hillfort still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The warrior and chieftain Guaire is reputed to be buried under this hillfort, guarded a savage hound that will let no one near the body.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Steeple of Fire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Rosdalla - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 April, 1054 or 1055<br>
              <span class="w3-border-bottom">Further Comments:</span> A great steeple of fire manifested in the skies over Rosdalla, remaining for some nine hours. Unidentified black birds emerged from it, swooping down and picking up small animals, dropping them from a great height. One of these birds was huge and plucked an oak tree from the ground. The birds eventually flew off and the tower vanished. Some have said that the event was actually one of the earliest tornadoes documented in Europe.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Tullynally-Castle.jpg' class="w3-card" title='A ghostly butler stands in a corridor.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly butler stands in a corridor.</small></span>                      <p><h4><span class="w3-border-bottom">Butler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Tullynally - Tullynally Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The butler fell in love with one of the castle's cooks but was rejected. He later hanged himself, though has been seen since, walking around the castle and its grounds. One witness saw a thirty year old Victorian woman standing in a guest room.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 21 of 21</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
