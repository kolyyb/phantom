


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Bell and Ringing Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Bells</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Phantom Bells and Ringing Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bell Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen (Aberdeenshire) - Aberdeen Central Library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> The East of Scotland Paranormal Society investigated this building and claimed to have heard footsteps, whispering, and the sound of a bell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Aberdovey.jpg' class="w3-card" title='An old postcard showing Aberdovey in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Aberdovey in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Lost Town</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdovey (Gwynedd) - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Swallowed by the sea, church bells belonging to a town lost off the coast here can still be heard on quiet evenings. They say that at low tides one can see sunken tree trunks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swallowed Sounds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdyfi (Gwynedd) - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Calm nights<br>
              <span class="w3-border-bottom">Further Comments:</span> The bells of a church which was swallowed by the sea can sometimes be heard when the weather is right.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf11893.jpg' class="w3-card" title='Off the coast of Aldeburgh, Suffolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Off the coast of Aldeburgh, Suffolk.</small></span>                      <p><h4><span class="w3-border-bottom">Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldeburgh (Suffolk) - Coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Much of Aldeburgh has been taken by the sea, leading to the belief that the bells from sunken churches could sometimes be heard under the waves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alderwasley (Derbyshire) - St Margaret's Chapel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Though disused, locals have reported singing and bell ringing from this aging chapel.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Armboth Fell (Cumbria) - Thirlmere Lake<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The echoes of phantom bells have been heard coming from beneath the waters here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stubborn Bell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barlinch (Somerset) - Barlinch priory (now ruins)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> When the priory bells were removed from the site, the Great Bell did not want to leave its location. Eventually the bell was dragged away (after a single unhappy peal) by man and ox to its new home in Exeter Cathedral, where it promptly cracked once mounted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chime</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford (Bedfordshire) - Willington Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of footsteps and the soft ringing of a bell haunt this old building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc2871a.jpg' class="w3-card" title='Off the coast of Blackpool.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Off the coast of Blackpool.</small></span>                      <p><h4><span class="w3-border-bottom">Kilmigrol</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackpool (Lancashire) - Off the coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: stormy nights. Last heard 21 May 2013?<br>
              <span class="w3-border-bottom">Further Comments:</span> A town lost off the coast of Blackpool many years ago, the bells can be heard ringing on stormy nights. The bells may have last been heard by a mother and daughter walking near Sandcastle Water Park.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bell for the Dead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenporth (Dyfed) - General area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Before a death in the village<br>
              <span class="w3-border-bottom">Further Comments:</span> If the phantom bell is heard to ring three times at midday or midnight, someone of importance is due to die.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blanchland (Northumberland) - Abbey grounds<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed by a group of bandits, the monks of this abbey can occasionally be seen drifting across their land. The phantom pealing of the abbey bell accompanies them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Coffins</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borley (Essex) - Church<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Ninetieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Music, crashes, thumps, bell ringing and footsteps have all been reported emerging from the church after it has been locked up for the night. A local tale involves the coffins in the Waldegrave family crypt mysteriously moving on their own accord between the times the tomb was opened.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cantre'r Gwaelod</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borth (Dyfed) - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 1770<br>
              <span class="w3-border-bottom">Further Comments:</span> To the west of Borth, off the coast, lays the lost land of Cantre'r Gwaelod, a walled country which sunk after a prince forgot to close the flood gates. The bells are said to ring in times of danger, and the lost land was reputedly spotted under the sea in 1770 by William Owen Pughe.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-21.jpg' class="w3-card" title='An old postcard of Boscastle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Boscastle.</small></span>                      <p><h4><span class="w3-border-bottom">Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boscastle (Cornwall) - Off coast, close to Forrabury Church?<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Stormy weather<br>
              <span class="w3-border-bottom">Further Comments:</span> Bells made for this church were cast and blessed, but as the ship carrying them approached the church, the captain claimed that he, not God, had transported the bells safely. A storm suddenly blew in, sinking the ship and taking the bells to the bottom of the sea.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tolling Bell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boscastle (Cornwall) - Smugglers Cottage<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Last heard late twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The tolling of a phantom bell said to either foretell a shipwreck in the area, or the sound itself comes from an old sunken ship.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Underwater Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bosham (Sussex) - Bosham Creak<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> When Danish raiders took the bells from the church, they pushed the Lord too far - he sunk their boat, and now the bells can be heard ringing in celebration.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Braidwood (Lanarkshire) - Wooded area between Braidwood and Crossford, near St Oswald's Chapel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A monastery once stood near these woods, and even though it has long since vanished, the bells can be occasionally heard ringing out and a ghostly monk walks between the trees.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgnorth (Shropshire) - Croft Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A mystery woman in black is said to haunt the dining room. Bells have been heard ringing in other parts of the hotel after dark.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Parochial-Offices.jpg' class="w3-card" title='Parochial Offices in Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Parochial Offices in Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton (Sussex) - Parochial Offices, Princes Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s, evenings<br>
              <span class="w3-border-bottom">Further Comments:</span> The sound of a ringing bell would be heard on the top floor of this building, normally by a caretaker who was alone on site. Another witness claimed to have seen a woman on the staircase waving a handbell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol (Somerset) - Safestore, Ashton Gate<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first Century<br>
              <span class="w3-border-bottom">Further Comments:</span> A customer and his parents at the facility heard bells ringing and echoing around the corridors - they spent some time trying to find the source of the discordant sounds but failed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tolling Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broadway (Hereford & Worcester) - Middle Hill wood<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Second World War<br>
              <span class="w3-border-bottom">Further Comments:</span> A set of church bells were hidden in the forest during the Reformation and forgotten about. They are said to ring at night, and were heard during the Second World War, a time when bell ringing was illegal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-120.jpg' class="w3-card" title='An old postcard of Bude, Cornwall.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Bude, Cornwall.</small></span>                      <p><h4><span class="w3-border-bottom">Vanishing Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bude (Cornwall) - The Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A gardener spotted a shadowy figure in the locked building, which vanished after he gained access. The bell of the HMS Bude which hangs on site supposedly rings unaided.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf360c.jpg' class="w3-card" title='Greyfriars Priory, Bungay.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Greyfriars Priory, Bungay.</small></span>                      <p><h4><span class="w3-border-bottom">Monk's Chanting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bungay (Suffolk) - Greyfriars Priory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sound of plainchant has been heard coming from the ruins of the old priory, accompanied by the ringing of bells that have now been long gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burford (Oxfordshire) - Burford Priory & Old Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Bell at 02:00h (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom monk is only one of several spooky occurrences at this site; another is an unearthly bell that makes itself heard at two every morning, and the dead gamekeeper who patrols the area with his rifle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lost Village</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burrator Reservoir (Devon) - Under water<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An old village was submerged when the reservoir was constructed in 1898. The church bells are supposed to occasionally still ring.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 130</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=130"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=130"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
