
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="London Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > SW1 - SW20 Districts</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>London (SW1 - SW20) Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Baldwin Buckstone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - (Haymarket) Theatre Royal<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> A welcome ghost, Buckstone only manifests when a performance is destined to make it big. Patrick Stewart reportedly spotted Buckstone during a performance of Waiting for Godot. Another former manager of the theatre, David Morris, is reported to return maybe once a decade to the theatre just to cause trouble for a single night, before returning to his grave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Aunt Ann</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - 19 St James's Place<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1864<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of an old aunt appeared to three members of the family just prior to the passing of another aunt staying in the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ringing Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - 9 Earl Street, Westminster<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1820s<br>
              <span class="w3-border-bottom">Further Comments:</span> Over a two and a half hour period, every bell in the house constantly rang out. The strange activity ended as suddenly as it had begun.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Diving Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Between Westminster and Greenwich, close to Westminster Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A man crossing the river on a steam ferry watched as another passenger, a lady wearing a black veil, jumped into the water. He quickly dived after her but was unable to find the lady. He swam back to the ferry and was pulled from the water by the captain and a few others. When the man told the captain what had happened, the captain told him she had been seen twice before that week, performing the same action, but could never be found in the water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Mist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Buckingham Gate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2020<br>
              <span class="w3-border-bottom">Further Comments:</span> A black smoke was seen to manifest and cross the road before abruptly vanishing while apparently moving towards the palace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chained Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Buckingham Palace<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to predate the construction of the palace, this monk appears bound in chains once a year on Christmas Day. The sound of a suicide (the single shot of a pistol) is occasionally heard on the first floor - the private secretary to King Edward VII ended his life here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lillie Langtry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Cadogan Hotel, Sloane Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This quiet little spook is said only to come out when the hotel is nearly empty. During life, Lillie was the mistress of the Prince of Wales, who later became Edward VII.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty Shape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Clarence House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> A misty shape scared a worker in the building, causing her to flee. The visitation had been preceded by much banging and crashing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Cockspur Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1975<br>
              <span class="w3-border-bottom">Further Comments:</span> A taxi driver watched a decapitated woman cross the road in front of him, vanishing once reaching the pavement. The figure is thought to be the same entity that haunts St James's Park.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Green Park underground station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> A young track engineer sat at the station waiting for the traction current to be cut off. The engineer felt a slap on the face and turned to see a grey figure vanishing along the platform. Another man present saw nothing, other than the victim flinch and swear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Henry Colt Duelling</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Green Park, Piccadilly<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Sir Henry appears once a year (reoccurring), but date unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir Henry, along with fellow duellist Beau Fielding, meet repeatedly to fight for the heart of the Duchess of Cleveland. The sounds of heavy breathing and fighting are heard emerging from the misty mornings. The park is also said to have a 'tree of death', where many people have hanged themselves over the years; now a tall dark figure is sometimes seen standing behind it, disappearing if approached. Finally, an early twentieth century report says that phantom violin music would be heard around the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Greencoat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Greencoat Boy public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The pub is a former hospital which housed seventeenth century children who had lost their fathers during wartime. The ghost is thought to date from this period, and now haunts the bar area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Matron</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Grey Coat School (Hospital)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Stories are still told today about the murdered nurse, and how her ghost walks various parts of the building, scaring the young students.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Reader?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Harrods department store<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> Justin Timberlake is said to have hired out the store after hours in an attempt to encounter the ghost which reputedly haunts the book department.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Herbert Beerbohm-Tree</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Her Majesty's Theatre, Haymarket<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Responsible for funding the theatre's rebuild in 1899, Sir Herbert was the manager and an actor, and his favourite seat in the theatre was the top box to the right. In recent years (so they say) people sitting here have felt cold spots and had the door open on its own accord.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon7053.jpg' class="w3-card" title='na'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> na</small></span>                      <p><h4><span class="w3-border-bottom">Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Jubilee Line, from Westminster to Stratford<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Since the construction of the Jubilee Line, reports of phantom monks walking the tracks have begun to emerge. The sightings may be connected to the large number of graves which were disturbed while work was commencing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Face Puller</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Lowndes Square, Kensington<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An old white haired woman in a chair has been seen sitting along this street, making funny faces at anyone who looks at her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Prisoners</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Morpeth Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The basement of this pub was used to prevent high risk prisoners escaping from the nearby Millbank Prison over two hundred years ago - the ghosts of those held are said to prod anyone in the cellar who they take a disliking to.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/1207veiled.jpg' class="w3-card" title='An illustration of a ghostly woman wearing a veil, by Wayne Lowden.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An illustration of a ghostly woman wearing a veil, by Wayne Lowden.</small></span>                      <p><h4><span class="w3-border-bottom">Woman with Veil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - New Scotland Yard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Woman unknown, other events 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> Some say that the entity which haunted the old New Scotland Yard moved along with the exhibits of the Black Museum (aka Crime Museum) and can now be found within the walls of this famous police station. More recently, it has been reported that perfume can be detected in a fire escape near the museum, while a new recruit encountered the ghost of a former staff member.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2183.jpg' class="w3-card" title='Old Admiralty House, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Old Admiralty House, London.</small></span>                      <p><h4><span class="w3-border-bottom">Margaret Reay</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Old Admiralty House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1969<br>
              <span class="w3-border-bottom">Further Comments:</span> Shot by a former lover, Reay haunts one of the bedrooms in the building, a place that she was not unfamiliar with.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2188.jpg' class="w3-card" title='The old New Scotland Yard building, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The old New Scotland Yard building, London.</small></span>                      <p><h4><span class="w3-border-bottom">Hooded Female</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Old New Scotland Yard, Parliament Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Haunting the Black Museum, this ghost is thought to be that of a woman whose body was found on site as the building was erected. The remains had no head, and the void beneath the spectre's hood reflects this. In the early part of the century, the Reformatory and Industrial Schools unit was haunted by something which opened windows and slammed doors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/sw19140.jpg' class="w3-card" title='The Palace of Westminster, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Palace of Westminster, London.</small></span>                      <p><h4><span class="w3-border-bottom">Mrs Milman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Palace of Westminster<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> 1899 (Milman), pre-1920s (MP), 1989 (other)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghost seen around the palace was identified as (the still living) Mrs Milman, and although she worked at the site as Assistant Clerk, she was not present at the time of the sightings. In a contemporary newspaper report, Milman said that she would be spotted in places where she was not present. Around the same time, Sir Henry Campbell-Bannerman and other MPs witnessed a colleague sitting in the House of Commons chamber, even though they knew the colleague to be seriously ill at home. In 1989, a lift operator claimed an unseen force picked him up and tossed him fifteen feet along a corridor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">American Air Force Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Private flat, Vandon Court<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness found a USAF service man in their apartment, wearing a uniform and holding a clipboard. The figure was said to be handsome and happy, and extended their hand, which the witness shook. The witness's hand began to freeze, causing them to scream loudly. The cries brought neighbours to the apartment, by which time the figure had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elderly Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Private residence, Cadogan Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> This witness awoke to find himself looking at an old man with salt and pepper hair, fully black eyeballs and prominent teeth. The witness said nothing until his sister stated she had seen the figure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Animated Queen Anne</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 - Queen Anne's Gate, Westminster<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year the statue of the Queen becomes animated and walks around the neighbourhood. Until recently, the statue was believed to be Bloody Mary, until a clean-up operation revealed an inscription pointing out the error.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 106</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=106"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=4&totalRows_paradata=106"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Main London Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Reports: People</h5>
     <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View tReports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2024</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
