
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Bournemouth Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Bournemouth</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Bournemouth Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor1752.jpg' class="w3-card" title='Bournemouth Town Hall, Dorset.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bournemouth Town Hall, Dorset.</small></span>                      <p><h4><span class="w3-border-bottom">First World War Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Bournemouth Town Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This incorporeal soldier of Indian heritage appears here once a year to help himself to a drink of water. There are also a few reports of phantom horses and carriages being seen outside the building, while a ghostly cat is also said to haunt several of the town hall rooms.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figures in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Branksome Park estate<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen in a garden on this estate, several little people dressed in brown played in and around the bushes before being scared off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blue Spheres</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 26 January 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> Steve Hornsby found a dozen jelly-like, small blue balls in his garden following a hailstorm during which the sky briefly turned dark yellow. While the balls were not formally identified, it was suggested that they could be marine invertebrate eggs, discharge from an airliner toilet, or a kind of plant feed gel.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor6659.jpg' class="w3-card" title='Langtry Manor Hotel, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Langtry Manor Hotel, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">Lillie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Langtry Manor Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> The story is that this building was commissioned in 1877 by Edward VII as gift for his mistress Lillie Langtry. The house became an hotel in the 1930s, and during the 1970s, the grey form of a woman, thought to be Langtry, was spotted several times by the hotel's chef. All this told, there is no historical documentation which links the building to Langtry - the story looks as if it was constructed around the 1940s, which means that the ghostly grey lady remains unidentified.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor5326.jpg' class="w3-card" title='Millhams Lane, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Millhams Lane, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Millhams Lane, and surrounding Longham area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February (reoccurring); last confirmed in 1970s, but legends say she continues<br>
              <span class="w3-border-bottom">Further Comments:</span> The area around the A348 bridge is reportedly haunted by a ghostly white (or grey) woman, killed when hit by a horse and trap along the road. One story says she tries to entice men to jump off the bridge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor6517.jpg' class="w3-card" title='West Howe Baptist Church, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> West Howe Baptist Church, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Outside West Howe Baptist Church, on the corner of Holloway Avenue and Anstey Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1979<br>
              <span class="w3-border-bottom">Further Comments:</span> A lady dressed in a white gown floated across the road before disappearing through a wall which led to a garden.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor8062.jpg' class="w3-card" title='Pavilion Theatre, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Pavilion Theatre, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">Emily</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Pavilion Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom named Emily was thought to be an actress who died in the 1930s. She was blamed for the disembodied footsteps which could be heard on the empty stage, and the occasional poltergeist-like outburst which involved items being thrown around. One member of staff was even said to have been chased along a corridor by the entity. A newspaper article written in 2007 and just prior to the theatre being renovated claimed that the building work may silence Emily, the implication that she was little more than squeaky floorboards and vivid imagination.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Don't Move the Painting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Private house, Talbot Woods area<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1988<br>
              <span class="w3-border-bottom">Further Comments:</span> Poltergeist activities were said to occur if a painting of an older woman was removed from the wall. Whether the painting or polt are still there is not known.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cabinet Toppler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Private residence, Abbott Road, Winton<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1981<br>
              <span class="w3-border-bottom">Further Comments:</span> A family fled their home after a poltergeist outbreak caused chaos. The entity was said to have toppled a kitchen cabinet in front of visiting police, and a priest had his holy water thrown back at him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">People</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Shelley Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The haunting here appears to be restricted to stories of ghostly people walking down corridors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor5325.jpg' class="w3-card" title='St Andrews Church, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> St Andrews Church, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">Smugglers in the Tunnels</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - St Andrews Church, Millhams Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The graveyard here is said to contain blocked off tunnels once used by the smuggling community to move their contraband away from the coast. The ghosts of these men are still reported to flit around the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Talbot Woods<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A shadowy hound, said to be slightly larger than a Saint Bernard dog, was reported to haunt this area. The creature has never seen directly, but only from the corner of the eye. Scraping sounds resembling claws on concrete can also be heard, always behind the witness.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor5324.jpg' class="w3-card" title='The Acorn pub, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Acorn pub, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">Smuggler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - The Acorn public house (was The Dolphin and then Gullivers), Kinson<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980, and 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> When this public house was known as The Dolphin, one witness reported waking during the night to the sounds of clinking. Leaving their room to investigate further, they came across the apparition of a man perched on a chest, counting gold coins. After the pub became Gullivers Tavern, mild poltergeist activity was reported, including glasses and pool cues being thrown around, and footsteps heard in empty parts of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Heavyset Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Two Counties Radio station, Southcote Road (now Heart Dorset)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A presenter working alone in the building reported seeing a man of compact build reflected in the window of the studio he was in. The presenter turned to have a better look at the figure but realised that he was still alone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor13458.jpg' class="w3-card" title='Upper Gardens, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Upper Gardens, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">Four Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Upper Gardens<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> On a couple of occasions, four men wearing long black coats with brimmed hats have been seen standing in a row.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor6498.jpg' class="w3-card" title='West Cliff Road, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> West Cliff Road, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">Puma</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - West Cliff Road<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2005, November 2018<br>
              <span class="w3-border-bottom">Further Comments:</span> In 2005, salesman Kevin Hamersley from Wiltshire said he spotted saw a puma along this road, just one of many sightings in the region. Thirteen years later, a panther was spotted on the patio of another West Cliff resident. It is speculated (and highly unlikely) that the creature lives in the nearby Alum Chime.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor5960.jpg' class="w3-card" title='Western Avenue, Branksome, Bournemouth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Western Avenue, Branksome, Bournemouth.</small></span>                      <p><h4><span class="w3-border-bottom">Tasmanian Wolf</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth - Western Avenue, Branksome<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 07 April 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> A strange cat-like creature spotted by a female driver was later identified by her as a Tasmanian wolf, though the creature is thought extinct.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 17 of 17</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>


</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
