
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Buckinghamshire Ghosts and Mysteries from The Paranormal Database.">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southeast.html">South East England</a> > Buckinghamshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Buckinghamshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Two Men Crossing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A421 - Dual carriageway between Tingewick and Buckingham<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 17 January 2016, 10:35h<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver observed two figures across the dual carriageway, who seemed to disappear into an out of sight staircase on the verge. As the driver passed the verge, he could see no staircase or other concealed exit where the figures could have vanished without trace. One was described as very tall while the other was short with baggy clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Something in the Cellar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amersham - Boot and Slipper public house, Rickmansworth Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2001<br>
              <span class="w3-border-bottom">Further Comments:</span> Staff here were said to be too afraid to go down into the cellar after they felt someone walk past them, while one male worker felt a phantom hand grasp his shoulder.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/henryviiia.jpg' class="w3-card" title='Henry the Eighth, recreated at Madame Tussauds.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Henry the Eighth, recreated at Madame Tussauds.</small></span>                      <p><h4><span class="w3-border-bottom">Limping Priest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amersham - Chenies Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom figure was seen dragging one of its feet while traversing the floor, though it was more frequently heard than seen. Some have named the figure as Henry VIII, rather than a priest, who stayed here with Anne Boleyn and, several years later, Catherine Howard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Auden</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amersham - Chequers public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2001<br>
              <span class="w3-border-bottom">Further Comments:</span> Named Auden by a passing medium, this figure dressed in white is thought to be older than the public house itself. A hooded figure was also reported in an upstairs bedroom, thought to be one of seven people held here before being executed for their religious beliefs, while a ghost of a chimney sweep also haunts the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amersham - Crown Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in an old fashioned uniform, this grey ghost is thought to be a former maid. She has been known to pack guest's suitcases for them!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lesbian in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amersham - Elephant and Castle public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2001<br>
              <span class="w3-border-bottom">Further Comments:</span> The landlady of this public house reported members of her family had seen a ghostly woman dressed all in black walking through walls near the kitchen. The same ghost is blamed for pinching barmaids' bottoms, convincing them that the ghostly girl was gay.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Helena Stanhope</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amersham - Woodrow High House (aka Elmodesham House)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> After Lady Helena Stanhope's lover, Sir Peter Bostock, was executed for treason, she committed suicide in the grounds of her house. Stanhope's green ghost is now reported to walk the building, her footsteps occasionally heard, or her presence felt while creating mild poltergeist activity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Electric presence</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amersham Common - Bendrose House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A bedroom in this farmhouse is haunted by an entity that is said to wake people staying in it with a brief electric shock. The shade of Cromwell has been named as a possible explanation, as has nylon bed covers...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aston Clinton - Akerman Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing an old fashioned brown coat, this phantom would drift from Vatche's Farm and travel down the old Roman road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aston Clinton - Partridge Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000<br>
              <span class="w3-border-bottom">Further Comments:</span> A crying child and a female organ player are reportedly two of several ghosts that have remained on earth to haunt these premises.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Former Vicar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aston Sandford - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be a nineteenth century vicar, this ghostly apparition manifests by the pulpit just before evening prayer. There he remains for a few minutes before fading from view.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rosamund Clifford</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury - Creslow Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1850<br>
              <span class="w3-border-bottom">Further Comments:</span> This feminine ghost could be heard lightly running down corridors, her silk dress rustling as she went. The silken sound was heard again later in the evening, but this time moving quite violently.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Faceless Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury - Estate Agents (no longer present), Anchor Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1994<br>
              <span class="w3-border-bottom">Further Comments:</span> A cleaner in this building reported seeing a shadowy, charcoal coloured figure, around six foot tall with no facial feature striding across a balcony. The figure vanished into a wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Lee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury - Hartwell House Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former owner of the establishment, Lee is still reported in the library and garden.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/220505cromwell1.jpg' class="w3-card" title='Unfinished portrait miniature of Oliver Cromwell.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Unfinished portrait miniature of Oliver Cromwell.</small></span>                      <p><h4><span class="w3-border-bottom">Cromwell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury - King's Head public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Lord Protector of England is said to return to the room in this pub which bears his name.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury - Private property along Walton Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1994<br>
              <span class="w3-border-bottom">Further Comments:</span> A student who took a cleaning job at this site heard heavy footsteps walking around in the upper part of the building although no one else was there. His cleaning equipment moved itself around while he was not watching, and on one occasion he was pushed on the staircase by an unseen force.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pale Grey Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury - Private residence, Bracken Way<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> The pale grey figure of a person was seen outside the front door of this property. A two year old child of an occupant was seen talking to what the child later said was an old man.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury - Saracen's Head public house<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1991-2005<br>
              <span class="w3-border-bottom">Further Comments:</span> George is thought to have been a local man; the haunting of this pub started shortly after his death. Tables and chairs are moved around by invisible hands at night, and unidentifiable banging has been heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Shadow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury - Surgery (name withheld)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A GP returned home as white as a sheet, claiming that he had encountered a large shadow which followed him along a brightly lit wall at his surgery. He had been the only person working there at the time. A receptionist also encountered a large ghostly figure which tapped her shoulder, causing her to turn around and see the entity close behind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury - Unnamed field in a village close to the area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> For several nights, a man walking to milk his cows in the early hours of the morning encountered a large black dog which blocked his path. He finally grew tired of having to walk around the hound and, while walking with a friend, struck the creature - the dog vanished but the man fell down paralysed, never to be able to move or speak again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Thin Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beaconsfield - Chiltern Cinema (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> The thin, grey haired wraith reported here was named as a former manager. The entity was said to be annoying rather than frightening and was frequently seen on stage by staff after all members of the public had left.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach Wheels</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beaconsfield - Road towards Beaconsfield from Gerrards Cross<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> Several times during the 1920s, the sound of coach wheels could be heard travelling along the road, though nothing was ever seen. It is thought they are related to an accident involving a coach and horses occurring in the early nineteenth century.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drummer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beaconsfield - Royal Standard of England public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Woman unknown, drumming heard 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> The drummer boy was one of twelve Cavaliers executed outside the pub and is the only one who remained earthbound. He is not completely alone however, as the building is also said to be haunted by a woman who loiters in the lady's restroom. Two members of the Luton Paranormal Society heard the sound of a drum outside the pub while at the site but were unable to find any explanation when they further investigated.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedgrove - The Duck public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> The owners of this pub reported seeing a figure in black standing by the bar after closing hours. The entity also moves heavy items around the cellar at night, and the sound of screaming has also been heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bennett End - Road between village and Radnage Bottom<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says that a phantom coach with horses sometimes travels down this road at night.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 131</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=131"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=131"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southeast.html">Return to South East England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
