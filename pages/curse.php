



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content=" Curse-Related Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Curses</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Tales of Curses from Around the Isles</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Airdrie-lovers-leap.jpg' class="w3-card" title='Do not underestimate the ghostly lepus.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Do not underestimate the ghostly lepus.</small></span>                      <p><h4><span class="w3-border-bottom">White Hare</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Airdrie (Lanarkshire) - Area which was once known as Lover's Leap, on the road towards Faskine<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The manifestation of a white hare would warn the Kerr family of impending doom. The site known as Lover's Leap marked the spot where a Kerr woman was found dead (along with her lover) shortly after this magic white hare was spotted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Curse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alloa (Clackmannanshire) - Alloa Tower<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 1500-1800<br>
              <span class="w3-border-bottom">Further Comments:</span> One long, detailed curse resulted in the Erskines' losing their home (amongst other personal disasters), and now the castle remains empty.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Bottles</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alnwick (Northumberland) - Ye Olde Cross public house<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> A collection of bottles on display in the pub are reportedly cursed - the last person to touch them died soon after, and now they are covered up to avoid further mishaps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Family Curse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anwoth (Dumfries and Galloway) - Cardoness Castle<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A curse placed on the castle ensured its ultimate ruin - three owners went bankrupt, and a fourth owner and his family were drowned in the nearby loch when the ice cracked whilst they skated in winter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Granite Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aviemore (Highland) - Rothiemurchus churchyard<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> A legend says that five cylinder shaped stones mark the spot where the last of the Comyns family is buried. The stones vanish and reappear depending on how the House of Rothiemurchus is fairing, but if moved or taken by a human hand, that person is doomed to die. One story says the stones where stolen by a group of people who were later found dead in a car, the stones discovered standing upright nearby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Curse of Ballinacourty</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballinacourty (County Waterford) - Ballinacourty house (no longer standing), and general area<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A mother whose son was hanged by a local sheriff for being 'lazy' cursed all who lived in the area to endure the heartbreak she felt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">No Ploughing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballylusk (County Laois) - Old burial ground<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A farmer who tried to plough this sacred land died after falling and banging his head. The two horses drawing the plough died the following day.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/laidly-worm.jpg' class="w3-card" title='The Laidly Worm, from the 1895 book English Folk and Fairy Tales by Joseph Jacobs.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Laidly Worm, from the 1895 book English Folk and Fairy Tales by Joseph Jacobs.</small></span>                      <p><h4><span class="w3-border-bottom">The Laidly Worm</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bamburgh (Northumberland) - Spindlestone Heugh (aka  Spindlestone Crag)<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Sixth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This area was, for a brief time, home to a large slimy serpent - it turned out to be the daughter of the king, transformed by a curse from her evil stepmother. The king's son discovered the truth, saved his sister, and turned his mother into a toad. Another variant of the story says a dragon held the king's daughter hostage until she was saved by a knight who came from overseas.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Animal Sickness</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bantry (County Cork) - Farm near old Danish fort<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre 1938<br>
              <span class="w3-border-bottom">Further Comments:</span> A local story claimed that any disturbance to the fort would result in livestock death, although the farmer's wife disbelieved the tales and dug out an old well on the site. Cows soon fell ill, and calves died. The warning heeded, the woman filled in the well, and as the last stone put in place, a dreadful, disembodied laugh could be heard. The sick cows recovered.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Druid's Curse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bingley (Yorkshire) - Ryshworth Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> A sixty centimetre stone head, thought to be Celtic, was deemed to be cursed after several people who encountered it became bankrupt, ill or died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gypsy Curse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham (West Midlands) - St Andrews, Birmingham City Football Club<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend says that a community of gypsies were evicted from the land many years ago, enabling the construction of the stadium. St Andrews is one of several football grounds around the country that claims this gypsy curse is responsible for any bad luck they encounter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Giant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackgang Chime (Isle of Wight) - Cave in the area (no longer present?)<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The chime was once a lush area, rich in vegetation and the hunting ground of a giant who feasted upon children. A holy man cursed the giant and the area, ensuring nothing would ever grow there again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackpool (Lancashire) - Ripley's Odditorium<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> When the skull of a local girl was displayed here, several people reported seeing a spectral female figure standing by it. The skull's owner was convinced the item was cursed and died a week after finally giving it away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boat of Garten (Highland) - River Spey<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> When the waters are low, an inscribed stone can be seen in the river - it is believed cursed, and anyone who meddles with it is doomed. Adding to the treachery of the waters, a white horse is said to patrol the river, looking for people to drown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smuggler Curse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bognor Regis (Sussex) - The Old Vicarage, Sudley Road<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A curse was placed on this building after the vicar attempted to stop local smuggling activities. The building no longer stands, but the area (which now has shops built on top) is still said to be unlucky and haunted by the sounds of footsteps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Thomas Boleyn Drives Headless Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Breckles (Norfolk) - Breckles Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The father of Anne Boleyn is said to be cursed with driving his coach over forty (or eleven) Norfolk bridges. One of the passing points is Breckles Hall, where the spectre once scared a poacher to death (another version states that the poacher was taken by the Devil because of his constant law breaking). Locals say that anyone who views the coach is dragged down to hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cedar of Lebanon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bretby (Derbyshire) - Tree on the grounds of Bretby Hospital (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Tree felled during 1950s<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend said that Lady Margaret had thrown herself off a tower here, her blood spilling on the tree which resulted in it being cursed. If a branch fell from the tree, a member of the family residing at the hall was said to die soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Cranium</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brougham (Cumbria) - Brougham Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The cursed skull created havoc whenever removed from the property, so a solution-focused individual finally walled it up in a room of the hall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Skull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Browsholme (Lancashire) - Browsholme Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 1850s<br>
              <span class="w3-border-bottom">Further Comments:</span> When the skull was last removed from the hall (as a joke), it was blamed for spontaneous fires and family deaths. The skull was quickly returned and has not left since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf912bnds.jpg' class="w3-card" title='St Mary&#039;s Churchyard, Bury St Edmunds.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> St Mary&#039;s Churchyard, Bury St Edmunds.</small></span>                      <p><h4><span class="w3-border-bottom">Maude Carew</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bury St Edmunds (Suffolk) - St Mary's Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 February, 23:00h (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Cursed by a monk after Maude murdered the Duke of Gloucester in 1447, her shade is now said to appear once a year. The alleged ghost often brings dozens of people to the area in the hope of seeing her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Petrified Sack of Corn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caistor (Lincolnshire) - Fonaby Top<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The large rock on top of the hill was once a sack of corn owned by a selfish farmer. The farmer refused to give a handful of grain to a passing holy man, who turned the sack into stone as punishment. A curse is said to fall on anyone who tries to move the stone - this was experienced by Gentleman John Walls, who used four shire horses to move the rock to his front door at Corn Sack Farm after taking a liking to it. Misfortune soon befell him, and he moved it back soon after (only requiring one horse for the return trip).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Open Window</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge (Cambridgeshire) - Eagle public house<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Window still present, poltergeist late 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> One story says a window in the upper part of the pub is cursed; if it is ever closed then ill fortune follows. Like so many other pubs, the site was also once home to a poltergeist, and is also said to be haunted by the ghosts of two Second World War airmen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unlucky Art</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carlisle (Cumbria) - Cursing Stone, Carlisle Museum<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 2001 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Since the installation of artist Gordon Young's sculpted granite 'Cursing Stone' inscribed with a 16th century curse in one of Carlisle's museums in 2001, misfortune has plagued the city - local farm stock were wiped out by foot-and-mouth disease, a devastating flood occurred, several factories have closed, a boy killed in a local bakery and Carlisle United football team dropped a league.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Don't Talk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caterham (Surrey) - Tree on High Street<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Tree still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says a witch was executed on this tree, cursing it while dying. Something untoward is supposed to happen to the loved ones of those who talk beneath the branches, while highly superstitious folk hold their breath while scampering past.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Toppling Tower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chelmondiston (Suffolk) - St. Andrew's church<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies (see below)<br>
              <span class="w3-border-bottom">Further Comments:</span> St. Andrew's church tower was cursed by a local witch. First it burned down, and when rebuild was struck by lightning and burned down again. To prevent this from happening a third time, a large square tower was built, only to be hit by a bomb during the Second World War.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 104</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=104"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=4&totalRows_paradata=104"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
