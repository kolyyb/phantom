

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Wales, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/wales.html">Wales</a> > West Glamorgan</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>West Glamorgan Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Baglan-Bwlch-area.jpg' class="w3-card" title='A ghostly shadow walks past a building.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly shadow walks past a building.</small></span>                      <p><h4><span class="w3-border-bottom">Shadowy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baglan - Bwlch area and Tylefedwen Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghost would move across the area before finally stopping at Tylefedwen Farm where it would point to a mine's air shaft before vanishing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Beast of Baglan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baglan - General area and surrounding countryside<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Known about by locals for many years, stories of the big cat were not taken seriously until 2005 when an off-duty police officer spotted the creature along Ladies Walk. Hair and print analysis showed the cat to be a puma.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Loud Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishopston - Old Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A white lady that manifested at this location would be accompanied by loud crashing sounds. Poltergeist activity was also reported. Twelve clerics finally exorcised the spirit.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown Aside</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cefn Bryn - Arthur's Stone (Neolithic burial ground)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This stone is said to have been found by King Arthur in his shoe as he passed through Llanelli en route to Camlann. He tossed the stone to one side and it landed here, some five miles distant.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knight in Shining Armour</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cefyn Bryn - Arthur's Stone (aka Arthur's Quoit?)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nights of a full moon<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing gleaming armour, this figure emerges from under the stone and walks off towards Llandrhidian.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Water Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glyn Neath - Area where rivers meet<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A traveller once climbed on the back of a white stallion in this area, which flew him many miles before fading into mist and dissipating.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glyncorrwg - Bakery, village church and surrounding area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1933 (Bakery) and 1900s (general area)<br>
              <span class="w3-border-bottom">Further Comments:</span> A baker quit his job after encountering an elderly lady dressed in black. The woman drifted into the bakery and stared at the baker before vanishing. At the time of the 1933 sighting, older members of the community recalled that the woman in black had been seen thirty years previous around the church and within the village itself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ysbryd</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glyncorrwg - Colliery south of village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1850s<br>
              <span class="w3-border-bottom">Further Comments:</span> This mine was thought to be home to an ysbryd (Welsh for ghost), which some miners claimed to have seen, and others heard. A couple of jokers later placed a donkey in one tunnel, the creature's braying mistaken by other workers for the cries of the ysbryd.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lamentation</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glyncorrwg - Cwmcos Glyncorrwg Colliery<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> July 1902<br>
              <span class="w3-border-bottom">Further Comments:</span> Miners stopped work after hearing the lamentations of women and children emerge from a nearby disused colliery, believing the wails to be a warning of impending disaster. Strange lights were also seen around the mine.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shaking Beds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gorseinon - Gorseinon Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Several strange events have been reported over the past few years, including empty beds which shake violently and the sighting of a shadowy nurse in an old style uniform.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Magic Well</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gorslas - Llyn Llech Owen<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This lake now covers the location of a magic well which would never run dry on the proviso that a rock would be placed over the opening after use. One man forgot to do so, and the water flooded the local area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Teddy & Beehive Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gower - Beach at Whitford Bay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 17 September 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman with her family which had a long stretch of beach to themselves suddenly found a middle aged couple had set up deckchairs and windbreaks around twelve metres away. The couple, one with a Brylcreem teddy boy hair style, the other with a beehive, were listening to 1950s/1960s music. The woman was intrigued as to how the couple had managed to set up everything without her noticing, and even more surprised when she turned to talk to her daughter and, turning back, found they had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Captured Jack</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gower - General area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1880s<br>
              <span class="w3-border-bottom">Further Comments:</span> A figure resembling Spring Heeled Jack took to haunting this area, leaping over hedges and fences on moonlit nights, his white coat flapping behind him. This Jack's reign ended when cornered by a farmer - the farmer promised not to reveal Jack's identity and the sightings ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sea Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gower Peninsula - Waters of Fall Bay<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950<br>
              <span class="w3-border-bottom">Further Comments:</span> This creature possessed a long neck (the only part of the beast above the water line), down which a lengthy mane ran. The total length of the monster was estimated to be nine metres.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Koala</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangennith - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Reports of koala bears came in during the 1990s, seen on the roads in this area. No physical evidence has been found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangennith - Main road running through village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century? 06:30h.<br>
              <span class="w3-border-bottom">Further Comments:</span> A motorist reported almost running over (or through) a white lady that stepped out on the road in front of him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Llangyfelach-church.jpg' class="w3-card" title='Satan steals a church.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Satan steals a church.</small></span>                      <p><h4><span class="w3-border-bottom">Stolen by the Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangyfelach - Church with separate tower<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil stole the tower of the church but Saint Cyfelach caught him and forced Old Nick to drop it a short distance away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thirsty Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llanrhidian - Arthur's Stone (cromlech)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Dark stormy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> The stones here are said to animate when the weather is right and there are no human witnesses, to head off and drink from the nearby waters. A ghostly knight in glowing armour also haunts the area; drifting out from under the rocks and heading north on nights of a full moon.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Henry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llanrhidian - Welcome to Town Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The spook known only as Henry has been observed sitting in the pub dressed in Regency clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">'White Hyena'</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Margam - Lakeside Golf Range<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> December 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> After receiving reports of a large cat, police officers called to the golf range observed a strange creature, a third bigger than an Alsatian and white in colour. One witness said the front legs were bigger than the hind legs and the creature had a long tail which reached the ground. However, an animal warden would soon go on to recover the animal, which proved to be a German Shepherd/Husky cross, slightly different to the given description.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert Scott</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Margam - Margam Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Former gamekeeper Scott is thought to have been murdered by a poacher and has seen been observed on the staircase in the hall. Disembodied laughter has also been reported, as have the fleeting glimpses of Victorian children. The nearby ruins of Margam abbey are haunted by a monk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Margam - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s-1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> The entity here could be heard walking across the ceiling and around one of the children's beds. There would also be sudden temperature drops and a heavy weight on one of the beds which caused the mattress to sink.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Margam-Ruined-Abbey.jpg' class="w3-card" title='A ghostly figure stands in the ruins.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly figure stands in the ruins.</small></span>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Margam - Ruined Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This otherworldly man of the cloth has been reported around the ruins of the Cistercian abbey.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Margam - Ruined Chapel, Capel Fair, Margam Country Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A father and daughter walking through the area entered the ruin to see a young woman sitting down in a corner. She looked startled and disturbed to see the two visitors. Because of her scared demeanour, father and daughter simultaneously backed out. Moments later, out of curiosity, they re-entered, but the woman had vanished. The daughter ran around the whole building but could see or hear nothing. The lady had disappeared in an instant.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Merthyr Tydfil - Aberdare-Merthyr Rail Tunnel (closed, no public access)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Since closing in 1962, local stories have sprung up saying the site is haunted by a white lady and/or the occasional ghost train.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 56</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=56"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=56"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/wales.html">Return to Welsh Preserved Counties</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Scotland</h5>
   <a href="/regions/scotland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/secondlevel/Lothian.jpg"                   alt="View Scotland records" style="width:100%" title="View Scotland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
