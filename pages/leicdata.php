

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Leicestershire, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastmidlands.html">East Midlands</a> > Leicestershire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Leicestershire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man with No Face</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anstey - Fields along Anstey Road, leading to Leicester<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be an early alien encounter (though it can also be interpreted as ghostly in nature), a woman playing in the fields came across a figure without a face dressed in black. Behind him, she could see a circular 'hut'. By the time she awoke her father, who was sleeping in a nearby field, both hut and man had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/0503bed.gif' class="w3-card" title='An old woodcut of a demonic figure standing over a bed.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a demonic figure standing over a bed.</small></span>                      <p><h4><span class="w3-border-bottom">Bedclothes Tugger</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Asfordby - Asfordby Rectory<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre 1913<br>
              <span class="w3-border-bottom">Further Comments:</span> The poltergeist at work here developed a habit of pulling people's bedcovers off them as they slept at night, including those belonging to the visiting Vicar of Whetstone The resident reverend F A Gage Hall exorcised the noisy spirit in 1913.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pooka</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashby de la Zouch - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness who awoke in the middle of the night encountered what she believed to be a fairy entity known as a Pooka, sleeping over the pelmet of a landing window. The creature resembled a little piglet, but with a longer, pointy nose and no visible tail. The part of the house where the sighting happened is on the path of a ley line.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dark Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashby de la Zouch - Queen's Head Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Several staff at the hotel have reported seeing a strange tall man dressed in dark clothing. He is said to emanate an evil presence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smoky Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashby de la Zouch - Royal Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A guest at this hotel felt his hair stand on end just prior to encountering an opaque smoky female form which stood in front of the bathroom door.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coachman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashby de la Zouch - Social Club<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Investigated by Spirit Team UK, this social club is said to be the haunt of many phantoms, including a coachman, a Tutor lady, and children who died of the plague.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashby de la Zouch - White Hart public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Once used to hold prisoners due to be executed at the nearby castle, the White Hart was investigated by the Spirit Team UK who detected phantom children and an elderly lady.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Upper Torso</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aston Flamville - Grass next to St Peter's Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by a witness in the passenger seat of a passing car, this male phantom only materialised from the torso upwards.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barrow-upon-Soar - High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> A large white or ginger cat ran out in front of a car before the driver had a chance to react and disappeared under the vehicle. Both driver and passenger felt a bump and, stopping the car, leap out to check on the animal. However, they could find no blood, fur, or any other evidence they hit something either on, under or near the car. Reflecting on the incident, one of the witnesses thought the creature was very large, seemed too visible under the lighting conditions, and seemed to suddenly appear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Glowing Mouthed Hounds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birstall - General Area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The locals here say their devil dogs, known as Shag Dogs, have a glow that emanates from the inside of their mouths.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Rings</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birstall - Mill Hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This area was widely believed to be the haunt of fairy folk, where they would come and dance in the pale moonlight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Barrel Mover</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birstall - Plough Inn<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity was renowned for moving barrels around the basement.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Flying Kit</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bitteswell - Asda distribution warehouse, Magna Park (site of former airbase)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> The warehouse was constructed at the end of the runway at Bitteswell. It is reported that in the toilets is a permanent stain on the floor in the shape of a man, and an airman in flying kit has been repeatedly seen in there.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman Seeking Husband</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaby - Blaby Ford<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 1995, around 20:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people driving to a party stopped at the forward to smoke. While sitting in the car they spotted the outline of a person wearing a long coat walking near a steep bank. They turned on the main lights of the car, but the figure had disappeared. They continued to the party, and towards the end of the evening people started to discuss local ghost stories, where one people told the story of the phantom woman which haunts Blaby Ford, looking for her husband who drowned in 1923.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Death by Witchcraft</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bottesford - Church<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Tomb still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Likely to be the only inscription on a tomb that blames death by 'wicked practice and sorcerye', the two children's graves marked as such were sons of the local Lord of Rutland. As a side note, a late nineteenth century piece of folklore says that if a ghost has not left a grave within 25 years after their remains have been buried, it may never do so.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shoe Filler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bottesford - Private residence, Devon Lane<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1979 - Late 1981<br>
              <span class="w3-border-bottom">Further Comments:</span> During the renovation of this property, a poltergeist would fill any shoes left in a locked porch with stones. Items would also disappear and reappear in strange places. A woman who climbed the banister-less staircase almost toppled over the edge, but said she was saved by what felt like a small hand which pushed her in the opposite direction. The day the house went on the market, the activity dropped to next to nothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Manchester</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bottesford - Second World War Airfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> It is thought that the sound of droning sometimes reported over the former airbase is that of a Manchester bomber - a high loss ratio ensured that these aircraft were later replaced with Lancasters. A phantom light source has also been reported moving in the tower, and a pilot in full kit was seen once.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bottesford Beck - Close to bridge over the river<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local folk belief placed a headless woman haunting a bushy piece of land.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/a_ladyjgrey.jpg' class="w3-card" title='A painting of Lady Jane Grey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A painting of Lady Jane Grey.</small></span>                      <p><h4><span class="w3-border-bottom">Lady Jane Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradgate - Bradgate Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Around Christmas (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Raised here as a child, Lady Jane's tormented shade now haunts both the mansion and the grounds surrounding the building. One ghost story says that she arrives in a coach pulled by four black headless horses at the house on Christmas Eve.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Scythe Carrier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Breedon on the Hill - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Strange mists and a figure carrying a scythe are said to have been reported at this site, investigated by the Spirit Team UK. Folklore says that the church was to be built at the base of the hill, but doves (or angels) moved the foundations at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hobbes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Breedon on the Hill - Hill in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Hobbes lived in a secret cavern under the hill and would travel to a tavern after dark to churn the milk. The entity left the area after his coarse woollen apron was replaced with a cotton one.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Music</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Breedon on the Hill - Near the church<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 October 2012, approximately 19:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple sitting close to the church heard loud music, sounding like an ice cream van tune, although nothing was in sight. When the music started a second time, they left the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/coachpix.jpg' class="w3-card" title='An old fashioned coach sits in shadow.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old fashioned coach sits in shadow.</small></span>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brooksby - Brooksby Agriculture College<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of a coach being pulled by horse can be heard in the hall here - the building was moved and rebuilt in the nineteenth century, with the hall being placed where a road once lay. A phantom woman also reportedly roams the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stanhope</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broughton Astley - A5<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Foggy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A member of the Stanhope family is reputed to haunt this road when the fog rolls in.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Hooded Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broughton Astley - B581, Broughton Way<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 11 August 2014, 22:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people watched as a grey hooded figure walked out in front of their car, forcing them to brake sharply. The figure crossed the road and dissipated near a gate. Both witnesses were left shaken.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 151</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=151"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=151"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastmidlands.html">Return to East Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="ViewWest Midlands records" style="width:100%" title="View West Midlands records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
