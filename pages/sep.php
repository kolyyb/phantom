
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in September">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > September</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>September (and Autumn) - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brampton Bryan (Hereford & Worcester) - Park<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says that on the day Cromwell died, the park was laid to waste as the Devil passed though it to collect the soul due to him. Now, once a year, Old Nick rides out and returns to the park to ravage it once more.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Archdeacon Bulkeley?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin (County Dublin) - Area around Old Bawn Road and the Dodder<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 08 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to either be driven by or containing Archdeacon Bulkeley, a coach drawn by six headless horses containing two passengers together with two footmen, drives through this area of Dublin. Anyone who spots the coach is said to die within 366 days.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John and Amy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Tasburgh (Norfolk) - Rainthorpe (or Rainthrorpe) Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 08 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Early evening of 8 September, a ghostly pair meet in the gardens of the hall, named by some as Amy Robsart and either John Appleyard (her half-brother) or Robert Dudley (her husband). The date of the manifestation is the day Amy died after falling down the staircase at Cumnor Place.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Janet Dalrymple</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bladnoch (Dumfries and Galloway) - Baldoon Castle ruins<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> On her wedding night, Janet went insane; she stabbed her husband several times and had to be dragged away by servants. Janet's husband survived, though she died shortly after - the date on which her shade returns to the scene of her outburst.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Women</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Selkirk (Borders) - Newark Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The castle is said to contain the souls of the women and children murdered by brutal soldiers at the site, who are heard once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-4.jpg' class="w3-card" title='An old postcard of Berkeley Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Berkeley Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Edward II Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berkeley (Gloucestershire) - Berkeley Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered on orders of his wife Queen Isabella (her fate is documented elsewhere in this database), the anniversary of the King's death is marked by his screams echoing around the castle. The skin of an animal was once on display near the entrance; a local legend said it belonged to a giant toad discovered in the dungeon, although it was more likely to belong to a seal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Walter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sherborne (Dorset) - Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 28 September, St Michael's Eve (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir Walter Raleigh is said to appear here once a year, near a tree that was named after him. Other ghosts claimed to haunt the castle include horses and a ghostly child. At one time, it was thought the castle was cursed, as ill fortune befell several of the owners.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mad Jack</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atcham (Shropshire) - Mytton and Mermaid Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> John 'Mad Jack' Mytton was a local eccentric who lived with 2000 dogs and 60 cats while drinking six bottles of port per day. His money eventually ran out and he died in a debtors' prison. However, once a year on his birthday, his ghost is reputed to visit the hotel that has now taken his name.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Female in Old Attire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Macclesfield (Cheshire) - Gawsworth Hall and general area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A female phantom is reported to drift around the courtyard of Gawsworth Hall, patrolling nearby lanes, and in the churchyard. Some have named her as Mary Fitton, a lady-in-waiting to Queen Elizabeth I.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pirates</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dalgety Bay (Fife) - St Bridget's Kirk<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Phantom pirates are supposed to haunt this ruined area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drifting Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fawkham Green (Kent) - Pennis Lane<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing at dusk, the figure of a nun has been reported floating down this lane. Her skull is housed at Pennis Farm and it is said if the object is ever removed a great curse will befall the valley.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sarah Preston</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boston (Lincolnshire) - Boston Stump (tower of St Botolph's parish church)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> In one version of the ghost story, Sarah is seen to leap from the tower of the church holding a young child, vanishing just before hitting the ground. Another variation has Sarah dying of the plague - she was blamed for bringing the black death in the town by her adulterous ways, and her cries of 'pestilence!' can still be heard as she runs towards the church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Poacher with Bag</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wadhurst (Sussex) - Beggars Bush, Bestbeech Hill Crossroads area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn evenings (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This thieving ghost in a tan coloured coat is seen struggling along the road, his brown sack bulging with stolen food.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach Arriving</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dartmouth (Devon) - The Royal Castle Hotel, The Quay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn months, 02:00h (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Early morning, during the time of golden leaves, the sound of a horse and coach can be heard coming to a stop outside the hotel. There are sounds of someone jumping on board before the hooves can be heard pulling away. One of the bedrooms may be haunted by a robed figure, and the sounds of two people fighting can be heard in the courtyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fighting and Singing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nether Stowey (Somerset) - Dowsborough (aka Danesborough or Dawesbury) Camp<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn nights (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of a young child singing have been reported here in the past, as have the sounds of soldiers crooning and chanting before starting to fight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf9298a.jpg' class="w3-card" title='Paths in Dunwich, Suffolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Paths in Dunwich, Suffolk.</small></span>                      <p><h4><span class="w3-border-bottom">Hobby Lanterns</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dunwich (Suffolk) - Paths around Dunwich<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1924, said to be most active between 29 September and 24 December<br>
              <span class="w3-border-bottom">Further Comments:</span> Will-o-the-Wisps, known locally as Hobby Lanterns, were said to lure people off the paths at night after extinguishing the victim's own light source.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Port-Soderick-Off-coast.jpg' class="w3-card" title='Petrified people stand on a vanishing island.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Petrified people stand on a vanishing island.</small></span>                      <p><h4><span class="w3-border-bottom">Banished Island</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Port Soderick (Isle of Man) - Off the coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Reappears every seven years, last seen late September, year unknown.<br>
              <span class="w3-border-bottom">Further Comments:</span> The occupants of an island insulted the wizard Fin MacCool. As punishment he turned the people into granite and sunk the island, permitting it to return once every seven years. The curse could be broken if a bible were placed on the island during a period of its return. The last time the island was spotted, the witness ran home, grabbed her bible and returned to the site, but was too slow - the island had already sunk back to the depths. The woman who tried to save the island died soon after; superstitious locals blamed her death on MacCool's curse, and this supposedly has prevented anyone from trying to save the island and its occupants again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Princess Alice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE2 (Greater London) - Thames River, Thamesmead<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> When a collier rammed a pleasure boat (the Princess Alice) here in 1878, over 600 people died - many of them being poisoned by the outflow of London's sewage which emerged from a pipe by the accident site. Their pitiful cries for help can sometimes be heard around the time of the accident.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Pembroke (Dyfed) - Manorbier Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This feminine figure dressed in black seems to appear near the gatehouse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grenadier Climbing Stairs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 (Greater London) - The Grenadier public house, Wilton Row<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This young trooper was killed after it was discovered he cheated in a card game in the public house - the grenadier only appears in the month of September, as he walks around the building, looking into rooms and pacing along corridors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Minster (Kent) - Old Oake Cottage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This strange looking figure of a monk always appears as if silhouetted behind an intense light. He is always heard before manifesting, shuffling feet marking his appearance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Peg Sneddle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crackenthorpe (Cumbria) - General area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Peg Sneddle, or Elizabeth Sleddall as she was known in life, would manifest prior to a death in the Machell family (she was married to a Machell, but used her maiden name after his premature death). This ceased when her body was hidden under a large rock in the nearby river, though it would reappear every year in September. A phantom coach and four also haunts the same area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor531bter.jpg' class="w3-card" title='Breydon Water, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Breydon Water, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">Fighting Galleons</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Great Yarmouth (Norfolk) - Breydon Water<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: 11 July, 14 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The first set of ships to haunt this area is said to be a fleet of galleons heading towards Burgh Castle. The second is a battle between a pirate ship and two smaller vessels.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Party with Ugly Young Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Madingley (Cambridgeshire) - Madingley Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: 24 December Lady Ursula seen. Party reported in September 1963<br>
              <span class="w3-border-bottom">Further Comments:</span> One witness claimed to have heard a ghostly party, followed by the sighting of a stuffy young man, whose face resembled that of a skeleton. Others have also reported similar occurrences, describing the man as having a decaying green face. Lady Ursula Hynde also returns to the hall once a year, upset at her son's actions - he pulled down a church, and used the wood to finish construction of Madingley Hall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bishop of Brundall on Barge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brundall (Norfolk) - River<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: 24 June & 18 September (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Sailing up the river, the Bishop's barge contains 28 rowers and is covered with fine cloth and silk. The Bishop is said to bless any ill people that he sails by.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 28</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=28"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=28"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
