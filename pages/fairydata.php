



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Fairy Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Fairies & the Little People</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Sightings of Fairies & Their Kin</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Aberfoyle-Fairy-Hill.jpg' class="w3-card" title='Fairies dance in the woods.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Fairies dance in the woods.</small></span>                      <p><h4><span class="w3-border-bottom">Kidnapping</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberfoyle (Stirling) - Fairy Hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 1692<br>
              <span class="w3-border-bottom">Further Comments:</span> The Rev Robert Kirk is said to have been kidnapped by fairies here. He appeared in ethereal form a couple of times after the event, begging for help in escaping from the fairy world.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Funeral</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystruth (Gwent) - Church Lane<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A small procession of fairies was reported travelling along the lane towards the church to bury one of their own. Another version of the story says the funeral was ghostly in nature, and when a living man reached out to help carry the coffin, everything vanished, with the man only left holding a horse's skull.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coffin Makers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aghacashlaun (County Leitrim) - Corargeen (fort)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The fairies which lived at this fort were said to have borrowed a man's hatchet to make a coffin for a neighbour's child two weeks prior to his death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Alderwasley-Hill.jpg' class="w3-card" title='Dwarves, while not common in the UK, pop up occasionally.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Dwarves, while not common in the UK, pop up occasionally.</small></span>                      <p><h4><span class="w3-border-bottom">Dwarf with Pointed Hat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alderwasley (Derbyshire) - Hill just past Alderwasley<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The witness to this encounter stated that they once bumped into a four foot high figure wearing a green pointed hat feeding the plants.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Folk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston (Sussex) - Burlough (or Burlow) Castle<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Fairies once made their home here and rewarded a local man with food after he helped one of their kind repair a kitchen utensil.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dancing within Limits</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alnwick (Northumberland) - Fairy ring at Chathill Farm (ring may no longer exist)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Children would dance around a fairy circle here, although it was said if they circled the site more than nine times, fairies would spirit the offending child away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Hill</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alva (Clackmannanshire) - Alva Glen<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Famed for its fairies, these pesky fellows kidnapped a local baker's wife - she was only returned after her husband broke the fairy spell by accidentally standing on one leg.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Secret Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Andreas (Isle of Man) - Shan Cashtal barrow<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The fairies here once travelled to and from the barrow using underground passageways leading to a nearby churchyard (at Maughold).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Annandale-Burnwark-Hill.jpg' class="w3-card" title='Sinister fairies at dusk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Sinister fairies at dusk.</small></span>                      <p><h4><span class="w3-border-bottom">Evil Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annandale (Dumfries and Galloway) - Burnwark Hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A palace concealed within this hill was run by an army of evil fairies - they would kidnap anyone foolish enough to venture close to their land.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Appletreewick-Trollers-Gill.jpg' class="w3-card" title='Trolls or just pareidolia?'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Trolls or just pareidolia?</small></span>                      <p><h4><span class="w3-border-bottom">Trolls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appletreewick (Yorkshire) - Trollers Gill (or Ghyll)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Living beneath the ravine, the trolls are said to emerge at night and track down their prey - humans. At least one farmer was lured to his death by their ability to trick people into straying from the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Short Men in Green</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aran Island (County Galway) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 1992<br>
              <span class="w3-border-bottom">Further Comments:</span> A fifteen year old boy spotted two figures sea fishing, each just over a metre tall, dressed in green and wearing brown shoes. The figures chattered to each other in Irish before jumping up and vanishing. One of the figures left a small pipe behind which the witness took, although it later disappeared when 'safely' locked away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Leprechaun's Shoe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardrah (County Cork) - Field in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A small shoe said to belong to a leprechaun was found near the village by a cow herder. Unfortunately, the man forgot to take the shoe home, and when he went to retrieve it the following day, the shoe had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pooka</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashby de la Zouch (Leicestershire) - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness who awoke in the middle of the night encountered what she believed to be a fairy entity known as a Pooka, sleeping over the pelmet of a landing window. The creature resembled a little piglet, but with a longer, pointy nose and no visible tail. The part of the house where the sighting happened is on the path of a ley line.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Athgreany-stones.jpg' class="w3-card" title='A fairy playing the bagpipes, surrounded by glowing lights.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A fairy playing the bagpipes, surrounded by glowing lights.</small></span>                      <p><h4><span class="w3-border-bottom">Bagpipes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Athgreany (County Wicklow) - Stone circle, aka Piper's Stones<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Played by the fairy folk, the sound of bagpipes could once be heard emerging from the stones. A tree in the centre of the stone circle is considered to belong to the fairies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robin's Well</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aynho (Northamptonshire) - Spring known as Puck Well<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century, current status unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Robin Goodfellow and his friends were thought to frequent this spring.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Cow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balephuil, Tiree (Argyll and Bute) - Farm in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Dugald Campbell noticed a strange little red cow being attacked by the rest of his herd. The red cow ran off with the rest of the herd giving chase. Campbell followed and watched as the red cow entered a solid piece of rock, closely followed by one of his cows. The rest of the herd, intercepted by Campbell, were prevented from vanishing into the rock.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phynnodderee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballasalla (Isle of Man) - Rushen Abbey<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This fairy creature would be observed digging around the ruins, although when the area was examined, there was never any evidence of soil disturbance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Home</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballevulin, Tiree (Argyll and Bute) - Large sandbank (no longer exists)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A large sandbank was once thought to be home to fairies that borrowed the local blacksmith's cauldron every night, returning it in the morning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sprites & Pixies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballona (Isle of Man) - Ballona Bridge<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> On this day, the fairy population comes out in force. If they are not acknowledged by anyone crossing the bridge, the fairy folk have no hesitation in making their presence felt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bankruptcy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyconnell (County Cavan) - Aughrim Wedge Tomb<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> November 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> According to the Belfast Telegraph, former billionaire Sean Quinn moved the Aughrim Wedge Tomb in 1992, which was home to fairy folk. These little entities cursed the man responsible for the relocation - Sean was at one point worth just under five billion euros, and as of November 2011, he only had eleven thousand euros in the bank.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bagpipers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyedmond (County Cork) - Fort Field<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The fort in this field was home to four fairy bagpipers dressed in red.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tiny Footballers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyroan (County Laois) - Marshy field west of village<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A worker returning home took a shortcut across a marshy field and watched several tiny men playing football. The worker was told to kick the ball, but he was unable to touch it. When he finally did kick the ball, the worker was knocked to the ground. When he recovered, the tiny footballers had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ballyvaughan.jpg' class="w3-card" title='A strange looking black goat.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A strange looking black goat.</small></span>                      <p><h4><span class="w3-border-bottom">Black Goat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyvaughan (County Clare) - Poulaphuca, area around dolmen<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This goat, though by some to be a fairy being, resides over the tombs in the area. The creature causes anyone who threatens the land to develop a hunchback.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Free Money</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bamburgh (Northumberland) - Rocky outcrop on which castle is built<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Lucky people are said to be able to find money which has been left by the fairies, but if they failed to add a coin of their own to the hoard they had found, everything would slip away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/1Cader-Idris.jpg' class="w3-card" title='An old sketch of Cader Idris.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old sketch of Cader Idris.</small></span>                      <p><h4><span class="w3-border-bottom">Idris</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmouth (Gwynedd) - Cader Idris (aka Caldair Idris)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Idris was said to be a wise giant, who may have had links with King Arthur. People would avoid the area at night, convinced that fairies would drive them mad. Another legend says that the site was the hunting ground of Gwyn ap Nudd (Ruler of the Otherworld) who searched the area with a pack of Cwn Annwn, looking for souls to take back home. Finally. Strange lights are said to appear around the summit for the first few days of a new year.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 395</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=395"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=15&totalRows_paradata=395"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
