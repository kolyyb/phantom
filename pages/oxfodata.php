
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Oxfordshire Ghosts, Folklore and Strange Places, Collected by The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southeast.html">South East England</a> > Oxfordshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Oxfordshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Adderbury-Area-around-Cobb-House1.jpg' class="w3-card" title='The ghostly figure in his coach.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ghostly figure in his coach.</small></span>                      <p><h4><span class="w3-border-bottom">Cobb's Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Adderbury - Area around Cobb House (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> George Cobb left death bed instructions that four oaks on his land should never be felled - sure enough, soon after his death the alterations were made, and Cobb's ghost travelling in a funeral coach has been seen since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Devil Helps Thrice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Adderbury - Church<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Fourteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This church is one of three in the area (Bloxham and King's Sutton being the other two) that were paid for by three brothers. One of their workers who helped construct this tower never took any pay, never slept and never ate - he vanished as soon as the tower was built. The brothers were convinced that they were helped by Old Nick.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashbury - Ashdown House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Several ghosts are reputed to haunt the house and the grounds. A phantom crying child lurks in the woods, while the stables are home to a groom who hanged himself. A grey lady is said to gaze from a window on the second floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Whittaker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bampton - Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> When the clergy tried to exorcise this shade, they found it so strong the only way to subdue it was to place the ghost in a large barrel of beer. The barrel stayed in the cellar of the house for many years, though its present whereabouts is unknown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sheppard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bampton - Road between town and Clanfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure, dressed in old clothing (although one story says he appeared naked) appears to lonely travellers along the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horace</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bampton - Talbot Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-to-late-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A former member of staff, Horace still turns up on occasion to keep an eye on the newer employees.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">File Flinger</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banbury - Co-op Travel Shop, White Lion Walk<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> An entity here locks the toilet door, creates the smell of toast, and knocks files of documentation from the shelves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dropped Mortar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banbury - Crouch Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Hill still present<br>
              <span class="w3-border-bottom">Further Comments:</span> During the construction of the churches at Bloxham, Adderbury and King's Sutton, the Devil was thought to have lent a hand. During the work he slipped and dropped some of the mortar he was carrying, thus creating Crouch Hill.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Trap Door</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banbury - Private residence along Middleton Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> September 1969 - July 1970<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple living in a Victorian property were subjected to several months of phantom activity. Footsteps were heard in a room where one witness was sitting, a room was ransacked by an invisible entity, and a solid wood trap door to the attic raised itself 45 degrees for several minutes. The house was finally blessed, but the occupiers moved out not wanting to wait to discover whether the blessing was successful.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cavalier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banbury - Reindeer Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Having vanished for many years now, it is reasonably certain this once gruesome entity has now gone forever.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Beast of Banburyshire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banbury - Surrounding area<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> Named as a big cat hotspot, this area of Oxfordshire has had dozens of reports of large felines over the first five years of the twenty-first century.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baynards Green - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed in the seventeenth century, this highwayman refused to leave and continued to ride around the area to the annoyance of locals.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Beckley-Farm-east-of-Village.jpg' class="w3-card" title='The ghost caused a serving girl to faint...'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ghost caused a serving girl to faint...</small></span>                      <p><h4><span class="w3-border-bottom">Tall Dark Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beckley - Farm east of Village<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> April / May 1857<br>
              <span class="w3-border-bottom">Further Comments:</span> This poltergeist caused pieces of the ceiling to collapse in every room, although no damage occurred to the furniture. Half the windows on the site were also smashed by an unseen hand throwing stones and pieces of roof tile. The entity appeared only once, manifesting as a tall, dark man to a serving girl who promptly fainted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Whispers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bicester - British Heart Foundation charity shop<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> Staff at the charity shop reported hearing voices and spotting a strange mist. Chairs would be found moved and lights which had been switched off were found turned back on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blind Algar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Binsey - Holy Well<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Well still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Algar was blinded after being stoned for touching the hand of the woman he loved. He prayed to St Margaret for help, and she give him the well that restored his sight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Petty Officer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Binsey - Perch public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> A Royal Navy petty officer is said to haunt this pub having drowned himself a short distance away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Edwin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blewbury - New Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Edwin Fry, the landlord who ran this public house during the middle of the twentieth century, still returns to look around the place. His hollow footsteps are the only indication of his presence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Devil Helps Thrice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bloxham - Church<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Fourteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This church is one of three in the area (King's Sutton and Adderbury being the other two) that were paid for by three brothers. One of their workers who helped construct this tower never took any pay, never slept and never ate - he vanished as soon as the tower was built. The brothers were convinced that they were helped by Old Nick.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burford - Burford Priory & Old Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Bell at 02:00h (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom monk is only one of several spooky occurrences at this site; another is an unearthly bell that makes itself heard at two every morning, and the dead gamekeeper who patrols the area with his rifle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Cloud</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burford - Road between Burford and Minster Lovell<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This cloud has been seen moving against the wind and causes an extreme state of terror if enveloping any witness.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Laurence Tanfield</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burford - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The evil Sir Laurence Tanfield and his wife, the Lady Tanfield, are said to travel the area in a fiery coach. To observe the coach is said to bring bad luck or death. One story says Lady Tanfield was bound into a bottle and cast into the river, while another variant of the story says both Tanfields are trapped beneath the bottom stone found under the bridge crossing the River Windrush.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Wardrobe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carterton - Carterton Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1937<br>
              <span class="w3-border-bottom">Further Comments:</span> Mrs Barclay advertised for someone to remove a haunted wardrobe in 'The Morning Post' newspaper on 19 August 1937. She had bought the object three years previous, through it was not until the start of 1937 that the wardrobe doors would open and close shut continuously, even breaking string tied around them. A phantom man began to appear around the house, old and bent over, and wearing a deerstalker hat. However, once sold and removed, the manor was never troubled again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burning Witches</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chadlington - Hawk Stone (or Hawkstone)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says that a chain would be passed through the eye hole in the stone, enabling witches to be held and burnt at the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Stallion</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Charlbury - Countryside around village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Roaming riderless, this white (or grey, according to other reports) horse jumps large hedges and runs wild in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bear</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Charlbury - Wychwood area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A bear was said to have taken up residence in the surrounding woodland. Described as dark brown and the size of a sheepdog, the creature's footprints were also discovered, although it is not known whether they were formally identified as belonging to a bear.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 138</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=138"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=138"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southeast.html">Return to South East England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
