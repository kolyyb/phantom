
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Wexford Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Wexford</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Wexford Ghosts, Folklore and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bannow-Bay-abbey.jpg' class="w3-card" title='A procession of chanting monks.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A procession of chanting monks.</small></span>                      <p><h4><span class="w3-border-bottom">Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bannow Bay - Tintern Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Travelling by the light of burning torches, a line of monks can be seen moving towards the abbey, chanting Gregorian hymns.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Empty Coffin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castleboro - Castleboro House<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1856<br>
              <span class="w3-border-bottom">Further Comments:</span> Lady Carew reported seeing a phantom funeral procession on the lawn of the house - two days later she became informed of her husband's death, which had occurred at the moment she had her 'vision'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">William Marshal</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Churchtown - Hook Lighthouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> William Marshal, the builder of this lighthouse, is said to continue to haunt the site. A ghostly hooded monk has also been reported in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Clough-Mills.jpg' class="w3-card" title='A ghostly shadowy figure haunting a road.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly shadowy figure haunting a road.</small></span>                      <p><h4><span class="w3-border-bottom">Archibald Jacob</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clough Mills - Road between Clough Mills and Wilton Castle, Black Stream area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Renowned for his monstrous temper, Captain Jacob died when his horse stumbled and threw him from the saddle, breaking his neck. Jacob's ghost remains in the area, probably still a little angry.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Harry Alcock</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clough Mills - Wilton Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1840 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year, as the sun sets, a phantom carriage moves away from the castle carrying Harry. Folklore says the ghostly sight to have been so common that dozens of people would gather to watch. The building also had the reputation to be haunted by a former magistrate and a one-time actress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Prisoners</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Duncannon - Duncannon Fort<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> Home to several ghost hunts, several occurrences were reported to have occurred, including ghostly tapping, unexplained shadows, cold spots, and phantom prisoners.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bed Mover</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Enniscorthy - Private residence, Court Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> July 1910<br>
              <span class="w3-border-bottom">Further Comments:</span> A lodger by the name of Randell became the centre of focus for a short lived poltergeist at this property. For three weeks his bed covers would be thrown off, his bed moved, and strange sounds and disembodied footsteps plagued his room. Randell moved out three weeks later, having lost around 4 kilograms in weight, and the strange activity ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Enniscorthy - St. Senan's Hospital (closed 2015)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly grey lady spotted around the hospital is considered to be a nurse or matron.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hessian Cavalry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Foulkesmill, Horetown - Battle of Horetown site and Green Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The entire Battle of Horetown is replayed on its anniversary, while a lone Hessian trooper is seen standing by a tree along Green Road where his body is thought to be buried.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Foulkesmill, Horetown - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom woman, dressed in a white dress with pink trim and holding a broom, manifested to a witness and floated through a bedroom. Another witness had a similar encounter in an adjoining room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Partying Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> New Ross - Maudlin's Bridge area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa early nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Local man Davy Hanlon met a troupe of fairies upon this bridge. They whisked him away to a distant wine cellar where Davy drank with the little people. They allowed him to keep a silver goblet and returned him home, although he became bedridden for many months after his encounter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> New Ross - St Mary's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2021<br>
              <span class="w3-border-bottom">Further Comments:</span> A photograph taken of the church at night revealed a blurred female form walking on a path. Although the 'phantom' is likely a real person caught at a slow shutter speed, several mediums stated the woman's spirit to be trapped in the wall outside the church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Serving Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Rathnure - Coolbawn House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom came into being after a lightning strike killed a serving girl standing by the window. Even though the site is now ruins, the girl's ghost is still said to look from the remains.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Battle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Scarawalsh - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of a ghostly battle, complete with screams, horses and cannon fire, have been heard here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Slim Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Slade - Loftus Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom woman in white died of the traditional broken heart after her father drove away her one true love. The entity has been reported exorcised. Another story attached to the site says that the Devil arrived one night to join a game of cards. Old Nick fled after one gambler spotted a cloven foot under the table.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Colman's Ducks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Templeshanbo - Church of Templeshanbo<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A pond which could once be found here contained indestructible ducks. Animals which tried to hunt the ducks would be found dead, and one man who accidently took one of the birds home and put it in a cooking pot found his fires burnt cold until he released the duck.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Feet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wexford - Bride Street<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 07 August 2022, 22:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> A photograph appeared in the local press showed a pair of disembodied feet outside the gates of Bride Street Church. While the image looked spooky, the effect is likely caused by a long shutter speed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mary Anne Wildes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wexford - Cape Bar and Undertakers (aka Cape of Good Hope, aka Mackens)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Photographer Simon Bloom killed his assistant Mary Anne Wildes before trying to take his own life. Simon failed and, at his trial, found guilty of Mary's murder. The crime occurred in the flat above the bar; as such, folklore says that Mary makes an occasional appearance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Wexford-Sea-off-quay.jpg' class="w3-card" title='A ghost ship.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghost ship.</small></span>                      <p><h4><span class="w3-border-bottom">Ship</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wexford - Sea off the quay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly sailing ship is reputed to occasionally put in an appearance just off the coast.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">de la Roche</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wexford - Selskar Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Alexander de la Roche founded this abbey. Once piece of folklore says that Alexander temporarily left his girlfriend for the Crusades, and after a few years she received news that Alexander had died. The woman became a nun. Alas, Alexander's death had been erroneously reported and he returned to marry his former love. Unable to do so, Alexander became a monk, although they are now united in love in ghostly form (possibly). Another piece of folklore says there are secret passageways under the building, and a drummer once tried to investigate them (with his friends listening on the surface); his drumming abruptly ceased during the exploration and he never emerged.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cromwellian Treasure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wexford - St Michael's Cemetery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A Cromwellian soldier stands guard over a hidden hoard of treasure somewhere in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Executed Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wexford - Wexford Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Around seventy protestant prisoners were executed on this bridge in 1798. Although the bridge has been replaced, the new build remains at the same site as the original; the screams of the executed can still be heard.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 22 of 22</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
