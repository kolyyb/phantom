

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts of Wales, Folklore and Strange Welsh Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/wales.html">Wales</a> > Clwyd</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Clwyd Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coloured Women</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergele - Gwrych Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s<br>
              <span class="w3-border-bottom">Further Comments:</span> This castle is haunted by both a white woman and a lady in red, the latter thought to have been killed during a fox hunt, while the former returned after being buried in unconsecrated ground. Another story says that the white woman was the former owner of the castle (Winifred Cochrane, Countess of Dundonald) who returned after her bitter husband, written out of her will, purchased the site and destroyed the contents.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gwennon Gorn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergele - Sea off the coast<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer, 1967 or 1968<br>
              <span class="w3-border-bottom">Further Comments:</span> One legend says a phantom vessel, the boat of Prince Madoc, has been sighted just off the coast, vanishing just as quickly as it materialised. One witness said that she spotted the vessel one summer; a ship containing several men, not dissimilar to Vikings, disappeared moments after being seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Leather</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergele - The Bull Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The man who appears dressed in leather is said to be a resident who died in a motorbike accident. A phantom monk and ghostly woman are also said to haunt the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Babell - Black Lion public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Some people believe that the white lady is a former landlady who died a couple of hundred years ago. The entity was blamed for mild poltergeist pranks, such as turning up the volume on the radio or removing items nailed to walls.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Cape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Betws y Coed - Anna Davies countrywear store<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s-2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> Poltergeist activity was said to have started by in the 1960s, with a ghostly woman wearing a cape manifesting some twenty years later. In 2011, one of the owners claimed that a shoe landed in front of him after being launched from a display stand.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-BetwsyCoed.jpg' class="w3-card" title='An old postcard showing Betws y Coed in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Betws y Coed in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Wybrant Gwiber</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Betws y Coed - General area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A bandit from a nearby village tried to kill the Wybrant Gwiber but failed. The bandit's throat was torn out and his body thrown into the river. What happened to the dragon is not known.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/swallow-falls-wales.jpg' class="w3-card" title='A photograph of the rushing waters of Swallow Falls.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A photograph of the rushing waters of Swallow Falls.</small></span>                      <p><h4><span class="w3-border-bottom">Sir John Wynn of Gwydir</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Betws y Coed - Rhaeadr Ewynnol (aka Swallow Falls) Waterfall<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir John was said to have oppressed anyone who stood in his way. After he died, his spirit was said to have been sent to stand under the waterfall to be punished and purified.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Whispering Shadow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Betws y Coed - Royal Oak Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> A shadow has been seen moving and a voice heard whispering 'where' or 'why'. A visiting medium observed a nun sitting outside one room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach and Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Betws y Coed - Waterloo Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This bridge and the surrounding area are said to be frequented by a phantom horse and coach.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodelwyddan - Bodelwyddan Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Several dead people still appear to be trapped within this building, including a woman dressed in Victorian garb, a soldier (likely to be from the First World War, when the castle doubled as a recuperation hospital), and an unknown shadowy figure who lurks in the corridors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Friendly Spirits</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodelwyddan - Glan Clwyd Hospital<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> The hospital was criticised by a watchdog after a patient recovering from an operation complained that a nurse had run down the corridor claiming to have seen a ghost. The nurse had then explained that she was psychic, and only saw 'friendly spirits'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Sweeping Black Dress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodfari - Dinorben public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Many staff and patrons have reported this spooky woman with long dark hair over the years, which is normally seen moving around the upper part of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Clergyman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broughton - Old Warren - a dead-end road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This black clad preacher man is said to pear into any car parked along the road, unnerving the occupants.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Knight</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bryn yr Ellyon - Barrow (no longer present)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1832<br>
              <span class="w3-border-bottom">Further Comments:</span> This area was once said to be haunted by a large man in golden armour. When the hill was dug up, the same set of armour was discovered beneath, complete with skeleton contained within.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buckholt - Farm off the old Abergavenny road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> One witness claimed to have tried to slap a cow to waken it - her hand passed straight through the creature before it vanished. The farmhouse itself was said to be the home of a poltergeist which would open and close doors and flick light switches.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dark Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caergwrle - Caergwrle Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This female form has been seen slowly moving around the grounds of the old castle; her feet several centimetres clear of the soft earth below.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caergwrle - Packhorse Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A team from the Cheshire Paranormal Society photographed a strange white figure crossing this footbridge late at night during one of their vigils. It is said a total of three spirits haunt the bridge, a young girl and two older women.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Capel-Curig.jpg' class="w3-card" title='An old postcard showing Capel Curig in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Capel Curig in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Travellers' Rest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Capel Curig - Devil's Kitchen<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil sometimes beckons road weary travellers into his kitchen - it is not known what happens to them once inside.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shaking Door</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cerrigydrudion - Brynaber Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> Haunted by a dead servant, events here included doors being violently shaken, chains being rattled, and the family pets being scared by an invisible presence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Transparent Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chirk - Chirk Castle<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> May 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> A photographer experimenting with long shutter speeds with his digital camera caught a semi-transparent female form outside the chapel. Unfortunately, as long shutter speeds can quite easily make people look semi-transparent, the chances of this being any form of apparition is pretty remote.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banging</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chirk - Unidentified council house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1955<br>
              <span class="w3-border-bottom">Further Comments:</span> After the local council acted on neighbour's complaints about the noise level at Mrs Verschuren's house, the lady claimed ghosts were responsible for the bangs. Verschuren and her two brothers said they had seen the phantoms of a nurse, a nun, a blind man with a stick, and a bishop. A local priest blessed the site but the noise continued.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dancing Pixies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clocaenog Forest - General area, & Craig Bron Bannog hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The little people have been seen in this area for many years now, often dancing the night (and day) away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Slim Man in Evening Wear</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Colwyn Bay - Jeff's Discount Store, Abergele Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1973<br>
              <span class="w3-border-bottom">Further Comments:</span> The manager of the building saw a tall, thin man at the top of the stairs dressed in formal evening wear. The ghost was mistaken for a customer, until he disappeared. Footsteps have also been heard in unpopulated areas, and several people have seen items from the shelves float through the air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Projectionist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Colwyn Bay - Princess Cinema (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This cinema was said to have been haunted by a projectionist who died in a flash fire in the booth.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stanley Ravenscroft</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Colwyn Bay - Theatre Colwyn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Ravenscroft, a former manager at the theatre, is held accountable when items are moved by an unseen hand or footsteps are heard in empty parts of the building.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 138</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=138"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=138"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/wales.html">Return to Welsh Preserved Counties</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Scotland</h5>
   <a href="/regions/scotland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/secondlevel/Lothian.jpg"                   alt="View Scotland records" style="width:100%" title="View Scotland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
