
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Sheffield Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northeastandyorks.html">North East and Yorkshire</a> > Sheffield</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Sheffield Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gabriel Hounds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Area around parish church<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> These hounds are said to have the faces of men, and have been heard passing the church on dark, still nights.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Former Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Ashdell Grove house, Westbourne Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This building is reportedly haunted by a maid who committed suicide after her secret lover, a local policeman, left her. Her pacing has been heard down a corridor, and she also moves items around in the building's offices. The figure of a middle aged man has also been seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pregnant Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Ball Inn, Darnall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> This thin, grey ghost is said to be a member of staff who committed suicide at the inn when she discovered she was with child. She walks the upstairs corridors, while in other parts of the building, strange noises are heard in the night and well secured shelves fall without warning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Apprentice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Barker's Pool<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1875<br>
              <span class="w3-border-bottom">Further Comments:</span> Around a thousand people assembled on the street to watch a glowing figure, named as the ghost of an apprentice, in the window of a building. A police officer pointed out that the 'entity' resembled a light being reflected in a window moving in the wind. Once the window had been secured, the ghost could no longer be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Beauchief Abbey and Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This traditional white figure supposedly haunts the grounds of both the hall and the abbey. The latter location is also the home to a spectral monk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Birley Woods and Golf Course<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> A local lad's grandfather told him that a wispy black figure in a cloak and hood was trapped in the wood and had been for hundreds of years. After the grandfather died, one of the lad's friends confided that he had encountered the figure while out in the woods.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Boggards</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Boggard Lane, Oughtibridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This lane is said to be named after the entities which live (or lived) down there. A private house along the lane is also said to be haunted by a phantom woodworker.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Bowshaw House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing a flowing dress, this pale spirit has occasionally been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Broken Necked Lovers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Bunting Nook<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A pair of eloping lovers were thrown off their horse down this lane - the fall broke both their necks. They now haunt this area where no bird is said to sing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Saucer Eyed Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Bunting Nook, Graves Park<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 2012?<br>
              <span class="w3-border-bottom">Further Comments:</span> This creature, with its unusually large eyes, has been known to turn itself into mist if threatened. A ghost hunter in the area reported to have spoken to a couple who witnessed a large black dog appear out of nowhere as they travelled down the lane.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Colonel John Bright</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Carbrook Hall public house (as of late 2019, a coffee shop)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A close friend and ally of Cromwell, the well-built ghost of Colonel Bright has been observed standing at the top of the stairs. Other ghosts reported here include the sounds of children, an elderly woman from the 1920s, a phantom Roundhead, a monk, and another shadowy figure who throws items around the bar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Riot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Cholera Monument Grounds<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> May  1873<br>
              <span class="w3-border-bottom">Further Comments:</span> Around two thousand men and young boys gathered at this site after a rumour started a ghost had been seen (although the local press declared the entity was really a 'young fellow of weak intellect'). Neighbours complained, the police arrived, scuffles occurred, no ghost appeared, and people went home. Rumours of a ghostly figure in the area continued throughout the rest of the month.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rings</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Dead Man's Lane (aka Dead Lane), Ecclesall (lane may have been renamed, but in Greystones area)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> An overgrown lane in the village (now a Sheffield suburb) was said to be home to fairies, with distinct fairy circles spotted in the grass. A headless woman also haunted the lane.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pilot?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Endcliffe Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1990s, around February<br>
              <span class="w3-border-bottom">Further Comments:</span> A man spotted a figure wearing a flying jacket and scarf walking across the foggy, open playing fields. While walking towards the witness, the figure suddenly vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Field near Wales Bar Petrol Station, now a golf course<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1987 or 1988<br>
              <span class="w3-border-bottom">Further Comments:</span> While crossing a field of sheep, a group of friends watched a large, cat-shaped creature amongst the sheep. The creature spotted the group and crouched, resulting in the friends fleeing the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chip Shop</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Firth Park<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking home one evening, a mother and daughter spotted a chip shop which they had thought was new, as they had not seen it before. The shop was open, lights were on, and people were inside. A couple of nights later, they decided to visit the takeaway, but it was not there - a DIY shop stood on the spot, twice the size of the chip shop they had seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Norah the Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Firth Park Brushes (former school), Barnsley Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The maid, who committed suicide on the site two hundred years ago, is still said to wander the area. When the high school stood on the area, cleaning staff reported lights that would turn themselves on, amongst other strange phenomena. A priest was called to exorcise the land in 2003; there have been no reported incidents since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Life Saver</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Flat, School Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1981/82<br>
              <span class="w3-border-bottom">Further Comments:</span> The family that tolerated a poltergeist in their home for almost a year were eventually thankful for the entity - one night it banged so loudly the whole family awoke only to find their rooms full of smoke. They managed to escape as the fire engulfed their building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Handsworth Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2014<br>
              <span class="w3-border-bottom">Further Comments:</span> While waiting at a bus stop, two people spotted a grey-coloured older woman walking past a tree. The woman had her hands placed under her chin, seemingly to secure a headscarf, and she wore wearing long clothing. The woman turned and looked at the people at the bus stop before vanishing in a flash.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Highcliffe Road, Ecclesall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Little is known about this phantom woman, other than an interesting point to note is that the road was once called Dead Man's Lane.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Not a Ghost</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Hill between Solly Street and Allen Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1820s<br>
              <span class="w3-border-bottom">Further Comments:</span> A Barghast was said by children to have taken up residence on this hill, which at the time was common land. It later emerged that the ghost was a hoax.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - House along Campo Lane<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> March 1855<br>
              <span class="w3-border-bottom">Further Comments:</span> After a lodger spotted a ghostly white figure in the kitchen, this house was visited by a many people hoping to see the entity. One of these visitors, a Mrs Rollinson, collapsed after witnessing a white female form dash past her. After briefly regaining consciousness and sharing her encounter, Rollinson once again fell unconscious and died soon after. It is worth noting that other people were with Rollinson at the time of the encounter, but they did not see the reported ghost.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Peering Face</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Industry public house, Darnall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> When the Sheffield Paranormal Society carried out a vigil of this property, photographs were taken inside the back room. When they came back from being developed, a face could be seen peering through a window, even though nothing was seen at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Mail Coach public house, 149 West Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1984<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing the clothing of a robber, this figure was seen lurking in the shadows prior to mysteriously disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield - Manor Castle public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1983<br>
              <span class="w3-border-bottom">Further Comments:</span> The landlord of this public house moved out after only three weeks, claiming that his child and pets had been constantly upset by a white human form.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 57</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=57"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=57"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northeastandyorks.html">Return to North East and Yorkshire</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North West England</h5>
   <a href="/regions/northwest.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/northwest.jpg"                   alt="View North West records" style="width:100%" title="View North West records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
