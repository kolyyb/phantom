

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Welsh Folklore and Strange Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/wales.html">Wales</a> > Mid Glamorgan</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Mid Glamorgan Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fish Fall</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdare - Mountain Ash<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 11 February 1859 (or 1841)<br>
              <span class="w3-border-bottom">Further Comments:</span> Live fish were seen falling from the sky during a storm around the home of a Mr Nixon. Some of the fish were removed from the guttering of neighbouring houses; a few of the creatures survived and kept in freshwater.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mass Premonition</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberfan - No fixed location<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-21 October 1966<br>
              <span class="w3-border-bottom">Further Comments:</span> According to the Journal of the Society for Psychical Research, Vol 44, no 734, around 200 people reportedly predicted this disaster in the days leading up to it, including a local girl who was unfortunately one of those killed in the appalling accident.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Trotting Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgend - Crossroads between Bridgend and Laleston<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Said to manifest every night at midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom hound haunting this crossroads would appear nightly and trot off along a road before vanishing. It was said several people had tried to follow the creature but would always lose sight of it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gold Guard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgend - Ogmore Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Protecting hidden treasure, this shade once attacked a man who tried to take all the gold - he died soon after of a wasting disease. It is not known whether the spectral woman who has also been seen here is the same ghost, although most legends say she does guard the hoard, which in turn is said to be haunted by the wailing of its former owners. Another legend says that whoever falls asleep here may wake in the past or future.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hazy Airman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgend - RAF Stormy Down (abandoned)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> June 2005 (man and son encounter), 08 April 2012 (photograph)<br>
              <span class="w3-border-bottom">Further Comments:</span> A man and his twelve year old son watched a hazy figure dressed in a Second World War uniform walking towards them, even though the figure did not actually move closer. The uniformed man then reached down to the ground as if to pick something up and slowly faded away. Other people have seen an airman near the former Hangar One and have also heard an air raid siren. A photograph taken in 2012 is said to show a phantom figure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Caerphilly-castle.jpg' class="w3-card" title='An old postcard of Caerphilly Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Caerphilly Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Old Hag</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caerphilly - Caerphilly Castle<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Marks a death in the de Clares family<br>
              <span class="w3-border-bottom">Further Comments:</span> Once owners of the building, the de Clares family were haunted by a Gwrach-y-rhibyn, warning them about an eminent death in the family. Now they have moved away, the ghost is still seen - drifting towards the castle only to disappear once she enters the ruins. A figure dressed in red also haunts the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Airship</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caerphilly - Caerphilly Mountain<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 May 1909<br>
              <span class="w3-border-bottom">Further Comments:</span> Two men speaking in a strange tongue were seen by a witness here - they made their escape in an airship. Though it is thought they were German, no country in the world had an airship capable of making that kind of flight at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Family</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caerphilly - Morgan Jones Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2003, 00:30h (approx)<br>
              <span class="w3-border-bottom">Further Comments:</span> Two friends walking home in the early hours of the morning spotted the silhouettes of a man, woman and child walking towards them along the same path, around one hundred yards away. As the distance fell to fifty yards, the family faded and vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Self-propelled Objects</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caerphilly - Panteg Inn<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This polt makes his presence felt by animating inanimate objects.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blonde</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caerphilly - Private residence at Penybryn<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> A visitor watched a blonde lady, in her early twenties, walk past him on the landing of a friend's place. He spoke to her, but she ignored him. A short time later the visitor asked his friend who the blonde woman was, only to find out that he should have been alone at the site. A search of the residence found nothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cilfynydd - Woods in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1968<br>
              <span class="w3-border-bottom">Further Comments:</span> Last seen by a newspaper editor and a friend, this phantom lady darts between the trees on a hillside. Some say she is accompanied by another white figure in a cloak.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Coity-Former-lane.jpg' class="w3-card" title='If the cottage glows red, don&#039;t enter.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> If the cottage glows red, don&#039;t enter.</small></span>                      <p><h4><span class="w3-border-bottom">Young Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coity - Former lane, may no longer exist<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An entity here would initially take the form of a lady so pretty that any man spotting her would fall in love. The woman would lead the man away from the road, taking them to a cottage filled with demons. The entity would also take the form of a hare, changing into a snake when people came too close.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gas or Ghost</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dowlais - Undenominational Christian Mission Hall<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 October 1897, around 20:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A mystery was declared after no source could be found for a large explosion outside the hall which shattered several windows. It was written that locals suspected a supernatural agency as the building had a history of being unlucky (in the form of once being damaged in a storm).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Child Throwing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dyffryn, Maesteg - Unidentified house<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre 1933<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghost here attached itself to a boy and forced him to set fire to haystacks and releasing the cattle from their sheds. When the boy refused to perform the ghost's demands, he found himself punished, and on one occasion, thrown over his parent's house. The ghost would also throw stones and smash windows (it is not clear whether the child acted as the ghost's agent for these activities). The phantom was eventually banished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hovering Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eglwysilan - Mountain Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1987<br>
              <span class="w3-border-bottom">Further Comments:</span> While driving along this dark road at night in the rain, a motorist encountered two white figures holding hands. He said that the faces were very clear, but they appeared not to have noses. No legs were visible, and they just hovered. The figures stopped in front of the car, looked straight at the driver for several seconds and then drifted to the other side of the road before disappearing through a field gate.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Postman's Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ewenny - Crossroads where one road leads to Ogmore<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, likely nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A postman reported that he would see a large phantom black dog every night, watching the creature move silently as if on a mission.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bell Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kenfig - Kenfig Pool<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Pool still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Legend says that the pool covers a lost town from which one can still hear the church bells ringing on stormy nights. The truth is likely to be based on the nearby town of Kenfig being lost to shifting sands during the sixteenth century. Other local legends say that the pool is bottomless and that a whirlpool pulls unsuspecting swimmers to their deaths.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Organ Music</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kenfig - Prince of Wales public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> After the landlord reported hearing phantom music and voices in his pub, John Marke and Allan Jenkins used the building to experiment with their 'stone wall' theory. While they recorded some sounds, it could be debated that there was a rational source to the sounds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Female</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangynwyd - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The church is reportedly home to a ghostly young woman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hazelnuts</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangynwyd - Gelli-siriol farmhouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> An apparition would appear to any stranger who approached the house after dark. The entity left the site after a basket of 'charmed' hazelnuts left by a former tenant were removed from a windowsill.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phil e'r Capel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangynwyd - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Phil e'r Capel harassed local man Wil Howel, forcing him to perform tasks such as tossing gold and silver into the river. The phantom also asked the girl who provided him with end-of-life care to take a hidden purse that contained a half guinea and hide it elsewhere.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Philip Thomas</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangynwyd - Pentre Farm and surrounding area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Catherine, a serving girl at the farm, refused to give the dying Philip Thomas a glass of water. His ghost returned, tossed a bucket of water over Catherine, instructed a child in the house to burn clothing, and pelted the building with stones. Philip's ghost never left Catherine's sight and would try to strangle her. The local vicar failed to banish the ghost, but fortunately the vicar of Glynogwr managed to rid Catherine of her phantom tormentor and the household returned to normal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangynwyd - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Even the most fearless residents on the village felt terror when they encountered this phantom, snow-white mare along the roads at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dark Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llanharan - High Corner House Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> This shadowy figure with black hair has been observed by the fireplace. He is blamed whenever items mysteriously vanish.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Landlady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Maesteg - Castle public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to have been murdered by her husband in Room 5, which is always said to be cold, a female form has been seen lurking downstairs. It may be the same entity responsible for the whistling that occurs after hours.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 46</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=46"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=46"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/wales.html">Return to Welsh Preserved Counties</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Scotland</h5>
   <a href="/regions/scotland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/secondlevel/Lothian.jpg"                   alt="View Scotland records" style="width:100%" title="View Scotland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
