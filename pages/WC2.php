
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="WC2 - A List of Ghosts and Strange Places in London, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > WC2</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of London's WC2 District</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2256.jpg' class="w3-card" title='12 Buckingham Street, Strand, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> 12 Buckingham Street, Strand, London.</small></span>                      <p><h4><span class="w3-border-bottom">Samuel Pepys</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - 12 Buckingham Street, Strand<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1953<br>
              <span class="w3-border-bottom">Further Comments:</span> Number twelve Buckingham Street was the home of Pepys, and his blurred, smiling phantom has been reported near the staircase of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2256a.jpg' class="w3-card" title='14 Buckingham Street, Strand, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> 14 Buckingham Street, Strand, London.</small></span>                      <p><h4><span class="w3-border-bottom">Dashing Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - 14 Buckingham Street, Strand<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Number Fourteen is home to a female ghost, described as 'pretty', who is said to dash into the house as if late for an appointment.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">William Terriss</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Adelphi Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Although William Terriss was murdered in 1897, several thespians have since heard or seen William walking about the building or playing around with lights and props. Other people name the ghost as that of Edmund Kean, another famous actor of yester-year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2246.jpg' class="w3-card" title='Albery Theatre, St Martin&#039;s Lane, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Albery Theatre, St Martin&#039;s Lane, London.</small></span>                      <p><h4><span class="w3-border-bottom">Sir Charles Wyndham</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Albery Theatre, St Martin's Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir Charles built the Albery, and his grey haired shade now walks between its walls, normally lurking near the dressing rooms.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Suicide</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Aldine House, Maiden Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> The owners of the building reported hearing strange banging's and scrapings from the floor above them - the sounds were attributed to the ghost of a man who had committed suicide there a few years previous.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Displaced Actress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Aldwych Underground Station (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Built where the Royal Strand Theatre once stood, it is thought the female ghost seen standing on the tracks migrated from the original building to the station shortly after it became operational. She is normally reported by cleaning staff working the night shift.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Climbing Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Bell Yard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Mentioned in Dickens's Bleak House, this building is reputedly haunted by phantom footsteps that move around on the staircase heading towards the legal chambers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Bow Street Magistrates' Court (no longer operating as a court)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s onwards?<br>
              <span class="w3-border-bottom">Further Comments:</span> This building was said to be haunted by a woman who would suddenly vanish when seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Man in a Tricorn Hat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - British Optical Association Museum, Craven Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 March 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> While engaging in redecoration of the building, one of the decorators encountered a crouching figure of an old man. Another ghostly figure was spotted a couple of days later, this time of a middle aged man wearing a dark blue coat with a tricorn hat, who asked the witness 'Where's Mary? I can't find Mary'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fruity</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Cheshire Cheese public house, Little Essex Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The polt at work in this pub is reputed to move the fruit machine.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Human Foot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Christ's Hospital (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1902<br>
              <span class="w3-border-bottom">Further Comments:</span> A human footprint was once visible in the stonework of this hospital. It was said to have been created when a ghostly beadle's wife stamped her foot in response to a disrespectful human.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2257.jpg' class="w3-card" title='Cleopatra&#039;s Needle, Embankment, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cleopatra&#039;s Needle, Embankment, London.</small></span>                      <p><h4><span class="w3-border-bottom">Tall, Dark Stranger</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Cleopatra's Needle, Embankment<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A tall, dark, male figure leaps over the edge of the embankment wall here, but the sound of him hitting the water is never heard. The man is thought to have committed suicide, and one of several entities found along the Thames who took their own lives. There are also reports of laughing coming from the area, the source of which cannot be discovered.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon3210.jpg' class="w3-card" title='Coliseum theatre, St Martin&#039;s Lane, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Coliseum theatre, St Martin&#039;s Lane, London.</small></span>                      <p><h4><span class="w3-border-bottom">Trench Trooper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Coliseum, St Martin's Lane<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 October 1918<br>
              <span class="w3-border-bottom">Further Comments:</span> A small group of friends out for an evening's entertainment observed a man they knew walking down an aisle of the theatre, but he quickly disappeared. They later discovered that he had been killed in trench warfare around the time they saw him. He has been seen in the same area periodically since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Kids</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Courtts Bank<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a Mecca for toy sellers and buyers, the sounds of joyous children can be heard briefly once a year. Another entity, claiming to be an Elizabethan nobleman, was exorcised during the early 1990s - he was thought to be either the Earl of Essex (executed in 1601) or Thomas Howard, Duke of Norfolk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Legless Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Covent Garden Piazza<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 December 1989<br>
              <span class="w3-border-bottom">Further Comments:</span> A photograph taken by Mr Webb of his young daughter shows a woman dressed in black floating legless in the background. While it is possible that the woman's legs are concealed behind a post, the angle at which she leans does create doubt that the lady is corporeal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/WC2_1021.jpg' class="w3-card" title='The Piccadilly Line of Covent Garden Station, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Piccadilly Line of Covent Garden Station, London.</small></span>                      <p><h4><span class="w3-border-bottom">William Terriss</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Covent Garden Station, Piccadilly Line<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Around midnight, winter months (reoccurring). Last seen 1972?<br>
              <span class="w3-border-bottom">Further Comments:</span> An actor, Mr Terriss was stabbed to death in December 1897 at a nearby theatre. His ghost, tall in stature, has been seen dressed in a grey suit with white gloves, standing on the platform late at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon1471.jpg' class="w3-card" title='Drury Lane Theatre Royal, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Drury Lane Theatre Royal, London.</small></span>                      <p><h4><span class="w3-border-bottom">Man in Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Drury Lane Theatre Royal<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to a long running show (grey man)<br>
              <span class="w3-border-bottom">Further Comments:</span> Seeing this ghost, dressed in a long grey coat with a Tricorn hat, is considered a good omen; if it appears either in the morning or afternoon during rehearsals, the performance will go on to be a great success. King Charles II is also supposed to haunt the building (he was a great theatre lover), as does the shade of Joe Grimaldi who helps anyone struggling with their lines. The backstage corridors are home to the wraith of Charles Macklin, a murderer never found guilty, and now walks the scene of his crime. Finally, actors have been known to experience a chill and simultaneously catching a whiff of lavender while on stage, the sensations blamed on a ghost.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon4589.jpg' class="w3-card" title='Duke of York&#039;s Theatre, St Martin&#039;s Lane, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Duke of York&#039;s Theatre, St Martin&#039;s Lane, London.</small></span>                      <p><h4><span class="w3-border-bottom">Violet Melnotte</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Duke of York's Theatre, St Martin's Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1935 onwards,<br>
              <span class="w3-border-bottom">Further Comments:</span> The builder of the theatre, Violet was known to watch the performances from her own personal box. After her death in 1935, a presence could be heard from within her seating area, and her phantom spotted in the green room in 1967.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anne Boleyn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Durham House (no longer standing) Durham House Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This building held Anne during her last days, and while the house no longer stands, its basement remains, and it is here the ghost still resides.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Oppressive Feeling</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Embankment Station - Page's Walk<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000<br>
              <span class="w3-border-bottom">Further Comments:</span> Staff who walk along the long dark tunnel known as Page's Walk complain of cold winds, doors which open and slam shut, and an oppressive feeling.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unlucky Place to Drink</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Fleece Inn Tavern (demolished 1820, thought to have stood on Catherine Street), Covent Garden<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1820<br>
              <span class="w3-border-bottom">Further Comments:</span> After a string of murders on the property, the site gained a reputation of being unlucky until it was demolished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Billie Carleton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Former BBC HQ, Savoy Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Carleton died in the former home of the BBC just after their 1918 Victory broadcast, and even now the presence is said to remain.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Fortune Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Home to the stage play The Woman in Black, the theatre is ironically also haunted by a grey lady spotted in a lower box. Two shadowy figures have also been observed standing to the right of the stage.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nell Gwynne</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Gargoyle Club, 69 Dean Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Another haunting blamed on Nell Gwynne, this building is said to have been her home. The ghost is said to resemble a grey shadow, accompanied by an overpowering smell of flowers. A duel is also thought to be re-enacted on the roof, between the Captain of the Guards and a group of men sent by King Charles II to kill him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2316.jpg' class="w3-card" title='Garrick Theatre, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Garrick Theatre, London.</small></span>                      <p><h4><span class="w3-border-bottom">Arthur Bourchier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC2 - Garrick Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Known for his dislike of critics, Bourchier now haunts the rear of the theatre, often appearing just after a performance, or making his presence known by tapping people on their backs.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 50</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=50"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=50"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Greater London</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View Greater London records" style="width:100%" title="View Greater London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
