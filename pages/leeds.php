
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Ghosts and Strange Places in Leeds from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northeastandyorks.html">North East and Yorkshire</a> > Leeds</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Leeds Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - 10 Woodhouse Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Footsteps, strange noises and doors opening have been blamed on a phantom Victorian lady looking for two children who died in the former nursery room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Girl's Laughter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Abbey Inn, Newlay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2007 (laughter)<br>
              <span class="w3-border-bottom">Further Comments:</span> Even though the only recent activity is said to be the sound of female laughter coming from the cellar, this pub is said to be also haunted by a grey lady, a cloaked figure (who could be a monk connected to the abbey that once stood here) and a man wearing a Guy Fawkes-style hat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Travelling Light</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Armley Prison<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s, 2014<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom manifests as a light that moves around the cells. One former employee reported prison officers in Victorian uniform and the sounds of clanging chains, with the most paranormally active areas being the new wings which were constructed over executed prisoner's graves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Suicidal Servant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Beckett University, Headingley campus<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former hospital, this campus was named by local press as being haunted by several ghosts, including a servant who threw themselves down a stairwell and something which would open and close doors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Cat with Cubs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Between Headingley train station and Burley Park station<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 June 2009 and 28 May 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> Workers on the track have seen a large black cat, around two metres in length, twice. On the first occasion the feline appeared to have two cubs, one light brown and the other black.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">James Wood</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Bond Street Shopping Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1977<br>
              <span class="w3-border-bottom">Further Comments:</span> A cobbler during life, a member of the cleaning staff encountered James' sorry looking ghost in one of the loading bays. The ghost was dressed in a muddy green jerkin and brown trousers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Haired Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Cardigan Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 October 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> The ladies' toilets appear to be home to a phantom elderly woman with long, straight grey/white hair. The figure was briefly spotted in the reflection of a mirror by one witness, while others have caught a glimpse of a figure from the corner of their eye or felt uneasy in the environment.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unhappy Butler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Carnegie College<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This fellow took his own life when he realised that his mistress would never love him. His body was found at the bottom of the large spiral staircase, and it is the same area that he still haunts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pianist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - City Palace of Varieties (theatre)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Two ghosts have made a home of this theatre - one plays the piano late at night, the other is an unknown lady who brings with her a zone of cold.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Jeans</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Copperfields College School<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1997 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Students and a dinner lady watched a man in a checked shirt and jeans sitting in the main hall wave and vanish without warning. Minor poltergeist-like activity has also been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">CCTV Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - East Ardsley Conservative Club<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> September 1993<br>
              <span class="w3-border-bottom">Further Comments:</span> This apparition materialised by the doorway of the club - it could be seen quite clearly on the CCTV network, but not directly by human eyes. The entity vanished after several minutes, though made a very brief reappearance a month later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gabble Retchets</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - General area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> These local devil dogs were believed to be the souls of those who were not baptised as children and have returned to haunt their parents.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in the Cellar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Golden Lion public house, North Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in Victorian attire, this gentleman was often seen walking around under the pub. The upper part was said to be haunted by an old woman who appeared in the children's bedroom at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kirkstall.jpg' class="w3-card" title='An old photograph of Kirkstall Abbey in Leeds.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old photograph of Kirkstall Abbey in Leeds.</small></span>                      <p><h4><span class="w3-border-bottom">Abbot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Kirkstall Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1935, 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> The former Abbot of the abbey walks the area that has been converted into a museum. It is more usual to hear the spirit moving around than see him. Traditionally the entity is said to wear white robes, although the sighting in the 1970s reported the ghost's robes as being brown/red. The abbey grounds are also home to a white female entity named Mary who witnessed her lover committing murder and turned him in.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Kirkstall Forge railway station (since relocated)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1904<br>
              <span class="w3-border-bottom">Further Comments:</span> The station porter and a passenger both spotted a figure in a long grey sheet with a long streak of red running through the cloth, standing on a shed roof. A booking clerk also witnessed the entity, which soon disappeared. Prior to this, lights had been seen flickering around the station.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Running</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Leeds General Infirmary<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> An employee at this hospital heard five voices and footsteps rushing down a corridor but could see nothing - there was a gust of wind as the invisible people passed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smoky Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Old Lewis's building, The Headrow<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> A labourer working in the building with a colleague photographed a misty 'figure' with his camera phone. Another worker on site claimed to have seen something from the corner of his eye, though when he quickly turned, it had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shaggy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Outer ring road, between Horsforth and Rodley<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid 1993, 02:45h (Figure), Spring 2013 (Cat)<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver began to feel overwhelmingly terrified while passing through a valley along this road. To quote, 'stood at the side of the road and looking directly at me, was a very tall, shaggy, brown or black, fur covered shape. At my estimation, it must have stood over two metres tall, around 1.2 metres wide, with no visible limbs or eyes, but I sensed that the eyes were there somewhere, as I could feel them boring into the car'. The witness accelerated away, but the feeling of dread lasted until morning, even after arriving home. Earlier the same year a large black cat was observed crossing the same road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">'Bill Sikes' Lookalike</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Park in Beeston area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 September 2014, 02:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness walking through this park reported seeing three ghostly figures; one resembling Oliver Twist's Bill Sikes, and a man and woman wearing what could have been well-to-do Victorian clothing. The witness said there were no feelings of ill ease.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Potternewton Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Not reported for years, the ghostly carriage was supposed to be pulled by a team of four horses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Two Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Private residence near a churchyard, Kirkstall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1993<br>
              <span class="w3-border-bottom">Further Comments:</span> Two small children appeared at a witness's bedside in the night. Both children were very still and looking at the witness's girlfriend. The witness started to wake her, but terror prevented him. Not wanting to take his eyes off the children, the witness tried to reach the light switch but could not find the button. The children vanished and the witness stayed awake until dawn.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">CID Busted</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Private residence, Ash Grove<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> October 1949<br>
              <span class="w3-border-bottom">Further Comments:</span> Leeds Criminal Investigation Department declared that the property's flying bottles and paint pots, and the strange voices, were not paranormal in nature, although it is not clear whether they arrested anyone in connection to the activity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elsie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Private residence, Kirkstall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 October 2015, 22:30h (Elsie seen)<br>
              <span class="w3-border-bottom">Further Comments:</span> A former shop worker, the ghost named Elsie was spotted in the hallway. Other activities reported here include disembodied footfalls, giggling, and lights switched on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Brass Band</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Private residence, Lynwood Crescent<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1990s, around midnight<br>
              <span class="w3-border-bottom">Further Comments:</span> As one young man went to bed, he could hear the faint sound of a brass band. Within five minutes the band sounded like it was outside his house so he climbed out of bed and looked out the window but could see nothing. Ten minutes later the sound of the band was so loud it was as if it was in the house. Once again, the man looked out of the window - there was nothing there and the sound stopped. Thinking the incident could have been a dream he did not mention it, although several years later he started to tell his brother the story, who finished the story as he had also heard the same thing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Basement</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leeds - Retail premises along Hunslet Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> An employee at this shop spotted an old man in the basement. When they described the man to their manager, they were told the description matched the manager's grandfather who had also worked on the site.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 33</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=33"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=33"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northeastandyorks.html">Return to North East and Yorkshire</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North West England</h5>
   <a href="/regions/northwest.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/northwest.jpg"                   alt="View North West records" style="width:100%" title="View North West records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
