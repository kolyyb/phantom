

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Co Louth Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Louth</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Louth Ghosts, Folklore and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Waiting Army</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardee - Dawson's Mount, aka Garret's Fort<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An enchanted army of ten thousand men was said to be concealed within this hill - when the day comes for the troops to move out, they will slaughter all they come across until a red headed woman tells them they have killed enough.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardee - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> To see this phantom coach indicated that someone in the neighbourhood would soon die.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/carlingford-castle.jpg' class="w3-card" title='An old photograph of Carlingford Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old photograph of Carlingford Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Henrietta Travescant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carlingford - Carlingford Castle and Abbey ruins<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A pirate before taking up the residency of Abbess, this woman is thought to be one of the pair of shades seen within the ruins.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Carlingford-Lough.jpg' class="w3-card" title='An old postcard of Carlingford Lough in County Louth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Carlingford Lough in County Louth.</small></span>                      <p><h4><span class="w3-border-bottom">Eel-like Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carlingford - Carlingford Lough<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The lough is said to be home to a lake monster which resembles a rather large eel. The site featured in a few news outlets in 2019 after Adrian Shine, a Loch Ness Monster hunter, was invited to investigate the waters.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Carraig</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carlingford - Mountain<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Coins found 1989-2002, hunt occurs second Sunday of May<br>
              <span class="w3-border-bottom">Further Comments:</span> There was a flurry of Leprechaun activity on this mountain after a green suit and several gold coins discovered in 1989. Two years later, 'Leprechaun Whisperer' Kevin Woods claimed have contacted Carraig, elder for the 236 remaining leprechauns in the area. The little people are now protected in the area, and while there is an annual Leprechaun Hunt, the only things found are tiny cauldrons which have been left by the fairy folk themselves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carlingford - Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure was watched as she moved towards the door of the rectory, followed by a priest. Both figures vanished, though the girl has been seen since within the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tool Mover</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coolcreedan - Farms in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> June 1949<br>
              <span class="w3-border-bottom">Further Comments:</span> Fairies would be blamed when gates were found left open and tools removed from their sheds, only to be found hanging in nearby trees.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">James II</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Donore - Athcarne Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> James II stayed here just before his defeat at the Battle of the Boyne, and now his shade lingers on. He is not alone here, however. A crazy looking girl walks around the area with her hands caked in blood, while a soldier has been observed hanging by his neck from a tree.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smashing Glass</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drogheda - Barney Mac's public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> CCTV caught a full beer glass breaking in two while on a table. While some people blamed a phantom, others said it was down to thermal stress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Drogheda-with-viaduct.jpg' class="w3-card" title='An old postcard of Drogheda&#039;s Boyne Viaduct.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Drogheda&#039;s Boyne Viaduct.</small></span>                      <p><h4><span class="w3-border-bottom">Crunching Gravel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drogheda - Boyne Viaduct<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> June or July 1990, 02:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> While photographing an eclipse of the moon, two friends heard footsteps walking along the gravel laying on the surface of the viaduct, although no one could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Noises</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drogheda - Newtown Meadows estate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> Boyne Paranormal Investigators visited the mostly deserted estate after four houses were reported to have strange events occurring within, including toys which would turn themselves on, banging, and the sounds of people moving around in empty rooms.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Drogheda-Boyne-river.jpg' class="w3-card" title='An old postcard showing Drogheda&#039;s River Boyne.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Drogheda&#039;s River Boyne.</small></span>                      <p><h4><span class="w3-border-bottom">Fleming and Golding</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drogheda - River Boyne<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> After Sir Edward Golding and his wife Elizabeth Fleming were drowned in a boating accident, their shades returned to haunt the area. The River Boyne is also the location where the tenth century poet Erard Mac Cossi threw a stone at a swan which fell to earth and transformed into a woman - she claimed to have been metamorphosed by demons who also travelled in the shape of swans.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Plunkett</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drogheda - Saint Peter's Church<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> Author Vikki Bramshaw claimed to have video recorded the ghost of Saint Oliver Plunkett, although there is a chance the ghost is a reflection. The ghost had not been seen before nor since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hanging Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drogheda - Stagrennan House, Mornington Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Unable to sleep, Frank K rolled over in his bed to turn on the lamp and spotted a woman standing with her back turned to him a couple of metres away. She had mousy brown curly hair in a ponytail tied with a black ribbon and was wearing a black and red vertically striped corset top with white puffy shoulders. Frank thought the woman had entered his bedroom by accident, before realising that she was floating off the ground, and that she had a rope around her neck which stretched towards the ceiling. The apparition quickly faded away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Centaur?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drogheda - Unnamed road in the area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1966<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people travelling by car had to stop and wait for a couple of minutes after a horse with a human's face blocked the road. The terrified couple were frozen with fear and when the creature suddenly vanished, they turned around and drove home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Murphy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dundalk - Castletown Road leading to bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Murphy originally hailed from the area and took to haunting this stretch of road. The sounds of ghostly rattling chains and clattering feet would be heard moving along the road, only ceasing when they reached the Castletown bridge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Builder</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dundalk - Roche Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Rohesia de Verdon promised a builder that if he constructed her a castle, he could marry her. By the time construction was complete, Rohesia had changed her mind and had the builder thrown out of a high window and he died on the rocks below. His ghost can be heard moaning from beneath the window.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kilcurry - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A silent, phantom coach was once said to appear whenever a local person was close to death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Giants-Grave.jpg' class="w3-card" title='An old postcard showing Proleek Dolmen and tomb.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Proleek Dolmen and tomb.</small></span>                      <p><h4><span class="w3-border-bottom">Giant's Grave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ravensdale - Proleek Dolmen and tomb<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The Scottish giant Parrah Boug MacShagean built the dolmen at this site and challenged Fionn mac Cumhaill to combat. Fionn poisoned the nearby river and Parrah died from drinking the water. Parrah's body is said to lay in the nearby tomb. A legend says that if a visitor can manage to land three stones on to the dolmen, their wish shall be granted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Heinous Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Termonfeckin - Road passing Rath House<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1966<br>
              <span class="w3-border-bottom">Further Comments:</span> Two young adults driving along this road witnessed an otherworldly horse with the face of a man, the creature's eyes bulging from their sockets. The manifestation stood in the road, blocking their path for two minutes before vanishing.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 20 of 20</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
