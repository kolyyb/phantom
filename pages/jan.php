
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in January">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > January</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>January - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Andrew Mills</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ferryhill (Parish of Merrington) (Durham) - Brass Farm, now known as High Hill House farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 January (or a couple of days either side) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Andrew went on a killing spree at the farm in 1863; he claimed he was talked into it by a devil. The wild cries of Andrew Mills can be still heard near the locality of the farmhouse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jamaican Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ireby (Cumbria) - Overwater Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 January (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Formerly Overwater Hall, the building was purchased in 1814 by Joseph Gillbanks. Legend has it that Gillbanks had an affair with a Jamaican girl who he tried to drown at Overwater Tarn when she told him she was pregnant. As she struggled to pull herself out of the water, he chopped her arms off. Her armless apparition is now said to appear here once a year, although one report suggests that she could have last been seen in August.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manningford Bruce (Wiltshire) - The Hatches<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 January (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Pulled by a team of four headless horses, this golden coach appears at The Hatches.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant Arthur</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Reynoldston (South Glamorgan) - Arthur's Stone<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 January (reoccurring) (walking stone legend said to occur)<br>
              <span class="w3-border-bottom">Further Comments:</span> One story associated with this Neolithic tomb has King Arthur finding a stone in his boot. The king picked the pebble out and threw it to one side, thus placing the stone where it now stands. Another legend says the stone walks down to the sea once a year for a quick drink. Finally, another story says the stone was split by a miller who was after a millstone - unable to lift it, the split stone has remained laying there since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bump</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gateshead (Tyne and Wear) - Dead Man's Arch (former site of Bensham Station)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 January (reoccurring), midnight<br>
              <span class="w3-border-bottom">Further Comments:</span> A local piece of folklore says a train driver took his life under the arch. If one walks along the footpath at midnight on New Year's Day, you can feel the bump of a body against your own.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Squire Surtees</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kingston St Mary (Somerset) - Tainfield House (no longer a single house)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 05 January (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding a mottled grey horse, the squire rides circles on the driveway, rattling a handful of chains.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> West Deeping (Lincolnshire) - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 January, 01:00h (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The date of this woman's death is marked by her phantom sobs heard emanating from the church. Her husband was so distressed by the death that he took his own life the following day (see Tallington, Lincolnshire).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Upset Husband</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Tallington (Lincolnshire) - Footbridge over the railway line<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 15 January, 17:50h (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> After the death of his wife (see West Deeping, Lincolnshire) this grief-stricken individual jumped from the footbridge over the railway track and was hit by the train from London, which killed him instantly. The incident is re-enacted once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lostwithiel (Cornwall) - Braddock Down<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 January (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The site of a Cromwellian defeat, the anniversary of which is marked by phantom hoof falls.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Mohun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Redditch (Hereford & Worcester) - Moon's Moat<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 January (reoccurring) but last seen 21 January 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> This small, moated island is reportedly haunted by this woman, who is reported to appear every St Agnes Eve.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smith on Horseback</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nether Lypiatt (estate) (Gloucestershire) - Nether Lypiatt Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 January (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> An owner of a horse once hanged a blacksmith for failing to complete a piece of work within a tight time limit - the date of the death is marked by the appearance of a large white horse carrying the dead smith (or the phantom smith opening the gates, depending on the source). Another report says that the ghost of the smith does not actually appear but unlocks and opens the gates. A spirit in the house itself, that of a grey lady, has been seen several times, and there are unsubstantiated reports that it has been exorcised.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Stourton (Wiltshire) - Road in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 January (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This headless horseman travels with a large black hound. The man is said to have broken his neck after being thrown from his horse, trying to travel from Wincanton Market to Stourton in less than seven minutes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Wintour</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Huddington (Hereford & Worcester) - Huddington Court, and road outside<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 January (reoccurring), road encounter in November (year unknown)<br>
              <span class="w3-border-bottom">Further Comments:</span> One story says that this decapitated shade that appears once a year still mourns the passing of her husband Robert Wintour, executed on 31 January 1606 for playing a part in the Gunpowder Plot. One November evening, a couple driving past the court spotted what they believed to be a person riding a bike, but as they passed, realised the figure to be a woman in a cloak who vanished. The couple stopped at a nearby pub, and being visibly shaken, the landlord guessed that they had encountered Lady Wintour, who would glide down the lane.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brimpton (Berkshire) - Lane running through village, leading to Able Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January (exact date not known)<br>
              <span class="w3-border-bottom">Further Comments:</span> Carrying its occupants to a ball, the coach was caught up in a storm that washed away Able Bridge - the coach plunged into the water, killing all the passengers. The event is heard once a year after sunset, marking the anniversary of the tragedy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rushing Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> East Malling (Kent) - Barming Woods<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January (normally) (reoccurring). Seen last in 1971<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure has been both seen and heard as he darts around trees and over roads. One witness said the entity charged towards their car, disappearing on impact.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rainaldus the Chaplain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Runwell (Essex) - Our Lady of the Running Well parish church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January - April (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Rainaldus is said to have practiced black magic, and that the Devil came for him (with the north door of the church still showing the scorch marks created by the Devil's hands). Now the ghost of a monk with a scarred face, dressed in black, haunts the area; and the old Chaplain is blamed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Guisborough (Yorkshire) - Guisborough Priory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January - first new moon of the year (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The monk that is said to appear at these ruins once a year has been rarely seen since the 1960s. A large chest of gold is also said to be buried in the area, watched over by a raven.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Runner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton (Greater Manchester) - Hall i' th'  Wood Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Runner between 25 December - 6 January (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Always heard but never seen, this spook frequents the old staircase, the footsteps always rushing. Other ghosts reported here include an old woman seen in the kitchen and two men, one dressed in black and the other green.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Night Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ilmington (Warwickshire) - Roads of village, particularly Pig Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: 24 December and 01 January the pack hunt, date of coach unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This spectral vehicle runs along the roads, before suddenly veering off and vanishing across local fields. This may be related to the ghostly hunt pack seen in the area, led by a man killed by his hounds.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 19 of 19</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>


</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
