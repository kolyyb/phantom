
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in October">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > October</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>October - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screams of Spanish Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dun an Oir (County Kerry) - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Over eight hundred Spanish soldiers landed here in 1580, overrunning an English garrison. Spanish reinforcements failed to come, however, and their victory was short lived when more English troops arrived. The Spaniards surrendered, but most were immediately put to death by the enraged English. Their death throes echo around the area on the anniversary of their defeat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Staring Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Audlem (Cheshire) - Audlem Road leading to Corbrook Court<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 October, 2001 and 2002 (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity manifested twice to the same witness, exactly a year separating the sightings. While driving down the road, the witness spotted the strange looking figure on the roadside, who stared into the car as he drove past. The figure did not appear in the rear view mirror.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Alexander MacAllister</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glenlivet (Moray) - Altnachoylachan Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The site of a battle in 1594 between the troops of George Gordon, 1st Marquess of Huntly and Francis Hay, 9th Earl of Erroll, is haunted by the headless ghost of MacAllister riding on horseback - he fought briefly, although was decapitated by the first cannon shot.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Osyth (Essex) - St Osyth's Priory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 07 October Saint Osyth manifests (reoccurring), monk seen late 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> While legend states that Saint Osyth herself haunts the building, a pale monk has also been spotted walking around at night carrying a candle. One witness reported seeing the ghost of a woman standing by a washing machine while the building was being used as a nursing home, while several new residents to the centre claimed to have watched a procession of monks walk by a window.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Stratford-upon-Avon (Warwickshire) - Walton Hall Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 09 October, every five years (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This pale riderless horse gallops over the gardens, twice a decade. The hotel itself is haunted by a young man, who some speculate to be the former rider of the horse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Mourning Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Charfield (Gloucestershire) - Charfield graveyard, memorial to those killed in the Charfield Station rail crash<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This woman in black, who covers her face with her hands, is widely believed to be the mother of two children killed in a rail disaster which occurred in 1929. Ten people, including the children, who were killed in the train crash could not be formally identified and are buried in a mass grave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/battle.jpg' class="w3-card" title='Site of the 1066 Battle of Hastings.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Site of the 1066 Battle of Hastings.</small></span>                      <p><h4><span class="w3-border-bottom">Knights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Battle (Sussex) - Site of the 1066 Battle of Hastings<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom knight on horseback appears on the anniversary of the battle, while other figures have been seen more sporadically. The ground is also said to bleed after a storm.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Chrysler Hit by Express</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Conington (Cambridgeshire) - Railway junction (8m NE Peterborough)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 16 October, 4pm (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> 1948 saw the tragic death of Colonel Mellows, when he drove his car over the tracks only to be hit by a speeding train. Another explanation of the ghosts here says that a lorry of German POWs was hit during the Second World War, and that one of these soldiers now haunts the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Blackpool-Carleton-Crematorium.jpg' class="w3-card" title='Green ghostly face.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Green ghostly face.</small></span>                      <p><h4><span class="w3-border-bottom">Green Face</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackpool (Lancashire) - Carleton Crematorium<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1936 (face), 31 October (reoccurring) (horse)<br>
              <span class="w3-border-bottom">Further Comments:</span> Harry Hodges, a taxi driver, was taking his fare to the crematorium. Pulling up outside the gates, he suddenly spotted a green face belonging to a man with long dark hair and sunken eyes. Harry's passenger screamed and ran off, and the face disappeared after moving across the front windscreen. A less detailed local legend says that a horse manifest at midnight on Halloween and races towards the gates before vanishing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall, Dark Stranger</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Totton (Hampshire) - Testwood House (aka Rumasa House)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 October (reoccurring), though not always the case, last seen 1972<br>
              <span class="w3-border-bottom">Further Comments:</span> This enigmatic figure stands at the entrance to the driveway and it is thought he murdered a young lady in the property. Those who have seen the man say he is dressed like an old coach driver, which would explain the occasional sounds of a phantom coach passing through the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Betty Potter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boxted (Essex) - Bend in the road close to Boxted Road, known as Betty Potter's Dip<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 October, midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Hanged from a tree for witchcraft (or she committed suicide, depending on the story), Betty is said to return once a year to the site of her death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poynton Green (Shropshire) - Field in the village<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 22 October, every ten years since 1941 (next in 2021) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Two Czech pilots died here on 22 October 1941, crashing in the field as they ran out of fuel. A local farmer rushed to the field trying to save them but was beaten back by the flames. He did, however, see a black cat emerge from the burning wreckage. The creature made its home with a local old woman, and when she died, the cat vanished, but appears once every ten years at the crash site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Battle of Edgehill Repeating</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edgehill (Warwickshire) - Kineton<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 October (reoccurring), also 23 December 1642, lasting several nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A bloody battle of the English Civil War, the fighting repeated itself for several successive nights in 1642, in front of dozens of witnesses. More recently, sounds of battle have been reported, though the visuals are not forthcoming. The date of the more recent battle sounds is said to be 23 October.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Duel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Rye (Sussex) - Mermaid Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 29 October (duel) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Several shades haunt this public house. A pair of ghostly duellers have been seen engaging each other with rapiers (the victor then drags the body through the hotel and drops it through a trapdoor), while a female figure dressed in grey haunts the upper part of the building. Room 5 is one of her haunts, while in rooms 10 and 18, a fading man has been seen entering and leaving (often through a wall).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Blunt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lydiard Millicent (Wiltshire) - Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> It was on this date that Lady Blunt watched her fiance murdered; the shock she suffered was strong enough to leave an enduring psychic echo which has lasted over two centuries.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Inverurie (Aberdeenshire) - Pittodrie House hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The cries for help heard only on Halloween emerge from the staircase. They may belong to a maid once caught in a fire which engulfed the building - she fell to her death trying to escape. A ghostly carriage pulled by a single horse haunts the driveway, while another spook has been reported in the library.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Secret Meeting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Maybole (Ayrshire) - Coves of Culzean<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The little people are reported to gather here once a year for secret planning sessions.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Dollar-General.jpg' class="w3-card" title='An old postcard showing Dollar in Scotland.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Dollar in Scotland.</small></span>                      <p><h4><span class="w3-border-bottom">Floating Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dollar (Clackmannanshire) - Deil's Cradle, found on a glen which was known as Burngrens<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The stone was said to float in the air on Halloween's night, the Devil sitting upon it, surrounded by witches, until dawn breaks the following morn.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymena (County Antrim) - Road leading to the White Gates, Crebilly Road area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A robber escaping from a house rode into a thin piece of wire tightly pulled between the gateposts here, losing his head in the process. His decapitated shade is either seen or heard at Halloween, still on horseback.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/OI-128.gif' class="w3-card" title='An old woodcut of a ram attacking a well-dressed gentleman.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a ram attacking a well-dressed gentleman.</small></span>                      <p><h4><span class="w3-border-bottom">Shuck and Friends</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Tuamgraney (County Clare) - Wooded area<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This wooded hollow is particularly haunted around Halloween, when it is said to change into dense woodland with demonic creatures, including a red eyed dogs and rams.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Waiting Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balbriggan (County Dublin) - Ardgillan Castle, bridge in garden over railway known as The Lady's Stairs<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom woman is said to be waiting for her husband to return. He was a keen swimming, but drowned one night, leaving his wife on the bridge awaiting his return. One version of the story says that whoever sees the ghost on Halloween will be picked up and thrown into the ocean.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Armboth Fell (Cumbria) - Thirlmere Lake<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The echoes of phantom bells have been heard coming from beneath the waters here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lantern Bearer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dacre (Cumbria) - Hawkesdale Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Having hanged himself in the hall, this young lad holding a lantern appears on Halloween and walks towards the nearby river Caldew, vanishing into its cold waters.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">All of Horrors of the Lakes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Keswick (Cumbria) - Ambroth House (no longer standing?)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend says that all the ghosts and spirits that haunt this part of England meet at the house once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor1752.jpg' class="w3-card" title='Bournemouth Town Hall, Dorset.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bournemouth Town Hall, Dorset.</small></span>                      <p><h4><span class="w3-border-bottom">First World War Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournemouth (Dorset) - Bournemouth Town Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This incorporeal soldier of Indian heritage appears here once a year to help himself to a drink of water. There are also a few reports of phantom horses and carriages being seen outside the building, while a ghostly cat is also said to haunt several of the town hall rooms.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 47</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=47"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=47"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
