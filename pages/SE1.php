
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="SE1 - London's Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > SE1</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of London's SE1 District</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Raining Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - 56/58 Reverdy Road, Bermondsey<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 April 1872, 16:00-23:30<br>
              <span class="w3-border-bottom">Further Comments:</span> This building was bombarded with stones, smashing every window, injuring people nearby, and even destroying furniture inside - a heavy police presence failed to stop the attack, though it ended almost eight hours after initiation.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tailless Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Anchor Inn, Clink Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This inn is haunted by a dog which lost its tail while trying to protect its master from press gangers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Teleporting Items</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Anchor Tap public house, Horselydown Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This tavern's resident ghost (Charlie to his mates) moves items around the building and has been known to hide things for months on end before returning them to their rightful location.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tailless Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Anchor Tavern<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This mangy looking mutt failed to save his master from the press gangers who operated here in the 1700's - it now appears around closing time, sniffing around for the owner.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon13578.jpg' class="w3-card" title='Blackfriars Bridge over the River Thames, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Blackfriars Bridge over the River Thames, London.</small></span>                      <p><h4><span class="w3-border-bottom">Tall Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Blackfriars Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A postal worker watched a tall woman wearing black standing on the edge of this bridge. The woman suddenly leapt into the river. Horrified, the postal worker ran to the edge of the bridge intending to jump over the railings to save her. A nearby policeman also rushed over but, instead of helping the woman or summoning additional support, grabbed the postman. The postal worker told the policeman what had happened, only to be told the woman had previously committed suicide and it was now her ghost which performed the same action every day. The policeman told the postman to return at the same time the following day to witness the woman make the suicidal jump again. The postal worker returned as requested and did indeed see the woman in black repeat her jump.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walter?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Boat moored close to London Bridge<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> December 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> An employee on this boat reported seeing an image taken on board which shows the side view of a figure with ruffled collar. The figure disappears at the knee and looks as if it floats on a white cloud.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - British Transport Police Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> More than one officer working alone has heard the distinct sound of footsteps walking along and stopping a short distance away. One witness reported the sound of footsteps was followed by a loud bang and the floor shaking. One detective spotted a shadowy figure running through the area, while a builder on the site walked past a lady who was not actually there...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lost Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Czar Bar (was the Horns public house), Crucifix Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This little lost spook has been heard moving around the building, crying and calling for her mother. Another witness said, perhaps more sinisterly, that it sounded like the little girl was crying in fear of her mother. More recently, a ghostly old lady has also been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Priest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - East Lane, Bermondsey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1894<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity, dressed in clothing that the witness dated as late eighteenth century, was seen standing by a window in the presbytery.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon1023.jpg' class="w3-card" title='The Northern Line platform of the Elephant &amp; Castle underground station.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Northern Line platform of the Elephant &amp; Castle underground station.</small></span>                      <p><h4><span class="w3-border-bottom">Vanishing Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Elephant & Castle Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Northern Line unknown, Bakerloo woman seen twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Footfalls and rapping echo around the Northern Line in the station after it has closed - on investigation, no sound source can be found. Another story says the last train of the night on the Bakerloo Line is haunted by a lone girl who walks from the last carriage to the tip of the train, vanishing as she reaches the front carriage.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - George Inn, George Inn Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The nondescript shape of a female figure has been seen drifting in the bedrooms of this old coach house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sound of New Boots</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Guy's Hospital, St Thomas Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1969?<br>
              <span class="w3-border-bottom">Further Comments:</span> A nurse dressed in late nineteenth century garb has been seen walking down the corridors of the hospital, sometimes placing her hand on patient's shoulders to reassure them. When not seen, she is heard - pacing loudly down echoing walkways.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/se12259.jpg' class="w3-card" title='Lambeth Palace and the River Thames, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Lambeth Palace and the River Thames, London.</small></span>                      <p><h4><span class="w3-border-bottom">Anne Boleyn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Lambeth Palace, & River Thames near the building<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Carried in a Thames barge crewed by faded grey figures, the shade of Boleyn is taken to the Tower of London where she met her maker. The palace itself sometimes echoes with Anne's voice, pleading and crying for her life to be saved.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blitz Bodies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Leather Exchange public house, Leathermarket Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> The cellar of this pub was used as a makeshift mortuary after a heavy bombing raid during the Second World War. Staff maintain that the presence of the dead remains.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Self-Locking Door</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Lollards Prison<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The prison is thought to contain the spirits of many a former inmate, with one door renowned for locking and unlocking itself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - London Road Depot (Bakerloo Line)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This area is thought to be haunted by a nun who is connected to a nearby Roman Catholic school.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">After Hours Drinking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Market Porter public house, Stoney Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> People walking past this pub late at night have reported seeing ghostly figures standing at the bar, just visible through the windows.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bloodied Hands</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Old Vic, Waterloo Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly woman has appeared several times, always clasping her bloodstained hands. Some believe that she was an actress, and that the blood is fake - used for a stage production of a Shakespearian play.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/se16970.jpg' class="w3-card" title='River Thames, close to the site of the London Eye.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> River Thames, close to the site of the London Eye.</small></span>                      <p><h4><span class="w3-border-bottom">Angel of the Thames</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - River Thames, close to the London Eye<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> At least four people have reported seeing an angel hovering over the River Thames in 2006. Some people say the sightings date back to 1666, the Great Fire of London, while other recent sightings have occurred around war time. Others have called the sightings a hoax, naming people behind a charity event as being responsible for the 'sightings'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Slamming Doors</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Site of Waterloo Necropolis Railway, now a training school<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Some staff at this railway training school allegedly now refuse to work at night in 'A' block, due to classroom and other doors slamming shut by themselves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Great Armies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Skies west of London Bridge<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 March, year unknown (but likely nineteenth century)<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to have been watched by many people, two great armies were watched as they engaged each other in the sky. During the fight, a flame was seen to head towards the city. As the flame faded, the armies vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/se12197.jpg' class="w3-card" title='St Thomas&#039; Hospital, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> St Thomas&#039; Hospital, London.</small></span>                      <p><h4><span class="w3-border-bottom">Grey Nurse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - St Thomas' Hospital<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century, prior to a death of a patient<br>
              <span class="w3-border-bottom">Further Comments:</span> St Thomas' Hospital, London. A ghostly nurse dressed in grey, reluctant to leave her job, has been mistaken for a real person - until she vanishes into thin air. Other stories say that the nurse's legs abruptly end at the knees. Although her facial details are perfect and of utmost clarity, no one has been able to place or identify her. Some tales report that patients who spot this phantom woman died soon after, a theme repeated in a few other hospitals in the UK.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Noisy Room</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Thomas a Becket public house, Old Kent Road<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> At one time, the landlord here claimed he had a room so haunted upstairs that nobody could spend more than five minutes inside without fleeing in terror.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Faces</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Three unidentified houses on Stamford Street (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1872<br>
              <span class="w3-border-bottom">Further Comments:</span> After the death of the landlord of these three, long term empty, properties, lights would be seen flitting around, and the occasional ghostly face seen at the windows.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Police (and Not a Ghost)</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE1 - Unknown house, Bermondsey Street<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1909<br>
              <span class="w3-border-bottom">Further Comments:</span> James Phillips was told he was 'more likely to find a policeman than a ghost' by a judge while in court for breaking into an alleged haunted house.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 26</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=26"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=26"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Greater London</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View Greater London records" style="width:100%" title="View Greater London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
