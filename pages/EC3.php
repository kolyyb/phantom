
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="EC3 - A List of London Hauntings and Strangeness from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > EC3</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of London's EC3 District</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Aldgate Underground Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-to-late-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This old woman was seen by an engineer as it stroked his friend's hair, seconds before the co-worker touched a live wire which sent 20,000 volts through his body. Remarkably, he survived. Phantom footfalls have also been reported coming from down the tunnel, abruptly finishing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Persian Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - All Hallows by the Tower, Great Tower Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1940<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom feline haunted the original church prior to its destruction by German bombers during the Second World War. In one story, a small group rehearsing Christmas carols claimed to have seen a phantom woman sitting on a chair. The figure disappeared and was replaced by a black cat which also vanished soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coughing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Alley linking St Mary At Hill and Lovat Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 November 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> A person walking through the alley heard footsteps following behind and a loud cough - they spun around, but no one could be seen. They retraced their steps and looked up and down St Mary At Hill but still no one could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec3_2273.jpg' class="w3-card" title='George and Vulture public house, St Michael&#039;s Alley, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> George and Vulture public house, St Michael&#039;s Alley, London.</small></span>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - George and Vulture public house, St Michael's Alley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The upper floor of this established public house is home to a phantom woman who drifts along its corridors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Silhouette</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - King William Tunnel, Under London Bridge (disused underground tunnel)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s?<br>
              <span class="w3-border-bottom">Further Comments:</span> An image taken by a photographer shows what appears to be a silhouetted figure along this tunnel, though no one else was there at the time. A medium called to the location claimed that the ghost was that of a man who died while breaking up a fight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jewish Cries</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Old London Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This area of the river under the bridge is thought to be the location where dozens of people with Jewish faith drowned in 1210, when a ship removing them from the country sank. Their screams and cries have been reported in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Priest with Black Hair</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - St Magnus the Martyr Church, Fish Street Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This holy man has been observed standing over the tomb of a former bishop, maybe paying his last respects? A headless figure has been reported at least once in the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Alice Hackney</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - St Mary at Hill church, off lower Thames Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Alice's body was moved during restoration work on the church in the late nineteenth century and reburied away from its original location. She has since been seen moving around, looking for the spot where she once lay with her husband. More recently, a witness using the alleyway connecting Lovat Lane to St Mary at Hill reported the air thick with cigarette smoke and hearing a cough, though the alley was deserted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stormy Beast</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - St Michael's Church, Cornhill<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Creature visit date unknown, Father Ellison spotted 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> In the dim and distance past, St Michael's was visited by a creature during a storm - it entered via the south window, and left claw marks in the stone. During the late twentieth century, a clergyman was seen moving towards the vestry, only to quickly vanish when approached. He was identified as Rev John Henry Joshua Ellison, a former rector who had died a few years previous.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smothering Action</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, as of 2009, armour on display in the White Tower<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The area in which Henry VIII's armour is normally stored has an evil reputation; several stories exist of guards being either attacked or feeling threatened by an unknown force. The armour is now on display in the White Tower, and it is not known whether this force has relocated.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lord Dudley</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Beauchamp Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Lord Guildford Dudley is said to appear in this tower, tears running down his face. There are also reports of light poltergeist activity and soft gasps. One set of battlements connected to this tower are known as Elizabeth's Walk and haunted by the ghost of a Cavalier.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec31006.jpg' class="w3-card" title='The Bloody Tower at the Tower of London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Bloody Tower at the Tower of London.</small></span>                      <p><h4><span class="w3-border-bottom">The Two Princes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Bloody Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Princes possibly heard in 1990, Raleigh unknown, female phantom seen August 1970<br>
              <span class="w3-border-bottom">Further Comments:</span> Believed to have been killed on the instruction of Richard III, the two young princes have been seen holding hands, cowering in various rooms of the tower. In 1990, two Coldstream Guards heard two young children giggling just outside the tower, accompanied by a bouncing sound. The Bloody Tower is also home to the ghost of Sir Walter Raleigh, who wanders around the area and along the battlements now known as Raleigh's Walk. One visitor reported seeing a phantom woman in a long black dress standing by a window; the witness had time to see the figure was wearing a white cap and a gold pendant before the figure melted away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Guy Fawkes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Council Chamber<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Post-1606<br>
              <span class="w3-border-bottom">Further Comments:</span> For years after his execution, Fawkes' screams and cries could be heard coming from the room where he was prepared for the end.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Mount</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, general area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> NA<br>
              <span class="w3-border-bottom">Further Comments:</span> The Tower of London was once known as the White Mount, and it was said Bran the Blessed head was buried beneath the structure to ensure Britain was never invaded. One story says that Arthur removed the head, as he alone wished to be the country's saviour.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bear</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Jewel House (now known as Martin Tower)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 1816<br>
              <span class="w3-border-bottom">Further Comments:</span> After seeing a large bear approaching him, a guard tried to bayonet the creature; the weapon passed straight through the ghost, and it continued to move towards the soldier. The soldier collapsed and died a few days later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec37779.jpg' class="w3-card" title='The main entrance of the Tower of London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The main entrance of the Tower of London.</small></span>                      <p><h4><span class="w3-border-bottom">Stretcher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, main entrance<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> During Second World War<br>
              <span class="w3-border-bottom">Further Comments:</span> A guard on duty reported seeing a group of people in old uniforms walking towards the entrance to the Tower carrying a stretcher containing a headless body. The figures faded away just before they reached the soldier.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec32804.jpg' class="w3-card" title='Martin Tower at the Tower of London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Martin Tower at the Tower of London.</small></span>                      <p><h4><span class="w3-border-bottom">Bubbling Column</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Martin Tower<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> October 1817<br>
              <span class="w3-border-bottom">Further Comments:</span> Edmund Swifte, Keeper of the Crown Jewels, lived in the tower with his family. One Saturday as Edmund and his family sat down for dinner, they were terrified by a tall, strange column of what looked like liquid, which drifted through their room. Edmund wrote that it resembled a glass tube, about the width of his arm. It hovered in the air before moving behind Swifte's wife, who screamed it had tried to grab her. Swifte picked up a chair and lashed out at the column - the chair passed clean through without impact, although it did cause the strange entity to vanish.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Henry Percy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Martin Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> While Percy was released from the tower after being held there for sixteen years, his ghost has been reported along the battlements at Martin Tower. Some say he attempts to push visitors and guards down the steps by the tower.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pacing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Middle Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1977<br>
              <span class="w3-border-bottom">Further Comments:</span> Two decorators at work reported hearing footsteps walking on the roof above them, even though the tower was empty. They summoned help and searched the area, but no intruder could be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Queen's House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Date of Grey Lady unknown, medieval man seen 1970s, chanting heard 1978<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom grey lady who haunts the Queen's House is said to only make her presence known to women. A phantom man, thought to be medieval, has been reported drifting along corridors in the upper part of the building, and is also held accountable for the ghostly footsteps on a rear staircase. In 1978, a guest in the house reported hearing faint plainchant late at night, though later she found out that no one else had heard anything.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Sally Port<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1968<br>
              <span class="w3-border-bottom">Further Comments:</span> A Scots Guard was found quaking in this area, claiming that several ghostly figures had emerged from the Sally Gate and started to follow him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec37778.jpg' class="w3-card" title='Salt Tower at the Tower of London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Salt Tower at the Tower of London.</small></span>                      <p><h4><span class="w3-border-bottom">Lady Jane</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, Salt Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 February (reoccurring), but least seen 1957<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom of Lady Jane Grey once haunted this tower, though it is now over fifty years since last being spotted standing on the roof.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec37777.jpg' class="w3-card" title='St Peter ad Vincula church within the Tower of London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> St Peter ad Vincula church within the Tower of London.</small></span>                      <p><h4><span class="w3-border-bottom">Procession</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, St Peter ad Vincula church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The bodies of the executed were placed under the floor of this building during the Tutor period. In 1876, Queen Victoria instructed the flagstones be lifted and all bodies removed, identified, and given a correct burial - the remains over two hundred were discovered, though identification was nigh impossible. Soon after this, an officer on patrol reported looking into the church late at night and seeing many people in period costume apparently walking around, following a woman who looked like Anne Boleyn. After a few minutes, the scene faded away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec31001.jpg' class="w3-card" title='The Wakefield Tower within the Tower of London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Wakefield Tower within the Tower of London.</small></span>                      <p><h4><span class="w3-border-bottom">Henry VI</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, the Wakefield Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 May, midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered in the tower in 1471, Henry's spirit appears a few minutes before midnight, walking around the area in which he died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ec31003.jpg' class="w3-card" title='The White Tower within the Tower of London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The White Tower within the Tower of London.</small></span>                      <p><h4><span class="w3-border-bottom">Lady in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC3 - Tower of London, the White Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Perfume unknown, tapping on 27 May 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Her identity unknown, this ghostly woman's perfume is said to be overpowering, and has made many people gag around St John's Chapel. One visitor felt tapping on her shoulder, although could not see who could have been responsible. When they mentioned it to a guide, the guide said it could have been 'Daniel', who people have seen wearing a black cloak.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 30</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=30"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=30"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Greater London</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View Greater London records" style="width:100%" title="View Greater London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
