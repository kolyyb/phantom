


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Mermaid Related Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Mermaids & Men</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Merfolk from across the Isle</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/magic_aberystwyth.jpg' class="w3-card" title='An old magic lantern slide of Aberystwyth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old magic lantern slide of Aberystwyth.</small></span>                      <p><h4><span class="w3-border-bottom">Bathing Mermaid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth (Dyfed) - Cliffs near the town<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> July 1826<br>
              <span class="w3-border-bottom">Further Comments:</span> Twelve people watched a beautiful pale woman washing herself in the sea, with what appeared to be a black tail splashing around behind her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shape Changers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bantry (County Cork) - Bay cliffs<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The mermaids said to live in this area would be capable of changing their shape as they came on to land. They would also be seen sitting on the rocks play harps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Women with Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barra (Outer Hebrides) - Reef in Caolas Cumhan<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Crofter Colin Campbell raised his rifle to fire at what he thought was an otter eating a fish but stopped when he realised the creature was a mermaid holding a child. The creature dived into the sea after Campbell made a noise.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swan Eater</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burscough (Lancashire) - Martin Mere<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 2002<br>
              <span class="w3-border-bottom">Further Comments:</span> An unknown large creature was seen to attack several swans from beneath the water of the lake - it is believed to be a type of large catfish, which can grow up to sixteen feet in length. There has also been at least one mermaid sighting in the lake, and Sir Lancelot's body is supposed to be buried nearby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mer Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Calf of Man (Isle of Man) - Rocky area, exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1810<br>
              <span class="w3-border-bottom">Further Comments:</span> Three men from Douglas found two merchildren on the rocks. One was already dead, but they saved the second child and took it to their town. The creature was described as around 60.3 centimetres in length and brown in colour, although the scales around its tail were slightly violet, and the hair on its head light green.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shorted Armed Mermaid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Campbeltown (Argyll and Bute) - Kintyre Peninsula<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 October 1811<br>
              <span class="w3-border-bottom">Further Comments:</span> Watched by a farmer for two hours, this mermaid had a pale upper body and a red/grey tail covered in hair. Its face was human, with deep-set eyes and short neck. The creature combed the hair on its head with what looked like short, stubby arms before diving into the water and washing its torso.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/220705merman.jpg' class="w3-card" title='Wood engraving of a merman by the sea.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Wood engraving of a merman by the sea.</small></span>                      <p><h4><span class="w3-border-bottom">Teenager</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castlemartin (Dyfed) - Off the coast in the area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1782<br>
              <span class="w3-border-bottom">Further Comments:</span> Henry Reynolds spotted a merman off the coast which he initially believed to be a pale skinned teenager before noticing its brown tail which waved about. While the body and arms looked human, the arms and hands looked too short and thick, while the creature's nose was long and sharp. Reynolds went to find other witnesses, but by the time he returned, the merman had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mermaid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Child's Ercall (Shropshire) - Pool<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This mermaid was so upset at the way she was treated by onlookers, she drove to the bottom of the pool and never returned. It was thought that she guarded a mass of treasure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/mermaid.jpg' class="w3-card" title='A stylised cast-iron mermaid.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A stylised cast-iron mermaid.</small></span>                      <p><h4><span class="w3-border-bottom">Mermaid Curse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Colmonell (Ayrshire) - Knockdolian Castle<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The mistress of the castle ordered the destruction of a black rock which stood just a short distance away, to prevent a mermaid sitting upon it and singing. As revenge, the mermaid cursed the family, ensuring they all died without leaving an heir for the estate.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Macphie's Captor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Colonsay (Argyll and Bute) - Unknown cave along the coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A mermaid kidnapped a man by the name of Macphie and held him in a cave along the island's shore. Macphie was given any item he wished for but remained unhappy until he managed to escape. The mermaid gave chase, but Macphie tossed his dog into the sea. Dog and mermaid fought until they both died from their injuries.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Llys Helig</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Conwy (Clwyd) - Conway Bay<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Llys Helig was a mythical palace, which was sunk to punish its evil owner. The ruins of the walls can sometimes be seen at low tide, though in recent years it has been discovered that the rocks are a natural formation. Mermaids have also been seen in these waters, and one was said to have cursed the town of Conway after she was washed up on the beach and no one offered to help her back into the sea.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/0503mermaid.gif' class="w3-card" title='An old woodcut of a mermaid swimming past a sailing ship.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a mermaid swimming past a sailing ship.</small></span>                      <p><h4><span class="w3-border-bottom">Mermaids</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Deerness (Orkney) - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> These women of the water were seen dozens of times during the last twenty years of the nineteenth century. One was said to have been shot, but the body was not recovered.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Treasure Guardians</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Duncansby Head (Highland) - Cave system off coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A team of mermaids and mermen are reported to guard a hidden cave full of treasure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Captured Fisherman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dunnet Bay (Highland) - Underwater sea cave<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A good looking fisherman once caught the eye of a mermaid who fell in love with him - she kidnapped the fellow and held him in a secret cave under the sea, where he remains to this day...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wish Granter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eilean Anabuich (Outer Hebrides) - Coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local man caught a mermaid on the rocks, only releasing the creature after she agreed to grant three wishes. One of the wishes ensured the man became a skilled doctor, another granted the gift of prophecy, but the third wish, to become a great singer, apparently failed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Twitchers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eynhallow (Orkney) - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 July 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> Two birdwatchers that vanished without trace on the island after arriving with 86 other people were thought by locals to have been taken by merfolk. An extensive search by the coastguard could find no trace of the missing people.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Well Dweller</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fornham All Saints (Suffolk) - Well in the village (exact location unknown)<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> An old story, most likely told to prevent children playing too close to the well, said a mermaid waited in the water at the bottom. She was ready to drown children who touched the water's edge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mermaid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hayfield (Derbyshire) - Mermaid's Pool, near Kinder Reservoir<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Easter Sunday Eve at midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Anyone who sees the creature is said to gain immortality. One man who came here once a year lived to be 104 (so the story goes)!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Inchkeith-merman.jpg' class="w3-card" title='Carving of a merman in Chichester&#039;s St Mary&#039;s hospital.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Carving of a merman in Chichester&#039;s St Mary&#039;s hospital.</small></span>                      <p><h4><span class="w3-border-bottom">Sea Folk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Inchkeith (Fife) - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The area around the island was said to be home to many mermaids and kelpies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hybrid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Sanday (Orkney) - Exact area not known<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Folklorist Walter Traill Dennison recorded in 1893 that there were decedents of a union between a male selkie and a woman living on the island - you could tell who the offspring were because they had webbed hands and feet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Halibut-Tailed creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Yell (Shetland) - Thirty miles off the coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1833<br>
              <span class="w3-border-bottom">Further Comments:</span> Three (or six) fishermen claimed to have caught a ninety centimetre long mermaid while at sea. The creature had arms around 23 centimetres in length with webbed fingers, and while it had blue eyes and nostrils, no ears or chin could be seen. It possessed a tail similar to a halibut and had two fins on its shoulders. After listening to the creature wail for three hours, the fishermen threw it back into the sea.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mermaid?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kilconly Point (County Kerry) - Sea<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960-1962<br>
              <span class="w3-border-bottom">Further Comments:</span> A mermaid which was spotted several times in this area was described as a 'normal' woman, leading one to conclude that maybe she was..?</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bloodied mermaid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killone Lake (County Clare) - Waters of the lake<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Every forty years, exact date unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A mermaid who frequented the lake would often steal wine from the crypt beneath the church (or the cellar of Newhall, depending on the storyteller). She was stabbed (or shot) when caught but managed to drag herself back to the lake before dying. The waters are said to turn red with her blood every forty years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mermaid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kinlochbervie (Highland) - Craigmore<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century (or 1900?)<br>
              <span class="w3-border-bottom">Further Comments:</span> A report exists of a seven foot long mer-creature being seen here, bathing on the rocks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sailor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kinlochbervie (Highland) - Sandwood Bay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1953<br>
              <span class="w3-border-bottom">Further Comments:</span> Leaving no footprints as he crosses the sand, this bearded sailor is one of the many who lost their lives in the sea nearby. He is particularly noted for the large brass buttons on his jacket and for shouting 'All on this beach is mine!' at some visitors during the 1940s. A red-headed mermaid has also been reported in the area.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 60</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=60"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=60"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
