

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in the West Midlands, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/westmidlands.html">West Midlands</a> > West Midlands</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>West Midlands Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Baginton-Manor-House.jpg' class="w3-card" title='A burning manor house.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A burning manor house.</small></span>                      <p><h4><span class="w3-border-bottom">Burning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baginton - Manor House (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1900s onwards?<br>
              <span class="w3-border-bottom">Further Comments:</span> This manor house was consumed by fire around the start of the twentieth century. Ten years after the fire, a witness reported seeing a house burning where the manor had stood, and again another witness reported the same occurrence a few years later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baginton - Old Mill House hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom is said to be more likely heard than seen, her voice has been detected in the restaurant. The ghost is reputedly that of a woman murdered by her husband, and remains on this mortal plane looking for her child. Another story says the site became home to the ghost of a man murdered over a debt and would occasionally manifest on the staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Baby Killer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - 32 Coxwell Road, Ladywell<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1955<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity was blamed for killing a month old infant found suffocated. The family's four year old son claimed he witnessed a 'white dog' sitting on the child's face prior to the body being discovered. Soon after the death, the building became plagued by whisperings and unexplained tapings at started promptly every night at 10:20..</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Leon Salberg</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Alexandra Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Having managed the theatre for over twenty five years, Leon died within the building, and is thought to still walk here. The ghost of an unknown woman has also been spotted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Holt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Aston Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Holt and servant boy unknown, green lady in Summer 1991<br>
              <span class="w3-border-bottom">Further Comments:</span> A former Mrs Holt (or Halte) was suspected by her husband of having an affair - he locked her in the loft where she lived for several years before dying. Another version of the story says Miss Mary Holt became locked in the loft by her father after she tried to elope. Either way, the phantom grey or white lady haunts the upper floor. A ghostly servant boy is said to hang from a beam on the property (he committed suicide after being caught stealing), while finally a green woman, a former housekeeper named either Mrs Whitaker or Walker has been seen in various areas of the site. A member of staff spotted her in the nursery room, briefly mistaking her for a real person before realising the area had been closed off and that the figure wore seventeenth century clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Maroon Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Aston Park<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman with her hair tied in a bun and wearing a maroon dress could be seen sitting by the tennis court. When approached, the lady vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Birmingham & Midland Eye Centre, City Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> During the building's construction, CCTV picked up the image of a strange grey figure - dispatched security sent to the scene found no one. A ward sister was encountered along a top floor corridor, only visible from the knees upwards. Around the same time, a crew of workers found themselves locked in a room after investigating cries heard from within; as before, nothing could be found. Someone finally summoned a priest to bless the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Brightstone Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 03:00h (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Screams can be heard coming from an unknown point along this road in the early hours of the morning. Some people have also claimed to have seen a pale young boy in tears.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Engines</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Castle Vale housing estate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1987<br>
              <span class="w3-border-bottom">Further Comments:</span> The housing estate was partly built over the site of the Second World War Castle Bromwich Aerodrome, and during the 1970s there were several reports of ghostly airmen manifesting around the area. In 1987, one man reported being virtually deafened by the sound of an old aircraft engine that came out of a thick fog that surrounded his house. The following evening, the fog and the engine sound returned, but this time also reported by a mother and her children who lived two miles away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wilfred Barwick</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Coleshill Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February 1975<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Barwick, a local butcher murdered in 1780, is said to haunt the road where he was killed. The entity may have last been seen stepping out in front of a Securicor van, vanishing as the driver tried to talk to him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wine Smashing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Court Oak public house, Harborne<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Nicknamed Corky by visitors to the Court Oak, this phantom is said to smash bottles of cheap wine in the cellar. Corky is also said to manifest behind the counter, appearing in the form of a male in his sixties.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Overturned Cars</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Drews Lane (between 175 and the corner shop), Ward End<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Quoted in the Tarmac report on haunted roads, former Drews Lane resident Jackie said 'I used to hear cars that used to roll over while driving. Hundreds of cars have been heard and seen upside down with no reason or rhyme. There were a couple a week sometimes, nothing to explain it. There were never any other cars involved, just that when they came down our road, they would turn over.'</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Family</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Fountain Close<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A family of five is said to haunt this area - two boys aged 9 and 12, a 7 year old girl, and their parents. The mother is said to have died during childbirth, and the rest of the family died mysteriously soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Birmingham-General-area.jpg' class="w3-card" title='Falling stones are sometimes associated with poltergeists.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Falling stones are sometimes associated with poltergeists.</small></span>                      <p><h4><span class="w3-border-bottom">Raining Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - General area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 June 1858<br>
              <span class="w3-border-bottom">Further Comments:</span> Hundreds of thousands of little stones rained down on the city, smashing greenhouses and littering the streets.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vampire Attacks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Glen Park Road (Ward End) and Saltley, Small Heath and Alum Rock areas<br>
              <span class="w3-border-bottom">Type:</span> Vampire<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> A gentleman, described as black and in his twenties, bit another man walking along the street before pouncing on neighbours who came to the victim's aid. One woman present was said to have had a chunk bitten out of her hand. Police, however, stated that they had received no reports of such an incident and dismissed the story as an urban myth.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Orange Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Gunter Road, Erdington<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter 1970, 07:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> A young lad on his way to school spotted a glowing orange man. The entity's features were not clear, and it remained for around twenty seconds before vanishing. The figure is also said to have appeared along other streets in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jumping Grey Thing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Icknield street, Hockley<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2014<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple in their car watched as a grey humanoid creature jump across the road, kangaroo-like, although seemingly gaining momentum by swinging its arms. The entity stopped in the road and looked at the car before turning and moving back into the grounds of a cemetery.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Lolly</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Kang's Discount Store<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> There were supposedly several witnesses who watched as a lollypop floated up and moved itself out of this shop.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bangs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Key Hill Cemetery, Icknield Street, Hockley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Reported by Birmingham Investigators of the Unknown, activity in this location includes banging or clanking sounds, dark shadows, feelings of heaviness and sickness, and a lantern light moving across the rear of the cemetery.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadowy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Kings Norton Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> The dark shape of a man has been reported crossing the park. A swing in the playground is also said to rock itself at night, even though there is no wind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Small Pickup Truck</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - M6 near Birmingham<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Author Paul Devereux reported overtaking a phantom pickup truck along this stretch of motorway, apparently with no driver. The truck disappeared once Mr Devereux had finished overtaking.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mary Littleton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Maxstone Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Mary was pushed down the staircase here after a furious argument with her husband - her shade lingers on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Face</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Nokes'z Hair, Church Road, Northfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2009-2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Several incidents have been reported coming from this hairdressers, including the manifestation of a face in the darkness, appliances switching themselves on, objects being thrown, and a black mist forming at the rear of a room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Old Crown public house, Deritend<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 August 1999<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple staying here reported a strange white mist in their room. One witness felt compelled to move towards the window. Two ghosts are said to haunt the inn, though who or what form the second one takes is unknown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hazy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham - Outside Sarehole Mill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2006, morning<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness driving past the building in broad daylight one morning spotted the hazy figure of someone walking behind a lady on the street in front of the mill. The transparent figure caused distortion to the street behind it, like a mirage. The witness noted that the shape of the head was round and reminiscent of a helmet.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 160</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=160"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=160"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/westmidlands.html">Return to West Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="View West Midlands records" style="width:100%" title="View West Midlands  records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>East Midlands</h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
