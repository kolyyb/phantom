

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Hertfordshire Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Hertfordshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Hertfordshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Returning Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abbots Langley - Churchyard and Vicarage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This young girl, who died suddenly, rises from the grave once a year and returns to the vicarage. Opinions differ on whether she is a former vicar's daughter, or a maid mistreated by a vicar's wife.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abbots Langley - Private home built on site of Leavesden Hospital, Leavesden High Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> Certain newspapers in the national press reported that Manuel Almunia, goalkeeper for Arsenal, lived in a house haunted by a monk-like figure holding a candle. The building was constructed on the site of a former asylum.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Albury-Halls-Garden-Pond.jpg' class="w3-card" title='Entrances to Hell take many forms.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Entrances to Hell take many forms.</small></span>                      <p><h4><span class="w3-border-bottom">Bottomless Pit</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Albury - Halls Garden Pond near the church<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This small body of water is said to be a one way ticket to Hell for anyone foolish enough to try to swim to the bottom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/OI-237.gif' class="w3-card" title='An old woodcut of a flaming figure approaching a man.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a flaming figure approaching a man.</small></span>                      <p><h4><span class="w3-border-bottom">Devil's Bang</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldbury - Former castle<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil reportedly helped the owner of the castle perform alchemy on the site, before a chemical mishap blew them both to hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blind George</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anstey - Fields around the village, and Devil's Hole cave<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to still be playing his fiddle in tunnels under the village, Blind George ventured into the Devil's Hole cave with his trusted dog - the hound ran out, and was found with its hair burnt away, and George was never seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashwell - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This shadow of a shambling headless person reportedly stalks the churchyard here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman on the Stairs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aston - Private house along Bennington Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> One person reported a phantom lady who would appear at the top of the stairs or at end of a hallway, and open windows and doors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ayot St Lawrence - Brocket Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to have hanged himself in the fourteenth century, the monk has made several appearances to staff and visitors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lawrence of Arabia</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ayot St Lawrence - Shaw's Corner<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Lawrence is thought to have enjoyed some of his time out of the army in this building, though there has been no clear identification made that the haunting entity was him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baldock - Hinxworth Place<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Stormy autumn nights<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to relate to a particularly ironic incident when a maid accidentally killed a child who had dressed up as a ghost to scare her, the infant's screams can now be heard echoing down through the ages.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Batford - Lower Luton Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> March and May 2014<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver had to swerve to avoid an older man standing in the road. When the driver looked for the figure, he had gone. A couple of months later a similar figure was seen again, this time by two witnesses, who said the man had a bandage on his head and held an old fashioned rifle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Berkhamsted-General-area.jpg' class="w3-card" title='Snakes, storms and serpents.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Snakes, storms and serpents.</small></span>                      <p><h4><span class="w3-border-bottom">Banished Wyrms</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berkhamsted - General area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Whilst visiting Britain, St Paul, was supposed to have banished forever all snakes, dragons, and thunderstorms. One out of three is not too bad...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berkhamsted - Railway bridge<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 September 2020, 04:45h<br>
              <span class="w3-border-bottom">Further Comments:</span> A railway worker working on the tracks heard a girl's voice say 'hello' and what sounded like 'can you...'. He peered into the trees next to the track, believing the voice could have come from nearby houses, but when looking back to the tracks spotted a young girl with her back turned a couple of metres ahead. The startled worker stumbled, and when he looked back up, the girl had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Biker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berkhamsted - Road passing Berkhamsted golf club, before left turn to castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1976, around 23:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> On a clear night with no other traffic on the road, a driver caught up with a Honda 50 type motorbike. The outline of the bike and rider were visible, and the driver maintained 25 yards (23 metres) between car and bike. Passing Berkhamsted golf club, the bike disappeared, 'as if someone had turned off a light'. The driver thought the biker may have come off into a ditch so stopped to look - there was no ditch nor any turnings the bike could have taken.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop's Stortford - Boar's Head public house, Water Lane and St Michael's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1922<br>
              <span class="w3-border-bottom">Further Comments:</span> Locals say that this shade travels through the town, stopping at a couple of watering holes, before continuing to the church. The phantom drifted silently passed a mother and child along Water Lane in 1922, heading towards the church. Both witnesses fled.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty Shape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop's Stortford - George Inn, North Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This spook is said to be a woman stabbed to death in room 27.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Door Locker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop's Stortford - Private house along Mary Park Gardens<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A family living along this street reported an unpleasant entity that would lock the bathroom door from the inside and fill the house with an oppressive atmosphere.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop's Stortford - Saplings Independent Nursery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> It is reported that a young child once burnt to death on the site, and that the shade of the child remains. One witness described the infant as a blonde haired girl.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bishops-Stortford-Town-Centre.jpg' class="w3-card" title='A running ghostly figure.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A running ghostly figure.</small></span>                      <p><h4><span class="w3-border-bottom">Screamer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop's Stortford - Town Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980?<br>
              <span class="w3-border-bottom">Further Comments:</span> The centre of town is reputed to be haunted by a screaming figure who runs through the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Officer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop's Stortford - Windmill House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The grounds of this home are haunted by an officer accidentally shot and killed on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monkey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borehamwood - Elstree, Big Brother House<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A few 'strange' reports have come out of the BB house, including a 'phantom' monkey seen on the ceiling, a moving plate, and a ghost seen by Imogen, which may have been another housemate.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Make Up Artist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borehamwood - Gate Studio<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> This person committed suicide at the studio and has subsequently been observed around the cinema screen. A builder who fell to his death on the site has also been reported. It is unclear which ghost was seen dressed in 1920s clothing by two female witnesses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Twilight's Last Gleaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bovingdon - Box Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A strange gleaming entity is reputed to haunt Box Lane.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Needless Bloodshed</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bovingdon - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Some say that part of the churchyard has been desecrated by a great deal of blood, and the dead are now restless.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Summoning Snooks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boxmoor - Grave of Robert Snooks<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Robert, the last man in England to be executed for highway robbery, is said to briefly return if his grave is circled and his name called out.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 154</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=154"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=154"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
