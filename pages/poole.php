
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Poole Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Poole</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Poole Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor7513.jpg' class="w3-card" title='High Street, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> High Street, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Mr Jenkins</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - 71 High Street (original shop no longer stands)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1964 and 1976<br>
              <span class="w3-border-bottom">Further Comments:</span> Since the old shop was replaced by a new building, the phantom of Mr Jenkins has not been seen. He was regarded as a rude ghost, pushing past people as he descended the staircase. Witnesses who spotted him said Jenkins was a young man wearing a suit with a high white collar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Baiter Park (former site of an isolation hospital)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Possible late 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly figure of a small thin child standing by a bench was spotted by one witness. The entity and entity locked eyes before the ghost faded away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor13369.jpg' class="w3-card" title='Bowling Green Alley, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bowling Green Alley, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Dark Shape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Bowling Green Alley<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 February 2019<br>
              <span class="w3-border-bottom">Further Comments:</span> After saying to his girlfriend 'I wonder if we'll see any ghosts', Mark Goddard photographed a black shape along an alley. He believed it showed a cloaked old lady, although it is more likely to be roughly hewn brickwork on the corner of a building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor7510.jpg' class="w3-card" title='Byngley House, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Byngley House, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Oppression</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Byngley House, Market Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Some visitors to the building have reported a strange repressive feeling or the sensation of being suffocated in one upstairs bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor6499a.jpg' class="w3-card" title='Canford Heath, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Canford Heath, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Cat Calls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Canford Heath<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 August 2005 (cat heard)<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman with her elderly mother crossing the heath heard a growling noise followed by a loud roar from behind bushes. As they ran off, the roar was heard again. A cyclist and her son have also reported seeing a ghostly woman wearing a grey Victorian dress on the heath, close to the bomb crater used by mountain bikers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor2053.jpg' class="w3-card" title='Crown Hotel, Market Street, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Crown Hotel, Market Street, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Screaming Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Crown Hotel, Market Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Piano in 1989, other sightings unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two children were once locked up in this building by a parent too embarrassed to let them run around and play in front of other people. The ghostly children now let their protests be heard in the form of crying and yelling. The tinkling of a piano has also been heard in the hotel, as has the sound of a body being dragged around in the upper part of the structure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor8982.jpg' class="w3-card" title='Customs House, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Customs House, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Carter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Customs House (currently a restaurant)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Carter was a gang member who sold several of his smuggling friends out - when the remaining members discovered Carter's betrayal, they murdered him. Carter is now thought to haunt the upper part of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Slamming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Former mill and forge on the quay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This old building is thought to have once been used for smuggling, and a hidden tunnel exists connecting it to St James Church. The strange sounds and the opening and closing of doors late at night may relate to the illegal activities that once occurred here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor7511.jpg' class="w3-card" title='Guildhall, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Guildhall, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Clerk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Guildhall (former museum)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1991<br>
              <span class="w3-border-bottom">Further Comments:</span> A clerk who hanged himself during a state of depression in the guildhall is thought to linger on and is sometimes heard walking around the upper regions of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Whistling</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Hamworthy House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This whistling was said to come from the ghost of a man shot dead after he was found in the arms of another man's wife (she was also shot and killed).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor9593.jpg' class="w3-card" title='Poole Harbour.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Poole Harbour.</small></span>                      <p><h4><span class="w3-border-bottom">Ringing Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Harbour waters<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, but said to occur at night<br>
              <span class="w3-border-bottom">Further Comments:</span> Eight bells were lost, as were the entire crew, when a storm sunk the ship carrying them. The bells are still said to peal at night, together with the wails of the drowned sailors. The harbour is also named as one of many places where Arthur threw Excalibur.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor11792.jpg' class="w3-card" title='High Street, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> High Street, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Young Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> Heading towards the quay, a witness spotted a young boy wearing loose fitting knee length trousers and a tweed cap run past. When the witness reached the place where the boy ran, the route was blocked by a high brick wall with no possible exit.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elsie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Hospital and Grey Towers Hall (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> The hospital here had a reputation as being haunted by a ghost named Elsie.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor3626.jpg' class="w3-card" title='Jolliffe House, West Street, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Jolliffe House, West Street, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Captain Jolliffe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Jolliffe House, West Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2001<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly presence is said to tease the cleaning staff by leaving handprints in freshly polished places and turning equipment on and off. Footsteps have been heard in empty parts of the building. A former occupier at the building, who lived in the wing no longer standing, reported feeling a silhouette of a face that manifested on a bedroom wall, and a hand that tugged at her ankles.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor7515.jpg' class="w3-card" title='King Charles Inn, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> King Charles Inn, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Young Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - King Charles Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1984<br>
              <span class="w3-border-bottom">Further Comments:</span> Disembodied footsteps were heard by staff and customer alike. One person detected a young female voice. Items have also been known to disappear and electrical equipment malfunction.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor13370.jpg' class="w3-card" title='Lighthouse Arts Centre, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Lighthouse Arts Centre, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Older Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Lighthouse Arts Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s?<br>
              <span class="w3-border-bottom">Further Comments:</span> A few ghostly figures are said to haunt this arts venue, including an older woman sitting in the cinema, a man in t-shirt and jeans standing close to Sherling Studio, a Victorian Lady who haunts the studio, and a woman who hums a tune in a dressing room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mover</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Private house along King's Avenue<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s - present<br>
              <span class="w3-border-bottom">Further Comments:</span> This property was believed by neighbours to be haunted for many years. When finally sold to a woman who did not know the area, she reported finding ornaments and other small items that she placed on shelves to be on the floor the following morning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor9500.jpg' class="w3-card" title='Market Street, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Market Street, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Children Playing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Private residence, Market Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1972<br>
              <span class="w3-border-bottom">Further Comments:</span> A babysitter, watching over a one year old and a three year old, was disturbed several times by the sound of children talking and moving around on the floor above, and of something heavy being dragged across the floor. Each time she investigated, the sounds stopped as she reached the top of the stairs, and the children she was minding were asleep.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Private residence, Mossley Avenue<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1957<br>
              <span class="w3-border-bottom">Further Comments:</span> A poltergeist that the family christened George was deemed by investigators to be nothing more than rainwater running under the house and expanding air pockets caused by opening and closing doors. This did not immediately explain why windows opened unaided and taps and lights which would be turned on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor2054.jpg' class="w3-card" title='Scaplen&#039;s Court, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Scaplen&#039;s Court, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Agnes Beard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - Scaplen's Court<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Agnes unknown, CCTV September 2008, 'feeling' Summer 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> It is believed that a maid, Agnes Beard, and her mistress were both murdered during a robbery here in the late sixteenth century. The man accused of the crime was found not guilty (even though most people thought that he did carry out the killings), and Agnes still protests the miscarriage of justice by making her presence felt in the building. A lone male figure also haunts the building, though is said to be non-hostile. CCTV apparently caught a ghostly figure wearing a bowler hat which remained on the screens for just over two hours, and a few years later, a visitor to this site had a suffocating feeling in one room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor7512.jpg' class="w3-card" title='United Reformed Church, Poole.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> United Reformed Church, Poole.</small></span>                      <p><h4><span class="w3-border-bottom">Lamps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poole - United Reformed Church, Skinner Street<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It has been said that sometimes one can see box pews and brass lamps within the church, even though the items were removed in the nineteenth century.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 21 of 21</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
