


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Royal Ghosts, Folklore and Haunted Palaces from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Royal Ghosts & Hauntings</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Haunted Palaces and Royal Hauntings</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Kittie Rankie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergeldie (Aberdeenshire) - Abergeldie Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Burnt as a witch at the top of a nearby hill, Kittie now haunts the castle and the surrounding land.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Groom carrying Lantern</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Althorp (Northamptonshire) - Althorp Park<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1820s / 1830s (Groom), others throughout the twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Several entities have been observed here. Seen only the once (though the exact date is not known), the horse groom was seen standing over a bed, a bright lamp in one hand - this was exactly two weeks after his death. Another ghost, that of a child, was seen several times prior to World War I, and a local gentleman named Jack Spencer (who died in 1975) was seen attending a party in the building a few months after his death. Finally, an old servant has been reported visiting bedrooms at night if lights have been left on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/henryviiia.jpg' class="w3-card" title='Henry the Eighth, recreated at Madame Tussauds.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Henry the Eighth, recreated at Madame Tussauds.</small></span>                      <p><h4><span class="w3-border-bottom">Limping Priest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amersham (Buckinghamshire) - Chenies Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom figure was seen dragging one of its feet while traversing the floor, though it was more frequently heard than seen. Some have named the figure as Henry VIII, rather than a priest, who stayed here with Anne Boleyn and, several years later, Catherine Howard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Princess Marama</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anstruther (Fife) - Johnson Lodge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This royal lady, said to be from a Pacific Isle, continues to walk around the building she spent the last years of her life in.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mary</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Askrigg (Yorkshire) - Nappa Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1878<br>
              <span class="w3-border-bottom">Further Comments:</span> This location is another haunt of the spiritually busy Mary Queen of Scots, who has been seen here wearing a black velvet dress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Duke of Suffolk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Astley (Warwickshire) - Astley Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Father of Lady Jane, Henry Grey, Duke of Suffolk, had his head lopped off and now haunts this building (headless, of course).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Thomas_Boleyn.jpg' class="w3-card" title='Thomas Boleyn, as pictured on his tomb.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Thomas Boleyn, as pictured on his tomb.</small></span>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylsham (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belaugh (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-4.jpg' class="w3-card" title='An old postcard of Berkeley Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Berkeley Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Edward II Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berkeley (Gloucestershire) - Berkeley Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered on orders of his wife Queen Isabella (her fate is documented elsewhere in this database), the anniversary of the King's death is marked by his screams echoing around the castle. The skin of an animal was once on display near the entrance; a local legend said it belonged to a giant toad discovered in the dungeon, although it was more likely to belong to a seal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Head</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Billingham (Isle of Wight) - Billingham Manor<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> A free floating decapitated head was seen around the manor house, coinciding with the execution of someone at Parkhurst Prison. The ghost of Charles I is also reported to haunt the manor, as is a monk seen standing along a path near the coach house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anne Boleyn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blickling (Norfolk) - Blickling Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> To mark the anniversary of her death, a coach pulled by four headless horses pulls up on the driveway, with a decapitated Anne on a seat, her head in her lap. After Anne's headless white ghost arrives, it is said then to climb out, before inspecting each room of the Hall (the place of her birth and childhood). On the same date, Sir Thomas Boleyn is said to drive a team of headless horses in this area, cursed to cross twelve Norfolk bridges (including those at Aylsham, Coltishall and Wroxham).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Duelling Knights with Fiery Swords</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blofield (Norfolk) - Path leading from Blofield church to St Michael's at Braydeston<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Returning to this realm once a year, the knights are said to be Sir Thomas Boleyn (father of Anne) and Sir Thomas Paston. Their shades battle with burning swords, and Lady Anne watches from a nearby carriage, pulled by headless horses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Disguised Queen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borthwick (Lothian) - Borthwick Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Mary Queen of Scots left the castle in 1567 dressed as a boy to escape her enemies - her ghost here remains clothed in this way. Another phantom in the castle has been named as Ann Grant, a servant murdered after falling pregnant with the lord's child.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/a_ladyjgrey.jpg' class="w3-card" title='A painting of Lady Jane Grey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A painting of Lady Jane Grey.</small></span>                      <p><h4><span class="w3-border-bottom">Lady Jane Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradgate (Leicestershire) - Bradgate Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Around Christmas (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Raised here as a child, Lady Jane's tormented shade now haunts both the mansion and the grounds surrounding the building. One ghost story says that she arrives in a coach pulled by four black headless horses at the house on Christmas Eve.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Queen Elizabeth</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradwell Vale (Derbyshire) - Heading towards Hazelbadge Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Once yearly, though exact date not known<br>
              <span class="w3-border-bottom">Further Comments:</span> Travelling with a royal entourage, the ghost of Queen Elizabeth I heads towards the hall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Thomas Boleyn Drives Headless Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Breckles (Norfolk) - Breckles Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The father of Anne Boleyn is said to be cursed with driving his coach over forty (or eleven) Norfolk bridges. One of the passing points is Breckles Hall, where the spectre once scared a poacher to death (another version states that the poacher was taken by the Devil because of his constant law breaking). Locals say that anyone who views the coach is dragged down to hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">A Husband Away</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brecon Beacons (Dyfed) - Llwynywormwood, also known as Llwynywermod<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1870<br>
              <span class="w3-border-bottom">Further Comments:</span> In an estate now owned by the Prince of Wales, one former occupant claimed to have seen her husband in a bedroom, even though he was four hundred miles from the property at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Howard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridestone (Devon) - Royal Oak public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A coach carrying Lady Howard has been spotted just outside the public house, though it is more likely to just be heard by those nearby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Sus1544bton.jpg' class="w3-card" title='Brighton&#039;s Royal Pavilion, Sussex.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Brighton&#039;s Royal Pavilion, Sussex.</small></span>                      <p><h4><span class="w3-border-bottom">Martha Gunn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton (Sussex) - Royal Pavilion<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Between the two world wars (Gunn)<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of an older-looking grey lady was seen in the Royal Pavilion flitting around examining banquet tables. The man who witnessed the spectre gave chase but was unable to catch the entity. After looking through some old documents, the witness identified the ghost as Martha Gunn, a celebrated 'dipper' (an eighteenth century person who operated a small, wooden hut on wheels to take people down to the sea). The Prince Regent who would later become King George IV, is also reported to walk inside his former home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Maria Fitzherbert</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton (Sussex) - Steine House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says that this house was connected to the Royal Pavilion by way of secret tunnel, to ensure Maria Fitzherbert could secretly meet George, Prince of Wales (later George IV). Maria is reputed to continue haunt the site; a female form wearing a long flowing dress is said to vanish into a wall along the basement corridor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Catherine of Aragon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buckden (Northamptonshire) - Buckden Palace<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Catherine is still reported to make the odd appearance in a room by the chapel.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Thomas</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burgh (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven (or twelve, or forty) bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mary</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buxton (Derbyshire) - Old Hall Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Mary Queen of Scots stayed in room 26 prior to her execution. Some believe her presence is still felt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buxton (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor95a.jpg' class="w3-card" title='Castle Rising, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Castle Rising, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">Queen Isabella's Crazy Laughter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castle Rising (Norfolk) - Keep area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Cold winter nights (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Edward III held his mother here for twenty seven years - she was accused of assisting in the murder of her husband Edward II in 1328. Rumoured to have lost her sanity towards the end, people have heard her laughter and screams on cold winter nights. More recently, witnesses have reported the eerie sounds of children playing and singing nursery rhymes.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 145</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=145"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=145"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
