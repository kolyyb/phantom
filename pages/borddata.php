

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in the Scottish Borders, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Borders</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Borders Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pearlin Jean</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allanton - Allanbank Hall (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This female spook, named after its lacy dress, is said to have resisted several exorcism attempts while the building stood. One witness said he had seen the phantom sitting upon a gateway, her clothing covered in blood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Romans of Parade</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bonchester Bridge - A6088<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom Roman legion is said to march along this road, coming from the direction of Chesters.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stable Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carlops - Allan Ramsay Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Paranormal investigators who visited the hotel reported seeing the apparition of a stable boy, whilst another claimed to have had his bed shaken by a pair of unseen hands.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse-Like Monster</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cauldshiels Loch - Waters of the loch<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Late eighteenth / early nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir Walter Scott wrote that people he had known had encountered a horse-like monster in this loch.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dasher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cessford - Marlfield House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity, while invisible, can be felt as it moves quickly along the corridors of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Legs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coldingham - Houndwood House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Only the lower part of this ghost has been seen, striding around the building. Another entity known as 'Chappie' took the form of a woman clad in silk; after knocking on the front door, she entered and ascended the staircase before disappearing never to be seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Voices</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cranshaws - Cranshaws Parish Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> A photographer at work inside the church heard people walking towards the door and a child say 'Mum, I want to tell'. The photographer opened the door to let the people in, but there was no sign of anyone, and nothing more could be heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Brownie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cranshaws - Cranshaws Tower<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This friendly entity would help keep the building clean and tidy, though it left for good after a servant criticised the quality of its work.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Merlin's Grave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drumelzier - Thorn tree in a meadow close to the church<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> According to one legend, the mighty magician Merlin was killed by shepherds close to this spot and was promptly buried.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Alexander Hay</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Duns - Duns Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Hays was killed at Waterloo but returned home regardless. Another soldier, this time a young teenager, loiters around the building that was once his former barracks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Edwardian Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Duns - Manderston House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This misty figure is believed to be the wife of Sir James Miller.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Puma</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Earlston - General area<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1983<br>
              <span class="w3-border-bottom">Further Comments:</span> Several witnesses reported seeing a large golden cat in the area, though it soon vanished without trace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rocking Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eddleston - Barony Castle Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former owner has been seen in a rocking chair, accompanied by the scent of brandy and cigars. A faceless woman in a blue dress has also been seen rushing through the corridors, while one room is said to be haunted by a lady who stands at the bottom of the bed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Piper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ednam - Nearby mound known as the Piper's Grave (aka Pict's Knowe)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Believing the fairies within the mound would teach him new tunes, a piper entered to seek enlightenment. He was never seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Poor Curse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ettrick - Ettrick Hall (no longer standing, was located top of Ettrick Water)<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 1700 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Legend has it that James Anderson demolished several cottages to clear space for the construction of the hall. Left homeless and penniless, the villagers cursed the building, saying that the structure would not stand for long. Indeed, all trace of the hall has long been removed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shellycoat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ettrick Waters - River<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Shellycoat was the name given to a playful water spirit which dressed in a shell covered jacket. He normally carried out his antics while hiding in the river, trying to lure the gullible forever closer to the cold waters.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Capybara</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eyemouth - A1, south of town<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2006 and June 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> This large rodent, not native to the UK, was spotted in 2006, sitting in the middle of the A1. Another creature, or possibly the same one, was spotted three years later at Reston.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eyemouth - Gunsgreen House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The house, at one time, was said to be haunted by the apparition of a woman in a grey dress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fosterland Burn - Close to a body of water<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer, midnight, pre twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> It was said that a fairy piper and singers would emerge from the burn and perform at midnight. One local farmer claimed he would listen to them during the summer months. The fairies could be more malicious and on one occasion tired (and failed) to kidnap a baby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lucy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galashiels - Binniemyre Guest House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> Known as Lucy, the friendly ghost of a woman in a white dress who very occasionally makes an appearance has been seen throughout the house. Some claim Lucy was jilted at the altar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hounds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galashiels - Buckholm Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Heard chasing the soul of the evil Laird of Buckholm on the anniversary of his death, these hounds are reported to normally walk the dungeon located within the tower.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Raining Worms</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galashiels - Galashiels Academy<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> Dozens of worms rained down from a cloudless sky resulting in a class and their teacher running for cover. Around 120 worms were recovered from an artificial football pitch and tennis courts after the incident.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Greenlaw Dean - Where two small bodies of water meet<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The fairies would meet and make merry at this site, where they had access to an ample supply of water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">William of Soulis</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hawick - Hermitage Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Every seven years, last heard 1885 (exact date unknown) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered by peasants who had grown tired of his wicked ways, William now returns to rendezvous with a wizard deep under the castle ruins. It is said a phantom Mary Queen of Scots haunts the building, though there is no evidence she ever visited. Another entity is also reported here - starved to death in the dungeon, the ghost of Sir Alex Ramsey now walks the castle grounds. In addition to the ghosts, some believe there is hidden treasure under the ruins of the castle, but the Devil watches over it - when people have tried to dig for the hoard in the past, violent storms have driven them away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Devil and his Mum</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Helmsdale - Hellmuir Loch (aka Hellmoor Loch?)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A cave by the loch was once home to the devil and his mother, although Old Nick did kill his mum after she slept in and failed to make his porridge for breakfast.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 69</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=69"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=69"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
