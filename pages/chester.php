
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Chester Ghosts and Other Strangeness from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northwest.html">North West</a> > Chester</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Chester Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bangs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Barlow's public house, aka Ye Old Vaults, 28 Bridge Street (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-2002<br>
              <span class="w3-border-bottom">Further Comments:</span> Blamed on a former landlord, the haunting here consisted mainly of banging sounds and loud groans.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Chester-Bear-and-Billet-Inn.jpg' class="w3-card" title='An old photograph of the Bear and Billet Inn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old photograph of the Bear and Billet Inn.</small></span>                      <p><h4><span class="w3-border-bottom">Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Bear and Billet Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom maid, who died of starvation after being locked in a room, is reported to haunt the staircase, and appears only to men. Mild poltergeist activity is also reported at the site, and strange screeching noises have been picked up on a telephone answering machine.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Henrietta</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Blue Bell Inn (currently East Glory Restaurant)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Henrietta waited for her Royalist lover to return from Battle of Rowton Moor, but soon discovered he had been killed. She committed suicide in the cellar. Her phantom is said to emerge from the cellar and walk to an upstairs window to wait for her lovers return.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Face</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Bombay Palace restaurant, Upper Northgate Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> December 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Co-owner of the restaurant Aaron Ali claimed to have photographed a strange face in the building after hearing banging on a door. However, the face on the photograph does look very similar to one on a ghost creating mobile phone app.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dry Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Chester Road / Butterbache, Huntington<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1991, a Sunday with heavy rain<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking home soaked after a paper round, a teenage boy spotted a blonde haired man wearing a white shirt, grey trousers and black boots standing by a gate leading to a cycle path. The teenager passed the man, and turned to have another look at his boots, only to find the figure had vanished. After the event, the paperboy realised that the man was completely dry, despite the heavy rain.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Silky</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Chirton Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former mistress of a Duke of Argyll, this white woman has yet to leave the bedroom of this house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sliding Ashtrays</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Coach and Horses Public House, Northgate Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Regular drinkers at the pub have seen ashtrays move themselves across tabletops and strange blue light flash from the walls.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Daisy and Tom Toy Shop (no longer open), Watergate Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2003-2006<br>
              <span class="w3-border-bottom">Further Comments:</span> Former staff from this store reported footsteps, the smell of pipe tobacco, something which rattled the heavy fire door, and a stock room which was blocked by a heavy table from the inside.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - George and Dragon public house, 1 Liverpool Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> These phantom footfalls can be heard as they take a path that now leads through several partitioned walls - it is believed to be a either a single Roman centurion on guard duty, or several soldiers who march from the front of the cellar to the back.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Romans</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Golden Eagle public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It is reported that the pub was built on a former Roman camp, which is why twenty Roman soldiers and an officer have been seen marching through a wall in the cellar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Landlord</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Marlbororough Arms<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1885<br>
              <span class="w3-border-bottom">Further Comments:</span> The misspelling of the pub signage is said to be due to a nineteenth century sign writer encountering the ghost of a former landlord who slit his own throat; in the worker's haste to complete the job, the man failed to spot his typographical error. Since this time, the phantom landlord has been heard gurgling in the basement.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Chester-Old-City-Hospital.jpg' class="w3-card" title='Ghost in a brown suit.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ghost in a brown suit.</small></span>                      <p><h4><span class="w3-border-bottom">Moving Beds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Old City Hospital (closed 1991)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1991 (noises) and man in brown in 1976<br>
              <span class="w3-border-bottom">Further Comments:</span> A former member of staff at the site and a colleague heard beds being moved in the ward above them, then realised the floor above was closed and empty. Another time the worker and another person heard voices and footsteps emerging from another empty ward. A man dressed in a brown suit was seen visiting his mother in 1976, although this woman later said her son had died years earlier.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jack</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Old Fire Station Restaurant, Northdate Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> Sporting a beard, this phantom fireman wearing a brass helmet was known only as Jack. A fireman who watched the figure sitting on a ladder was so shocked he rang the emergency bell to summon help. Even though the phantom had vanished by the time other people arrived on the scene, the witness quit his job soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Blue Overalls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Old lead steel works (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2002<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghost, said to be a workman who died after being hit by a train, may never be seen again: the area he haunted was scheduled for demolition and new flats will be built on the former works site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Meowing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Overpool Road, near Charter Crescent junction<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Malcolm Pennicard and his car featured in the news in July after reporting a ghostly meowing coming from his car whenever he drove along Overpool Road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Herbert & Charlie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Pepper Street garage (no longer operational, was based next to what is now Habitat)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> These two phantom monks were spotted several times by staff working at the garage shortly after it began opening 24 hours a day.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Cellar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Pied Bull public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The man in the cellar of this pub is said to date from the seventeenth century, while the upper part of the building is haunted by a Victorian-era maid.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Queen Hotel, Station Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, possibly 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> In an area thought to be a former nursery, people are said to have their hair pulled by a phantom female child.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-119.jpg' class="w3-card" title='An old postcard showing the River Dee.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing the River Dee.</small></span>                      <p><h4><span class="w3-border-bottom">Jenny</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - River Dee, area known as The Groves<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Jenny is said to wait just beneath the waters of the river, waiting for an unsuspecting man to grab and drown. A Viking warrior is also reputed to haunt the same area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Legionnaire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Roman Walls<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This trooper walks along the walls that once protected his garrison. One legend identifies him as an officer who fell in love with a Celtic girl; one fateful day he left his post to meet her, only to let Celtic warriors enter the city and cause chaos. Some have also reported a Cavalier in the same area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Squeaking Cycle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Saltney Junction railway station, area of<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early winter mornings (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> An old man on a squeaky bike haunt the area around this old station - it is thought that he hanged himself in a shed here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-19.jpg' class="w3-card" title='An old postcard showing St John&#039;s Cathedral in Chester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing St John&#039;s Cathedral in Chester.</small></span>                      <p><h4><span class="w3-border-bottom">Grey Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - St John's Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s, and 2003?<br>
              <span class="w3-border-bottom">Further Comments:</span> Heard talking in old English, this figure traditionally walks around the cathedral and made several appearances during the 1970s. On Halloween in 2003 a group of children approached the monk thinking he was a man dressed up for the occasion. When a bag was held out at him and one of the children asked, 'trick or treat', he replied 'I'll have a treat' before sticking his head into the container and eating the contents (wrappers and all).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elizabeth Warburton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Stanley Palace<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Several ghosts are said to reside at the building, including Elizabeth Warburton, a former owner who has been spotted walking through a wall, and a man in Tudor clothing. Other entities observed consist of a Second World War Officer and a grey haired woman sitting by the piano, while bangs, children crying, and disembodied footsteps have been heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in a Bonnet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - Taxi Office<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing a white nightgown and bonnet, this phantom was observed walking through a locked door. The witness questioned their sanity before being told that others had seen the entity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sighs of Criminals</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester - The Bridge of Sighs, near Northgate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A man cycling to an appointment along Northgate Street looked up at the bridge and spotted two or three people dressed in old fashioned clothing. They then vanished, leaving the man rather surprised. Prisoners being led to church prior to execution would cross this bridge, and it is believed their sighs can still be heard.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 30</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=30"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=30"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northwest.html">Return to North West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
