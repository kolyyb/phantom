
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Manchester Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northwest.html">North West</a> > Manchester</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Manchester Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - 16 Wardle Brook Avenue (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Former home of the Moors Murderers, neighbours once reported the sounds of children crying, months after the pair of killers had been sentenced to imprisonment.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Potter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - 35 Northern Drive, Collyhurst (no longer standing - area redeveloped)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1972<br>
              <span class="w3-border-bottom">Further Comments:</span> Two children were taken to hospital suffering from shock after seeing the ghost of Mrs Potter, a widow murdered in the flat two years previous. The presence told the girls to 'wait for me in this room' before touching one of them on the mouth.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nothing Moving</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - 44 Penny Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Owners of this establishment called the police when they heard lots of movement in the building when it should have been empty - after a long search, no one was found, and nothing was either moved or removed. The strange sounds continued for several weeks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bob Tailed Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Area around the canal near the Pomona Public House, Old Trafford<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Anglers fishing in this area reported a cat the size of a cocker spaniel which often appeared at day and night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figures in Jumpsuits</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Banks of the River Mersey, south Manchester<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 August 1979, 21:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking home with her two daughters, Lynda Jones and the children watched a twenty metre long orange/pink oval craft drop into a field. When they approached to take a better look, the ufo had changed to a grey colour and was hovering sixty centimetres above the ground. They all ran away after one daughter started screaming, and in later hypnotherapy Lynda recalled being abducted by almost human figures in jumpsuits.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Barlow and Others</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Barlow Hall, Chorlton-cum-Hardy<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mostly unknown, but servant on clear moonless nights (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Several ghosts were reputed to haunt the hall. Saint Ambrose Barlow is said to walk the upper part of the hall, as is a (sometimes headless) woman. A servant who drowned in the lake after poorly executed practical joke went wrong sometimes returns to run towards the Mersey. A poltergeist-like entity carries coal and moves other items between rooms.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Bootle Street Police Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> Footsteps have been heard by officers on the nightshift and an old timecard machine occasionally clangs as people walk past. A former female prisoner is said to haunt the corridor outside her cell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4094.jpg' class="w3-card" title='Brannigans nightclub, Manchester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Brannigans nightclub, Manchester.</small></span>                      <p><h4><span class="w3-border-bottom">Pusher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Brannigans nightclub, Peter's Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> This invisible phantom causes trouble behind the bar, while other staff say that the force has tried to push them down staircases within the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fanny</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1840s?<br>
              <span class="w3-border-bottom">Further Comments:</span> A man spotted his sister Fanny standing in the nave of the building - he was quite surprised, as he thought his sister to be many miles away. He called out to the woman, who started to walk but vanished suddenly. The following day, the man received word that Fanny had died in an accident, the timing of which matched his sighting of her in the cathedral.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Cathedral area, & bridge over the Irwell towards Salford<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1825<br>
              <span class="w3-border-bottom">Further Comments:</span> A tradesman spotted the spectral dog outside the cathedral and the creature jumped up at him. The phantom creature was finally exorcised under the bridge over the Irwell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Baby</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Cheetham, private house (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1964<br>
              <span class="w3-border-bottom">Further Comments:</span> The house was briefly home to phantom cries, sounding like those of a child, and the apparitions of an older lady and a more sinister black figure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Dee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Chetham School, Audit Room<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1595<br>
              <span class="w3-border-bottom">Further Comments:</span> Doctor Dee worked from this room, and a scorch mark on a piece of wood is reputed to have been left after Dee was visited by the Devil.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Church Inn, Prestwich<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom monk who haunts the cellar under this public house occasionally cries out 'Hello!' to gain attention.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bill Benny</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Cloisters Nightclub, Oxford Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> The former owner of the club, Bill Benny was said to have appeared on CCTV a few years after his death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Two Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Entrance alleyway to Chorlton Park, Chorlton-Cum-Hardy<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa March, 1979<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness sitting near the entrance to the park with their cousin watched two young twins in old fashioned school uniforms whistling and walking towards them. One of the twins smiled at the witness just prior to both siblings walking through the railings and disappearing. The witness screamed and fled the scene, although the cousin did not see the apparitions.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Green Train</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Fairfield Railway Station, Audenshaw<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: Autumn and winter (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to appear around sunset and visible from Booth Road, an old-looking green train with several carriages vanishes if anyone moves too close. A tunnel that has been filled in is said to be home to a disembodied voice that calls out the name Mary.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4645a.jpg' class="w3-card" title='Boggart Hole Clough, Manchester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Boggart Hole Clough, Manchester.</small></span>                      <p><h4><span class="w3-border-bottom">Hair Puller</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Farm (no longer standing) located at Boggart Hole Clough (was also known as Blackley Dell)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity possessed a split personality - it would sometimes help with the washing and cleaning, while other times throw items around, pull bedcovers from people's beds as they slept, and laugh loudly while banging around at night. When the family threatened to move out, the spirit told them it would go with them, so they stayed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4725.jpg' class="w3-card" title='Manchester&#039;s General Hospital.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Manchester&#039;s General Hospital.</small></span>                      <p><h4><span class="w3-border-bottom">Shadowy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - General Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Once seen standing by a television set, this shade is more likely to be blamed for flushing toilets in empty cubicles and throwing bed pans around.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Yellow Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Godley Green<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This large hound of ill fortune is yellow in colour - one story states it was once mistaken for a lion.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Golborne, Castle Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 1960<br>
              <span class="w3-border-bottom">Further Comments:</span> This female form causes trouble on the roads as she drifts out in front on oncoming traffic. Some witnesses say the entity is more like a monk than a woman and a cyclist who spotted the figure in the road in 1960 said the phantom figure was twice the height of a normal person.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Employee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Great Western Hotel, Moss Side<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure dressed in grey is often reported in the basement.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Alice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Heaton Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former resident of the hall, Alice still clings on to the last reminisce of her former life.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Paddy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - House at corner of Spath Road and Holme Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1957<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen in the garden of this house, this small black dog's grave is only a few metres away from the spot it haunts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Home Wrecker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - House in Levenshulme district<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1933<br>
              <span class="w3-border-bottom">Further Comments:</span> A poltergeist outbreak caused hundreds of pounds of damage in this house, pulling taps from the wall and flooding the site, and throwing household items out of the windows. A team of people were said to have laid the entity, although their methodology is not clear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Manchester-Manchester-Airport.jpg' class="w3-card" title='A ghostly old man.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly old man.</small></span>                      <p><h4><span class="w3-border-bottom">Old Bare Footed Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester - Manchester Airport<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Observed in what was once the barracks of the 613 RAF Squadron, this phantom old man has been reported by cleaners and late working staff. The occasional scream has also been heard coming from the building.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 60</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=60"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=60"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northwest.html">Return to North West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
