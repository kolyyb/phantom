
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Canterbury Ghosts and Strangeness from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southeast.html">South East England</a> > Canterbury</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Canterbury Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kent8661.jpg' class="w3-card" title='Bishop&#039;s Finger Inn, Canterbury.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bishop&#039;s Finger Inn, Canterbury.</small></span>                      <p><h4><span class="w3-border-bottom">Ellen Blean</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Bishop's Finger Inn, St Dunstan's Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Every Friday night (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Blean, a housekeeper to a Canon in Canterbury, poisoned him and his mistress when she discovered their affair. Blean disappeared soon after, and many years later her body was discovered walled up in a nearby building. The story goes that Blean's ghost haunts the street and appears in the inn itself, although the story's connection to the inn is said to be fictitious.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Abigail</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Building along Hawks Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> John Hippisley, who runs the Canterbury ghost walk, tells the story of Abigail. She committed suicide after several years of being beaten by her husband. Abigail ultimately had the last laugh, as her death was mistaken as a murder for which her husband was found guilty and hanged. Her spirit is said to linger in the upper part of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kent1085.jpg' class="w3-card" title='Canterbury Cathedral, Kent.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Canterbury Cathedral, Kent.</small></span>                      <p><h4><span class="w3-border-bottom">Nell Cook</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Canterbury Cathedral - Archbishop's Palace, the Dark Entry, and other areas<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Nell was a servant at the cathedral. He discovered his niece was having a relationship with a canon and killed them both. Nell's ghost now walks in regret. Phantom plainchant has also been heard from the area, and the figure of a nun and the ghost of Simon of Sudbury are also reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kent1094.jpg' class="w3-card" title='Roads running through the centre of Canterbury.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Roads running through the centre of Canterbury.</small></span>                      <p><h4><span class="w3-border-bottom">One Time Mayor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Central city streets<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding his bicycle around the city he loved, the mayor has proved to be quite harmless.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Clouded Leopard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Countryside around the area<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1975<br>
              <span class="w3-border-bottom">Further Comments:</span> An escapee from Howletts Park, this leopard lived for eight months in the Kent countryside, before being shot by a farmer.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kent8668a.jpg' class="w3-card" title='St Margaret&#039;s Street, Canterbury.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> St Margaret&#039;s Street, Canterbury.</small></span>                      <p><h4><span class="w3-border-bottom">Girl in Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Former private residence (now an Estate Agent) along St Margaret's Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century, every Friday morning at 05:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> John Hippisley, who runs the Canterbury ghost walk, tells the story of a phantom girl dressed in grey clothing would be seen to enter the front door and ascend the staircase before vanishing. Eventually, a female's skeleton was discovered under the floorboards, draped in grey cloth.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stagehand</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Gulbenkian Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom presence is said to be a stagehand wearing black clothing. While only being seen once, the figure is more likely to be heard climbing ladders.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown Glass</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Maidens Head public house (Room 4 in particular)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2019 - 2022<br>
              <span class="w3-border-bottom">Further Comments:</span> The pub appeared in the local press after it was reported that keys were taken and glasses smashed by an unseen hand in one of the guest rooms. A jukebox reputedly played while disconnected.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kent3707.jpg' class="w3-card" title='The new Marlowe Theatre, Canterbury.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The new Marlowe Theatre, Canterbury.</small></span>                      <p><h4><span class="w3-border-bottom">Figure in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Marlowe Theatre (original theatre demolished in 1982)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1967<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly figure could be seen loitering on the right hand side of the stage during a performance. Please note that the image is of the newer Marlow; the one in which the ghost was encountered was demolished in 1982.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sheet Removal</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Private residence along Querns Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> The Canterbury Times reported that Dorothy Crawford had lived in her haunted home for thirty years, experiencing shaking furniture and having bed sheets removed at night. Crawford had also observed an old woman dressed in nineteenth century garb in her home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman's Voice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Rowlands Bistro (no longer operating)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1980s / early 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Locking up late one night with only one other person in the building, a member of staff in the office heard a disembodied woman's voice say, 'I'll be there in a minute'. The witness quickly ran downstairs to find the waiter. The waiter himself stood in shock, having encountered a cold shape which had passed through him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Shop in The Parade<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1973<br>
              <span class="w3-border-bottom">Further Comments:</span> A worker in the upper part of the shop heard rustling. When they turned, a black shadowy figure was observed moving towards a kitchen. When the area was investigated, no one was there. The shadow had been spotted a couple of years previous by a cleaner.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Robed Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Sudbury Tower, Pound Lane, City wall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This solitary figure haunted a bedroom in the tower, reportedly tucking the occupant in at nights! Some believe the figure to be Simon of Sudbury.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kent1303.jpg' class="w3-card" title='Sun Street, Canterbury.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Sun Street, Canterbury.</small></span>                      <p><h4><span class="w3-border-bottom">Bloody Water</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Sun Street<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A pump that once jutted from a wall was said to give out red water, coloured by the blood of Thomas Becket.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kent1093.jpg' class="w3-card" title='The Old Weaver&#039;s House, Canterbury.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Old Weaver&#039;s House, Canterbury.</small></span>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - The Old Weaver's House, currently a restaurant<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by a domestic assistant, the lady in grey was observed ascending the staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kent8660.jpg' class="w3-card" title='Tiny Tim&#039;s Tearoom, Canterbury.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Tiny Tim&#039;s Tearoom, Canterbury.</small></span>                      <p><h4><span class="w3-border-bottom">Three Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Tiny Tim's Tearoom (restaurant), 34 St Margaret's Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Workmen working on restoring the building during the 2000s reported the sounds of children playing on the staircase, strange noises in the attic, and youngsters whispering in a panelled room where they had found the relics from several children.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Undisclosed shop along The Parade<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> An employee reported that a customer had seen a black figure walk into and through a wall in the upper part of this shop.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Happy Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Canterbury - Unnamed church residence along Burgate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A property along this road was haunted by an unhappy man who made himself frequently seen. A skeleton was discovered the area, after which the phantom took on a cheerier disposition.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 18 of 18</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southeast.html">Return to South East England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
