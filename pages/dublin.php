

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Dublin Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Dublin</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Dublin Ghosts, Folklore and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Waiting Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balbriggan - Ardgillan Castle, bridge in garden over railway known as The Lady's Stairs<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom woman is said to be waiting for her husband to return. He was a keen swimming, but drowned one night, leaving his wife on the bridge awaiting his return. One version of the story says that whoever sees the ghost on Halloween will be picked up and thrown into the ocean.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Balbriggan-unknown.jpg' class="w3-card" title='A strange green cat.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A strange green cat.</small></span>                      <p><h4><span class="w3-border-bottom">Green Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balbriggan - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom cat, green in colour, was reported to haunt this town.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Yellow Eyed Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balbriggan - Unidentified estate road<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa Christmas 2021<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness watched a large black dog with yellow eyes cover eighteen metres in just three strides. Over the following two months the witness lost two members of their family.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Apologise to the Tree</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - Ormeau Golf Club<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Tree still present<br>
              <span class="w3-border-bottom">Further Comments:</span> If the fairy tree found near the course is hit by an errant golf ball, the player is advised to apologise to avoid the wrath of the little people. Even if not struck, it is said that golfers nod at the tree as they pass. Just in case.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blanchardstown - Former monastery converted into a home<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth or early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A servant was said to have encountered a phantom nun within this house. Footsteps would also be heard, while doors would open themselves at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach and Four</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clondalkin - Newlands House, currently part of the Newlands Golf Club<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Newlands was once owned by Arthur Wolfe, Chief Justice of Ireland; the ghostly coach is said to follow the route it once took, taking Arthur's body away from his home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Butcher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - 118 Summerhill (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1966<br>
              <span class="w3-border-bottom">Further Comments:</span> Unease spread amongst workmen in the process of demolishing the house after they spotted a spectral butcher moving around the rooms. The ghost had previously been reported in two other houses along the road which had already been knocked down.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - 13 Henrietta Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Now thought to have been exorcised, this phantom woman wearing old fashioned clothing was seen by neighbours looking in through the window.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - 7 and 8 Hendrick Street (demolished 1963)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s<br>
              <span class="w3-border-bottom">Further Comments:</span> Starting in the 1920s, and lasting for a period of around thirty years, occupants in number 7 reported hearing shoeless footsteps which traversed the rooms and corridors. Number 8 was haunted by a man seen standing by the fireplace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Applerock Studios<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> Investigated by GhostEire, the site located in a former red light district is home to shadowy people and an unidentified voice picked up on recording equipment.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shaking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Ardee House (demolished mid twentieth century)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A bedroom in this property was considered particularly haunted, with occupants being violently shaken awake.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Archdeacon Bulkeley?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Area around Old Bawn Road and the Dodder<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 08 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to either be driven by or containing Archdeacon Bulkeley, a coach drawn by six headless horses containing two passengers together with two footmen, drives through this area of Dublin. Anyone who spots the coach is said to die within 366 days.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert Emmet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Brazen Head public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Rebel leader Robert Emmet is said to have used the Brazen Head for meetings until executed in 1803. His ghost is said to remain, still on the lookout for enemies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Dublin-Cabra-district.jpg' class="w3-card" title='A ghostly black dog in the snow.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly black dog in the snow.</small></span>                      <p><h4><span class="w3-border-bottom">Lord Norbury</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Cabra district<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The otherworldly black hound which stalks the area is said to be the ghost of Lord Norbury, a harsh judge renowned for his overuse of the hangman's noose.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Canal through Drimnagh<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1992, 02:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A cyclist riding along the canal witnessed a glowing elderly woman in white with long hair dancing with a young man. The man wore a seventeenth or eighteenth century military uniform and looked 'normal', unlike the woman who also hummed a tune. The cyclist did not stop.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">James Clarence Mangan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Castle Inn, Lord Edward Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Mangan was born in this building and is said to periodically return, his presence bringing an overwhelming depression to the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shuffling Feet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Clontarf Castle Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> One guest at the hotel claimed to hear footsteps in his room at night. The man also said that his shower had turned itself on and his TV flicked channels over at will...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Matching Band</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Corkagh Estate (now Corkagh Park)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Easter  Monday, 1916<br>
              <span class="w3-border-bottom">Further Comments:</span> During the Easter Rising of 1916, the family and staff at Corkagh House heard a band playing and the sound of marching feet heading towards the house. Those present thought they would be attacked but the sounds suddenly stopped and nothing could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screams of the Drowning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Deey Bridge and canal lock<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A passenger boat sunk here in the eighteenth century; many of the occupants drowned, and their psychic screams continue to this day.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Eleanora</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Drimnagh Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Eleanora's new husband fought her true love, with both men dying for their injuries. After Eleanora died (of either suicide or exposure), her ghost began haunting her home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert Emmet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Drummond House (aka Emmet's House)<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> Patrick F. Byrne wrote that the ghost of Emmet could be observed in the garden, although one of Byrne's friends proved the 'ghost' was only a shadow caused by a gate post.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/DublinCastle1904.jpg' class="w3-card" title='Dublin Castle. Process print, 1904.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Dublin Castle. Process print, 1904.</small></span>                      <p><h4><span class="w3-border-bottom">Invaders</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Dublin Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Men who tried to invade the castle were executed on site and their headless remains buried within the grounds. Sometimes their spirits are said to wander.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knock on Wood</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Fishamble Street Theatre (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 22:00h each evening (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The Green Room of this former theatre was haunted by phantom rapping, which always started at the same time each evening and lasted exactly fifteen minutes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Faceless Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Flat near the North Circular Road<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1988 / 1989, around midnight<br>
              <span class="w3-border-bottom">Further Comments:</span> Two friends chatting outside a flat spotted an old woman walking along the road, pausing every so often to pick up scraps of rubbish. The friends commented on how terrible it was to see the woman alone in that state and continued to watch as she climbed some stairs to the house on the opposite side of the road. The woman turned to face the friends, who then realised the woman's face was brightly glowing, and her legs and hands were also illuminated. The glowing woman then turned her back and vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Gregory</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dublin - Former Abbey Theatre (National Theatre of Ireland) (demolished 1960)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1960<br>
              <span class="w3-border-bottom">Further Comments:</span> Lady Gregory took the lead in finding the funds to build the theatre, and had a favourite chair in the auditorium. After Gregory's death, anyone who sat in the chair would feel something pushing against them.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 86</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=86"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=86"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
