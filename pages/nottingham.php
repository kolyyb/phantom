
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Nottingham Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastmidlands.html">East Midlands</a> > Nottingham</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Nottingham Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wet Prints</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - 1 Queens Street, British Transport Police Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1977 and 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> An officer who was working alone at night in 1977 heard a heavy door slam. Going to investigate, he found a set of wet footprints leading to the toilets, even though the weather was dry outside. Following the prints into the toilets, he found the room empty. Thirty years later, another officer spotted a man walk into the toilets - realising that he and his colleague should have been the only people on site, the officer investigated the room and found it empty.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Charles Watkin Williams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - 23 North Street (now Forman Street)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early / mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A high court judge who allegedly died while with a 'working girl' was once said to haunt the upper part of this building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">AA Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - A52 (or A42) road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1930s onwards?<br>
              <span class="w3-border-bottom">Further Comments:</span> A patrolman belonging to the Automobile Association is said to ride his motorbike along this road. It is not known whether he stops to help broken down motorists.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - All over the city<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 27/28 May 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> Many across the city heard roaring during the night, with one describing the noise like a blow torch.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Ambulance Station, Wilford Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> This station was home to phantom footsteps and opening doors. Two witnesses watched a shadowy man enter the toilets; concerned he was a trespasser they followed him in, only to find the area empty.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Nottingham-Babbington-col.jpg' class="w3-card" title='A ghostly miner at Babbington Colliery.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly miner at Babbington Colliery.</small></span>                      <p><h4><span class="w3-border-bottom">Victorian Worker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Babbington (occasionally spelt Babington) Colliery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> It is unclear whether this former mine was haunted by one miner or several. In one case a miner wearing old-style clothing was spotted walking into an area that was being sealed off. In another incident, a group of miners watched a figure wearing coal dust covered clothing, apart from a bright white scarf. As they approached him, the figure faded from view; a dirty white scarf lay at the spot he had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roundheads</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Belgrave Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> At least one witness has reported seeing spectral Roundheads on horseback ride through this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert Jackson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Bell Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Robert is said to walk through the restaurant, which is also haunted by two men who are seen sitting at a table before disappearing. The women's toilets are home to a female phantom, while a jester is said to stand outside the front door.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure in a Hat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Bonington House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> At least one member of staff has spotted a figure wearing a wide brimmed hat and long cloak pass through a wall in the upper part of the building. Mild poltergeist activity is reported here (such as turning off beer taps in the cellar), and some speculate that Richard Bonington himself is still here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Edna</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Bonington Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1982 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> During the construction of the complex where the theatre stands, the skull and bones of a female were discovered. It is thought that the bones belonged to 'Edna', an active spirit who opens doors, moves items around, and can be heard walking around in parts of the building that should be empty.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Long Black Coat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Bridlesmith Gate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1969 onwards?<br>
              <span class="w3-border-bottom">Further Comments:</span> Since a secret cave was found by workmen under a shop in 1969, this pedestrianised street has become home to a phantom man wearing a long black coat and tall black hat. His connection to the cave is unknown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cold Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Castle Rock Brewery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 February 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> An employee reported seeing a male figure walking across the cold storage area, although he could not be found when the room was checked. Over the years other staff have reported seeing 'things' out of the corner of their eyes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Church Cemetery (also known as Rock Cemetery)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Old lady unknown, Victorian in 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> An elderly phantom lady is said to haunt this area, fading away after being spotted. The area is also haunted by a Victorian woman, who when last seen, vanished suddenly.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Underground Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - City of Caves, Broadmarsh shopping centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> This network of caves is said to be haunted by several figures. Dark indistinct shadows have been seen darting around, and an upset woman in Victorian clothing has been spotted. It has also been reported that a few people have heard explosions overhead - the caves were used as bomb shelters during the Second World War.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Colwick Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1831 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen wandering around the gate of the church, this woman in white is thought to be looking for an unknown item. The ruined church itself is home to a tall man who has been seen standing at the location the altar once stood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chaworth Musters</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Colwick Hall Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2009 (Shouting / poking)<br>
              <span class="w3-border-bottom">Further Comments:</span> One of two ghosts said to visibly manifest in this area, Mrs Chaworth Musters reputedly hides behind trees on the land, concealing herself from the mob that stormed the hall in 1832. The second phantom is named as Mary Ann Chaworth, and she quietly haunts the corridors. Less quietly, the sounds of footsteps, children's laughter and voices have all been heard from the servants' quarters. One witness awoke to the sounds of shouting and the feeling of being poked by an unseen presence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Nottingham-Colwick-Woods.jpg' class="w3-card" title='Ghostly children in the woods.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ghostly children in the woods.</small></span>                      <p><h4><span class="w3-border-bottom">Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Colwick Woods<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghosts of a white woman and of two children have been reported in this wooded area. While the woman in white has been named as Mary Chaworth Musters, the children are thought to have been murdered by their father William Saville, hanged for the crime in 1844. The sound of horses has also been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Brown Haired Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Cumberland Place - old Children's Hospital and then a laboratory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1949<br>
              <span class="w3-border-bottom">Further Comments:</span> Two female technicians, and one of their sisters, watched a nurse in an old fashioned uniform glide through a door before vanishing. They said the entity had a pretty face and golden brown hair.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing Spirit</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Denewood Crescent<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February 1968<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity made a couple of appearances in this house and was also heard to sing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Average Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Fanum House, and neighbouring building, Lenton<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1982<br>
              <span class="w3-border-bottom">Further Comments:</span> A female employee in Fanum House spotted a figure standing in a doorway that suddenly vanished. The building has a history of hauntings, going back to the 1960s, when an 'average looking' man was seen by staff. This figure would also quickly vanish. There was also a brief poltergeist outbreak in the kitchen - tea urns would transport themselves from one side of the room to the other. The polt outbreak also affected the neighbouring building, where the owner stored fruit machines. He came in one morning to find all the machines moved - a feat only achievable by a team of several strong men.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Glasses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Ferry Inn, Wilford<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> An exorcist was brought in to deal with the poltergeist that slid glasses around on the bar and created loud banging.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Flying Horse Hotel (now Flying Horse Walk, a shopping arcade)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> While staying at the hotel, one male guest refused to re-enter his room after encountering a grey lady passing down a corridor. The beer cellar was also said to be haunted, with lone staff reporting feeling a tapping on their shoulder.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Food shop (since closed), Oakdale Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s, 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> It was said that footsteps could be heard in empty parts of the food store. After it closed, it reopened as a new business, which experienced doors slamming shut on their own accord and hand driers in the toilets being switched on and off repeatedly.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Former antiques shop, Mansfield Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1975/6<br>
              <span class="w3-border-bottom">Further Comments:</span> This young girl, said to be around ten years of age, had a pale face and large eyes. She wore dark clothing, a white pinafore, and her boots had holes in the toes. A medium was called to the shop, after which the phantom disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moaning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nottingham - Former Police Station, Canal Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1976<br>
              <span class="w3-border-bottom">Further Comments:</span> This building, while being used by a private company, was said to be haunted by moans and groans, banging and by a light being switched on by an unseen hand. The manager of the firm was not impressed with the stories and discovered that all strange activity (except the moaning) could be stopped by changing the electrics.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 81</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=81"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=81"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastmidlands.html">Return to East Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="ViewWest Midlands records" style="width:100%" title="View West Midlands records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
