
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in June">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > June</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>June (& Summer) - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hairy Earth</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wistow (Leicestershire) - Wistow's Grave<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Concealing the heir to the kingdom of Mercia's body, this grave is reported to sprout hair throughout the month of June, starting on the first of the month.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman's Singing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hickling (Norfolk) - Hickling Broad, south of village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year a gentle female voice can be heard singing. This could be the Woman in White, seen punting from one side for the Broad to another, heading towards a mill. Another ghost is associated with the broad; a drummer is said to be a Napoleonic soldier who drowned while skating across the ice, en route for a secret rendezvous with his girlfriend, can sometimes be heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ammunition Train</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Soham (Cambridgeshire) - Approach to the station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Every 2 June an accident that cost the lives of two people is re-enacted. An ammunition train caught fire, the explosion shattering every window in Soham and destroying the old station. The rebuilt tracks no longer follow the scene of the accident.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newtyle (Angus) - Bulb Farm (no longer standing, replaced by housing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 June (reoccurring), or thereabouts, after sunset<br>
              <span class="w3-border-bottom">Further Comments:</span> A white figure, thought to be decapitated, was said to move slowly around the area, as if looking for its missing body part. Groaning is also heard in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lily Cove</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Haworth (Yorkshire) - Old White Lion public house, Main Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 10 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A local celebrity, Lily fell to her death from a hot air balloon in the early part of the twentieth century, and now returns to the pub on the day she died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Children's Paradise</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Horsey (Norfolk) - Horsey Mere<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 June, around midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to be the burial place of maybe thousands of children from the days of the Romans, they are said to return once a year to play for a couple of hours.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sevenoaks (Kent) - Maidstone Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed in a road traffic accident in 1959, this ghostly figure now steps out in front of traffic on the anniversary of her death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nort2370.jpg' class="w3-card" title='The site of the Battle of Naseby, Northamptonshire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The site of the Battle of Naseby, Northamptonshire.</small></span>                      <p><h4><span class="w3-border-bottom">Midair Battle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Naseby (Northamptonshire) - Sky over site of the Battle of Naseby<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Reported less frequently now, but for one hundred years after the battle, locals would sit on the nearby hills and watch the battle occur once again, complete with the sounds of men screaming and cannons firing. A photograph taken by the Northampton Paranormal Group on this date in 2008 may show a phantom soldier, though nothing was seen when the image was taken.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Goring the Cavalier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hitchin (Hertfordshire) - Hitchin Priory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 15 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed by Roundheads a few miles down the road during the civil war, the ghost of Goring continues onwards to his earthly destination once a year. A grey lady has also been spotted in the grounds of the priory.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Piper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glen Esk (Angus) - Waters of Lochlee<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A great and popular piper was once taken by little folk donned in green across the waters here - once a year we mortals have the chance of listening to his tunes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hessian Cavalry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Foulkesmill, Horetown (County Wexford) - Battle of Horetown site and Green Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The entire Battle of Horetown is replayed on its anniversary, while a lone Hessian trooper is seen standing by a tree along Green Road where his body is thought to be buried.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sprites & Pixies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballona (Isle of Man) - Ballona Bridge<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> On this day, the fairy population comes out in force. If they are not acknowledged by anyone crossing the bridge, the fairy folk have no hesitation in making their presence felt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">King Arthur</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cadbury (Somerset) - Cadbury Castle, and road between North Barrow and South Barrow villages<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Escorted by mounted knights carrying lances, King Arthur of the Britons traverses this route back to Camelot (as Cadbury is thought to be) on this day in June. One witness said that on 23 June 1995 they encountered an armoured man (more dark age than medieval) holding a standard on horseback - the figure disappeared around a corner, though there was no place that he could have gone. One legend says a door opens every seven years in the hillside so that Arthur and his company can leave to feed their horses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swimming Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford (Kent) - Eastwell Park and Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June - Horseman seen (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding towards the park house, this phantom horseman veers off at the last minute and enters the nearby lake. A white lady haunts the house itself, seen by porters on the night shift. In the seventeenth century, the Earl of Winchelsea cut down several oak trees, bestowing a curse on his family which took the life of his wife and son shortly after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Arthur's Table</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bossiney (Cornwall) - Bossiney Mound<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June, midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year, King Arthur's table is said to surface from the mound, briefly waiting for the man to sit at it once more.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nan Clark</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW7 (Greater London) - Nan Clark's Lane, Mill Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 June (reoccurring), though last reported sighting in Nov<br>
              <span class="w3-border-bottom">Further Comments:</span> Nan Clark was a landlady of a local inn in the eighteenth century, whose history is shaky - she may have murdered or been murdered by her husband or lover, either in the lane which takes her name or in a local pond. Whatever truly happened, Nan now returns to the area once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hampton Court (Surrey) - Landing Stage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 June or 24 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by local fishermen (even though they cannot remember the exact date they witnessed the phenomenon), the white lady's history is unknown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Lovers-Leap-Dargle.jpg' class="w3-card" title='An old postcard of Lover&#039;s Leap rock in the Dargle Valley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Lover&#039;s Leap rock in the Dargle Valley.</small></span>                      <p><h4><span class="w3-border-bottom">Unfaithful Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bray (County Wicklow) - Lover's Leap rock, Dargle Valley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The female ghost which appears here once a year is said to be that of a woman who cheated on her boyfriend. He died of grief, and the woman sat in mourning over his grave continuously for several days before taking her life on the rock, leaping into the river below.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Those that will Die</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Tavistock (Devon) - Church<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The spirits of those who will die during the following year come to the church on Midsummer's Eve - they were last seen by a pair of brothers who saw themselves in the crowd.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Lonely Shepherd</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clydach (Gwent) - Limestone Pillar<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A shepherd who drove his wife to suicide was petrified by God as punishment. Once a year, on Midsummer Night, the rock animates and walks down to the River Usk where his wife drowned herself, before returning prior to dawn.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Trellech-wells.jpg' class="w3-card" title='A Welsh fairy.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A Welsh fairy.</small></span>                      <p><h4><span class="w3-border-bottom">Fairy Meet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Trellech (Gwent) - Area around the wells<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Fairies are said to make their presence known around this area once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Petrified Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Enstone (Oxfordshire) - Hoar Stone<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The three stones standing here are said to be a man, his dog and his horse. The man, the larger stone, is said to wander down to the village stream on Midsummer Eve. The stones are also said to return to their positions if anyone tries to remove them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fish Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crickhowell (Powys) - Stone by the River Usk<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year this strangely shaped stone dives into the local river and goes for a swim.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangattock (Powys) - Caves in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly choir was said to sing once a year within the cave network.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Llangattock-Field.jpg' class="w3-card" title='The Devil strutting his funky stuff.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Devil strutting his funky stuff.</small></span>                      <p><h4><span class="w3-border-bottom">Dancing Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llangattock (Powys) - Field north of Llangattock Park<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> On the eve of Midsummer, the Devil would emerge and spend time dancing with the local fairies around a clump of trees.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 44</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=44"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=44"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
