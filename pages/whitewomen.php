


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghostly Women in Folklore and Strange Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Women in White</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghostly Women in White and White Women</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/A31-Hogs-Back.jpg' class="w3-card" title='A female ghost was spotted here in the 1960s.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A female ghost was spotted here in the 1960s.</small></span>                      <p><h4><span class="w3-border-bottom">Box Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A31 (Surrey) - Hogs Back, Guildford bound. 1 mile Northeast of Tongham junction<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 08 January 2007, 22:40h, and 1960s (around 03:00h)<br>
              <span class="w3-border-bottom">Further Comments:</span> Travelling home in 2007, a driver watched a horse drawn box carriage cross the road around 100 metres in front of him. The witness reported that he could not make out too many details as it was raining hard, though he was able to see a dim lantern towards the front of the carriage. As the driver reached the spot where he had seen the vehicle pass, he realised there was no side road where the coach and horses could have crossed. Another witness in the 1960s spotted a female figure with long hair, wearing a white dress, standing by the roadside. The witness stopped to see if she was lost or needed a lift, but she had vanished. Mentioning the encounter to a work colleague the following day, the colleague described the figure which was encountered and said the area was haunted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen (Aberdeenshire) - Ardoe House Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990?<br>
              <span class="w3-border-bottom">Further Comments:</span> The daughter of a previous owner, this shade is thought to have returned after the girl's suicide. She is said to walk down the staircase dressed in nightwear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergavenny (Gwent) - Llanvihangel Court<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This pallid woman is said to leave the house at midnight and walk into a nearby wood. The White Room is said to be haunted by a little man with green eyes. It is not known for sure whether these ghosts are connected to a series of dark stains on the staircases of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coloured Women</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergele (Clwyd) - Gwrych Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s<br>
              <span class="w3-border-bottom">Further Comments:</span> This castle is haunted by both a white woman and a lady in red, the latter thought to have been killed during a fox hunt, while the former returned after being buried in unconsecrated ground. Another story says that the white woman was the former owner of the castle (Winifred Cochrane, Countess of Dundonald) who returned after her bitter husband, written out of her will, purchased the site and destroyed the contents.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/2205womaninshroud.jpg' class="w3-card" title='Shrouded Woman, public domain.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Shrouded Woman, public domain.</small></span>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberhafesp (Powys) - Aberhafesp Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom white woman is said to make her way from the nearby church to a door on the east side of the hall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Acton-Burnell-castle-LClout.jpg' class="w3-card" title='A photograph of Acton Burnell Castle, copyright 2006 Lawson Clout.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A photograph of Acton Burnell Castle, copyright 2006 Lawson Clout.</small></span>                      <p><h4><span class="w3-border-bottom">Girl in White Lace</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton Burnell (Shropshire) - Acton Burnell Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly girl dressed in white lace is said to haunt this ruined site. A student managed to photograph a strange misty 'face' in 2004 and reported hearing scratching noises. (Image copyright 2006 Lawson Clout)</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alcester (Warwickshire) - Ragley Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman in white is said to appear at midnight, sit for a while, and move off to a nearby stream to drink.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allington (Wiltshire) - Road to Horton, near Tan Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 October 1904<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by two lay preachers driving a horse and trap, this phantom was mistaken for a real woman as she walked towards them in the rain. They could see she had auburn hair and a beautiful face, before she vanished without trace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burnt Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alton (Hampshire) - Crown Hotel, High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom dog reportedly haunts the hotel, where it was kicked to death by a drunken customer. Some say that a white (or grey) woman also glides around inside the building; the girl was a kitchen maid murdered in the hotel. Finally, the ghost of an old man haunts room number three.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Altrincham-Ashley-Hall.jpg' class="w3-card" title='A phantom white lady.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom white lady.</small></span>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Altrincham (Greater Manchester) - Ashley Hall (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This flitting entity was seen several times in a particular room of the hall, her large black eyes staring into the empty space ahead. The room was sealed up as the haunting presence was so strong.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annagh (County Cork) - Unstated property<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1927<br>
              <span class="w3-border-bottom">Further Comments:</span> While camping close to this property, one traveller awoke to see a white female form standing at a gate. He woke one of his friends who also observed the ghostly figure. The following day the travellers moved on and never returned (special thanks to the Churchtown Village Renewal Trust, from their 'The Annals of Churchtown').</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annesley (Nottinghamshire) - Annesley Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970, 25 September 2012 and 10 October 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> Dying in childbirth, this mistress of a former owner still walks the walls. The ghost, or perhaps another, was seen in 1970 as it crossed the road outside the hall; the driver of a car drove straight through the figure. In October 2012, one team of ghost hunters recorded laughter in the stable when the area was empty - the area is reputed to be the location where a serving girl hanged herself - while another team in September spotted a black dog.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Armagh (County Armagh) - Armagh Gaol<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> During a fashion shoot in this former prison, studio photographer Alan Wells caught a strange white female apparition in the background of one of his images.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swimming Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford (Kent) - Eastwell Park and Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June - Horseman seen (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding towards the park house, this phantom horseman veers off at the last minute and enters the nearby lake. A white lady haunts the house itself, seen by porters on the night shift. In the seventeenth century, the Earl of Winchelsea cut down several oak trees, bestowing a curse on his family which took the life of his wife and son shortly after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gliding Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford (Kent) - Repton Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom white lady here was reportedly seen by soldiers using the building during the Second World War and was thought to be the murdered wife of a former owner. Within the manor she would ascend the staircase holding a candle, while one person who observed her outside said she had no feet and had drifted across a nearby field. The kitchen area was also reputedly haunted by a ghostly monk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashmore (Dorset) - Ashmore - area between Washer's Pit and Spinney's Pond<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local story says that a woman hanged over the pit was saved by a washer woman. This did not prevent the woman's ghost from returning to the area when she later died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wilt2122.jpg' class="w3-card" title='Avebury Manor, Wiltshire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Avebury Manor, Wiltshire.</small></span>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Avebury (Wiltshire) - Avebury Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A white lady drifts around the grounds of the manor and sometimes the nearby henge and stone circles. The phantom woman's face is covered by a large hood. The inside of the manor house is said to be haunted by several other entities. One spirit would scatter rose petals across the floor of the Crimson Room, and a phantom monk was once spotted in the library. A ghostly Cavalier also occasionally makes an appearance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Avebury (Wiltshire) - Footpath by Trusloe Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A government worker walking from Avebury Manor reported encountering a ghostly woman in white lace and a white hood at an iron gate close to Truslow. The witness said the woman appeared from nowhere, took his shoulders, and spun him around before pushing him away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tabby Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Axbridge (Somerset) - King John's Hunting Lodge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> This feline apparition has been observed padding around on the first floor, vanishing when it sits down. A white Elizabethan lady is also reported to haunt the same floor, seen sitting in a chair.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aycliffe (Durham) - A167 north of town<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century, though sightings may go back to eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> While the clothing of the woman varies (from wearing a raincoat to a wedding dress), the figure is always white. In true 'phantom hitchhiker' style, the woman had been picked up by passing drivers, only to vanish a few miles down the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylmerton (Norfolk) - Circular Hollows (local name Shrieking Pits)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1700s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The White Lady has been observed looking into each pit, crying out in pity. Her origin is unknown, but she could just be a story concocted by local smugglers. The sound of the screams heard in the pits, which local folklore attributes to the woman's phantom, is more likely to be the wind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballincollig (County Cork) - Oriel Lodge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1930s<br>
              <span class="w3-border-bottom">Further Comments:</span> A husband and wife encountered a white female phantom standing by the kitchen door, after which they had their bed covers pulled from them by an unseen hand on three consecutive nights.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballygarvan (County Cork) - Small bridge over a stream<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A small bridge over a stream near Ballygarvan was home to a white woman. After appearing to one man who fell critically ill the following day, a priest went to the site and banished her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnsdale Forest (Yorkshire) - Robin Hood's Grave<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The area around the site thought to be Robin Hood's final resting place (one of them!) is said to be haunted by a white lady who drifts around the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Barthomley-church-field.jpg' class="w3-card" title='Phantom lady in white.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Phantom lady in white.</small></span>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barthomley (Cheshire) - Church field<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A lady in white haunts this field, while the road nearby is home to a phantom dog.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 350</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=350"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=13&totalRows_paradata=350"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
