


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Bridal Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Brides</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Phantom Brides and Related Myths</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Aberaeron-Llanina-House.jpg' class="w3-card" title='A phantom head.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom head.</small></span>                      <p><h4><span class="w3-border-bottom">Hovering Head</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberaeron (Dyfed) - Llanina House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly form of a severed head belonging to a murdered young bride reputedly hovers around this building. There are also reports of a phantom man whose clothes are saturated with water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mistletoe Bride</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bawdrip (Somerset) - Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> A grey lady that walks on the grass in front of the rectory is said to be the mistletoe bride (the nearby Knowle Hall one of several places in the UK that claims the legend as its own). Poltergeist activity has also been reported in the rectory in the past.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Newly Wed Bride</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Braemar (Aberdeenshire) - Braemar Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> Convinced she had made a bad wife after waking up and finding her new husband gone, this girl leapt off the battlements to her death - unfortunately, her husband had only gone hunting without informing her. She now appears to others, not wanting them to make the same mistake.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman with Mistletoe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bramshill (Hampshire) - Bramshill House (police college)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> Not just home to the ghostly bride (who suffered the fate of hiding in a chest from her husband while playing hide and seek, only to become stuck and suffocate) who wakes people up who spend the night there, but also to a grey lady who haunts the library and a green man who hovers by the lake - it is reported that he has no legs. Finally, a man in a flannel suit was observed walking through a wall by a security guard in the 1970s.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mistletoe Bough</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brockdish (Norfolk) - Brockdish  Hall<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The hall is one of several sites which is supposedly the site of the legend of the Mistletoe Bough - a bride who died on her wedding day after being locked in a chest during a game of hide and seek was found many years later in skeletal form.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bride in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castleton (Derbyshire) - Castle Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Left standing at the altar, this bride who never was now haunts the place where her reception should have been. A maid who saw her ran screaming from the building, while another witness said the ghost was cut off at the knee. A more recent ghost is that of a man in a blue pinstripe suit, seen by two different proprietors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bride in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chagford (Devon) - Whiddon Park Guest House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 10 July (reoccurring), last seen 1971<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered by a former suitor on her wedding day, this bride now returns on that fateful date. Dressed in black, she has been seen standing in the doorway of a bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sarah</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester (Cheshire) - Thorntons Chocolate shop (was once House of Bewlay Tobacconist), Eastgate Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Stamping and thuds emanating from an empty room have been reported from this shop, as well as strange wailing. Lights flicker on and off, and doors open and close by an unseen hand. The source of the haunting is said to be Sarah, a jilted bride who hanged herself, though her presence and poltergeist activities are said to be 'friendly' in nature.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Wedding Dress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cullompton (Devon) - Manor House Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to frequent room 6, this bride was jilted at the altar, and still awaits her partner's return.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bride Dressed in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Devizes (Wiltshire) - Three Crowns Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The bride committed suicide in the building after being jilted at the altar, and her pallid form has existed since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Exton_House_rutland.jpg' class="w3-card" title='An old painting of Exton Hall.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old painting of Exton Hall.</small></span>                      <p><h4><span class="w3-border-bottom">Mistletoe Bough</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exton (Rutland) - Exton Hall<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The decaying hall is one of several sites which is supposedly the site of the legend of the Mistletoe Bough - a bride who died on her wedding day after being locked in a chest during a game of hide and seek was found many years later in skeletal form.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wedding.gif' class="w3-card" title='An old woodcut of a couple being married.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a couple being married.</small></span>                      <p><h4><span class="w3-border-bottom">Wedding Party</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Featherstone (Northumberland) - Route between Pynkinscleugh & Featherstone Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Once a year, date unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> To celebrate a wedding, the guests, bride and groom went out hunting; it resulted in a fight that killed most of them, and now they all travel back to the castle once a year, covered in blood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Man glowing Blue</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hartley (Kent) - A229, heading towards Maidstone<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 26 June 1999<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly man with blonde hair and a blue tint stood in the road; an incoming vehicle had to violently swerve to avoid the figure. When the driver realised the man had vanished, he quickly drove off. There has also been a report of a phantom woman with long blonde hair along the A229, said to have been a bride killed one day prior to her wedding day.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pink Bride</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kemnay (Aberdeenshire) - Burnett Arms Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing a pink wedding dress, the ghost of a former owner named Maggie who died in 1934 is thought to haunt this hotel. A local legend said that two coffins were buried under the hotel, one containing Maggie's body and the other filled with her money, though an excavation under the building in 2008 found nothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6353.jpg' class="w3-card" title='King&#039;s Lynn Quay, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> King&#039;s Lynn Quay, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">The Bride</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn (Norfolk) - Purfleet Quay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This woman killed herself the day after being wed - her ghost loiters around the Purfleet area before screaming and throwing itself into the quay waters. Other screams, belonging to fighting soldiers, can also be heard in the area, during which the water runs red with blood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor402.jpg' class="w3-card" title='Tudor Rose Hotel, King&#039;s Lynn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Tudor Rose Hotel, King&#039;s Lynn.</small></span>                      <p><h4><span class="w3-border-bottom">Small Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn (Norfolk) - The Tudor Rose Hotel, 11 Nicholas Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Shortly after a wedding, the bride was stabbed to death by her new husband in the hotel. Since then, a short woman in a long white dress has been spotted and phantom footsteps heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Honeymooner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kingsbridge (Devon) - King's Arms Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This bride found herself alone after her husband ran away before their first night together - her shade still waits for his return.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ess824.jpg' class="w3-card" title='Lawford Church, Essex.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Lawford Church, Essex.</small></span>                      <p><h4><span class="w3-border-bottom">Bride on Other's Photographs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lawford (Essex) - Church on the hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing on three wedding photographs (though none have been published to the author's knowledge), this ghostly bride is said to have died of a broken heart after discovering her husband to be had been killed on route to their wedding, after his horse had bolted. In addition, chanting has sometimes been heard coming from the church as it stands empty.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bride</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool (Merseyside) - Royal Infirmary<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The old house which once stood here before the hospital was the scene of the ultimate wedding day disaster in which the bride fell to her death from an upper story window while her guests partied on below. Her shade has been reported by nurses in the newer building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Water Horse House</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Loch Garve (Highland) - Underwater<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This freshwater monster had a house built for him and his mortal bride - the general area of the property is known as the water there never freezes because of the home's open fire!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Carrickogunnel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newmarket on Fergus (County Clare) - MacAulliffe's Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Holding a bright lantern to lure passing ships onto the rocks, the repulsive ghost of Carrickogunnel lurks around the castle. The other phantom found here is that of a bride who mysteriously vanished shortly after being married.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bride</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newton le Willows (Merseyside) - Willow Park (and other meres and woods in the area)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1947, and September 1949<br>
              <span class="w3-border-bottom">Further Comments:</span> This woman in white is said to be a bride murdered by her husband on their wedding night hundreds of years ago. She may have last been seen by a married couple who spotted a figure with folded arms standing amongst trees. Two years prior to this sighting, two woman passing Castle Hill observed the phantom woman in white.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Silent Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> No fixed location (Greater London) - Railway line between London & Carlisle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in black and wearing a veil, this figure has been seen sitting quietly and disappearing without warning. She is believed to be a bride whose new husband had stuck his head out of a window and was decapitated. Found holding his body once they reached London, totally insane, her spirit has been making the journey since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/AnneBoleyn002.jpg' class="w3-card" title='A portrait of Anne Boleyn, also thought to haunt this site.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A portrait of Anne Boleyn, also thought to haunt this site.</small></span>                      <p><h4><span class="w3-border-bottom">Jane Seymour</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Owslebury (Hampshire) - Marwell Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Seymour family once owned this property, and Jane is thought to haunt the surrounding grounds. The shade of Anne Boleyn has also been reported loitering near a row of nearby yew trees. Finally, the hall is reputedly the location where the Mistletoe Bough bride story originated, with the chest still (?) being present today.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Peebles-Forest.jpg' class="w3-card" title='An old postcard showing a forest in Peebles.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing a forest in Peebles.</small></span>                      <p><h4><span class="w3-border-bottom">Newly Married</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peebles (Borders) - Forest<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> One witness watched a man and woman in wedding attire run the forest and then vanish into thin air.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 31</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=31"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=31"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
