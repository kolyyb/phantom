

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Highland Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Highland</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Highland Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sputnik?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardgay - Moors in the area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> October 1962<br>
              <span class="w3-border-bottom">Further Comments:</span> Three months after a shepherd reported finding strange wreckage, a team from RAF Kinloss visited the site in October and removed debris consisting of a box and some bottles. While some people believe the craft was a Russian satellite, others say it was an American spy balloon.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Potato Thrower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardgay - Private house, exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The unsettled spirit here threw potatoes and other vegetables at a minister who came to investigate the disturbance - the entity was finally identified as the shade of a young baby killed by its mother.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Voyeuristic Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Auchindarroch - Auchindarroch House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This grey lady is often seen just outside the house, either peeping in through the windows, or drifting over the lawn. Another shade, face covered by a hood, has been observed inside the building, while sometimes a strange and unrecognised perfume fills parts of the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Isobel Gowdie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Auldearn - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1955<br>
              <span class="w3-border-bottom">Further Comments:</span> Tried for witchcraft in 1662, Isobel Gowdie claimed she could transform into a hare and had met the queen of the fairies under a hill. Her phantom was spotted by Robin Green, a retired soldier, as he camped in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Annie Fraser</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aultsigh - Aultsigh Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The entity heard moving around the building is named as a young woman murdered by a jealous suitor. He concealed the woman's body under the floorboards of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Granite Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aviemore - Rothiemurchus churchyard<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> A legend says that five cylinder shaped stones mark the spot where the last of the Comyns family is buried. The stones vanish and reappear depending on how the House of Rothiemurchus is fairing, but if moved or taken by a human hand, that person is doomed to die. One story says the stones where stolen by a group of people who were later found dead in a car, the stones discovered standing upright nearby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Boulton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballachulish - Ballachulish House<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman who visited the house claimed to have been there before, though only in her dreams. To her surprise, the owner of the property agreed, saying that her visitor resembled a ghost that had been seen there several times! Two more traditional ghosts can be found here; seen approaching the house on horseback, a spectral rider dismounts before vanishing on the driveway, while a scruffy little man has been seen near the front gate.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hovering Light</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballachulish - Castle Stalker<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to a death of a Stewart chieftain<br>
              <span class="w3-border-bottom">Further Comments:</span> An orb of brilliant light was said to appear and hover over the castle just before the death of a clan Stewart chieftain.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Andrew</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballchraggan - Balnagowan (or Balnagown) Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Andrew terrorised the neighbourhood, raping and murdering the locals. They decided to act against the laird, storming his castle and hanging him by the neck from the highest window. Now his shade remains within the walls of the building, though he still harasses visiting women. A much gentler shade has been seen here, a young green-eyed girl who smiles fondly at passers-by.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Buildings</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balnakeil - Balnakeil Beach<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 1980<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman taking a polaroid photograph of the beach was amazed to see two buildings and a shadowy post on the developed image - no buildings were in the frame as she pressed the camera shutter. An analysis of the image suggested that it was an under exposed image of a farm.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Devil's Hand</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beauly - St John's Priory ruins<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A stone doorway is said to display the mark of Satan.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben Alder - Below the mountain, en route for Loch Ossian<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> April / May 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> Having traversed old drover track on western shoulder of Ben Alder, a walker was looking for a suitable spot to cross a stream. The walker noticed the image of a smiling young girl, dressed in blue/green watery outfit, sitting on a slab of rock amidst the fast moving water. She was pointing to the water, enticing the walker to cross. The walker panicked and moved further downstream, crossing at a point where the water was calmer.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben Hope - On mountain<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> While approaching the cloud line, a climber noticed a grey figure 20 yards (18 metres) in front of him. The figure was walking steadily, wearing old hiking gear. The climber tried to catch him up., but no matter how fast he went he could not close the gap between himself and the figure. The climber followed him to the top of the mountain, where he disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dyeing Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben Lomond - Green Loch (An Lochan Uaine)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The fairies would be happy to dye wool or cloth left by the side of the loch. Unfortunately, one person left unclear instructions on what colour to dye some thread, resulting in the fairies permanently ceasing their work.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Music</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben MacDhui ((aka Ben MacDhui, Mac Dhui)) - General area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1926<br>
              <span class="w3-border-bottom">Further Comments:</span> Debates in the media ensured after reports of ghostly music were published. While some favoured the supernatural, others claimed the sounds were created by wind blowing through rocky funnels caused by water erosion.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Four Legged Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben MacDhui ((aka Ben MacDhui, Mac Dhui)) - Rothiemurchus Forest<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> First entity in the twentieth century, the latter reoccurring to mark tragedies<br>
              <span class="w3-border-bottom">Further Comments:</span> Several people report having been chased by an invisible entity that 'felt' semi-human but could be heard pounding along on hooves. All the witnesses reported feeling extreme terror. An entity known as the Bod-an-Dun also haunts the area, appearing to mark a tragedy in the Grant family that once owned the land.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fear Liath Mor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben MacDhui (aka Ben MacDhui, Mac Dhui) - High on Mountain<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Popularly referred to as Britain's yeti relation, the Fear Liath Mor has been heard, seen, and felt by dozens of mountaineers over the past 150 years. The sound of snow being compacted by large feet has made climbers scarper back down the slopes; those who have seen the creature say it is more man than ape, but still covered in grey or brown hair, with long arms and a large head, and standing around twenty foot in height. During the Weird Weekend 2009, researcher Andy Roberts presented a compelling argument that the Fear Liath Mor is more a feeling of dread than a biological entity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boat of Garten - River Spey<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> When the waters are low, an inscribed stone can be seen in the river - it is believed cursed, and anyone who meddles with it is doomed. Adding to the treachery of the waters, a white horse is said to patrol the river, looking for people to drown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Cook</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boat of Garten - The Boat Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This brave man was murdered while trying to save a fellow worker from an assault. He now periodically appears in the hotel.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Future Train</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bonar Bridge - Site of railway line<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Two men walking through the area watched a train pass them by. This was, however, years before railway tracks had been laid through the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Herbivore Heist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brackletter - Field of the White Cow<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Fairies stole a white cow but returned to the farmer's field every night to let it graze. The farmer and his dog would give chase, but the fairies would escape by throwing bread that the dog would stop to eat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broadford, Isle of Skye - Bay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A doctor out for a walk along the coast watched a light slowly drift across the sea and land nearby. It changed into a cloaked woman holding onto a small child, before vanishing completely. When the doctor told his story at the inn where he was staying, the landlord explained that a few years previously two bodies had been washed up on the coast - that of a woman and child.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broadford, Isle of Skye - Broadford Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Mist like figures have been reported here in the past, as have a former housemaid, although the hotel now appears quiet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Flying Squid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broadford, Isle of Skye - Coast near the area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 January 1952<br>
              <span class="w3-border-bottom">Further Comments:</span> Constable John Morrison kicked a squid he found on a beach, only for the creature to wrap a 180 centimetre tentacle around his ankle. The policeman managed to remove his boot and killed the cephalopod with rocks and a set of shears.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Famh</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cairngorn Mountain Range - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This author is not sure if this is a joke, but... the Famh is said to be a strange dog/mole hybrid (Dole? Mog?), which possesses a vicious set of teeth and a disproportionately sized mouth. It lurks in the mountain range, shying away from human contact. The area is also haunted by a piper, who is seen standing on the tallest peaks, and is heard playing at night.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 235</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=235"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=9&totalRows_paradata=235"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
