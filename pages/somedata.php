

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database - Somerset &amp; Bristol records</title>
<meta name="description" content="Bristol and Strange Somerset - Ghosts, Folklore and Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Somerset & Bristol</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Somerset and Bristol Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Running Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A37? - Heading towards Glastonbury<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2001 (?)<br>
              <span class="w3-border-bottom">Further Comments:</span> While driving fast along a straight stretch of road, a driver his wife and child spotted a young boy holding a goldfish bowl run up an embankment at superhuman speed and into the road. The driver hit the brakes as the boy passed in front of the car, thinking it was too late to avoid collision, but there was nothing. The boy had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/0574.jpg' class="w3-card" title='Illustration of the Old Leigh Courthouse ghost from the book The Astrologer of the Nineteenth Century.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Illustration of the Old Leigh Courthouse ghost from the book The Astrologer of the Nineteenth Century.</small></span>                      <p><h4><span class="w3-border-bottom">Suicide</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abbots Leigh - Old Leigh Courthouse (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The courthouse was supposed to be haunted by a woman who cut her own throat. The bangs and crashes which her spirit created were so alarming that parts of the building were locked up at night. The courthouse was later demolished. The creative image of the ghost is from 'The Astrologer of the Nineteenth Century' (1825).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dragon Slayer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aller, Curry Rivel - Exact area unknown<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The village is named after a local hero who slew a dragon but died as the beast released a final breath of flame. In another version of the tale, the hero survives and finds a brood of hatchlings in the dragon's cave, which is subsequently blocked up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elizabeth Maronne</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Almondsbury - Bowl Inn and other areas in the village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Elizabeth was said to have been abused by her father and died young. Her phantom voice has been heard singing nursery rhymes or otherwise sobbing gently. The inn also claims two other female ghosts, though little is known about them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Redcoat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Axbridge - George House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This property was once thought to be haunted by the ghost of a soldier in a redcoat carrying a musket. A neighbouring shop reported hearing pacing footsteps, as if someone were guarding the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tabby Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Axbridge - King John's Hunting Lodge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> This feline apparition has been observed padding around on the first floor, vanishing when it sits down. A white Elizabethan lady is also reported to haunt the same floor, seen sitting in a chair.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Trio</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Axbridge - Shute Shelve Hill, Hanging Field<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The three ghosts lurking here, two men and a woman, were all hanged for murdering the lady's husband.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sight and Sound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Backwell - New Inn (no longer open)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-2015<br>
              <span class="w3-border-bottom">Further Comments:</span> Patrons at this inn would report seeing apparitions, hearing noises and footsteps, and having objects moved around rooms by unseen hands.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cloaked Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banwell - Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> One witness recalled, as a child, a figure who led him across his bedroom at night and helped him throw up (he was suffering from an illness at the time). There are also reports of phantom footfalls in the hallways and of cloaked figures being seen moving about the abbey.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Beast of Banwell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banwell - Banwell Hill<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> September 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> While walking her dog, Helen Stokes encountered a large black creature that she described as 'very fast'. Both owner and pet ran away from the creature.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blown Cross</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banwell - Hillfort<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> It was believed that the villagers tried to erect a massive cross on the hill, though the Devil visited every night and blew it down. Exasperated, the villagers carved the cross into stone, preventing further destruction. It is now thought the cross was the base of a Roman hillfort.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stubborn Bell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barlinch - Barlinch priory (now ruins)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> When the priory bells were removed from the site, the Great Bell did not want to leave its location. Eventually the bell was dragged away (after a single unhappy peal) by man and ox to its new home in Exeter Cathedral, where it promptly cracked once mounted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blacksmith</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Batcombe - Burn Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A giant blacksmith is said to appear if you call out his name while standing on this hill.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chained Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Batcombe - Gold Hill<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Buried in nearby woodland by an owner, this phantom hound is now reputed to roam the area, a large chain around its neck.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9510.jpg' class="w3-card" title='Henrietta Street, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Henrietta Street, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Robinson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - 20 Henrietta Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The disembodied footsteps occasionally heard on site are attributed to Admiral Robinson.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9184.jpg' class="w3-card" title='Pulteney Street, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Pulteney Street, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Howe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - 71 Pulteney Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in his naval uniform, Admiral Howe has been observed and heard moving around his former home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9508b.jpg' class="w3-card" title='Bath Abbey, Somerset.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bath Abbey, Somerset.</small></span>                      <p><h4><span class="w3-border-bottom">Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 August 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Phantom monks are reputed to haunt this area. A photograph taken by Russ Cribb on Flickr purports to show a ghostly cowled figure standing in front of the abbey's main door.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some2988.jpg' class="w3-card" title='Abbey Church, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Abbey Church, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Naked Roman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Area around Abbey Church, and other areas within the town centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The figure of a naked Roman soldier (how do you tell without a uniform to assist you?) has apparently been seen running around the centre of the town. On one occasion, a police officer gave chase, but the pursuit abruptly ended when the figure faded into the air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some2080b.jpg' class="w3-card" title='Bath Assembly Rooms, Somerset.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bath Assembly Rooms, Somerset.</small></span>                      <p><h4><span class="w3-border-bottom">Admiral Phillip</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Assembly Rooms, Saville Row and 19 Bennett Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Phillip, the first governor of New South Wales, has been seen dozens of times over the past fifty or so years, always wearing the same black hat and long cloak.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9513b.jpg' class="w3-card" title='Royal Mineral Water Hospital, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Royal Mineral Water Hospital, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Nurse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Bath Royal Mineral Water Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to have committed suicide on the site, this grey phantom nurse has vanished as of late.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tiny the Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Beckford's Tower, Lansdown Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> William Beckford's pet dog, Tiny, was once buried by the tower, though the creature's ghost returned after the remains were moved. It is also said that William's ghost haunts the graveyard surrounding the tower.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bunty</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Beehive public house (currently Grappa's Bar)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A former Victorian worker at the pub, Bunty makes fleeting appearances around the hallway dressed in a blue/grey gown. Her presence was said to be non-threatening.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monkey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Brassknocker Hill and surrounding area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1979 / 1980<br>
              <span class="w3-border-bottom">Further Comments:</span> There was a brief flat of monkey sightings here, the creature said to stand between 3 - 4 feet high (90 - 120cm) and had bright rings around its eyes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Crown Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in a First World War uniform, this phantom would appear in a darkened corner sipping his drink. The haunting is said not to have lasted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some3240.jpg' class="w3-card" title='Crystal Palace pub, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Crystal Palace pub, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Crystal Palace public house, 10 Abbey Green<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure is reported to materialise when building work is carried out inside the property. The ghostly monk is said to be quite transparent.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 413</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=413"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=16&totalRows_paradata=413"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
