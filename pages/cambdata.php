
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Cambridgeshire Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Cambridgeshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Cambridgeshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Benjamin Jones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alconbury - Road between Alconbury and Alconbury Weston, near the church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered by a soldier (Gervase Matcham), the ghost of the little drummer boy, Benjamin Jones, was said to be heard walking and drumming behind anyone travelling this stretch of road after sunset.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alconbury - Road in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This stretch of road is reportedly the haunt of a nun who steps out in front of incoming cars and causes them to swerve. There is also another report of three ghostly crashed cars that silently burn at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alconbury - The Megatron (restaurant, demolished 2008)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A flying saucer-shaped restaurant (sadly no longer standing) became home to a poltergeist that knocked on walls and threw items around. The owner of the restaurant remained sceptical until he heard footsteps that followed him around late at night. The spooky activity ceased after the performance of an exorcism.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hood & John</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alwalton - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Two stones in this churchyard are named Robin Hood and Little John, both thought to be markers where arrows fired by the famous bandits fell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Red</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arringron - Hardwick Arms hotel, Ermine Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> A lady in red is reputed to haunt a guest room, while a monk is said to manifest by the fireplace next to the bar. Poltergeist-like activities have also been reported in the dining room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Mist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arrington - 2 Croydon Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Grey mist being the only physical manifestation of the haunting, a family living here were also terrified by objects moving on their own accord, footsteps and bangs coming from every corner of the house, and an invisible force prodding their beds at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chatty Fellows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balsham - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend tells of talking skulls found in this graveyard, though what became of them is not known.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shug Monkey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balsham - Wratting Road<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> For reasons unknown, the shuck which has been observed in this area is believed to have a bald head and wide eyes, like that of a monkey, while the body is that of a large black shaggy dog. It leaps out in front of cars travelling along this road, only to dart out of the way at the last possible second, either on all four legs or standing upright.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnwell - Old Abbey House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nun unknown, poltergeist outbreak in 1959<br>
              <span class="w3-border-bottom">Further Comments:</span> This grey misty ghost drifts slowly around the house; the nun is said to have come here during her life to secretly meet her lover. There was a brief poltergeist outbreak in 1959, with bedcovers being violently removed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cromwell's Wealth</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bartlow - Bartlow Hills, aka Three Hills<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Hills still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A chest full of treasure is said to have been hidden by Oliver Cromwell in this area. It has also been said that people have reported feeling something brush against them while on one of the hills.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">B-17 Crew?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bassingbourne - Former RAF Bassingbourne<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s, 1994, and 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> There were a handful of reports of a phantom aircrew made during the 1960s, at this base once used by the USAAF. The B-17 may have been seen in 1994, moving silently overhead. More recently, a man who lived on the site of the former airfield reported seeing a pair of legs in dress uniform which manifested in his home, and the sounds of aircraft starting up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Observing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bluntisham - Former public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2007 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> An occupier of this property has reported the feeling of being watched and of doors closing themselves, although the presence is non-threatening.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Farmer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bourn - Area around Wysing Arts Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom farmer reputedly wanders the fields around the area looking lost and frustrated. He is said to be occasionally joined by the smell of burning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gervase Matchem</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brampton - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Matchem was executed at Huntingdon and gibbeted in Brampton after murdering a young drummer boy in the village. Matchem's spectre was (or maybe still is) reputed to walk around the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dancing Statues</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burrough Green - School and village green<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 April (evening) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year the two statues standing above the door of the school come alive and dance on the village green - sometimes their footsteps are visible in the grass.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shove</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burwell - Burwell House<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2001<br>
              <span class="w3-border-bottom">Further Comments:</span> Visiting the house for a party, one guest was shoved from behind by an unseen force. The guest had been standing with their back to a table, which in turn was against a wall; only one other person was in the same room at the time, who stood in front of the guest.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ghostly Black Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burwell - Burwell to Snailwell - path southeast, by a belt of trees leading to the A11<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Another shuck legend, though details are sketchy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Geoffrey de Mandeville</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burwell - Ruins of Burwell Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Windy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed while trying to storm the castle with a gang of thieves and robbers, de Mandeville dying words can sometimes be heard on the wind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam353.jpg' class="w3-card" title='Abbey Road, Abbey House, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Abbey Road, Abbey House, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">A Nun and Brown Dog-Thing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Abbey Road, Abbey House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth and twentieth centuries<br>
              <span class="w3-border-bottom">Further Comments:</span> It is claimed that Abbey House is, or at least was, the most haunted house in Cambridge. While I'm not a fan of 'most haunted' titles, when you look at the number and type of the ghosts supposed to haunt the site (a poltergeist, ghostly echoes of chains, a butler, a woman in white, a grey lady, a squirrel, a dog, and a hare all amongst the entities which have popped up), one can understand why the title has been applied here. An exorcism was performed in the 1980s, which may have laid the entities.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drug Induced Ghost</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Addenbrookes Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The story is that a ghost appears to those who are being administrated morphine; the psychological effects of the drug would appear to be ignored in this tale...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam240.jpg' class="w3-card" title='Arbury Road, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Arbury Road, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Unlucky Shuck</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Arbury Road<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This road, and area, are said to be frequented by a shuck that brings ill luck to all that see it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/camb4362.jpg' class="w3-card" title='Haunted studios of BBC Radio Cambridgeshire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Haunted studios of BBC Radio Cambridgeshire.</small></span>                      <p><h4><span class="w3-border-bottom">Womaniser</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - BBC Radio Cambridgeshire, studio 1a<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> The studio is reportedly haunted by an old man renowned for his womanising ways - he was never seen, though his presence felt. In the early 2000s the BBC invited local ghost hunters and psychic mediums to investigate. Armed with crystals, dowsing rods, a piece of paper on a string, and an EMF meter, the attempt to reach the other side resulted in both male and female presences being detected.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Second World War Pilot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Cambridge Airport<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A few buildings on this site have reports of phantom pilots in their flying gear or RAF uniforms. Sounds of footsteps in empty areas are not uncommon, and there is also a story of ghostly singing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Police Officer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Chesterton Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 August 2012, 14:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Taking a shortcut through a car park and past an office block, this witness looked up to see a police officer in 1940s uniform with a gasmask case slung over his arm. The officer was floating. The witness froze briefly in terror before taking haste.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam55.jpg' class="w3-card" title='Christ&#039;s College, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Christ&#039;s College, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Mr Round</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Christ's College, Mulberry Tree in Garden<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight on nights of the full moon (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A tall, elderly gentleman wearing a beaver hat, Round is said to have murdered the only doctor who had the skill to save his dying girlfriend - thus his ghost walks the grounds in the area around the mulberry tree in regret.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 202</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=202"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=8&totalRows_paradata=202"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
