



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Black Shuck, Devil Dogs, and Other Hell Hounds from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Shucks and Hell Hounds</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Black Shuck and his Kin from across the Isles</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A684 (Yorkshire) - Between Northallerton and Leeming Bar<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2001, between 20:00h - 22:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A large black dog ran in front of two women travelling to Leeming Bar by car. The driver closed her eyes and braked hard, expecting to hit the creature. The passenger watched the hound pass through the bonnet, and noticed the creature had no facial features, floppy ears, and was shadow-like. A man that the women spoke to once they reached their destination later killed himself - was the hound a portent?</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth (Dyfed) - General area of Penparcau<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This headless dog was said to have once belonged to a young giant who ran so fast that he decapitated the dog by pulling too sharply on the leash.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Eyed Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ailsworth (Northamptonshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This huge hound possesses eyes that burn as bright as the sun, making the creature easy to spot if he tries to sneak up on you.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Alfriston-downs.jpg' class="w3-card" title='The phantom hound of the Downs.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The phantom hound of the Downs.</small></span>                      <p><h4><span class="w3-border-bottom">Running Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston (Sussex) - Between the Downs and Town Fields<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Full Moon (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A large black dog is said to run from the Downs to Town Fields - here it stares over a flint wall before retreating the way it came.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston (Sussex) - The White Way, road leading from the town towards Seaford<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This creature is reportedly the dog of a young man murdered while out walking during the 1700s - because the pitiful hound sat by the victim's shallow grave, the criminals killed it also. The haunting is said to have ceased after the bodies of dog and owner were discovered when the road was widened.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Long Necked Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Algarkirk (Lincolnshire) - Area around the village church<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This black dog was described by a witness as being tall and lean, with a long neck and a protruding muzzle. The hound is not regarded here as an ill omen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dog in Trees</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Algarkirk (Lincolnshire) - Church<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This large black hound was seen between three trees that grow close to the religious house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allington (Wiltshire) - Area between village and All Cannings<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The story of the black dog was used to frighten children in this area. One man may have been chased by the creature (though it was dark and stated it might have been a pony), while another couple who spotted the hound said it stood almost as tall as their pony.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Greyhound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alphamstone (Essex) - Sycamore Farm<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> One night during the late 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> After seeing a creature in his chicken coop, a farmer fired a shotgun at the creature - it ran through the wire netting and escaped. Examining the scene in the morning, the farmer could find no holes in the netting or footprints of the dog.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alveston (Warwickshire) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Late Nineteenth Century<br>
              <span class="w3-border-bottom">Further Comments:</span> Charles Walton (the man murdered in Lower Quinton (see Quinton database entry for further information)) claimed to have seen a phantom black dog for several successive nights when he was younger - on the last night, instead of the dog, he saw a ghostly headless woman. He soon heard that his sister had died at roughly the same time as the last sighting.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alveston (Warwickshire) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered alongside his peddler master, this glowing white hound was said to haunt the lanes close to Alveston.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Greyhound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amlwch (Gwynedd) - Stone circle between village & St Ellian Church<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom hound was said to haunt this prehistoric site - it beat up a local preacher a couple of times before moving on to a further plane of existence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Barguest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appletreewick (Yorkshire) - Trollers Gill (or Ghyll)<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Possessing glowing red eyes and a yellow tint to its fur, the dog-like barguest is a harbinger of doom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ardura-Mull.jpg' class="w3-card" title='A ghostly black hound sitting in the mist.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly black hound sitting in the mist.</small></span>                      <p><h4><span class="w3-border-bottom">Death Prediction</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardura, Isle of Mull (Argyll and Bute) - Area around Lochbuie House<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1909<br>
              <span class="w3-border-bottom">Further Comments:</span> The black dog around this area would warn of impending death. One version of the story says that dog carried a puppy on the back of its head. In 1909, Dr MacDonald spotted the dog while treating chief Murdoch Gillian MacLaine - his patient died soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shaggy Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot (Berkshire) - Old Huntsman's House<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It has been reported that this house is haunted by a sheepdog-looking entity, which has also been heard barking. Folklore said a man in a red coat also haunted the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesbury (Buckinghamshire) - Unnamed field in a village close to the area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> For several nights, a man walking to milk his cows in the early hours of the morning encountered a large black dog which blocked his path. He finally grew tired of having to walk around the hound and, while walking with a friend, struck the creature - the dog vanished but the man fell down paralysed, never to be able to move or speak again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ayr-wooded-areas.jpg' class="w3-card" title='A strange black dog standing in the mist.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A strange black dog standing in the mist.</small></span>                      <p><h4><span class="w3-border-bottom">Black Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ayr (Ayrshire) - Wooded areas<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Mistakenly blamed for killing a child, it was later discovered that the dog had tried to defend the infant from a wolf (a la Gelert). However, this hound was less forgiving for its former master's murderous action and now runs around the dense woodland.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Webbed Footed Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baildon (Yorkshire) - Slaughter Lane<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A large dog with eyes the size of saucers once haunted this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Yellow Eyed Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balbriggan (County Dublin) - Unidentified estate road<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa Christmas 2021<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness watched a large black dog with yellow eyes cover eighteen metres in just three strides. Over the following two months the witness lost two members of their family.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moddra-na-Craeuv</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyandrew (County Cork) - Glen of Croke, which stretched from Saffron Hill to Pinegrove<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom woman who once appeared here was said to be a Creagh girl who died shortly after her lover was killed by her father. The Moddra-na-Craeuv, also known as the dog of the Creaghs, also haunts the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Trotting Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballygar (County Galway) - Lane in the area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1913<br>
              <span class="w3-border-bottom">Further Comments:</span> Potentially last seen by a cyclist, the black dog which haunted this area would vanish into thin air as witnesses looked on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shug Monkey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balsham (Cambridgeshire) - Wratting Road<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> For reasons unknown, the shuck which has been observed in this area is believed to have a bald head and wide eyes, like that of a monkey, while the body is that of a large black shaggy dog. It leaps out in front of cars travelling along this road, only to dart out of the way at the last possible second, either on all four legs or standing upright.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Barberbooth-General.jpg' class="w3-card" title='A phantom Collie-like dog.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom Collie-like dog.</small></span>                      <p><h4><span class="w3-border-bottom">Black Collie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barberbooth (Derbyshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom dog stands as tall as a very large collie and is said to belong to the fairies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rough Coated Dog with Large Yellow Eyes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barham (Suffolk) - Along the main Norwich Road, Barham Church Lane, just past Barham Hall gates.<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1910s?<br>
              <span class="w3-border-bottom">Further Comments:</span> The dog harassed a couple of men walking back from work - one of them reported trying to hit the creature with a stick, but the piece of wood passed straight through the dog's body. Another version of the story states the dog disappears into a solid wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shuck Haunt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnby (Suffolk) - Bridge over Hundred Stream<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Legend states the stream to be the haunt of a Shuck, though no further information could be found.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 377</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=377"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=15&totalRows_paradata=377"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
