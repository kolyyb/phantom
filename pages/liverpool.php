
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Liverpool Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northwest.html">North West</a> > Liverpool</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Liverpool Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pacing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - 44 Penny Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Friday, Saturday & Monday nights (reoccurring), 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> It was reported in the local press during the 1970s that footsteps could be heard within this building coming from empty rooms, although a former worker at the site said that the story was a publicly stunt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Liverpool-Wimborne-Road.jpg' class="w3-card" title='More of a skull than a face.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> More of a skull than a face.</small></span>                      <p><h4><span class="w3-border-bottom">Old Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - 82 Wimborne Road, Huyton (house no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1958<br>
              <span class="w3-border-bottom">Further Comments:</span> A family living here were terrified by a phantom old lady who stood five foot tall, bend over with a walking stick. Her face was little more than a shrivelled skull. Builders working close to the site quit once they heard an exorcism had taken place. The house has since been demolished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Small Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - 9 and 11 South Hunter Street (houses demolished in early 1960s)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1953<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by a mother and daughter on separate occasions, this phantom was described as a small woman wearing a lace cap and soft shawl. She was semi-transparent and vanished when approached.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Adelphi Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970<br>
              <span class="w3-border-bottom">Further Comments:</span> George has been seen several times standing by beds on the fifth floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Glowing Ball</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Allerton Hall, 	Clarke's Gardens<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2014<br>
              <span class="w3-border-bottom">Further Comments:</span> The hall is reputedly haunted by former residents and ghost hunts on the site have turned up flashes of light and sightings of glowing balls. An 'orb' caught on camera featured in the press in 2014.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Sarge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Ambulance training centre, Quarry Street, Woolton<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> For many years the building was reputedly haunted by an entity known as 'The Sarge' - a former policeman killed when the wheel of his patrol wagon struck a bollard and he was thrown to the ground. Ambulance crews reported phantom footsteps, breaking windows and doors which unlocked and opened themselves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flag Waver</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Area around former Otterspool rail station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Before closing in 1951, a teenager was saved by a man from being hit by a train, although the rescuer was killed in the process. He is said to wave a red warning flag in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Time Slip</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Bold Street<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1999<br>
              <span class="w3-border-bottom">Further Comments:</span> On various dates, several people (including an off duty policeman) have reported slipping back in time along this road, finding them surrounded by people dressed in clothing of the 1940s, with a cobbled street and old style shops.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Spookside</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Brookside Close<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> It was reported in the local press that the cast of 'The Close', a low budget British horror film being filmed at the former soap opera site, regarded the set as haunted. Lights would flicker out at inopportune moments, items would disappear, and the final scene of the film was constantly cancelled as bad weather would come from nowhere.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Coach and Horses public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former landlord, George still returns to keep a watchful eye on his former establishment.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Earl of Sefton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Croxteth Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century, camera in April 2009, most recently July 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> The Sixth Earl of Sefton has been sighted walking through the tearoom and in his former bedroom. An incorporeal young boy was once seen standing by the fireplace in the dining room, while other strange shadows have been reported drifting down various corridors. Footage from a CCTV camera in April 2009 reputedly shows a phantom figure moving across the grounds, though the image is more likely to be a close-up of an insect. Two people exploring the basement in 2013 encountered a short hideous figure in a cloak, before later spotting the figure on the staircase where he vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gin Runner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - David Lewis Northern Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Beaten to death nearby by Customs officers who caught the man trying to bring his illicit goods ashore, the ghost of the smuggler has been reported in the hospital that was built close to the site of his murder.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/jack003.jpg' class="w3-card" title='A nineteenth century illustration of Spring Healed Jack.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A nineteenth century illustration of Spring Healed Jack.</small></span>                      <p><h4><span class="w3-border-bottom">Spring Healed Jack</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Everton area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This area was one from which this enigmatic creature/entity was reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Former hotel along Sir Thomas Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s and 2001<br>
              <span class="w3-border-bottom">Further Comments:</span> A voice in an empty toilet was heard issuing warnings of future events, while a figure was seen passing a door and climbing the stairs when the three people in the building were accounted for. A local legend says that a woman was murdered on the staircase close to the toilets.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Former police and fire station, Lark Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> Footsteps have been reported on the rear staircase and a heavy door opens and closes unaided, according to the Night Vision Investigations team.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Older Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Former post office (currently a computer centre)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman in a checked red coat walked by two witnesses who were standing by a telephone box. The woman passed within thirty centimetres of the pair and proceeded to walk through the locked door to the post office.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Glasses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Head of Steam public house, Lime Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Glasses are said to be thrown from shelves and people feel something following them upstairs.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Baby Bouncer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - House owned by Liverpool Housing Trust, Madryn Street, Toxteth<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1992<br>
              <span class="w3-border-bottom">Further Comments:</span> A family living here with a poltergeist reported that the entity picked up their young son and threw him from his cot. The polt would also move the washing machine around the kitchen, hide money, and mash ashtrays.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Purple Faced Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Huyton, streets in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> One local myth says certain streets were home to a blue/purple faced young girl who would follow families with children.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Knowsley Hall and Riding Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom monk is said to haunt the area around the hall and the hill. A local legend says a secret tunnel ran from the monastery that once stood on the hill, and that the path the ghostly monk takes indicates the route of the passage.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swinging Swain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Lea Hall (or possibly Lee Hall) (no longer standing), Gateacre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Windy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A local villager who tried his luck with the squire's daughter found himself lynched from a tree close to the hall. On windy nights, the body could be heard swinging. A murder in the hall itself could be repeatedly heard, the moans of a woman and the laughter of her homicidal husband. A journalist who visited the hall between the world wars looking for ghosts to write about found himself frozen to the spot, surrounded by complete silence; he managed to escape and flee after hearing a clock striking in the distance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Torso</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Lime Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Sightings of the ghost of Lime Street are said to go back hundreds or years. One of the more recent witnesses described the entity as being quite tall, though the legs could not be seen, with the mouth silently moving. An unnamed hotel (that later became offices) along the road is also haunted by the figure of a tall woman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thomas Campbell Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Liverpool Airport, formerly known as Speke<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Black died in an accident on the runway of the airfield in 1936 - his shadowy form has been seen walking around the area where the old hangars stood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hitchhiker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Liverpool to Birkenhead tunnel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed in the middle of the twentieth century in the tunnel under the Mersey, this phantom hitchhiker is still occasionally seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Liverpool-Monks-Well.jpg' class="w3-card" title='The Devil in chains.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Devil in chains.</small></span>                      <p><h4><span class="w3-border-bottom">The Demons Below See</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Liverpool - Monk's Well, Wavertree<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present (but a flower bed)<br>
              <span class="w3-border-bottom">Further Comments:</span> This old well has the words 'Qui non dat quod habet, Daemon infra vide 1414' carved above it. It was said that any traveller passing would have no choice but to drink the waters and give a donation to charity, otherwise the Devil (chained under the well) would laugh.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 64</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=64"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=64"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northwest.html">Return to North West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
