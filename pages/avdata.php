


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Aviation Ghosts and Mysteries from The Paranormal Database.">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Aviation Ghosts</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Aviation Ghosts and Mysteries</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wellington</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdare (South Glamorgan) - Sky over Cwmbach estate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1979<br>
        <span class="w3-border-bottom">Further Comments:</span> A witness outside their home heard engines but could see anything. They stepped inside their doorway but as the sound of the engines grew louder the witness stepped outside again. Looking up, the witness spotted a twin engine, dark coloured Wellington bomber fly over and past, as 'real looking and sounding as any solid aircraft'. At the time, the witness thought nothing was strange about the encounter as they were unaware that there were no flying airworthy examples of a Wellington that existed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">RF398</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Albrighton (Shropshire) - Cosford Aerospace Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
        <span class="w3-border-bottom">Further Comments:</span> The last remaining Avro Lincoln Bomber situated here is said to be haunted by a pilot seen in the cockpit, while a tape recorder left on board overnight picked up the sounds of a busy airport, though the hangar and the area were devoid of life.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Landing Craft</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Albrighton (Shropshire) - RAF Cosford (now Aerospace Museum)<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1963<br>
        <span class="w3-border-bottom">Further Comments:</span> This ufo report was based on a story made up by two young people returning late from a leave pass. As time went on, the story grew, and it eventually became that an UFO had touched down near a hangar on the base, and was recovered and removed for reverse engineering.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Run and Shout</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anglesey (Gwynedd) - RAF Valley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970<br>
        <span class="w3-border-bottom">Further Comments:</span> Having just locked the main hangar, the key orderly heard a shout and the sound of running from within. He unlocked the hangar, but no one was could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pilot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ansty (Warwickshire) - Ansty Aerodrome<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996, 19:00h<br>
        <span class="w3-border-bottom">Further Comments:</span> A figure wearing a Second World War pilot's suit walked past one witness as he stood by a photocopier. The figure then passed through a pair of closed doors leading outside and disappeared in a field. The pilot is rumoured to be a Polish airman who hanged himself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lancaster Bomber</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnoldswick (Lancashire) - Area near Rolls Royce's Bankfield factory, and area towards Craven<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2004<br>
        <span class="w3-border-bottom">Further Comments:</span> Around thirty witnesses claimed to have seen a silent, grey coloured aircraft resembling a Lancaster Bomber moving silently through the sky. The accounts were virtually all isolated and spanned the month.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barton under Needwood (Staffordshire) - Former RAF Tatenhill, now a business park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2008-2010<br>
        <span class="w3-border-bottom">Further Comments:</span> A witness who works on the site has reported the smell of old aftershave in the corner of a warehouse which is only detectible at 20:30h, and the sounds of young men in disused buildings.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">B-17 Crew?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bassingbourne (Cambridgeshire) - Former RAF Bassingbourne<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s, 1994, and 2004<br>
        <span class="w3-border-bottom">Further Comments:</span> There were a handful of reports of a phantom aircrew made during the 1960s, at this base once used by the USAAF. The B-17 may have been seen in 1994, moving silently overhead. More recently, a man who lived on the site of the former airfield reported seeing a pair of legs in dress uniform which manifested in his home, and the sounds of aircraft starting up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">RAF Pilot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belsay (Northumberland) - A696, just outside of village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2015<br>
        <span class="w3-border-bottom">Further Comments:</span> Media outlets reported that Rob Davies and Chris Felton had filmed a figure which resembled a 'RAF pilot' standing on the roadside late at night. Although the footage was poor, the witnesses said the figure was dressed all in beige and held his arm out as if hitchhiking. When they turned the car around and drove back for another look, the figure had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/a_spitfire1.jpg' class="w3-card" title='Photograph of a spitfire flying overhead.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Photograph of a spitfire flying overhead.</small></span>                      <p><h4><span class="w3-border-bottom">Spitfire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Biggin Hill (Kent) - Biggin Hill Airport & village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 January (spitfire) (reoccurring)<br>
        <span class="w3-border-bottom">Further Comments:</span> Observed flying overhead, and sometimes heard, a phantom Spitfire haunts the skies of Biggin Hill - January would appear to have become its favourite haunting month. Airmen dressed in trench coats have been reported in the village; they stop people and ask directions before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Clubfoot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Binbrook (Lincolnshire) - Former Binbrooke RAF Base<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
        <span class="w3-border-bottom">Further Comments:</span> The nickname given to an Australian worker on the base who blew himself up trying to sabotage a Lancaster bomber during the Second World War was maybe a bit too unkind, and he was seen for years after his death walking around on the perimeter road. A ghostly NCO also made a comeback at the former base. Sergeant Sinclair managed the loading of aircraft bombs during the Second World War; a mistake caused a bomber to explode on take-off, killing Sinclair while he was trying to flag the aircraft down to alert them to the error. For a short while his ghost was seen on the runway, arms waving wildly in the air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Three Airmen Playing Squash</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bircham Newton (Norfolk) - Wartime Airfield (owned by Construction Industry Training Board)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s onwards<br>
        <span class="w3-border-bottom">Further Comments:</span> Many ghosts are said to exist here, mostly coming into being during the Second World War. During the war, a car full of 'merry' (ie, very drunk) pilots crashed into a hangar, killing the driver and all the passengers; the scene is now re-enacted at irregular periods. Another group of ghosts were called into being when a bomber that crashed on the landing strip lost three of its crew - they now return to play their favourite racquet game! The sound of a squash ball echoing in the deserted building, and that of soft footfalls are more common than seeing the airmen, though one man in officer's clothing has been seen a few times.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Flying Kit</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bitteswell (Leicestershire) - Asda distribution warehouse, Magna Park (site of former airbase)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
        <span class="w3-border-bottom">Further Comments:</span> The warehouse was constructed at the end of the runway at Bitteswell. It is reported that in the toilets is a permanent stain on the floor in the shape of a man, and an airman in flying kit has been repeatedly seen in there.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Trolley Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bletchley (Buckinghamshire) - RAF Bletchley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s (trolley lady), 2008 disembodied voice heard<br>
        <span class="w3-border-bottom">Further Comments:</span> Throughout the 1950s, patients would report a ghostly woman in a green apron pushing a trolley through the sick quarters, disappearing through a door. In 2008, during a visit to Bletchley Park, four visitors sitting in a car heard a disembodied voice giving instructions to fill in log sheets.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Avro Lancaster</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bonby (Lincolnshire) - Skies over the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 August 2015, 19:00h<br>
        <span class="w3-border-bottom">Further Comments:</span> A large aircraft moved silently over the treetops, so low that the witness thought it would land or crash into a field. It was later identified by the witness as an Avro Lancaster.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wet Footprints</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boscombe Down (Wiltshire) - RAF base<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1947<br>
        <span class="w3-border-bottom">Further Comments:</span> A pilot killed when his new aircraft crashed into the sea was heard walking about his hut at night, his wet footfalls slopping on the floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Manchester</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bottesford (Leicestershire) - Second World War Airfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
        <span class="w3-border-bottom">Further Comments:</span> It is thought that the sound of droning sometimes reported over the former airbase is that of a Manchester bomber - a high loss ratio ensured that these aircraft were later replaced with Lancasters. A phantom light source has also been reported moving in the tower, and a pilot in full kit was seen once.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hazy Airman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgend (Mid Glamorgan) - RAF Stormy Down (abandoned)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> June 2005 (man and son encounter), 08 April 2012 (photograph)<br>
        <span class="w3-border-bottom">Further Comments:</span> A man and his twelve year old son watched a hazy figure dressed in a Second World War uniform walking towards them, even though the figure did not actually move closer. The uniformed man then reached down to the ground as if to pick something up and slowly faded away. Other people have seen an airman near the former Hangar One and have also heard an air raid siren. A photograph taken in 2012 is said to show a phantom figure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Airman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burn (Yorkshire) - Wheatsheaf public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, but said to be most likely to appear on Mother's Day<br>
        <span class="w3-border-bottom">Further Comments:</span> This pub is said to be home to a phantom airman wearing a great coat, most likely to be seen on the fourth Sunday of Lent.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pipe Smoking Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burscough (Lancashire) - Burscough airfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1950s / 1960s?<br>
        <span class="w3-border-bottom">Further Comments:</span> When farmers worked the land where a Second World War airfield once stood, it was reported that they would occasionally be approached by a young man smoking a pipe. This figure would wish them a good evening before vanishing. It was said that this ghost was once a pilot who worked here, though no one can be quite sure of his true past.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Airman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burtonwood (Cheshire) - Old airfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
        <span class="w3-border-bottom">Further Comments:</span> An airman has been observed standing around with no head - it is thought that he was decapitated as he tried to bail prior to crash landing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Helicopter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buxton (Derbyshire) - Skies above Harpur Hill area<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1973-1974<br>
        <span class="w3-border-bottom">Further Comments:</span> A phantom helicopter was reported flying around the area at night several times over a two year period. Press and police were unable to discover who or what was behind the mysterious craft.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Second World War Pilot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge (Cambridgeshire) - Cambridge Airport<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
        <span class="w3-border-bottom">Further Comments:</span> A few buildings on this site have reports of phantom pilots in their flying gear or RAF uniforms. Sounds of footsteps in empty areas are not uncommon, and there is also a story of ghostly singing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wellington</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cannock (Staffordshire) - Skies over private area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000, 22:00h<br>
        <span class="w3-border-bottom">Further Comments:</span> A man sitting in his living room at home heard an old fashioned aircraft approaching. The sound grew louder and louder, finally deafening and items within the house began to shake. The man went out to investigate, and found other people outside trying to spot the aircraft. Despite the weather being clear, no one could see the aircraft, although one old man was convinced that the sound was of a wellington bomber.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sounds of Driven Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castle Donington (Leicestershire) - Donington Hall (currently Nottingham East Midlands Airport HQ)<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to death (coach), man and woman in January and November 2020<br>
        <span class="w3-border-bottom">Further Comments:</span> It was once said that when the head of the household hears a phantom coach approach but is unable to see it, a member of the family will die within a year. The hall is also reported to be haunted by a hand, possibly belonging to Lady Catherine Hungerford. During 2020 a figure was spotted on the road outside, vanishing when approached, and an air hostess wearing an old fashioned hairstyle silently moved along a hallway.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 158</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=158"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=158"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>

<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
