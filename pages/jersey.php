

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Jersey Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/otherregions.html">Other Regions</a> > Jersey</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Jersey Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Glowing Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crabbe - Field<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1991<br>
              <span class="w3-border-bottom">Further Comments:</span> A small group of people walking through the area spotted a glowing grey woman standing on a grass verge. The figure wore flowery clothing under a gown and showed no reaction when one of the group called out 'hello'. The witnesses panicked and fled.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/houguebie.jpg' class="w3-card" title='An old postcard showing La Hougue Bie on Jersey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing La Hougue Bie on Jersey.</small></span>                      <p><h4><span class="w3-border-bottom">Slay Lie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Grouville - La Hougue Bie<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Local lord Seigneur de Hambye killed a dragon in the marshes close to here but was then murdered by his squire. The squire took credit for killing the monster, and claimed the beast had killed de Hambye, but not before his former master requested that the squire marry his widow. Fortunately, the squire would sleep talk, and it was not too long before the truth slipped from his lips in the marital bed. He was hanged for murder. Seigneur de Hambye was buried under La Hougue Bie.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-14.jpg' class="w3-card" title='An old postcard showing St Aubin on Jersey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing St Aubin on Jersey.</small></span>                      <p><h4><span class="w3-border-bottom">Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Aubin - Unknown House (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The owner for this long gone house would murder French immigrants, their screams being heard around the neighbourhood, although the locals were too afraid to do anything about it. When the murderer finally died, the screams would continue to be heard around the area, until the house was demolished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-13.jpg' class="w3-card" title='An old postcard showing St Brelade on Jersey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing St Brelade on Jersey.</small></span>                      <p><h4><span class="w3-border-bottom">Cursed Wreckers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Brelade - Coastline<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The coastline here was once highly fertile until a group of wreckers lured several Spanish ships close to the shore. All the ships hit the rocks and sank, but not before a Spanish captain shouted out a curse that condemning the wreckers and the future of the land. A year to the day later, a massive wave hit the area, drowning all the wreckers and washing the topsoil away and replacing it with sand.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Bricks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Brelade - Parish church<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The church was going to be built over a mile away, but the workmen awoke the day the construction was due to start and found their materials had been moved closer to the sea. The men spent the day moving everything back to the original spot, but after the materials were once again moved the following night, the church was constructed where it now stands. The jury is divided on whether God, the Devil or fairy folk were responsible.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Brelade - Smugglers Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly woman in black is said to still haunt this building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Clement - Off the Coast<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A story says that people of the parish would hide at low tide when the sounds of a huge bull would be heard rampaging along the coast. A sceptical local fisherman investigated the sounds and discovered the noises were created by the sea pouring into a rock formation.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/History-of-Witches-and-Wizards.jpg' class="w3-card" title='From the book The History of Witches and Wizards, 1720.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> From the book The History of Witches and Wizards, 1720.</small></span>                      <p><h4><span class="w3-border-bottom">Devil's Print</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Clement - Rocqueberg, rocky outcrop<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> The area was reputedly the meeting place for witches and for summoning the devil. A footprint left by the devil is said to be still visible on a rock.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Helier - Broad Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A soldier dressed in ceremonial uniform was observed by a policeman walking into a door of an empty property and vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Raincoat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Helier - Conway Street and New Cut<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A policeman spotted a figure wearing a raincoat walk out of New Cut who looked a little strange. The policeman followed the figure as he walked and turned into Bond Street, but as the policeman turned into the road, the figure had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Helier - House along St James Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> A widow and her daughter were reported to have experienced some classic polt antics, including objects being thrown across rooms and a cold presence in the daughter's bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Helier - House along St Johns Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1930s and 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> This house was said to be so haunted by strange smells and sounds that no one would live in it for long. German forces took over the property in the 1940s, but once again, soldiers would move out within a few months of taking up occupancy. By the time people moved back in during the 50s, the haunting had ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Teacher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Helier - House along Winchester Street<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a teacher was briefly seen in the house where she lodged by a family member who had briefly moved back.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-15.jpg' class="w3-card" title='An old postcard showing St Helier on Jersey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing St Helier on Jersey.</small></span>                      <p><h4><span class="w3-border-bottom">Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Helier - Off the Coast, heading towards Minquiers<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It is said that in 1551 all the church bells in Jersey were removed and shipped to France. Unfortunately, they did not make it; the vessel carrying the bells sank shortly after departing. It is now said that anyone who hears the bells while out at sea will not be returning to land.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Helier - Property along Beresford Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This property was said to be haunted by an unpleasant elderly woman who perched herself on a chair at the top of the staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/bonnenuit.jpg' class="w3-card" title='An old postcard looking down at the bay of Bonne Nuit, Jersey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard looking down at the bay of Bonne Nuit, Jersey.</small></span>                      <p><h4><span class="w3-border-bottom">Petrified Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St John - Bonne Nuit<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Anne-Marie and William were lovers, but a sea sprite had also fallen in love with Anne-Marie and planned to kill William. The creature transformed itself into a white horse and waited in William's stable. William however had dreamt that the horse was dangerous, so when he mounted the creature in the morning, William ensured he had a branch of mistletoe upon himself. Sure enough, as William rode his horse close to the beach, the creature galloped into the sea to drown him. William whipped out his mistletoe and struck the sprite, turning it into a rock which remains to this day. Probably.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blinded</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St John - Lavoir des Dames<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The fairies which were believed to live bathe here would strike anyone blind who spotted them. The area is also said to be home to spooky noises, although the source is unknown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/waterworksvalley.jpg' class="w3-card" title='An old postcard of a river running through Waterworks Valley on Jersey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of a river running through Waterworks Valley on Jersey.</small></span>                      <p><h4><span class="w3-border-bottom">The Bride</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Lawrence - Waterworks Valley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year, on an unconfirmed night, a team of six horses draw a coach containing a faceless bride. During her lifetime, the bride is said to have jilted at the altar, resulting in her suicide a short time later. Now in death her ghost drives around the area looking for the man who betrayed her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Geffroy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Martin - Geoffroy's Leap (aka Geoffrey's Leap)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Geffroy was thrown off the cliff by a crowd for an unknown crime but survived the fall and swam back to land. The crowd argued whether to throw Geffroy back off or let him go. Geffroy volunteered to jump off, but the second time he died on the rocks below. His ghost is said to haunt the area regretting his error.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-116.jpg' class="w3-card" title='An old postcard showing Mont Orgueil castle on Jersey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Mont Orgueil castle on Jersey.</small></span>                      <p><h4><span class="w3-border-bottom">William</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Martin - Mont Orgueil castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> One charity ghost hunt at this castle channelled the spirit of William, executed in the seventeenth century for a murder that he did not commit.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman on the Edge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Martin - Private residence in region of St Catherine's Wood<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 07 November 2016<br>
              <span class="w3-border-bottom">Further Comments:</span> A man sitting on the bed noticed a woman who he assumed was his girlfriend also sitting on the edge, wearing a black dress. A few moments later, his girlfriend walked into the room... The witness was reportedly not scared by the incident, and found it calming.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/devilshole.jpg' class="w3-card" title='An old postcard showing the Devils Hole on Jersey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing the Devils Hole on Jersey.</small></span>                      <p><h4><span class="w3-border-bottom">Sea Sounds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Mary - Devils Hole<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A wooden devil figurehead from a ship once stood here, and although the first was stolen and burned, a modern replacement exist. Although some modern tales relate the crashing of the sea from the holes to legends, there is little to suggest people ever thought the noise was related to the Devil.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moaning Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Ouen - Greve de Lecq, rocks known as the Paternosters<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Prior to a storm<br>
              <span class="w3-border-bottom">Further Comments:</span> A ship on route to Sark collided with another boat and sank with all on board drowning. Several of the dead were children, and it is said that these youngsters can now be heard moaning when a storm front is moving in.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Ouen - Rue ŕ la Pendue<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1900<br>
              <span class="w3-border-bottom">Further Comments:</span> La blianche femme, or white woman, is reputed to haunt this road. She gave chase to a local man in 1900, who had to take shelter in a house close by.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Ouen - Vinchelez Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Phantom soldiers were said to haunt this lane.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 26</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=26"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=26"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/otherregions.html">Return to Other Regions</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Types </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report2.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>Â© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
