

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Tyrone Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northernireland.html">Northern Ireland</a> > County Tyrone</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Tyrone Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caledon - Road leading to Tynan<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom coach is said to travel the road in this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Strangler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cavankirk - Unnamed isolated farmhouse<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1890s<br>
              <span class="w3-border-bottom">Further Comments:</span> A brother and sister who ran this farm fled and moved to the US after it came under supernatural attack by an entity which took the form of another sibling who had died in Canada. Bangs and crashes were heard in the kitchen, and on two occasions, the entity tried to strangle the sister.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Clogher-Rectory.jpg' class="w3-card" title='A ghostly white lady.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly white lady.</small></span>                      <p><h4><span class="w3-border-bottom">Quake</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clogher - Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December 1885 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The rector wrote of a deep rumbling which manifested in the kitchen, like an earthquake, although no furnishings moved. The noise suddenly stopped and was replaced by smashing crockery in the pantry. Together with his servants, the rector approached the pantry, but before reaching the door, it opened and a white woman emerged. Pursued by the rector, the white woman moved upstairs and vanished in front of the rector's son. The woman appeared several more times afterwards, but never so dramatically.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moon or Hitchhiker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coalisland - Mullaghmoyle Road, Brackaville<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Reports of a white phantom in the form of a woman resulted in an approximately sixty car long traffic jam as people tried to spot the spook. Local Councillor Desmond Donnelly said the 'ghost' was more likely to be the moon reflecting in the nearby river.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Outline</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cookstown - Conway Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> The Mid Ulster Mail reported that a customer had photographed the outline of a figure within the pub, although they did not publish the image.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Robert Ponsonby Staples</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cookstown - Lissan House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir Robert believed that wearing rubber soled shoes was bad for one's health and would walk around his home in bare feet. It is unclear whether Sir Robert is responsible for the 'strange magnetism' that Hazel Radclyffe-Dolling reported coming from the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vague Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cookstown - Unknown council house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One bedroom here is home to phantom footsteps, while the indistinct figure of a man has been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Dungannon-Bridge.jpg' class="w3-card" title='Dungannon&#039;s ghostly older lady.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Dungannon&#039;s ghostly older lady.</small></span>                      <p><h4><span class="w3-border-bottom">Elle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dungannon - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An older looking woman wearing a gown, Elle is said to haunt the area around the bridge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dungannon - Deanos Hot Food Bar (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> While one worker claimed to have seen a woman walk through a wall which once led to an alleyway, it was more common for staff to experience a large iron door slamming shut unaided and lights to turn themselves off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dungannon - Gallows Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1963<br>
              <span class="w3-border-bottom">Further Comments:</span> A midnight search party was set up to find this ghostly figure, described as a tall man with staring eyes who screamed and yelled. The figure was never found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dungannon - Scotch Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom female warrior patrols the street clasping a spear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Working Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dungannon - Scotch Street, alleyway<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This alleyway is reputed to be haunted by the ghost of a Victorian prostitute.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure on Stairs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dyan - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2001-2005<br>
              <span class="w3-border-bottom">Further Comments:</span> A former resident here reported seeing a figure on the staircase, hearing voices while in bed, spotting an old man looking out of the window, and hearing marching along the road outside. A local historian informed them the road was used by soldiers marching to the Battle of Benburb.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Aunt Hessey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fintona - Ecclesville House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghost is never seen; only the rustling of her silken dress is heard in the drawing room. One guest once heard her playing the piano.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">German Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newmills - Monument to Robert Marrow<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1923<br>
              <span class="w3-border-bottom">Further Comments:</span> In the early hours of the morning, two figures wearing German uniform covered in blood appeared by the moment, where a German trench mortar had been placed. A group of men who witnessed the figures fled. A previous manifestation of the Germans had resulted in another witness fainting.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Clothing Shredder</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newtownstewart - Unidentified cottage, Brackey<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1874<br>
              <span class="w3-border-bottom">Further Comments:</span> Over the course of several months, the windows of Mr Allen's home were smashed, sometimes by stones but on other occasions, with no apparent cause. Bowls inside the house would be thrown off surfaces, clothing shredded, and heavy stones would bounce down the staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hum</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Omagh - General area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> October - November 2023<br>
              <span class="w3-border-bottom">Further Comments:</span> Many residents across the town reported hearing a hum that kept them awake, occasionally accompanied by vibrations. While the source of the problem proved problematic to track down, local councillor Stephen Donnelly was quick to say it was not aliens.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cricketer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sion Mills - Cricket Field (now abandoned),<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 October 2016<br>
              <span class="w3-border-bottom">Further Comments:</span> This man dropped dead during a cricket match. He is said to now appear at night dressed in his whites, carrying a bat. He may have been briefly spotted in 2016 by a witness walking her dog.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rattling Chains</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sion Mills - Mourne River<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of chains and phantom footfalls have been heard along the bank.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sion Mills - Old Railway Bridge, and river underneath<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> No longer in service, this bridge is thought to be haunted by the phantom of a suicide, which has been seen emerging from the waters below. The sounds of a train have also been reported coming along the track.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wailing Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Strabane - Rail station, engine shed (track no longer exists)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Four workers at the yard heard wailing, cries and loud bangs in one of the sheds. When the men investigated, they encountered a shadowy figure which glared back at them through the darkness. The ghost continued to create a commotion for two hours before vanishing, though it appeared again the following night to three of the men.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Trillick-Forest.jpg' class="w3-card" title='The ghost of the last wolf of Ireland.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ghost of the last wolf of Ireland.</small></span>                      <p><h4><span class="w3-border-bottom">Last Wolf</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Trillick - Forest and mountain in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom white wolf, thought to have been the last one killed in the area, haunts the mountain where she was born. The creature has reportedly been seen leaping out at a car as it passed some bushes and also observed by a family out picnicking.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 22 of 22</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northernireland.html">Return to Northern Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
