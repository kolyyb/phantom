


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Music Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Music</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghostly Tunes and Otherworldly Sounds</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bushy Bearded Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abbots Bromley (Staffordshire) - Royal Oak Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This cloaked, bearded gentleman was seen lurking around the attic and one of the bathrooms. A few unearthly notes from a non-existent music box have been heard in the bar area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blind George</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anstey (Hertfordshire) - Fields around the village, and Devil's Hole cave<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to still be playing his fiddle in tunnels under the village, Blind George ventured into the Devil's Hole cave with his trusted dog - the hound ran out, and was found with its hair burnt away, and George was never seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lost Piper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arbroath (Angus) - Dickmountlaw Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Emanating from deep underneath the farmhouse, piping can be heard - a musician named Tam once hid in a nearby cave complex, found himself lost, and is still looking for a way out.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Piper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardo (Aberdeenshire) - Gight Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> What is it with inquisitive pipers and tunnels? Playing his pipe while investigating this secret passageway, a man vanished without trace (though, of course, he is now heard).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Loud Heart Beats</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aspley (Nottinghamshire) - House along Melbourne Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1987<br>
              <span class="w3-border-bottom">Further Comments:</span> A family fled the building after being subjected to strange sounds, including deafening heart beats and mournful music played by an unplugged organ, and furniture which would move itself around. When a priest tried to perform an exorcism, his holy water threw itself on him. After the family left the property, they asked the council to rehouse them and were refused.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Athgreany-stones.jpg' class="w3-card" title='A fairy playing the bagpipes, surrounded by glowing lights.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A fairy playing the bagpipes, surrounded by glowing lights.</small></span>                      <p><h4><span class="w3-border-bottom">Bagpipes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Athgreany (County Wicklow) - Stone circle, aka Piper's Stones<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Played by the fairy folk, the sound of bagpipes could once be heard emerging from the stones. A tree in the centre of the stone circle is considered to belong to the fairies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Organ Plays</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Avenbury (Hereford & Worcester) - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1919<br>
              <span class="w3-border-bottom">Further Comments:</span> Several times, late at night, the church organ was heard to burst into tune, stopping as investigators reach the churchyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Piper Piping</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballintra (County Donegal) - Cave system known as Pullans<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Moonlit nights (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The sound of pipes echoes throughout this cave structure on certain nights, as the ghost of a piper tries to find his way out.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Band</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballitore (County Kildare) - Village Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1933<br>
              <span class="w3-border-bottom">Further Comments:</span> On two different occasions, villagers heard drums, brass, and a cornet being played in the village hall at midnight, although the building would be locked and unlit.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Music</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben MacDhui ((aka Ben MacDhui, Mac Dhui)) (Highland) - General area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1926<br>
              <span class="w3-border-bottom">Further Comments:</span> Debates in the media ensured after reports of ghostly music were published. While some favoured the supernatural, others claimed the sounds were created by wind blowing through rocky funnels caused by water erosion.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Scottish Piper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berwick upon Tweed (Northumberland) - Berwick castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly Scot plays his pipes while pacing up and down one of the ruined battlements.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Talking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenavon (Gwent) - Former nursing home<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2013?<br>
              <span class="w3-border-bottom">Further Comments:</span> While checking out this site, investigators heard a piano and children playing in an empty area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Red</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodiam (Sussex) - Bodiam Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, singing heard on Easter Sunday (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This spectre stands on the top of a tower, illuminated by bright moonlight, as if waiting for someone. A child haunts the bridge - the young boy disappears when he reaches the halfway point. More stories tell of phantom singing, music, and words spoken in an unknown tongue.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Coffins</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borley (Essex) - Church<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Ninetieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Music, crashes, thumps, bell ringing and footsteps have all been reported emerging from the church after it has been locked up for the night. A local tale involves the coffins in the Waldegrave family crypt mysteriously moving on their own accord between the times the tomb was opened.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Organ Music</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bowers Gifford (Essex) - St Margaret's Church<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness reported hearing the organ playing, though they were alone in the church at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman's Voice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradfield St George (Suffolk) - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February 2006, 04:15h<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple awoke to hear their baby gripe in the adjoining room. Before they could climb out of bed, they heard a strange woman's voice say 'hello' in a maternal fashion, followed by a 'Shhh...'. Bounding into the baby's room to challenge the woman, all that could be seen was the sleeping child. A couple of days later, toys could be heard playing tunes in the loft, although the music promptly ceased when the loft hatch was opened.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tinkling the Ivory</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradford on Avon (Wiltshire) - Fat Fowl restaurant<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> The piano in the restaurant is said to occasionally play a note or two under its own accord, while cutlery is moved, and doors close by unseen hands.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Secret Door</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brecon Beacons (Powys) - Llyn Cwm Llwch (somewhere around the area)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year, a secret door opens here which will take mortals to the fairy realm. The door was once always open, but after a man stole a fairy flower, access was restricted. The area is also home to an old woman who uses music to lure the weak willed into the lake waters where they drown - once she has killed nine hundred people, she will regain her youth and become mortal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Music</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Breedon on the Hill (Leicestershire) - Near the church<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 October 2012, approximately 19:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple sitting close to the church heard loud music, sounding like an ice cream van tune, although nothing was in sight. When the music started a second time, they left the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Music in the Air</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridport (Dorset) - Colmer's Hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 1939<br>
              <span class="w3-border-bottom">Further Comments:</span> Fairy music was once not uncommon in this area. While last heard in 1939 by a group of school children, one hundred years previous it was said a young girl danced herself to death to the tunes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/sus11446.jpg' class="w3-card" title='The ruined Brighton Pier.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ruined Brighton Pier.</small></span>                      <p><h4><span class="w3-border-bottom">Victorians</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton (Sussex) - Pier<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January, early 2000s, around 23:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> While walking along the seafront, two sisters watched Victorian figures walking around and listened to the sound of music and laughter. The seafront was lit up as if daytime, and a young man and a boy walked out to sea along a rope bridge. As the sisters walked away, they realised the scene they had watched was impossible, and turning back they saw only the darkness over the sea. One sister later discovered that there was a chain bridge on the spot which led to the pier, but it was destroyed in 1896.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Piano</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton (Sussex) - Planet (club)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A former member of staff reported that they and other staff have heard a piano being played when the club is quiet and no one else around.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Private-Flat-Tidy-Street.jpg' class="w3-card" title='Tidy Street in Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Tidy Street in Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Piano</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton (Sussex) - Private flat, Tidy Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A friend of the owner was awoken to see the owner's deceased husband standing over the bed. The site was also home to phantom piano music and poltergeist activity in a bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairground Organ</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol (Somerset) - Gardiner Haskins store (former soap factory)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2018<br>
              <span class="w3-border-bottom">Further Comments:</span> A few spooky events were documented in the local press, including the music from a fairground organ, a disembodied male voice, and the fleeting sighting of a Victorian woman dressed in white. A poltergeist was blamed for moving pots of paint around the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4708b.jpg' class="w3-card" title='Town library, Burnley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Town library, Burnley.</small></span>                      <p><h4><span class="w3-border-bottom">Piano Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley (Lancashire) - Town library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A piano in a lecture theatre here was seen to play itself and has been also heard several times since.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 168</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=168"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=168"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
