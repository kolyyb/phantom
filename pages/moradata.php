

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Moray, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Moray</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Moray Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Post Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridge of Avon - Ballindalloch Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen outside the castle, this woman carries a letter addressed to her lover that will never be delivered. Inside, the building is haunted by two female ghosts (each in different locations) and a former male owner who walks towards his wine cellar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Miller and Wife</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cabrach - Former mill near the town converted into a house named 'Milltown', close to Cabrach parish Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> After the death of the miller, the sounds of footsteps and dragging could be heard coming from the empty mill. When the building was converted, locals hoped that the haunting would stop, but it was not the case. The miller's wife has also been seen at least once, standing outside the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Corgarff-Well.jpg' class="w3-card" title='Take the gold and die.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Take the gold and die.</small></span>                      <p><h4><span class="w3-border-bottom">Kettle Gold</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Corgarff - Well at the base of a steep hill (unsure if still present)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Early nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This well was fed from three springs, one curing deafness, the second blindness, the third lameness, although the price of removing an ailment was gold. An entity administered the area, storing donations in a kettle which was buried under what was known as the Kettle Stone. Anyone who tried to remove the stone and steal the gold would die a terrible death soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hanging Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cromdale - Cromdale Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 09 October 1976<br>
              <span class="w3-border-bottom">Further Comments:</span> A photograph taken of the tree appears to show a white figure hanging from it. The tree was said to have been used to hang criminals and rebels.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Earl of Seafield</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cullen - Cullen House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> After accidentally killing a close friend, the Earl was so upset that he took his own life - but even this radical measure failed to end his distress, and so he remains earthbound.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Whipped Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cullen - Seafield Arms Hotel and surrounding roads<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom female reputed to haunt this site was whipped to death by her husband four hundred years ago. Her ghost is said to be bloodied and confused.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Two Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dufftown - Balvenie Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1994<br>
              <span class="w3-border-bottom">Further Comments:</span> As well as the phantom horses, the ruined building is also home to a phantom white lady.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Empty Train</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dunphail - Site of old railway line<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghost train is said to follow the route where the tracks once lay, and even though the vehicle's lights are all switched on, no-one can be seen inside.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Older Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Elgin - Bishopmill House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The local press reported that workers in the property had observed a ghostly older woman carrying a bag.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/elgin-cathedral.jpg' class="w3-card" title='Elgin Cathedral (public domain).'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Elgin Cathedral (public domain).</small></span>                      <p><h4><span class="w3-border-bottom">Patrick Sellar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Elgin - Elgin Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sellar is buried at the cathedral and is still thought to haunt the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/OI-087.gif' class="w3-card" title='An old woodcut of bagpipes.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of bagpipes.</small></span>                      <p><h4><span class="w3-border-bottom">Bagpiper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Elgin - Red Lion Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom piper has been heard playing his music on the second floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Caped Gentleman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Elgin - Safeway Supermarket<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1993<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in dark clothing, this tall figure is only seen during the early hours of the morning, though his laughter can be heard coming from the store's empty corners at any time of the day or night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/a_bonnie.jpg' class="w3-card" title='An old painting of Bonnie Prince Charlie.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old painting of Bonnie Prince Charlie.</small></span>                      <p><h4><span class="w3-border-bottom">Bonnie Prince Charlie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Elgin - Thunderton House Public House (former Royal lodging house)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> The Bonnie Prince stayed at this former hunting lodge for a week prior to the battle of Culloden as he recovered from an illness. His ghost has been reported within the walls of the building. Poltergeist behaviour has also been observed in the bar area and the cellar - one member of staff watched as an invisible hand swept the evening's takings from the counting table onto the floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Shadows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Elgin - York Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather dependent: said to become more active on stormy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen darting around the tower after dark, no one really wants to investigate these dark shapes any further. Screeching and laughter have also been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drinkers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Findhorn - Royal Findhorn Yacht Club<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> December 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Barman Ray Shepherd-Smith served two elderly men drinks after they claimed to be life members. After ringing up their drinks, Ray returned to see the men, but they had vanished. CCTV footage showed no one entering or leaving the bar at the time of the encounter. The club is also said to be haunted by founder James Chadwick.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Brodie of Brodie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Forres - Brodie Castle<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 September 1889<br>
              <span class="w3-border-bottom">Further Comments:</span> The night that Grandfather Brodie died in Switzerland, the sounds of moaning and loud moving noises could be heard coming from his locked office in the castle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Loud Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Forres - Dallas Dhu Historic Distillery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Like a few other Scottish distilleries, this old building is said to be haunted by a worker who drowned in a vat of whisky. His loud footsteps can be heard walking around the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Train</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Forres - Divie Viaduct<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Now a footpath, this disused rail viaduct is still home to a phantom train that passes by at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Five Floating Heads</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Forres - Dunphail Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> These heads belong to a small party who tried to break in and feed the starving besieged defenders of the castle; they were discovered and decapitated by their enemies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Slinky Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Forres - Fields near Dalvey House<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 January 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> While feeding their horses, two women spotted a long tailed black cat emerge out of nearby woodland. Several of the horses fled across the field, and when the women approached the black feline, it ran off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/three-witches.jpg' class="w3-card" title='Three witches with a cat, a dog and a bird. Engraving, circa 1800, after a woodcut, 1619.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Three witches with a cat, a dog and a bird. Engraving, circa 1800, after a woodcut, 1619.</small></span>                      <p><h4><span class="w3-border-bottom">Witches</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Forres - Sueno's Stone<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A Picto-Scottish standing stone, one legend says that Macbeth met the three witches by the stone (and that they were later imprisoned within it).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Broken in Three</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Forres - The Witch's Stone (outside police station)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This large stone marks the spot where women were murdered for being witches. In 1790, a local man broke the stone into three pieces to be used for building materials but ended up using metal strips to join it back together. However, a legend sprang up that the stone was broken when a witch tried to steal it; she dropped the heavy stone while flying away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Alexander MacAllister</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glenlivet - Altnachoylachan Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The site of a battle in 1594 between the troops of George Gordon, 1st Marquess of Huntly and Francis Hay, 9th Earl of Erroll, is haunted by the headless ghost of MacAllister riding on horseback - he fought briefly, although was decapitated by the first cannon shot.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pony Sized Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Grantown - A939, normally around the Glenlivet forest area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This strange hound stands as tall as a pony, has green tinted fur, and either has a very small head or no head at all.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Barbara</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Grantown-on-Spey - Castle Grant<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> In a protest about her arranged marriage, Barbara starved herself to death in a room in the tower; she is still seen glazing out of the window, sometimes washing her hands in a basin floating in front of her. A ghostly piper also patrols the lands outside the castle.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 39</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=39"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=39"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
