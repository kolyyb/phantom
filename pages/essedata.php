

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Essex Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Essex</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Essex Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dark Feline</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abberton - Abberton reservoir, south of Colchester<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> A large black cat was seen at the reservoir.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Greyhound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alphamstone - Sycamore Farm<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> One night during the late 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> After seeing a creature in his chicken coop, a farmer fired a shotgun at the creature - it ran through the wire netting and escaped. Examining the scene in the morning, the farmer could find no holes in the netting or footprints of the dog.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fighting Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashdon - Fields in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Moonlit nights<br>
              <span class="w3-border-bottom">Further Comments:</span> Perhaps connected to nearby Linton's Royalist stronghold, the fields in this area were said to be haunted by fighting phantom soldiers from the Civil War.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drowned Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashdon - Pond known as Lady Well (no longer present) in private residence along Dorvis Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local myth says that a pond which was once in a garden at this village was haunted by a phantom woman. She drowned in the pond after a storm caused her horse to bolt, throwing her into the water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moaning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashingdon - Hill on which Saint Andrews Church stands<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The hill is said to be the home of ghostly moans which belong to dying soldiers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Great Cry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashingdon - Lane winding past church<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The story goes that two men walking past the church had a heated argument which resulted in the murder of one of them. The killer, in turn, killed himself in an asylum; the haunting scream in the area is said to be his, rather than his victims.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bloodstained Hill on which No Grass Grows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashingdon - Saint Andrews Church, Hill on which it stands<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It is said that an incredibly vicious battle was fought on this site, the blood running so thick that no grass could ever grow here again. The hill is currently grassy...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Audley End - Audley End Road & Chestnut Avenue.<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The coach departs the Lions Gate and is driven along the local streets until it reaches the B1383. As normal, the coach driver is said to be headless.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unknown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aveley - Old Ship Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Campaign for Real Ale website mentions this site is haunted, although the finer points are not stated.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hairy Creatures with Large Pointed Ears</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aveley - Road in the area, exact location not known<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 October 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> While driving home, a family of four were abducted by a blue UFO; the occupants described as four foot high hairy beings, with large eyes and pointed ears. After 3 hours, the family were returned to their car.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Opening Doors</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barking - Eastbury House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A door in one of the rooms would open and close by itself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Checked Coat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barking - Thames View (Council Estate)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost's history unknown, he caused poltergeist activity in a council home, throwing toys around and slamming doors open at night. When visible, the spirit was often seen heading towards a bedroom of a four year old boy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blue Workman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basildon - Ambassador Bowling Club, Alley 17 (now a bingo hall)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> The figure of a man dressed in blue overalls has been observed on lane 17, standing by a bench. The machinery along the lane has a history of behaving erratically, and there have been reported drops in temperature.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basildon - Cash's Well (aka Vange Well No.5) and surrounding area,  Langdon Hills country park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2019 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The area around the well has gained a spooky reputation with disembodied screams heard in the surrounding woodland.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hit by a Cart</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basildon - Church Road, coming in from Clay Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Either one or two ghosts haunt this road - a spirit was blamed for throwing men over a bush as they returned home from the local pub, and the shade of a little girl has been reported, killed by a cart.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Raining Straw</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basildon - General area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 28 July 1992, early afternoon<br>
              <span class="w3-border-bottom">Further Comments:</span> Large piles of straw fell from the sky over the town one summer's afternoon. This may have been connected to the straw fall in South Wonston (Hampshire) on the same day.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Beige Dress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basildon - Goldsmith's Manor, Langdon Hills<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Reportedly haunting one of the attics, the lady is believed to have been the wife of a former owner. She is also blamed for various bangs and tapings around the manor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crimson Monk Floating without Legs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basildon - Vicinity of Holy Cross Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Workers at a nearby factory complained that this figure was appearing as they travelled to work early in the mornings, moving across the road on which they travelled into the grounds of Holy Cross church. Footsteps have also been heard into the church after hours, and the same figure has also been seen crossing the highway heading towards Laindon church. One report sent to the Paranormal Database alleges that the haunting was faked by a couple of friends who used a projector on the mist in the area to create the illusion of the red monk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beauchamp Roding - Church on top of hill<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> When the church was being constructed in the village, building materials were in short supply, so a large stone from the top of the hill was dragged down ready to be used. The following day, the stone had moved itself back to the top of the hill. The stone was brought down, only to relocate itself again the following day. The villagers took this as a sign the church should be built at the top of the hill, and that is why the church is not in the village itself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Hamilton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Benfleet - Conservative Club, High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghost, still waiting for Lord Horatio Nelson, reportedly patrols the corridors and rooms to ensure that he has not turned up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/vail.jpg' class="w3-card" title='A woman wearing a veil, illustrated by Wayne Lowden.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A woman wearing a veil, illustrated by Wayne Lowden.</small></span>                      <p><h4><span class="w3-border-bottom">Veiled Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Billericay - Burghstead Lodge (currently Registry Office & Library Archives), south-west room on first floor<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A nurse was attending to an ill man in this lodge. On two consecutive nights she spotted, standing over the man, a veiled woman in a green gown who mysteriously vanished. On the third night, the nurse approached the woman and removed her veil. What she saw is unknown, as the following morning she was found insane and her patient was dead. The nurse herself died a few months later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Green</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Billericay - Burghstead Lodge, High Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing to a nurse on three consecutive nights, this ghost took the soul of an elderly owner of the house and drove the nurse to suicide.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Billericay - Cheyne Library, 118 High Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a little girl has been seen on the stairs of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lioness</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Billericay - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 29 May 1982 (Lion), 2008 (black cat)<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by the same woman twice in one day, the lion-like cat was described as 'champagne coloured'. A photograph taken in 2008 shows what appears to be a large black cat, though without a scale reference, it may just be a small cat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Inspector</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Billericay - Police Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1976 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom Inspector was blamed for minor haunting occurrences, including a stapler which was thrown as an officer worked alone one night. The haunting ceased after a memorial was collated and displayed.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 522</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=522"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=20&totalRows_paradata=522"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
