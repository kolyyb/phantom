
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="How strange is Edinburgh? Very. Take a look for yourself in the Paranormal Database.">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Edinburgh</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Edinburgh Ghosts, Folklore and Forteana</h3>

 


                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/scot3872a.jpg' class="w3-card" title='Ann Street, Edinburgh.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ann Street, Edinburgh.</small></span>                      <p><h4><span class="w3-border-bottom">Mr Swan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - 12 Ann Street - private residence<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> As he drowned at sea hundreds of miles away, Mr Swan appeared to his family at the house and took the opportunity to wave goodbye before disappearing into thin air. Some report that he still returns once every few years to have a quick look in. As a side note, during the 1950s one house along Ann Street was said to be home to a white lady; it is unclear whether this is the same property or another.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Curse of the Mummy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - 15 Learmonth Gardens<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 1930s<br>
              <span class="w3-border-bottom">Further Comments:</span> The figure of an ancient priest was seen in the building after the family returned from a trip to Egypt - it was said that the ghost had accompanied a bone that had been illegally removed from a tomb by a family member. When the piece of skeleton was destroyed, the haunting stopped.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Yellow Eyes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - 25a Regent Terrace<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1979<br>
              <span class="w3-border-bottom">Further Comments:</span> The strange events which occurred in this shared flat included a voice which asked 'yes?', the sounds of a baby crying and heavy footsteps, the disappearance and reappearance of personal items, and the brief glimpse of a pair of narrow yellow eyes in the darkness.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Female Form</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - 36 Rutland Street<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December 1887, 07:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> A doctor who lived her reported seeing the apparition of a female patient he had seen the previous night. The doctor later found out the woman had died at the exact moment of his encounter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - 5 Hazeldean Terrace<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1957<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a Prestonpans soldier was said to haunt this site and performed various poltergeist tricks to obtain attention from the media.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gnomey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - 5 Rothesay Place<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom appeared shortly after the family purchased furniture which belonged to a recently deceased sailor. Another version of the story names the phantom as Merry Jack Tar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lorry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - A7 towards Stow<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-twentieth century to early twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom lorry has been blamed for causing a few accidents along this part of the A7. Other sources state phantom cars have also been encountered, including an orange Austin in 2010.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Area of Muirhouse Gardens<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A local paper reported that this area was haunted by an evil looking old man, with wild looking eyes and long matted hair.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Area where sycamore tree once stood, Dovecote Road, Corstorphine<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This white lady stabbed her lover to death by the tree, while fending off his attack. She was executed for the crime, but her ghost holding a sword would return to the tree where the crime was committed. Another version of the story says that she was murdered here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/scot5414.jpg' class="w3-card" title='Arthur&#039;s Seat, Edinburgh.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Arthur&#039;s Seat, Edinburgh.</small></span>                      <p><h4><span class="w3-border-bottom">Resting Giant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Arthur's Seat<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The rocky outcrop known as Arthur's Seat was once used as a resting place for a passing giant. In 1836 a group of schoolboys discovered seventeen tiny coffins hidden in the area, each containing a small carved figure inside. Their purpose is unknown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burning Light</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Baberton House (aka Kilbaberton house)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> One former occupant of the house reported that the Green Room was haunted, as lights would always be lit by something unseen. Another occupant, aged four at the time, had to be moved from their bedroom after they complained about the girl at the bottom of the bed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cold Spot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Bank of Scotland, George Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa January 1994<br>
              <span class="w3-border-bottom">Further Comments:</span> Two cleaners working alone in this building left the site after encountering disembodied footsteps, the sound of doors opening and closing, and an unnaturally cold room that enabled one witness to see their breath. A door that only the cleaners could access to had been deadbolted, even though none of the crew had touched it. One of the cleaners would later talk to a member of staff who said strange things happen on the site at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/scot10560.jpg' class="w3-card" title='The Banshee bar, Edinburgh.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Banshee bar, Edinburgh.</small></span>                      <p><h4><span class="w3-border-bottom">Rosie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Banshee public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Rosie was a prostitute who walked in the vaults beneath this pub and is now said to scratch and mark male members of staff. Another phantom, 'Six Finger Bill', sits under tables and grabs customer's ankles.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hound?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Beehive Public House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 16 July 2012, around 16:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Someone drinking at the pub felt a firm nudge on their left leg, as if a dog had pushed against them, although no canine was present.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/scot5410.jpg' class="w3-card" title='Bell&#039;s Wynd, Edinburgh.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bell&#039;s Wynd, Edinburgh.</small></span>                      <p><h4><span class="w3-border-bottom">Mrs Guthrie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Bell's Wynd - building no longer stands<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth Century<br>
              <span class="w3-border-bottom">Further Comments:</span> The body of Mrs Guthrie lay undiscovered in a room along this wynd for 21 years. Her ghost manifested itself to a locksmith who broke in, curious to know why the flat was not being used. Guthrie's ghost appeared along the Wynd for many years after her body was removed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman Holding Baby</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Bruntsfield Links, private house (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Reported by a servant who finally quit after seeing the apparition too many times, this phantom lady holding a young child had no head.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Building along Waterloo Place<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> A heating engineer reported several incidents while working on this site, including hearing footsteps after everyone else had left, keys removed from their hooks, cold spots, and objects which would disappear and reappear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Guard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Building on Leith Docks<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly security guard is reputed to haunt this site. A painter working alone heard a door opening and footsteps walking through the building - when investigated, no one could be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/scot4314.jpg' class="w3-card" title='Calton Hill, Edinburgh.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Calton Hill, Edinburgh.</small></span>                      <p><h4><span class="w3-border-bottom">Gateway</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Calton Hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Seventeenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> According to one story, dating back to the Seventeenth Century, a magical fairy gateway leading to France, Holland, and the fairy kingdom opened on the hill, thought which though only those with second sight could see and use the mythical entrance. A young lad claimed to have access to the gate and would act as a drummer to the many fairies who used the portal. The lad claimed to be needed on a certain date by the little people, and despite all physical efforts to force him to miss the meeting, the lad slipped away from his wardens and was never seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/scot9896.jpg' class="w3-card" title='Canongate, Edinburgh.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Canongate, Edinburgh.</small></span>                      <p><h4><span class="w3-border-bottom">Figure in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Canongate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> Standing tall, this figure in black was spotted by a witness as they walked towards each other. It vanished when they reached touching distance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Canongate area, Queensberry House (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This building was said to be haunted by the ghost of a young boy murdered (and partially eaten) at the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Royston</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Caroline Park House (also known as Royston)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1976 (babysitter's encounter)<br>
              <span class="w3-border-bottom">Further Comments:</span> This green tinted ghost is said to rise from the ground somewhere in the garden and drift slowly towards the front door before disappearing. Loud banging is sometimes heard coming from one of the rooms and is thought to be a cannon ball bouncing around - its motivation is unclear. A babysitter working here heard furniture being flung around in the room above her, although the following morning it was discovered that nothing had been moved.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/9893_lothian.jpg' class="w3-card" title='Castle Hill and The Royal Mile, Edinburgh.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Castle Hill and The Royal Mile, Edinburgh.</small></span>                      <p><h4><span class="w3-border-bottom">Deacon Brodie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Castle Hill and Royal Mile<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Brodie is reputed to walk this area, carrying a lantern. A coach pulled by a team of black horses (which sometimes breath fire) s also said to start its journey here before travelling along the royal mile. The coach is filled with people in black garb. Another phantom, General Dalzell, rides the same route, but this time on a white horse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Punch</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa early twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman visiting the cathedral felt three sharp blows to the back of her neck but was unable to see anything which may have caused the strikes. She spoke to a worker at the cathedral who told her that the blows had been reported previously, and that the person had later received bad news. Sure enough, the woman later discovered that her son had died in an accident which had occurred at the same time as the blows had been felt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/scot6896.jpg' class="w3-card" title='Chalmers Close, Edinburgh.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Chalmers Close, Edinburgh.</small></span>                      <p><h4><span class="w3-border-bottom">Invisible Energy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edinburgh - Chalmers Close<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 March 2006, around 20:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> An invisible force of energy hit a mother and son in the chest before rushing past them at the top of Chalmers Close - both were shaken by the event. A few seconds prior to this, the pair had seen a red headed girl running towards them - they stopped to let her pass, but she turned off.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 107</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=107"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=4&totalRows_paradata=107"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
