

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in the City of York, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northeastandyorks.html">North East and Yorkshire</a> > York</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>York Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Young Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - 18-20 Stonegate (currently Jack Wills)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local report says that a teenage girl using the changing room looked in the mirror and spotted another young girl. Believing that the girl had accidently walked into the changing room, the teenager turned and asked the girl to leave. The girl turned and walked through the wall. Staff later found out that a doorway once existed in the wall where the girl disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york6513.jpg' class="w3-card" title='A building along Stonegate, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A building along Stonegate, York.</small></span>        <p><h4><span class="w3-border-bottom">Young Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - 41 Stonegate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A six-year-old Victorian girl who fell to her death on the staircase was said to have haunted this building during the twentieth century. The girl could be heard ascending the staircase and was reputedly seen sitting upon a shop counter when a tearoom operated on the site. One story says the ghost is a doctor's daughter and dates to when the building was a private residence. During one of the doctor's dinner parties, the curious girl emerged from her top floor bedroom and tried to peer over the banister to see who was in attendance. She lost her balance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york1717.jpg' class="w3-card" title='Number 5 College Street, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Number 5 College Street, York.</small></span>        <p><h4><span class="w3-border-bottom">Crying Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - 5 College Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> During one of the outbreaks of plague in the city, a married couple contracted the disease and died. Their only child, a young daughter, remained uninfected. She shouted to passersby for help, but no one would believe that she was clear of the disease; the doorway was bricked up to prevent the plague from spreading, with the young girl and her dead parents still inside. The girl's crying could be heard for days, until the house grew quiet - the girl had eventually starved to death. Now she can be seen looking from the window, and the cries of a young girl heard emerging from the building, even though it is empty.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Woman with Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - A64 Malton Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Misty evenings<br>
              <span class="w3-border-bottom">Further Comments:</span> This tragic figure, possibly murdered by her highwayman boyfriend, now appears on misty or foggy evenings.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york3013.jpg' class="w3-card" title='All Saint&#039;s Church, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> All Saint&#039;s Church, York.</small></span>        <p><h4><span class="w3-border-bottom">Glowing Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - All Saint's Church, Pavement<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This pale young lass with long curly hair was seen a few times, always observing funerals from the church door. She vanished suddenly if anyone approached. Another description of the entry says she is dressed in black with a swollen red face.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Headless Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Area of Bishopthorpe<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A wealthy woman was murdered for her money, and her body hidden a short distance from her house in Bishopthorpe. It was several days before the corpse was discovered, and it is said that the state of decay was so bad that her head had become separate from her torso. Her headless ghost haunted the area where her body lay for years after the event.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york2048.jpg' class="w3-card" title='Bedern Arch, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bedern Arch, York.</small></span>        <p><h4><span class="w3-border-bottom">Child's Hand</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Bedern Arch<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The story goes that several orphans died in the nearby Industrial Ragged School during the mid-nineteenth century, due to the negligence of the alcoholic schoolmaster; now, on cold nights, people have reported walking by the arch only to feel a small child take them by the hand... Childish giggling and terrible screaming has also been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york1708.jpg' class="w3-card" title='Black Swan Inn, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Black Swan Inn, York.</small></span>        <p><h4><span class="w3-border-bottom">Legs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Black Swan Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen descending a staircase in the building, only the lower part of this male body appears. Two other spirits can be found in the bar area; that of a workman wearing a bowler hat (dressed in Victorian clothing) and a lady in a white dress who watches the fireplace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Castlegate House (or Lodge)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This shadowy feminine figure has been seen on the staircase of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york1705.jpg' class="w3-card" title='Church of the Holy Trinity, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Church of the Holy Trinity, York.</small></span>        <p><h4><span class="w3-border-bottom">Two Ladies and a Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Church of the Holy Trinity, Micklegate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century onwards (?) at morning service<br>
              <span class="w3-border-bottom">Further Comments:</span> These three figures are reported to be a mother, her child, and a nanny - the story is that mother and child were buried many miles apart, and now the nanny reunites the child with mother. Another story says that the women have no connection; the mother and child were united in death after the infant died of plague, and she slowly pined away, while the second woman is an abbess murdered trying to prevent Henry VIII's men from storming her church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york2771.jpg' class="w3-card" title='The Davygate area of York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Davygate area of York.</small></span>        <p><h4><span class="w3-border-bottom">Clash of Armies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Church Street & Davygate area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1958<br>
              <span class="w3-border-bottom">Further Comments:</span> Two women walking in the area claimed to have seen a large battle taking place around them, between Roman troops and barbarians. As a van drove by, the scene vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york3012b.jpg' class="w3-card" title='Clifford Tower, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Clifford Tower, York.</small></span>        <p><h4><span class="w3-border-bottom">Blood of the Jews</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Clifford Tower<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Blood unknown, paper in March 1686 or 1687<br>
              <span class="w3-border-bottom">Further Comments:</span> Many Jews committed suicide in the tower rather than surrender to the rioting anti-Semitic mobs tearing up the town. The ground of the tower is said to be stained red, even after being dug up and replaced. Sir John Reresby wrote that one of his men on watch at the tower observed a piece of paper blowing in the wind that changed into a monkey before transforming into a turkey-cock. The entity wandered around for a bit before vanishing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Serving Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Clifton Lodge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This building is said to be haunted by the ghost of a servant who killed her child who had been born outside of wedlock.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york2634.jpg' class="w3-card" title='Cock and Bottle Inn, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cock and Bottle Inn, York.</small></span>        <p><h4><span class="w3-border-bottom">Man with Big Nose</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Cock and Bottle Inn, Skeldergate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be the Royalist and alchemist George Villiers, Second Duke of Buckingham, this ghostly figure, with black wavy hair and a large nose, has been spotted in the public house. The building was built on the top of a site once owned by Villiers, which would explain his reluctance to leave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york1722.jpg' class="w3-card" title='Coppergate, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Coppergate, York.</small></span>        <p><h4><span class="w3-border-bottom">Fire Alarm Starter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Coppergate shops, those built on site of old Craven's Sweet factory (includes the Bodyshop)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity was known to cause trouble when the factory stood. In more recent years, it has been blamed for setting off the fire alarms in these buildings.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Coinage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Dance school attached to St Mary's Church, Bishop Hill Junior<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of coins being poured onto a table and into a metal box have been reported coming from this building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york6446.jpg' class="w3-card" title='Dean Court Hotel, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Dean Court Hotel, York.</small></span>        <p><h4><span class="w3-border-bottom">Roman Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Dean Court Hotel, Duncombe Place<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Reported to walk around the hotel, this phantom may have also been spotted by a trio of guests in their bathroom mirror.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york2687a.jpg' class="w3-card" title='The old Debtors Prison in York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The old Debtors Prison in York.</small></span>        <p><h4><span class="w3-border-bottom">Mary Bateman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Debtors Prison<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Mary Bateman would appear during the witching hour and could have been the entity that became well known for playing pranks on prisoners.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Drake's Fish and Chip Restaurant<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2021<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom woman was reported to haunt the third floor of this building. The local press reported that the ghost was consulted when redevelopment work was carried out on the site in 2021.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Human Shape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Dun Cow public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 August 1977<br>
              <span class="w3-border-bottom">Further Comments:</span> While on honeymoon, a woman awoke to see the shape of a tall person by the window. The shape appeared to be made up of small pulsating balls of light. The figure suddenly disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Phantom Hand</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Earl Grey Tea Rooms, Shambles<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2016<br>
              <span class="w3-border-bottom">Further Comments:</span> One visitor reported on Trip Advisor that they had felt a child's hand grab them while in the lady's toilet, although nothing was there. Another story says a phantom older man sits at a corner table.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york3790a.jpg' class="w3-card" title='The River Ouse running through York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The River Ouse running through York.</small></span>        <p><h4><span class="w3-border-bottom">Pale Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Edge of the River Ouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring) (legend), ghost unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be the shade of a drowned person, this figure drifts around the banks of the river. Another piece of folklore says that a person drops five white pebbles into the river when the Minster clock chimes one in the morning during the first of May, they see their past, present and future.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york3235.jpg' class="w3-card" title='First Hussar pub, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> First Hussar pub, York.</small></span>        <p><h4><span class="w3-border-bottom">Caped Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - First Hussar public house, North Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A small team of men working through the night to complete a painting and decorating job all reported seeing a ghostly man dressed in a cape, who vanished through a wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york6548.jpg' class="w3-card" title='Five Lions pub, York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Five Lions pub, York.</small></span>        <p><h4><span class="w3-border-bottom">Green Jenny</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Five Lions public house (now Watergate Inn)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure has not been seen for many years, and her history is all but forgotten.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>        <p><h4><span class="w3-border-bottom">Walking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York - Fordington Lodge, outside of the city<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Haunted by an unknown entity, the sounds of footsteps were often heard crossing the living room floor. Doors sometimes opened themselves, and once a sofa moved itself away from the wall, before moving back a few seconds later.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 80</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=80"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=80"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northeastandyorks.html">Return to North East and Yorkshire</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North West England</h5>
   <a href="/regions/northwest.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/northwest.jpg"                   alt="View North West records" style="width:100%" title="View North West records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
