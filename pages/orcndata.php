

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in the Scottish Islands, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Orkney</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Orkney Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nuckelavee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All over isles - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A hybrid, sea living creature, the Nuckelavee was described as having the features of both a mighty horse and its rider. The rider was legless and appeared to grow straight out of the horse's back. Its head was ten times the size of a normal human head, possessing a very wide mouth that jutted out like a pig's snout, and a single red eye that blazed with flame. It was both hairless and skinless. Nuckelavee's breath was toxic and it was responsible for the 'Mortasheen', a fatal disease of cattle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Good Looking Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birsay - Rocks along coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A wife and husband witnessed a mermaid sitting on the rocks, described as 'good looking' by the wife. The couple tried to capture the creature, but it swam away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walking Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birsay - Standing Stone in the area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> At the stroke of midnight on Hogmanay, this stone would walk down to Birsay loch and would briefly dip its head into the water before moving back to its original location. Anyone foolish enough to try to observe the stone at this time would be found dead the following morning, laying by the base of the stone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/0503mermaid.gif' class="w3-card" title='An old woodcut of a mermaid swimming past a sailing ship.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a mermaid swimming past a sailing ship.</small></span>                      <p><h4><span class="w3-border-bottom">Mermaids</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Deerness - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> These women of the water were seen dozens of times during the last twenty years of the nineteenth century. One was said to have been shot, but the body was not recovered.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Twitchers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eynhallow - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 July 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> Two birdwatchers that vanished without trace on the island after arriving with 86 other people were thought by locals to have been taken by merfolk. An extensive search by the coastguard could find no trace of the missing people.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/OI-065.gif' class="w3-card" title='An old woodcut of several ears of corn dropping grains.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of several ears of corn dropping grains.</small></span>                      <p><h4><span class="w3-border-bottom">Bleeding Ears</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eynhallow - General area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Another local legend says that the ears of corn which grow here bleed if cut after dusk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Horse of Clumly</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hestwall - Clumly Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Stormy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> Both horse and rider were killed in an accident as they galloped back home, the rider having just disposed of a body. Now the pair only appear when the environment is right.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Scapasaurus</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Holm - Deepdale<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1941<br>
              <span class="w3-border-bottom">Further Comments:</span> The rotting corpse of a basking shark that washed up on the beach was mistaken as a Scapasaurus or sea monster, killed by depth charges.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Angry Little Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hoy - Cliff top at Torness<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by a witness throwing themselves off the cliff edge in a mad dance, the witness said that these small men looked 'primitive', with long dark hair.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Disappearing Collie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hoy - Former lodge of Melsetter House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom dog slowly disappeared as it approached the witness down a corridor in their holiday home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Water Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hoy - Pegal Burn<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This creature would attack anyone using the bridge over the burn at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Water Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hoy - Runsigill Hill, Radwick<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The water horse that lived on the hill would attack people travelling along the road to Longhope.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/ORKNEY-SCAPA_General.jpg' class="w3-card" title='An old postcard showing the Scapa coastline.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing the Scapa coastline.</small></span>                      <p><h4><span class="w3-border-bottom">Soft Landing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hoy - Scapa Flow<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Attack in 1850s, diver sighting unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A young lad sitting on the rocks claimed he was attacked by a sea monster with a horse's mane - it tried (unsuccessfully) to clamp its teeth around his legs. Many years later, a diver reported descending to investigate a sunken German ship, only to realise he was standing on the back of a large sea monster that had made its home in the wreak.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dead Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Hunda - Hope of Hunda<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> February 1942<br>
              <span class="w3-border-bottom">Further Comments:</span> The remains of a creature some 8.5 metres long were originally named as being that of a Scapasaurus, although later revealed to be a carcass of a basking shark.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hybrid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Sanday - Exact area not known<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Folklorist Walter Traill Dennison recorded in 1893 that there were decedents of a union between a male selkie and a woman living on the island - you could tell who the offspring were because they had webbed hands and feet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Island Covered in Buildings</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Sanday - In the sky off coast of the island<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1840, 1857<br>
              <span class="w3-border-bottom">Further Comments:</span> This island, covered in fantastic white buildings, has appeared twice in the sky just off the coast. On one instance it remained viable for most of the day. Some people believe it to be the Fairy Isle, while others say it was a mirage of a part of Norway.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Lady of Scar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Sanday - Scar House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom woman, known as Black Lady of Scar, was reputed to have been pushed overboard by the from a ship on a passage back to Scotland from India. She had married the laird of Scar in India, but while travelling home with her husband, he decided that the marriage was a mistake and murdered her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Not a Seal</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kirkwall - Inganess Bay<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1904 / 1905<br>
              <span class="w3-border-bottom">Further Comments:</span> Two children out picking whelks were shocked when they encountered a mystery sea creature at the water's edge. They described it as being black in colour and far bigger than a seal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Kirkwall_Kirkwall_Harbour.jpg' class="w3-card" title='An old photograph of Kirkwall Harbour.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old photograph of Kirkwall Harbour.</small></span>                      <p><h4><span class="w3-border-bottom">Tailed Sphere</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kirkwall - Northwest of Kirkwall<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 January 1985<br>
              <span class="w3-border-bottom">Further Comments:</span> The Kirkwall coastguard reported a very bright spherical object with a tail, moving north west to the south east. The object then split in two. The Aberdeen coastguard reported seeing the same or similar two objects a little later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kirkwall - St Magnus Cathedral and Bishop's Palace<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking around the cathedral three times at midnight shall summon a ghostly white nun. Somewhere underneath the site a phantom piper walks around ancient tunnels which connect to the Bishop's Palace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hogboy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Maeshowe - Area around the cairn<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Hogboy was the name of a hogboon, a dark and moody family spirit which lived in the cairns and became upset if milk was not offered to them after a birth or marriage in a family. This hogboon was blamed for several deaths of people and animal on a nearby farm, after the farmer ploughed up a burial mound.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/boarwhale.gif' class="w3-card" title='An old woodcut of a large fish-like creature with legs.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a large fish-like creature with legs.</small></span>                      <p><h4><span class="w3-border-bottom">Whale like Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> North of Island - Sea<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1537<br>
              <span class="w3-border-bottom">Further Comments:</span> Fishermen reported a whale with characteristics of a boar in the sea in this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Therianthropy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> North Ronaldsay - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Goodman of Wastness, a farmer by trade, stole a selkie's seal skin and hid it, forcing the woman to become his wife. After many years of marriage and a house full of children the selkie found her seal skin and ran off back to the sea, never to be seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ubby</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Orkney - Skaill House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Ubby is said to be a former owner of the house responsible for constructing the artificial island in the nearby loch. His ghost is one of several friendly entities which haunt the house, which also include a tall man with dark hair and the smell of cigarette smoke.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Great Auk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Papa Westray - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1986<br>
              <span class="w3-border-bottom">Further Comments:</span> Reports of an extinct Great Auk being spotted alive on a beach 142 years after the last confirmed sighting turned out to be nothing more than a hoax.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 42</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=42"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=42"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
