
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Brighton Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southeast.html">South East England</a> > Brighton</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Brighton Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/El-Mexicano.jpg' class="w3-card" title='A restaurant along New Road, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A restaurant along New Road, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Flying Knife</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - 7 New Road (was Leadbelly's, currently El Mexicano)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> A knife was thrown through the air by an unseen hand and a set of keys vanished only to reappear in plain view an hour later, when this restaurant was Leadbelly's. Since that time, banging has been heard coming from the restaurant when empty, and a member of staff reportedly spotted a phantom old man in the basement.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ali-Cats-public-house.jpg' class="w3-card" title='Ali Cats public house, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ali Cats public house, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Groaning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Ali Cats public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> During refurbishment work in this pub, workmen were said to have heard strange human groans and encountered unexplained cold spots.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/All-sainst-church.jpg' class="w3-card" title='All Saints Church, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> All Saints Church, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Pale Woman in Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - All Saints church, Patcham<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This ill looking woman appeared along a pew during a Christmas service, disappearing after a kind gentleman placed his coat around her shoulders, trying to keep her warm.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ape.jpg' class="w3-card" title='Ape record shop, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ape record shop, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Nuns</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Ape (formally Lawleys), North Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former manager is said to have seen three phantom nuns in this property. The entities reportedly glide across the floor before vanishing through the back wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Aquarium-Public-House-Brighton.jpg' class="w3-card" title='Aquarium public house, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Aquarium public house, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Known by Name</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Aquarium Public House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An unknown voice is said to call out to staff and customers using their first names.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Area around Norfolk Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> A German bomb killed many people here during the Second World War. The screams of the injured could be heard for years after the event.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Area-around-the-library.jpg' class="w3-card" title='Area around the library, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Area around the library, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Child in Pyjamas</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Area around the library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to early 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to have been killed during a Second World War bombing raid, this small ghostly child dressed in pyjama is said to carry a wooden toy. It is not clear whether the ghost has been observed since the 1990s, when the last of the bomb damaged buildings were fully demolished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Churchill-Square-Brighton.jpg' class="w3-card" title='Churchill Square Shopping Centre, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Churchill Square Shopping Centre, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Black Belly</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Area surrounding Churchill Square Shopping Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1911 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The area surrounding this shopping centre is reputedly home to Black Belly, an apparition which is normally observed from the corner of the eye. The figure is said to resemble a large bald man, wearing a shirt which does not quite cover his stomach, revealing a bloated and bruised stomach. The figure is also said to hover a few centimetres from the ground. One of the last witnesses to see Black Belly is reported to be a telecoms engineer who reported the sighting to the police.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Brighton-Haunted-Bandstand.jpg' class="w3-card" title='Bandstand along Kings Road, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bandstand along Kings Road, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Gaunt Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Bandstand along Kings Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> One person reportedly spotted this phantom, thought to be the ghost of a homeless girl who died of an overdose, as they stopped behind the bandstand to urinate. The ghost is said to be wearing ragged clothing and be very pale with sunken skin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/bat-and-ball.jpg' class="w3-card" title='Bat and Ball pub, Brighton (photograph supplied).'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bat and Ball pub, Brighton (photograph supplied).</small></span>                      <p><h4><span class="w3-border-bottom">Barefooted Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Bat & Ball public house, Ditchling Road<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness cashing up at the end of the night watched a barefooted woman wearing a nightdress appear and start to dance. The figure passed through the bar and vanished when reaching the corner of the room. This dancing woman may or may not be the same as the phantom grey haired, middle aged woman who is said to vanish into thin air here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bath-Arms-public-house.jpg' class="w3-card" title='Bath Arms pub, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bath Arms pub, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Victorian Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Bath Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Victorian man in 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom is said to be a middle aged Victorian man seen standing by a pillar. Another phantom reported to haunt the site wears a tricorn hat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Brighton-Beach.jpg' class="w3-card" title='Brighton Beach.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Brighton Beach.</small></span>                      <p><h4><span class="w3-border-bottom">Large White Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Beach<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Last dog sighting unknown, tall man seen in Spring 1977<br>
              <span class="w3-border-bottom">Further Comments:</span> Reportedly the size of a small horse, this phantom dog will follow lonely walkers on the beach for short distances before vanishing. A witness reported seeing a tall man in old fashioned clothing and a tall hat walking along the sidewalk during the night in 1977 - the figure vanished into the mist.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey-Haired Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Bears and Friends (no longer present), Meeting House Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom grey-haired man wearing a knee length coat was said to have made a few appearances here during the 1990s.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mother</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Bedford Tavern<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The inn is reputedly haunted by a woman who died in the cellar where she was locked up by her abusive husband, or after childbirth, or both, or neither.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Escaping Extinguishers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Belgrave Hotel (now renamed)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> One story says that certain fire extinguishers in this hotel would be moved around unaided at night, although it is possible that they were moved by a mortal practical joker.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Belle-Vue-Field.jpg' class="w3-card" title='Belle Vue Field, Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Belle Vue Field, Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Betsy Bedlam</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Belle Vue Field, Regency Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This female phantom is said to have acquired her name from her expression, not too dissimilar to inmates of Bethlem Hospital in London.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/The-Black-Lion-public-house.jpg' class="w3-card" title='The Black Lion pub in Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Black Lion pub in Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Deryck Carver</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Black Lion public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Deryck Carver was burnt in a barrel of Tar in 1555 and is believed to have been the first brewer in Brighton and the first Protestant martyr in England to be put to death. Since the 1940s it has been claimed his ghost haunts the cellar, and in 2006 the manager of the building claimed have felt an unnerving presence in that area on several occasions. A builder in the upper part of the building spotted a shadowy figure in 1995.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Christine Holford</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Blue Gardenia Club<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> April 1963<br>
              <span class="w3-border-bottom">Further Comments:</span> Club owner Harvey Holford was found guilty of manslaughter after shooting and killing his wife. Shortly after Holford's conviction, lights in the building would turn themselves on and off and kitchen utensils moved themselves around the room. The poltergeist was said to have left after a medium visited the club, although there are other reports that it may have lingered longer.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Brighton-Dome.jpg' class="w3-card" title='The Brighton Dome Theatre.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Brighton Dome Theatre.</small></span>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Brighton Dome (theatre), New Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by actors as they rehearse, the phantom lady is described as wearing a long white dress wearing bouffant style hair. A soldier dressed in a red tunic, the sound of disembodied footsteps and the rattling of chains also haunt the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Battle-of-Waterloo-public-house.jpg' class="w3-card" title='The Brighton Rocks pub, formally known as the Battle of Waterloo.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Brighton Rocks pub, formally known as the Battle of Waterloo.</small></span>                      <p><h4><span class="w3-border-bottom">Coach Driver</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Brighton Rocks public house (formerly Battle of Waterloo), Rock Place<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996, 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure, complete with cloak, was seen in the 1990s drifting around the front door and the gentlemen's toilets. He was said to be the driver of a local mayor who was murdered by a highwayman shortly after leaving the pub. More recently, cupboards held shut by bricks have flung themselves open, heavy filing cabinets have fallen over, and a dog is either terrified of nothing or plays with something invisible.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Brighton-Town-Hall-alt.jpg' class="w3-card" title='Brighton&#039;s Town Hall.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Brighton&#039;s Town Hall.</small></span>                      <p><h4><span class="w3-border-bottom">Henry Solomon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Brighton Town Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Solomon (the first Chief Constable of Brighton) was killed around the middle of the eighteenth century, after being hit around the head with a red hot poker during the attempted escape of a prisoner in the cells. The former officer's ghost reportedly haunts the basement, while the upper parts of the building are haunted by a monk; a remnant from when a monastery stood on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flashing Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Bugel Inn, St Martins Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1965<br>
              <span class="w3-border-bottom">Further Comments:</span> This public house was once plunged into chaos by a poltergeist - it played with door locks, moved furniture around, and would flick the lights on and off at speed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bugle-Inn.jpg' class="w3-card" title='The Bugle Inn in Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Bugle Inn in Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Charlie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Bugle Inn, St Martins Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1965<br>
              <span class="w3-border-bottom">Further Comments:</span> Locals christened this entity 'Charlie', and blamed him for moving furnishings, flicking lights on and off, locking and unlocking doors, and generating other minor mischief.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Caroline-Of-Brunswick.jpg' class="w3-card" title='The Caroline of Brunswick pub in Brighton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Caroline of Brunswick pub in Brighton.</small></span>                      <p><h4><span class="w3-border-bottom">Name Calling</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Caroline Of Brunswick public house, Ditchling Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> An unknown phantom in this pub reputedly calls out to staff using their first name.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton - Clayton tunnel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Since a tragic train crash in 1862, there have been reports of screams, cries, and the sound of crunching metal emanating from this tunnel.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 146</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=146"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=146"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southeast.html">Return to South East England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
