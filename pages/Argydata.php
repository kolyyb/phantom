

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Argyll and Bute Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Argyll and Bute</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Argyll and Bute Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">City Gent</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A83 - Past the A815 junction, heading towards ferry<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1980s, 04:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people travelling to the ferry in the early hours of the morning drove past a man walking along the isolated road. He wore a dark woollen coat with a bowler hat, and carried an umbrella and a briefcase. The night sky was pitch black, and the man did not have a light source. The witnesses remained curious about the figure for years afterwards, as he was so out of place, and later found out the area was haunted by a figure dressed in similar clothing, woefully inadequate for the environment he walks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Clach na Glaistig</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Achindarroch - Farmhouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A stone known as Clach na Glaistig would be washed with a portion of milk every evening to thank a ghost for keeping the calves and cows separate at night. The entity was said to have a face resembling a lichen-covered stone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mr Stewart</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appin - Invercreran House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Stewart was murdered by the two brothers who once owned the house after they were tricked into selling their property. Sounds of a body being dragged down the staircase have been reported, in addition to the phantom footsteps and the slamming of doors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">James of the Glen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appin - Old Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2016<br>
              <span class="w3-border-bottom">Further Comments:</span> Landlord Jim Milligan blamed the ghost of James of the Glen for the mild haunting at this inn, which included footstep-like sounds, rattling of swords on the walls, and tipping chairs.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardentinny - Ardentinny Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One room is said to be haunted by ghostly children - people have awoken and believed the children have entered their room by mistake, only for the ghosts to vanish.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ardura-Mull.jpg' class="w3-card" title='A ghostly black hound sitting in the mist.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly black hound sitting in the mist.</small></span>                      <p><h4><span class="w3-border-bottom">Death Prediction</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardura, Isle of Mull - Area around Lochbuie House<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1909<br>
              <span class="w3-border-bottom">Further Comments:</span> The black dog around this area would warn of impending death. One version of the story says that dog carried a puppy on the back of its head. In 1909, Dr MacDonald spotted the dog while treating chief Murdoch Gillian MacLaine - his patient died soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drowned Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aros, Isle of Mull - Loch Frisa<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> While the specifics may vary from tale to tale, the loch is named as home to a water horse that drowned a local man known for his dissolute lifestyle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Cow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balephuil, Tiree - Farm in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Dugald Campbell noticed a strange little red cow being attacked by the rest of his herd. The red cow ran off with the rest of the herd giving chase. Campbell followed and watched as the red cow entered a solid piece of rock, closely followed by one of his cows. The rest of the herd, intercepted by Campbell, were prevented from vanishing into the rock.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Home</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballevulin, Tiree - Large sandbank (no longer exists)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A large sandbank was once thought to be home to fairies that borrowed the local blacksmith's cauldron every night, returning it in the morning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ogress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balmaneach, Isle of Mull - MacKinnon's Cave<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The cave is named after a piper who descended into it the cave to see how far underground he could travel - he bumped into a female ogre who killed him after he failed to please her with a tune from his pipes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Humanoid Beast</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balvaig - Area in and around sea, and Grador Rock<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Incredibly ugly to look at, this monster would sit for many hours on the rock. One man went too close, and he was later found with his flesh clawed off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drummer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baravulin - Unidentified small cottage<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1924<br>
              <span class="w3-border-bottom">Further Comments:</span> Phantom drumming would start whenever a bagpiper played a tune. Through a series of raps, the phantom claimed to have been an Irishman who fell at the Battle of Tel El Kebir in Egypt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barcaldine - Ardchattan Priory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Buried alive under the floor of the building, this nun has been seen drifting around the priory.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Donald Campbell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barcaldine - Barcaldine Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The same figure that briefly appeared post-mortem at Inverawe House, Donald's shade is said to be full of loathing and hate. A blue lady is said to appear and listen to any music played on site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Short Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baugh, Tiree - Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> More often heard than seen, when this Glaistig did manifest, she took the form of a very short woman with golden hair that came down to her heels. The entity left after being beaten over the head with a stick.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bellochantuy - Argyll hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2021, late 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A guest awoke to the sounds of quiet laughter and rummaging. As the guest sat up, they spotted a black-clad figure who vanished when the light was switched on. The guest searched the room but no one was there. After they returned to bed, the guest watched several small balls of light flit around the room. Another visitor to the hotel during the late 1990s reported on a local forum that he had witnessed a dark figure walking around his room before vanishing. The nephew of former owners believed the site to be haunted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Eoghann a' Chin Bhig</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben Buie, Isle of Mull - General area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to disaster in the MacLean family<br>
              <span class="w3-border-bottom">Further Comments:</span> Eoghann, while living, tried to kill his father - he was decapitated for his efforts. Now he appears before personal disaster or a death in the family.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grinding</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben Feall, Coll - General area of the hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This hill was home to fairies who could be heard grinding their corn and singing deep within.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bowler Hat Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben Ime - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This mountain is haunted by an old man in a bowler hat. A gold watch chain crosses the figure's waistcoat and he carries a parcel under one arm. It is unclear whether it is the same man which a Glasgow university lecturer encountered while in the area; the figure left no footprints in the snow and said he was going to catch a train before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Colin Campbell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Benderloch - Glendhu Burn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This angry shade, covered in mud and carrying a dirk, is thought to be Colin Roy Campbell, aka the Red Fox, assassinated on 14 May 1752.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Goat Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridge of Orchy - Ben Doran<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An Urisk banished to the mountain by a saint has remained here since and may lurk in a nearby waterfall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Foot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridge of Orchy - Eas Urchaidh (waterfall)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A Glaistig lived behind this waterfall, her foot sticking out just enough to split the falling water into two distinct streams. A member of the Campbell family tried to grab the entity but she escaped, cursing the clan never to have a baby born north of her home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Former Owner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Campbeltown - Glen Scotia Scotch Whisky Distillery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This distillery is reputed to be haunted by a former owner who drowned himself in the nearby loch after being cheated out of a large amount of money.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shorted Armed Mermaid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Campbeltown - Kintyre Peninsula<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 October 1811<br>
              <span class="w3-border-bottom">Further Comments:</span> Watched by a farmer for two hours, this mermaid had a pale upper body and a red/grey tail covered in hair. Its face was human, with deep-set eyes and short neck. The creature combed the hair on its head with what looked like short, stubby arms before diving into the water and washing its torso.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Piping</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Campbeltown - Piper's Cave<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Something strange lured a piper into this cave from which he never returned. Hundreds of years later, the piper could still be heard playing his instrument deep below the earth.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 131</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=131"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=131"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
