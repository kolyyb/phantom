
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="WC1 - London Ghosts, Haunted Sites and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > WC1</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of London's WC1 District</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - 001 Red Lion Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1915<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple who lived at Number 001 for several months in 1915 reported hearing footsteps and the sounds of a scuffle, although investigations never turned up anyone else on the site. A woman in a dress dating from around the mid-nineteenth century was said to have been spotted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Frightened Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - 003 Red Lion Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1915<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman in a dress dating from around the mid-nineteenth century was said to have been spotted on the staircase of this building. The figure was seen heading towards the front door where she vanished. A story said she had been murdered on the site and, as the main suspect had an alibi, no one was charged with the crime.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Emma Louise</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Arthur Tattersall House, 119 Gower Street, University College London Halls of Residence<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> An urban myth, it is said that Emma Louise was murdered long ago in tunnels around this building, and that if you say her name three times, her presence makes itself known.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Reader</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Blooms Town House Hotel, Bloomsbury - Room 1<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This part of the hotel, built on the site of a garden, is thought to be haunted by the spirit of a man who once enjoyed sitting in the sunlight reading books from the nearby library.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/WC12248.jpg' class="w3-card" title='Inside the British Museum.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Inside the British Museum.</small></span>                      <p><h4><span class="w3-border-bottom">The Case of Amen-Ra</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - British Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Two fables can be found here. The first concerns a haunted mummy case - as well as the traditional curse, the casket in question was accompanied by acts of poltergeist like behaviour and the manifestation of a female figure. Thirteen mysterious deaths and suicides have been attributed to the sarcophagus. The second dates to when the museum was Montague (or Montagu) House. Two brothers fought a duel in the land behind the building (known as both Long Fields and Southampton Fields). So intense was their fight that no grass ever grew again in their footprints. According to T F Thiselton Dyer (Strange Pages from Family Papers), the footprints were located 'about three-quarters of a mile north of Montague House and five hundred yards east of Tottenham Court Road'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Egyptian Princess</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - British Museum Station (closed 1933)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1933<br>
              <span class="w3-border-bottom">Further Comments:</span> Connected to the 'curse' of the Amen-Ra's tomb, this Egyptian Princess would return from the grave late at night and would wail and scream in the tunnels. A more recent report states that these sounds can now be heard further down the track, in Holborn station.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Colin Evans</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Conway Hall<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1937/38<br>
              <span class="w3-border-bottom">Further Comments:</span> Over a period of around two years, Evans would use the hall to demonstrate his power of levitation - he would hover between three feet and fifteen feet off the ground for periods up to sixty seconds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dickens</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Doughty Street, road outside Dickens's museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1971<br>
              <span class="w3-border-bottom">Further Comments:</span> Dickens' ghost, dressed in black with a top hat, has been seen pacing along the street where he once lived.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dr John Cumming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Grange Blooms Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A Scottish clergyman, Cumming preached that the End of Days would take place between 1848 and 1867. Cumming's ghost may still loiter waiting for the end to come.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anthony Babington</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Lincoln's Inn Fields<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a popular place for the populace to view the public executions, this dark figure seen lurking around the area at night could be one of those hung, drawn and quartered on the spot.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon13267.jpg' class="w3-card" title='Mabel&#039;s Tavern, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Mabel&#039;s Tavern, London.</small></span>                      <p><h4><span class="w3-border-bottom">Mabel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Mabel's Tavern<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Mabel was a former landlady who is now said to return to the tavern. She has moved boxes in the cellar, and been heard calling out the name of the newer landlady and operating a dumb waiter, even though the equipment had previously been removed from the pub.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - New British Library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A crying man dressed in eighteenth century garb has been seen on the new site, and builders reported the sound of chains during construction.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/OI-099.gif' class="w3-card" title='An old woodcut of a tall, strange-looking, bird.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a tall, strange-looking, bird.</small></span>                      <p><h4><span class="w3-border-bottom">Black Winged Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Old Chambers, Lincoln's Inn (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1901<br>
              <span class="w3-border-bottom">Further Comments:</span> Charles Appleby was found dead in his room, large claw marks on his arms and neck, the door and windows locked from the inside. Witnesses outside the building who could see into his room through a window said they could see the man fighting a shadowy bird like creature, though it was at least the same size as Appleby. Reports of the creature continued for a while afterwards, and one team of investigators who put down chalk dust during their stakeout recording finding large claw marks in the powder the following day.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert Perceval</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Private chambers, Serle Street, Lincoln's Inn<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> While a student here, the young Perceval watched as a doppelganger entered his room, blood covering the head and chest. Perceval fainted, and when he came to, the figure had vanished. Several days (or months, depending on the storyteller) later he was found dead, battered around the areas where his double had been seen to bleed. After his death, Perceval's ghost was said to manifest in his former room, still covered in blood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unearthly Sounds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Private flat, Great Coram Street (now known as Coram Street)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1872<br>
              <span class="w3-border-bottom">Further Comments:</span> Harriet Buswell was found dead on Christmas morning, her throat slit. Her killer was never found. The room in which her body was found became so haunted that it was kept permanently locked, although unearthly sounds would be heard emerging at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hospital Visitor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Private flat, Red Lion Square<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1971<br>
              <span class="w3-border-bottom">Further Comments:</span> A female guest at her friend's small flat was woken by another girl who wanted to know why her bed was occupied. The guest apologised and explained that she was invited, after which the girl left the room and the guest fell back asleep. The following morning, she mentioned the incident to her friend - he paled and explained his flatmate who normally slept there was in hospital after a suicide attempt. They later discovered the girl had died during the night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knocker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> An entity hear would start knocking when a person entered the bathroom. The knocking would stop once the entity was acknowledged.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chess Hater</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Private residence off Grays Inn Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre 1951<br>
              <span class="w3-border-bottom">Further Comments:</span> Chess player William Winter claimed that he shared this property with a poltergeist that would pull his chess books from the wall, knock over chess pieces, and pull down pictures of famous players. Non chess related items would remain untouched.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cromwell, Ireton & Bradshaw</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Red Lion Square, Drake Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather dependent: stormy nights (mostly)<br>
              <span class="w3-border-bottom">Further Comments:</span> These three ghosts have been seen in the square, walking side by side, heading towards the gallows. On one occasion they were seen walking straight through a set of railing, vanishing shortly after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Catholics</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Ship Tavern, Gate Street, Kingsway<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a secret meeting place for Catholics, it is thought the ghosts of those executed nearby still returned for their rendezvous.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Decaying Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Tavistock Square<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1878, summer, around 04:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A man on an early morning walk through the square watched as a tall, slim figure walked by him. The figure wore a black and white hat with a monocle in one eye. The figure also had a corpse-like face and the man recognised the figure as resembling an old friend who was overseas. A nurse walking close by screamed and ran off when she spotted the figure. A week later the man discovered his friend had died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Forty Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Torrington Square<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The area of land that existed here before the square was built upon it was named the Field of Forty Footsteps. Two brothers fought and killed each other over for the love of a girl, each step they took during battle killed the grass, and it never grew back.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - Unidentified residence, Great James Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A flat along this road was said to be haunted by the sounds of footsteps, with the occasional manifestation of a male figure dressed in evening wear with a white handkerchief covering his face.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jeremy Bentham with Dapple</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - University College<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Bentham named his walking cane Dapple, and it never left his side during life. Much the same can be said post-mortem; the man and his stick can be heard or seen walking down the corridors of knowledge after dark.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Marcus Beck</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> WC1 - University College Hospital<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Before being stolen in January 2001, the portrait of Beck was considered cursed. People who fell asleep under the painting quickly became ill, and in some cases died. It was also said that if the shutters were not closed on the painting at night, a patient would unexpectedly die.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 29</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=29"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=29"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Greater London</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View Greater London records" style="width:100%" title="View Greater London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
