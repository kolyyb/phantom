
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Cambridge Ghosts and Strangeness from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->
 <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Cambridge</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Cambridge Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam353.jpg' class="w3-card" title='Abbey Road, Abbey House, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Abbey Road, Abbey House, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">A Nun and Brown Dog-Thing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Abbey Road, Abbey House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth and twentieth centuries<br>
              <span class="w3-border-bottom">Further Comments:</span> It is claimed that Abbey House is, or at least was, the most haunted house in Cambridge. While I'm not a fan of 'most haunted' titles, when you look at the number and type of the ghosts supposed to haunt the site (a poltergeist, ghostly echoes of chains, a butler, a woman in white, a grey lady, a squirrel, a dog, and a hare all amongst the entities which have popped up), one can understand why the title has been applied here. An exorcism was performed in the 1980s, which may have laid the entities.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drug Induced Ghost</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Addenbrookes Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The story is that a ghost appears to those who are being administrated morphine; the psychological effects of the drug would appear to be ignored in this tale...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam240.jpg' class="w3-card" title='Arbury Road, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Arbury Road, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Unlucky Shuck</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Arbury Road<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This road, and area, are said to be frequented by a shuck that brings ill luck to all that see it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/camb4362.jpg' class="w3-card" title='Haunted studios of BBC Radio Cambridgeshire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Haunted studios of BBC Radio Cambridgeshire.</small></span>                      <p><h4><span class="w3-border-bottom">Womaniser</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - BBC Radio Cambridgeshire, studio 1a<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> The studio is reportedly haunted by an old man renowned for his womanising ways - he was never seen, though his presence felt. In the early 2000s the BBC invited local ghost hunters and psychic mediums to investigate. Armed with crystals, dowsing rods, a piece of paper on a string, and an EMF meter, the attempt to reach the other side resulted in both male and female presences being detected.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Second World War Pilot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Cambridge Airport<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A few buildings on this site have reports of phantom pilots in their flying gear or RAF uniforms. Sounds of footsteps in empty areas are not uncommon, and there is also a story of ghostly singing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Police Officer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Chesterton Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 August 2012, 14:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Taking a shortcut through a car park and past an office block, this witness looked up to see a police officer in 1940s uniform with a gasmask case slung over his arm. The officer was floating. The witness froze briefly in terror before taking haste.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam55.jpg' class="w3-card" title='Christ&#039;s College, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Christ&#039;s College, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Mr Round</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Christ's College, Mulberry Tree in Garden<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight on nights of the full moon (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A tall, elderly gentleman wearing a beaver hat, Round is said to have murdered the only doctor who had the skill to save his dying girlfriend - thus his ghost walks the grounds in the area around the mulberry tree in regret.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam370.jpg' class="w3-card" title='Corpus Christi College, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Corpus Christi College, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Dr Butts, Dark Mark on Throat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Corpus Christi College<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Easter 1904, onwards (Butts), lover unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Doctor Butts took his life on Easter Sunday 1632 (found hanging by his garters in his room), after suffering from what now would be identified as depression. The Doctor has appeared several times during the early 20th century; described as being dressed in white, having long hair and a gash around his neck. The story goes that an exorcism was attempted, but it failed. The kitchen of the building is reputedly haunted by the lover of Dr Spencer's daughter - he suffocated in a cupboard while hiding from the Doctor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Open Window</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Eagle public house<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Window still present, poltergeist late 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> One story says a window in the upper part of the pub is cursed; if it is ever closed then ill fortune follows. Like so many other pubs, the site was also once home to a poltergeist, and is also said to be haunted by the ghosts of two Second World War airmen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam604.jpg' class="w3-card" title='Emmanuel College, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Emmanuel College, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Heavy Stamping Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Emmanuel College<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September 2020<br>
              <span class="w3-border-bottom">Further Comments:</span> Rumoured to have been a suicide victim, the stamping footsteps of the deceased plagued several families that lived there over a period of years. The original building no longer stands, and it was said that the replacement had yet to yield any phantom footsteps, until 2020 when a student tweeted that he had heard someone walking around the empty site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam755.jpg' class="w3-card" title='Stone lions outside Fitzwilliam Museum, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Stone lions outside Fitzwilliam Museum, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Animated Stone Lions Drink from the Gutter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Fitzwilliam Museum<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> On the stroke of midnight, the lions that stand outside the Fitzwilliam Museum leave their posts to drink from the gutters a few meters away. Other sources say they disappear inside the building they guard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Witch</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Former home on site of former Woolworth's shop<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom witch was reputedly haunting a building which was demolished and replaced with Woolworth's.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady on Staircase</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Girton College, Taylor Knob Staircase<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1800s<br>
              <span class="w3-border-bottom">Further Comments:</span> The Grey Lady was seen in the late nineteenth century but has not been seen since. She is rumoured to be Miss Taylor, a young girl who died before starting Girton. The college was also briefly home to a Roman Centurion when first built, though little has been heard from this soldier since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monkey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Gog Magog Hills<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom monkey which haunted this region was reported in the dim and distant past.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pink Light</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Guest house in the area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> A guest here reported their room was filled with an intense pink light after they switched off the bedside lamp. When the lamp was turned on again, the pink light vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wallaby</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Howgate Road area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 November 2019<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen from a distance of one metre, the wallaby left the witness and was last seen heading towards the allotments.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Girl in Blue</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Huntington Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 July 2014, around 22:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A fast moving, slightly out of focus, young girl with light brown hair and in a blue raincoat looked at this witness and disappeared into a bush, creating no noise. The witness looked for the girl, but she had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Barrett</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - King's College, The Gibbs Building<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One story says that Barrett was a strange fellow who stored a coffin in his room. After a night filled with screaming, Barrett's friends found him dead the following day, his body laid out in the coffin. On the anniversary of Barrett's death, his screams are said to echo around this building. Peter Haining claimed the cries likely belonged to a Mr Pote, who cursed the building after being forced to move away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Top Hat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - New Court, Trinity<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1922<br>
              <span class="w3-border-bottom">Further Comments:</span> Author T C Lethbridge reported seeing a ghost during his time at university. He mistook the figure as a porter, though upon reflection realised that the man was wearing a top hat (which porters only did on a Sunday, and it was not Sunday!). Lethbridge's friend, present in the same room during the sighting, failed to perceive the figure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam756.jpg' class="w3-card" title='Newmarket Road, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Newmarket Road, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Large Furry Penguin-Like Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Newmarket Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once seen in Merton Hall, this strange creature has been more recently reported waddling along the Newmarket Road. A local paranormal group examining the case came to the plausible conclusion that the ghost may be that of a doctor in a cloak, wearing a beak-like mask which they believed would protect them from the plague.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam2000.jpg' class="w3-card" title='Peterhouse. Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Peterhouse. Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Francis Dawes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Peterhouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> Dawes was a former Peterhouse bursar who hanged himself in September 1789 after an election scandal resulted in an unpopular fellow becoming Master of Peterhouse. In April 1997, a butler and a waiter spotted a white figure as it moved between the spiral staircase and the centre of the Combination Room, the location where Dawes is thought to have committed suicide. In November of the same year, unexplained knocking was heard and once again a strange figure was spotted by butlers. Around the same time, Andrew Murison, the college treasurer, also heard knocking prior to seeing a benign figure in the dining room. At the time an exorcism was considered if the entity appeared again, but Dawes appears to have fallen silent once again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam5136.jpg' class="w3-card" title='Newmarket Road, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Newmarket Road, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Man Moaning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Private building along Newmarket Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> A former student who stayed in this residence reported that one room (her bedroom) was always freezing cold. The witness also reported hearing a moaning sound close to her head as she lay in bed, unsettling her so much she could not stay in the room any longer, and moved out of the building soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Green</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Private house along Trumpington Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1890s<br>
              <span class="w3-border-bottom">Further Comments:</span> Several witnesses were said to have encountered a ghostly woman wearing a green dress with a red feather in her hat. One man commented on a portrait of the woman that hung in a room he had just left, only to be told that no such painting existed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man in Trilby</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Private house, Bristol Road<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> After modernising the building, the owners became aware of the presence of an old man, who materialised before the mistress of the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam614.jpg' class="w3-card" title='Montague Road, Cambridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Montague Road, Cambridge.</small></span>                      <p><h4><span class="w3-border-bottom">Frail Young Woman in a Hammock</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Private residence, Montague Road<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1924<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness saw a young lady in the summer house lying on a hammock. Sometime later he was told the description matched that of his aunt, who had died of tuberculosis a few months before his visit.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 33</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=33"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=33"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
   <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
