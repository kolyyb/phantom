


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Hidden Treasure and Related Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Hidden Treasure</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Treasure Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Cradle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abernethy (Perth and Kinross) - Castle Law (aka Castlelaw)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The hidden golden cradle was said to have once rocked the children of kings. The small loch in the area is said to fill some people with dread, and if you run around it nine times recanting a certain spell, a hand shall rise and drag you in.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Iron Chest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton (Suffolk) - Wimbell Pond<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Protecting their hidden hoard of money, if anyone comes too close to discovering this secret stash a white figure appears and cries out 'That's mine!'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant's Treasure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Addleborough (Yorkshire) - Barrow in the area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The giant's treasure slipped from his grasp as he crossed the area, the hoard sinking into the ground and becoming covered in rocks. The giant had to leave the treasure behind, but it was written that a mortal man could recover the wealth if a fairy in the form of a chicken or an ape appeared; the man would have to reach out and grab the treasure without making a sound, and if he failed to do so, the fairy would never appear to him again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Alcocks-Arbour-hill.jpg' class="w3-card" title='The demonic cockerel of Warwickshire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The demonic cockerel of Warwickshire.</small></span>                      <p><h4><span class="w3-border-bottom">Cockerel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alcocks Arbour (Warwickshire) - Area around the hill<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A demonic cockerel stands guard over a hidden treasure hoard within the hill - the last man to discover it was eaten by the bird.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Devil's Trackway</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alton Priors (Wiltshire) - Knap Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This causeway was said to have been constructed by the devil. There are also reports of buried treasure in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dragon Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anwick (Lincolnshire) - Drake Stone, currently in the churchyard<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> 1651, stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A farmer watched in horror as his horses and plough were sucked underground in the middle of a field - a few seconds later a large dragon emerged and flew off. The stone that remains today is said to cover the dragon's treasure. Some believe the dragon was in fact Satan.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Idol</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ash (Kent) - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Hunted for by many a treasure seeker, still yet to be discovered, a large statue made from solid gold is supposed to be hidden in the lands here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Clashing Blades</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashmore (Dorset) - Cranborne Chase<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October, late 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> A member of the Ghost Club and his partner reported being surrounded by the sounds of men fighting, sword clashing with sword, and the moans of the dying. There are also tales of a golden coffin and stolen treasure buried in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse and Rider</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Athlone (County Westmeath) - Clonfinlough Stone<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This stone is carved with scenes which could depict an ancient battle. One legend says that at certain times of the year a horseman manifests and gallops around it. Another story states that a man named Michael will one day find two pots of gold under the stone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wilt3609c.jpg' class="w3-card" title='Silbury Hill, Avebury.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Silbury Hill, Avebury.</small></span>                      <p><h4><span class="w3-border-bottom">Burial</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Avebury (Wiltshire) - Silbury Hill<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Hill still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The largest man made mound in Europe, no one can say for sure why the hill was built, though one group has alleged that it was the only way the builders knew of immobilising a great evil. The ghost of King Sil is also said to ride around the base of the mound on moonlit nights, which is likely to be connected to the belief that a horse and rider of solid gold are contained within. Finally, another ghost, this time headless, is said to haunt the base.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Free Money</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bamburgh (Northumberland) - Rocky outcrop on which castle is built<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Lucky people are said to be able to find money which has been left by the fairies, but if they failed to add a coin of their own to the hoard they had found, everything would slip away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Money Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bandon (County Cork) - Monerone area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A rock in the centre of the road was known locally as the Money Stone and according to legend was used to mark the location of buried treasure. People had tried to dig up the hoard but were always scared off by a supernatural presence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barby (Northamptonshire) - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1851<br>
              <span class="w3-border-bottom">Further Comments:</span> The spirit that harassed a family in the unnamed building only ceased manifesting and creating loud bangs when they discovered a large sum of hidden money and paid off her debts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Glass Tower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bardsey Island (Gwynedd) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> As well as the final resting place of King Arthur, Merlin is said to have kept the Thirteen Treasures of Britain within his glass tower that stood on Bardsey Island. These treasures were: 
1) Dyrnwyn - the sword's blade would burst into flame when wielded by royalty, 
2) The Hamper of Gwyddno Garanhir - the hamper would multiply its contents by five-score, 
3) The Horn of Bran - this drinking beaker would provide whatever the user wanted to whet their lips, 
4) The Chariot of Morgan the Wealthy - extremely fast magical travel, 
5) The Halter of Clyno Eiddyn - any horse dreamt of would appear haltered in the morning, 
6) The Knife of Llawfronedd the Horseman - would carve the meat at a meal for twenty four men, 
7) The Cauldron of Diwrnach - would only boil water for a brave man, 
8) The Whetstone of Tudwal Tudglyd - when used to sharpen the sword of a hero, any strike made by the weapon would be fatal, 
9) The Coat of Padarn Redcoat - would only fit anyone of noble blood, 
10) The Crock of Rhygenydd - would serve any food the user wanted, 
11) The Dish of Rhygenydd - would serve any food the user desired, 
12) The Chessboard of Gwenddolau - would play itself once set up, &
13) The Mantle of Arthur - would make the wearer invisible.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cromwell's Wealth</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bartlow (Cambridgeshire) - Bartlow Hills, aka Three Hills<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Hills still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A chest full of treasure is said to have been hidden by Oliver Cromwell in this area. It has also been said that people have reported feeling something brush against them while on one of the hills.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bags of Gold</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bearley (was Bearly) (Warwickshire) - Windmill (current status not known)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a former miller would hold down a flagstone, under which several bags of gold were said to be hidden. The hoard was only said to be accessible at the stroke of midnight during a full moon, when the phantom miller would take his ghostly horses to a pond to water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/OI-233.gif' class="w3-card" title='An old woodcut showing a tiny fairy riding a small horse.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut showing a tiny fairy riding a small horse.</small></span>                      <p><h4><span class="w3-border-bottom">Helpful Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beedon (Berkshire) - Burrow Hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The little people here are traditionally helpful in nature, and once fixed a plough for a farmer. When archaeologists tried to dig up the barrow in 1850, a storm blew up and drove them away. A golden coffin is said to be buried in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Caps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beelsby (Lincolnshire) - Unnamed field in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> In an unnamed field in this area treasure is reputedly buried. Two fairy-like entities wearing red caps are said to occasionally appear, searching for the hoard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Plough</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belton (Norfolk) - Mill Hill (Belton Common)<br>
              <span class="w3-border-bottom">Type:</span> legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This barrow was once thought to contain a golden plough.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bentley (Suffolk) - Bridge on road linking Brantham and Bentley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once based at Dodnash Priory that once stood nearby, this monk now haunts the road between two villages. A single stone remains at the site of the priory, and it is said treasure is concealed underneath.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Coffin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berwick St John (Wiltshire) - Winklebury Camp, also known as Winkelbury Camp<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A golden coffin is reputed to be buried in the area. Another legend says that if you walk around the hill seven times while swearing, the Devil will appear to grant you a single wish.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Williams' Treasure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bethesda (Gwynedd) - Site of a house once owned by Dr John Williams<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Archbishop Williams appeared twice after his death to the female owner of a shop built where his house once stood. Williams instructed the woman to dig in a certain place in her garden, but she refused. Many years later, a father and son digging in the garden stumbled upon Williams' hidden hoard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coffin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bibury (Gloucestershire) - Saltway Barn Barrow<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A golden coffin is said to be buried in this long barrow.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hording Lizard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bilsdale (Yorkshire) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A dragon was said to reside in a tumulus, protecting its hoard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bisley (Gloucestershire) - Money Trump<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people passing the trump reported seeing several phantom headless men. Treasure is said to be hidden here.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 233</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=233"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=9&totalRows_paradata=233"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
