

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Wiltshire, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Wiltshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Wiltshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Henry Watts</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A4 - Memorial between Marlborough and Hungerford (now relocated)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October 1956<br>
              <span class="w3-border-bottom">Further Comments:</span> Henry Watts erected a memorial along this road to his son Alfie after the boy was killed in a cart accident in 1879. Four people spotted a tall man in a long coat standing by the memorial one evening as they all travelled home, but he vanished when they stopped the car nearby. Some speculated the ghost was concerned that the cross would be destroyed by the planned widening of the road, but instead the memorial was carefully relocated close by.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Aldbourne-Dors-Grave.jpg' class="w3-card" title='A ghostly figure walks down a hill.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly figure walks down a hill.</small></span>                      <p><h4><span class="w3-border-bottom">Hanging Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldbourne - Dor's Grave, aka Dyer's Grave, aka Doll's Grave<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This area is said to be haunted by a man who hanged himself. His ghost was said to appear either dangling from a tree between the memorial hall and social club or walking down the hill.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pony</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldbourne - Road leading to the village from Braydon<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A white pony silently galloped past two men as they returned to the village. It has been speculated that the pony was King Arthur's mount Sigral (ridden at the Battle of Baydon) or belonging to a local man who committed suicide.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Alderbury-Green-Dragon-pub.jpg' class="w3-card" title='Ghostly monks using a secret tunnel.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ghostly monks using a secret tunnel.</small></span>                      <p><h4><span class="w3-border-bottom">Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alderbury - Green Dragon public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A secret tunnel is rumoured to connect this pub with the nearby church and is haunted by phantom monks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Barn Gates</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All Cannings - Area behind the village hall (was the reading rooms)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A barn that stood in a field behind the reading rooms may have been demolished many years ago, but some people still reported the sound of its gate creaking open after it had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Green</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All Cannings - Campfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September 1972<br>
              <span class="w3-border-bottom">Further Comments:</span> The occupier of the cottage at the time reported sitting in bed and spotting a young woman in a green dress with fair hair who materialised in a corner of the room. The woman in green appeared to descend two steps (though there were no steps in the room) before vanishing from view. The owner of the cottage in 1910 believed his home to be haunted by a woman murdered by her husband after she was caught having an affair.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Green Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All Cannings - Campfield House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> After finding his wife in the arms of another in their bedroom, a husband dispatched the unfaithful woman and tried to burn down the house to conceal his crime. However, the fire was quickly put out, and now the woman dressed in green returns to the room where she was murdered.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Leaper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All Cannings - Footpath between village and Allington, close to canal on the All Cannings side<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This corpse way was home to a violent entity which leapt out at people and pushed them into the cold canal waters.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Joshua Cowdry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All Cannings - House opposite Cliff Farm<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1946 or 1947<br>
              <span class="w3-border-bottom">Further Comments:</span> Observed by a neighbour, Joshua walked up his garden path and appeared to enter his house. The neighbour discovered a few hours later that Joshua had died in hospital around the time of the sighting.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elizabeth Bynge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All Cannings - The Old Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Manifesting as a grey lady with a beautiful face, this wraith has been seen by several people over the years, once observed standing at the foot of a bed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ruth King</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All Cannings - White Rose Cottage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> After Ruth died, many villagers believed that her ghost had returned to protect the money which she had hidden in her home. One subsequent owner reported a woman in black who moved from bedroom to bedroom. Eventually the sightings died away, but no money has ever been found (only an empty purse concealed behind some plaster).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in a Red Cap</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All Cannings - Wickham's Lane area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Described as a smallish man wearing a red floppy hat, this figure walks the streets in this area of the village.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allington - Area between village and All Cannings<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The story of the black dog was used to frighten children in this area. One man may have been chased by the creature (though it was dark and stated it might have been a pony), while another couple who spotted the hound said it stood almost as tall as their pony.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-69.jpg' class="w3-card" title='An old postcard of Allington village.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Allington village.</small></span>                      <p><h4><span class="w3-border-bottom">Parson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allington - Around village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1840s?<br>
              <span class="w3-border-bottom">Further Comments:</span> After the death of the local parson, several villagers pushed his body down a nearby well. The justification for this action was never given, and all concerned died before they gave up their reason. For many nights after the parson's death, horses in the village were released from their stables, and the parson was blamed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allington - Road to Horton, near Tan Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 October 1904<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by two lay preachers driving a horse and trap, this phantom was mistaken for a real woman as she walked towards them in the rain. They could see she had auburn hair and a beautiful face, before she vanished without trace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">House</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allington - Tan Hill<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> House seen 1962 or 1963, coffin seen around 1940, and previously in 1870s<br>
              <span class="w3-border-bottom">Further Comments:</span> A young girl walking on the hill came across a horse which appeared to be empty. She later discovered that the building had been demolished many years before her encounter. Twenty years previous, three shepherds watched a phantom funeral procession, complete with a horse-drawn hearse and figures carrying burning torches. Upon the coffin sat a golden crown. As a side note, the hill once had a carved white horse upon it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sound of an Army</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alton Barnes - Adam's Grave, aka Walker Hill, aka Walker's Hill, aka Walkers' Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer, 1965 or 1966<br>
              <span class="w3-border-bottom">Further Comments:</span> Close to the hill, Miss Muriel Cobern heard dozens of horses galloping, as if an invisible army were passing. The sound suddenly stopped as she fled the area. The site is also said to be the resting place of a giant, who will awaken if the hill is run around seven times.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Devil's Trackway</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alton Priors - Knap Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This causeway was said to have been constructed by the devil. There are also reports of buried treasure in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Saint Mylor's Relics</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amesbury - Amesbury Abbey<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A sadistic uncle cut off Mylor's left foot and right hand to prevent the lad from becoming king. A silver hand and a bronze foot were made to replace the missing body parts, and they started to work as flesh and bone. Unfortunately, Mylor was executed soon after. His relics are currently housed at the abbey. There are also reports of phantom figures and ghostly hounds on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amesbury - George Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A soldier from the First World War is reputed to haunt this site, and the sound of horses trotting can occasionally be heard from the courtyard outside.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Whooshing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amesbury - London Road area where railway line once ran<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Two friends heard steam being released, as if from an old engine which could not be seen. One of the people had previously heard clanking and a whistle in the same location.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Outlined Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amesbury - Lords Walk, along London Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter 1985/1986<br>
              <span class="w3-border-bottom">Further Comments:</span> This petite phantom lady, grey in colour but with a brighter outline, was observed by two friends taking a shortcut through the woods - they ran away from the figure, turning to look back only to see the woman had vanished. The walk may also be home to a ghostly Gypsy caravan that was briefly spotted before vanishing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Farm Labourer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amesbury - New Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom farm labourer is reputed to periodically appear to have a drink. Once he has finished his beverage, he promptly vanishes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amesbury - South Mill track<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter, 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> A man walking his dog spotted some children playing near the river, the dog edging away from the nearest child, who was a girl who wore a long white dress. The man assumed the children were from a party at a nearby house, but soon realised that the path and area were very muddy, but the girl in white's dress was pristine. On the return walk, the children had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rider</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atworth - A365<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter 1944<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver was forced to brake hard when a horse and rider emerged from a hedge on one side of the road and crossed in front of him. The apparition then vanished into a wall on the opposite side of the road.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 386</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=386"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=15&totalRows_paradata=386"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
