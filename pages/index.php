

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts and Mysteries from The Netherlands and Belgium via The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/otherregions.html">Other Regions</a> > The Netherlands &amp; Belgium</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana from The Netherlands & Belgium</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Chained Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aalten (Netherlands, Gelderland) - Presbytery<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The chained black dog which haunted this site was said to be a former pastor who converted to Protestantism.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Lord of Rode</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aarschot (Belgium, Flemish Brabant) - Horst Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Every night at midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom Lord of Rode is said to often return to this site, punished for murdering a priest (who may or may not have had an affair with the Lady of Rode).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Agimont (Netherlands, Maastricht) - Agimont Castle (aka Canne or Neercanne)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather dependent: Stormy Nights<br>
              <span class="w3-border-bottom">Further Comments:</span> After driving her coach through a religious procession, Baroness Von Dopff was caught in a storm so heavy that she had to take shelter in a cave. The cave collapsed, and now she is said to drive the coach when the weather repeats its ferocity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Kludde</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All over Flanders (Belgium, Flanders) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to resemble a large black dog, this creature walks on its hind legs and rattles chains to warn people it is nearby. It is pointless trying to escape if the creature sees you, as nothing can outrun it. The Kludde can also take the form of a cat or bird.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Rixt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ameland (Netherlands, Friesland) - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather dependent: windy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> Rixt was a wreaker who used a lantern tied to a cow to lure ships onto the sandbanks where they would break up. While shifting through the wreckage of one ship, she came across the body of her long lost son. Rixt collapsed and died on the spot. Now on windy nights she can be heard calling out her son's name.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Dove Wander</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amen (Netherlands, Drenthe) - Boundary stone between Amen, Grolloo, Hooghalen and Zwiggelte<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Dove Wander, also known as Dove Waander, is a boundary stone which takes its name from a man (Waander) who chose where the marker should be placed. A shadowy figure seen around this area is said to be Waander.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Laakmannetje</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amersfoort (Netherlands, Amersfoort) - Along the border of Gelderland and Utrecht<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Laakmannetje was a surveyor who moved the boundary stones on the border. As punishment he now measures the border with a fiery metal chain.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Helena</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amsterdam (Netherlands, Noord-Holland) - Zeedijk and Spooksteeg<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Post 1753<br>
              <span class="w3-border-bottom">Further Comments:</span> In a fit of jealously, Helena killed her sister and hid the body in a basement. Helena admitted to the crime many years later on her deathbed, but this has not prevented Helena's ghost walking the area at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Heretics</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amsterdam (Netherlands, Noord-Holland) - Bloedstraat<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend says that the ghosts which haunt this street were executed for heresy at a monastery which once stood on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">The Executed</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amsterdam (Netherlands, Noord-Holland) - Dam Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Spanish Inquisition found the town square to be the perfect place for people accused of witchcraft to be burnt alive. Some of those executed are rumoured to remain in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amsterdam (Netherlands, Noord-Holland) - Spinhuis (currently a hotel)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly priest or monk haunts the former spin house. It is said that the holy man fell in love with a woman; she was punished for his actions by being sent to the Spinhuis, while he killed himself distraught at the consequences his actions had.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Family</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amsterdam (Netherlands, Noord-Holland) - Montelbaans Tower (bell tower)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 June (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in clothing dating to the 1900s, a family (or sometimes just a young boy) manifest briefly here once a year, running up a staircase and vanishing at the top.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Black Matthew</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amsterdam (Netherlands, Noord-Holland) - All over the city<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Black Matthew was a magician who used his powers to ensure he never lost a game of chance. His winning streak came to an abrupt end when he tangled with the devil, and now he wanders the city looking for a new game to play.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Noises</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amsterdam (Netherlands, Noord-Holland) - Hotel Amsterdam De Roode Leeuw<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> A review on Trip Advisor referred to this hotel being known for its ghosts, something the management denied. The guest claimed that they and their partner heard strange sounds outside their room which would cease whenever either of them spoke or opened the door to see where the sound was coming from.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Druon Antigoon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Antwerp (Belgium, Antwerp) - Somewhere near the river Scheldt<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, but fountain in city depicts the giant<br>
              <span class="w3-border-bottom">Further Comments:</span> Druon Antigoon was a giant who charged a toll to cross the river. If anyone refused to pay the giant would cut off their hand and throw it into the water. The giant was defeated by a Roman soldier named Silvius Brabo, who managed to cut off the giant's hand in combat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Lange Wapper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Antwerp (Belgium, Antwerp) - River Scheldt and general area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Sixteenth century, but currently has statue outside Het Steen<br>
              <span class="w3-border-bottom">Further Comments:</span> Born when a farmer touched a cabbage and parsley, Lange Wapper was given magical powers after saving an old woman who was thrown into the Scheldt. Stories based around Lange Wapper's activities range from mischievous (vanishing with a laugh after giving sweets to children) to malicious (indirectly killing a woman and her four lovers).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Winnie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Apeldoorn (Netherlands, Gelderland 
Gelderland) - Veluwe (wooded area around the region)<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Nicknamed Winne, a puma which was thought to be stalking the area was discovered to be a cross-breed between a European wild cat and a house cat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Berend</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arnhem (Netherlands, Gelderland) - Castle Doorwerth<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> After kissing a knight's wife, Berend was challenged to a duel by the offended knight. Berend won, killing his foe after the knight was distracted by a flash of lightning. Perhaps racked by guilt, Berend spent the rest of his life waking from dreams where the knight's wife and a bloodied sword lay in bed alongside him. Berend is still said to wander the castle at dusk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Babberich (Netherlands, Gelderland) - Babberich House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A maid is said to have decapitated five burglars who tried to enter this house through a window - she stood to one side and removed their heads one by one as they climbed in. The sixth burglar managed to escape. The maid's ghost is said to remain, protecting the house as she did in life.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Three White Women</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barchem (Netherlands, Gelderland) - Pit, known locally as Wittewijvenkuil<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The white women (more fairy in nature than ghosts) who haunted this site were said to tolerate children playing nearby but were less favourable to adults (although they did, on one occasion, prevent a traveller from falling into the pit).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Sefa Bubbles</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beselare (Belgium, Flanders) - General area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 1700's, but Witches Festival celebrated every other year on last Sunday of July<br>
              <span class="w3-border-bottom">Further Comments:</span> Sefa Bubbles was a witch who lived in this area, and was considered to be in charge of all the local witches.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Witch Tree</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bladel (Netherlands, Noord-Brabant) - Tree in Ten Vorsel estate<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Tree still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The tree was said to have been planted on the grave of a witch who also led a gang of local outlaws. The tree is said to create a strange atmosphere at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Kaatje</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blokzijl (Netherlands, Overijssel) - Bronze statue near the lock<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Statue still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Kaatje was murdered in the seventeenth century, her killer never found and the motive for the crime never certain, although it was speculated that many of her recipes were stolen - such was the renown of her cooking. A bronze statue was erected of Kaatje and it is said if you stroke her head with your left hand it will bring luck, and with the right comes wealth. If one becomes greedy and tries both hands, a punishment is given out instead.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">Cornelis Theurisz</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bobeldijk (Netherlands, Noord-Holland) - Unidentified house<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 June 1616<br>
              <span class="w3-border-bottom">Further Comments:</span> After setting off to sea, Cornelis Theurisz's wife found Cornelis in their home. Cornelis said that he had reached his ship late and that it had already sailed, so he had returned home. When the couple later lay down together, Theurisz's wife realised that he was ice cold. She called out for God's help - Cornelis vanished and was never seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>
                      <p><h4><span class="w3-border-bottom">John van Mijnsbergen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boxmeer (Netherlands, Noord-Brabant) - Area around Boxmeer Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> John van Mijnsbergen, lord of the castle, was murdered by Joseph de Greef. Although the killer was known, he was never caught, and John still wanders around looking for him.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 118</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=118"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=4&totalRows_paradata=118"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/otherregions.html">Return to Other Regions</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Types </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report2.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
