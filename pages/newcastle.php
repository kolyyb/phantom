
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Newcastle upon Tyne Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northeastandyorks.html">North East and Yorkshire</a> > Newcastle upon Tyne</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Newcastle upon Tyne Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/tyne7309.jpg' class="w3-card" title='All Saints Church, Newcastle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> All Saints Church, Newcastle.</small></span>                      <p><h4><span class="w3-border-bottom">Jack the Beadle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - All Saints Church, Quayside<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Jack was a well-liked member of the community before being caught and tried for removing and selling lead from coffins. A local legend now places Jack's ghost in the churchyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/tyne7311.jpg' class="w3-card" title='Broad Chare, Newcastle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Broad Chare, Newcastle.</small></span>                      <p><h4><span class="w3-border-bottom">Martha Wilson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Area around Broad Chare<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom woman haunting the area has been named as Martha Wilson, a suicide buried at a nearby crossroads. One man who encountered Martha said she wore a veil - she lifted it up as he approached, in time for him to realise she had no face.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman's Voice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Blackie Boy public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman's voice was heard screaming 'Get out! Get Out!' from an empty cubical in the men's toilets, causing a member of staff in the room to flee.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Furniture Mover</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Board Room public house, Stevenson Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> Landlord Richard Bell reported shattering glasses, moving pictures and upset furniture at his pub.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Carriage public house (former station), Archbold Terrace<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> George was said to have worked for the railways and died in an accident on the track. He is said to haunt the pub and is blamed when things fall or break. A woman and man are also said to haunt the site - they were killed while standing on the platform when a bomb exploded nearby during the Second World War.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Joseph the Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Former La Dolce Vita nightclub (now closed)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A lover of dance when he lived, Joseph's shade would appear to have been kick-started when a nightclub opened near his grave. The tall monk dressed in his robes was seen dancing in the club several times by both staff and customers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/tyne12901.jpg' class="w3-card" title='Amen Corner, Newcastle'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Amen Corner, Newcastle</small></span>                      <p><h4><span class="w3-border-bottom">A Hound Follows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - General area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Belief circa nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> It was believed that a phantom dog would follow midwives as they travelled around the city. If the dog laughed as the midwife reached her destination, all would be well, but if the hound howled, then complications would follow. One report described the hound as Mastiff-like.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mummy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Hancock Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> It was reported in 2004 that this Egyptian mummy's ghost is still active, and now walks around the museum at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/tyne11171.jpg' class="w3-card" title='The front doors of the Lit and Phil Library.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The front doors of the Lit and Phil Library.</small></span>                      <p><h4><span class="w3-border-bottom">Door Opener</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Lit and Phil Library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 October 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> Video footage released showed a fire door opening on its own accord, although a gust of wind or change in air pressure are the more likely mundane explanations. The building is also used by ghost hunt organisers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golfer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Maisonette built over an old coal mine, near Battle Hill<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> Reported on BBC Northeast, the occupants of several maisonettes were haunted by strange sounds, unexplained footsteps and moving objects. One family took to leaving at night and sleeping elsewhere. Another man reported his golf clubs and balls were scattered across a room - he fled from his home. A BBC sound engineer speculated that the building's construction could be responsible for creating and amplifying the sounds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/tyne6875.jpg' class="w3-card" title='Looking across to Newcastle Keep.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Looking across to Newcastle Keep.</small></span>                      <p><h4><span class="w3-border-bottom">Black Shadow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Newcastle Keep<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> May 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A four foot tall black mass was photographed here during a ghost hunt, seemingly coming out of a wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/tyne9507.jpg' class="w3-card" title='The Black Gate in Newcastle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Black Gate in Newcastle.</small></span>                      <p><h4><span class="w3-border-bottom">Small Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Newcastle Keep, the Black Gate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> A small child is reputed to haunt this area. Steve Taylor, the local ghost walk organiser, reported several strange occurrences, including stone throwing, growling sounds and strange black shapes. People on the site have experienced being bitten or scratched by unseen perpetrators.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man with Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Old George public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly man and his dog have been spotted in the bar, vanishing if approached. Another ghostly figure has been seen sitting in a chair, while elsewhere footsteps without feet go about their business.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Faded Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Private residence along Bentinck Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2002<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman and her child moved away from this property after seeing the faded figure of a nun drift down a passage before vanishing. The woman had experienced poltergeist-like activity in the property prior to the sighting, including toys being activated in the early hours of the morning without cause and items vanishing for several days at a time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Newcastle-upon-Tyne-Quayside.jpg' class="w3-card" title='Jane wanders the quayside.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Jane wanders the quayside.</small></span>                      <p><h4><span class="w3-border-bottom">Jane Jameson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Quayside, area around John Wesley's memorial<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Jane was executed in 1829 for the murder of her mother, although she did try to frame her boyfriend for the crime. Jane now calls out for her lover to find her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Edwardian Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Robinson's Wine Bar, Cloth Market<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly figure was seen leaning against a wall within the bar. A manager who witnessed the entity described it as a tall man with grey hair, wearing Edwardian clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cavalier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Sallyport, Carpenter's Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This soldier died fighting against the Scots in 1644. His phantom reportedly gazes from the tower window. One witness watched him float from the ground into the air, disappearing into the ceiling - she was later told that a spiral staircase stood where she had seen the apparition.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Girl in Blue Dress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - St Andrew's Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A young curate who fell in love with the daughter of a local landowner was now about to set off to see the lass at home when he spotted her walking around the churchyard. The man ran into the churchyard, but she had vanished. The following day he discovered that she had died at home during the night. Though it is said she still walks the area, none have seen her recently.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Newcastle-St-Marys-Well.jpg' class="w3-card" title='A vision of the  Blessed Virgin Mary.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A vision of the  Blessed Virgin Mary.</small></span>                      <p><h4><span class="w3-border-bottom">Healing Well</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - St Mary's Well, Jesmond<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Well still present, vision in late eleventh century<br>
              <span class="w3-border-bottom">Further Comments:</span> This well was once said to have magical healing properties, while at an unstated location nearby, an apparition of the Blessed Virgin Mary appeared just after England was invaded by the Normans.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/tyne6267.jpg' class="w3-card" title='Newcastle Cathedral.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Newcastle Cathedral.</small></span>                      <p><h4><span class="w3-border-bottom">Knight</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - St Nicholas Church (now Newcastle Cathedral)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1882<br>
              <span class="w3-border-bottom">Further Comments:</span> Before this church became Newcastle Cathedral, there were reports of a tall shadowy knight who would walk around day and night, vanishing around corners and pillars. The clank of ghostly armour was also heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Stevenson Street, North Shields<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A house along this road was haunted by the cries of a child, and the heart wrenching sobs of a woman - an infant's body was later discovered concealed within the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Henry Hardwick</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - The Cooperage public house, alleyways near the building<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Trying to escape pressgangers, Hardwick was murdered, and has been seen near to where he died. A man dressed in Edwardian garb has also been seen in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bob Crowther</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Tyne Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost that has been felt as it pushes past people is thought to be an actor killed when a stage prop travelling at high velocity landed on him. He also reportedly has a favourite seat, in which he is occasionally seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Prior Olaf</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Tynemouth Priory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This Viking raider who eventually found God has been seen on the site where so much raping and looting took place. Other phantom monks have been observed on site, several appearing to pray at a stone in the graveyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Longhaired Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newcastle upon Tyne - Unnamed public house along Westgate Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly figure wearing a red coat walked through a barman closing the bar.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 27</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=27"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=27"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northeastandyorks.html">Return to North East and Yorkshire</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North West England</h5>
   <a href="/regions/northwest.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/northwest.jpg"                   alt="View North West records" style="width:100%" title="View North West records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
