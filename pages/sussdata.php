
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Sussex, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southeast.html">South East England</a> > Sussex</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Sussex Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/sus1110a23.jpg' class="w3-card" title='The A23 road at night, running through Sussex.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The A23 road at night, running through Sussex.</small></span>                      <p><h4><span class="w3-border-bottom">Road Crossers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A23 - Various locations<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> A plethora of ghostly figures have been reported on this road, from London to Brighton. Most appear to interact with drivers - one male figure walks in front of a car, only to disappear when hit, while figures dressed in light coloured clothing have also been observed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A272 - A272/A24 junction (Buck Barn crossroads)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> October 1947<br>
              <span class="w3-border-bottom">Further Comments:</span> An aging man sitting on a stone stood up and walked into the path of an oncoming motorcycle. The cyclist felt the impact but retained his balance and took several seconds to stop and turn back to the scene of the accident. However, the old man had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse and Buggy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A272 - Goldbridge Road towards Newick, after the bridge and before the bend<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1994, 23:20h (approx)<br>
              <span class="w3-border-bottom">Further Comments:</span> Two cars driving in opposite directions were forced to perform emergency stops to avoid a horse and buggy being driven by a cloaked man wearing a top hat. The horse and buggy disappeared. One of the drivers left his car quite shaken, running over to the other to ask what had happened to the old vehicle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Alfriston-downs.jpg' class="w3-card" title='The phantom hound of the Downs.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The phantom hound of the Downs.</small></span>                      <p><h4><span class="w3-border-bottom">Running Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston - Between the Downs and Town Fields<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Full Moon (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A large black dog is said to run from the Downs to Town Fields - here it stares over a flint wall before retreating the way it came.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Folk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston - Burlough (or Burlow) Castle<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Fairies once made their home here and rewarded a local man with food after he helped one of their kind repair a kitchen utensil.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blue Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston - Deans Place Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Believed to have been murdered on the site, a woman dressed in blue has been observed on the landing. A dog is also said to haunt the car park outside the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Modern Looking Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston - Smugglers Inn public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1994<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by the owner and her daughter, this ghostly lady seen at the base of the staircase near the bar was said to be wearing a modern gown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Oxen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston - St Andrew's church<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Fourteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The layout and shape of this church was determined by the strange appearance of four white oxen on the village green. Their tails all touched, forming a 'x' shape that the church was built in the shape of.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston - The White Way, road leading from the town towards Seaford<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This creature is reportedly the dog of a young man murdered while out walking during the 1700s - because the pitiful hound sat by the victim's shallow grave, the criminals killed it also. The haunting is said to have ceased after the bodies of dog and owner were discovered when the road was widened.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Woman with Grey Hair</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfriston - 'Tuckvar' house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This tall phantom lady is said to haunt the stairway and was also seen in the larder. An elderly man with grey hair was seen in and just outside of the garage; he is believed to be the man who once looked after the horses' onsite. Finally, the attic is said to be home to a wispy female figure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Emily</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amberley - Amberley Castle Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This teenager is seen in and around the kitchen area of the hotel - the stories say she died shortly after having an affair with a bishop.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Girl and Old Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amberley - Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Accompanied by a sense of evil, these two ghosts have been seen several times over the past hundred years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Huge Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Angmering - Area of Ecclesden Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1964 (monk), pre-1991/92 (others)<br>
              <span class="w3-border-bottom">Further Comments:</span> Locals talk of an enormous monk, standing around fifteen foot tall, which walks the vicinity of the manor house. A disembodied voice that would whisper the name of witnesses was reported in the house itself, where a housekeeper had also reported spotting the ghost of a former owner. Another worker heard several people talking loudly, but upon entering the room from which the voices had emerged, silence fell - the room was empty.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardingly - The Oak public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing to a couple of customers, this phantom girl with dark hair has also been blamed for minor mishaps around the building, such as overnight spillages and items rotating on hooks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Party</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arlington - Woodhorne Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A loud party has been heard coming from the house, while it was empty. People living in the property reported hearing arguments coming from a stream that runs by the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Pale Coat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arundel - A27 Arundel Road, heading into Arundel<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 2011, 22:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman spotted standing by a road sign along this road wore a pale beige raincoat, smiled manically, and appeared to be illuminated by a bright light from below. One witness felt uneasy by the woman's strange posture, and the police were called as it was thought at the time the woman may have escaped from a nearby hospital. The police did not find anything. In the July of the same year, a husband and wife returning from a wedding spotted a woman in pale clothing as she stepped out in front of their car. They braked and watched as she rose into the air and disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-2.jpg' class="w3-card" title='An old postcard of Arundel Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Arundel Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Cannon Fire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arundel - Arundel Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Several phantom sounds have been reported at the castle; kitchen equipment has been heard in use, though always empty on investigation, and the distant rumble of cannons being shot at the walls is also reported. The library is haunted by a blue man, flicking through an unknown book, and a young girl flees towards a tower, where she once jumped from and took her life.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-114.jpg' class="w3-card" title='An old postcard of Arundel Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Arundel Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Flitting White Bird</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arundel - Castle and surrounding area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to death in family<br>
              <span class="w3-border-bottom">Further Comments:</span> It is locally believed that prior to a death in the family, an unidentified white bird hovers and bumps into one of the windows of the castle tower.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rattling in Kitchen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arundel - Norfolk Arms Hotel<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> This playful spirit would turn on and off electrical items, and push kitchen items off work surfaces. Some people have also reported a semi-naked man running around the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Priest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arundel - St Nicholas Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing on a photograph taken in the church, this phantom priest could be seen standing by the altar. Two women also haunt the church; a nun has been seen (and heard walking) in the bell tower, while a woman in blue has been observed praying at the altar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balcombe - Railway tunnel, line to Three Bridges<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Last seen mid 1990?<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed by a train whilst sheltering from a storm, these three (or four, depending on the source) First World War soldiers have made several appearances; they always slowly fade away when approached.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/battle-abbey.jpg' class="w3-card" title='Battle Abbey in Sussex.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Battle Abbey in Sussex.</small></span>                      <p><h4><span class="w3-border-bottom">Black Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Battle - Battle Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century and possibly 10 November 2014<br>
              <span class="w3-border-bottom">Further Comments:</span> The monk appears in several places - the guest house, monks' walk, by the walls outside the abbey... A monk has also been seen at the vicarage of St Mary's Church. Back at the abbey, a woman wearing a large red dress has also been seen in the Common House, while King Harold is occasionally reported, complete with the arrow jutting from his head.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Charlie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Battle - Corner of Netherfield Road and London Road<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness saw his gardener standing at this junction, though several hours had passed since the worker had committed suicide.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Battle - Gatehouse Restaurant<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This small ghostly house cat floats along a corridor in the restaurant before vanishing through a wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Battle - Mount Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen in several of the buildings along this road, including the Bayeux Restaurant, a little girl with golden hair has appeared dressed in early twentieth century clothing.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 445</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=445"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=17&totalRows_paradata=445"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southeast.html">Return to South East England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
