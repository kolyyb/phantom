
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Leicester Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastmidlands.html">East Midlands</a> > Leicester</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Leicester Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Engineer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Abbey Pumping Station Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An engineer fell to his death in the beam engine house. His spirit still lingers, playing with lights and making noises in the basement.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Leicester-Admiral-Beatty.jpg' class="w3-card" title='A strange man in a long coat.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A strange man in a long coat.</small></span>                      <p><h4><span class="w3-border-bottom">Man in Overcoat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Admiral Beatty public house, Wellington Street (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly figure of a man wearing a long coat would be seen looking out a window. Even when not present, the area where he stood would be colder than the rest of the pub (although it could just have been draughty).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Human Sacrifice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Area of land north-east of the Hoston Stone, Humberstone (was known as Hell-hole Furlong)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> How the area of land gained its name was unknown even to nineteenth century folklorists, so it was conjectured that it was an area of human sacrifice.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman Dressed in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Aylestone Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom woman dressed in black with a white face was spotted by a park ranger while on night patrol.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bel's Grave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Belgrave<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The area is said to be named after a giant who died in the area - he tried to travel between Mountsorrel to Leicester in three leaps but failed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">CCTV Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Belgrave Hall Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 December 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> Recorded by CCTV, two ghostly figures were briefly seen in the grounds. Though the image was later explained as being 'leaves caught in the light', the building is reportedly haunted by a middle aged woman in a pale dress, who gives off a friendly aura.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Between Leicester and 'Bambury, upon Dunmothe' (ed note - latter place cannot be located on modern maps)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Possibly fifteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> An old account states that several men encountered a headless phantom crying out 'Bowes! Bowes!'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">May</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Braunstone Hall and Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Several phantoms are reported to haunt this area, including a young woman in white who walks the corridors. She is named as May, who died aged eighteen of tuberculosis. Other phantoms are said to be carriages pulled by black horses which vanish into the trees, a groom who hanged himself in a room above the stables, and a sad looking boy who glazes from an upstairs window.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Richard III</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Car park (and surrounding area)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2005-2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Philippa Langley felt a strange sensation crossing this car park, on a location marked by an 'R', and virtually on top of the location where Richard III's body was later found (the 'R' has been in the car park since the eighties and was painted by an attendant). The offices of Denham Foxon & Watchorn which overlook the car park were reported to be home to mysterious noises, also (now) blamed on the king.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cat Anna</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Castle ruins<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The bogyman-like witch known as Cat Anna was said to live in the cellars of the ruined castle. It is likely that this legend and the story of Black Annis are connected.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hooded Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Cathedral Church of St Martin (aka Leicester Cathedral)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Figure unknown, bells circa 1800<br>
              <span class="w3-border-bottom">Further Comments:</span> This hooded phantom was said to glide around the churchyard and periodically kneel with its head on the ground, as if listening to the earth. Around the start of the nineteenth century, two houses on the site became home to bells which would ring without visible cause.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pools of Water</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Council house on Netherhall Estate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1991<br>
              <span class="w3-border-bottom">Further Comments:</span> A family living in an unnamed council house on the estate reported mysterious pools of water which would form before quickly disappearing again. The liquid was said to be 'silky' when touched. A woman with dark hair was reportedly seen when an exorcist came to visit. It is unknown whether this report is connected to the Bell Lane haunting.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Leicester-Dane-Hills.jpg' class="w3-card" title='Black Annis lurks within a cave.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Black Annis lurks within a cave.</small></span>                      <p><h4><span class="w3-border-bottom">Black Annis</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Dane Hills<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local monster, this old hag possessed iron claws that were used to skin children. She lived in a cave hidden somewhere amongst the hills, now thought to be buried under the city. Some believe the myth to be a continuation of Danu, an entity so old that stories no longer exist about her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ivy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Donisthorpe Mills, Bath Lane (as of June 2009, in process of being demolished)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> The yard was said to be haunted by 'Ivy', a short woman who wore dark clothing, had grey hair, and whose face was covered in warts. A nightshift worker who ventured out of a quick cigarette encountered her, ensuring that he never went outside alone again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Changing Shape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Freewheeler Club, Churchgate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1972<br>
              <span class="w3-border-bottom">Further Comments:</span> An exorcist was summoned to this building after staff reported seeing a strange ghost which would change shape.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadowy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Friar Lane (though topographical area has changed since report)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Normally manifesting as a set of footsteps which would follow people as they walked down this road towards St Mary's church, the shadow of this ghost once manifested and revealed itself to be headless and humped to one witness.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Graveyard in Belgrave area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This pale woman has been seen moving around the headstones.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Disconcerting Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Guildhall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> There are mixed opinions to whether the library here is haunted by a grey monk or a white lady. A tormented Cavalier has been seen in the Great Hall, a phantom policeman haunts other parts of the building, a ghostly dog has been reported in the courtyard, a black cat in the Great Hall, and at least one witness claims to have been shocked to see a pair of legs manifest from the portrait of Henry Earl of Huntington hanging in the Major's Parlour. Ghost Scene Investigations and Leicester City Museums released video footage in 2006 of what appears to be a ghostly figure crawling across the floor in one room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Edwardian Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Haymarket Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> This small boy has been seen several times dressed in a sailor's suit, normally during a rehearsal. Before the theatre was built, a child once drowned in a well on the land and the shade has been named as his.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Smalley</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - House in East Gates<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1924<br>
              <span class="w3-border-bottom">Further Comments:</span> Mrs Smalley, who died in 1727, refused to leave her home post mortem until banished by the Vicar of St Martin's Church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shag Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Kilby Road (was once Black Lane)<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Black lane was patrolled by a large black dog with bright eyes and a large, fanged mouth which glowed like burning coals. It once saved a local girl from being attacked by a robber.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Manageress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Little Theatre, Dover Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The theatre is reputedly haunted by a former manageress dressed in Second World War garb.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Factory Worker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - New Pingle Street, Britella factory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> The workers at this factory threatened to go on strike unless the management attempted to exorcise the ghost which took up residence here. The author does not know the outcome of their demands.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Empty</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Newarke Houses (now a museum)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1908<br>
              <span class="w3-border-bottom">Further Comments:</span> This property had a spooky reputation and stories said it remained empty for many years due to a haunting.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Typing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leicester - Office building, New Walk<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2004/5<br>
              <span class="w3-border-bottom">Further Comments:</span> Many poltergeist-related events took place in this building, including disembodied footsteps, light fittings falling and shattering, alarm sensors triggering, sounds of and a blood-curdling scream. Nothing was ever seen, and the events occurred for around a year before ceasing.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 40</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=40"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=40"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastmidlands.html">Return to East Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="ViewWest Midlands records" style="width:100%" title="View West Midlands records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
