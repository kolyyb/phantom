



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Watch the skies - there may be dragons. Stories from the Paranormal Database.">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Dragons, Wyverns & Wyrms</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Dragons in the Landscape (and in the flesh)</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dragon Slayer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aller, Curry Rivel (Somerset) - Exact area unknown<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The village is named after a local hero who slew a dragon but died as the beast released a final breath of flame. In another version of the tale, the hero survives and finds a brood of hatchlings in the dragon's cave, which is subsequently blocked up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dragon Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anwick (Lincolnshire) - Drake Stone, currently in the churchyard<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> 1651, stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A farmer watched in horror as his horses and plough were sucked underground in the middle of a field - a few seconds later a large dragon emerged and flew off. The stone that remains today is said to cover the dragon's treasure. Some believe the dragon was in fact Satan.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Berkhamsted-General-area.jpg' class="w3-card" title='Snakes, storms and serpents.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Snakes, storms and serpents.</small></span>                      <p><h4><span class="w3-border-bottom">Banished Wyrms</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berkhamsted (Hertfordshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Whilst visiting Britain, St Paul, was supposed to have banished forever all snakes, dragons, and thunderstorms. One out of three is not too bad...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-BetwsyCoed.jpg' class="w3-card" title='An old postcard showing Betws y Coed in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Betws y Coed in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Wybrant Gwiber</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Betws y Coed (Clwyd) - General area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A bandit from a nearby village tried to kill the Wybrant Gwiber but failed. The bandit's throat was torn out and his body thrown into the river. What happened to the dragon is not known.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Imprints</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bignor (Sussex) - Bignor hill<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A large snake-like creature wrapped itself around Bignor hill and squeezed so tightly that it left imprints of its coils.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hording Lizard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bilsdale (Yorkshire) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A dragon was said to reside in a tumulus, protecting its hoard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/0601-dragon.jpg' class="w3-card" title='A dragon carved in wood.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A dragon carved in wood.</small></span>                      <p><h4><span class="w3-border-bottom">Limbless Worm</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop Auckland (Durham) - Wooded area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A creature, said by legend to be a long, hostile worm, but likely to have been a boar, inhabited an oak wood and would attack man and beast. It was slain by a member of the Pollard family.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Duel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bisterne (Hampshire) - Dragon Fields<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The area was held to ransom by a fire breather who demanded a pail of milk once a day. The knight who engaged the dragon in battle barely made it out of the conflict with his life, and though the dragon lay slain, the victor died shortly after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Piers the Dragon Killer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brent Pelham (Hertfordshire) - Exact location unknown, but buried in St Mary church<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Piers Shonks is a celebrated local dragon slayer, whose tomb in the village church is decorated with his battle. One story says he died after Satan came to seek revenge for the death of his pet, another that Satan tried to claim his soul many years later when Piers lay on his deathbed; the Devil said that he would take Piers' soul whether buried inside a church or not, so the man was buried in the north wall of the local church, denying the Devil of his prize.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/2205serpent001.jpg' class="w3-card" title='An Image from An Essay Towards a Natural History of Serpents.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An Image from An Essay Towards a Natural History of Serpents.</small></span>                      <p><h4><span class="w3-border-bottom">George and the Dragon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brinsop (Hereford & Worcester) - Lower Stanks and Duck's Pool Meadow near the church<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The village of Brinsop claims to be the location where St George had his famous battle with a dragon. The creature lived in Duck's Pool Meadow but was finally slain at Lower Stanks meadow. An old stone carving in the church depicts the final battle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dragon Slaying</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bures (Suffolk) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Fifteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The local church gives the account of how a local dragon that was immune to arrows was finally chased into the marshes where it vanished. A virtually identical tale exists from nearby Wormingford.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sheep Eater</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burley (Hampshire) - Hill known as Burley Beacon<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This dragon terrorised the neighbourhood after demanding a sacrifice of sheep - the locals gave him milk instead. Before long, the reliable knight of yore came along, covered his armour with birdlime (a sticky substance made from bark) and powered glass, and the engaged the creature. The knight won the fight, but his two hunting dogs were killed, and he later died of his sustained injuries.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Slain Dragon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buslingthorpe (Lincolnshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A member of the Buslingthorpe family was said to have killed a dragon in this area and was awarded a large amount of land for the act of bravery.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Carantoc's Dragon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carhampton (Somerset) - General area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Saint Carantoc, while searching for his altar, met with King Arthur who voiced concerns about a dragon terrorizing the county. Carantoc agreed to help the King (in return for the location of the altar), and tamed the dragon using his stole. Locals wanted to kill the creature, but the saint set it free after making the dragon promise never to hurt anyone again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">One Eyed Dragon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castle Carlton (Lincolnshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir Hugh Bardolfe takes credit for slaying this dragon, noted for having a single eye located in its forehead. Man and beast fought during a storm, and when the dragon was blinded by a flash of lightning, Sir Hugh struck a wart on one of its legs, killing it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Castle-Gwys.jpg' class="w3-card" title='The cockatrice of the castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The cockatrice of the castle.</small></span>                      <p><h4><span class="w3-border-bottom">Multi-eyed Cockatrice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castle Gwys (Dyfed) - Exact area not known<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The body of this cockatrice was covered with eyes, and it was said that the estates of Winston belonged to anyone who could see the creature before it could see them. One man managed to win the lands by hiding in a barrel which rolled into the monster's lair.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Winged Serpent</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cawthorne (Yorkshire) - Cawthorn Park region<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A winged serpent dwelt in Serpent's Well and would between the area and Cawthorn Park.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Challacombe-Bronze-Age.jpg' class="w3-card" title='Fire breathing dragon.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Fire breathing dragon.</small></span>                      <p><h4><span class="w3-border-bottom">Fire Breathers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Challacombe (Devon) - Bronze Age burial mounds in the area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Breathing fire as they travelled, the dragons in this region are said to have an interest in Bronze Age burial mounds scattered around the region.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor1823.jpg' class="w3-card" title='Off the coast of Christchurch - one of the sites of a dragon attack.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Off the coast of Christchurch - one of the sites of a dragon attack.</small></span>                      <p><h4><span class="w3-border-bottom">Sea Dragon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Christchurch (Dorset) - Skies above and off coast<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1113<br>
              <span class="w3-border-bottom">Further Comments:</span> The third abbot of Saint Martin of Tournai documented the arrival of a five headed dragon in Christchurch. The dragon was said to have emerged from the sea and taken flight towards the town, destroying the church and many of the surrounding houses (but not those occupied by the abbot and his entourage). The dean of the destroyed church tried to escape by boat, but the vessel was also incinerated (although the dean escaped harm).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wormstall</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Churchstanton (Somerset) - Area where Stapley Farm now stands<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The dragon which took up residence in this area was killed by a knight; the lashing of the dying dragon's tail is said to have carved out a hollow in a field known as Wormstall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Multi-headed Dragon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crowcombe (Somerset) - Shervage Wood<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two men from the village engaged a double-headed winged lizard that terrorised the area, winning the battle by forcing the creature to eat burning pitch. Carvings on the benches in the Church of the Holy Ghost show some of the battle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Worm</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dalry (Ayrshire) - Mote Hill<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local blacksmith eventually killed this beast by constructing a suit of armour covered with retractable spikes. The creature swallowed the smith whole who then wriggled so violently that the monster's intestines were ripped to shreds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smith's Wyrm</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Deerhurst (Gloucestershire) - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> John Smith, a local hero, was rewarded with a large amount of land after killing a dragon that was trying to make a home in the neighbourhood. Smith left a large quantity of milk for the creature to consume. The wyrm drunk so much milk that kinks appeared in its scales, enabling Smith to decapitate it with an axe.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vortigern's Tower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dinas Emrys (Gwynedd) - Thought to be location where the spring can be found<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Every time construction started on Vortigern's Tower, it collapsed. Vortigern asked the druids why this was, and they recommended sacrificing a boy to settle the earth. A young lad was brought, but he told Vortigern that the tower fell because two mighty dragons fought beneath the ground in a stone chest. This boy grew up to become Merlin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Beast</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dolbury (aka Dolebury) (Devon) - Exe valley between Dolbury Hill & Cadbury Hill<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Every night (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> To guard the treasure buried under both hills, this beast flies between the two locations each night.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 96</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=96"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=96"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
