
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in August">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > August</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>August - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Animated Queen Anne</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SW1 (Greater London) - Queen Anne's Gate, Westminster<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year the statue of the Queen becomes animated and walks around the neighbourhood. Until recently, the statue was believed to be Bloody Mary, until a clean-up operation revealed an inscription pointing out the error.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walking Stone Knights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ingham (Norfolk) - Trinitarian Priory<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 August or 02 August (reoccurring) - stone knights, dancing monks unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Chas Sampson wrote that the two stone knights in the church are said to wake once a year and go for a walk to the water's edge. Here they fight an Asian soldier in uniform, before walking back. Other stories claim that phantom dancing monks can occasionally be seen in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">King William II</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Minstead (Hampshire) - New Forest, the Rufus Stone<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered (allegedly) on this spot by a close friend, William II is now doomed to rise on the anniversary of his death and walk to Winchester.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tom Crocker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burgh Island (Devon) - Pilchard Inn, and surrounding area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A smuggler by trade, Crocker met his match outside the pub, where he was shot dead (by customs or by rivals, we do not know). He returns on that fateful day. Other sources state that he was hanged.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Area-between-Rock-and-South.jpg' class="w3-card" title='A phantom woman.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom woman.</small></span>                      <p><h4><span class="w3-border-bottom">Misty Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Rock (Northumberland) - Area between Rock and South Charlton<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 15 August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This mournful spirit makes the journey between Rock and South Charlton once a year, marking the death of her husband.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Clophill-Chicksands-Priory.jpg' class="w3-card" title='The ghost of Bertha Rosaca.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ghost of Bertha Rosaca.</small></span>                      <p><h4><span class="w3-border-bottom">Pregnant Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clophill (Bedfordshire) - Chicksands Priory (currently owned by the Ministry of Defence)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 17 August (reoccurring), last seen 1960<br>
              <span class="w3-border-bottom">Further Comments:</span> After a secret liaison with a male superior, Bertha Rosaca became pregnant - her spirit now appears once a year in protest of her subsequent treatment. The spirit, when last seen, wore black and disappeared through a wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man Bern being Chased</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Reedham (Norfolk) - High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Chased through the village by an angry ghostly mob, the shade of Bern is said to reach the river before jumping into a boat and making his escape.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure of a Hooded Person</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Maldon (Essex) - Beeleigh Abbey, near Maldon<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 22 August (reoccurring) (some sources say 11 August)<br>
              <span class="w3-border-bottom">Further Comments:</span> Just one of many spooky happenings at Beeleigh Abbey, this spook has been observed standing in a corner of the James Room. The entity could be a monk or the ghost of John Gate, executed in 1553 for supporting Lady Jane Grey. Either way, the ghost can be heard screaming and crying throughout the building. A poltergeist has also been reported opening a trapdoor and vibrating beds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Waiting Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belaugh (Norfolk) - Riverside<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman in a white dress is said to wait for her Viking lover by the riverside.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk, Betrayer of the Abbey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ludham (Norfolk) - St. Benet's Abbey, ruins close to River Bure<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 May (reoccurring) (monk), 10 August 2019 (dog)<br>
              <span class="w3-border-bottom">Further Comments:</span> Now little more than rubble, the site of the old abbey is home to the ghost of the monk who betrayed his brothers to Norman soldiers - the troops later hanged him anyway. Two people out investigating the ghostly monk spotted a large black dog-like creature, the size of a small horse, running towards the church. The two witnesses quickly left the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Spontaneous Combusting Rat Catchers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beccles (Suffolk) - General area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> To remove the rats and the Black Death from their village, three men sold their souls to a group of witches. The men played musical instruments as they walked down the road, the rats followed them, and all disappeared into hell. They are allowed a brief respite once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Waving White Lady in a Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hockley (Essex) - Road outside the church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August (possibly) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> After losing control of the horses, a coach driver and his female passenger died. Now, the phantom vehicle is seen outside the church; the white woman in the carriage waves at witnesses before the coach pulls away and the accident is repeated.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hanging Highlanders</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newton le Willows (Merseyside) - Hermitage Green Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Caught by Cromwellian troops, these Highlanders were immediately hanged on the surrounding trees. The sound of running can be heard around the anniversary of the executions. A couple of people here in January 1990 heard loud drumming which moved down the lane, though nothing could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Joseph Bexfield</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thurlton (Norfolk) - Marshes<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Bexfield drowned in August 1809, after following a Jack O'Lantern (now known to be burning marsh gas). His ghost is said to be still out on the marshes, looking for the way home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Black Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Halesowen (West Midlands) - High Street area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend says that the area where the Saxon church once stood is home to a large black dog which glides around. Its howls are prevalent throughout August.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Colwinston (South Glamorgan) - Pwyllywrach Manor<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> August, first Monday of the month (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Cries and screams can be heard from a man torn into small pieces by hell hounds. The echoes can be heard once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Auchtertool-Pathway.jpg' class="w3-card" title='A folklore favourite - the phantom funeral procession.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A folklore favourite - the phantom funeral procession.</small></span>                      <p><h4><span class="w3-border-bottom">Funeral and Piper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Auchtertool (Fife) - Pathway once known as Lady's Walk, heading towards the kirk<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Exact date not known, but one night in August (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Carrying a shrouded coffin at shoulder height, a phantom funeral procession led by a tartan clad piper crosses a field, heading towards the church from the direction of Hallyards Castle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Man with Fire for Eyes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W6 (Greater London) - St Paul's Churchyard, Queen Caroline Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: 03 August or July, prior to midnight, every fifty years (next in 2055)<br>
              <span class="w3-border-bottom">Further Comments:</span> First seen in 1805, the original witness is said to have died of shock shortly after seeing the towering figure whose eyes gleamed with fire. Since then, no further reports have emerged, other than mistaken identities.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Healing Waters</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Loch Monar (Highland) - Waters of the loch<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: first Mondays of February, May, August and November<br>
              <span class="w3-border-bottom">Further Comments:</span> If entered on certain days of the year, the waters of this Loch would heal the sick on the proviso that the Loch was entered three times at midnight and the water then sipped, before a coin was thrown in. The sick would then have to leave the site before the sun came up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sudeley (Gloucestershire) - Sudeley Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: reputedly during August & September, but last seen May 1983<br>
              <span class="w3-border-bottom">Further Comments:</span> It is always around these two months that a ghostly housemaid is seen cleaning and dusting, dressed in Victorian working garb. A visiting American Professor reported encountering her during a visit to the castle. Other reported ghosts here include a woman in a green dress and a man with a hawk perched on his arm.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 20 of 20</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
