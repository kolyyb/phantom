

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Lincolnshire, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastmidlands.html">East Midlands</a> > Lincolnshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Lincolnshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Biker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A15 - A15 northbound near Ruskington turning<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 November 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver spotted a tall male figure, wearing a leather jacket, with medium length black hair standing on the roadside. The witness said that the figure literally appeared out of nowhere and vanished soon after being seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Long Necked Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Algarkirk - Area around the village church<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This black dog was described by a witness as being tall and lean, with a long neck and a protruding muzzle. The hound is not regarded here as an ill omen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dog in Trees</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Algarkirk - Church<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This large black hound was seen between three trees that grow close to the religious house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dead Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alkborough - Humber<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Possibly early twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Small blue lights would occasionally be seen darting around the waters here, with many believing that the glowing balls forecast the eminent drowning of local fishermen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Puppy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alkborough - West Halten Lane, near the cricket ground<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> While cycling home a witness braked hard to let a small dog cross in front of him - the puppy moved half a metre out of the way before vanishing on the grass verge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dragon Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anwick - Drake Stone, currently in the churchyard<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> 1651, stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A farmer watched in horror as his horses and plough were sucked underground in the middle of a field - a few seconds later a large dragon emerged and flew off. The stone that remains today is said to cover the dragon's treasure. Some believe the dragon was in fact Satan.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Signalman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barkston - Barkston Junction Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter, reoccurring<br>
              <span class="w3-border-bottom">Further Comments:</span> Maybe protesting about the automated signals, this ghost has been seen crossing the track in front of incoming trains and climbing to check the obsolete signals.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shag Foal</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnoldby le Beck - Church of St Helen's<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A black dog the size of a donkey, this entity was said to let loose the most unearthly cries.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tatter Foal</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barton-upon-Humber - General area<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil was said to occasional visit this area in the form of a rough looking horse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Caps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beelsby - Unnamed field in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> In an unnamed field in this area treasure is reputedly buried. Two fairy-like entities wearing red caps are said to occasionally appear, searching for the hoard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hunchback</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belmersthorpe - Bluebell Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The area round the well found at this inn is home to a ghostly hunchback.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Clubfoot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Binbrook - Former Binbrooke RAF Base<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The nickname given to an Australian worker on the base who blew himself up trying to sabotage a Lancaster bomber during the Second World War was maybe a bit too unkind, and he was seen for years after his death walking around on the perimeter road. A ghostly NCO also made a comeback at the former base. Sergeant Sinclair managed the loading of aircraft bombs during the Second World War; a mistake caused a bomber to explode on take-off, killing Sinclair while he was trying to flag the aircraft down to alert them to the error. For a short while his ghost was seen on the runway, arms waving wildly in the air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Following Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blyborough - Road by village pond, leading to Grayingham<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> As this creature followed a woman, she turned and lashed out with her umbrella - only for it to pass straight through the creature.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bolingbroke-Castle-hare.jpg' class="w3-card" title='The ghostly hare of Bolingbroke.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ghostly hare of Bolingbroke.</small></span>                      <p><h4><span class="w3-border-bottom">Hare</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolingbroke - Bolingbroke Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1660s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The castle was haunted by a phantom hare. Locals believed the creature to be a ghostly witch once held prisoner in the castle. The entity was said to leap over people or run between their legs, and any dog sent after it would return crying out.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Avro Lancaster</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bonby - Skies over the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 August 2015, 19:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A large aircraft moved silently over the treetops, so low that the witness thought it would land or crash into a field. It was later identified by the witness as an Avro Lancaster.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sarah Preston</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boston - Boston Stump (tower of St Botolph's parish church)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> In one version of the ghost story, Sarah is seen to leap from the tower of the church holding a young child, vanishing just before hitting the ground. Another variation has Sarah dying of the plague - she was blamed for bringing the black death in the town by her adulterous ways, and her cries of 'pestilence!' can still be heard as she runs towards the church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-5.jpg' class="w3-card" title='An old postcard of Boston Stump, St Botolph&#039;s parish church.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Boston Stump, St Botolph&#039;s parish church.</small></span>                      <p><h4><span class="w3-border-bottom">Devil's Breath</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boston - Boston Stump (tower of St Botolph's parish church)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present!<br>
              <span class="w3-border-bottom">Further Comments:</span> The winds which whip and whistle around the Stump are said to be caused by the Devil. He was once cornered by St Botolph; the saint preached so intensely, the only thing the Devil could do was huff and puff, and the gasps have not yet subsided.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hanging People</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boston - Guildhall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> Several ghosts are said to haunt this site, including a phantom woman who peers into the former jail cells, people being hanged, and the sound of snoring.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Carpenter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boston - New England Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> Two members of a wedding party returning to their room were met outside their door by a man in white overalls. He told them he was working in the room next door for the 'next twenty minutes, but would try to keep the noise to a minimum'. Retiring, the guests heard the man next door sawing and hammering for an hour. The following morning the guests complained about the worker to the manager, who told them there were no workmen on site. A few minutes later a female employee at the hotel approached the guests, having overheard the conversation, and told them that the room was haunted by the ghost of a carpenter who had died during the 1970s. The employee also said she had once seen a ghostly woman in a blue dress in the bar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cloaked Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boston - Outside of  Blackfriars Theatre, Spain Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 December 2012, around 02:35h<br>
              <span class="w3-border-bottom">Further Comments:</span> Approaching the theatre from the west end of Spain Lane, this witness noticed a short (5 foot - 5 foot 3 inch) hooded figure in a full length cloak at the opposite end of the building. As the witness neared the figure, it turned, revealing the expressionless face of a young fair haired woman. Within a couple of seconds, the figure vanished into thin air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Watcher Shuck</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bourne - Bourne Wood<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The large black dog here would observe people while they were in the wood, though the creature would never leave the protective tree line. The site may also be the haunt of a grey woman and a grey man.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Road</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bourne - Kings Street, off A15 near Baston<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter, 2003, circa 22:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend says this area of road is haunted by a woman who died there during the 1970s or 1980s. One driver, returning home with three friends in the car, spotted her in the road and swerved to avoid the figure. Looking back, she had vanished, and while all in the car said they had seen her, one said that she seemed to vanish as they approached where she had stood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Catherine Digby?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bourne - Red Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom grey lady who haunts this site is said to likely be Catherine Digby, a popular local lady who died in August 1811.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crusader</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bracebridge - Bracebridge Hall & Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Buried at the nearby church, this knight materialises at the hall (by a portrait) and rides a spectral mare to the graveyard to ensure his body has not been disturbed. He also sometimes appears in the church itself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Reflection</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bracebridge - St John's Hospital (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> September 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> A photograph which was published in the Lincolnshire Echo purported to show a ghostly figure at the window of a discussed hospital, although the image looked suspiciously like a technical fault...</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 270</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=270"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=10&totalRows_paradata=270"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastmidlands.html">Return to East Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="ViewWest Midlands records" style="width:100%" title="View West Midlands records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
