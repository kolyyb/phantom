
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Bolton Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northwest.html">North West</a> > Bolton</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Bolton Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hand on Bottom</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Albion Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1978 / 79<br>
              <span class="w3-border-bottom">Further Comments:</span> The female owner at this hotel felt something grab her bottom as she climbed into the bath. The ghost is also said to play around with the beer pumps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Men with Light Bulb Heads</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Alleyway in town, exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1926<br>
              <span class="w3-border-bottom">Further Comments:</span> A local lad escaped one night from his house to play hide and seek with his friends. While searching for them, he observed three figures in grey rubber suits and fishbowl helmets in a back yard; they also possessed pallid faces shaped like upside-down pears, with slits for eyes and mouth. When they moved towards the boy, he ran home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smell of Smoke</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Alma Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1974-1975<br>
              <span class="w3-border-bottom">Further Comments:</span> A former landlord of the public house reported the smell of cigar smoke in the ladies' toilet, though there was never any evidence of smoke or ash.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Noosed Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Belmont Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This shade, mistakenly hanged for highway robbery, still has the hangman's noose tied around his neck when he appears. They also say his eyes look as if they're ready to pop out of his head. Another report says that the road is also haunted by a fatal car crash from the mid-twentieth century which killed a family - the screech of brakes followed by screams is heard but no accident can be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roman Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Boar's Head public house (no longer standing, replacement building is currently Hogarth's)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pub demolished 1999<br>
              <span class="w3-border-bottom">Further Comments:</span> The Boar's Head was said to be haunted by Roman soldiers. They were only seen chest upwards and could be heard marching through the building. When the pub closed, archaeologists discovered medieval artefacts (including a cellar) before the site was concreted over.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Minnie Stott</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Bradshawgate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1987<br>
              <span class="w3-border-bottom">Further Comments:</span> A teenager who worked for a nearby garage watched a mist as it formed into a human-like shape at the top of a set of steps. The teenager was shocked when later shown a picture of Minnie Stott, a young woman murdered in the 1940s, as that was the figure she had seen manifest.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Churchgate Public House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> There is a story that the pub is haunted by a woman thrown out of the pub one winter's evening, after becoming pregnant with her employer's child. She died in the cold, and her child's cries can now be heard in the building. During the 1950s, bones were unearthed under the site and were thought to belong to a Cavalier who haunts the bar and cellar. Finally, a dog has been seen running upstairs but when chased nothing is found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smoking Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Derby Arms, Derby Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom complete with smoking pipe has been observed in the living room of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smartly Dressed Male</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Eden Grove (former school, closed 2013)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The man who reputedly haunted this site had a whiff of tobacco and sweet flowers about him, with a smart wardrobe consisting of sports coat and flannel trousers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Farnworth cemetery, Darcy Lever<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 11 October 2012, 14:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Alanah and Sherri-Lee Belk spotted a large black cat from around fifteen metres away in a field. The creature ran off in the opposite direction.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bolton-Former-Tax-Office.jpg' class="w3-card" title='Ghost of a murderer walking through slums.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ghost of a murderer walking through slums.</small></span>                      <p><h4><span class="w3-border-bottom">Murder Suicide</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Former Tax Office and ABC Capitol Cinema, Churchgate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1800s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The Tax office and Cinema were built upon former slums Antelope Court. In one of the slums a murder suicide occurred in the 1800s. Ever since, workers of both building reported equipment activating by unseen hands, lights flickering on and off and the spectre of an old man, thought to be the murderer, and his wife, the victim, have been seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Runner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Hall i' th'  Wood Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Runner between 25 December - 6 January (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Always heard but never seen, this spook frequents the old staircase, the footsteps always rushing. Other ghosts reported here include an old woman seen in the kitchen and two men, one dressed in black and the other green.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bolton-Halliwell-Lodge.jpg' class="w3-card" title='A phantom in a nightdress.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom in a nightdress.</small></span>                      <p><h4><span class="w3-border-bottom">Husky Voice</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Halliwell Lodge (public house, no longer operating)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This female ghost would materialise wearing only a nightdress in the early hours of the morning. It was also said her deep husky voice could be heard making suggestive remarks to any men still awake.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Granny</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Moss Bank Cottage, Smithels<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Even though she had never seen her grandmother, a young girl described the ghostly figure of an aging woman that stood over her bed to her aunt, who identified the figure as the girl's grandmother, dying many years earlier. The cottage was also said to be haunted by another old woman, this one sitting in the living room wearing a red shawl.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vida Swift</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Octagon Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Post-1968<br>
              <span class="w3-border-bottom">Further Comments:</span> Dying while carrying out the role of wardrobe mistress, Vida has been seen walking around the corridors and frequenting the stage control box.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Running</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Old Three Crowns public house, Deansgate<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2017 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The sound of running footsteps and breathing have been heard in this pub. An engineer working in the cellar refused to return after he felt an icy hand pass over his shoulder and turn a nearby tap. The investigative team 'Haunted History of Bolton' recorded EVP, footsteps and poltergeist activity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Workshop Worker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Outside the college and along Deane Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly figure has been seen standing outside the college, wearing clothes that identify him as an old blue collar worker.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Private residence, Halliwell<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> On several consecutive nights, a couple were disturbed by the sounds of a crying infant coming from beyond their bedroom wall. After the woman placed her hand on the wall and comforted the infant, the sounds were never heard again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tucker In</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Private residence, Hunger Hill area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> In a residence in this neighbourhood, an old woman has been seen and heard to say, 'I am here to tuck you in'. Disembodied footsteps, an old man, a strange atmosphere and a ghostly hand reaching around a child's bedroom door have also been experienced.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bed Sharer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Private residence, Hunger Hill Estate<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> Accompanying the usual bangs, crashes and raps, this polt could be felt as it climbed into the beds of the building's inhabitants. It would also pinch skin and pull hair. The intrusion ended when the family left the building after a particularly bad night, and never returned.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Dark Clothing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Private residence, Somerset Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> A man dressed in dark clothing leaves the dining room and ascends the staircase. He has reportedly been seen by several guests in the property.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George Marsh</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Smithills Hall (aka Smithells Hall)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 April (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Tried and executed for heresy, Marsh is remembered as his footprint (created when he stamped his foot) in the stone floor turns bloody each April 24. His shade is also reported to haunt the Green Room, where he was interrogated, though that is not date dependent. A priest is also reported to haunt the vicinity, as is a photogenic grey lady and a cat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Slamming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - The Kings Head public house, Wigan Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Doors would slam closed, and the landlord was once awoken by an oppressive atmosphere that flooded his bedroom. The pub is said to have been built on top of a graveyard, the haunting attributed by some to be carried out by phantom Cavaliers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Emily</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Unity Brook Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October 2008 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> This female ghost, dressed in black, appeared to the assistant manager on three occasions. She was described as slim, late twenties or early thirties, with dark hair and high cheek bones. The entity has been nicknamed Emily by staff.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Older Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton - Wilton Arms public house, Belmont Road<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> Sharing a drink with the landlord after locking up, a witness looked across to the fireplace and smiled at an older lady, between 60 and 70 years of age, who sat there. She smiled and nodded back. The witness continued drinking for a minute before realising that all the doors were locked. Turning back to talk to the woman, she had vanished - all doors remained locked and a search of the pub found no one.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 28</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=28"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=28"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northwest.html">Return to North West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
