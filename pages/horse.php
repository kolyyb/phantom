


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Equine Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Horses & Riders</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Phantom Horses & Riders from across the Isles</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/A31-Hogs-Back.jpg' class="w3-card" title='A female ghost was spotted here in the 1960s.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A female ghost was spotted here in the 1960s.</small></span>                      <p><h4><span class="w3-border-bottom">Box Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A31 (Surrey) - Hogs Back, Guildford bound. 1 mile Northeast of Tongham junction<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 08 January 2007, 22:40h, and 1960s (around 03:00h)<br>
              <span class="w3-border-bottom">Further Comments:</span> Travelling home in 2007, a driver watched a horse drawn box carriage cross the road around 100 metres in front of him. The witness reported that he could not make out too many details as it was raining hard, though he was able to see a dim lantern towards the front of the carriage. As the driver reached the spot where he had seen the vehicle pass, he realised there was no side road where the coach and horses could have crossed. Another witness in the 1960s spotted a female figure with long hair, wearing a white dress, standing by the roadside. The witness stopped to see if she was lost or needed a lift, but she had vanished. Mentioning the encounter to a work colleague the following day, the colleague described the figure which was encountered and said the area was haunted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horsemen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A58 (Yorkshire) - A58/A641 junction, also known as Hell Fire Corner<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Both ghostly horsemen and phantom car have reported around this junction. The area is considered an accident black spot, so the question to ask is do the ghosts cause the accidents, or are they the products?</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cloaked Gentleman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth (Dyfed) - Nanteos House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1984<br>
              <span class="w3-border-bottom">Further Comments:</span> One of several ghosts reported to haunt the house, the cloaked figure disrupted a video unit sent to shoot several TV scenes here to such an extent, the crew refused to work after dark. The other ghosts comprise of a phantom horseman and two spectral women, one of whom appears before a death. The building is also thought to have once contained part of the Holy Grail.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Abington-Pig-Lane.jpg' class="w3-card" title='A phantom coach and horses.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom coach and horses.</small></span>                      <p><h4><span class="w3-border-bottom">Coach with Headless Driver</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abington (Northamptonshire) - Pig Lane (lane may no longer exist or has been renamed?)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Racked with guilt after deliberately running over his daughter's lover in 1780, this man now continues to drive his coach and horses down the lane, though he has lost his head since death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Middle Aged Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acle (Norfolk) - Heading west into Acle from Halvergate turnoff<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 March 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Passing the turn off to Halvergate towards Acle, a driver watched as a middle aged man walked out into the middle of the road (from the right hand side) in the path of their car. The figure turned to look at the driver, and the car passed straight through him. The area is traditionally known to be haunted by a horse and cart.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton (Suffolk) - Acton Park, The 'Nursery Corner'<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown date, but appears at midnight<br>
              <span class="w3-border-bottom">Further Comments:</span> Midnight, at an unspecified date, and the park gates are said to burst open, letting in a ghostly carriage pulled by four horses which race to the area known as the Nursery Corner. Local legend states it is here that once a vicious battle took place between Romans and Britons.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pony</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldbourne (Wiltshire) - Road leading to the village from Braydon<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A white pony silently galloped past two men as they returned to the village. It has been speculated that the pony was King Arthur's mount Sigral (ridden at the Battle of Baydon) or belonging to a local man who committed suicide.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldsworth (Gloucestershire) - Larkethill Wood<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Possibly the same entity that haunts the park, a coach and team of horses are said to erratically travel along the road which skirts the woodland.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crump</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldsworth (Gloucestershire) - Lodge park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir John Dutton, nicknamed Crump because of his hunched back, is said to drive a phantom coach and horses under the gates to the park. A white stag may also haunt the parkland.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mr Rimmington</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Almondbury (Yorkshire) - Woodsome Hall golf course<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Rimmington reportedly takes several forms, including a man on horseback accompanied by two hounds, and the slightly less intimidating form of a robin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Antrim-Antrim-Castle.jpg' class="w3-card" title='A strange-looking coach with horses sinking into water.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A strange-looking coach with horses sinking into water.</small></span>                      <p><h4><span class="w3-border-bottom">Sinking Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Antrim (County Antrim) - Antrim Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A coach pulled by a team of four horses which sunk into the pond drowning all on board is seen repeating the tragic accident once a year. In addition, a stone wolfhound which stands by the gateway is said to have once been flesh and blood - the dog gave a warning to an advancing sneak attack upon the castle before turning into rock. It now safeguards the Clotworthy family name on the condition it is never removed. Finally, the sound of heavy breathing can be heard coming from the ruined building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stable Lad</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arbigland (Dumfries and Galloway) - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered to prevent the continuation of an affair of the heart, this young stablehand was then buried at a crossroads to conceal the crime. His ghost is reported to still travel around on horseback.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Richard Costen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardmore (County Waterford) - Ardogina House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Richard died trying to escape from the law for a crime he did not commit. He can be heard screaming, choked to death on the reins of his horse. The spectral Jeremiah Coghlan reportedly rides on the back of a white horse - if seen, the viewer will die within a year. Finally, Sir Francis Prendergast, a psychotic murderer, stalks nearby woodland.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Michael</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arreton (Isle of Wight) - Barrow known as Michael Morey's Hump<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Said to happen at midnight, last sighting unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Michael Morey murdered his grandson and, trying to hide the crime, set fire to the body. Morey's deeds were soon discovered, and his executed remains displayed on the gibbet that stood upon the barrow. At midnight, his ghost on horseback is said to ride three times around the barrow, crying out 'Michael Morey's Hump'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot (Berkshire) - Old road system (now changed)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1967<br>
              <span class="w3-border-bottom">Further Comments:</span> No longer seen since extensive roadwork, a ghostly horseman once ran around the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ascot-Royal-Ascot-Hotel.jpg' class="w3-card" title='Ghostly horse near a demolition site.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ghostly horse near a demolition site.</small></span>                      <p><h4><span class="w3-border-bottom">Pale Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot (Berkshire) - Royal Ascot Hotel (no longer standing - site now a roundabout along the Bracknell Road)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1964<br>
              <span class="w3-border-bottom">Further Comments:</span> In the process of demolishing the hotel, workmen reported encountering a phantom pale horse and hearing disembodied footsteps walking along empty corridors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot (Berkshire) - Woodlands Ride, heading into the forest<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A mother and daughter encountered a carriage being drawn by two white horses. A woman in a crinoline dress sat within the vehicle, while the driver wore an old fashioned cravat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ash-Rectory.jpg' class="w3-card" title='Phantom horses pass through a bedroom.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Phantom horses pass through a bedroom.</small></span>                      <p><h4><span class="w3-border-bottom">Night Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ash (Surrey) - Ash Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1938<br>
              <span class="w3-border-bottom">Further Comments:</span> The rector wrote that he had been awoken by the sounds of galloping and opened his eyes just in time to see a phantom coach and a team of horses pass through his bedroom. It was said that the phantom occurrence had also been experienced by previous rectors, and that the rectory was built on top of an old road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man in Green</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ash Green (Surrey) - Ash Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1930s<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure that appeared in one of the bedrooms looked real, though when a witness tried to grab him, he passed through the figure. Described as short, and looking like a tramp, the ghost had what appeared to be a slashed throat. The nearby fields are haunted by the sounds of galloping horses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swimming Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford (Kent) - Eastwell Park and Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June - Horseman seen (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding towards the park house, this phantom horseman veers off at the last minute and enters the nearby lake. A white lady haunts the house itself, seen by porters on the night shift. In the seventeenth century, the Earl of Winchelsea cut down several oak trees, bestowing a curse on his family which took the life of his wife and son shortly after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lord Lonsdale</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Askham (Cumbria) - Area around St Peter's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Driving a phantom coach, the ghost of Lord Lonsdale passes through this location.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Reduced Rent</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aspley Guise (Bedfordshire) - Woodfield House, Weathercock or Woodock Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1948<br>
              <span class="w3-border-bottom">Further Comments:</span> A man protested about the cost of his rates here and had them reduced after he stated that his house was haunted. He attempted to have them further cheapened by saying that the road itself was haunted by a ghostly horseman, but no further reduction was given. The ghost story behind this is that the shades of a murdered couple haunted the spot before the ghost of Dick Turpin joined them - Turpin had found their bodies and helped the murderer bury them, in return for a safe house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Army</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Attercliffe (Yorkshire) - Near the river<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1661<br>
              <span class="w3-border-bottom">Further Comments:</span> A local vicar, while sitting by the river, watched a line of white troops pass accompanied by white horses - he wrote that it took over an hour for the column to pass.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atwick (Yorkshire) - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This decapitated spectral bandit remains lurking in the shadows of his old ambush area. One version of the story says he remains on his horse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rider</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atworth (Wiltshire) - A365<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter 1944<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver was forced to brake hard when a horse and rider emerged from a hedge on one side of the road and crossed in front of him. The apparition then vanished into a wall on the opposite side of the road.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 531</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=531"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=21&totalRows_paradata=531"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
