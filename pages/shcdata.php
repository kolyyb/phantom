



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Spontaneous Human Combustion -  Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Spontaneous Human Combustion</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>SHC across the Isles</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pensioner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen (Aberdeenshire) - Constitution Street<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 February 1888<br>
              <span class="w3-border-bottom">Further Comments:</span> The remains of an aging soldier were discovered in a barn along this road; much of the body had been reduced to ash, though the face could be recognised. The bales of straw surrounding him failed to catch fire.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thomas Williams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberporth (Dyfed) - Unidentified cottage<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1808<br>
              <span class="w3-border-bottom">Further Comments:</span> Human combustion, but in this case not so spontaneous. A sailor who liked his rum, Thomas Williams fell unconscious after a drinking session. His friend summoned a doctor who arrived after dark. The attending physician held a candle close to Thomas, who promptly caught light, bathed in blue flame. A bucket of water thrown over the fire only made the flames larger. The witnesses all left (for reasons not fully explained) and returned the following morning to find Thomas's torso completely consumed, but his shirt only charred. Thomas's friends said that he had been wicked and that the devil had come for him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">First Irish Case?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballybane (County Galway) - Private residence, Clareview Park<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 22 December 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> The BBC reported that Dr Ciaran McLoughlin, West Galway coroner, had recorded a verdict of spontaneous combustion in a case he had investigated. The body of Michael Faherty had been found in his living room, totally burnt, although damage to the rest of the room was minimal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fire Dancer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chelmsford (Essex) - Unknown dance hall<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 August 1938<br>
              <span class="w3-border-bottom">Further Comments:</span> As Phyllis Newcombe (22) left the dance floor, her dress burst into flame, and she died a few hours later in hospital. Tests showed that her clothing could not be ignited with a cigarette.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Colchester (Essex) - Mill Street<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 February 1888<br>
              <span class="w3-border-bottom">Further Comments:</span> The body of a soldier was discovered in a hay loft, much of his corpse charred by fire, but the surrounding area untouched by the flames. His legs and boots were also undamaged.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Stout</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cootehill (County Cavan) - Unknown property<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1785<br>
              <span class="w3-border-bottom">Further Comments:</span> Mrs Stout was seen going to bed, apparently fine, but found dead the following morning. Her body was heavily burnt, with parts not altered by fire crumbling when touched. Her nightdress remained undamaged by the fire. An inquest determined she had 'died by the visitation of God'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Untouched Armchair</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ebbw Vale (Gwent) - Council house, exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> January 1980<br>
              <span class="w3-border-bottom">Further Comments:</span> The body of a seventy two year old was found reduced to ash, leaving only a pair of feet in socks, a soot covered skull, and an undamaged armchair in which most of the remains sat...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Barry Soudaine</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Folkestone (Kent) - Flat along Canterbury Road<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 December 1987<br>
              <span class="w3-border-bottom">Further Comments:</span> Barry Soudaine was found his kitchen, only a foot and a trainer remaining amongst a pile of ash. Some plastic fittings in the kitchen had melted, but there was not serious fire damage. The coroner recorded an open verdict regarding the death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elizabeth Clark</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hull (Yorkshire) - Trinity Almshouse<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> January 1905<br>
              <span class="w3-border-bottom">Further Comments:</span> Elizabeth Clark spent the night here and was discovered in the morning covered in heinous burns which later claimed her life. No one heard any noises during the night, and although covered in the ash from her nightdress, the bed itself was not scorched.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Torso Reduced to Ash</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich (Suffolk) - St Clement, Ipswich<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1744<br>
              <span class="w3-border-bottom">Further Comments:</span> Grace Pett's burnt body was discovered in her house; other people living there were not aware of the fire that had killed her during the night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blackened Remains</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Nairn (Highland) - Area close to Hardmuir Wood<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 29 July 1852<br>
              <span class="w3-border-bottom">Further Comments:</span> The charred remains of John Anderson could only be recognised by the horses and cart found by his body. One witness said that John had fallen from his cart while burning, his horses remaining still, and that the fire had been extinguish with buckets of water within fifteen minutes of starting. The doctor who performed the autopsy believed the fire to be 'no ordinary combustion'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Annie Webb</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newport (Gwent) - Private house along Corporation Street<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 2 February 1980<br>
              <span class="w3-border-bottom">Further Comments:</span> When Annie was found, only a pair of legs, piece of arm and skull remained. A white tablecloth lay untouched nearby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Carpenter Bursts into Flame</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norfolk Broads (Norfolk) - Norfolk Broads<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 29 (or 30) July 1938 (or 1937)<br>
              <span class="w3-border-bottom">Further Comments:</span> While enjoying a boating holiday, Mrs Carpenter burst into flame and was reduced to a charred corpse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Combusting Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Stanmore (Greater London) - Anmer Lodge nursing home<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> January 1987<br>
              <span class="w3-border-bottom">Further Comments:</span> Peppi the cat enjoyed life at the nursing home, before exploding into blue flame with a bang and a flash. If the story is true, then Peppi could be a rare example of Spontaneous Feline Combustion.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Body in the Basement</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Reading (Berkshire) - Private house along Baker Street<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 November 1978<br>
              <span class="w3-border-bottom">Further Comments:</span> A young daughter found her mother burnt to death in the basement - some witnesses described the body as charred, while the room itself was barely touched.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert Francis Bailey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE11 (Greater London) - 49 Auckland Street, Lambeth<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> September 1967<br>
              <span class="w3-border-bottom">Further Comments:</span> This homeless man's burning body was found in an empty building, blue flame still coming from his charred body. It was though Robert was still alive when he combusted, as his teeth were dug deep into a wooden stair banister pole.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Human Fireball</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Widnes (Cheshire) - Halton College<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1985<br>
              <span class="w3-border-bottom">Further Comments:</span> A girl walking down the stairs with a group of friends suddenly burst into flame. They quickly beat the flames out, but she later died of her injuries.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 17 of 17</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
