
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Berkshire Ghosts and Mysteries from The Paranormal Database.">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southeast.html">South East England</a> > Berkshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Berkshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tap Turning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldermaston - Butt Inn<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The resident poltergeist here turns on the beer taps and closes doors with a bang.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Euerafriad</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldworth - Church<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> John Euerafriad sold his soul to the Devil (for what we do not know), whether he was buried within the church or not. As normal, with this type of deal, he was buried under the church wall (neither in nor out of the church) which denied the Devil his due.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Arbourfield-Pond.jpg' class="w3-card" title='The ghost in the pond managed to upset locals.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ghost in the pond managed to upset locals.</small></span>                      <p><h4><span class="w3-border-bottom">Farmer's Wife</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arbourfield - Pond near White's Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The farmer's wife returned as a ghost soon after her death, moaning as she walked the area before vanishing into a pond. This upset the locals, who summoned seven preachers who performed an exorcism, ending with a heavy stone being placed at the bottom of the pond to prevent the woman's return.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arbourfield (aka Arborfield) - Site of Arbourfield Hall, aka Arborfield Hall (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The site where the hall once stood is haunted by a woman jilted by the building's owner. She slit her own throat under a nearby oak (or yew) tree. Another version of the story says she and the gardener were due to be married, but the girl was murdered by a jealous butler.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Eliza Kleininger</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot - Berystede Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, post-1886<br>
              <span class="w3-border-bottom">Further Comments:</span> Eliza, aka the blue lady, was a French maid who died in a fire which consumed the original house that stood on the site. She was said to have run back into the burning building to save precious jewellery belonging to her mistress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Eliza Kleininger</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot - Berystede Hotel, Bagshot Road - Rooms 306, 361, 362<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed in the 1886 fire that destroyed most of the building, Eliza has returned to hunt for the jewellery lost in the blaze.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot - Huntingdon House (no longer standing, Windsor Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom elderly lady dressed in black would stand silently on the top of the staircase. It was thought that she awaited the return of her husband from the First World War, from which he did not come back. The house was destroyed by fire in 1977, and the ghost has not been seen since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shaggy Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot - Old Huntsman's House<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It has been reported that this house is haunted by a sheepdog-looking entity, which has also been heard barking. Folklore said a man in a red coat also haunted the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot - Old road system (now changed)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1967<br>
              <span class="w3-border-bottom">Further Comments:</span> No longer seen since extensive roadwork, a ghostly horseman once ran around the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flashing Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot - Private residence, Fernbank Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Christmas 1974 to early 1976<br>
              <span class="w3-border-bottom">Further Comments:</span> Flashing lights began outside this property, though they moved inside shortly after. Poltergeist-like activities then proceeded to grow more intense over the months, with larger and larger items being moved, until a visitor recited a prayer on the site and the events ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ascot-Royal-Ascot-Hotel.jpg' class="w3-card" title='Ghostly horse near a demolition site.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ghostly horse near a demolition site.</small></span>                      <p><h4><span class="w3-border-bottom">Pale Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot - Royal Ascot Hotel (no longer standing - site now a roundabout along the Bracknell Road)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1964<br>
              <span class="w3-border-bottom">Further Comments:</span> In the process of demolishing the hotel, workmen reported encountering a phantom pale horse and hearing disembodied footsteps walking along empty corridors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Admiral Sullivan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot - Tall Trees house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend says that the admiral looks out from his old library's window, though there are no witnesses to this.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ascot - Woodlands Ride, heading into the forest<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A mother and daughter encountered a carriage being drawn by two white horses. A woman in a crinoline dress sat within the vehicle, while the driver wore an old fashioned cravat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Furrow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashampstead Green - Grim's Ditch<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Between five and six miles long, this ancient ditch is said to have been created by the Devil and two inhuman helpers in a single night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadowy Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bagley Wood - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> May 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking through the woods after dusk, a couple found themselves chased by a large shadowy quadruped that stood just over a metre tall. One of the witnesses said it did not move like a deer or a dog. The woods is also reputedly home to a phantom highwayman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Landlord</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basildon - Blewbury Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, post-1950s<br>
              <span class="w3-border-bottom">Further Comments:</span> A former landlord is said to be heard walking down the staircase and into the bar late at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Fane</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basildon - The Grotto (house)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Local stories said that Lady Fane haunted her former home, though no recent witnesses exist. While a woman staying here did encounter a phantom young girl during the Second World War, the entity wore a raggedy green dress - surely unsuitable attire for a lady? Another legend places the Mistletoe Bough tale as occurring on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/OI-233.gif' class="w3-card" title='An old woodcut showing a tiny fairy riding a small horse.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut showing a tiny fairy riding a small horse.</small></span>                      <p><h4><span class="w3-border-bottom">Helpful Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beedon - Burrow Hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The little people here are traditionally helpful in nature, and once fixed a plough for a farmer. When archaeologists tried to dig up the barrow in 1850, a storm blew up and drove them away. A golden coffin is said to be buried in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chimes without Wind</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beenham - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October and November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> One resident reported that around this time of year strange things would happen - wind chimes in the bathroom would be vigorously shaken, lights flick on, and a young man has been spotted in a former bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Binfield - Billingbear House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1924<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly white lady haunted this house before it burnt down. One room was transported to Pace College, Manhattan, and is said to contain a ghost (whether it is the white lady is unknown).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse and Rider</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Binfield - Forest Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness travelling along this road had just driven past the Warren pub towards the A321 when they spotted a fast moving horse and rider in woodland on the right hand side of the road. The rider appeared to be topless and looked as if they were sitting where the horse's head should have been, the witness speculating that the entity could have been a centaur. The phantom rode into heavier woodland and the witness lost sight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Women in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Binfield Heath - Crowsley Park House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1987<br>
              <span class="w3-border-bottom">Further Comments:</span> This BBC owned property featured in the press when four people appeared in court charged with offenses relating to breaking into the site. It was claimed that the four had visited the site after hearing stories of how rooms glowed in the dark and phantom women in white walked the grounds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dame Hoby</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bisham - Bisham Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1609 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Dame Hoby, being a great academic, was disappointed that only son was not so gifted. She would constantly push him to study harder, and if he failed then a sound beating would follow. During one punishment, he died, and now Dame Hoby walks the abbey grounds weeping and washing the blood from her hands from a basin that follows her around.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bracknell - Caesar's Camp (hillfort)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Second World War (marching), 2010s (footsteps)<br>
              <span class="w3-border-bottom">Further Comments:</span> Two women who lived near the camp (in a house no longer standing) heard disembodied marching and voices, though nothing could be seen. A phantom man with red hair was also seen by one of these witnesses, though this was a separate experience to the first. More recently, a witness recalled being followed by things falling from trees, always slightly behind but never ahead, and hearing something moving up and down the path, although nothing could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murderous Landlord</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bracknell - Hind's Head Inn (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The landlord would kill wealthy travellers in their sleep, until one man escaped and summoned help. The landlord was executed for his crimes, and his angry ghost haunted the inn until it was pulled down.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 146</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=146"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=146"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southeast.html">Return to South East England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
