

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Dyfed, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/wales.html">Wales</a> > Dyfed</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Dyfed Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Aberaeron-Llanina-House.jpg' class="w3-card" title='A phantom head.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom head.</small></span>                      <p><h4><span class="w3-border-bottom">Hovering Head</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberaeron - Llanina House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly form of a severed head belonging to a murdered young bride reputedly hovers around this building. There are also reports of a phantom man whose clothes are saturated with water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rapping</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberarth - Two unnamed adjoined houses<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> March 1909<br>
              <span class="w3-border-bottom">Further Comments:</span> A resident in one of two homes plagued by unexplained rapping moved out after being unable to stop the sounds. Visitors to the properties traced the sounds to the chimney but were unable to discover the source of the sounds which started at 9pm and ceased at 3am.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thomas Williams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberporth - Unidentified cottage<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1808<br>
              <span class="w3-border-bottom">Further Comments:</span> Human combustion, but in this case not so spontaneous. A sailor who liked his rum, Thomas Williams fell unconscious after a drinking session. His friend summoned a doctor who arrived after dark. The attending physician held a candle close to Thomas, who promptly caught light, bathed in blue flame. A bucket of water thrown over the fire only made the flames larger. The witnesses all left (for reasons not fully explained) and returned the following morning to find Thomas's torso completely consumed, but his shirt only charred. Thomas's friends said that he had been wicked and that the devil had come for him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/magic_aberystwyth.jpg' class="w3-card" title='An old magic lantern slide of Aberystwyth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old magic lantern slide of Aberystwyth.</small></span>                      <p><h4><span class="w3-border-bottom">Bathing Mermaid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth - Cliffs near the town<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> July 1826<br>
              <span class="w3-border-bottom">Further Comments:</span> Twelve people watched a beautiful pale woman washing herself in the sea, with what appeared to be a black tail splashing around behind her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/magiclantern-aberystwyth.jpg' class="w3-card" title='An old magic lantern slide of Aberystwyth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old magic lantern slide of Aberystwyth.</small></span>                      <p><h4><span class="w3-border-bottom">Beast of Bont</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth - Countryside surrounding the area<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Something here killed over fifty sheep in the space of a year; police marksmen were called in to comb the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Brown Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth - Crossroads en route to Waun Fawr<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1901<br>
              <span class="w3-border-bottom">Further Comments:</span> While newspapers reported that a phantom had taken up haunting the lanes in this area, local police maintained that it was a case of mistaken identity - a witness had observed a person in the distance and panicked needlessly.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Devils-Bridge.jpg' class="w3-card" title='An old postcard of the Devil&#039;s Bridge in Aberystwyth, Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of the Devil&#039;s Bridge in Aberystwyth, Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Cheated Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth - Devil's Bridge<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil is said to have built the bridge for an old lady - he expected her soul as a reward but ended up with that of a dog instead. Crossing the bridge at night is said to risk the Devil pushing you off as revenge for the old woman's treachery. Another legend says that a ghost that haunted a nearby house was banished to a cavern under the bridge where it now works.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth - General area of Penparcau<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This headless dog was said to have once belonged to a young giant who ran so fast that he decapitated the dog by pulling too sharply on the leash.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Saint Padarn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth - Lanbadarn Fawr<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Sixth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Lanbadarn Fawr was founded by Padarn. King Arthur once tried to steal Padarn's tunic but was seen. Padarn then caused the ground to open and swallow Arthur, where the king remained until he apologised.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Edna</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth - Market Hall<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2023<br>
              <span class="w3-border-bottom">Further Comments:</span> After claims that items had been snatched from the hands of traders by an unseen force and objects moving without reason, one paranormal investigator said the ghost was Edna, who once ran the fruit and vegetable stall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cloaked Gentleman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth - Nanteos House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1984<br>
              <span class="w3-border-bottom">Further Comments:</span> One of several ghosts reported to haunt the house, the cloaked figure disrupted a video unit sent to shoot several TV scenes here to such an extent, the crew refused to work after dark. The other ghosts comprise of a phantom horseman and two spectral women, one of whom appears before a death. The building is also thought to have once contained part of the Holy Grail.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Model Boat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth - Private residence near Penparcau Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2013 - 2022<br>
              <span class="w3-border-bottom">Further Comments:</span> This property experienced poltergeist-like effects, including unexplained knocking and shuffling. On one occasion, a small white model boat dropped into the lap of one witness, while on another, a coat hung outside to dry was found folded on a bed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming in the Basement</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ammanford - Unidentified house<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> June 2018, ongoing as of June 2019<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple who had lived eleven uneventful years in their home fled after recording screaming and voices coming from beneath their basement. The couple claimed to have recorded hundreds of hours of strange sounds which they believe to come from a secret underground criminal lair. A newspaper report stated that the police had been made aware but had not found anything suspicious.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crowd of People</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bancyffordd - Road by fields east of area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A worker returning to a farm late at night observed a crowd of people surrounding a horse and cart. A loud scream from an unseen source caused the man to flee and he arrived at the farm in a state of shock. Other workers investigated the location of the man's sighting but could see nothing out of the ordinary. A few days later, the man was working the on the same field and watched a funeral procession pass, identical to that he had seen a few nights previous.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Mary</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenporth - Church<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Known as White Mary, this ghost once manifested to a local man and instructed him to find the thief who had stolen the church's communion cup. She gave her champion exact details on where to find the item and the criminal, so it was not long before the missing goblet was returned.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bell for the Dead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenporth - General area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Before a death in the village<br>
              <span class="w3-border-bottom">Further Comments:</span> If the phantom bell is heard to ring three times at midday or midnight, someone of importance is due to die.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Yr Hen Wrach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borth - Cors Fochno (a peat mire)<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Yr Hen Wrach (translated meaning The Old Hag) reputedly stands seven foot tall and makes people ill by breathing on them. Another local legend says the second oldest creature on earth, a toad, lives here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cantre'r Gwaelod</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borth - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 1770<br>
              <span class="w3-border-bottom">Further Comments:</span> To the west of Borth, off the coast, lays the lost land of Cantre'r Gwaelod, a walled country which sunk after a prince forgot to close the flood gates. The bells are said to ring in times of danger, and the lost land was reputedly spotted under the sea in 1770 by William Owen Pughe.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">A Husband Away</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brecon Beacons - Llwynywormwood, also known as Llwynywermod<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1870<br>
              <span class="w3-border-bottom">Further Comments:</span> In an estate now owned by the Prince of Wales, one former occupant claimed to have seen her husband in a bedroom, even though he was four hundred miles from the property at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Saucer Occupants</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broad Haven - Broad Haven County Primary School<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> February 1977<br>
              <span class="w3-border-bottom">Further Comments:</span> A large disc was seen landing near the school by fourteen of its pupils, some of whom also said men with pointed ears and dressed in silver clothing had climbed out. For several months after the event, sightings continued in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broad Haven - Haven Fort Hotel (aka Havens Hotel)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre 1970<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend claimed a white lady haunted the hotel grounds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Silver Suited Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broad Haven - Ripperston Farm<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 April 1977<br>
              <span class="w3-border-bottom">Further Comments:</span> A seven foot (2.13 metres) tall silver clad entity was seen by Billy and Susan Coombs from the window of their farm. The family were plagued by other ufo related experiences around this time, although in 1996 businessman Glyn Edwards claimed he had wandered the area in a silver suit as a joke.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Uniformed Pixies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brynberian - Pentre Ifan Cromlech<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> These little people, standing a couple of feet tall, wore red military uniforms, as if guarding the burial site. Other people have seen the little people dancing over the dolman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monster's Grave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brynberian - Prehistoric burial ground<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The people of the village once found a water monster in a nearby lake - they took the creature and buried it on the mountain slope.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caldey Island - Priory and all over Island<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s, though no sightings since 1929<br>
              <span class="w3-border-bottom">Further Comments:</span> Standing taller than six foot, the monk dressed in black was said to rise from the graveyard at the old priory before drifting off. Though reported on the land immediately around the priory, some islanders encountered him further away, in their homes or on the roads. The ghosts of a white lady and of a madman were also said to haunt the island at some point in the dim and distant past.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 154</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=154"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=154"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/wales.html">Return to Welsh Preserved Counties</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Scotland</h5>
   <a href="/regions/scotland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/secondlevel/Lothian.jpg"                   alt="View Scotland records" style="width:100%" title="View Scotland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
