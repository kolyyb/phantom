
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Coventry Ghosts and Strangeness from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/westmidlands.html"> West Midlands</a> > Coventry</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Coventry Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Female</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - 160, 166 & 167 Spon Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The same female shade was reported to haunt all three of these buildings, witnesses claiming that the short woman wore black and white with her hair up in a bun. The owner of 160 said the ghost would turn on and off electrical appliances, while in 166 the occupier said that the entity smashed glasses and threw stones.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lorry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - A4028 road (now A428?) eastbound, connecting Coventry and Rugby<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom HGV travelling at speed vanishes just before hitting the vehicle travelling in front of it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Coventry-Area-bombed.jpg' class="w3-card" title='Cats sneaking away at night.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cats sneaking away at night.</small></span>                      <p><h4><span class="w3-border-bottom">Escaping Cats</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Area bombed during World War 2<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Second World War<br>
              <span class="w3-border-bottom">Further Comments:</span> Locals say that prior to the blitz which levelled a large part of Coventry, all the neighbourhood cats disappeared as if they knew what the future held.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anti-Cleaning Cleaner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Aston Court Hotel, Holyhead Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Several ghosts are thought to be at home here, including a former cleaner who disliked her job so much she now hides the cleaning equipment in the hotel! A slightly younger ghostly girl is blamed whenever men are hurt on the site - it is stated that she was beaten by a former master and is now extracting revenge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/west2428.jpg' class="w3-card" title='The ruins of Coventry Cathedral.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ruins of Coventry Cathedral.</small></span>                      <p><h4><span class="w3-border-bottom">Droning of Aircraft</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Post-Second World War (aircraft), January 1893 (torchbearer)<br>
              <span class="w3-border-bottom">Further Comments:</span> Since the city was levelled by German bombers during the Second World War, some people have reported hearing 'old' aircraft over the ruins of the cathedral. In 1893, witnesses spotted a human arm holding a blue torch in the north aisle. The arm vanished when one man grabbed a poker and approached it, although manifested again a short time later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Raining Apples</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Coundon, area around B4098<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 December 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> An eighteen metre stretch of the B4098 was left covered in smashed apples after the fruit fell from the sky. One witness watched the apples rain down onto the bonnet of her car, while a man living along the road discovered his garden covered in pulp. A mini tornado was blamed for removing the apples from an orchard and dropping them close by, although the orchard remained unnamed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banging</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Golden Cross<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2016-2017<br>
              <span class="w3-border-bottom">Further Comments:</span> During renovation work, the landlady of the pub reported banging on the doors, open of which would always open itself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">No Ghost Here?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Gosford Books<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> While a local ghost walker/storyteller says that the bookshop was haunted by children who would stack books in the aisles, the current owner of the establishment says he has not encountered anything during his 36 years of running the shop.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Three Bangs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Hairdressing Salon<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> October 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> A visitor to this quiet salon was using the upstairs bathroom. The door would not close properly so was left slightly ajar. Suddenly there were three loud knocks and the door was pushed open with force, but no one could be seen and nothing more was heard, even though the area was covered in creaky floorboards.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Coventry-High-Street.jpg' class="w3-card" title='Do you want to go blind? Because that&#039;s how you go blind.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Do you want to go blind? Because that&#039;s how you go blind.</small></span>                      <p><h4><span class="w3-border-bottom">Peeping Tom</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - High Street<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 1200s<br>
              <span class="w3-border-bottom">Further Comments:</span> The original 'Peeping Tom', Tom was a tailor who looked out on the naked Lady Godiva as she rode through the city to protest at her husband's tax increase. All the town's inhabitants stayed in their houses and did not look; Tom was too curious for his own good, and gazed out as Godiva rode passed, and was stuck blind for doing so.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unclear</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Manor house (demolished 1964), Wilsons Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1964<br>
              <span class="w3-border-bottom">Further Comments:</span> Known locally as a haunted house (before being destroyed), but the exact nature of the phantoms at work is not clear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Galloping Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Moat House Lane, Canley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown - late twentieth or early twenty-first century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking along the lane, one man heard wheels moving along the road followed by the sound of galloping horses - the witness had to jump out of the way. Other people have reportedly heard farm animals in the area, although none are around.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Art Student</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Old railway building (currently Horizon Recording Studio?), Warwick Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> This building is haunted by both an art student who died of a drug overdose and by a former rail employee. People recording at the studios have felt something brush past them or breathe heavily down their necks. Disembodied footsteps run down the corridors, while lights also turn themselves on and off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Former Landlords</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Old Windmill public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> According to the Spirit Team UK website, this pub still contains the memories of former landlords and staff. One entity tried to push someone into the fireplace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Laughter and Music</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Prince William Henry public house, Foleshill Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> May 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> The basement of this pub has recently been subjected to phantom music, laughter and a dark shadowy stranger.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Private cottage, Wall Hill Road, Allesley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1930s - 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> Occupants in this cottage witnessed three ghosts, one a woman in white who resembled a nun, a woman in black, and a man wearing a nightshirt, handing a candle, descending the staircase Doors would open unaided and footsteps crossed the upstairs landing. The family would also hear rattling chains several times a week; when renovation work took place on the roof, a secret room was discovered that chains hanging from a wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Videoed Polt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Private residence in Holbrooks<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> The Daily Mail reported that the family here were being terrified by an entity which threw things around the kitchen and opened and closed drawers. Video evidence, of a chair moving and cupboard doors opening, was less than convincing. The paper later stated that Derek Acorah had banished the phantom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chair</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Private residence, Kenilworth Road<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown if still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The artist John Heritage-peters lived at this site and threatened to haunt anyone who removed a chair from the gallery. The artist died in 1965 and subsequent house owners honoured his wish. When offered for sale in 1988, the house featured in the local press, complete with chair and associated folklore.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Piano</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Private Victorian House, Humber Avenue<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> An occupier of a house along this road reported hearing the twangs of a piano, as if an unseen hand plucked the strings rather than hit keys.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Falling Items</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Purdy's Pet Shop<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2020 - 2022<br>
              <span class="w3-border-bottom">Further Comments:</span> CCTV showed items falling from shelving and customers reported something tugging on their clothing. A ghost hunting group conducted a seance on the site to raise money for charity and claimed to have contacted a former male occupant.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thomas Wildey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Ryton Bridge, River Avon<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wildey murdered a mother and her daughter, for which he was hanged. His body was then placed in a gibbet by the bridge where it slowly rotted for many years. The strange patchy mist that hangs over the area is blamed on the ghost of the man, as are the lights seen to flitter nearby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Photogenic Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - St Mary's Guildhall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 1985<br>
              <span class="w3-border-bottom">Further Comments:</span> Caught on film at an official function, a figure in a hooded gown can plainly be seen standing a few meters from the head of the table. A good image of this photo can be found in 'Haunted Britain and Ireland' by Richard Jones.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Dustpan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Swanswell Gate, Hales Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> June 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> Three men decided to spend the night here to raise money for charity after they saw a dustpan unhook itself from a wall and fly across the room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unknown Force</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - Terry Road<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> April 1994<br>
              <span class="w3-border-bottom">Further Comments:</span> A 'very transparent ripple' hit a person walking their neighbour's dogs, chilling the person with such dread they screamed out loud. It was a week before they were about to sleep properly.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coventry - The Establishment bar and grill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> A deputy manager was said to have seen on CCTV a child-like figure looking like they were playing hide and seek. When investigated, no one could be found. A newspaper photographer claimed to have taken a picture of a ghost in a mirror, although the 'phantom' resembles a normal person/slow shutter speed combination.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 29</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=29"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=29"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/westmidlands.html">Return to West Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="View West Midlands records" style="width:100%" title="View West Midlands  records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>East Midlands</h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
