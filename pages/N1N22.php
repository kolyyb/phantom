
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="London Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining folkloric, paranormal &amp; cryptozoological locations in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > N1 - N22 Districts</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>London (N1 - N22) Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Richard Cloudesley</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N1 - Area around St Mary's Church, Upper Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1517<br>
              <span class="w3-border-bottom">Further Comments:</span> Cloudesley is said to have returned from the grave shortly after being buried in 1517. Though details are vague, but the field in which Cloudesley was buried was found disturbed (the earth 'swelling'), and a team of exorcists were called to lay the phantom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sam Collins</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N1 - Collins' Music Hall (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1963, 9pm (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The spirit of Sam would slam all the doors in the building shut at 9pm exactly and took exception of strangers spending too much time in his former work environment - one man felt dead cold fingers tightening around his throat as he made himself at home on the office sofa.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N1 - Old Queen's Head public house, Essex Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century sightings, running sounds occur on first Sunday of the month<br>
              <span class="w3-border-bottom">Further Comments:</span> This young girl has been heard running around the upper part of the pub and has pushed against at least one member of staff. Other reports exist of an unknown woman in Tudor dress seen drifting around the site, and gentle moaning coming from the upper part of the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cowled Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N1 - Private residence, De Beauvoir Square, De Beauvoir Town<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1898<br>
              <span class="w3-border-bottom">Further Comments:</span> One woman claimed to have seen a greyish white figure with its face covered by a cowl in a property in this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N1 - St Mary's Parish Church, Islington<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 December 1898 (ghost), January 1899 (large crowd gathers)<br>
              <span class="w3-border-bottom">Further Comments:</span> After a local paper printed a story concerning the appearance and disappearance of a white figure outside the church on Christmas night, a large crowd of people gathered looking for the entity. A large police presence eventually moved the crowd on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Possibly the Vicar's Daughter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N1 - St Peter de Beauvoir, De Beauvoir Town<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> September 1898<br>
              <span class="w3-border-bottom">Further Comments:</span> The churchyard became the centre of attention for hundreds of visitors after a rumour started that the location was haunted. Police arrested at least five people for disorderly conduct. One suggested explanation was that the 'ghost' was the vicar's daughter who would wear white and enjoyed cycling through the bushes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cowled Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N1 - St Peter's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A grey-white figure with its face covered by a cowl was said to haunt this church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">William</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N1 - Whittington Hospital, Islington<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Ghosthunter Leonard Low claimed to have taken a photograph in the hospital of 'William', a ten year old boy from the fifteenth century who suffered from leprosy. Leonard said that the ghost stories he was told from nurses concerning the haunted hospital date back at least forty years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Male Presence</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N10 - Private residence, Muswell Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1954-1974<br>
              <span class="w3-border-bottom">Further Comments:</span> A former resident at this house reported a male presence haunted the site and would pull the covers off the bed and chill the air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Laundress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N12 - Whestone<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This area was once haunted by the apparition of a laundress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with Hollow Eyes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N16 - 69 Spencer Grove (no longer standing), Stoke Newington<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> September 1967<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghost, dressed in white, was seen emerging from a wardrobe by several members of the family. The family finally left the building after a mysterious fire broke out.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N16 - Private residence along Dunsmures Road, Stamford Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> This house was haunted by a woman who could be heard crying at night. There was also a report of the living room carpet being found covered in ants and other insects - when the witness returned two minutes later, there was no trace of the creatures.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Constantia Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N17 - Bruce Castle, Tottenham<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 November (Coleraine seen) (reoccurring), party seen in 1971<br>
              <span class="w3-border-bottom">Further Comments:</span> The Lady Coleraine is seen once a year, screaming as she jumps from a balcony trying to escape the possessive nature of her husband. A couple walking past one night witnessed a party occurring on site, complete with people dressed in eighteenth century garb - they realised something was strange, as there was no sound, and the figures appeared to glide...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flashes of Light</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N17 - Tottenham<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 July 1813<br>
              <span class="w3-border-bottom">Further Comments:</span> Intense flashes of light were reported coming from a perfectly clear sky.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Orange Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N19 - Skies above Archway<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 2 February 2007, 17:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> It was reported that traffic was brought to a standstill when between 10 - 15 orange lights were seen moving in a southerly direction. The UFOs all moved at the same speed, before stopping and moving upwards. The sighting lasted for about ten minutes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Faint Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N19 - Unnamed council flat<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1978<br>
              <span class="w3-border-bottom">Further Comments:</span> The couple living in this flat, as well as neighbours, reported that something unseen was moving heavy items around the property. They also reported a faint outline of a figure. The entity vanished after the site was blessed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Steam Train</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N2 - East Finchley to Wellington Sidings underground<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This stretch of the Northern Line is reputed to be haunted by a spectral steam engine.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N2 - Strawberry Vale, East Finchley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> The pallid ghost of a lady was reported here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/jack001.jpg' class="w3-card" title='A nineteenth century illustration of Spring Healed Jack.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A nineteenth century illustration of Spring Healed Jack.</small></span>                      <p><h4><span class="w3-border-bottom">Fire Spitter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N21 - Green Dragon Alley<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 28 February 1838<br>
              <span class="w3-border-bottom">Further Comments:</span> The attack on Lucy Scales along this road was attributed to Spring Heeled Jack; Lucy was hit in the face with blue flame which emerged from a cloaked figure who had stepped out in front of her. Lucy's sister witnessed the attack and her screams brought assistance. The attacker, described as a tall, thin gentleman walked off carrying a lamp (or bounded away, depending on the source).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Forts</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N22 - Dip in road close to Kerry Way, Curraglass<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> A member of parliament made headlines after he declared a dip in the road, which had cost forty thousand Euros to repair, had been caused by fairies who lived in forts which the road navigated around.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Imprisoned Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N3 - Avenue House, East End Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Paranormal investigation groups claim to have located two ghosts within the estate; a woman-hating man remains in the Turret Room, while a child remains locked in the basement. Other stories include a phantom governess and disembodied footsteps, while during the Second World War women staying in the house would be disturbed at night by something unseen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dora</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N3 - Finchley's Avenue House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure, possibly that of a Victorian governess named Dora, was seen walking through a closed door twice by different witnesses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N3 - Nether Street, West Finchley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One police officer is said to have lost his mind after seeing this phantom woman and became obsessed with reading everything he could about local ghost stories.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Running Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N4 - Gloucester Drive, Finsbury Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa first half of 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> The disembodied sounds of children running up and down the road and outside people's houses has been heard, though often no-one can be seen. One resident briefly spoke to the entities, which vanished after twenty seconds or so.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Goat Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N4 - Parkland Walk<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s / 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A local urban legend said that a part-goat part-man entity haunted this walk and local children dared each other to run down the path late at night. A sculpture of a spriggan can be now found along the walk, with some believing its placement was inspired by the legend.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 42</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=42"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=42"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Main London Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Reports: People</h5>
     <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View tReports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2024</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
