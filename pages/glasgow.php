
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Glasgow Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Glasgow</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Glasgow Ghosts, Folklore and Forteana</h3>

 


                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Glasgow-Mansfield-Street.jpg' class="w3-card" title='A ghostly tall man.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly tall man.</small></span>                      <p><h4><span class="w3-border-bottom">Partick Poltergeist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - 23 Mansfield Street (since demolished)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1959-1961<br>
              <span class="w3-border-bottom">Further Comments:</span> The family living in this home experienced two years of inexplicable noises and constant coldness. They finally fled the site after watching a piece of coal levitate and drop back into a bucket, and a ghostly tall man standing over the children's bed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Arches, below Glasgow's Central Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Reported in a national newspaper, the Alien Wars show is home to a phantom little girl dressed in old style clothing. While actors from the show are said to be afraid of the ghost, the phantom herself is reputedly frightened off by the show's special effects.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Reston Mather</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Auchinlea Park - Provan Hall and Blochairn House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Reston Mather, one of the last owners of the property, is thought still to haunt Blochairn House. Provan Hall is home to a phantom woman and a young boy, who are said to have been killed in the upper part of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with a Lantern</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Barlinnie Prison<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The area outside the surgery wing and the bricked up entrance to the old execution block are home to a phantom woman dressed in Victorian clothing, holding a lantern.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bathtime Murder</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Blythswood Square, private house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A family living here reported being witness to a ghostly murder - a beautiful woman drowning a man in the bath. The family moved out soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sounds of Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Cathedral House Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of children have been reported coming from the top floor of this hotel. Other people have claimed to feel something invisible brush against them. In a newspaper interview in 2022, it was stated that Suzi Quatro had reported seeing a ghost here to the manager.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man with Roman Nose</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Cathedral Street, and private house in the area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-mid-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly figure of a man was said to follow owners or guests out of the house and to the nearby cathedral if they, or a loved one, were going to shortly die.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Opening Doors</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Central Hotel<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1976-1982<br>
              <span class="w3-border-bottom">Further Comments:</span> A former worker at the hotel reported that they would walk around during the early hours of the morning closing all the fire doors on the fourth floor, which would then reopen and close themselves a few moments later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Commercial property, St Vincent Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness who spotted an elderly woman staring at herself in a mirror in the men's toilets found humour in the situation until he told a colleague about the incident. Realising that the woman was wearing old fashioned clothing, that no one of that age worked in the building, and that security would not let anyone off the street in, the witness returned to the toilet and found it empty. Workers nearby had not seen anyone leave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Auld Wives Lifts</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Craigmaddie Muir<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> These three stones are said to have been carried here by a trio of married women who wanted to know who could carry the heaviest weight. They are set out so two of the stones carry the third, and another legend states that if one visits the site and does not climb through the hole, then they will die before having children.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Gurning Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Crosshill area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1976-1979<br>
              <span class="w3-border-bottom">Further Comments:</span> A story popular on the internet says that over a four year period several women were said to have encountered a strange looking man who made animalistic sounds. The figure appeared in both homes and along streets, quickly vanishing without trace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Suicide</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Dalmarnock Road Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1972<br>
              <span class="w3-border-bottom">Further Comments:</span> A man in his 30s wearing black trousers and a blue coat was seen to leap over the bridge by one witness, only to vanish into thin air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Falling Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Edinburgh Road, Cranhill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 2016<br>
              <span class="w3-border-bottom">Further Comments:</span> While walking towards a shop, one witness watched a man stumble across the road holding his throat before falling against a fence. As the witness approached, the figure vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Deil's Kirkyard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Former marshland (no longer present, replaced by yachting pond, Camphill Park)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1830<br>
              <span class="w3-border-bottom">Further Comments:</span> The site where the fallen from the Battle of Langside were buried, a woman staying nearby watched as the dead rose and marched down the nearby road. The woman ran to her minister, who is said to have exorcised the ghosts so they never appeared again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-George-Square-Glasgow.jpg' class="w3-card" title='An old postcard showing George Square in Glasgow.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing George Square in Glasgow.</small></span>                      <p><h4><span class="w3-border-bottom">Two Men Walking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - George Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen in the early hours of the morning, these two shades out for a walk wore eighteenth century clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Green</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Gothic villa close to Springburn Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown - twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman wearing a long green dress was once said to wander the property with her dog.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Diana</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Govan Old Parish Church<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa early 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> A video recorded by Chinese tourists at Govan Old Parish Church appears to show Princess Diana standing by a stained glass window. The jury appears to be out to whether the footage is faked, an optical illusion or something a little spookier.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Taken by Fairies?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Govan Road<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 January 1966<br>
              <span class="w3-border-bottom">Further Comments:</span> While out in the early hours of New Year's Day in the company of his two elder brothers, Alex Cleghorn vanished while standing by their sides, and was never seen again. A few of the locals said it was the little folk, as who else could have pulled off such a disappearing act?</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shawled Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Grahams Bar (now O'Brien's), Saltmarket<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An elderly woman wearing a shawl covering her head would once make infrequent appearances to staff and visitors alike.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flashback</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - High Street, just after The Royal Infirmary and to the Tolbooth Clock<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996-1997<br>
              <span class="w3-border-bottom">Further Comments:</span> On three occasions between 1996 and 1997, a witness (and on one occasion, their friend) looked out the window of their flat and watched the street below fill Victorian figures, including ladies with long dresses, workmen carrying hods and horses and carriages. Each time the scene lasted for around ten minutes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Former Wife of a Caretaker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Hutchesons' Hall, Ingram Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s-2005<br>
              <span class="w3-border-bottom">Further Comments:</span> A former worker at this site said it was haunted by a grey lady, who could have been the wife of a caretaker or a cleaner. The entity would open and close doors and would be seen looking out of a window from the street. Footsteps would also be heard on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Devil on Top</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Jack's Mountain (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1950s<br>
              <span class="w3-border-bottom">Further Comments:</span> Jack's Mountain was a local name for a massive pile of rubbish and slag which stood, by some accounts, 30 metres tall. A local legend said that the Devil lurked on top.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man Holding Throat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Langbar, Easterhouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970<br>
              <span class="w3-border-bottom">Further Comments:</span> A tenant in a flat here reported their home as being haunted by a former occupant seen walking around the site holding his throat. A priest was called to perform an exorcism, although the outcome of the ritual is not known.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Three Orange Globes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - London Road, Carm<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1979, 16:40h<br>
              <span class="w3-border-bottom">Further Comments:</span> An apprentice joiner spotted three bright orange globe-shaped lights in the sky while walking from work. He rushed home and told his parents, who laughed at his tale until a local radio broadcast mentioned several people had reported orange lights over the city.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Septimus Pitt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Mitchell Library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Although Pitt the phantom is the creation of Scottish poet Brian Whittingham, staff at the library have reported icy chills on the second floor.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 44</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=44"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=44"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
