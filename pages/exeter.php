
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Exeter Hauntings and Strangeness from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Exeter</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Exeter Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Shadow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Alleyway that runs from the top of South Street to the Cathedral Green<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 April 2010, 01:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> As a man walked down the alleyway, he became aware of a dark shadow looming over him and felt a presence over their right shoulder. The man turned and saw a black mass which seemed to dissipate upwards into the night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Henrietta_Maria_Queen.jpg' class="w3-card" title='Portrait of Queen Henrietta Maria.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Portrait of Queen Henrietta Maria.</small></span>                      <p><h4><span class="w3-border-bottom">Queen Henrietta Maria</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Barnfield House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This Queen stayed here to give birth before escaping to France, though she also appears to continue to remain here today.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Three Headed Entity</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Cathedral Green<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Multi-headed entity unknown, hand felt circa 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen moving silently across the Green, one strange apparition is reported to possess three heads. A man walking with a group of friends across the yard felt a hand grab his right shoulder and pull him backwards. Thinking it was one of his friends, the man turned around, but nobody was there - all the group were ahead of him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fish Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Coastline<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1737<br>
              <span class="w3-border-bottom">Further Comments:</span> Fishermen on the shore caught a four foot tall humanoid, with duck-like feet and a tail protruding from its back. The creature tried to escape but was killed when the fishermen beat it with sticks. Another fish-man was caught a few months later in the same area, though this one was described with more seal-like qualities.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Plague Victims</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Devon Air Radio Studios, St David's Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Built on an old plague pit, the ghosts of the inhabitants are blamed for the sounds of closing doors (in places the doors are already closed) and other unexplained events.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Ernest Jones, next to the Guildhall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A female ghost is said to vanish as quickly as she appears. She is blamed for knocking over displays and setting off the fire alarm.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Exeter Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> July, 7pm (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen only around this time, the nun quickly disappears once spotted. A monk has also been reported in the area surrounding the cathedral, while a small group of witnesses heard music coming from within the building although it was dark and empty at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Last Executed</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Exeter prison<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1973<br>
              <span class="w3-border-bottom">Further Comments:</span> Two prisoners reported seeing a middle-aged man walking along the upper gallery, vanishing as he reached a cell door. After staff carried out a little research, they discovered the man was the last person to be executed in the prison.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Workman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Exeter University<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1967<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom man dressed in painter's overalls has been seen walking down the corridors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cycling Through</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Exeter's Underground Passages<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The guides of the underground passages, normally open to the public, tell of a phantom cyclist who passes through the area. There is also rumoured to be hidden treasure somewhere in the tunnels.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mr Jones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Fore Street<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Mrs Burrow spotted Mr Jones, a retired silversmith a little way ahead of her along this street. Burrow walked faster to catch up, but Jones vanished as she reached him. The following day Burrow received a message that Jones had died at the moment she had seen his ghost.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gardener's Father</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Franklyn Mansion gardens<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> While working in one of the gardens, the gardener claimed to have spotted his father's ghost.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Silk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Globe Hotel<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 1932<br>
              <span class="w3-border-bottom">Further Comments:</span> An American guest at the hotel reported waking several times during the night to the sound of swirling, as if someone were waving armfuls of silk scarves through the air. The woman in the room next door also reported hearing the same noise.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rose Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Guildhall Shopping Centre basement<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A shadowy form of a woman has been reported as haunting this area, the smell of roses accompanying her presence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Nurse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly nurse occasionally appears by the bed of anyone close to death and rearranges any nearby flowers so that they form a cross.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Lord Haldon Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This tragic figure fell pregnant after having an affair with a local landowner - he killed the maid to cover the relationship up, and now she returns to the top of the building, dripping wet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant Bat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Magdalen Road<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 May 2000<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness walking down the road late at night stated he watched a giant bat, with a wingspan of 1.2 metres, swoop around the churchyard along this road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Heavy Breathing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Marks & Spencer store<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to be built on an old Roman burial ground, phantom heavy breathing has been reported from staff, as have light poltergeist-like behaviour.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wayne004.jpg' class="w3-card" title='A pencil sketch by Wayne Lowden of a ghostly crying woman.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A pencil sketch by Wayne Lowden of a ghostly crying woman.</small></span>                      <p><h4><span class="w3-border-bottom">Crying Mother</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Martin's Lane, general area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A weeping woman waits along this lane at night - her daughter ran away to follow Sir Francis Drake on his last voyage, and never returned.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Okehampton Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 09 November 2013, 03:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Crossing the bridge, this witness could hear footsteps walking on loose stone. Stopping to listen for a couple of minutes, the footsteps continued to crunch, although no one could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crowd Pleaser</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Private home in St Sidwells area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1897<br>
              <span class="w3-border-bottom">Further Comments:</span> Police were called is disperse a crowd of people who had gathered to catch a glimpse of the ghost which had taken up residence in this house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Private residence, Whipton<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Not known<br>
              <span class="w3-border-bottom">Further Comments:</span> While entering their home, a student could see a woman wearing a dress standing on the other side of the front door. The woman stood watching the student until they opened the door, upon which the figure vanished. The student's mother was at home but not near the front door at the time (and was also wearing clothing different to what the student had seen).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Girl with Rag Doll</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Prospect Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This young child only appears once a year in the upper part of the building. She is said to smile sweetly at any witnesses, before quickly fading away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-42.jpg' class="w3-card" title='An old postcard of the River Exe and Exeter.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of the River Exe and Exeter.</small></span>                      <p><h4><span class="w3-border-bottom">The Vikings are Coming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - River Exe<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend has it that a ghostly Viking long ship complete with an angry fighter passes up the river.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coughing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Exeter - Royal Clarence Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Built on the site of Walter Raleigh's fathers' house, some attribute the coughing sounds to Walter himself.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 34</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=34"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=34"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
