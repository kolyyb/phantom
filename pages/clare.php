

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Clare Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Clare</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Clare Ghosts, Folklore and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ballyvaughan.jpg' class="w3-card" title='A strange looking black goat.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A strange looking black goat.</small></span>                      <p><h4><span class="w3-border-bottom">Black Goat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyvaughan - Poulaphuca, area around dolmen<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This goat, though by some to be a fairy being, resides over the tombs in the area. The creature causes anyone who threatens the land to develop a hunchback.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banshee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bunratty - Rathlaheen Cottage, near Bunratty Castle<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1956<br>
              <span class="w3-border-bottom">Further Comments:</span> The wails of the banshee were heard within this building just before a death occurred.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lord Clare and Troops</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carrigaholt - Fields near Carrigaholt Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantoms of Lord Clare and his yellow dragoons were said to have continued their manoeuvres well after their deaths.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lucky</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carrigaholt - Rahona Lodge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1917<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghost was described as 'lucky' by former owner Charlotte Kean, as it could spend as much time as it liked at her family's summer home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carrigaholt - Road between Carrigaholt  and Ross (R487?)<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom hound which haunts the road connecting these two locations was thought to be the ghost of Robin of Ross in animal form.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Maire Rua</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clarecastle - Carnelly House driveway<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Marie was a tough woman who, during her lifetime, received a pardon for two counts of murder. Legend says she was buried in a hollow tree, and as a result her ghost haunts the driveway.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Stamer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clarecastle - Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The wife of the first Colonel George Stamer of Carnelly was said to have accidently dropped her child into the nearby river. Her phantom was said to walk along the banks, looking for the infant.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Moher-co-Claire.jpg' class="w3-card" title='An old postcard showing the Cliffs of Moher in County Clare.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing the Cliffs of Moher in County Clare.</small></span>                      <p><h4><span class="w3-border-bottom">Missing City</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cliffs of Moher - Sea off the coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One story says that a great city was lost off the coast here, and manifests once every seven years. When seen from the coast, a person had to keep their eyes fixed on the city until they reached it, otherwise it would vanish.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Puca</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clonlara - Bridge (may no longer be standing), two miles south west of village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A dark shadow-like horse was named by some to be a puca, although it would appear no one stayed around long enough to find out for sure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Clonlara-Canal-bridge.jpg' class="w3-card" title='The ghost was soon banished.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ghost was soon banished.</small></span>                      <p><h4><span class="w3-border-bottom">Banished Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clonlara - Canal bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1769<br>
              <span class="w3-border-bottom">Further Comments:</span> A female ghost appeared and haunted the area around the canal bridge as it was being constructed in the 1760s. The entity was banished by the laying of a slab with her likeness upon it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach and Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Corofin - Area around Cragmoher<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom horse and coach were said to have been encountered in this area, although the finer details are unknown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Corofin-crossroads.jpg' class="w3-card" title='A banshee breaking the norm.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A banshee breaking the norm.</small></span>                      <p><h4><span class="w3-border-bottom">Warning the Inmates</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Corofin - Crossroads near the former workhouse<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Considered strange behaviour for banshees, these Corofin entities would appear close to the workhouse and foretell the death of inmates.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Brocshee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Corofin - Rath Lake<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1931<br>
              <span class="w3-border-bottom">Further Comments:</span> Several local farmers reported seeing a giant badger swimming in this lake, and the story made the front page of the local newspaper.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Portent of Doom</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cratloe - Woodland<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> To be followed or escorted by this black hound would result in misfortune soon after the encounter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vault Dwellers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Doonmore - Doonmore Castle (ruins by the coast)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The vault beneath this castle was said to be home to the most awful sounds, either caused by the souls of former prisoners or the waves crashing against the rocks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Donn of the Sandhills</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dough - Close to the site of the old castle<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Donn was said to be one of the most powerful fairy kings, but he was forgotten about as he lacked a bard to sing his praises.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man with Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drumcliff - Around the ruins of the round tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It was said that this phantom man with his ghostly hound would walk quickly around the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drumcliff (aka Drumcliffe) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once thought to haunt this area, the phantom black hound would appear to be mostly forgotten about.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ennis - Ballyalla House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen standing on the staircase, this little elderly lady sometimes makes an appearance when visitors arrive.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Procession of Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ennis - Park near Clare Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be heading towards the abbey, a line of phantom monks would walk slowly through the park.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/1corpse-candles.jpg' class="w3-card" title='An illustration of ghostly looking men holding long candles.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An illustration of ghostly looking men holding long candles.</small></span>                      <p><h4><span class="w3-border-bottom">Corpse Candles</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ennis - Ruins of Killone Abbey Convent<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, likely to be nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Lights which darted around the headstones were once said to haunt these ruins.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ennistymon - Private cottage, south side of town<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> A visitor to the cottage fell ill at ease and became convinced that it was haunted, something to do with the death of a child. Another visitor to the cottage experienced nothing in the building but felt that the town had 'the guts and heart knocked out of it.' Later research revealed that several children had been murdered close to the cottage by Crown Auxiliaries during the War of Independence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ennistymon (aka Ennistimon) - Ennistymon House (now Falls Hotel)<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The grounds around this house were said to be home to a black dog, which despite its size, was quite harmless.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ennistymon (aka Ennistimon) - Wooded glen near stream/waterfall<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 11 December 1876<br>
              <span class="w3-border-bottom">Further Comments:</span> A servant of the MacNamaras family heard a phantom coach while walking through this area. He ran, opening gates to let the invisible vehicle through, and it drove past him on the third gate. The family later learned that Admiral Sir Burton MacNamara had died in London.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Inchiquin - Cahernanoorane (fort)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Fairy song was said to have been heard coming from this fort.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 58</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=58"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=58"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
