

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Welsh Ghosts, Folklore and Strange Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/wales.html">Wales</a> > South Glamorgan</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>South Glamorgan Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wellington</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdare - Sky over Cwmbach estate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1979<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness outside their home heard engines but could see anything. They stepped inside their doorway but as the sound of the engines grew louder the witness stepped outside again. Looking up, the witness spotted a twin engine, dark coloured Wellington bomber fly over and past, as 'real looking and sounding as any solid aircraft'. At the time, the witness thought nothing was strange about the encounter as they were unaware that there were no flying airworthy examples of a Wellington that existed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman Standing in Corner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barry - Royal public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> The pub is said to be haunted by a female figure. The last time it was seen was in the men's toilets, standing by the wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smiling Faces in the Curtains</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barry - Unknown semi-detached council house<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman living in the building set her curtains on fire after she became convinced they were possessed, causing her to lose her looks. She was jailed for 15 months for arson.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wissie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boverton - Boverton Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early Nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly lady in black who haunts the area was the wife of King John - he condemned her to exile here, though she apparently continued to love him dearly. Her tall, sobbing form was seen by a couple of workmen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broughton, Vale of Glamorgan - Unnamed Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s?<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a young girl is reputed to haunt this area, one story stating that a doctor driving through the area hit the figure - believing her to have been real, he jumped out his car to help her, but she had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Murder Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cadoxton - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present, sighting of ghosts pre 1920s?<br>
              <span class="w3-border-bottom">Further Comments:</span> A stone monument was erected in the churchyard in memory of Margaret Williams, whose murdered body was found on nearby marshland in 1822. Because she was pregnant at the time of her death, it was widely believed her lover and her killer were the same man. Nonetheless, Margaret's killer was never caught. Two ghostly forms were reported by the stones, thought to be of Margaret and her former lover.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Richards</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Caerau - Unnamed House<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> While laying in bed, a witness claimed to see a former neighbour, John Richards, standing close by holding a dimly burning taper. Richards had died ten years previous; the witness died the following day.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wales2887.jpg' class="w3-card" title='Cardiff Castle, Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cardiff Castle, Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Cardiff Castle<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Coach last seen 1868<br>
              <span class="w3-border-bottom">Further Comments:</span> A coach is reportedly heard when a member of the Hastings family is due to die. At the castle it was heard by John Boyle, the night his cousin the Marquis Hastings died. Other ghosts reported to haunt the castle include the second Marques of Bute (who walks through several walls), a faceless woman in a long skirt who is known as Sarah, and a three metre tall giant who walks around the park.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wales6642.jpg' class="w3-card" title='Cardiff Docks, Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cardiff Docks, Wales.</small></span>                      <p><h4><span class="w3-border-bottom">The Smoking Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Cardiff Docks<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1962<br>
              <span class="w3-border-bottom">Further Comments:</span> A teenager fishing with a friend encountered a man smoking a pipe, wearing a trilby hat and either a trench coat or overcoat. The strange figure vanished in a large empty field of flat prairie grass.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Matron</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Cardiff Royal Infirmary Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Several ghost stories have been reported from this hospital. A plumber observed a matron in old fashioned clothing and bonnet who vanished without warning. A local legend says that if the woman in grey offers you a drink, do not take it, otherwise you shall die within a week.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Castell Coch<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A 'new' building on the site of thirteenth century ruins, the White Lady once haunted the older area, and now has set up shop in the replacement castle. The woman is said to have died of a broken heart after her son drowned in a local pond. There is also reported to be a buried hoard of treasure here, zealously protected by either three eagles or an entity who confuses any potential gold thieves, while an ethereal cavalier walks the castle grounds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wales11600.jpg' class="w3-card" title='Cardiff Cathedral, Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cardiff Cathedral, Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The female entity said to haunt this area is thought to be the same woman who is looking for her lost son close to the river.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Cefn Mably House (mostly destroyed by fire in 1994)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1910<br>
              <span class="w3-border-bottom">Further Comments:</span> A bedroom in this old house was reputedly haunted by a beautiful woman in white who would drift through the doorway as the moon shone through the window.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cyhyraeth</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Church, St Mellons<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A herald of death, the cries of the Cyhyraeth would be heard moving towards the church, the route of the entity denoting the direction the body would be taken.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jumping</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Commercial unit (name withheld), Old St Mellons<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1993<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of someone continuously jumping up and down could be heard coming from the first floor area of this site, although it was empty at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pulling of Clothes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Cottage between Parish Church and Baptist Chapel, Lisvane<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1882<br>
              <span class="w3-border-bottom">Further Comments:</span> Occupants in this home had bed clothes pulled off during the night by an unseen agency. Chairs would be heard scraping across floors, and bottles and crockery smashed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Council house, Rumney<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1962<br>
              <span class="w3-border-bottom">Further Comments:</span> A family of seven moved from the site after the haunting, a white girl who would peer around the corner of doorways, became too much for them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gwrach y Rhibyn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Cow and Snuffers Inn, Llandaff (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 November 1877<br>
              <span class="w3-border-bottom">Further Comments:</span> A guest staying close to the Cow and Snuffers watched a screeching old woman with wings, red hair, and pale face enter the inn. The guest believed the entity to be a Gwrach y Rhibyn, a death omen. The following day, the guest discovered the innkeeper had died during the night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Cowbridge Road area, Canton<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This area of Cardiff is said to be haunted by the ghost of a young boy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Circle of White Light</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Cowbridge Road, Ely<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa February / March 2023, early afternoon<br>
              <span class="w3-border-bottom">Further Comments:</span> Travelling along a busy road, one witness watched a circle of white light slowly cross the rooftops of four houses, as if a huge torch had been shone on the surfaces, before disappearing. Nothing could be seen in the sky, and there were no vehicles on the road that could have caused the light.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bully Dean</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Herbert House (demolished 1958) and Greyfriars<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1754<br>
              <span class="w3-border-bottom">Further Comments:</span> This house and surrounding area was haunted by a terrifying figure known as the Bully Dean, though little more is known.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nurse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Llandough Hospital<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> Manifestation of the living unknown, nurse in uniform December 2002<br>
              <span class="w3-border-bottom">Further Comments:</span> A member of the nursing staff told her friend that she had seen her own dead body lying on a bed - a week later she died of typhoid fever, her body placed in the location revealed in the vision. It is not clear whether the nurse who experienced this continued to haunt the hospital, but in 2002 a pregnant woman awoke in the hospital and found a nurse at the end of her bed. The nurse told her that everything would be okay and walked out. The woman then realised the nurse was wearing an old fashioned uniform with a Victorian nursing hat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wales5806.jpg' class="w3-card" title='National Museum Cardiff, Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> National Museum Cardiff, Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Dunbar Smith</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - National Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Smith unknown, Lord Ninian on election night (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Dunbar Smith designed the museum, and after he died the building became home to his ashes. The ashes were moved from their location to make way for a new public toilet during the 1960s, after which Dunbar was said to have returned, complaining about his post-mortem treatment. Another ghost, that of Member of Parliament Lieutenant-Colonel Lord Ninian Edward Crichton-Stuart, killed in action during the First World War, is said to become particularly active on election night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wales11601.jpg' class="w3-card" title='New Theatre, Cardiff.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> New Theatre, Cardiff.</small></span>                      <p><h4><span class="w3-border-bottom">Older Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - New Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An older woman is reputed to haunt this theatre. Her ghost manifests in one of the boxes where she is said to have either dropped dead in or fallen from (depending on the version of the story you are told). Staff and visitors have also reported cold spots and the sensation of being watched.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stamping Feet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff - Private residence, King's Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1905 - June 1906<br>
              <span class="w3-border-bottom">Further Comments:</span> The family living here would be woken around midnight by the sound of stamping feet in the hallway and on the staircase. One of the occupants refused to believe the sounds had a paranormal origin, although was not able to find the source of the noise.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 69</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=69"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=69"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/wales.html">Return to Welsh Preserved Counties</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Scotland</h5>
   <a href="/regions/scotland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/secondlevel/Lothian.jpg"                   alt="View Scotland records" style="width:100%" title="View Scotland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
