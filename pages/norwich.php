
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Norwich Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

 <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Norwich</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Norwich Ghosts, Folklore and Forteana</h3>
    
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor215.jpg' class="w3-card" title='19 Magdalen Street, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> 19 Magdalen Street, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">Victorian Female Murder Victim</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - 19 Magdalen Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> When this location was home to a charity shop, staff were subjected to the sounds of footsteps in empty parts of the building, cold spots, and drafts. A shadowy figure was also reported to lurk on the staircase. The psychical research team BSIG spent a night on the site; they documented EVP and witnessed a previously locked door swing open. It was speculated that the haunting was caused by a Victorian woman murdered near the site. More recently, the EDP's Weird Norfolk team reported an exorcism had been performed in the shop in the 1980 and that, perhaps other than one recent fleeting encounter, the haunting has all but ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sam the Lord Sheffield</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Adam and Eve public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1549 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Dying at the inn in 1549, Lord Sheffield (now nicknamed Sam) now makes the odd appearance in the building. Sometimes staff report the feeling of someone running fingers through their hair, even though no one else is nearby. In 2006 one man reported seeing a ghostly hand holding a head while sitting in the pub's car park.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6401.jpg' class="w3-card" title='Augustine Steward House, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Augustine Steward House, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">Lady in Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Augustine Steward House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> When the plague killed the occupants of this building, the house was boarded up for several weeks. Unfortunately, a girl in the house had survived; unable to escape, she starved to death. Her grey ghost haunts both the Augustine Steward House and the Tombland alleyway.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Child with Hoop and Stick</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Business premises, Thorpe Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2019<br>
              <span class="w3-border-bottom">Further Comments:</span> One person in the building spotted a child running across the landing with hoop and a stick. Other members of staff have encountered a Victorian lady walking down the staircase and disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor978.jpg' class="w3-card" title='Cathedral Close, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cathedral Close, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">Poltergeist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Cathedral Close<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Items began to move on their own accord after builders broke up an old stone staircase in one building along here. The activity died down after church officials were brought in to investigate.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Coachmaker's Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom figure (who looks like Dick Turpin) stands at the end of the bar and has been mistaken for a paying customer. A ghostly woman in black reportedly walks down the staircase, while bottles and glasses fall from the shelves without just cause.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mr Issacs, a Former Rabbi</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Cupit (or Curat) House (town centre)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Third week of November, 1889 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Since a building extension, the ghost of Mr Issacs has been heard walking in the upper part of the house. Evidence discovered indicated that the Rabbi may have killed his wife, and as punishment, his spirit returns.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6403.jpg' class="w3-card" title='Elm Hill, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Elm Hill, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">Father Ignatius</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Elm Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This holy man cursed several people who died soon after, so the locals forced him to flee the city. His shade walks along this hill, big black bible in hand, still cursing the unwary.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Voices from the Past</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Farnworths (was at 35 London Street)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> Local press reported at the time that the manager would hear men's voices in the building although no other people were present. On another occasion wet drag marks were seen on the floor, with no rational explanation for their presence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6397.jpg' class="w3-card" title='Former  Lamb Inn, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Former  Lamb Inn, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">John Aggas</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Henry's (formally the Lamb Inn)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1999<br>
              <span class="w3-border-bottom">Further Comments:</span> Last seen sitting on a chair by a window, this friendly phantom is thought to be John Aggas, a former landlord murdered by his brother in law. The ethereal John would make himself known to children staying at the inn, by perching on the foot of their bed and recounting ghost stories!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Turquoise Jacket</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Lane between St Stephens Street and Malthouse Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 January 2022, 13:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Taking a shortcut through this narrow lane, a witness spotted an older woman wearing a 1970s-style turquoise jacket and brown skirt a short distance away. The witness looked away for a second, during which time the older woman vanished - there was nowhere the woman could have gone in such a brief period of time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor63.jpg' class="w3-card" title='Maddermarket Theatre, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Maddermarket Theatre, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">Black Robed Priest Celebrating Mass</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Maddermarket Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a church, the theatre is now known for the infrequent sightings of a spectral priest, though little has been seen or heard from the spirit since the 1920's.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6399.jpg' class="w3-card" title='Maid&#039;s Head Hotel, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Maid&#039;s Head Hotel, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">Lady in Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Maid's Head Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This old woman dressed in grey clothing walks around the hotel, the smell of lavender following her. She is believed to be a former maid at the building. A former mayor of the city walks around the courtyard shaking his head.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor64b.jpg' class="w3-card" title='Norwich Castle, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Norwich Castle, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">Old Lady in a Black Dress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Norwich Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1820 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The earliest ghost sighting here dates to 1820 when several prisoners were scared 'half to death' by something indescribable. In 1844 the Devil, dancing away on the walls, was seen by two witnesses. More recently, staff at the castle museum have reported watching an old lady float around the grounds. There are rumours that the hill on which the castle stands conceals the grave of an old king, together with his hoard of silver and gold.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Pockthorpe area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A team of headless horses driven by a headless coachman would be seen flying over the rooftops in this area of the city.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dark Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Private residence, Silver Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2007-2008<br>
              <span class="w3-border-bottom">Further Comments:</span> A former occupier of this house claimed lights would flick on and off, a dark figure could be seen, and covers forcefully removed off the bed. Neighbours reported hearing sounds from the building after the occupier left.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6121.jpg' class="w3-card" title='Norwich Railway Station, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Norwich Railway Station, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">Large Rat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Railway Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This station is home to a frightful entity which takes the form of a rat-like creature with large teeth and obnoxious breath.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6402.jpg' class="w3-card" title='Samson and Hercules House, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Samson and Hercules House, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">Lady in Grey Again</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Samson and Hercules House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost which haunts the nearby Augustine Steward House has also been seen here, her grey form drifting around, the legs fading away just below the knee.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with Dark Hair</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - St John Ambulance Training Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2019<br>
              <span class="w3-border-bottom">Further Comments:</span> The site is home to a woman with dark black hair wearing a red dress. Her legs are said to be below floor level, suggesting that the current building is higher than the original land.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Strangers Club, Elm Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Heard walking on the wooden floorboards upstairs, the owner of these footfalls can never be found when the upper story is investigated.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Footfalls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Tap & Spile, Oak Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Footfalls and poltergeist activity are blamed on the ghost of a former landlord executed for the murder of a prostitute.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor969.jpg' class="w3-card" title='The Bridge House pub, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Bridge House pub, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - The Bridge House public house (currently the Lollard's Pit), Riverside Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Constructed over sixteenth century holding cells, the screams are assumed to be those of terrified prisoners.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Tombland<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 April 2015, circa 23:15h<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple walking their dog at night encountered a woman wearing layers of old fashioned grey dresses with a torn white apron. The woman wore an eye mask, which the witnesses believed prevented her from seeing where she was walking.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6400.jpg' class="w3-card" title='Wild Man pub, Norwich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Wild Man pub, Norwich.</small></span>                      <p><h4><span class="w3-border-bottom">Little Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Norwich - Wild Man (aka Wildman) public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A former staff member claimed to have seen a young boy run across the bar area and disappear up the staircase, though no trace of him could be found. He is thought to have died in the cellar in a fire, where heavy barrels are now heard moving across the floor late at night.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 24 of 24</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
   <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
