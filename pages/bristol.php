
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Bristol Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Bristol</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Bristol Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - 14 Bellevue<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> A family living here reported the strange sounds of scratching at the window. The kitchen table would also creak and groan without reason.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - 9 St James Parade<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> The great ghost hunter Elliott O'Donnell was called on to investigate this case. The Brittons family living here reported hearing strange taps and footsteps, while Mrs Britton fainted after seeing the face of an old lady moving towards the cot of her child. The haunting is said to have stopped when the family were re-housed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - A38 near Barrow Gurney Reservoir<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A certain part of the road is said to be covered in skid marks where various cars have had to brake to avoid hitting a phantom woman dressed in a white coat. The figure always vanishes immediately afterwards.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jogger?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - A38, Bedminster Down<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> December 2012, 06:50h<br>
              <span class="w3-border-bottom">Further Comments:</span> Whilst driving to work this witness spotted a smiling woman in a red top and black jogging bottoms running towards them in the middle of the road. After they passed each other the woman could not be seen in the rear view mirrors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - All Saint's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Last seen 1948?<br>
              <span class="w3-border-bottom">Further Comments:</span> Many believe that this ghostly monk in black has returned to watch over a pile of treasure which was hidden during his lifetime. The figure has been spotted several times over the last two hundred years, most recently around Christmas 1948 when he was seen walking down the church aisle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Duchess</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Area around Duchess Lake (now filled in) and Dower House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> December (reoccurring) - exact date not known<br>
              <span class="w3-border-bottom">Further Comments:</span> Duchess Elizabeth Somerset fell from her horse and broke her neck - her ghost is said to ride around the area. A local legend says that if you wait near her monument, three knocks will signify her presence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pregnant Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Arno's Manor Hotel, Room 160 and other areas<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1977<br>
              <span class="w3-border-bottom">Further Comments:</span> This nun took her own life, and for varying reasons, her sisters bricked her body up in an alcove. The haunting is said to have started during the Second World War, when workmen came across her skeleton and to avoid any delay to their work, just hid the bones elsewhere. Since then, a figure in brown has periodically been reported, and some people have heard a soft female voice calling them by name.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Arnos Court - graveyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman in black has been seen here, crying over the death of her husband taken during the First World War. Another woman has been observed staggering around screaming; it is believed she was accidentally buried alive, and the mistake not realised until the body was exhumed many years later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/som5546.jpg' class="w3-card" title='Ashton Court, Bristol.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ashton Court, Bristol.</small></span>                      <p><h4><span class="w3-border-bottom">Decapitated Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Ashton Court Estate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Clear moonlit nights (horseman)<br>
              <span class="w3-border-bottom">Further Comments:</span> Ashton Court is best known for its ghostly headless horseman, though there are also reports of grey ladies and a phantom hound.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Goram and Vincent</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Avon Gorge<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> To prove themselves worthy of the lady Avona's love, the giant brothers Goram and Vincent raced to dig a ditch to drain a lake. Goram started work but fell asleep after digging the Hazel Brook Gorge, while Vincent dug the Avon Gorge that drained the unwanted lake, thus winning Avona's heart.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Badocks Field, Westbury on Trym<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2015, 19:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> One witness watched a woman dressed in Victorian clothing picking blackberries. She walked into the bramble bush and just disappeared. The witness was later told that the site was haunted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Radio Breakdown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - BBC Bristol, Whiteladies Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A poltergeist located here is said to be responsible for the high number of electrical breakdowns which occur.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Black Castle public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This pub is reputedly haunted by one or more headless nuns. Mild poltergeist phenomenon has also been reported in the building, as has the sighting of a ghostly little girl.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Disappearing Islands</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Bristol Channel<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The islands which appear and disappear without warning are reported to be home to a race of immortals. The area has also been home to sea serpent reports.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Isambard Kingdom Brunel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - British Empire and Commonwealth Museum (no longer open)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Brunel once had his office in the area which was dedicated to temporary exhibitions. The smell of his tobacco smoke and the steady pacing footsteps has unnerved several members of staff.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure is said to move between the cathedral and the nearby library.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Clack Mill (demolished in 1937)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1937<br>
              <span class="w3-border-bottom">Further Comments:</span> This dark clothed phantom appeared to a young boy living at the mill - the ghost left through a wall after attempting to have the lad follow it. The mill was also haunted by the shade of a man who hanged himself behind the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-6.jpg' class="w3-card" title='Old postcard of the Clifton Suspension Bridge.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Old postcard of the Clifton Suspension Bridge.</small></span>                      <p><h4><span class="w3-border-bottom">Jumpers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Clifton Suspension Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The shades seen drifting around on the bridge are thought to belong to those who have jumped off, taking their own lives.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Executed Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - College Green<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Condemned to death and promptly executed for deserting an England/Scotland conflict, the trooper now drifts around the green in the early morning hours.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Coombe Dingle bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> One lady claimed to have seen the ghost of a Roman soldier several times in this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Morphing Shuck</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Dead end lane in Hallen Bristol, next to the M49 bridge<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1908 and December 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> The large black dog seen here in 1908 was seen to climb out from a hedge, before transforming into a donkey and standing on its hind legs. A very similar creature was seen in 2004, as it passed through a car, shaking the occupant.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hudd</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Deep Pit (colliery), Kingswood<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1880<br>
              <span class="w3-border-bottom">Further Comments:</span> At an inquest held after a miner named Hudd died in the pit, one witness stated that he had seen Hudd's ghost moments after his death, as it headed up an incline towards the bottom of the shaft.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knight Templar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Fire Brigade Headquarters, Temple Back<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1975<br>
              <span class="w3-border-bottom">Further Comments:</span> This apparition appeared nine times during 1975. While one witness thought it looked like a man wearing waterproof clothing, others thought that the ghost's dress looked more medieval and speculated it may have been a Knight Templar, the organisation of which once owned lands around the city.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Former council flat, Horsefair<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen only the once (but heard often), this phantom woman in brown was said to have an 'evil' face. Ghostly footsteps and strange tapping plagued the family here, with much of the activity centred around their baby's cot. The family were moved out after sound recordings picked up a constant metallic vibrating sound, which the council believed to be a health hazard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bearded Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol - Former Vicarage next to All Saint's Church, Corn Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1840s<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure dressed in ancient clothing was seen stalking the building at night, scaring the occupants, to such an extend on jumped out of a window to avoid the ghost. It is unclear whether this is the same entity as the black monk that also haunts the area.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 80</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=80"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=80"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
 <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>


</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
