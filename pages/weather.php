

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of weather-related ghosts and strangeness from the Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > Weather Dependent</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Weather Dependent Events - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Long Compton (Warwickshire) - Long Compton Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather dependent: After a storm<br>
              <span class="w3-border-bottom">Further Comments:</span> A coach pulled by six headless horses is said to travel up the hill. There are also vague reports of a shuck-like dog that runs in the same area. Another ghost, that of a scary old woman with matted black hair, was also reported here in 1973.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Storm Warning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Zennor (Cornwall) - Tinner's Arms<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Before stormy weather<br>
              <span class="w3-border-bottom">Further Comments:</span> This poltergeist could be little more than faulty wiring, as it only becomes active just before the area is hit by electrical storms.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stolen Bell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St David's (Dyfed) - Cathedral and Whitesand Bay<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Before stormy weather<br>
              <span class="w3-border-bottom">Further Comments:</span> The largest bell was stolen from the building by imps disguised as men and transported out to sea where it was dropped into the Whitesand Bay. The bell still rings just before a storm, as a warning to nearby fishermen to return home. Another version of the tale says the bell was rested on several rocks during the heist, and it is the rocks that now ring before the weather changes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swallowed Sounds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdyfi (Gwynedd) - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Calm nights<br>
              <span class="w3-border-bottom">Further Comments:</span> The bells of a church which was swallowed by the sea can sometimes be heard when the weather is right.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/som5546.jpg' class="w3-card" title='Ashton Court, Bristol.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ashton Court, Bristol.</small></span>                      <p><h4><span class="w3-border-bottom">Decapitated Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol (Somerset) - Ashton Court Estate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Clear moonlit nights (horseman)<br>
              <span class="w3-border-bottom">Further Comments:</span> Ashton Court is best known for its ghostly headless horseman, though there are also reports of grey ladies and a phantom hound.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ruthin (Clwyd) - Ruthin Castle (hotel)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Clear nights<br>
              <span class="w3-border-bottom">Further Comments:</span> This lady was married to a fifteenth century steward at the castle - she decapitated her husband when she discovered him with a younger woman. In turn, she was beheaded for the act of revenge, and now the spook makes fleeting appearances at the former fortress. A man in armour is also occasionally reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Aggie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dunbar (Lothian) - Dunbar Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Clear nights<br>
              <span class="w3-border-bottom">Further Comments:</span> The flitting ghost of the Patrick, ninth Earl of Dunbar, has been spotted around the ruins of Dunbar castle. Bagpipes have also been heard coming from the empty ramparts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Eyed Sailor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Faversham (Kent) - Shipwrights Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Cold winter nights<br>
              <span class="w3-border-bottom">Further Comments:</span> Having died in a shipping accident, the phantom sailor now appears on icy nights. His favourite trick is to open the front door and walk in, only to disappear moments later, leaving only a strange, disconcerting odour.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Witch</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Enfield (Greater London) - Hadley Road, Enfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Cold, misty nights<br>
              <span class="w3-border-bottom">Further Comments:</span> An old woman who once lived along this stretch of road was executed in 1622 for being a witch - on cold misty nights, her ghostly form now returns.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thirsty Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Llanrhidian (West Glamorgan) - Arthur's Stone (cromlech)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Dark stormy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> The stones here are said to animate when the weather is right and there are no human witnesses, to head off and drink from the nearby waters. A ghostly knight in glowing armour also haunts the area; drifting out from under the rocks and heading north on nights of a full moon.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Boots Made for Walking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Besford (Hereford & Worcester) - Church Farm, local field named Dog Kennel Piece<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Dark, moonless nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A kennelman was sent by his master to discover what was upsetting a pack of hounds once kept here. The kennelman never returned and in the morning his body was found torn apart by the dogs with only his boots untouched. A ghostly form of the footwear now stomps around the field at night, while the sound of howling hounds drift in the wind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hand</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dovenby (Cumbria) - Wooded area on road between village and Tallentire<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Dark, stormy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A disembodied hand is reported to reach out from the trees and push passing cyclists from their bikes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Squire Cabell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buckfastleigh (Devon) - Church, the Cabell Tomb<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Dark, stormy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> When the weather is right, the squire emerges from his tomb and, together with a pack of hounds, sets off across the nearby moor. Many believe that it is this legend that Hound of the Baskervilles is based upon. A local legend says if one runs around the tomb thirteen times before sticking a finger in the keyhole, the ghost can be felt licking the tip.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Pebmarsh-hill.jpg' class="w3-card" title='A phantom jaywalker manifests during storms.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom jaywalker manifests during storms.</small></span>                      <p><h4><span class="w3-border-bottom">Phantom Jaywalker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Pebmarsh (Essex) - Hanner Monk's Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Dark, stormy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> This fleeting form is seen dashing across the road when the weather is right. It is thought to be the ghost of Hanner Monk, though it is not clear if the man died on the road or elsewhere.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/The-Coal-Shed.jpg' class="w3-card" title='Brighton&#039;s The Coal Shed restaurant.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Brighton&#039;s The Coal Shed restaurant.</small></span>                      <p><h4><span class="w3-border-bottom">Jemmy Botting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton (Sussex) - The Coal Shed restaurant, and Boyce's Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather dependent: Dark, windy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> Botting was a hangman who claimed to have executed 175 during his career. He died in October 1837 after falling out of his wheelchair - he was so despised locally, no one came to help him up. His ghost is said to haunt the Coal Shed, while the sound of his wheelchair is heard passing along Boyce's Street when the weather is right.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Biplane</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Weybridge (Surrey) - Skies over the town<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: During thunderstorms<br>
              <span class="w3-border-bottom">Further Comments:</span> The plane was blown out of the sky in during a storm in the mid-1930s - now when the weather conditions are bad, the aircraft can be heard struggling in the wind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Arguing Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hardraw Scar (Yorkshire) - Waterfall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: During thunderstorms<br>
              <span class="w3-border-bottom">Further Comments:</span> The cries and shouts of three men can be heard here during thunderstorms - it was in such weather that the former friends first fought during their lives, which resulted in the death of one of them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stanhope</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broughton Astley (Leicestershire) - A5<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Foggy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A member of the Stanhope family is reputed to haunt this road when the fog rolls in.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Treasure Hunter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Higher Combe (Somerset) - The Caratacus Stone, just outside the village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Foggy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> The jutting rock is said to have treasure concealed beneath it, though it is now protected by the ghost of a man who tried to remove the stone to find the hidden wealth - his body was found crushed under the stone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">David Bowen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberhowy (Powys) - Road to village from Cardigan Bay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Foggy nights (last reported 1956?)<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of David on his push bike is said to guide people along the road when the weather is densely foggy and dangerous. Bowen lost his son in an automobile accident on such a night, and now tries to ensure such a tragedy never happens again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Troops</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Marston Moor (Yorkshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather dependent: Foggy nights. Last seen November 1932<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen crossing the road in front of a car, these figures looked ragged and battle weary, dressed in clothing that matched that of the 1644 Battle of Marston Moor. Local reports say the battle is sometimes replayed in full, normally on foggy nights. On one occasion prior to the twentieth century, two fiery pillars were seen in the sky at midday, accompanied by the fighting armies. After some time, the northern army defeated the southern forces, after which troops and pillars vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Steam Locomotive</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Elsham (Lincolnshire) - Ancholme Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Foggy weather<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to reappear in the same spot since an accident in the 1920s which killed four people, this large steam locomotive gently glows in its foggy environment.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nance</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheriff Hutton (Yorkshire) - Area near the A64 York/London road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Foggy weather<br>
              <span class="w3-border-bottom">Further Comments:</span> The young lady Nance has been seen standing on the side of the road holding her young baby - she died in the area after being deserted and left penniless by the child's father. It is thought she helps those lost and in peril.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Long Coat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buxhall (Suffolk) - Rattlesden Road area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: frosty nights or when snow is on the ground in February<br>
              <span class="w3-border-bottom">Further Comments:</span> The silhouette of a man in a long coat has been spotted walking around this road, carrying a lantern at waist height. The same witness reports his dog behaves strangely when taken past the woods close to Cockerells Hall Drive.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/war1641knight_a.jpg' class="w3-card" title='The Rollright Stones, England.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Rollright Stones, England.</small></span>                      <p><h4><span class="w3-border-bottom">Dancing Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Long Compton (actually in Warwickshire) (Oxfordshire) - The Rollright Stones - the King Stone and the Whispering Knights<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Mist. Grey figure seen pre-1937<br>
              <span class="w3-border-bottom">Further Comments:</span> The King Stone was once a place where fairies danced around. Legend says that the monolith was once a King who was petrified by a witch (though which ruler is unclear). There are so many other smaller stones that it is said to be impossible to count them all. Another set of nearby standing stones known as the Whispering Knights are thought to be the witch's aids, who conspired against the king but were turned to stone anyway. Finally, a grey female figure would appear on misty days, twisting and moving strangely within the stone circle.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 80</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=80"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=80"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
