

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Hereford and Worcester Ghosts and Folklore from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/westmidlands.html">West Midlands</a> > Hereford & Worcester</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Hereford & Worcester Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abberley - Rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1947 (woman), 1950 (footsteps)<br>
              <span class="w3-border-bottom">Further Comments:</span> Local folklore claimed that a misty lady would manifest over a coffin-shaped box containing her child. Two clergymen opened the box and found nothing but dust inside. The Rector reported hearing footsteps in empty parts of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roger de Clifford</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aconbury - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Roger became so troublesome that it was trapped within a bottle and buried under his monument. Despite this, he still returns, and to be touched by the hooded shade is an indication the witness shall die within the year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Organ Plays</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Avenbury - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1919<br>
              <span class="w3-border-bottom">Further Comments:</span> Several times, late at night, the church organ was heard to burst into tune, stopping as investigators reach the churchyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Return from the Red Sea</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beoley - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghost that haunted this building was exorcised to the Red Sea for fifty years. When this time expired, the ghost came back, extremely upset about its forced vacation.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Besford - Besford Court<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This location is haunted by either a nun or a lady dressed in grey. Or maybe both? An upstairs bedroom is said to be the most popular place for the ghost to materialise.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Boots Made for Walking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Besford - Church Farm, local field named Dog Kennel Piece<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Dark, moonless nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A kennelman was sent by his master to discover what was upsetting a pack of hounds once kept here. The kennelman never returned and in the morning his body was found torn apart by the dogs with only his boots untouched. A ghostly form of the footwear now stomps around the field at night, while the sound of howling hounds drift in the wind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Shadows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Besford - Dower House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Intensely bright phantom lights were seen outside the building, and a few months later, two witnesses watched a jet black shadow cross a wall, though there was no light source present that could have cast it. A strange face would sometimes look through the window at those inside, before slowly fading away, while a misty woman was once seen inside the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Steam</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bewdley - Severn Valley Railway,  Northwood Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom locomotive is said to be invisible, though it leaves a trail of hazy steam as it passes. The Parasearch team investigated the site in 2008 but did not reach any conclusions.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bewdley - Talbot Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1989<br>
              <span class="w3-border-bottom">Further Comments:</span> A photograph taken in the bar after closing hours revealed a blurred unknown man in black in the background. It was also reported that furniture had moved itself and the jukebox would switch itself on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brampton Bryan - Park<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says that on the day Cromwell died, the park was laid to waste as the Devil passed though it to collect the soul due to him. Now, once a year, Old Nick rides out and returns to the park to ravage it once more.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Demon Bird</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bransil - Bransil Castle<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A large black bird was once said to protect a pile of hidden gold within the castle grounds. The hoard could not be moved unless the bones of Lord Beauchamp, former owner of the gold, were in possession of the treasure hunter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bedroom Shuck</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bredon - Cottage, exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> During Second World War<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing in a bedroom, this dog had glowing red eyes and warm breath - it disappeared as it approached the bedroom door to leave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bretforton - Church, and surrounding area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom funeral procession arrives at the church, though for whom it represents is a mystery. Fields on either side of the church are said to be haunted by a decapitated woman, carrying her head under arm.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lola Taplin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bretforton - The Fleece Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1977 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Once the landlady here, the short tempered spirit of Lola now haunts the bar, occasionally throwing food and other objects around the room. One witness reported a feeling of sadness and felt herself being poked in the side. One local legend says that Lola Taplin has taken the form of an owl and watches over the pub.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Spot Loggins</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bretforton - Well on Bretforton House Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Loggins was a cattle drover who drowned in a well located on this farm and is now said to haunt the area around the well. One myth says that if you are blindfolded and run around the well three times, you will lose anything you are carrying. A brand of locally produced ice cream is named after the ghost.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bringsty - Brockhampton Estate<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s - 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Rooms were left empty as various strange events drove occupants from them, including the lifting of bedcovers and the sounds of footsteps walking around. An exorcism failed to dispel the entity. A phantom Victorian woman is also said to walk the manor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/2205serpent001.jpg' class="w3-card" title='An Image from An Essay Towards a Natural History of Serpents.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An Image from An Essay Towards a Natural History of Serpents.</small></span>                      <p><h4><span class="w3-border-bottom">George and the Dragon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brinsop - Lower Stanks and Duck's Pool Meadow near the church<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The village of Brinsop claims to be the location where St George had his famous battle with a dragon. The creature lived in Duck's Pool Meadow but was finally slain at Lower Stanks meadow. An old stone carving in the church depicts the final battle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tolling Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broadway - Middle Hill wood<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Second World War<br>
              <span class="w3-border-bottom">Further Comments:</span> A set of church bells were hidden in the forest during the Reformation and forgotten about. They are said to ring at night, and were heard during the Second World War, a time when bell ringing was illegal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ephraim Rolfe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broadway - Road towards Evesham<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A village simpleton, Rolfe was popular with the locals; he was mistakenly killed by a landlord who mistook him for a poacher. His thin and bony form can now be seen in the fields surrounding the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bromsgrove - Avoncroft Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 March 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> While at a re-enactment event, one participant spotted a boy standing by a fireplace in the Merchant's House. The boy was no older than thirteen, with shoulder length wavy hair, a red tunic and a brown leather belt around the waist. As the participant approached the boy, the young lad vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/pig002.jpg' class="w3-card" title='An old woodcut of a boar.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a boar.</small></span>                      <p><h4><span class="w3-border-bottom">Abhorrent Boar</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bromsgrove - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The hero Sir Ryalas battled long and hard before killing a monstrous boar in this region. Upon death the creature transformed into a human, recognised as the son of a local woman. Sir Ryalas tracked down the woman and also killed her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mary Yates</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bromsgrove - Harvington Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> A former owner of the building in the seventeenth century, Mary has been named as the ghostly elderly woman seen by several people on the site. She is also said to appear at a crossroads outside the park, the site where legend says she was hanged for witchcraft. Back in the hall, other people have reported a phantom old man with a pipe and an Edwardian woman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mistress Hicks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bromsgrove - Road and fields surrounding Harvington Hall (particularly the A448/A450 area)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This woman was executed for being a witch - she was blamed for raising storms and making local women and children vomit pins. Some say she still walks the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Steering Wheel Fight</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bromyard - A465 near the town<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> Many of the deaths on the road here are blamed on an entity which grabs the steering wheel and prevents people from turning at corners - the ghost is said to be that of a woman who once died while fighting someone as she sat in the driving seat of a fast moving car.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Where is Anne?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bromyard - Falcon Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1964<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a young gentleman that walks around the second floor of the building has been heard asking the question 'Where is Anne?'.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 141</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=141"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=141"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/westmidlands.html">Return to West Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="View West Midlands records" style="width:100%" title="View West Midlands  records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>East Midlands</h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
