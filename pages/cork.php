

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="County Cork Folklore, Ghosts and Strange Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Cork</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Cork Ghosts, Folklore and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Allihies-Children-of-Lir.jpg' class="w3-card" title='Children of Lir  - two children being turned into swans.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Children of Lir  - two children being turned into swans.</small></span>                      <p><h4><span class="w3-border-bottom">The Swans</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allihies - Children of Lir site<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Site still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Eva, daughter of King Bov, turned her stepchildren into swans, believing them to detract her husband's attention from herself. She instantly regretted her action but was unable to undo the damage. Nine hundred years passed until Christianity reached the shores of Ireland. The power of Christ turned the children back into their former selves, but incredibly aged. They died soon after the transformation and were buried on this site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mind the Edge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allihies - Cliff near old copper mine, Beara Peninsula<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-1850s-1930s?<br>
              <span class="w3-border-bottom">Further Comments:</span> After working late one moonless night, a local man returning from the copper mine took a shortcut across a hill. He became lost, and in his confusion fell over the edge of a cliff and died. For many years following the accident, travellers crossing the hill would pause to throw a stone over the edge of the cliff, fearing that failing to carry out the ritual would result in them experiencing the same fatal accident.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Weaver</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annagh - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Laden with thread, this phantom was believed to be a tailor who had immorally acquired the thin yarn during his lifetime and was repenting his sins by carrying it upon his back (special thanks to the Churchtown Village Renewal Trust, from their 'The Annals of Churchtown').</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annagh - Channeleen<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A cyclist was chased by a strange white figure who refused to speak. The cyclist finally stopped pedalling and threw his lamp at the ghost, who immediately vanished. The Channeleen was also home to a phantom woman who wore black (special thanks to the Churchtown Village Renewal Trust, from their 'The Annals of Churchtown').</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annagh - Unstated property<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1927<br>
              <span class="w3-border-bottom">Further Comments:</span> While camping close to this property, one traveller awoke to see a white female form standing at a gate. He woke one of his friends who also observed the ghostly figure. The following day the travellers moved on and never returned (special thanks to the Churchtown Village Renewal Trust, from their 'The Annals of Churchtown').</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Leprechaun's Shoe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardrah - Field in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A small shoe said to belong to a leprechaun was found near the village by a cow herder. Unfortunately, the man forgot to take the shoe home, and when he went to retrieve it the following day, the shoe had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Brian</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballincollig - Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1930s<br>
              <span class="w3-border-bottom">Further Comments:</span> It was said that a dark room in the castle was home to a ghost named Brian, who would appear on horseback to anyone who ventured into the structure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballincollig - Oriel Lodge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1930s<br>
              <span class="w3-border-bottom">Further Comments:</span> A husband and wife encountered a white female phantom standing by the kitchen door, after which they had their bed covers pulled from them by an unseen hand on three consecutive nights.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Killer Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballingeary - Road towards Droumanallig, 300 metres west of Ballingeary<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A female phantom began to kill those who failed to answer her riddle 'A candle and a candlestick there is, and you fit a couplet to that'. Local man Michael Creedon banished her after replying 'You ought to have got to heaven in time rather than being a ghost as you are'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rocking Virgin Mary</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballinspittle - Roadside grotto<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 22 July 1985 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Cathy O'Mahony and her mother observed the statue of the Virgin Mary rocking on its heels. The following night they returned with friends who observed the same event. Since this time, tens of thousands of people have visited the shrine in the hope of seeing something.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moddra-na-Craeuv</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyandrew - Glen of Croke, which stretched from Saffron Hill to Pinegrove<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom woman who once appeared here was said to be a Creagh girl who died shortly after her lover was killed by her father. The Moddra-na-Craeuv, also known as the dog of the Creaghs, also haunts the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Island</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballycotton - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 7 July 1878<br>
              <span class="w3-border-bottom">Further Comments:</span> Local fishermen awoke to see a strange new island out to sea, covered with trees and rocks. As dozens climbed into their boats and set off towards it, the island faded from view.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Great Fish</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballycotton - Somewhere between Ballycotton and Old Head of Kinsale<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa May 1906<br>
              <span class="w3-border-bottom">Further Comments:</span> A fishing vessel reported being attacked by a dark fish the size of a whale. The fish rammed the boat several times, only ceasing the assault when the captain ordered the engines to be killed; after half an hour, the fish disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John the Baptist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballycotton - St. John's well<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Well still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A rock near the well was said to be the head of John the Baptist, which slowly turned to stone and was weathered by the elements over time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bagpipers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyedmond - Fort Field<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The fort in this field was home to four fairy bagpipers dressed in red.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballygarvan - Small bridge over a stream<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A small bridge over a stream near Ballygarvan was home to a white woman. After appearing to one man who fell critically ill the following day, a priest went to the site and banished her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Skull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballynoe - A Queen Anne mansion<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> December 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> A skull discovered in this mansion was quickly linked to the 'ghostly' sounds occasionally heard in the area. Police said the skull was smooth, which indicated regular handling, and was likely to have belonged to a former doctor who lived in the building in the 1800s.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mary</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballytrasna - Unnamed farm<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> August 1991<br>
              <span class="w3-border-bottom">Further Comments:</span> After having a vision of the Virgin Mary, a farmer converted part of his farm into a shrine which he named House of Mercy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Former Students</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyvourney - Ballyvourney De Salle College<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> After numerous stakeouts, CPI (Cork Paranormal Investigators) are convinced that this disused college is home to several entities, including a priest, a young boy and four teachers. The sound of laughing children and the ringing of a bell are just a few of the incidents reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gobhnet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyvourney - St. Gobnait's Well (still present) and area surrounding<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Fifth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Gobhnet (or Gobnait) was a nun who built an oratory on the site where she stumbled upon nine white deer. She kept bees that doubled as her protectors, chasing away thieves. The well on the site is still a site of pilgrimage, who tend to visit on 11 February.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lynx</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyvourney - Unnamed Road<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1981<br>
              <span class="w3-border-bottom">Further Comments:</span> A large cat seen as it stepped in front of a car was later identified as a lynx.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baltimore - Fastnet Rock<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Rock still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Ireland's most southerly point, Fastnet Rock was thrown into position by a giant from Mount Gabriel (Schull).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Money Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bandon - Monerone area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A rock in the centre of the road was known locally as the Money Stone and according to legend was used to mark the location of buried treasure. People had tried to dig up the hoard but were always scared off by a supernatural presence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bandon-Roads-in-the-area.jpg' class="w3-card" title='A ghostly woman riding a horse.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly woman riding a horse.</small></span>                      <p><h4><span class="w3-border-bottom">Woman on Horseback</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bandon - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom woman on a white horse once rode around this area. She is said to have given local man Owen Kelly a lift after he failed to recognise her as a spirit - after riding around in circles and crossing the river three times she vanished, leaving Owen by the fence of his farmland.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Presence</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bantry - Bantry House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A comforting presence is reported to felt in the nursery.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 136</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=136"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=136"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
