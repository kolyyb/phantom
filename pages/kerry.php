

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="County Kerry Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Kerry</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Kerry Ghosts, Folklore and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Funeral Procession</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyferriter - Graveyard<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> After a non-local man was buried in this graveyard, the ghosts of his ancestors were seen walking slowly towards his grave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Danish Captain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyheigue - Ballyheigue Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghost was once a well-treated prisoner in the castle, who died trying to protect the building from a large scale robbery.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rumoured Entity</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barrow - Barrow House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Prior to renovation, the site had a reputation as being home to a ghost, although the exact details are not known and it is now said the phantom is quiet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Humming Train</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beaufort - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> A sound likened to that of a train has been said to wake locals in the early hours of the morning, even though the closest rail tracks are 15 kilometres away. The humming sound has been reported to be so loud that houses vibrate.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Brandon-Mount-Brandon.jpg' class="w3-card" title='A gold covered snake-like creature.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A gold covered snake-like creature.</small></span>                      <p><h4><span class="w3-border-bottom">The Kerry Carabuncle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brandon - Mount Brandon - Lough  Veagh and/or Lough Geal<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This lake is home to a mythical snake-like beast which is covered in jewels and gold. The monster is said only to appear once every seven years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dying Cattle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brosna - Former croft at Knockeencreen (currently a cattle shed?)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A man whose cattle were dying mysteriously was informed by a passing gypsy that his home was blocking a fairy path, and by leaving the doors open at night, the fairies could pass happily pass through. The man did so, and the deaths stopped.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Earless Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clashmealcon - Area around Brown's Castle<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly, earless dog that haunted this area was supposed to emerge from the castle's moat at night before wandering off to hunt rabbits.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Death Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Derrymore - Derrymore House<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> As the owner of the house lay dying, the other family members around his bed heard a horse drawn coach pull up outside the house. Believing it to be the doctor, they rushed downstairs, but nothing could be seen on the gravel driveway. The owner died soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Warning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Derrynane (aka Darrynane) - Off the coast<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely 1870<br>
              <span class="w3-border-bottom">Further Comments:</span> A fishing vessel crewed by eleven men sank on shallow rocks, the accident killing two. Some of the survivors claimed they had heard a disembodied voice warning them not to embark, and then just before the accident, they heard laughter and other noises, but could not see anything in the darkness of night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cow Protectors</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dingle - Fairy fort just outside of town<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Farmer Hanafin left a cow unmilked on his land to ensure the local fairies could feed their children. When Hanafin fell into financial difficulty and the bailiffs tried to take his cattle as payment, the fairies fought back; the bailiffs were almost killed by an unseen force, and the cattle ran back to pasture.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Banshee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dingle - John Street<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A family named Hussey were filled with dread when they heard the wailing of the banshee outside their home. However, the banshee stopped crying and told them that she would never warn 'hoarding traders' of impending death. Sure enough, the following day the family discovered another man by the name of Hussey in the town had died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screams of Spanish Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dun an Oir - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Over eight hundred Spanish soldiers landed here in 1580, overrunning an English garrison. Spanish reinforcements failed to come, however, and their victory was short lived when more English troops arrived. The Spaniards surrendered, but most were immediately put to death by the enraged English. Their death throes echo around the area on the anniversary of their defeat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sheriff</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fenit Island - Barrow Round Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ruins of this castle were reputedly home to the ghost of a former Kerry sheriff.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ghost Whisperer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kenmare - Dromquinna Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> Owner Mike Robertson made the local press after he offered a reward of 50k to anyone who could tame the three ghosts which haunted his property, thought to a one woman and two men. The ghosts were blamed for the strange sounds which were heard after dark.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mermaid?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kilconly Point - Sea<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960-1962<br>
              <span class="w3-border-bottom">Further Comments:</span> A mermaid which was spotted several times in this area was described as a 'normal' woman, leading one to conclude that maybe she was..?</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gwendolyn Herbert</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney - Cahernane House Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Gwendolyn, the daughter of the first owner of the property, is said to remain here and has been spotted on the main staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Staircase Lurker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney - Coolclogher House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom said to have been heard on the staircase would appear to have been banished after a Mass was performed on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney - Derrycunnihy Church and surrounding area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A variation on the vanishing hitchhiker story has the ghostly white woman supposed to haunt this church sometimes appear in the back seat of passing cars. The stories say she died in an accident close to the church while cycling home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">French Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney - Governor's Rock<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> The GhostEire team recorded an EVP that they identified as a girl speaking French.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sarah Reynolds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney - Killegy Cemetery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former servant at Muckross House, it is said that Sarah died in mysterious circumstances after entering a relationship with the owner of the property.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Donal</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney - Lake Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Donal McCarthy Mor founded the nearby Muckross Abbey but has now wandered away and frequents the hotel bar. The site is also reputed to be haunted by a young woman or girl in nineteenth century clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Killarney-Lough-Leane.jpg' class="w3-card" title='The phantom horse and rider emerge from the lake.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The phantom horse and rider emerge from the lake.</small></span>                      <p><h4><span class="w3-border-bottom">O'Donoghue</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney - Lough Leane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (O'Donoghue, every 7 years) (reoccurring), others on misty nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A warrior by the name of O'Donoghue is said to emerge from the waters of the lake on the back of a white horse once every seven years. Another story has an unidentified figure walking a hound appear on misty nights.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robed Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney - Old Kilbeggan Distillery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2007?<br>
              <span class="w3-border-bottom">Further Comments:</span> Several people are reported to have seen a robed figure walking around the site, leading to speculation the distillery is haunted by a monk. One of the original owners of the property, constructed in the mid-eighteenth century, was also said to have remained here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pale Eel-Thing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney - Upper Lake<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 16 September 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Cryptozoologist Jonathan Downes and his wife spotted a nine-foot (2.74 metre) long, pale eel-like creature swimming in these waters.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bess Stokes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killorglin - Ard na Sidhe Country House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1934<br>
              <span class="w3-border-bottom">Further Comments:</span> Bess Stokes was said to take the form of an older lady in grey, and haunt the gate close to the cottage that she had built.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 40</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=40"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=40"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
