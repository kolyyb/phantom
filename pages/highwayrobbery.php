


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Highwayman Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Highwaymen & Women</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Highway Robbers - Ghosts and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">William Nevison</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardwick le Street (Yorkshire) - Hanging Wood<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The woods were a popular place for Nevison, a highwayman, to lurk before riding out to commit his crimes. After he was hanged in York, his shade returned to the scene of better times.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Armathwaite (Cumbria) - Fox & Pheasant Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A previous landlady and her friend observed a woman wearing old fashioned brown clothing walk through the closed front door. The ghost is said to be a woman who lost both her children outside the inn in a coach accident. A highwayman is also reputed to haunt the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atwick (Yorkshire) - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This decapitated spectral bandit remains lurking in the shadows of his old ambush area. One version of the story says he remains on his horse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadowy Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bagley Wood (Berkshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> May 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking through the woods after dusk, a couple found themselves chased by a large shadowy quadruped that stood just over a metre tall. One of the witnesses said it did not move like a deer or a dog. The woods is also reputedly home to a phantom highwayman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tom Faggus</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnstaple (Devon) - Ashleigh Road area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people witnessed the ghost of a man riding a large horse in this area. The witnesses carried out some research and believe they had encountered the ghost of Tom Faggus, a Robin Hood-like figure who operated in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Batley-Howley-Hall.jpg' class="w3-card" title='A ghostly woman with a red veil.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly woman with a red veil.</small></span>                      <p><h4><span class="w3-border-bottom">Veiled Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Batley (Yorkshire) - Howley Hall ruins, and surrounding golf course<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman with a red veil has been reported here, sometimes accompanied by two male escorts. It has been speculated that one of these men could be a highwayman by the name of William Nevison. Civil war soldiers are also said to haunt the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baynards Green (Oxfordshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed in the seventeenth century, this highwayman refused to leave and continued to ride around the area to the annoyance of locals.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Tricorn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beckhampton (Wiltshire) - Road running through village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom highwayman is reputed to haunt this road. There is also a story that a ghostly horse and coach have been heard along the Beckhampton Road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Tom</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford (Bedfordshire) - Junction of Tavistock Street, Union Street and Clapham Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Around Christmas, 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A nineteenth century highwayman, Black Tom was executed and buried at this junction with a stake driven through his heart to prevent his return. The ritual failed, and both he and another unknown ghost were seen countless times for years following his death. A 1960s sighting of Tom took place in broad daylight, with witnesses stating they saw a gruesome figure with a broken neck staggering down the road, vanishing into thin air seconds later. The witness who spotted Tom during the 1990s thought he was just a drunk man in fancy dress, until he suddenly disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Turpin in Green Velvet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bickley (Kent) - The Chequers public house, Southborough Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says Dick Turpin's ghost was seen in an upstairs room sitting at a desk, writing with a quill. Another story says a woman in eighteenth century garb also haunts the upper part of the building. The pub hit headlines in September 2013 after it was reported a phantom French Soldier called Barnard scared away two bar staff who were staying overnight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George Lyon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Billinge (Lancashire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding on his horse, this former highwayman passes through the village where he once hid out.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murph</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackpool (Lancashire) - Eagle and Child public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former highwayman, Murph can sometimes be heard mumbling and moaning at the bar late at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blandford Forum (Dorset) - Crown Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing a black crinoline dress, this ghostly woman has been observed on the first floor. One of the cleaning staff spotted a woman in black enter a room, but upon investigation, the room was devoid of life. The hotel is also home to a phantom highwayman who hangs about in the courtyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Female in Long Dress with Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blythburgh (Suffolk) - A12 from Ipswich, towards Kessingland, near Blythburgh common<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1700s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Sometimes appearing with a horse, the unknown woman and highwayman have been reported a few times over the past two hundred years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Noosed Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton (Greater Manchester) - Belmont Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This shade, mistakenly hanged for highway robbery, still has the hangman's noose tied around his neck when he appears. They also say his eyes look as if they're ready to pop out of his head. Another report says that the road is also haunted by a fatal car crash from the mid-twentieth century which killed a family - the screech of brakes followed by screams is heard but no accident can be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tom Hoggett</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boroughbridge (Yorkshire) - Great North Road (A1), between town and Scotch Corner<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This former highwayman, wearing a coat so long it almost drags along the ground, is still thought to lay in wait for the unwary along this stretch of road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bramshot-Roads.jpg' class="w3-card" title='A ghostly highway escapes across a field.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly highway escapes across a field.</small></span>                      <p><h4><span class="w3-border-bottom">Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bramshot (Hampshire) - Roads in the village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Roads running in and through the village are reportedly home to several phantom entities. A highwayman, a cavalier, a small group of Tudors have all been reported here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pocock</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgwater (Somerset) - A39 between Street and Bridgwater<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> Pocock, a highwayman, was badly injured in a fight. Pocock managed to escape and hide, only to die of his wounds in a cave which has never been discovered. His ghost has been seen on horseback, curiously galloping on the spot, and has been blamed for several accidents in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grizelda the Highway Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buckton (Northumberland) - Roads in the village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> At the end of every month (reoccurring), last seen 1987<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed as a male highwayman, this girl has been seen still riding her midnight black horse. She reportedly held up a stagecoach carrying an execution order condemning a friend of hers, which bought him enough time to escape the noose.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf830.jpg' class="w3-card" title='Ye Olde Three Tuns pub, Bungay.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ye Olde Three Tuns pub, Bungay.</small></span>                      <p><h4><span class="w3-border-bottom">Rex Bacon and Friends</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bungay (Suffolk) - Ye Olde Three Tuns public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1969<br>
              <span class="w3-border-bottom">Further Comments:</span> An investigation by a medium revealed this building was haunted by 24 ghosts (!), one of whom was Mr Rex Bacon, a highwayman from the eighteenth century whose base of operations was the inn.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Whistling</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Calne (Wiltshire) - Area around Black Dog Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2016, 0200h<br>
              <span class="w3-border-bottom">Further Comments:</span> A group of friends exploring this area heard a loud, tuneful whistling with no discernible source. One of the witnesses was later told the site was the haunt of a phantom highwayman who would whistle while he worked.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Naked Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Calne (Wiltshire) - Road in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A naked man is said to haunt a road in this area. One story says that the figure was a highwayman who stripped and oiled himself up, to ensure that no one could grab him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Carlisle-A6-Barrock-Hill.jpg' class="w3-card" title='The ghostly highwayman of the A6.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ghostly highwayman of the A6.</small></span>                      <p><h4><span class="w3-border-bottom">Whitfield</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carlisle (Cumbria) - A6, Barrock Hill near Carlisle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An infamous highwayman, Whitfield's capture resulted in his hanging for murder - but the rope, not tight enough, did not kill Whitfield outright. Several days later a passing coachman heard Whitfield crying and moaning, still dangling from the noose. To end the suffering, the coachman shot the hanged man; Whitfield's ghost can still be heard today crying for help.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/kent9790.jpg' class="w3-card" title='Upper Bell Inn, Chatham.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Upper Bell Inn, Chatham.</small></span>                      <p><h4><span class="w3-border-bottom">Turpin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chatham (Kent) - Upper Bell Inn (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This public house was said to be haunted, possibly by the ghost of Dick Turpin, although there is a distinct lack of accounts of any sightings. A headstone which stood in the beer garden most likely added to its spooky atmosphere, as did its location, close to Blue Bell Hill and its infamous vanishing hitchhiker (but more on her another day). One paranormal group claimed to have photographed 'orbs' on this site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chingford (Essex) - Area around Richmond Avenue and The Avenue in Highams Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> New Year's Eve, year unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman out walking in the area reported seeing a phantom highwayman who rode past her - the terrified witness ran back to her family.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 101</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=101"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=4&totalRows_paradata=101"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
