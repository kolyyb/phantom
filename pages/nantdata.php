


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Northamptonshire, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastmidlands.html">East Midlands</a> > Northamptonshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Northamptonshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Abington-Pig-Lane.jpg' class="w3-card" title='A phantom coach and horses.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom coach and horses.</small></span>                      <p><h4><span class="w3-border-bottom">Coach with Headless Driver</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abington - Pig Lane (lane may no longer exist or has been renamed?)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Racked with guilt after deliberately running over his daughter's lover in 1780, this man now continues to drive his coach and horses down the lane, though he has lost his head since death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Vanishes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abington - Abington Park Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1986<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman walked over to a horse drawn carriage on display and leaned in to inspect the interior of the carriage. Inside, a sitting lady suddenly appeared. The apparition stared at the woman then vanished. The witness made a quick exit, without even telling her friend why she was leaving.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Baby</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abington - Black Lion public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The flickering and dimming of the electric lights is sometimes accompanied by the cries of a small child. Various items have been reported as being moved by an invisible hand.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jane Leeson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abthorpe - Area around ruined Manor House (may no longer be standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Leeson is remembered mainly for donating money to fund a school built in the village, though she hung around for a little while after her death. A monk or friar is also said to have haunted this building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Eyed Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ailsworth - General area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This huge hound possesses eyes that burn as bright as the sun, making the creature easy to spot if he tries to sneak up on you.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Groom carrying Lantern</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Althorp - Althorp Park<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1820s / 1830s (Groom), others throughout the twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Several entities have been observed here. Seen only the once (though the exact date is not known), the horse groom was seen standing over a bed, a bright lamp in one hand - this was exactly two weeks after his death. Another ghost, that of a child, was seen several times prior to World War I, and a local gentleman named Jack Spencer (who died in 1975) was seen attending a party in the building a few months after his death. Finally, an old servant has been reported visiting bedrooms at night if lights have been left on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Mildmay</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Apethorpe - Apethorpe Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Well known in the neighbourhood for her generosity, Lady Mildmay's shade is said to return to the hall and give money to anyone who approaches.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robin's Well</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aynho - Spring known as Puck Well<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century, current status unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Robin Goodfellow and his friends were thought to frequent this spring.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Queeky</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aynho - Unnamed cottage (and another unnamed house)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1947 - 1953<br>
              <span class="w3-border-bottom">Further Comments:</span> Alexa Young moved from her home in the village to escape a poltergeist she nicknamed Queeky, only to discover that it had followed her to the new house. The entity would move and hide objects, sometimes returning them several days later. Unlike most poltergeist encounters, Young claimed to have seen the entity on one occasion, describing it as a four-foot figure dressed in grey, with bird-like qualities.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Badby-woods.jpg' class="w3-card" title='A phantom rider in woodland.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom rider in woodland.</small></span>                      <p><h4><span class="w3-border-bottom">Woman on Horseback</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Badby - Woods<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding through the forest, the woman travels either to or from her former lover's home - on her last mortal journey her husband intercepted her mid-way and murdered the adulteress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barby - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1851<br>
              <span class="w3-border-bottom">Further Comments:</span> The spirit that harassed a family in the unnamed building only ceased manifesting and creating loud bangs when they discovered a large sum of hidden money and paid off her debts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drummer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barford Bridge - Tumulus (flattened in 1964)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Before being excavated in 1964, the tumulus was said to be the home of a phantom drummer boy who would appear at midnight. A dozen skeletons, thought to be Roman, were removed in the dig.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Button Cap</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnack - Former rectory, Kingsley House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Named after a former vicar who would walk around wearing slippers, a dressing gown, and a night cap with a large button sown on, this ghost could be heard pacing throughout the building, turning the pages of a book, and occasionally creating the sound of rolling barrels in the basement.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shagfoal</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnack - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Similar to (or maybe identical to) a shuck, the Shagfoal is reported to resemble a large, black longhaired bear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk with Whip</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnwell - Barnwell Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The strong gusts of wind that are felt on windless days are blamed on a murdered monk, who walks this area with a whip.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnwell - Graveyard belonging to former All Saint's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> Last seen by a man walking his dog in the area, this phantom may be related to a gravestone that features a monk-like figure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Isabel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barton Seagrave - River Ise<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Floating high enough so that her feet remain dry, the shade of Lady Isabel passes quietly over the river.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Suffocation</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blisworth - Canal tunnel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Even though the tunnel is 2,813 metres in length, when first constructed there was only one ventilation shaft. This poor design resulted in two people dying in 1861; one suffocated while the other fell unconscious into the canal and drowned. It is said as travellers pass over the site of this accident, they can hear coughing and the loud splash of someone hitting the water. One witness reported seeing the lights of long dead construction workers in the tunnel on the bicentenary of its opening, and then again about a year later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Spring Warning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boughton - Field, exact location not known, but close to Kingsthorpe Road<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century, current status unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This spring would mostly be dry, but when it did run, it pumped out vast quantities of water. It was considered an omen of death or of troublesome times.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Video Tape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boughton - Old cemetery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> It is reported that if the cemetery is videoed at night, a voice of a young girl can be heard on the tape during playback, apologising repeatedly for an unknown crime.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadowy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boughton Green - St John the Baptist church ruins<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure, also heard moaning around the same time of year, is thought to be a criminal hanged in 1826. Others speak of a darker tale - that the ghost takes the form of a beautiful man or woman, depending on the gender of the witness, and asks them for a kiss. If it is given, the kisser is doomed to die within a month. This is said to have last happened to a man called William Parker on Christmas Eve 1875; after meeting a red headed girl at the church who faded away shortly after engaging with her, William died exactly one month later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Animated Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brackley - St Peter's Church<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The stone figures standing over the west door climb down when the clock strikes twelve, crossing the churchyard to drink from a nearby well.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Braunston - Admiral Nelson Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Several people over the years have seen a shadowy man dressed in black pass through a bricked up doorway at the inn.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Braunston - Millhouse Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former manager of this inn reported seeing a phantom dog which reportedly died in the property.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Brington-Unnamed-pond.jpg' class="w3-card" title='Fairies enjoying the pond.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Fairies enjoying the pond.</small></span>                      <p><h4><span class="w3-border-bottom">Water Lovers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brington - Unnamed pond in the area<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Fairies in this parish could be seen playing on the surface of a pond and within the plants on the banks.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 151</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=151"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=151"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastmidlands.html">Return to East Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="ViewWest Midlands records" style="width:100%" title="View West Midlands records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
