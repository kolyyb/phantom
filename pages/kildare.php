

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Kildare Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Kildare</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Kildare Ghosts, Folklore and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bog-of-Allen.jpg' class="w3-card" title='Vaguely human shapes standing in the bog.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Vaguely human shapes standing in the bog.</small></span>                      <p><h4><span class="w3-border-bottom">Strange Mist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allen - Bog of Allen<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Before a large part of this boggy land was drained, it was renowned for strange shapes, vaguely human in appearance, which appeared at night. One man who walked across the area encountered a strange cloud which enveloped him, filling the poor man with terror, before it drifted off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Peeping Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Athy - White's Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly figure has been seen peering out a second floor window of the castle. A few critics pointed out that most witnesses have just left public houses and bars at midnight and may not have been in the clearest of minds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wizard Earl of Kildare</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballitore - Rath of Mullaghmast (earthworks and forts)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Wakes every seven years (date unknown)<br>
              <span class="w3-border-bottom">Further Comments:</span> The wizard Earl of Kildare reputedly sleeps around this area of forts and standing stones, waking every seven years to ride a horse shod with silver. Some say that while out riding he is accompanied by his men, and that when the horse's silver shoes have worn down, the wizard will banish Ireland's foes from the land.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Band</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballitore - Village Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1933<br>
              <span class="w3-border-bottom">Further Comments:</span> On two different occasions, villagers heard drums, brass, and a cornet being played in the village hall at midnight, although the building would be locked and unlit.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stable Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castledermot - Kilkea Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2020s<br>
              <span class="w3-border-bottom">Further Comments:</span> A stable boy, killed after being discovered in the bedroom of the owner's daughter, still walks around the building looking for his former girlfriend. The castle also carries stories about being haunted by a former wizard who once resided there, who remains in a trance under the structure. More recently, a groundsman spotted a woman and a child heading running along the avenue towards the castle - the figures suddenly vanished. Shadowy figures and disembodied voices have also been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Grey Coat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Celbridge - Castletown Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The spectre dressed in a long grey coat is more likely to be heard laughing mockingly. One legend attached to the building has the Devil paying a previous owner a visit - it is unclear whether the haunting is connected to this event.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Big Eyed Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clane - Clongowes Wood College<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom hound with glowing eyes which walked the corridors at night was said by students to be the manifestation of a hanged murderer. The building was also once witness to a Crisis Manifestation, when the ghost of a Marshall appeared to two sisters - they identified him as their brother, who, it was later discovered, was killed in action at the time of the visitation.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Matron</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clane - Firmount House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> During a two day paranormal investigation at this location by NIPRA the group believed they encountered a matron who worked with Tuberculosis patients.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tiny Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Forenaghts Great - Longstone Rath<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October 1971<br>
              <span class="w3-border-bottom">Further Comments:</span> Author Herbie Brennan and a friend visited this site late at night and spotted several small white horses galloping along the earthwork. He described them as being no taller than a cocker spaniel and around 25 in number.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Handsome Jack</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Grangemellon - Grangemellon Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Colonel John St Leger, aka Handsome Jack once owned this castle while a member of the Hellfire Club. After his sudden death in Madras, he returned to the area, in a coach and four, driven by a headless horseman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Kildare-Road-Clongowes-Wood.jpg' class="w3-card" title='A phantom child runs across the road.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom child runs across the road.</small></span>                      <p><h4><span class="w3-border-bottom">Young Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kildare - Road outside Clongowes Wood College<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Several motorists have had to brake sharply to avoid hitting a child who runs out in front of them. The infant always quickly vanishes after the driver stops. The area is also home to a phantom coach which halts at the front gates at midnight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drinking Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kilgowen - Monolith<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Monolith still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This large stone reputedly moves down to a nearby stream to drink every night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kilkenny - Rothe House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly footsteps heard around the building may be related to the seven-foot tall figure spotted walking through a doorway and heading towards the Phelan Room. A priest may also haunt the site (or may be the same figure).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Leixlip-Conollys-Folly.jpg' class="w3-card" title='The Devil pops up after being summoned.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Devil pops up after being summoned.</small></span>                      <p><h4><span class="w3-border-bottom">Walking Backwards</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leixlip - Conolly's Folly (aka Conolly's Obelisk)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A local piece of folklore claims that walking backwards around the structure twelve times results in the Devil appearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Possible Poetry Inspiration</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lyons Hill - Canal Lock (13th Lock)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Arthur Griffin's poem 'The Thirteenth Lock' may have been inspired by the Lyons Hill's lock, the thirteenth along the Grand Canal, the building of which had displaced an old graveyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Projectionist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Maynooth - Aula Maxima (theatre)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be a projectionist from the 1940s who fell to his death, this phantom is reputed to turn a particular chair around 180 degrees if he dislikes a production.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Suicidal Student</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Maynooth - Maynooth College, Rhetoric House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> After two students committed suicide in room 2 of the building, and a third leapt from the window after believing an evil spirit was trying to take over his body, someone summoned a priest to spend the night. In the morning the priest was found a gibbering wreck; fear had turned his hair white.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Limping Liam</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Prosperous - Main Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1798 onwards?<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghost nicknamed Limping Liam hobbles around this area. One story says the body of an unknown man killed in the Irish Rebellion of 1798 was brought to the town on the back of the cart. The body tumbled off the cart and limped a little way along the road before falling and dying again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sallins - Soldier's Island (small island at conjunction of the Grand Canal and the Naas)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The island is said to take its name from a solider who hanged himself from a tree - his ghost is said to have remained.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Railway Guard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Straffan - Former railway track<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1853 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> This former rail employee walks along the track carrying a lamp. In 1853 an accident between two trains killed sixteen people; an inquiry blamed the guard who had forgotten to attach a lamp at the rear of the train (which would have prevented the collision). To atone, the guardsman forever walks the rails.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 20 of 20</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
