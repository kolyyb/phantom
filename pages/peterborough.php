
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Peterborough Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

 <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Peterborough</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Peterborough Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dashing Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - A1 north of Peterborough, 2 - 4 miles outside of city<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1995<br>
              <span class="w3-border-bottom">Further Comments:</span> During a clear day, a driver and his wife overtaking a lorry at 70mph watched a woman in 1950s clothing, carrying a bag, dash in front of their car to the central reservation. The driver had no time to slow down but did not hit the woman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam8369.jpg' class="w3-card" title='Haunted Peterborough Cathedral.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Haunted Peterborough Cathedral.</small></span>                      <p><h4><span class="w3-border-bottom">Choir</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Several ghosts are said to haunt the cathedral. The sounds of an invisible choir have been reported, while phantom monks appear in the graveyard (one of whom vanishes through a closed side doorway. A light resembling a candle has been spotted floating around the upper floors at the front of the building. Finally, an old piece of folklore says that if a hare makes a lair in the building, the city would be destroyed by fire within a week.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Cherry Tree public house, Oundle Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The pub is said to be haunted by a man wearing Victorian clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fred</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Cock Inn, Werrington<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Not much is known about this ghost, other than being nicknamed Fred.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam8939.jpg' class="w3-card" title='Cowgate, Peterborough.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cowgate, Peterborough.</small></span>                      <p><h4><span class="w3-border-bottom">Snatchers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Cowgate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once the site of a cemetery, the area at the end of Cowgate is said by some (but not many) to be haunted by people who had their earthly remains removed by body snatchers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Eight Bells public house, Lincoln Road, Millfield (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> This pub was said to be home to a phantom monk, a ghostly woman, and the occasional mild outbreak of poltergeist behaviour.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Disembodied Left Hand</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Field near railway line, Werrington<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 26 September 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> A disembodied left hand appeared to be feeling its way around an area by three large trees. The witness who observed the appendage was later told by a neighbour that they had seen a lad wearing a cap and blazer sitting in one of the trees who had vanished when approached.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Broken Crockery</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Guest house (no longer operating as such), Eastfield Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960<br>
              <span class="w3-border-bottom">Further Comments:</span> Two men staying at the property awoke one morning to find broken cups and plates scattered across the kitchen, though they had heard nothing during the night. The sensation of being watched by something unseen was also felt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam8375.jpg' class="w3-card" title='HG&#039;s pub, Peterborough.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> HG&#039;s pub, Peterborough.</small></span>                      <p><h4><span class="w3-border-bottom">Those Shadows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - HG's public house, Cathedral Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A dark shadowy figure has been spotted ascending the staircase, but when staff check to see who the intruder is, no one can be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crashing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - House along Mayor's Walk (likely no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1891 - January 1892<br>
              <span class="w3-border-bottom">Further Comments:</span> Bedsheets would be thrown on to the floor, doors violently shaken and crashing so loud it was if parts of the house were falling (although nothing would be found out of place). Neighbours tried to stay the night to discover the source of the activity but would flee before dawn.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hounds All Black and Wide-Eyed</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - In all the woods between Stamford and Peterborough<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Between Lent and Easter (reoccurring), but last reported 1100s<br>
              <span class="w3-border-bottom">Further Comments:</span> These woods were the scene of a wild hunt, which included large black hounds and loathsome riders on the backs of mares and goats.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam8324.jpg' class="w3-card" title='Peterborough Museum.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Peterborough Museum.</small></span>                      <p><h4><span class="w3-border-bottom">Thomas Hunter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Anzac during the 1970s, others unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sergeant Thomas Hunter, with the Anzacs during the First World War, died in the building 31 July 1916 when it was a hospital. Hunter was last seen drifting up the staircase, though he is blamed for moving items around the building at night or placing icy cold hands on people. Other entities said to haunt here include a scruffy man in the cellar, a white woman in the upper part of the building, a young girl in the former operating theatre, and a phantom Roman soldier who stands by a sword that is on display.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pale Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Orton Hall Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Staff and visitors have reported seeing a phantom white lady around this building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Prezzo Italian restaurant (was O'Connell's public house)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom woman dressed in Victorian era clothing is not alone here - the building is also home to a ghostly male figure dressed in eighteenth century garb.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam8377.jpg' class="w3-card" title='Queensgate Shopping Centre, Peterborough.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Queensgate Shopping Centre, Peterborough.</small></span>                      <p><h4><span class="w3-border-bottom">Giggling</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Queensgate Shopping Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sometimes heard once the shopping centre is closed to the public, the sound of a young child giggling and disembodied footsteps have been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Engines Overhead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - RAF Westwood (aka RAF Peterborough)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Even though the area is now a housing estate, this former RAF base is still said to be haunted by the sound of old engines flying overhead and airmen dressed in old uniforms.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam117.jpg' class="w3-card" title='Skies over Peterborough.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Skies over Peterborough.</small></span>                      <p><h4><span class="w3-border-bottom">Scareship</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Skies over Peterborough<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 March 1909<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by two police officers, this UFO resembled a large airship, travelling at a then unheard of speed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Watch the Timing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - St John's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Concerning the bells of the church and the cathedral, an old rhyme says, 'when the church and the abbey they strike together, they'll either be a death or change in the weather'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse and Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Sutton Heath Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> While travelling along the Sutton Heath road, one man watched a figure driving a horse and carriage emerge from a bush on the right hand side and cross the road ahead of him. The driver stopped, not believing what he had witnessed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jangling</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - The Bull Hotel, Westgate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Footfalls and the sound of keys have been heard here, but the source never seen. A ghostly dog is also said to haunt the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam8372.jpg' class="w3-card" title='The Verve Bar, Peterborough.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Verve Bar, Peterborough.</small></span>                      <p><h4><span class="w3-border-bottom">Cellar Shenanigans</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - The Verve Bar<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It is said that the cellar of this establishment is not averse to a little poltergeist activity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cam8943.jpg' class="w3-card" title='Tourist Information Centre, Peterborough.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Tourist Information Centre, Peterborough.</small></span>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peterborough - Tourist Information Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom monk is reputed to haunt this building.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 22 of 22</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
