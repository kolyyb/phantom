

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places on Scotland's Islands, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Shetland</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Shetland Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Barbara Pitcairn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brae - Busta House Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It has been suggested that the apparition of a grey-haired woman wearing a brown dress and a lace cap is Barbara Pitcairn, secretly married to a former owner of the house just prior to his death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Long Haired Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brae - Fields in the area, and stone (once?) known as MacQueen's Big Rock<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The long-haired female fairy that followed herds of cattle around the island would be appeased with milk poured into a large rock.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smelly Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burra Haaf - Waters in the area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1900s<br>
              <span class="w3-border-bottom">Further Comments:</span> Several reports of a creature with a sail-like body and a flipper on the end of its nose exist in this region of water off the Shetland Isles. A fishing crew who encountered it stated the beast destroyed several of their nets and came so close to their boat they could detect a pungent stench.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Petrified Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fetlar - Haltadans stone circle<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This group of standing stones are said to have been trolls who danced until dawn - unable to return to their cave in time, they all turned to rock.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Fetlar-watermills.jpg' class="w3-card" title='Fairies in folklore are not particularly friendly.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Fairies in folklore are not particularly friendly.</small></span>                      <p><h4><span class="w3-border-bottom">Harassed by Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fetlar - Unnamed disused watermills on the island, unlikely to remain<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Several abandoned watermills on the island were associated with myths, the most prevalent one being that fairies had driven the owners away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nygel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Foula - Mills with water wheels<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Nygel was a water horse that appeared to hang around locations with water wheels, using its teeth to stop the wheel turning. It had a tail that arched over its back. One story said the creature entered a home of a local man; the occupant fled, and the building was left empty for thirty years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Halibut-Tailed creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Yell - Thirty miles off the coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1833<br>
              <span class="w3-border-bottom">Further Comments:</span> Three (or six) fishermen claimed to have caught a ninety centimetre long mermaid while at sea. The creature had arms around 23 centimetres in length with webbed fingers, and while it had blue eyes and nostrils, no ears or chin could be seen. It possessed a tail similar to a halibut and had two fins on its shoulders. After listening to the creature wail for three hours, the fishermen threw it back into the sea.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gentleman in Top Hat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Yell - Windhouse, near the Lumbister RSPB Reserve<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Merely one of the spooks found here, the man in the top hat not putting on the Ritz shares the house with a woman dressed in silken clothes, a former pet dog, and a servant who is doomed to spend the rest of time ascending the staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lerwick - Garrison Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An old story says that a ghostly soldier would lurk at the foot of a staircase near the performers' entrance, while another says the lighting area is haunting by something.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lerwick - Old Radio Scotland building<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Employees working late would hear footsteps cross the upper floor, even though the area would be devoid of life. When the broadcasters moved from the building, the haunting ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lerwick - Osla's Cafe<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A former employee claimed to have heard ghostly footsteps on the staircase and the sound of someone unseen rummaging around through a trunk upstairs.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lerwick - Shetland Library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Formerly a church, the library is reputedly haunted by the ghost of a man wearing a brown coat. Staff claim that books have flown off shelves without being touched, and disembodied footsteps heard in the basement.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">U Boat Sighting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Muckle Flugga - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 28 July 1918<br>
              <span class="w3-border-bottom">Further Comments:</span> The German submarine U108 encountered a giant marine saurian, estimated to be 18 to 24 metres in length.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Small Trolls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northmavine - Trowie Knowe cairn<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This cairn was used by trows who would only leave it by moonlight to go hunting.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unknown Killers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Papa Stour - Across the island<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> After midday, unknown entities would emerge from hiding and cross the island, looking for anyone who had wandered too far from town. They were said to be able to hurt or kill even the strongest man.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Papa-Stour-Exact-location-unknown.jpg' class="w3-card" title='A selkie out at sea.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A selkie out at sea.</small></span>                      <p><h4><span class="w3-border-bottom">Selkie Prince</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Papa Stour - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A group of hunters who arrived at the island killed and skinned many seals before a violent storm began to batter the island - unbeknown to the hunters, a selkie prince was amongst the seals. The hunters finally returned the skins to the remaining seals, after which the storm ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Remains in the Bay</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Scalloway - Bay<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1810<br>
              <span class="w3-border-bottom">Further Comments:</span> The remains of a large mystery sea creature drifted into Scalloway Bay. Described as a looking like an upturned vessel, it remained in the bay for two weeks before disappearing - during this time, no one was brave enough to approach it for a closer look.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Presence</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Scalloway - Gallows Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The 'presence' that haunts this hill is likely a reminder of the innocent women who were murdered here after accused of witchcraft.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Scalloway-Castle.jpg' class="w3-card" title='An old photograph of Scalloway Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old photograph of Scalloway Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Scalloway - Scalloway Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> During a production of Macbeth, a witness reported seeing a figure on the wall of the castle who vanished. Local folklore says Earl Patrick Stewart, who commissioned the castle, insisted the walls contain the hair and blood of locals.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Kelpies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Scalloway - Waters around the area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This area was thought to be home to humanoid sea creatures that drowned their victims before partaking of the deceased flesh.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shoepultie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Shetland - Exact areas unknown<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This creature was said to be like traditional water horses, other than it would use its tail like a propeller once in the water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Shetland - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1822<br>
              <span class="w3-border-bottom">Further Comments:</span> A fisherman who caught a mermaid while out at sea killed the creature with a knife. It was said that from that moment, he was cursed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Noggle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Shetland - Near water wheels<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Noggle was a type of water horse that could be found stopping water wheels. When a miller appeared to chase the beast off, he would often be tricked into mounting it - being taken to deep water and his doom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tangie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Shetland Islands - Fitful Head (cave), aka the Thief's Hole<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A mythical seahorse that was owned by the robber-bandit Black Eric. The creature continued to roam the area for a while following its master's death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Upset Sea Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Shetland Islands - Southeast of the island of Fetlar<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1881 or 1882<br>
              <span class="w3-border-bottom">Further Comments:</span> The crew of the fishing vessel Bertie fired several shots at a sea monster they passed - this angered the beast, which created waves of such force that they washed over the boat, taking away fishing gear and other supplies. The creature continued to stalk the crew until night fell.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 36</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=36"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=36"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
