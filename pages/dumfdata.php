

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts and Folklore. Strange Scotland - Dumfries and Galloway, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Dumfries and Galloway</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Dumfries and Galloway Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stagecoach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annan - A75 between Gretna and Annan<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2004, around 21:30h (stagecoach), 2000s (man in red)<br>
              <span class="w3-border-bottom">Further Comments:</span> A lone driver performed an emergency stop after a stagecoach pulled by a horse drove across the road in front of her. The witness described the coach 'like a grainy fuzzy picture you'd see on an old black & white TV'. The incident lasted no more than ten seconds. Almost immediately after, the driver received a phone call from her partner, who had feared she had been involved in an accident. A few years later a woman driving from Annan reported hitting a man wearing a red jumper and dark trousers as he stepped out in front of her car. However, the man vanished when she looked for his body. There are said to be several other reports of people hitting the same man in the area. Other ghosts seen along this road include an old man with no eyes and an aging woman in Victorian clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming Hag</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annan - A75 to town from Dumfries<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> April 1962<br>
              <span class="w3-border-bottom">Further Comments:</span> Two brothers travelling along this road were terrified by strange entities that appeared in front of their car, including phantom animals, an old man and a woman who resembled a traditional witch. Their ordeal ended when they thought they were now about to hit a white van head on - the vehicle vanished into thin air on impact. There have been more, but less dramatic, sightings of entities along this road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Long Legs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annan - Cummertrees junction, A75<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Waiting in their car after breaking down, one person watched as a pair of legs taller than their vehicle run past. The witness realised that the legs, which disappeared into nearby woodland, were not attached to a body.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Porteous</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annan - Spedlins Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The poltergeist activity here was attributed to the ghost of a miller named Porteous who starved to death in the dungeon under the castle. The problems followed the owners of the family when they moved out, though the ghost's activities ceased when a bible was placed by the old dungeon door.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Annandale-Burnwark-Hill.jpg' class="w3-card" title='Sinister fairies at dusk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Sinister fairies at dusk.</small></span>                      <p><h4><span class="w3-border-bottom">Evil Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annandale - Burnwark Hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A palace concealed within this hill was run by an army of evil fairies - they would kidnap anyone foolish enough to venture close to their land.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Family Curse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anwoth - Cardoness Castle<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A curse placed on the castle ensured its ultimate ruin - three owners went bankrupt, and a fourth owner and his family were drowned in the nearby loch when the ice cracked whilst they skated in winter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stable Lad</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arbigland - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered to prevent the continuation of an affair of the heart, this young stablehand was then buried at a crossroads to conceal the crime. His ghost is reported to still travel around on horseback.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps in the Attic</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Auchencairn - Old Smuggler's Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Phantom footsteps have been reported in the upper part of the building. As with other public houses, light switches have been known to turn themselves on and off and electrical equipment has activated with no human intervention. A shadowy figure was spotted in a bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">House Destroyer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Auchencairn - Trees (were nicknamed 'ghost trees', likely no longer standing), marking the site of Ringcroft Cottage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> February - 01 May 1695<br>
              <span class="w3-border-bottom">Further Comments:</span> Farmer Andrew Mackie would find his tied animals let loose overnight, until on morning he discovered one moved and carefully tied to the back of the house. After that, stones rained upon Mackie's farm. Initially light and small, the showers of stones soon became pebble-sized projectiles, bruising several witnesses. One visitor claimed to have seen a teenage boy dressed in grey who disappeared, and another spotted a disembodied white arm. The entity began to set fires within the cottage; eventually part of the wall collapsed, making the site uninhabitable. The haunting ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Haunted No More</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Auchencheyne, Thornhill - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Nothing is known about the ghost which haunted this bridge, other than it no longer does so.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanished</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Auchentrown - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Nothing is known about the ghost which haunted this bridge, other than it no longer does so.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walking Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Auldgirth - Cairn Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen in a bedroom, this lady terrified a four year old boy. Another entity in the house, this time a man, is said to be a lot less threatening.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beeswing (once known as Lochend) - Loch Arthur<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This small village has been named as a possible location to where the story of the Lady in the Lake occurred.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Haunted No More</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackstone - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Nothing is really known about the ghost which haunted this listed bridge, other than it no longer does so.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Janet Dalrymple</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bladnoch - Baldoon Castle ruins<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> On her wedding night, Janet went insane; she stabbed her husband several times and had to be dragged away by servants. Janet's husband survived, though she died shortly after - the date on which her shade returns to the scene of her outburst.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castle Douglas - Cuckoo Bridge, near the town<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The waters below are said to hide the body of a murdered infant, whose cries can sometimes be heard. A horrifying white shape has also been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castle Douglas - Greenlaw Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This old manor house is reputedly haunted by a grey, older woman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Threave-Castle.jpg' class="w3-card" title='An old postcard of Threave Castle in Scotland.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Threave Castle in Scotland.</small></span>                      <p><h4><span class="w3-border-bottom">Talking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castle Douglas - Threave Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of phantom conversation have been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monkey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cheuchbrae - Rockhall Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This primate was killed after the death of its master, Sir Robert Grierson, as no one could be found to look after the little critter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Clanyard-Bay-Cave.jpg' class="w3-card" title='A phantom piper and his hound wander the cave.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom piper and his hound wander the cave.</small></span>                      <p><h4><span class="w3-border-bottom">Piper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clanyard Bay - Cave along the coastline<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sound of the phantom pipe music is said to be heard by fishermen off the coast. One story says that, pre-ghostly form, the piper ventured into the cave with his dog not realising that it was home to fairies - the little people retained the piper but let his dog escape, but not before removing every one of the creature's hairs.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Marion Carruthers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clarencefield - Comlongon Castle Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This lady, now said to have a green hue, jumped from the tower rather than spend any more time with her husband.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bleeding Swan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Closeburn - Closeburn Castle<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to death in the Kirkpatrick family<br>
              <span class="w3-border-bottom">Further Comments:</span> Swans once brought peace and happiness here, until one was killed by a family member. Now, family death is heralded when a swan appears with a red-stained breast.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Piper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Corsock - Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> How this phantom headless piper can pipe without a head has yet to be answered.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Criffle Diamond</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Criffel - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1890<br>
              <span class="w3-border-bottom">Further Comments:</span> It was once said that ships coming up the Solway would be able to see the glint of a large diamond coming from Criffel. If the diamond were looked for, it could never be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elizabeth Buchan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crocketford - Galloway Inn (now Galloway Arms Hotel)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Elizabeth, who founded the fanatical sect of the Buchanites, was said to haunt this old inn.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 82</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=82"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=82"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
