
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in December">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > December</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>December (and Winter) - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cries of the Trapped</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> SE13 (Greater London) - Lewisham Station, & St Johns area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 04 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A crash in 1957, caused partly by fog, killed ninety people and injured over one hundred. It is their cries which can be heard on the anniversary on the accident. Another version of the story says the phantom screams date to the Second World War.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Emily</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Haworth (Yorkshire) - Haworth Parsonage, surrounding moor land, the path to Bronte waterfall, and former Weaver's Restaurant<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The famous abode of the Bronte Sisters, it is Emily who now haunts the grounds, her head bowed as in deep thought and meditation. She suddenly vanishes of anyone who comes too close. The apparition may appear several days either side of the date given.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footfalls Down the Stairs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Great Baddow (Essex) - White Horse Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The site of a murder on a distant 20th December has resulting in the ghostly footsteps coming down the attic stairs in this pub.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Neville's Cross (Durham) - Lane in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The lane is home to a phantom woman murdered on the site. She is said to be sometimes accompanied by a strange looking child who possesses an oversized head. During the nineteenth century she was said to appear in or on waggons and carts passing through the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Meeting Point</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishops Castle (Shropshire) - Stiperstones (aka Stiper Stones)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> It is a curious local legend that states that once a year all the ghosts in the UK meet at the stones, one assumes for their AGM. Another story says that if the stones ever sink into the earth, England shall be ruined; the Devil occasionally sits on the stones to speed up the process.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lower Boscaswell (Cornwall) - Pendeen Vau, aka Pendeen Fogou<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 or 22 December (Winter Solstice) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure in white holds a red rose in her mouth and turns and walks into the fogou once spotted. Some say to see her will bring misfortune, and anyone foolish enough to follow her... well, no one as yet has followed her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Battle of Edgehill Repeating</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Edgehill (Warwickshire) - Kineton<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 October (reoccurring), also 23 December 1642, lasting several nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A bloody battle of the English Civil War, the fighting repeated itself for several successive nights in 1642, in front of dozens of witnesses. More recently, sounds of battle have been reported, though the visuals are not forthcoming. The date of the more recent battle sounds is said to be 23 October.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sound of Hooves & Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kempston (Bedfordshire) - Kempston Manor (currently office space)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a family home, local legend has it that a child ran out of the manor to meet his mother and father who were returning for Christmas in a horse-drawn coach. The child was hit by the horses and died of his injuries. The anniversary of the event is marked by the reoccurring sounds of the tragedy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Farmer on Pony Back</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Egremont (Cumbria) - Tarpot area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This old man vanished without trace when returning home from an inn on a long forgotten Christmas Eve - both he and his ride are occasionally seen on the anniversary of the mystery.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Harp Player</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Stubley (Derbyshire) - Stubley Old Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing once a year, the shade of Fatima gently pucks her harp strings. A Roundhead has been reported less regularly, while a young girl skips around the fields outside.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Abigail in her Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> High Laver (Essex) - Main road through village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Abigail Masham spent her remaining years in the village. Her residence is now gone, and Abigail passes through the village in a carriage once a year to have a quick, sad look at where her house once stood before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Salford (Greater Manchester) - Kersal Cell<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The misty shade of a monk appears but once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anne Boleyn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hever (Kent) - Bridge over the Eden<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Boleyn's spirit crosses the bridge once a year. The area is also haunted by a farmer, robbed and murdered nearby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sound of Silver</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Evesham (Hereford & Worcester) - River<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Hidden in the river when the local abbey was closed for business in 1539, the silver bells still ring their song around Christmas.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gilbert</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Marden (Kent) - Road to Hawkhurst<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Gilbert, an eighteenth century highwayman who tried to rob a coach, was stabbed to death by one of the occupants after she recognised him as having killed her brother. The killing drove the woman mad. The scene was said to silently re-enact every Christmas Eve.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ringing Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Preston (Lancashire) - Site of old monastery (estimated to be A59 / Friargate area)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says that a church which once stood here sunk into the earth and its bells can be heard to ring on Christmas Eve.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Stony Hurst (Lancashire) - Cromwell Bridge<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil is reputed to cross the bridge once a year - if he looks you in the eye, he'll take your soul!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradley (Lincolnshire) - Bradley Woods<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Said by some to be a phantom nun and by others to be a woodsman's wife, local legend says this woman dressed in black will appear on Christmas Eve if one calls out 'Black Lady, Black Lady, I've stolen your baby!'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lost Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brigg (Lincolnshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> An old lady who left her house to beg for money to buy her Christmas lunch became lost in the snow and fog, freezing to death. Her ghost is sometimes seen, asking for directions home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Radley</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Halam (Nottinghamshire) - Valley southeast of village<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The buried village of Radley is hidden somewhere in this area, and long vanished church bells can be heard on Christmas Eve.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach & Four</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sandford on Thames (Oxfordshire) - Fields around the village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Driven by a headless horseman, a coach moving at breakneck speed pulled by four horses tear around fields near the village on the night before Christmas.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lost Bell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Shrewsbury (Shropshire) - Bomere Pool<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Several stories surround this pool. In one, a village once stood here, but the villagers mocked God, He sent forth a storm that flooded the area. The church bell can now be heard peeling once a year. Another story says the pool is home to a massive fish - it was caught only once, but escaped, taking the sword of the fisherman (or knight) with him. He now uses the sword to cut any net which falls near him. The final story says the pool is bottomless, and that when any attempt to drain the pool is made, the waters always return overnight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Holford (Somerset) - Normansland driveway<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing on the stroke of midnight, this coach pulled by a team of black horses circles the driveway before mysteriously vanishing again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Blunderhazard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barsham (Suffolk) - Between Barsham and Norwich<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once per year, just before Christmas, a ghostly member of the Blennerhassett family leaves the village in a coach pulled by headless horses. The phantom would travel to Hassett's Tower in Norwich before returning home before sunrise.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf777bles.jpg' class="w3-card" title='Roos Hall, Beccles, Suffolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Roos Hall, Beccles, Suffolk.</small></span>                      <p><h4><span class="w3-border-bottom">Headless Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beccles (Suffolk) - Roos Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Christmas Eve is marked by the arrival of a coach driven by a headless horseman and pulled by headless horses, which travel down the driveway and vanish as they arrive by the front door. Close by, an oak known as Nelson's Tree is reputed to have been used as gallows and is haunted by a woman in white, although it is not clear whether she was one of the tree's victims or mourns the loss of a loved one. In the hall itself, on a wall within in a bedroom cupboard, there is the imprint of the Devil's hoof branded into solid brick. Finally, another tale says there is a window at the hall which always opens itself, even if locked shut.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 128</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=128"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=128"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
