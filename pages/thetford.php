
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Thetford Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

 <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Thetford</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Thetford Ghosts, Folklore and Forteana</h3>
                     
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bear</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - A1066 at Snare Hill<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> June 1979<br>
              <span class="w3-border-bottom">Further Comments:</span> Several witnesses reported seeing a bear on the outskirts of the nearby forest.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gamekeeper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - A11, dual part of the road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 November 2006, between 23:45h - 00:15h<br>
              <span class="w3-border-bottom">Further Comments:</span> Driving to pick someone up, this witness claimed to have seen a ghostly figure on his car bonnet. The figure wore a light thorn coloured suit with a thin red pinstripe and had dark hair. The figure was said to be smiling. The driver turned to his passenger who was asleep, and when he looked back the ghost had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">1930s Motor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - A11, slip road from Thetford<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver and their passenger were stopped at temporary traffic lights. Both spotted a man wearing googles and hat in what they thought was a classic vehicle from the 1930s. As the witnesses drove away, they noticed the old car had disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chieftain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - Barrow known as Thet Hill, alongside the River Thet<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This redheaded warrior or chieftain reportedly stands near the barrow that may contain his remains.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Concealed House</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - Castle Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local story states that a mansion lies buried under the hill, filled with wealth beyond dreams. Another story says that the hill was created by the Devil, created by mud falling from his shoe. Walking around the hill seven times at midnight is said to summon him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor772tord.jpg' class="w3-card" title='Countryside around Thetford Warren, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Countryside around Thetford Warren, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">Leper with Two Dimensional Face Gibbering Horribly</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - Countryside near Thetford Warren<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mostly unknown, faceless man in July 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> Once the location of a leper hospital, this area is now reported to be the haunt of a figure with a strange, gibbering, unreal face, who terrifies witnesses. The tower house along the Brandon Road is supposed to once have been a leper house; one of the occupants has been seen in the area. A strange face has also been reported looking out from the first floor window of the warrner's house, though internally there are no floors. In July 2011 there was a report of a man observed wearing blue and cream coloured clothing, although he had no face, just black holes for eyes and a hole for a mouth.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">A Quick Drink</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - Devil's Punchbowl (mere)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This mere has an erratic water level; perhaps previous generations attributed the sudden disappearance of water to the Devil having a quick drink while passing by.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor7713a.jpg' class="w3-card" title='Grime&#039;s Graves, Thetford.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Grime&#039;s Graves, Thetford.</small></span>                      <p><h4><span class="w3-border-bottom">The Devil's Holes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - Grime's Graves, prehistoric flint mine<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The name 'Grime's Graves' is thought to have come about in the sixteenth century, Grime being a corruption of Grim, a pagan god that Christians associated with the devil.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - Site of the Priory of the Canons of the Holy Sepulchre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1937 (singing), monk seen in the summer of 1992, around 21:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> The monks could be heard singing in Latin, in the ruins of the building, followed by a single reading from an unknown text. In 1992 a group of nine teenagers walking through the grounds heard the jangling of chains or keys as they walked through the area. A few moments later a monk wearing sandals and a chain belt ran past them. The teenagers screamed and ran back towards town.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Disappearing Headlights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - Snare Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> A motorist reported seeing car headlights around a hundred metres ahead approaching him, although the lights disappeared after he briefly lost visual contact with them. There was nowhere for the car to have turned off, and it did not pass him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little George</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - St George's Nunnery - Nun's bridges<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Sixteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The seven year old George fell from a wooden horse in May 1559, cracking his skull on the wall of the bridge and dying from his injury. The young ghost haunted the area for many years (riding around on a headless wooded horse) before being exorcised.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor771tord.jpg' class="w3-card" title='The Bell Hotel, Thetford.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Bell Hotel, Thetford.</small></span>                      <p><h4><span class="w3-border-bottom">Landlady Betty Radcliffe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - The Bell Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed in 1750 by her lover, Betty now walks the corridors at night checking guests comfortable; guests may hear her heavy skirt swishing as she passes the doors and spot a flickering light like that of a candle. Other ghosts reported here include a young girl seen looking from the windows of the staff quarters, a dog lying in front of what would have been the original fireplace in the bar, jangling keys, a pair of old fashioned suede boots coming through the ceiling, and a monk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor548tord.jpg' class="w3-card" title='The Thetford Warren, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Thetford Warren, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">White Rabbit with Flaming Eyes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - The Thetford Warren (connected to golf course)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> While sounding like a Monty Python sketch, the white rabbit that has been seen running around the area has large flaming eyes and brings disaster to all that see it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6653.jpg' class="w3-card" title='Thetford Forest, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Thetford Forest, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">Eric</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - Thetford Forest<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> The spectre of an elderly gentleman is said to wander around the picnic site in the forest - it is reported that people have spoken to him before he vanishes into thin air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor722tord.jpg' class="w3-card" title='Thetford Forest, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Thetford Forest, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">Black Cat with Glowing Eyes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Thetford - Wooded areas<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> April 1985<br>
              <span class="w3-border-bottom">Further Comments:</span> A large cat with eyes that glowed red was reported in the forest.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 15 of 15</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
