

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Antrim, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northernireland.html">Northern Ireland</a> > County Antrim</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Antrim Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Antrim-Antrim-Castle.jpg' class="w3-card" title='A strange-looking coach with horses sinking into water.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A strange-looking coach with horses sinking into water.</small></span>                      <p><h4><span class="w3-border-bottom">Sinking Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Antrim - Antrim Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A coach pulled by a team of four horses which sunk into the pond drowning all on board is seen repeating the tragic accident once a year. In addition, a stone wolfhound which stands by the gateway is said to have once been flesh and blood - the dog gave a warning to an advancing sneak attack upon the castle before turning into rock. It now safeguards the Clotworthy family name on the condition it is never removed. Finally, the sound of heavy breathing can be heard coming from the ruined building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Antrim - Clotworthy House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1993<br>
              <span class="w3-border-bottom">Further Comments:</span> Investigating strange sounds after dark, a security guard spotted a white figure standing in a room. The guard closed the only door leading to the room to trap the figure, but the apparition vanished without trace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bloody Puddle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Antrim - Mussenden Temple library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A mysterious patch of blood occasionally appears on the floor of this building, evaporating away a few minutes later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hanged People</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Antrim Town - Corner of the Belmont Road and Cunningham Way<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A few residents to the area have spotted ghosts of people hanging from the trees. Local folklore says that this was once the location of a hanging tree.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Female Cyclist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Antrim town - Dublin Road (poss. British Road), towards the airport<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1991<br>
              <span class="w3-border-bottom">Further Comments:</span> While travelling to work, a man spotted a young girl wearing a long 1900's type dress cross the road on an old fashioned bicycle. When the witness reached the point where he had seen her, he realised that there were no gaps in the heavy hedge rows and that on either side of the road deep ditches leading into heavily ploughed fields made it impossible for anyone to cross at that point.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Operation Big Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballybogy (aka Ballybogey) - General area and surrounding countryside<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> A joint army and police team created 'Operation Big Cat' after two dead calves were found and a local man admitted releasing a puma and a panther from his private collection. Although there were dozens of sightings and livestock attacks, the cats were never caught.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ballycastle-Graveyard.jpg' class="w3-card" title='A ghostly black nun.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly black nun.</small></span>                      <p><h4><span class="w3-border-bottom">Black Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballycastle - Graveyard<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend says if you walk around Julia McQuillan's (aka the black nun) grave seven times clockwise, seven times anticlockwise and then place your hand through the hole in the grave, you will summon the ghost of the nun. McQuillan was a recluse who became renown after issuing seven prophesies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Isobel Shaw</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballygally - Ballygally Castle Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Lady Isobel jumped to her death from an upper window to escape a slow lingering death administrated by her husband. She now knocks on various doors before running away. Madame Nixon is named as another ghost to be found here and can be recognised by the rustling of her dress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hell Gate</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymena - Dundermot Mound<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Dundermot Mound is reported to be the location of a secret entrance to hell - occasionally it opens, releasing demons who take any witnesses straight to the underworld.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coachman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymena - Dundermot Mound<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom coach and four rides around the area. The ghostly driver will ask anyone he spots 'is the bridge at Glarryford still up?' - any answer the witness gives will ensure that they will die within a year (so I suggest looking at your feet and saying nothing).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymena - Road leading to the White Gates, Crebilly Road area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A robber escaping from a house rode into a thin piece of wire tightly pulled between the gateposts here, losing his head in the process. His decapitated shade is either seen or heard at Halloween, still on horseback.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Carrie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymena - Tullyglass Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Carrie is believed to have died in the tower around two hundred years ago when the Tullyglass was a mansion house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George Hutchinsons</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymoney - Main Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight, every Friday the thirteenth (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> With a large metal ball chained to his ankle, George 'Bloody' Hutchinsons walks one way down the street before turning back again. It is reported that anyone who can prevent him from completing his journey will dismiss the spirit forever.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pipe Smoker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymoney - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> A few ghosts are said to haunt this property. A couple of female forms flitter around, while a male pipe smoker and an authoritative man are also reported. Poltergeist-like activities have included banging sounds, doors opening, and footsteps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cleaners</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymoy - Ballymoy Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Saturday (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Invisible wraiths are said to sweep the castle clean each Saturday.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Savage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - The Brickyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Nineteenth century landlord John Savage slit his own throat after suffering what is thought to be a state of depression. His ghost was said to haunt the brickyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Helena Blunden</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - The Irish Linen Mill (currently a print shop)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> N/A<br>
              <span class="w3-border-bottom">Further Comments:</span> A known hoax which claimed a woman named Helena fell on the basement stairs and was dead before she hit the bottom step.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure in Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - Ulster Folk and Transport Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> Local press published a photograph of a spooky looking figure sitting in a train carriage, although the 'ghost' is most likely accidently created using a slow shutter speed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman Drinking Coffee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - Vicarage Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This female ghost walks down the street carrying a cup of coffee in one hand and a cigarette in her other.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Herdman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - Waterworks<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Shot in 1862, John Herdman is said to haunt the area of the Waterworks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadowy Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - York Road Railway Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s - present<br>
              <span class="w3-border-bottom">Further Comments:</span> A strange person has been reported sitting in the locked canteen at night. This may be the same figure glimpsed in the running sheds - during a bungled robbery, a man was either shot or beaten and subsequently died from his injuries. Night staff have also reported hearing disembodied footsteps in the area and on one occasion, having a fire extinguisher thrown at them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - 91 Beechmount Grove (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> June 1989<br>
              <span class="w3-border-bottom">Further Comments:</span> John Skillen released a book of his experiences in this house where he lived with his family. During their time here, the ghost tossed John against a wall and slapped his children. The family moved away after the attacks escalated.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Belfast-Above-Yorkgate.jpg' class="w3-card" title='A rather dramatic green UFO over Belfast.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A rather dramatic green UFO over Belfast.</small></span>                      <p><h4><span class="w3-border-bottom">Green Triangle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - Above Yorkgate shopping centre<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 November 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> Shoppers at the centre reported seeing a green triangular ufo cross the sky above them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Galloper Thompson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - Alexandra Park Avenue<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A member of the Thompson family who loved horses died and took to galloping along the Avenue after dark (the horse taken from the family stable and normally found exhausted the following day). The haunting ceased after the ghost, nicknamed 'Galloper Thompson', was caught in a bottle and tossed into the sea.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast - Arden Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October 1972<br>
              <span class="w3-border-bottom">Further Comments:</span> A brief myth which sprang up around Halloween, the streets were said to be the haunting ground of a black figure which could change size.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 72</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=72"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=72"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northernireland.html">Return to Northern Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
