

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Shropeshire Folklore, Ghosts and Strange Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/westmidlands.html">West Midlands</a> > Shropshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Shropshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Human Shape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A442 - Between Telford and Bridgenorth<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 11 March 2009, 20:00h, and 20 November 2021, 19:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> In 2009, halfway between Telford and Bridgenorth, both passenger and driver of a car spotted a black human shape wearing a long coat, on the side of the road. They thought the figure would step out in front of the car, but it disappeared before doing so. Twelve years later, another couple in a vehicle witnessed a dark figure wearing a long coat on the side of the road. The figure vanished as oncoming headlights appeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Acton-Burnell-castle-LClout.jpg' class="w3-card" title='A photograph of Acton Burnell Castle, copyright 2006 Lawson Clout.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A photograph of Acton Burnell Castle, copyright 2006 Lawson Clout.</small></span>                      <p><h4><span class="w3-border-bottom">Girl in White Lace</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton Burnell - Acton Burnell Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly girl dressed in white lace is said to haunt this ruined site. A student managed to photograph a strange misty 'face' in 2004 and reported hearing scratching noises. (Image copyright 2006 Lawson Clout)</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Maid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton Burnell - Acton Burnell house<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This spirit only appeared once, entering a bedroom and announcing 'Mass is at eight o'clock' before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Laid by the Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton Burnell - Devil's Causeway, between village and Cardington<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It was said Old Nick created this old road in a single night. He was said to appear to anyone using the road at midnight, taking the form of a man on a white horse (although he had the horns and feet of a cow). If the person encountering him had a good soul, they could pass unheeded. If they had led an evil life, the Devil would beat them to a bloodied pulp.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Demonic Frogs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton Burnell - Frog Well<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil and three imps are said to live here, manifesting in the form of frogs. While the imps are happy to be seen, Old Nick takes the form of a larger frog and spends most of the time hiding away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">RF398</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Albrighton - Cosford Aerospace Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The last remaining Avro Lincoln Bomber situated here is said to be haunted by a pilot seen in the cockpit, while a tape recorder left on board overnight picked up the sounds of a busy airport, though the hangar and the area were devoid of life.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Landing Craft</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Albrighton - RAF Cosford (now Aerospace Museum)<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1963<br>
              <span class="w3-border-bottom">Further Comments:</span> This ufo report was based on a story made up by two young people returning late from a leave pass. As time went on, the story grew, and it eventually became that an UFO had touched down near a hangar on the base, and was recovered and removed for reverse engineering.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hooded Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ash Magna - Ash Grange<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> A motorcyclist reported seeing a ghostly monk, his face covered by a hood, floating around thirty centimetres from the ground.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ash Magna - Road just outside of Ash Magna<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen several years apart by two brothers, a floating figure is reported to haunt a road just outside of this village.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hannah Phillips</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Astley Abbots - Main road through the village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Hannah died a few days before her wedding day, slipping into the river where she drowned. Her ghost, wearing a long dark dress, has been observed by motorists along the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mad Jack</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atcham - Mytton and Mermaid Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> John 'Mad Jack' Mytton was a local eccentric who lived with 2000 dogs and 60 cats while drinking six bottles of port per day. His money eventually ran out and he died in a debtors' prison. However, once a year on his birthday, his ghost is reputed to visit the hotel that has now taken his name.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tom Moody</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barrow - Willey Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Tom was paranoid about being buried alive and took elaborate steps to ensure it never happened. His phantom still walks the hall, often accompanied by one of his favourite hounds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sunken Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baschurch - Berth Pool<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> When the nearby church construction was delayed by a supernatural agency, the bells were said to have been cast into the pool. Neither horse nor ox were able to drag the bells out, so they remain to this day. The site has also been proposed as a burial site for Owain Ddantgwyn, named by some as King Arthur.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sunken Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baschurch - Church and Berth Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The original Baschurch church was going to be constructed on Berth Hill, but after each day's work, the stones were pulled down and thrown into a nearby pool. When the construction site was changed, no further delays were experienced.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baschurch - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, but may have been heard 17 October 2009, around 22:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Having hanged himself, this headless gentleman is reported to still drive his horse and trap around the neighbourhood. One witness heard a horse passing close to her late at night, but nothing could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cries for Help</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Benthall - Benthall Edge - area of former limestone pit<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A man carrying the week's wages for his mining crew was robbed and buried alive in this area in the late nineteenth century. Cries and screams are still reported coming from his living grave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Owd Scriven o' Brompton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berrington - Area once known as Banky Piece (no longer exists) and church<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa fifteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The wooden effigy on display in the church is known as 'Owd Scriven o' Brompton', and is said to have been a knight who encountered a lion in the area. The knight managed to slay the big cat, although was left with a large scar on his face, which is visible in the effigy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Caught in the Tread</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishops Castle - Lea Stone, located near the castle ruins<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The Lea Stone was created when the Devil picked the rock from his boot and tossed it aside.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Meeting Point</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishops Castle - Stiperstones (aka Stiper Stones)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> It is a curious local legend that states that once a year all the ghosts in the UK meet at the stones, one assumes for their AGM. Another story says that if the stones ever sink into the earth, England shall be ruined; the Devil occasionally sits on the stones to speed up the process.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bomere-Heath-Pool.jpg' class="w3-card" title='The sword of Wild Edric.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The sword of Wild Edric.</small></span>                      <p><h4><span class="w3-border-bottom">Edric's Sword</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bomere Heath - Pool (may no longer exist)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Legend has it that Wild Edric's sword was placed in a pool on the heath, ready for the day that Edric was ready to wield it once more. The blade was protected by a magical fish, which may or may not have used the sword to cut through fishermen's nets.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knight</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bomere Heath - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Two brothers stopped playing and ran and hid after spotting a ghostly grey knight standing at the top of their staircase. One of the brothers also reported waking at night to see a man smoking a pipe at the foot of his bed, while their mother said she had seen faces while on the staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Running Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bomere Heath - Small road between Bomere Heath and Walford, near the railway bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Winter 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver and his wife watched an elderly lady wearing grey with a headscarf run across the road ahead of them. The scene was weird, as the figure looked wispy, and her clothes appeared to be moving slower than the woman herself. Stranger still, while the driver had watched her cross the road from the left hand side, his wife saw her cross from the right.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drover</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Boningale - Horns of Boningale Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A drover murdered in a fight is reputed to haunt this inn. An energy-conscious poltergeist is also said to switch lights off during the night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgnorth - Boycott Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Walter is said to be the ghost of a former landlord who now haunts the pub.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridgnorth - Croft Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A mystery woman in black is said to haunt the dining room. Bells have been heard ringing in other parts of the hotel after dark.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 169</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=169"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=169"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/westmidlands.html">Return to West Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="View West Midlands records" style="width:100%" title="View West Midlands  records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>East Midlands</h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
