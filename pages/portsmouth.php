
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Portsmouth Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southeast.html">South East England</a> > Portsmouth</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Portsmouth Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fox Terrier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - 44 Laburnum Grove<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1910<br>
              <span class="w3-border-bottom">Further Comments:</span> Elliott O'Donnell wrote that a medium who receded here believed her home to be haunted by several cats, which were mistreated by a former owner of the property. The medium also detected the presence of a fox terrier.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Portsmouth-Blue-Posts-Inn.jpg' class="w3-card" title='The murdered sailor returned.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The murdered sailor returned.</small></span>                      <p><h4><span class="w3-border-bottom">Murdered Sailor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Blue Posts Inn (destroyed by fire in 1870)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1870<br>
              <span class="w3-border-bottom">Further Comments:</span> This inn was haunted by the ghost of a sailor murdered therein. His corpse was secretly buried in the courtyard, though in 1938 a gravestone was placed in the area thought to house the body.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jumping Keyboard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Cash Converters, London Road, Fratton<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> Two workers in Cash Converters reported seeing an analogue synthesiser fly three metres across the shop floor, hitting a CD rack. The former owner of the keyboard told the manager she had sold the item because of experiencing similar problems at home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Frog Fall</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Copnor area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa July 1954<br>
              <span class="w3-border-bottom">Further Comments:</span> Many tiny frogs were said to have fallen from the sky during heavy rainfall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Buckingham</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Former Greyhound public house, now a private residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The site where John Felton assassinated the Duke of Buckingham was widely believed to be haunted by the tragic figure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George and Emily</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Groundlings Theatre, Portsea<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghosts of two children, Emily and George, are said to be among the nine (or ten) phantoms at home within this Georgian Theatre.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jack Aitken</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Harbour area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Windy weather<br>
              <span class="w3-border-bottom">Further Comments:</span> Hanged for attempting to commit arson in a naval dockyard, Aitken's body was then displayed in a gibbet - the chains of which can still be heard clinking in the wind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">First World War Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Hayling Avenue<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1930s<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman walking down the road walked by a detachment of soldiers travelling in the opposite direction. She noticed they were wearing an old style of uniform. It was only when she looked back and realised the soldiers had vanished did she know she had witnessed something strange.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shaking Bed</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - HMNB Portsmouth, HMS Nelson, Saumarez block (now demolished)<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> September 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness reported waking up after the room fell cold. He could feel something sitting on his legs, but nothing could be seen. After several minutes, the bed began shaking violently, and the witness felt his legs being lifted slightly, before the top half of his body was also lifted at a forty five degree angle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flashing Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - HMNB Portsmouth, Leviathan Block<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-2013<br>
              <span class="w3-border-bottom">Further Comments:</span> It was reported that journalists working on the Navy News newspaper would see flashes of light emerging from the corridor outside their office. An uncomfortable feeling would accompany the light.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Buster Crabb</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Keppels Head hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One of two hotels claiming Crabb spending his final night here, the Navy frogman is said to haunt the building. Crabb vanished while attempting to spy on a Soviet warship which docked at Portsmouth during the Cold War.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Clothes Thrower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Private house along Agincourt Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1992<br>
              <span class="w3-border-bottom">Further Comments:</span> At its peak, the entity in the house pushed an occupant down the stairs. It also scattered clothing around rooms and snatched items from people's hands. A blessing performed at the house was said to make things worse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Landing Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Private residence, Buckland Park area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1986<br>
              <span class="w3-border-bottom">Further Comments:</span> The poltergeist activity in this home included items being thrown and pictures falling from the wall. A figure observed on the landing may have also been seen walking up the stairs.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Scratching</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Private residence, Copnor area<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2001-2009<br>
              <span class="w3-border-bottom">Further Comments:</span> A homeowner was forced to move after experiencing several years of scratching noises, footsteps, and having the duvet removed from the bed during the night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chair Thrower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Private residence, Cosham area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Figures could be seen walking through the kitchen into the larder, knocks would rattle on the doors, and items would be thrown or occasionally hidden before turning up in unusual places.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Navy Uniform</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Private residence, Cumberland Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990-1992<br>
              <span class="w3-border-bottom">Further Comments:</span> A family living in a property here were subjected to kitchen equipment moving unaided, exploding lightbulbs and the smell of cigars, although no-one smoked. A child would talk to a male figure wearing navy uniform with cuts over his face.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Private residence, Powerscourt Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1994 - 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> A family ultimately decided to move from this property after five members encountered the apparition of a Victorian woman who would manifest in the corner of a bedroom. Poltergeist activity also resulted in items being moved around the house, disembodied footsteps, and a mirror thrown at two people on the stairwell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chair Thrower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Private residence, Stamshaw area<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1993<br>
              <span class="w3-border-bottom">Further Comments:</span> Chairs would repeatedly be thrown from a table while the occupant cleaned the kitchen floor. Pictures were also propelled across the room. An eerie feeling could be felt in the back bedroom, as if that part of the building did not belong to the front of the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Evaporating Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Railway bridge, St Mary's Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2002<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver slowed to take a better look at a man who stood on the side of the bridge which did not have a footpath. The driver thought that the man did not 'look right', which was proved correct as the driver watched the figure evaporate in the rear view mirror.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pool of Blood</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Royal Portsmouth Hospital (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s-1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Local folklore says that the operating theatre had a pool of blood on the floor which could never be cleaned, no matter how hard the staff tried to remove it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Buster Crabb</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Sally Port hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One of two hotels laying claim to Crabb spending his final night here, the Navy frogman is said to haunt the building. Crabb vanished while attempting to spy on a Soviet warship which docked at Portsmouth during the Cold War.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered People</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - Site of the White Garter public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> When the White Garter was pulled down, dozens of bodies were found under the building - it is thought that a landlady and a male 'friend' murdered and stole goods from many of their guests. The ghosts of the murdered people are restless and have been seen around the public house or felt as they pull the sheets off beds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - St Agatha's church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1954-1970<br>
              <span class="w3-border-bottom">Further Comments:</span> While the church was used for storage, a rumour existed of a ghostly figure which haunted the site. More recently, a local legend speaks about ghostly nuns in the area, although there is nothing to suggest there was a nunnery here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dark Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - St Ann's Church, Dockyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2018<br>
              <span class="w3-border-bottom">Further Comments:</span> Having worked in the building before many times without incident, a member of the chaplaincy team had two fleeting glances of a dark figure standing on the balcony, looking over the edge as if watching the witness. The figure was described as curious and unthreatening.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Looking for Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portsmouth - St Mary's Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1969<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly woman in a dressing gown was once said to wander the hospital looking for her baby, who had died of unknown complications.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 28</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=28"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=28"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southeast.html">Return to South East England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
