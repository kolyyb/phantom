


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Giant Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Giants</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Giants and their Legacies from around the Isles</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth (Dyfed) - General area of Penparcau<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This headless dog was said to have once belonged to a young giant who ran so fast that he decapitated the dog by pulling too sharply on the leash.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant's Treasure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Addleborough (Yorkshire) - Barrow in the area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The giant's treasure slipped from his grasp as he crossed the area, the hoard sinking into the ground and becoming covered in rocks. The giant had to leave the treasure behind, but it was written that a mortal man could recover the wealth if a fairy in the form of a chicken or an ape appeared; the man would have to reach out and grab the treasure without making a sound, and if he failed to do so, the fairy would never appear to him again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sound of an Army</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alton Barnes (Wiltshire) - Adam's Grave, aka Walker Hill, aka Walker's Hill, aka Walkers' Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer, 1965 or 1966<br>
              <span class="w3-border-bottom">Further Comments:</span> Close to the hill, Miss Muriel Cobern heard dozens of horses galloping, as if an invisible army were passing. The sound suddenly stopped as she fled the area. The site is also said to be the resting place of a giant, who will awaken if the hill is run around seven times.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baltimore (County Cork) - Fastnet Rock<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Rock still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Ireland's most southerly point, Fastnet Rock was thrown into position by a giant from Mount Gabriel (Schull).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/1Cader-Idris.jpg' class="w3-card" title='An old sketch of Cader Idris.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old sketch of Cader Idris.</small></span>                      <p><h4><span class="w3-border-bottom">Idris</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmouth (Gwynedd) - Cader Idris (aka Caldair Idris)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Idris was said to be a wise giant, who may have had links with King Arthur. People would avoid the area at night, convinced that fairies would drive them mad. Another legend says that the site was the hunting ground of Gwyn ap Nudd (Ruler of the Otherworld) who searched the area with a pack of Cwn Annwn, looking for souls to take back home. Finally. Strange lights are said to appear around the summit for the first few days of a new year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown Objects</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barrasford (Northumberland) - Stones (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Last stone removed nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Before being removed by farmers, the standing stones which stood around the area were thought to have been thrown by giants engaged in combat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blacksmith</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Batcombe (Somerset) - Burn Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A giant blacksmith is said to appear if you call out his name while standing on this hill.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fighting Giants</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bennachie (Aberdeenshire) - Standing Stone<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> A monolith found here features a larger-than-life handprint and footprint - it is said that the prints belong to two giants who fought over the love of a woman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Church Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Binsey Fell (Cumbria) - General area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: misty nights<br>
              <span class="w3-border-bottom">Further Comments:</span> The Fell came into being when a giant accidentally dropped an apron full of rocks being used to build a church (to redeem himself from his life of sin and violence). A large ghostly figure wearing an apron is said to appear when the mist grows thick.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant's Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birmingham (West Midlands) - Warstones Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> A giant who lived at Birmingham castle was killed when the Giant of Dudley threw a stone at him (launched from Dudley!). The rock also demolished the castle. The stone was erected as a memento, and the lane named after the War Stone used as a weapon. The rock is now on a plinth within the cemetery.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Giant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackgang Chime (Isle of Wight) - Cave in the area (no longer present?)<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The chime was once a lush area, rich in vegetation and the hunting ground of a giant who feasted upon children. A holy man cursed the giant and the area, ensuring nothing would ever grow there again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Magical Giant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bosporthennis (Cornwall) - Croft<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Said to have taken place every 01 August (when giant was alive)<br>
              <span class="w3-border-bottom">Further Comments:</span> A giant who lived nearby was said to use this site once a year for magical rites and was happy for local people to watch.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Goram and Vincent</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol (Somerset) - Avon Gorge<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> To prove themselves worthy of the lady Avona's love, the giant brothers Goram and Vincent raced to dig a ditch to drain a lake. Goram started work but fell asleep after digging the Hazel Brook Gorge, while Vincent dug the Avon Gorge that drained the unwanted lake, thus winning Avona's heart.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Goram's Remains</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bristol Channel (Somerset) - Flat Holm and Steep Holm<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> After losing a girl and a bet to his brother Vincent, the giant Goram threw himself into the Bristol Channel. There he petrified, his head and shoulders forming Flat Holm and Steep Holm.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brockhampton Green (Dorset) - Large stone (no longer present)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend says a large boulder that could once be found here was thrown into position by a giant. Another story says that twenty men moved the stone and tried to throw it at a giant. The stone has since been removed or broken up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fight</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bundoran (County Donegal) - Close to an unidentified stream in the area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The local giant fought another giant who wandered into his land. The local giant managed to kill the newcomer, who, it transpired, was his son.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mysterious Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Callanish, Isle of Lewis (Outer Hebrides) - Callanish standing stones<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> It is still argued to what extent this stone circle represents the heavenly bodies above, and what its ultimate purpose was. One legend says that the thirteen stones were giants who refused to become Christians - they were petrified for the sin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wales2887.jpg' class="w3-card" title='Cardiff Castle, Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cardiff Castle, Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cardiff (South Glamorgan) - Cardiff Castle<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Coach last seen 1868<br>
              <span class="w3-border-bottom">Further Comments:</span> A coach is reportedly heard when a member of the Hastings family is due to die. At the castle it was heard by John Boyle, the night his cousin the Marquis Hastings died. Other ghosts reported to haunt the castle include the second Marques of Bute (who walks through several walls), a faceless woman in a long skirt who is known as Sarah, and a three metre tall giant who walks around the park.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John of Gaunt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carn Brea (Cornwall) - Stones on nearby hill<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Stones still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The giant John would often throw boulders at neighbouring giants, who would toss them back at him - hence, the hillside is covered in large rocks. The giant's remains are said to be buried somewhere on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crushed Friend</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carn Galva (Cornwall) - General area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A giant who once lived here (occasionally named as Holiburn of the Carn) was visited by a human friend. After a few games of quoits, the human had to leave. The giant patted his friend's head in a playful way, unfortunately forgetting his own strength - his fingers pieced the human's head, killing the man instantly. The giant died of a broken heart seven years later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor3610.jpg' class="w3-card" title='The Rude Man of Cerne, aka Cerne Abbas Giant.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Rude Man of Cerne, aka Cerne Abbas Giant.</small></span>                      <p><h4><span class="w3-border-bottom">Chalk Giant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cerne Abbas (Dorset) - Hillside<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> One story says this location is where a visiting Danish giant fell asleep, exhausted after killing dozens of local men. The remaining villagers took the opportunity to sneak up and cut off his head, drawing a chalk outline around the body to ward off other giants. The chalk figure is also considered a fertility symbol, and anyone who 'sleeps' on the figure is believed to be blessed (or cursed) with many children.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cerne Abbas (Dorset) - Stone on Bockhampton Green<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Two origin myths surround this stone. One says the rock was thrown into position by an angry giant, while the other states the rock was rolled into position by villagers attempting to move it up the hill to use as a weapon against the giant.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant Grave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cheselbourne (Dorset) - Round Barrow<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The mound found here is said to be the grave of a giant who dropped dead after losing a game of 'throw the boulder the furthest'. Two stones nearby are said to mark the place where the giant and his (winning) friend stood; when the cockerel crows, the stones move.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clonakilty (County Cork) - Knockatlowig (stone row)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The largest stone found here is said to have been thrown into position by a giant standing a couple of kilometres away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant's Droppings</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cressage (Shropshire) - The Wrekin<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Angry at the town of Shrewsbury, a giant gathered a mound of earth and set off to dam up the River Severn. After the giant became lost, he approached a man heading home towards the town. The local lied and managed to convince the dim-witted villain it would take months to reach the town. The giant sighed and dumped the earth where he stood, forming the Wrekin.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 98</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=98"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=98"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
