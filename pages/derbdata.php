
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Derbyshire Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastmidlands.html">East Midlands</a> > Derbyshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Derbyshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Alderwasley-Hill.jpg' class="w3-card" title='Dwarves, while not common in the UK, pop up occasionally.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Dwarves, while not common in the UK, pop up occasionally.</small></span>                      <p><h4><span class="w3-border-bottom">Dwarf with Pointed Hat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alderwasley - Hill just past Alderwasley<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The witness to this encounter stated that they once bumped into a four foot high figure wearing a green pointed hat feeding the plants.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alderwasley - St Margaret's Chapel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Though disused, locals have reported singing and bell ringing from this aging chapel.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfreton - Alfreton House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> The sound of footsteps on wooden floorboards walking down an empty corridor was heard, even though the area is carpeted. The haunting is said to date back to the 1940s.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cloaked Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alfreton - Doehole Lane, Ashover<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver was forced to brake hard as he and his passenger watched a transparent cloaked figure with a hood covering its head cross the road. The figure vanished before reaching hedgerow on the verge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dancing Ladies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alport - Nine Stones<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This set of four (not actually nine) stones, also called the Grey Ladies, was thought to come alive at midnight (or midday) and dance on the nearby moor. Others have said that hundreds of fairies come to the stones to dance and party.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Boggart</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arbor Low - Arbor Low Stone Circle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Locals talk of a boggart that haunts the area of the circle - whomever spends time here after dark risks upsetting the entity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashbourne - Hanging bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wayne Anthony, in Haunted Derbyshire and the Peak District, writes that the bridge is said to be haunted by two ghosts. A headless figure is sometimes seen standing on the bridge, while another ghost is seen leaping off the edge into the River Henmore.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Twelve Pallbearers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford in the Water - Shady Lane, near Thornbridge Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> These twelve men carrying a coffin are said to be headless. The coffin is also empty, the space reserved for any witnesses to the spectral sight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Towndrow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashover - Church and churchyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1890<br>
              <span class="w3-border-bottom">Further Comments:</span> The headless entity spotted in this location was believed to be the Mrs Towndrow who was murdered in 1841 by her husband John shortly before he killed himself. A local legend states that of you thrice circumambulate an empty stone coffin found in the churchyard before sitting in it, you can hear the restless dead.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Jessops the Third</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashover - Overton Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1956<br>
              <span class="w3-border-bottom">Further Comments:</span> The wife of a pastor moved out of this hall complaining it was haunted. At the time locals believed the ghost was the third wife of Mr Jessops, a former owner of the property, which appeared to enjoy walking over the grassy forecourt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Only at Dawn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashover - The Turning Stone<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> At certain times of the year (but when, no one knows for certain) this large stone turns itself over when the cockerel crows.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jim Marlowe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bakewell - Castle Hill (boarding house)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Every Friday (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Marlowe was a butler who shot himself one Friday night to escape his nagging wife. His footsteps reputedly can be heard once a week leaving the pantry and ascending the staircase to the room in which he died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-34.jpg' class="w3-card" title='An old postcard showing Haddon Hall in the village of Bakewell.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Haddon Hall in the village of Bakewell.</small></span>                      <p><h4><span class="w3-border-bottom">Dorothy Vernon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bakewell - Haddon Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wayne Anthony, in Haunted Derbyshire and the Peak District, writes that the ghost of Vernon has been sighted on the steps which now bare her name, running down them as if being chased. Other ghosts on this site include a blue woman, a young boy and a phantom monk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Talking Tree</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bakewell - Hassop Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A beech tree once stood on the grounds of the hall and would whisper the name of the true owner of the property. The hall is also reputed to be haunted, though nothing is said of the form the ghost takes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bakewell-Magpie-Mine.jpg' class="w3-card" title='A phantom miner.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom miner.</small></span>                      <p><h4><span class="w3-border-bottom">Old Miner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bakewell - Magpie Mine<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> A survey team in the mine spotted a man holding a candle who vanished down a shaft. A photograph taken by the team reportedly shows a figure standing on top of a deep pool of water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bakewell - St Anselm's School<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown - mid-twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> During a social event at the school, some of the guests were photographed by a master on steps in the garden. When the photograph was developed, a boy in Victorian clothing could be seen standing with the guests, though no one could recall him being there at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Farmer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bamford - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom farmer holding a lantern is said to haunt this bridge, though he has not been seen for around one hundred years. Another witness encountered a ghostly animal, 'neither dog or ram', which blocked his path.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Barberbooth-General.jpg' class="w3-card" title='A phantom Collie-like dog.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom Collie-like dog.</small></span>                      <p><h4><span class="w3-border-bottom">Black Collie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barberbooth - General area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom dog stands as tall as a very large collie and is said to belong to the fairies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Colour Shifting Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barlborough - Barlborough Hall (currently Mount St Mary's College)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nights of the Full Moon (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Wayne Anthony, in Haunted Derbyshire and the Peak District, writes that a phantom woman, reported as being grey blue or white, haunts this Elizabethan hall. A room in the upper part of the building is home to a ghostly blood stain belonging to a murdered priest - the story says it took many decades to be cleaned up, and even now it periodically returns. The hall was also once home to Robin Hood's long bow, now kept at Renishaw Hall, and local tradition has it that Robin and Marian were married in a nearby church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barlow - Private farm near Barlow<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s - present<br>
              <span class="w3-border-bottom">Further Comments:</span> A screaming woman holding a child was said to haunt this location, although she has not been seen for many years. A former worker at the site said he had seen phantom old man in a corner of a room and had experienced poltergeist activity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sunken Village</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baslow - Road from village heading towards Sheffield, was fenland known as Leachfield<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A village which once stood here was said to have quickly sunk into swamp while watched by an occupant who stood on a nearby hill.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Turning Rock</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baslow Edge - The Eagle Stone<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Stone still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Wayne Anthony, in Haunted Derbyshire and the Peak District, writes that this large rock will sometimes slowly turn itself over when it hears the cock crow.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hob the Boggart</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beeley - Hob Hurst's House (Bronze Age Barrow)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This helpful entity was renowned for making boots for local people he liked.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Darting Large Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beeley - Moor<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2016 (cat), phantom horseman on first full moon in March<br>
              <span class="w3-border-bottom">Further Comments:</span> A cat, reportedly the size of a small horse, darted out in front of a car. The previous year, a panther was observed on the moor, from which the witnesses fled. The moor is also said to be haunted by a phantom horseman who appears once a year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cloaked Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belper - Car park by the Mill House public house<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 09 March 2014, around 17:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> A man in black old fashioned clothing with a cloak and hat was spotted standing by the fence of this car park. The bottom half of his face was covered by a scarf.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 323</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=323"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=12&totalRows_paradata=323"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastmidlands.html">Return to East Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="ViewWest Midlands records" style="width:100%" title="View West Midlands records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
