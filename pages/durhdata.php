

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Durham Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northeastandyorks.html">North East and Yorkshire</a> > Durham</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Durham Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aycliffe - A167 north of town<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century, though sightings may go back to eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> While the clothing of the woman varies (from wearing a raincoat to a wedding dress), the figure is always white. In true 'phantom hitchhiker' style, the woman had been picked up by passing drivers, only to vanish a few miles down the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aycliffe - Near the church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A father walking with his brother and son spotted a female entity wearing Victorian dress. The figure did not have any feet and was not in contact with the ground. The entity looked at the three people before moving off 'faster than a greyhound'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Barnard-Castle.jpg' class="w3-card" title='An old postcard of Barnard Castle in County Durham.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Barnard Castle in County Durham.</small></span>                      <p><h4><span class="w3-border-bottom">Lady Ann Day</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnard Castle - Castle and River Tees<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple of versions of this ghost story exist. One has Lady Ann Day being tossed into the River Tees by her murderer, with her ghost of Day reliving her deathly plummet. The second version says Day died of natural causes, and her ghost would be seen at night wearing a red cloak as she crossed the river, possibly sitting on the shoulders of her husband.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roger</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnard Castle - Friar House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Likely pre nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Roger, the husband of Ann who haunts the castle, also returned as a ghost and took up residence at Friar House. The occupants did not appreciate their phantom lodger, so Roger was banished by priests.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Lids</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnard Castle - HMYOI Deerbolt (youth offending prison)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2014<br>
              <span class="w3-border-bottom">Further Comments:</span> During a month in 2014 it was reported that lighting was switched off and on again and on one night, two bin lids were found spinning on the floor. The area was locked down as member of staff who spotted the lids feared an offender was loose, but CCTV footage showed the lids flying off the bins unaided.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnard Castle - Old Well Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly child is reputed to haunt one of the rooms, its presence upsetting one guest.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Death's Scythe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnard Castle - St Mary the Virgin's church<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to the death in the family<br>
              <span class="w3-border-bottom">Further Comments:</span> The stone statue of Death stands above the grave of George, son of Humphrey Hopper. To see Death's scythe wave is an indication that the witness or a close family member will soon die.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beamish - Beamish Hall, and Starling Bridge crossing Beamish Burn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> This woman suffocated after hiding in a trunk to avoid a prearranged marriage. Her mournful shade has been seen in both the aforementioned places, most recently by a vet who spent a night in the haunted house to raise money for charity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Basket</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beamish - Home Farm<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> An old basket found between joists in the kitchen was removed, but within 24 hours it returned itself to its original position. The removal/return activity carried on for a few weeks before the basket gave up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Presence</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beamish - Sun Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This inn was moved brick by brick from Bishop Auckland in the 1970s, and with it came a presence felt in the back room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Persian Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birtley - Private house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2012, daytime<br>
              <span class="w3-border-bottom">Further Comments:</span> While making coffee the occupant watched a grey Persian cat walk across the floor and disappear into the living room. No doors or windows were open, and the cat could not be found. The witness later found out that a cat matching the description had once lived there but had died a few years previous.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wailing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birtley - Private residence belonging to a miner<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1892<br>
              <span class="w3-border-bottom">Further Comments:</span> It was said that other villagers would visit the property at night to listen to the strange wailing which would come from within.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Luck of Muncaster</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop Auckland - Binchester Hall (original building no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1833<br>
              <span class="w3-border-bottom">Further Comments:</span> A cup known as the Luck of Muncaster was stored at the hall, which was said to bestow blessing on the family. It did not stop the demolition of the hall, however, and the current whereabout of the cup is unknown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hazy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop Auckland - Four Clock's Centre (former church), Newgate Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> First half of 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> CCTV caught a hazy figure on camera, while another witness spotted a woman in a hat. Footsteps have also been heard in an empty room and doors have locked themselves from the inside.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smashing Glass</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop Auckland - Pollards Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 August 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> A man whose family once ran the pub was visiting the site. As he told the story of how glasses would move unaided, his own glass lifted off the table and smashed against a wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/0601-dragon.jpg' class="w3-card" title='A dragon carved in wood.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A dragon carved in wood.</small></span>                      <p><h4><span class="w3-border-bottom">Limbless Worm</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishop Auckland - Wooded area<br>
              <span class="w3-border-bottom">Type:</span> Dragon<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A creature, said by legend to be a long, hostile worm, but likely to have been a boar, inhabited an oak wood and would attack man and beast. It was slain by a member of the Pollard family.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Guardians</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bishopton - Castle Hill<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Though the castle is long gone, the area is still a place where fairies hide - anyone trying to steal earth or soil from the land is warned off by whispering voices.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Charlie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bournmoor - Dun Cow public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a former miner, Old Charlie has his own stool to keep him quiet. Another phantom, with the less affectionate nickname Crazy Lady of Bournmoor, was the wife of a Napoleonic era officer who never returned from combat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Emma and Edward</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bowes - Ancient Unicorn Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Emma and Edward were the children of different innkeepers who fell in love - Edward died of fever and Emma pined away soon after. Their ghosts are reputed to be regulars at the Inn, as is another young boy who haunts the cellar and a man in a bowler hat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Haired Head</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnopfield - Railway track running through the village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1932<br>
              <span class="w3-border-bottom">Further Comments:</span> A floating face was reported by several men along the line. The entity had a twisted face and white hair, and its prior to its materialisation, metallic clanging would be heard. The ghost was eventually named as a platelayer who had died after being hit by a coal wagon in 1879.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Byers Green - (Former) Marquis of Granby public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century?<br>
              <span class="w3-border-bottom">Further Comments:</span> This former pub is said to be haunted by a tall man. A plumber is also said to have fled the site after watching a stool move across the room unaided.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anne Walker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester le Street - Exact location not recorded<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1632<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Anne Walker appeared to a miller, telling him the location where her body could be found. The miller ignored the ghost, but after she appeared several more times, he informed the authorities. Her body was discovered in a coal pit, as the ghost had described. One of her family and a family friend were later arrested and executed for her murder.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Lumley</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester Le Street - Lumley Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2005. 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> The 'Lily of Lumley', as the lady is also known, was killed by several monks after refusing to agree with their religious beliefs. When Sir Ralph discovered what had happened to his wife, he had the monks executed. The monks are sometimes seen walking in file around the area of the castle, while Lady Lumley walks within the castle's corridors and grounds. An Australian cricket player reported seeing the monks in 2005, while in 2017 a witness claimed to have photographed two ghostly faces in the windows.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester Le Street - River Wear<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, likely to be early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly woman was said to haunt this stretch of waterway.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Madam Lambton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chester le Street - South Biddick Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This shade has been reported drifting around the hall.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 155</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=155"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=6&totalRows_paradata=155"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northeastandyorks.html">Return to North East and Yorkshire</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North West England</h5>
   <a href="/regions/northwest.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/northwest.jpg"                   alt="View North West records" style="width:100%" title="View North West records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
