
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Hampshire Ghosts and Weird Stuff from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southeast.html">South East England</a> > Hampshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Hampshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Car Crossing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A4 - Road west of Hungerford<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom car crosses the road on a rise, vanishing as it does so.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abbots Ann - Poplar Farm Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure was once seen behind the bar and is blamed for slamming doors when no other cause is apparent. Another entity takes the form of a black dog which patrols both inside and outside the pub. The dog is thought to be protecting a secret passageway.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Running</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldershot - Alma Lane, between town and Barracks<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s and 1972<br>
              <span class="w3-border-bottom">Further Comments:</span> The disembodied running heard along this lane has been attributed to a messenger taking the news of the victory at Waterloo to officers based at Aldershot - he was attacked and killed by robbers en route.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Aldershot-Basingstoke-Canal.jpg' class="w3-card" title='There are parallels between alien abduction and fairy abduction.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> There are parallels between alien abduction and fairy abduction.</small></span>                      <p><h4><span class="w3-border-bottom">Little Green Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldershot - Basingstoke Canal<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 August 1983, 01:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> An elderly man who set up here for a spot of night fishing was invited into a UFO by two little men in tight green outfits, with visors covering their faces. They shone a light at him before saying that he was 'too old', promptly returning him to his fishing rod.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldershot - Cambridge Military Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A legend says that the phantom woman here was a Boer War nurse who committed suicide after accidently giving her lover an overdose of Laudanum. A nurse and two other members of staff spotted her around 1995; a solid silhouette which vanished into thin air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bulletproof</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldershot - North Camp, Aldershot Garrison<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> March - April and September 1877<br>
              <span class="w3-border-bottom">Further Comments:</span> Spring Heeled Jack was blamed after several sentries reported being attacked by a figure with cold, corpse-like hands. Bullets fired at the attacker had no effect.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ball of Smoke</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldershot - Private residence, Cambridge Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A former resident said that he was woken by a thump at the bedroom door at 01:30h. Opening the door to investigate, something flittered around the banister out of sight, like a child playing hide and seek. As the witness reached the corner of the banister, they briefly spotted a large ball of smoke before it once again darted around a corner. After a period of some time, a female witness watched through frosted glass a figure trying to open the kitchen door, although when investigated, no one could be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Bag Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldershot - Road between Tweseldown and Aldershot<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom homeless woman was said to have been run over and killed along this road. She may have been seen in 1990, the car full of witnesses not being aware she was a phantom until long after the event.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alresford - Cheriton battlefield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 29 March, every four years (last in 2020)  (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> These troops are said to be doomed to return to their former battleground.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alresford - Ruined Manor House<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000<br>
              <span class="w3-border-bottom">Further Comments:</span> Two witnesses observed a figure in a grey or silver once piece suit, with a visor over the face, hovering over the tree line. As the witnesses left the area, the figure drifted parallel with them for a bit, before turning away and silently disappearing into the distance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burnt Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alton - Crown Hotel, High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom dog reportedly haunts the hotel, where it was kicked to death by a drunken customer. Some say that a white (or grey) woman also glides around inside the building; the girl was a kitchen maid murdered in the hotel. Finally, the ghost of an old man haunts room number three.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fanny Adams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alton - Flood Meadows and general area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1867 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered by Frederick Baker in 1867, the ghost of young Fanny Adams still plays in the area where she died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Civil War Skirmish</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alton - St Lawrence's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A Royalist Colonel was trapped in the church by a detachment of Roundheads - he and several of his enemy were killed during the short fight. The small battle can still be heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/OI-111.gif' class="w3-card" title='An old woodcut showing a rather fat pig with an oversized ring through the end of his nose.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut showing a rather fat pig with an oversized ring through the end of his nose.</small></span>                      <p><h4><span class="w3-border-bottom">Pig</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Andover - Church<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1171<br>
              <span class="w3-border-bottom">Further Comments:</span> The priest here was hit by lightning - the congregation watched as a small pig appeared and ran about his legs and feet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Andover - White Hart Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure apparently haunts the upper regions of the building (particularly Rooms 14 and 20), though at least one sighting has her wearing dark green. The bar area is home to two misty figures.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Faceless Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - Barge Inn, formally Goat and Barge Public House (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s, 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> The landlord's wife and a customer both watched a phantom faceless woman walk past the bar. In another incident, a member of the public was knocked off his feet and another kicked in the knee as an invisible force rushed by them. Cries and screams would sometimes be heard late at night. More recently, workers in a building on the site of the pub have reported hearing disembodied footsteps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - Hackwood House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1862<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen emerging from one of the walls of a bedroom, this grey lady has also been seen on the staircase in the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Former Performer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - Haymarket Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Haymarket is said to be haunted by several ghosts of old performers, though no one can say who, why or when.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Attacking Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - Kingclere Road and Catern's Grave<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> A particularly hostile entity, this grey cowled shade has reportedly attacked at least two witnesses and tried to take possession of another's body.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Passionate Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - Old Vic public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1978 / 1979<br>
              <span class="w3-border-bottom">Further Comments:</span> The female spirit was responsible for keeping the landlord awake at nights, with her sighing and heavy breathing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walking Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - Red Lion Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1989 - 1995<br>
              <span class="w3-border-bottom">Further Comments:</span> Former manager Robert Rutherford claimed that he had encountered a ghostly woman wearing a ruffled dress multiple times while working at the hotel. Cleaning staff would also encounter the figure sitting on beds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Purple Object</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - Skies over the town<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 04 August 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> A purple pear-shaped object was seen by Kris Reed, from his home. The ufo hovered before moving away, creating a noise like a washing machine. On the same night, another witness reported seeing a large red ball moving slowly across the sky.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Shape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - Stockroom<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2018<br>
              <span class="w3-border-bottom">Further Comments:</span> A worker reported seeing a black shape a stockroom. On other occasions, the same worker sensed a presence which wanted the person to leave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Baboon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - The Swallows private house, on the outskirts of the town (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Haunted by a large baboon and a huge dark cat, the owners finally left when screaming started coming from the attic. The building was pulled down when no one could be found to move in.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roll out the Barrel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke - White Hart public house, London Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Successive landlords have been woken at night by the sound of barrels being rolled outside, though nothing can be found when investigated further. A few staff have reported seeing a couple of spooky figures, one male and the other female.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 254</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=254"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=10&totalRows_paradata=254"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southeast.html">Return to South East England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
