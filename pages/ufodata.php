



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="UFOs and Alien Encounters - Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > UFOs</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>UFO Sightings and Alien Encounters</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Orange Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen (Aberdeenshire) - Over city<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A handful of reports concerning strange lights were recorded during 2006. One family observed six orange lights hovering in the sky before appearing to disintegrate.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Landing Craft</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Albrighton (Shropshire) - RAF Cosford (now Aerospace Museum)<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1963<br>
              <span class="w3-border-bottom">Further Comments:</span> This ufo report was based on a story made up by two young people returning late from a leave pass. As time went on, the story grew, and it eventually became that an UFO had touched down near a hangar on the base, and was recovered and removed for reverse engineering.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf385argh.jpg' class="w3-card" title='The sky above Aldeburgh, Suffolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The sky above Aldeburgh, Suffolk.</small></span>                      <p><h4><span class="w3-border-bottom">Round Platform with Handrail around Edge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldeburgh (Suffolk) - Somewhere over the town<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1916<br>
              <span class="w3-border-bottom">Further Comments:</span> A female witness watched for 5 minutes as this disk carrying a dozen men dressed like sailors flew overhead, at a height of about 30 feet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Aldershot-Basingstoke-Canal.jpg' class="w3-card" title='There are parallels between alien abduction and fairy abduction.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> There are parallels between alien abduction and fairy abduction.</small></span>                      <p><h4><span class="w3-border-bottom">Little Green Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldershot (Hampshire) - Basingstoke Canal<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 August 1983, 01:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> An elderly man who set up here for a spot of night fishing was invited into a UFO by two little men in tight green outfits, with visors covering their faces. They shone a light at him before saying that he was 'too old', promptly returning him to his fishing rod.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anlaby (Yorkshire) - Anlaby school, playing ground<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 January 1978<br>
              <span class="w3-border-bottom">Further Comments:</span> A youngster playing in the grounds of the school watched a round craft land and three figures dressed in golden clothing climb out. The child ran off to find friends to tell, and when he returned with someone the craft was already back in the sky - it quickly flew off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man with No Face</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anstey (Leicestershire) - Fields along Anstey Road, leading to Leicester<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be an early alien encounter (though it can also be interpreted as ghostly in nature), a woman playing in the fields came across a figure without a face dressed in black. Behind him, she could see a circular 'hut'. By the time she awoke her father, who was sleeping in a nearby field, both hut and man had gone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashburton (Devon) - Skies north of the village<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 1915<br>
              <span class="w3-border-bottom">Further Comments:</span> Admiralty Intelligence Officers investigated a series of strange lights which were seen in the skies over the area by multiple witnesses. The investigating officers also watched the lights rise into the air and vanish. The officers' report suggested that the lights may have been attached to a balloon used to lift wireless equipment to enable German spies to contact the mainland, although no evidence could be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wilt3651.jpg' class="w3-card" title='The skies above Avebury, Wiltshire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The skies above Avebury, Wiltshire.</small></span>                      <p><h4><span class="w3-border-bottom">Mystery Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Avebury (Wiltshire) - Skies above area<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1991<br>
              <span class="w3-border-bottom">Further Comments:</span> Some say that UFO sightings here go back at least three hundred years, though only more recently have crop circles and photographs of glowing balls of light been documented.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hairy Creatures with Large Pointed Ears</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aveley (Essex) - Road in the area, exact location not known<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 October 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> While driving home, a family of four were abducted by a blue UFO; the occupants described as four foot high hairy beings, with large eyes and pointed ears. After 3 hours, the family were returned to their car.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Saucer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bagshot (Surrey) - Two miles past Junction 3, heading towards London<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> January 1985<br>
              <span class="w3-border-bottom">Further Comments:</span> Watched for between five and eight minutes, this saucer shaped craft had 'fifty pence piece type edges'. White lights could be seen at the front of the object, and blue lights at the rear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Orange Globes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bangor (County Down) - Skies around the area<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 May 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> Several media companies and air traffic control were contacted after witnesses watched three orange globes fly across the night sky. The objects were visible for around five minutes before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Purple Object</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Basingstoke (Hampshire) - Skies over the town<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 04 August 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> A purple pear-shaped object was seen by Kris Reed, from his home. The ufo hovered before moving away, creating a noise like a washing machine. On the same night, another witness reported seeing a large red ball moving slowly across the sky.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">UFO Technology under R&D</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bawdsey (Suffolk) - Top secret location<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s/1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A relatively new myth, this location is said to house downed UFO technology, under reverse engineering by UK scientists.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Belfast-Above-Yorkgate.jpg' class="w3-card" title='A rather dramatic green UFO over Belfast.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A rather dramatic green UFO over Belfast.</small></span>                      <p><h4><span class="w3-border-bottom">Green Triangle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belfast (County Antrim) - Above Yorkgate shopping centre<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 November 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> Shoppers at the centre reported seeing a green triangular ufo cross the sky above them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Inverted Mushroom</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bircham Newton (Norfolk) - Skies over Bircham Newton<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This strange flying machine passed overhead here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fifty Pence</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackmore (Essex) - Skies over<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 March 1985, 19:05h<br>
              <span class="w3-border-bottom">Further Comments:</span> A nightclub manager reported watching a heptagon shaped object hovering before moving away at speed. The ufo projected two beams of light from the front and two weaker beams from the rear, and had clusters of red, yellow and green lights on the underside.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Men with Light Bulb Heads</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton (Greater Manchester) - Alleyway in town, exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1926<br>
              <span class="w3-border-bottom">Further Comments:</span> A local lad escaped one night from his house to play hide and seek with his friends. While searching for them, he observed three figures in grey rubber suits and fishbowl helmets in a back yard; they also possessed pallid faces shaped like upside-down pears, with slits for eyes and mouth. When they moved towards the boy, he ran home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murphy Incident</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton (Greater Manchester) - Winter Hill<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1999<br>
              <span class="w3-border-bottom">Further Comments:</span> After spotting a ufo hovering over his field, a farmer reported the sighting to the Manchester Aerial Phenomena Investigation Team. The team reported by were followed by an unidentified man, while the farmer was warned to stop talking about the incident by the Ministry of Agriculture, Fisheries and Food, although the government agency denied they had spoken to the farmer.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mysterious Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bonnybridge (Falkirk) - Skies over town<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1977 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Sightings began at this UFO hotspot around 1977, though it was not until the 1990s that the popular media began to take an interest. Though media coverage has died down as of late, it has been reported that many locals still witness strange lights but are now less likely to talk about their encounters.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Almost a Disc</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bonsall (Derbyshire) - Over village<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000<br>
              <span class="w3-border-bottom">Further Comments:</span> A UFO videotaped over the village was circular with a semicircle missing from an edge, rather like a digestive biscuit with a bite taken from it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Missing Time</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bordon (Hampshire) - Forest Road / Hendon Road area<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> June 1999 and March 2001<br>
              <span class="w3-border-bottom">Further Comments:</span> Two friends driving from the hospital to one of their parent's homes, a two minute trip, lost one hour forty minutes of their lives. Neither was aware of any periods of unconsciousness or 'jumps' in time. Two years later, another report came in of a missing time episode in the area; this time the witness lost three hours during a five minute walk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Round Object</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brantham (Suffolk) - Slough Road<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1986, around 22:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A brother and sister watched a large round metal looking object with blue and red lights around the sides hovering over the field opposite their home. It was stationery for around three minutes, before disappearing in a flash.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red and Orange Spheres</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bridlington (Yorkshire) - Dotterel junction area<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> Witness David Hinde reported two spherical lights in the sky, glowing red and orange. He watched them for ten minutes while they darted around, defying the movements of conventional aircraft.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-7.jpg' class="w3-card" title='An old postcard showing Brixham in Devon.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Brixham in Devon.</small></span>                      <p><h4><span class="w3-border-bottom">Domed Craft</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brixham (Devon) - Skies over town<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 28 April 1967<br>
              <span class="w3-border-bottom">Further Comments:</span> This craft hovered over the town for around an hour before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Saucer Occupants</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Broad Haven (Dyfed) - Broad Haven County Primary School<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> February 1977<br>
              <span class="w3-border-bottom">Further Comments:</span> A large disc was seen landing near the school by fourteen of its pupils, some of whom also said men with pointed ears and dressed in silver clothing had climbed out. For several months after the event, sightings continued in the area.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 198</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=198"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=7&totalRows_paradata=198"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
