
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of King's Lynn Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > King's Lynn</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of King's Lynn</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6350.jpg' class="w3-card" title='King Street, King&#039;s Lynn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> King Street, King&#039;s Lynn.</small></span>                      <p><h4><span class="w3-border-bottom">Man in Three Cornered Hat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - 9 King Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> This eighteenth century gentleman has been observed by several different witnesses in the building. In its latter days as a museum, one visitor spotted the ghostly gentleman walking around with his hands behind his back.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Bank House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> While several staff have reported feeling as if someone is watching them, only one person has reported seeing the phantom woman wearing a long black dress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Taps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Clifton House<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Tradition says this building was home to a tapping entity, but no reports can be found post-1950s.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6351a.jpg' class="w3-card" title='Devil&#039;s Alley, King&#039;s Lynn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Devil&#039;s Alley, King&#039;s Lynn.</small></span>                      <p><h4><span class="w3-border-bottom">Satan's Hoof Print</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Devil's Alley<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> No longer present<br>
              <span class="w3-border-bottom">Further Comments:</span> A single footprint belonging to Old Nick was said to be visible down this aging alleyway, although the route has now been resurfaced.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor963.jpg' class="w3-card" title='Duke&#039;s Head, King&#039;s Lynn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Duke&#039;s Head, King&#039;s Lynn.</small></span>                      <p><h4><span class="w3-border-bottom">The Red Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Duke's Head, Tuesday Market Place<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure in red has been seen walking the corridors and climbing staircases. She is said to be the shade of a woman who killed herself over her two lovers. Room 18 was once haunted after an attempted suicide resulted in a dying man being brought into the suite - his ghostly moaning once drove people away, though now it has faded.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Party Goers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Eagle Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Since the Eagle was rebuilt in the late 1950s, the owners have reported the sounds of a party downstairs. It is thought that these sounds belong to a party which was brought to a sudden end when a German bomb hit the original building in June 1942.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Mildenhall Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The attic of the hotel is reputedly haunted by a woman who died shortly after giving birth to a child outside of wedlock.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cold Spot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Private residence, close to the docks<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> March - June 2002<br>
              <span class="w3-border-bottom">Further Comments:</span> Perhaps attached to a visitor who briefly stayed at this house, the site was subjected to several poltergeist-like incidents. These included a cold spot, bangs and doors opening unaided. A shadowy figure was seen in the kitchen, briefly mistaken for a real person before it faded away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Private residence, Seabank area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970 - 1976<br>
              <span class="w3-border-bottom">Further Comments:</span> A former occupant of this home reported hearing disembodied footsteps walking up and down the staircase at night. Doors and windows would also bang shut. On one occasion the witness awoke to see a woman in black and dark grey. It was said that an exorcism was performed on the site prior to the family moving in.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor979kynn.jpg' class="w3-card" title='King&#039;s Lynn Public Library.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> King&#039;s Lynn Public Library.</small></span>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Public Library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure is reported to haunt the basement of the building, once a monastery. At one time, staff were apparently too scared to enter the area by themselves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6353.jpg' class="w3-card" title='King&#039;s Lynn Quay, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> King&#039;s Lynn Quay, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">The Bride</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Purfleet Quay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This woman killed herself the day after being wed - her ghost loiters around the Purfleet area before screaming and throwing itself into the quay waters. Other screams, belonging to fighting soldiers, can also be heard in the area, during which the water runs red with blood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6359.jpg' class="w3-card" title='The Exorcist&#039;s House, King&#039;s Lynn, Norfolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Exorcist&#039;s House, King&#039;s Lynn, Norfolk.</small></span>                      <p><h4><span class="w3-border-bottom">Mrs Buckley</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - The Exorcist's House<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a former occupant of this old house was seen briefly by the two daughters of the man who had just purchased the property.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor965.jpg' class="w3-card" title='The Globe Hotel, King&#039;s Lynn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Globe Hotel, King&#039;s Lynn.</small></span>                      <p><h4><span class="w3-border-bottom">The Chill</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - The Globe Hotel, Tuesday Market Place<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Rather than a building design flaw, this 'chill' has been blamed on the spirit of a man once murdered in the stables, a site now incorporated inside the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor402.jpg' class="w3-card" title='Tudor Rose Hotel, King&#039;s Lynn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Tudor Rose Hotel, King&#039;s Lynn.</small></span>                      <p><h4><span class="w3-border-bottom">Small Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - The Tudor Rose Hotel, 11 Nicholas Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Shortly after a wedding, the bride was stabbed to death by her new husband in the hotel. Since then, a short woman in a long white dress has been spotted and phantom footsteps heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6349.jpg' class="w3-card" title='Thoresby College, King&#039;s Lynn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Thoresby College, King&#039;s Lynn.</small></span>                      <p><h4><span class="w3-border-bottom">Mover</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Thoresby College<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> At least one cleaner has reported that small items vanish, only to reappear in different places around the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Quite a Few</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - True's Yard Fisherfolk Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> This building is said to be haunted by a few dozen ghosts, although no sightings appear to predate the year 2000. The building's ghosts featured in several news publications in September 2021 after a ghost hunter's motion-activated music box was left on the site, causing museum staff some concern.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor962.jpg' class="w3-card" title='Tuesday Market Place, King&#039;s Lynn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Tuesday Market Place, King&#039;s Lynn.</small></span>                      <p><h4><span class="w3-border-bottom">King's Lynn's Heart</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - Tuesday Market Place, house No's 15/16<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Legend unknown, but heart still visible<br>
              <span class="w3-border-bottom">Further Comments:</span> Several legends explain why this heart is here, though all revolve around the same theme; the market square was once a place of execution, and the heart exploded out from the chest of the unfortunate female victims, embedding into the wall. Number 15 is also home to a poltergeist who throws things to the floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nor6357.jpg' class="w3-card" title='White Hart pub, King&#039;s Lynn.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> White Hart pub, King&#039;s Lynn.</small></span>                      <p><h4><span class="w3-border-bottom">Lost Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> King's Lynn - White Hart public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom which haunts this pub is thought by most to be a monk from the church across the road - the church and public house were once connected by a now bricked up tunnel.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 18 of 18</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
   <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
