

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Yorkshire, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northeastandyorks.html">North East and Yorkshire</a> > Yorkshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Yorkshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horsemen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A58 - A58/A641 junction, also known as Hell Fire Corner<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Both ghostly horsemen and phantom car have reported around this junction. The area is considered an accident black spot, so the question to ask is do the ghosts cause the accidents, or are they the products?</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A684 - Between Northallerton and Leeming Bar<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2001, between 20:00h - 22:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A large black dog ran in front of two women travelling to Leeming Bar by car. The driver closed her eyes and braked hard, expecting to hit the creature. The passenger watched the hound pass through the bonnet, and noticed the creature had no facial features, floppy ears, and was shadow-like. A man that the women spoke to once they reached their destination later killed himself - was the hound a portent?</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Coffin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acaster Malbis - Hauling Lane?<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown - pre nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Two men driving sheep and returning home to York spotted a coffin floating in the air ahead of them, dipped as if carried on the shoulders of invisible men. A Bishop walked behind the coffin, his mouth moving but with no sound being heard. The apparitions vanished close to a field. Both men were said to have spent several days in bed after their encounter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant's Treasure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Addleborough - Barrow in the area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The giant's treasure slipped from his grasp as he crossed the area, the hoard sinking into the ground and becoming covered in rocks. The giant had to leave the treasure behind, but it was written that a mortal man could recover the wealth if a fairy in the form of a chicken or an ape appeared; the man would have to reach out and grab the treasure without making a sound, and if he failed to do so, the fairy would never appear to him again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banished Spirit</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldwark - Roads close to the stone known as the Conjuring Stone, close to Chapel-garth (currently on grounds of Aldwark Manor)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghost which haunted the roads around this area would terrify the locals after dark, so a holy man was dispatched to banish the entity. After a time, the ghost was tied to a large rock which was designated the Conjuring Stone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mr Rimmington</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Almondbury - Woodsome Hall golf course<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Rimmington reportedly takes several forms, including a man on horseback accompanied by two hounds, and the slightly less intimidating form of a robin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anlaby - Anlaby school, playing ground<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 January 1978<br>
              <span class="w3-border-bottom">Further Comments:</span> A youngster playing in the grounds of the school watched a round craft land and three figures dressed in golden clothing climb out. The child ran off to find friends to tell, and when he returned with someone the craft was already back in the sky - it quickly flew off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Earth-shaking Sounds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appletreewick - Low Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Coming from deep underground, low unearthly groans would often knock ornaments from their stands. The cause of the disturbance was finally exorcised.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Appletreewick-Trollers-Gill.jpg' class="w3-card" title='Trolls or just pareidolia?'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Trolls or just pareidolia?</small></span>                      <p><h4><span class="w3-border-bottom">Trolls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appletreewick - Trollers Gill (or Ghyll)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Living beneath the ravine, the trolls are said to emerge at night and track down their prey - humans. At least one farmer was lured to his death by their ability to trick people into straying from the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Barguest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appletreewick - Trollers Gill (or Ghyll)<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Possessing glowing red eyes and a yellow tint to its fur, the dog-like barguest is a harbinger of doom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">William Nevison</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ardwick le Street - Hanging Wood<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The woods were a popular place for Nevison, a highwayman, to lurk before riding out to commit his crimes. After he was hanged in York, his shade returned to the scene of better times.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Arthington-Skies.jpg' class="w3-card" title='The flying nun of Arthington.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The flying nun of Arthington.</small></span>                      <p><h4><span class="w3-border-bottom">Sister Hylda</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arthington - Skies over the community<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1200<br>
              <span class="w3-border-bottom">Further Comments:</span> This nun haunted the area for seven years, zooming around at low altitude and accusing a friar of murdering her. It finally grew too much for the Abbess, who summoned all the nuns from the district and, along with an Archbishop, exorcised the spirit.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Askrigg - Fors Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> It was once believed that any torch taken into a particular stone passage would be extinguished by the ghosts of the monks. Logic would suggest that the torch would be blown out by the winds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mary</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Askrigg - Nappa Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1878<br>
              <span class="w3-border-bottom">Further Comments:</span> This location is another haunt of the spiritually busy Mary Queen of Scots, who has been seen here wearing a black velvet dress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rector</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aston - High Trees house, former rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This Rector murdered his wife after he caught her having an affair with a manservant. He is doomed to walk the buildings walls, and the bloodstain that her body created cannot be removed from the bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Breaker of Everything</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Attercliffe - 37 Candow Street (road no longer exists)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1910<br>
              <span class="w3-border-bottom">Further Comments:</span> Every breakable item in the household was reportedly smashed by this poltergeist. Visitors had to dodge a flying basin, and one person fainted after witnessing the activity. The occupants finally moved in with neighbours.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Projectionist?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Attercliffe - Adelphi theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Visited during the day when a nightclub, a member of the Sheffield Paranormal Society and other witnesses reported hearing a series of bangs, experiencing icy cold temperature and a twitching curtain, and feeling compression on her chest whilst in the former cinema projection room. It was said that a former projectionist loved his job and may have never left. The site was also said to have experienced several people dying in a fight in the dim and distant past. The visitors were told that cleaners in the toilets had been hit by unseen hands, while two security guards experienced something which resulted in them never returning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Army</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Attercliffe - Near the river<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1661<br>
              <span class="w3-border-bottom">Further Comments:</span> A local vicar, while sitting by the river, watched a line of white troops pass accompanied by white horses - he wrote that it took over an hour for the column to pass.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Attercliffe - Station Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This hotel is reputedly haunted by a man knifed to death back in the days when the building was three cottages.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Halliwell Boggle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atwick - Between village and Bewholme, pool of water at the bottom of the hill where the church stands (pool may no longer be present)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Story circa nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity was said to terrorise anyone foolish enough to walk this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atwick - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This decapitated spectral bandit remains lurking in the shadows of his old ambush area. One version of the story says he remains on his horse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Auckley - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This restless spirit was said to have made an appearance at many church events over a period of four hundred years, before she finally departed this plane of existence late in the nineteenth century.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Best at the Bottom</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Austwick - Unidentified deep pool<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A man with a group of friends either jumped or fell into a deep pool in the village. The man did not return to the surface, but a series of bubbles emerged from the water, the popping forming the words 'It's best at the bottom'. The man's friends all jumped in to see whether the words spoke the truth. None returned from the pool.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aysgarth - Road between Aysgarth & Woodhall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> March 1934<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in dark Victorian clothing with white gloves and a walking stick, this woman has been mistaken for someone living, though she never interacts with witnesses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crimson Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ayton - Crossroads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two phantoms were reputed to haunt this crossroads. A crimson lady (who is now likely redundant) would scare passing horses by leaping out at them. The second ghost, a pale woman on horseback, would scream at witnesses, and was said to chase passing cars.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 823</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=823"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=32&totalRows_paradata=823"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northeastandyorks.html">Return to North East and Yorkshire</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North West England</h5>
   <a href="/regions/northwest.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/northwest.jpg"                   alt="View North West records" style="width:100%" title="View North West records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
