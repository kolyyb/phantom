
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Dorchester Ghosts and Strangeness from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Dorchester</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Dorchester Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lantern Carrier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - A352 roundabout (towards Bournemouth)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 1992<br>
              <span class="w3-border-bottom">Further Comments:</span> While returning from a gig with a friend, one man drove past a figure holding a lantern. The figure resembled the old man featured on the cover of Led Zeppelin IV. The driver asked his friend whether he could see the figure, to which the friend replied 'Yes, don't stop'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Laurence</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Antelope public house (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Lawrence of Arabia would once frequent this public house, and it was said that the figure in motorcycling gear that would appear by the fireplace was the man himself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Dorset County Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> The PIT (Paranormal Investigation Team) group claimed to have photographed two entities within the museum, Judge George Jeffreys and Mary Anning. The latter was headless in her picture.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor8926.jpg' class="w3-card" title='Avenue Stadium - Dorchester&#039;s football stadium.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Avenue Stadium - Dorchester&#039;s football stadium.</small></span>                      <p><h4><span class="w3-border-bottom">Panther</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Field near Dorchester Football Stadium, and Talbothays Road<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2007, and 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> This large black cat, said by the witness to be the size of an Alsatian, was observed for several seconds in 2007. The year previous, another (or the same) black cat was spotted along Talbothays Road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Dorchester-Former-hospital.jpg' class="w3-card" title='One nurse you did not want to encounter.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> One nurse you did not want to encounter.</small></span>                      <p><h4><span class="w3-border-bottom">Nurse Kitty</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Former hospital, Prince's Street (since redeveloped)<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Nicknamed Kitty by other staff members, this phantom nurse was said to appear shortly before a patient died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor8707.jpg' class="w3-card" title='Icen Way, Dorchester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Icen Way, Dorchester.</small></span>                      <p><h4><span class="w3-border-bottom">Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Icen Way<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of horses dragging prisoners to their execution site are said to be heard along this road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor8711.jpg' class="w3-card" title='Judge Jeffrey&#039;s Restaurant, Dorchester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Judge Jeffrey&#039;s Restaurant, Dorchester.</small></span>                      <p><h4><span class="w3-border-bottom">Condemned Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Judge Jeffrey's Restaurant<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Used as a court room by Judge Jeffreys, the ghost that reputedly haunts this building is one of many that the Judge had hanged.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor8922.jpg' class="w3-card" title='Maumbury Rings, Dorchester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Maumbury Rings, Dorchester.</small></span>                      <p><h4><span class="w3-border-bottom">Dreaming Dreams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Maumbury Rings<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A Neolithic henge and Roman amphitheatre, those who fall asleep here are said to dream only of the Roman soldiers who frequented the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Only Way is Up</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Old Malthouse, High East Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of someone climbing the stairs and opening a door were heard several times by a former occupant of the building, though they never heard anyone come back down. Upon investigation, the upper part of the building was always found empty.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Prisoner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Pond along towpath<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century onwards?<br>
              <span class="w3-border-bottom">Further Comments:</span> An escaping prisoner tripped and drowned in this pond, weighed down by his chains. The sound of rattling is still said to haunt the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor8712.jpg' class="w3-card" title='Shopping arcade along Antelope Way, Dorchester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Shopping arcade along Antelope Way, Dorchester.</small></span>                      <p><h4><span class="w3-border-bottom">Jeffreys</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Shopping arcade along Antelope Way<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Hanging Judge is said to walk in this area, following a path that he took during life.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Dorchester-gibbet.jpg' class="w3-card" title='The Grey Lady of Dorchester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Grey Lady of Dorchester.</small></span>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Site of gibbet<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom figure seen around the site where the gibbet was stood is thought to be either a Tudor woman or the ghost of Martha Brown, the last woman to be hanged for murder in Dorset.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor5262.jpg' class="w3-card" title='St Peter&#039;s Church, Dorchester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> St Peter&#039;s Church, Dorchester.</small></span>                      <p><h4><span class="w3-border-bottom">Nathaniel Templeman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - St Peter's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 or 25 December 1814<br>
              <span class="w3-border-bottom">Further Comments:</span> A former rector, Templeman is only said to materialise to protect the church and its property against thieves and vandals. He was also seen in 1814 by two churchwardens as they rested after decorating the church - they made the mistake of drinking communion wine to warm themselves up, and Templeman manifested to shake his head at them before floating along the north aisle and disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Water Fairies</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Thorncombe Wood - Rushy Pond<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> While some say this pond is home to fairies, others maintain it is home to a more spectral entity (though who or what is unclear).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gorilla?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Unnamed Park<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> March 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> A teenager photographed a strange creature close to her home in parkland, before it ran up a tree and disappeared. The teenager said the creature resembled and moved like a small gorilla, although the nearby Monkey World attraction denied they were missing any of their animals.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dor8710.jpg' class="w3-card" title='Wessex Royal Hotel, Dorchester.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Wessex Royal Hotel, Dorchester.</small></span>                      <p><h4><span class="w3-border-bottom">Woman in Bonnet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Wessex Royal Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing a bonnet and a long dark dress, this phantom woman has been seen from outside the hotel, glazing from a window on the ground floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach in the House</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Wolfeton (aka Wolveton) House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One owner of the property won a large amount of money when he drove a horse and coach up the stairs inside the house - his victorious ghost reportedly re-enacts the antic. A separate entity is that of a headless grey woman, a member of the Trenchard family who slit her own throat, while a priest is said to haunt the gatehouse (where he was held prisoner).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wildmen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dorchester - Yellowham Hill<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown (but not recent)<br>
              <span class="w3-border-bottom">Further Comments:</span> This area was once thought to be the home of large hairy men, who abducted women.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 18 of 18</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
   <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
