
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Burnley Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northwest.html">North West</a> > Burnley</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Burnley Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4647c.jpg' class="w3-card" title='Burnley Football Club, Lancashire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Burnley Football Club, Lancashire.</small></span>                      <p><h4><span class="w3-border-bottom">Bee Hole Boggart</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Area around Burnley Football Club<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1843, exact date not known.<br>
              <span class="w3-border-bottom">Further Comments:</span> Years before the area was developed, the entity patrolled the area looking for people - it once kidnapped and murdered an old woman, leaving her skin to be discovered on a rose bush.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unknown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Bridge End House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> T F Thiselton Dyer in his book Ghost World says the place is haunted but provides no further information.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4704.jpg' class="w3-card" title='Burnley newspaper offices, Lancashire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Burnley newspaper offices, Lancashire.</small></span>                      <p><h4><span class="w3-border-bottom">Edward Fishpool</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Burnley newspaper premises, Bull Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A former printer at the building, Edward died before completing a piece of work and now walks the area in regret.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Skipping Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Burnley Wood Primary School<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1971<br>
              <span class="w3-border-bottom">Further Comments:</span> Cleaning staff and the caretaker all reported a ghostly child who would skip and sing down corridors in the school. They also heard the unnerving sounds of a baby moaning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gas Lamp</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Claughton Street<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa August 1906<br>
              <span class="w3-border-bottom">Further Comments:</span> Over the course of a week, 400 people were said to have visited the street in the hope of spotting a ghost which had taken to occasionally appearing. A local police officer demonstrated that the ghost was the reflection of two gas lamps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hunt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Eagle's Crag, Cliviger Gorge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Lord William Towneley and his dog appear once a year, giving chase to a nimble doe. Another variant of the tale names the hunter as Gabriel Ratchets, while yet another story states that a man and his wife manifest on the same date - he killed and buried her on the spot.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4703.jpg' class="w3-card" title='Bethesda Street, Burnley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bethesda Street, Burnley.</small></span>                      <p><h4><span class="w3-border-bottom">Smashed Bulbs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Electrical store (no longer present) along Bethesda Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Reportedly built on the site of a graveyard from which some bodies were not moved, several spooky occurrences have been reported in this shop, including doors opening without human intervention, exploding light bulbs, and apports.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse Drawn Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - From Hufling Hall, via Hufling Lane, to Towneley Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Travelling in a horse drawn carriage, a ghostly woman is said to leave Hufling Hall and head to Towneley Hall. It is said that you are more likely to hear the horses when it is quiet then see anything.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc1595.jpg' class="w3-card" title='One of the waterways which runs through Burnley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> One of the waterways which runs through Burnley.</small></span>                      <p><h4><span class="w3-border-bottom">Trash</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - General area, though near the church a favourite haunt<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local name for their version of the shuck, Trash was said to be very hairy, with feet that created splashing noises no matter what surface it stepped on. Death would follow a sighting.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Girl Laughing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Mark One, Manchester Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> July/August 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Members of staff at this retail store have reported unexplained breathing and footsteps in the stock room. Clothes hangers on the rails in this room have been heard moving on their own, while the sound of a young girl laughing and her small pattering footsteps has been heard. There are reports of scraping, dragging footsteps downstairs in the cellar and the presence of a man felt.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Visiting Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Old Grammar School<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A large, scorched patch on some flagstones in the building is explained away by a story which involves a group of students silly enough to try to summon the Devil - as he climbed out of the ground, the pupils panicked and hit Old Nick with books, stones and anything they could grab, until he disappeared back to Hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Arrow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Over Rowley lake, Brunshaw road<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 October 2011, around midnight<br>
              <span class="w3-border-bottom">Further Comments:</span> A night fisherman spotted an arrow shaped object travelling at a fantastic speed above him. The craft was silent, had a light at each corner and left no contrails. The witness believed it could have been manmade. And possibly connected to a nearby military base.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Witches</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Pendle Forest<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1860s<br>
              <span class="w3-border-bottom">Further Comments:</span> In the mid-eighteenth century the belief in witchcraft persisted and farmers were said to use apotropaic magic to protect themselves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Powered Tools</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> October 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> Power tools would turn on unaided in the middle of the night, crockery would move in the kitchen, and the sound of coins could be heard on the staircase, all when this witness was home alone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lights Off</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Private residence along Mitella Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A worker at this building reported light switches would turn continually flick themselves off as she left a room, though there was no reason why they should. An electrical diagnostic throughout the property discovered nothing unusual.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown Objects</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Private residence, Colburn Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> The occupant of this property reported that objects would be thrown around in the kitchen, while doors would open by themselves and tapping could be heard around the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc5510a.jpg' class="w3-card" title='Rosehill House Hotel, Burnley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Rosehill House Hotel, Burnley.</small></span>                      <p><h4><span class="w3-border-bottom">Shadowy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Rosehill House Hotel, Rosehill Avenue<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Though most of the hotel dates back many years, this figure was seen sitting on the end of a bed in a new extension to the roof - the occupant insisted on changing his room. The entity was also seen on the staircase.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc2352.jpg' class="w3-card" title='Smackwater Jack&#039;s, Burnley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Smackwater Jack&#039;s, Burnley.</small></span>                      <p><h4><span class="w3-border-bottom">Footfalls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Smackwater Jack's<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The disembodied footsteps reported in this bar have been blamed on a former ironworker who once worked on the site, before it became a pub.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4668b.jpg' class="w3-card" title='St Peters Church, Burnley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> St Peters Church, Burnley.</small></span>                      <p><h4><span class="w3-border-bottom">Moving Bricks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - St Peters Church<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown / traditional<br>
              <span class="w3-border-bottom">Further Comments:</span> The church was going to be built on another location, but night after night the Devil appeared and moved the building materials, until the construction was relocated to where the church now stands.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc5511b.jpg' class="w3-card" title='Manchester Road, Burnley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Manchester Road, Burnley.</small></span>                      <p><h4><span class="w3-border-bottom">Lady O'Hagan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Summit area of Manchester Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The last owner of nearby Towneley Hall, the woman now drives her horse and trap in the area. It is said you can hear the hooves pounding on the cobbled streets, even though the stones have been replaced with modern materials.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Swan Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Strange sounds have been heard around this inn, and cleaning staff have seen a figure walk into the men's toilet, yet never coming out.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4708b.jpg' class="w3-card" title='Town library, Burnley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Town library, Burnley.</small></span>                      <p><h4><span class="w3-border-bottom">Piano Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Town library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A piano in a lecture theatre here was seen to play itself and has been also heard several times since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lanc4261a.jpg' class="w3-card" title='Towneley Hall, Burnley.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Towneley Hall, Burnley.</small></span>                      <p><h4><span class="w3-border-bottom">Sir John Towneley</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Towneley Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former owner of the building, Towneley is reluctant to vacate his former property. He makes his presence known by playing with the lights and creating unrecognisable sounds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ticking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Unnamed house along Crowther Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Once belonging to a watchmaker who hanged himself, local legends state that you can hear strange ticking as you pass the house late at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Miner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burnley - Victoria Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Having died in the hospital after a mining accident seriously injured many of the pit workers from a nearby colliery, this solitary figure is occasionally reported by staff working the night shift.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 25</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northwest.html">Return to North West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
