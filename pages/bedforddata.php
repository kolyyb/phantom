
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Bedfordshire Ghosts and Mysteries from The Paranormal Database.">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Bedfordshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Bedfordshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hanging Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ampthill - Hairdressers along Church Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Not known<br>
              <span class="w3-border-bottom">Further Comments:</span> An exorcist was called to the hairdressers after a customer reported seeing a man hanging from the upstairs room. While the ceremony appears to have banished the hanging man, the ghost was soon replaced with that of a small child.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ampthill-Houghton-House.jpg' class="w3-card" title='Shadowy figures stand in the ruins.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Shadowy figures stand in the ruins.</small></span>                      <p><h4><span class="w3-border-bottom">Shadows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ampthill - Houghton House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> From the ruins of this once proud house come reports of shadowy figures that quickly vanish if seen. The sound of several galloping horses pulling a coach have also been heard on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Housekeeper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ampthill - Private residence, Chandos Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A property here was haunted by a phantom housekeeper. The entity was spotted a couple of times and blamed when items were moved around.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Old Fashioned Clothing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ampthill - Queen's Head public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This pub is said to be haunted by a man wearing clothing dating from the Eighteenth Century.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knight in Armour</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ampthill - Road from Ampthill to Millbrook, and Ampthill Great Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> This glowing ghostly knight appeared in front of several witnesses along the road in the early twentieth century. Soldiers at a nearby barracks also reported the figure around the same time, observing the ghost from a distance as it descended a hill. At least one local man reports seeing the ghost several times around the park; on this site it is said to depart the site of a castle (now marked by a cross) and move towards a brook where he vanishes. A phantom woman in white has also been reported in the park.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coachman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ampthill - White Hart Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A caped coachman is rumoured to haunt the bar, while in a room elsewhere, doors and windows open unaided.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dark Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aspley Guise - Gypsy Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 09 September 2008, 21:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> While out running, a witness spotted a dark figure around one hundred metres ahead of him. As the distance closed to around twenty metres, the figure vanished. A friend of the witness said that his uncle had encountered a phantom woman along the same stretch of road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Reduced Rent</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aspley Guise - Woodfield House, Weathercock or Woodock Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1948<br>
              <span class="w3-border-bottom">Further Comments:</span> A man protested about the cost of his rates here and had them reduced after he stated that his house was haunted. He attempted to have them further cheapened by saying that the road itself was haunted by a ghostly horseman, but no further reduction was given. The ghost story behind this is that the shades of a murdered couple haunted the spot before the ghost of Dick Turpin joined them - Turpin had found their bodies and helped the murderer bury them, in return for a safe house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Imprint</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Astwood - Astwood Court<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A table on site reportedly contains the imprint of a ghost's hand - the female phantom once hit the table creating the indent, before vanishing forevermore.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elderly Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barton le Clay - Waterside Mill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A former owner of this mill reported encountering an old lady with long grey hair on two occasions; each time she waved a bony finger at him, as if wanting the witness to follow her. He did not.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Battlesden - Battlesden Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s or 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> This church was said to be home to a phantom monk or lay friar and was spotted a couple of times during the mid-twentieth century.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Steward</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Battlesden - Battlesden House (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The steward overcharged his customers when selling milk and was condemned to haunt this area. The ghost appears to have vanished since the destruction of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Arms</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Battlesden - Lane near the battlefield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn 1982<br>
              <span class="w3-border-bottom">Further Comments:</span> Two witnesses out cycling saw a disembodied pair of arms throw a small log over a hedge - one of the pair swerved to avoid the piece of wood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knocking on the Door</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - 38 Mill Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Many times has the sharp rapping at office doors been heard in this building, only for the door to be opening and nothing found beyond. Invisible feet have also been heard padding along the corridors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gliding Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Bedford Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1972, 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> In 1972 two nurses watched an attractive girl in a long dress and coat glide into a toilet on the south wing of the hospital. They followed, only to find the room empty. The same entity may have been observed 28 years later when three members of staff reported seeing a lady in a long dress walk through the wall of a storeroom. The Shand Ward is said to be haunted by shuffling footsteps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/bedford-grammar-school.jpg' class="w3-card" title='An old picture postcard of the grammar school.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old picture postcard of the grammar school.</small></span>                      <p><h4><span class="w3-border-bottom">Samuel Whitbread</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Bedford School<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of the 1903 headmaster Samuel Whitbread is reported to wander the corridors of the higher floors of the main building (which he commissioned) at night. On numerous occasions he has asked people who enter the building at night for directions out; one boy when leading him to the exit found he suddenly disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Bowler Hat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Cecil Higgins Art Gallery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This building is thought to be haunted by several entities, including a man wearing a bowler hat dressed in 1930s clothing, another man but this time wearing a dark suit, and a stable boy who walks around the library. Minor poltergeist activity is also reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Cineworld<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Both Screen 4 and the toilet area are reputed to be haunted by a hooded figure. Cold spots and strange sounds have also been reported. The cinema stands on the former site of Newnham priory.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Tom</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Junction of Tavistock Street, Union Street and Clapham Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Around Christmas, 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A nineteenth century highwayman, Black Tom was executed and buried at this junction with a stake driven through his heart to prevent his return. The ritual failed, and both he and another unknown ghost were seen countless times for years following his death. A 1960s sighting of Tom took place in broad daylight, with witnesses stating they saw a gruesome figure with a broken neck staggering down the road, vanishing into thin air seconds later. The witness who spotted Tom during the 1990s thought he was just a drunk man in fancy dress, until he suddenly disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - King's Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Built on the site of a morgue (!), there are any number of contenders who could be the source of this young looking spectre. An older looking man is reported to haunt the cellar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Creeping Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Methodist Church, along Newnham Avenue<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness reported seeing a well-dressed man creeping along the back of the church. This figure walked through a wall, leaving a thin veil of smoke in his wake. A few years prior to this, another entity had been seen around the same place, though the witness said the ghost had worn tweed clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Park public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a Second World War soldier who died when this building was used as a hospital is said to occasionally appear to staff. The ghost also has been known to open doors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Record Player</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Private flat along Tavistock Street<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A man using the flat came home after work to find a record playing on his music system. Furniture would also rearrange itself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Private flat, Ram Yard<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 September 1999<br>
              <span class="w3-border-bottom">Further Comments:</span> Awaking to see her grandmother sitting by her bed, this witness briefly forgot that her grandmother had died a couple of years previous. The witness then noticed an orange glow outside. The window - a car outside was on fire, blocking the exit and filling the building with smoke. The fire brigade soon had the blaze under control, although the witness was convinced that her grandmother had saved her from dying of smoke inhalation.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Tom?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedford - Private residence along Gladstone Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1985 or early 1986<br>
              <span class="w3-border-bottom">Further Comments:</span> A man at this property awoke to find what he thought was an intruder in his bedroom. The man's shouts awoke his wife, and they both witnessed a dark shadowy figure in a black cloak and floppy hat move to the opposite side of the room and vanish. A few months later they heard the legend of Black Tom, the local horse rustler hanged close to their home.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 126</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=126"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=126"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
