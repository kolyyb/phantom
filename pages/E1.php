
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="E1 - A List of London Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > E1</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of London's E1 District</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unlucky Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - 88 Newark Street, Whitechapel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s<br>
              <span class="w3-border-bottom">Further Comments:</span> The family who lived here for six years reported a suppressive atmosphere and terrible luck. Their children reported a phantom cat, while the mother was too scared to enter certain rooms.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in the Gutter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Durward Street, Whitechapel (was Bucks Row)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1888 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The general area is supposedly haunted by the sounds of the victims of Jack the Ripper, thought only in Durward Street has an apparition been observed; the glowing woman lays in the gutter, the location where Mary Ann Nichols was discovered (the Ripper's first victim).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smell of Death</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Former Bass Sales Office, Cephas Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980<br>
              <span class="w3-border-bottom">Further Comments:</span> Workers in this building, constructed on the site of an old Doctor's office, reported cold spots and the smell of embalming fluid.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hanging Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Former wicker cane warehouse (no longer standing), Durward Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> A man at the warehouse, which was once a board school, encountered a ghostly young boy hanging from a cord attached to a hook on the ceiling.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Annie Chapman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Hanbury Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1930s, reputed to be more common in the autumn<br>
              <span class="w3-border-bottom">Further Comments:</span> Annie Chapman was murdered by Jack the Ripper at 29 Hanbury Street, and it was said that she haunted the area where she was killed, her phantom accompanied by a man. On one occasion during the 1930s, a witness could hear her murder, but could see nothing at the site. A phantom, headless body in the area is also said to be Annie. 29 Hanbury Street has since been demolished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blurred Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - London Jewish Hospital (closed 1979)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1977<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be a former patient, this ghost took up residence in a day room used by nurses and would brush past them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lillian Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Magistrates Court, Aylward Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> A former employee at the court, Mrs Brown still walks where she worked.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Catherine Eddows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Mitre Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, but said to occur late September<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed by Jack the Ripper, Catherine's torn body is still occasionally seen laying in the gutter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Private flat in Shadwell<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2002 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> One resident reported hearing strange taps and bangs which occurred in their property over a nine year period. In 2011, other residents reportedly spotted a black shadow wearing a Cavalier-like hat pass by their doors, while a guard had heard disembodied footsteps on the stairwell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Magpie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Residence along Great Russell Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Every morning between 02:00h - 03:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom magpie is said to tap on the window of a house along this street before manifesting inside. The bird then perches on a floating stick, both bird and baton vanishing the moment a sound is made.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Royal London Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A spectral grey lady has been reported walking around this London Hospital. There is also a tradition ensuring that the shutters are closed at the hospital - if they are left open overnight, it is widely believed someone unexpected dies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Browne</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Stepney Magistrate's Court (now demolished)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1976<br>
              <span class="w3-border-bottom">Further Comments:</span> A former Matron at the court, Browne was blamed for throwing items around and knocking trays of food from people's hands.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2375.jpg' class="w3-card" title='The steps haunted by Judge Geoffreys.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The steps haunted by Judge Geoffreys.</small></span>                      <p><h4><span class="w3-border-bottom">George Geoffreys</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Steps outside of the Town of Ramsgate public house, Wapping High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Geoffreys, the infamous Hanging Judge, was arrested on the steps outside this pub that lead down to the Thames; he was dressed as a sailor, trying to flee the country. His spirit remains trapped on the stairs, reportedly seen by river police.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Annie Chapman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Ten Bells public house (was Jack the Ripper), Commercial Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> The landlord of this pub claimed that Annie Chapman's ghost had taken up residence within the walls. Strange winds were reported, as was mild poltergeist behaviour.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon12490.jpg' class="w3-card" title='Thames close to where Carron Wharf once stood.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Thames close to where Carron Wharf once stood.</small></span>                      <p><h4><span class="w3-border-bottom">Crying Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Thames close to Carron Wharf (closed 1974)<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1897<br>
              <span class="w3-border-bottom">Further Comments:</span> After the discovery of a body in this section of river, for three consecutive nights two witnesses heard crying and could see a large man in the water, waving his arms before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Watcher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - The 'Wood Street Compter' - a basement<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The basement is all that remains of a former debtors' prison. Even though access to the basement is now prevented, people walking past report hearing whispers and the feeling that something is watching them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon148357.jpg' class="w3-card" title='The Town of Ramsgate public house.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Town of Ramsgate public house.</small></span>                      <p><h4><span class="w3-border-bottom">Cellar Noises</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> E1 - Town of Ramsgate public house, Wapping High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Prisoners were once held in the cellar of the Town of Ramsgate, where strange sounds are now said to emerge.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 17 of 17</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Greater London</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View Greater London records" style="width:100%" title="View Greater London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
