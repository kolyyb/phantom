

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Suffolk - it's strange but you'll like it. Ghosts and folklore from the Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Suffolk</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Suffolk Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">A Cyclist Pulls Out</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A12 - Junction between Jay Lane and Rackham's Corner<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1988, late afternoon during winter, again in August 2001<br>
              <span class="w3-border-bottom">Further Comments:</span> Driving back to Great Yarmouth, a driver and his wife watched as a cyclist pulled out in front of their car, while travelling at 70 mph. They hit him, but felt no bump, and could find no trace that the cyclist had ever existed. A phantom jaywalker has also been seen in this area, described as having a weird and horrible leer.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton - Acton Park, The 'Nursery Corner'<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown date, but appears at midnight<br>
              <span class="w3-border-bottom">Further Comments:</span> Midnight, at an unspecified date, and the park gates are said to burst open, letting in a ghostly carriage pulled by four horses which race to the area known as the Nursery Corner. Local legend states it is here that once a vicious battle took place between Romans and Britons.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stalking Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton - Private house along Barrow Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1982<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple living in the house fled after phantom footsteps started to follow the wife around when she was alone in the house. Doors would also open themselves or jam when the human occupiers tried to open them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Iron Chest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acton - Wimbell Pond<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Protecting their hidden hoard of money, if anyone comes too close to discovering this secret stash a white figure appears and cries out 'That's mine!'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Summoned Entity</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Akenham - Church<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local myth states that walking around the church thirteen times anticlockwise will summon the Devil.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cat Attack</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Akenham - Farm Track<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 February 2012, 09:50<br>
              <span class="w3-border-bottom">Further Comments:</span> Police reported that a man had called them saying that a large cat had attacked a deer. The deer had been found with marks around the throat area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf11893.jpg' class="w3-card" title='Off the coast of Aldeburgh, Suffolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Off the coast of Aldeburgh, Suffolk.</small></span>                      <p><h4><span class="w3-border-bottom">Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldeburgh - Coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Much of Aldeburgh has been taken by the sea, leading to the belief that the bells from sunken churches could sometimes be heard under the waves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Airborne Battle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldeburgh - Skies over Aldeburgh<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 04 August 1642, 17:00h - 18:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A large phantom battle occurred in the skies over Aldeburgh; gunfire was heard and a large stone fell from the sky. What happened to this piece of rock is unclear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf385argh.jpg' class="w3-card" title='The sky above Aldeburgh, Suffolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The sky above Aldeburgh, Suffolk.</small></span>                      <p><h4><span class="w3-border-bottom">Round Platform with Handrail around Edge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldeburgh - Somewhere over the town<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1916<br>
              <span class="w3-border-bottom">Further Comments:</span> A female witness watched for 5 minutes as this disk carrying a dozen men dressed like sailors flew overhead, at a height of about 30 feet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man with his Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ampton - Callow Hill Lane<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> A young boy reported that he walked alongside a man and a dog, which both disappeared once they reached this lane.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">One Legged Priest</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashill - House in village; exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> Reputedly haunted by a priest with only one leg, this house received media attention when police responded to an activated burglar alarm; they arrived on the scene only to discover a set of one-legged footprints that disappeared into a wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">ABC</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Assington - Assington, near Sudbury<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> One of many big cat sightings reported in Suffolk during 1996.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lantern</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Badwell Ash - Lane near Ladybrick barn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly lantern which slowly moved down the lane towards parked cars was eventually declared to be nothing more than an optical illusion created by nearby houses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rough Coated Dog with Large Yellow Eyes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barham - Along the main Norwich Road, Barham Church Lane, just past Barham Hall gates.<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1910s?<br>
              <span class="w3-border-bottom">Further Comments:</span> The dog harassed a couple of men walking back from work - one of them reported trying to hit the creature with a stick, but the piece of wood passed straight through the dog's body. Another version of the story states the dog disappears into a solid wall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stagecoach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barham - Lane near Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Observed during daylight hours, a coach is said to travel along a lane here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shuck Haunt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnby - Bridge over Hundred Stream<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Legend states the stream to be the haunt of a Shuck, though no further information could be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Saxon Traitor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnham - Tutt Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The man who helped the Danes take Thetford was rewarded for the treachery with execution by his newfound 'allies'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rolling Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barrow - Stone in pavement near village school<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> New Year's Eve (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A stone set into the pavement here is reported to mark the spot where a highwayman was hanged. Once a year the stone turns itself over on the stroke of midnight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Blunderhazard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barsham - Between Barsham and Norwich<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once per year, just before Christmas, a ghostly member of the Blennerhassett family leaves the village in a coach pulled by headless horses. The phantom would travel to Hassett's Tower in Norwich before returning home before sunrise.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footfalls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barsham - Holy Trinity church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Disembodied footsteps have been heard in the church and are thought to belong to a phantom Georgian woman who is very occasionally seen in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Butler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bawdsey - Bawdsey Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> Soldiers and workers based here during the Second World War are said to have encountered the phantom of a former butler who would stare out from the windows.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">UFO Technology under R&D</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bawdsey - Top secret location<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s/1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A relatively new myth, this location is said to house downed UFO technology, under reverse engineering by UK scientists.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf724.jpg' class="w3-card" title='Beccles Cemetery, Suffolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Beccles Cemetery, Suffolk.</small></span>                      <p><h4><span class="w3-border-bottom">White Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beccles - Cemetery<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1974<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman passing through the cemetery watched a large white dog as it faded away in front of her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Art Critic</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beccles - Crown Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Pictures which fall to the floor without identifiable cause are said to have been moved by this pub's ghost, which can be heard ascending the staircase and moving through rooms in the upper part of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lynx</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beccles - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1991<br>
              <span class="w3-border-bottom">Further Comments:</span> This cat was shot and killed by a local farmer, who claimed both police and the Government asked him to destroy the body and tell no one about the incident.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 421</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=421"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=16&totalRows_paradata=421"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
