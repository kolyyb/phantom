
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Devon Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Devon</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Devon Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Motorbike</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A377 - Between Copplestone and Morchard Bishop<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 22 September 2022, 17:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people travelling along this road could hear a motorbike approaching their car from behind. The bike could not be seen, but clearly heard as it overtook the car and accelerated away. Both witnesses were left shaken by their encounter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Glasses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abbotsham - Thatched Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> As well as the 'normal' sounds of disembodied footsteps, the other phantom sound often reported is the clinking of glasses from an empty room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Abbotskerswell.jpg' class="w3-card" title='A ghostly monk-like figure in a field.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly monk-like figure in a field.</small></span>                      <p><h4><span class="w3-border-bottom">Fading Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abbotskerswell - Area around St Augustine's Priory (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1890s, 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a monk was reported in the nineteenth century as haunting the roads in this area, though has not been reported as of late. A phantom nun was reported in the same area in 2015.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aller (?) - Private flat<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa August 1963<br>
              <span class="w3-border-bottom">Further Comments:</span> After moving in to their flat, a couple were subjected to a manifesting male figure who appeared in their living room. Ornaments would also be moved by an unseen hand. An exorcism drove the ghost out, only for the entity to move into an adjoining flat. The occupants here appear to have been more tolerant and learned to live with the ghost.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alwington - Portledge Manor<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1868<br>
              <span class="w3-border-bottom">Further Comments:</span> At midnight, a ringing church bell woke the Crosse household, who were staying at the manor at the time. The Crosse family asked neighbours about the ringing the following morning, to be told that the bell only rang to foretell of a death in the Pine-Coffin family. It was later discovered that a member of that family had died in India at the time of the ringing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sobbing Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashburton - Bay Horse public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A crying child has been reported in the upper part of the building, as have sounds of children running around.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashburton - Skies north of the village<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 1915<br>
              <span class="w3-border-bottom">Further Comments:</span> Admiralty Intelligence Officers investigated a series of strange lights which were seen in the skies over the area by multiple witnesses. The investigating officers also watched the lights rise into the air and vanish. The officers' report suggested that the lights may have been attached to a balloon used to lift wireless equipment to enable German spies to contact the mainland, although no evidence could be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Death Warning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashleigh Barton - Manor House<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to a death in the family<br>
              <span class="w3-border-bottom">Further Comments:</span> This shade haunts the upper part of the building prior to family tragedy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Emily</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashprington - Waterman's Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two ghosts haunt this site. The first, Emily, is said to be young, and died after being hit by a cart outside the pub. The second phantom is a former landlady with grey hair, and may have manifested to an old landlord.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fireplace Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashwater - Manor Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This hot shade can be seen in the embers of a fireplace here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashwater - Unidentified farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2022, 23:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> While working late and alone, a witness spotted a man standing in the farmyard. The man promptly vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hanging Servant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aveton Gifford - South Efford House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early / mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> One of three shades found here, this servant's shade could be seen dangling from a rope over one of the staircases. The other ghosts are of a naval officer (who can be heard shouting and swearing) and that of a ferryman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady de la Pole</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Axminster - Shute Barton manor house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Hanged in the garden for being a Royalist during the Civil War, Lady de la Pole now continues to walk the grounds of the building. There are also reports of a phantom feline that looks quite normal, until it walks through something solid.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tom Faggus</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnstaple - Ashleigh Road area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people witnessed the ghost of a man riding a large horse in this area. The witnesses carried out some research and believe they had encountered the ghost of Tom Faggus, a Robin Hood-like figure who operated in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Boot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnstaple - Braunton Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> A man staying at this inn for a few nights reported being woken by the sound of someone talking in the room. The witness also said that he had photographed 'part of a leg with a dark boot' in the same room, although the image was accidently deleted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fred</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnstaple - Cinema<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> The spirit at work within this building could be that of a worker who fell from the roof during repair work in 1931 - he died a week after the accident.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Puma</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnstaple - Civic centre<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> This large black cat was seen loitering close to the town centre.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mucay and Justy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnstaple - Guildhall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Ken Mucay and Bob Justy were named by a medium as the two ghosts haunting this council building, both dying in their 50s, though many years separated their deaths.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Photographed Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnstaple - Nineteenth century burial site (may no longer exist)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1909<br>
              <span class="w3-border-bottom">Further Comments:</span> After a rumour started that a ghost had been seen in this disused site, a man claimed to have photographed the spirit rising from a grave. The image was displayed in his window; it is unclear whether any copies of the photograph exist today.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jack</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnstaple - Pilton Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Jack had a fatal heart attack in the park during the early part of the twentieth century. Since then, his shade has been creating mischief in the green space.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beer - Bovey House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> While the smell of lavender is often reported, without any apparent source, the headless ghost, dressed in blue silk, has not been observed for years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smuggler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beer - Private cottage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> After the sound of an impossible to open trapdoor slamming shut woke a family for fourteen consecutive nights, they called a medium who claimed it was caused by a smuggler/fisherman. The entity was told to leave, but could occasionally return, his presence denoted by the smell of smoke. A phantom old woman is also said to haunt the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Door Opener</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belston - Tors Hotel<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Late Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This polt is blamed for slamming doors in the upper part of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-126.jpg' class="w3-card" title='An old postcard showing Berry Pomeroy Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Berry Pomeroy Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Blue Lady de Pomeroy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berry Pomeroy - Berry Pomeroy Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> After being raped by her father, this former member of the de Pomeroy family now haunts the ruins of the building for the crime of killing her own child. The child has also been heard crying and screaming in the building. The castle also lays claim to possessing a phantom cavalier, and a ghostly black dog.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Margaret</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berry Pomeroy - Dungeon, Margaret Tower, Berry Pomeroy Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Left to starve to death in the dungeon by her sister (they both chased after the same suitor), Margaret now haunts the area. Visitors have reported feeling something brush past them on the staircase down to the room.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 429</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=429"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=17&totalRows_paradata=429"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
