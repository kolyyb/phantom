
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Lancashire, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northwest.html">North West</a> > Lancashire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Lancashire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nondescript Shape</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Accrington - 38 Pendle Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1965-1966<br>
              <span class="w3-border-bottom">Further Comments:</span> This house was said to be haunted by a strangely shaped entity which glowed and generated a feeling of unease. Bumps and scratching sounds were also heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Accrington-Black-Abbey-Road.jpg' class="w3-card" title='A ghostly woman in Lancashire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly woman in Lancashire.</small></span>                      <p><h4><span class="w3-border-bottom">Screaming Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Accrington - Black Abbey Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly figure drifts quietly down the road, screaming only if approached. She was burnt to death after having a love affair with one of the monks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Out Dated Machinery</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Accrington - Broad Oak Printing Works<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> After a night watchman reported an icy entity which took hold of his arm, a tape recorder was left in the same room to see what it would pick up. On playback, the tape was filled with the sounds of working print equipment, though nothing had been activated whilst the recording was in action.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Brown Haired Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Accrington - Cemetery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> A girl with brown hair and blue eyes was seen to vanish by two witnesses visiting a grave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Apple Rain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Accrington - East Crescent<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 8/9 November 1984<br>
              <span class="w3-border-bottom">Further Comments:</span> Over a period of an hour an estimated three hundred apples rained down on several houses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chilly Room</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Accrington - Spinning Jenny Club, theatre lounge (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Visitors to the building would feel freezing air when entering the room, even when the temperature outside rocketed. Lights would also turn themselves off at the most inopportune moments.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ainsdale - Road to Birkdale<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman driving home from work spotted a young man standing on the roadside. The man waved at her before vanishing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman's Skull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appley Bridge - Skull House<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This item is said to be impossible to remove from the building - it always teleports back!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thirty-Something Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashworth Valley - Heywood/Rochdale Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 July 2008, 22:15h<br>
              <span class="w3-border-bottom">Further Comments:</span> A black animal (either a dog or a large cat) was seen running across this road followed by a woman in her thirties. The animal disappeared into a wall, and the woman vanished as a car braked hard to avoid hitting her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Astley-canal.jpg' class="w3-card" title='One of the UK&#039;s canal ghosts.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> One of the UK&#039;s canal ghosts.</small></span>                      <p><h4><span class="w3-border-bottom">Anne Mort</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Astley - Area near the canal<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen walking by the canal waters, this grey lady may be Anne who died of a broken heart.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Molly</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bacup - Flowers public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Investigated by the Paranormal Activity Research Team of Lancashire, the pub is home to the ghost of 'Molly' - a spirit who has manifested along the corridor leading to the toilets. Several other entities were also detected by the team, including an elderly man with a limp and a little girl who died of scarlet fever.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Whistling</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bacup - Lane near Burnley Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It is said that walking this lane during a quiet day, or at night, whistling can be heard, followed by breathing which increases in heaviness until it sounds like someone is standing by your ear. The sounds are said to be created by a woman who died of a broken heart after losing her husband and child.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bacup - Royal Court Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 April 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> A full investigation of this theatre was carried out by the Paranormal Activity Research Team of Lancashire. Numerous entities were detected by the team, including a woman named Kitty and a man named Jackson by the Circles seating area, a doctor-like figure in reception, and other presences throughout the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cat Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bacup - Weir area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One night, on a farm in the area, a cat had been found stealing the cream and the owner of the property had thrown several stones at the creature. The cat escaped but was hit in a few places by the stones. The following day, at a neighbouring farm, the farmer's wife was found covered in bruises. There could only be one explanation - the bruised woman was a witch who could transform into a cat!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ralph's Wife</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banks - Ralph's Wife's Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Dark winter nights (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Ralph was either a smuggler or a fisherman - either way, he vanished at sea. The ghost of his wife is said to walk between St Stephens Church to Fiddler's Ferry, carrying a lamp so he can find his way home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lancaster Bomber</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnoldswick - Area near Rolls Royce's Bankfield factory, and area towards Craven<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> Around thirty witnesses claimed to have seen a silent, grey coloured aircraft resembling a Lancaster Bomber moving silently through the sky. The accounts were virtually all isolated and spanned the month.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Muttering Yokel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barton on Irwell - Olde Rock House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A royalist fleeing his Cromwellian pursuers disguised himself as a farmer and walked about the area muttering 'Now thus, now thus', until the roundheads had passed. His shade still re-enacts his escape.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Dawson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bashall Eaves - Fields & Hedgerows in area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Shot in the back in 1934 by an unknown party, John Dawson's ghost now looks for evidence around the village that could help identify the killer.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bebington - Church and graveyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> These robed figures float above the ground, 'walking' where the path was once much higher.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Father and Son</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bebington - Leasowe Castle (also known as Mockbeggar Hall)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Many years ago, this couple were captured by the castle's owner. Rather than be tortured, the father killed his son and then killed himself - their ghosts have since been reported standing over peoples' beds.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bebington-Poulton-Road.jpg' class="w3-card" title='A ghostly nun on the road.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly nun on the road.</small></span>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bebington - Poulton Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 1970<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom woman is believed to be a nun killed while travelling back to the nunnery. A driver in 1970 stopped his car when he saw a woman standing in the middle of the road. She vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George Lyon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Billinge - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding on his horse, this former highwayman passes through the village where he once hid out.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drunken Cavalier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Billinge - Stork Hotel (aka Stork Inn)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> This drunken Cavalier was once seen in the men's toilets and can sometimes be heard moving around at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Elevator</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birkdale - Palace Hotel (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 1969<br>
              <span class="w3-border-bottom">Further Comments:</span> The hotel, in particular the lift and the second floor, suffered from ghostly activity while demolition of the building was occurring; the lift often moved on its own accord (or failed to move when required), even though all power to the building had been cut. The ghost was named as the designer of the building, who leapt from the roof after he discovered the hotel had been built facing the wrong way.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birkenhead - Former Cammell Laird site, Merseyside<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> Lights turn themselves on and off, door open and close, and a handful of staff have reported seeing a strange old lady walking around the site. Men in boiler suits have been watched walking past windows only to vanish shortly after.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 368</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=368"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=14&totalRows_paradata=368"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northwest.html">Return to North West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North East & Yorkshire</h5>
   <a href="/regions/northeastandyorks.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/secondlevel/yorkshire.jpg"                   alt="View North East & Yorkshire records" style="width:100%" title="View North East & Yorkshire records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
