
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Durham Ghosts and Strangeness from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northeastandyorks.html">North East and Yorkshire</a> > Durham</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Durham Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - A1 heading towards Durham<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> A horse-mounted highwayman stood on the roadside, looking towards Durham. The legs of the horse vanished at the knee, and the ghost vanishes if approached.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">St Cuthbert's Mist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - All over city<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 May 1942 (night)<br>
              <span class="w3-border-bottom">Further Comments:</span> A Luftwaffe mission to bomb the city failed after a mist appeared and covered the castle and cathedral, preventing them from being hit. It is said that St Cuthbert created the mist to protect his relics.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Teenage Tearaway</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - Area around the Old Rectory<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Late nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A postman spotted a ghostly shrouded child as he walked past the graveyard - he panicked and ran, the ghost in pursuit. Reaching home, the postman dived into his bedroom where both he and his wife barricaded themselves in. The phantom could be heard smashing up the room next door; both people took the opportunity to run away. A local wise woman who was summoned convinced the ghost to return to the grave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - Crook Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The staircase here is reportedly haunted by a fleeting female in grey, while a beautiful woman in white has been seen in other areas of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-129.jpg' class="w3-card" title='An old postcard of Durham Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Durham Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Grey Shadow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - Durham Castle, black staircase<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Reportedly the wife of one of the former Prince Bishops of Durham, this figure haunts the black staircase where she fell and broke her neck.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/DurhamSanctuaryKnocker.jpg' class="w3-card" title='A reproduction of the Sanctuary Knocker at Durham Cathedral: a &#039;cat headed&#039; demon with three tongues (private collection).'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A reproduction of the Sanctuary Knocker at Durham Cathedral: a &#039;cat headed&#039; demon with three tongues (private collection).</small></span>                      <p><h4><span class="w3-border-bottom">Passage of Horror</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - Durham Cathedral<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A secret passage is said to run from the cathedral to Finchale Abbey, though the journey is so terrifying no one can survive the journey. The Sanctuary Knocker at the Cathedral is hundreds of years old and appears to show a demonic entity with three tongues.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Inmate</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - Durham Prison<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A ground floor cell is said to be haunted by the scene of one prisoner murdering another with a table knife.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Northumbrian Pipes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - Elvet Bridge (including Jimmy Allen's club)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The horse rustler Jimmy Allen died in this former cell days before his pardon arrived - he can still be heard playing on his pipes from beneath the bridge. The ghost of another man, who mysteriously vanished, would be spotted at the base of the bridge, standing on the river's banks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - HMP Low Newton Prison<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 26 February 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> A prisoner reported seeing a woman standing by a window in their cell. A guard later told them that someone had committed suicide in the spot the figure had stood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Violet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - Kerry Foods Factory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom woman who suddenly disappears when spotted was nicknamed Violet after the scent in the air which she leaves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - Langley Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen galloping towards the hall, this coach is pulled by four black, headless horses. One legend says the coach contains a coffin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Twisted Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - North Bailey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Emerging from the cellar of a property once owned by the city's chief constable, this little man, twisted and bent over, was said to wear a white ruffled shirt, black trousers and a nightcap.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in a Black Dress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Durham - Private residence, Coatsworth Road<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> A sixteen year old girl screamed for her mother after seeing a phantom woman wearing a black dress appear in a bedroom. The following morning the family received a phone call informing them the girl's grandmother had died the night before, around the same time as the sighting.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 13 of 13</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northeastandyorks.html">Return to North East and Yorkshire</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North West England</h5>
   <a href="/regions/northwest.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/northwest.jpg"                   alt="View North West records" style="width:100%" title="View North West records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
