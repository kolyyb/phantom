
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Northampton Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastmidlands.html">East Midlands</a> > Northampton</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Northampton Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Auctioneers Public House, Market Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1999<br>
              <span class="w3-border-bottom">Further Comments:</span> A former landlord and his wife reported hearing the cries of children and the smell of burning timber - it is speculated that this ghost sighting relates to the Great Fire of Northampton, when in 1675 the buildings around Market Square were burnt to the ground.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Between Spinney Hill and Bailiff Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> Driving back in daylight from shopping, this witness and the driver of another car had to break hard after a tall figure stepped off the kerb and crossed the road. Once it reached the other side, the figure vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Possessed Cabinet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Birchfield Road East (private house), and 330 Wellingborough Road (Trends furniture shop)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2003<br>
              <span class="w3-border-bottom">Further Comments:</span> This cabinet, sold to a house along Birchfield Road East, was quickly returned to the shop after the owner said that the figure of a woman dressed in 1930s clothing materialised several times by it. Similar occurrences happened at the shop after the cabinet came back.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty forms</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Black Lion public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Many of the pub's owners have reported lights that turn themselves on and off and pets that refuse to go into the basement. A misty man with a black dog was spotted in one room and a phantom woman was encountered on the stairs.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Rain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Bulwick Rectory<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 16 July 1850<br>
              <span class="w3-border-bottom">Further Comments:</span> Black rain fell on the rectory, twenty four hours after the building had been shaken for an hour by a loud rumbling.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Running</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Commercial building, Sixfields area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness heard footsteps sprinting towards their position, but when they turned around, no one could be seen. The sound of the footsteps continued to echo around the area for several seconds after they had ceased.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/northh6382.jpg' class="w3-card" title='Empty building near Market Hall, Northampton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Empty building near Market Hall, Northampton.</small></span>                      <p><h4><span class="w3-border-bottom">Sounds of Movement</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Empty building near Market Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> There have been several reports of the sounds of people moving around this building in the dead of night, even though it remains totally boarded up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/northh3535.jpg' class="w3-card" title='Grosvenor Centre, Northampton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Grosvenor Centre, Northampton.</small></span>                      <p><h4><span class="w3-border-bottom">Floating Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Grosvenor Centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Built on the site of an old monastery, it was perhaps inevitable that the building would become haunted by the figure that has been seen by cleaning staff after shopping hours.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady with Gloves</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - House at 18 Horsemarket, no longer standing<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> This lady, dressed in black lacy dress with white gloves, was seen several times over a fifty year period.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Man with the Flickering Lantern</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Hunsbury Hill Country Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple out walking their dog after dusk encountered a tall man wearing a long coat and carrying a flickering lantern. One of the couple spoke to the man but received no reply. The couple turned to each other and then back to the man, but he had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elderly Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - King William public house (aka the King Billy)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> An elderly lady has been reported walking along the main corridor in the upper part of this pub. The Northants Haunted group conducted a seance on site, revealing that six spirits remained in the building, one of whom had the initials JG.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cyclist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Kingsland Avenue<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1994<br>
              <span class="w3-border-bottom">Further Comments:</span> While waiting at a crossroads, a witness watched a man in a cap and Plus-fours breeches, ride past on a penny farthing cycle. The cyclist faded away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Notre Dame Convent School<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A former teacher at the school, this shade now walks knee deep across a hall that was raised after her death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Old Jolly Smokers public house, no longer standing (was located at 25 The Mayorhold)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Accompanied by the smell of incense, this brown hooded figure haunted the pub before it was demolished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swinging Forks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Private flat, St Michaels Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2011?<br>
              <span class="w3-border-bottom">Further Comments:</span> One resident reported a presence in a room, while knives and forks would swing on their stand without reason. Footsteps would also be heard, and doors would open on their own accord.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banging and Scratching</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Private residence, Abington area<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980s, always during winter<br>
              <span class="w3-border-bottom">Further Comments:</span> A former resident that the winter months would be haunted by scratching from the bottom of the staircase, bangs from the basement, and doors which would open themselves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/northh3536.jpg' class="w3-card" title='Royal Theatre, Northampton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Royal Theatre, Northampton.</small></span>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Royal Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1999<br>
              <span class="w3-border-bottom">Further Comments:</span> No theatre would be complete without at least one shade, and this female entity is blamed whenever the electrics play up or props are moved.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/northh3719.jpg' class="w3-card" title='Shipman&#039;s Pub, Northampton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Shipman&#039;s Pub, Northampton.</small></span>                      <p><h4><span class="w3-border-bottom">Harry Franklin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Shipman's Public House<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A former landlord who took his own life in a 'horrific manner', Harry can be heard walking above the heads of the locals as they sip their pints.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Foggy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - St Giles' Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1965<br>
              <span class="w3-border-bottom">Further Comments:</span> A schoolboy and his friends spotted the upper half of a 'foggy grey' figure in front of the alter, moving towards the windows of the church. The witnesses all fled.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Strafford</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Wheatsheaf Hotel<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1645<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing to King Charles I twice before the Battle of Naseby while he stayed in this hotel, Strafford tried to give the King advice concerning the upcoming battle. Charles I ignored it, and lost the fight. A public house in Daventry also lays claim to this event.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/northh3720.jpg' class="w3-card" title='Wig &amp; Pen pub, Northampton.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Wig &amp; Pen pub, Northampton.</small></span>                      <p><h4><span class="w3-border-bottom">Cellar Dweller</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Northampton - Wig & Pen public house, Saint Giles Street<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The entity in the basement rarely manifests on the upper levels and is happy to scare any dogs that try to enter its domain.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 21 of 21</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastmidlands.html">Return to East Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="ViewWest Midlands records" style="width:100%" title="View West Midlands records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
