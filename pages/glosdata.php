

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Glouchestershire Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Gloucestershire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Gloucestershire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldsworth - Larkethill Wood<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Possibly the same entity that haunts the park, a coach and team of horses are said to erratically travel along the road which skirts the woodland.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crump</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldsworth - Lodge park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir John Dutton, nicknamed Crump because of his hunched back, is said to drive a phantom coach and horses under the gates to the park. A white stag may also haunt the parkland.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Budge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arlingham - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 June 1902<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Mrs Budge, wearing traditional Victorian housekeeping garb, was seen sitting in the church. When spoken to, she vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Little Old Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arlingham - Parish church and vicarage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing black clothing and a white lace cap, the phantom old woman who has been seen in these buildings is said to give off an icy aura.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arlingham - Slowwe House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> One owner of the property reported to the local press that she had seen a lady wearing a long grey dress around the house. Poltergeist activity was also reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Funeral Procession</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arlingham - Through village, heading towards Arlingham Manor<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 May 1757<br>
              <span class="w3-border-bottom">Further Comments:</span> Exactly a year after the phantom funeral procession was seen pulling up outside the manor house, the last heir of the family living here died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Funeral Procession</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arlington - Manor House<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 May 1757<br>
              <span class="w3-border-bottom">Further Comments:</span> This funeral was said to be an ill omen for the family who owned building - exactly a year after the manifestation was seen by several people, the last male heir died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming Parliamentarians</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aust - Area near the Severn Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This group of phantom soldiers are said to have bullied a ferryman into taking them across the river - he tricked them into leaving his boat too early, and they were washed away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Running</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Avening - Long Stone (Hampton Fields) and Tingle Stone<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Both these stones are said to run around their fields as the clock strikes midnight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lurking Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Avening - Road between village and Minchinhampton<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be the shade of a suicide, this ghost waits alongside the road, but for what is anyone's guess.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-4.jpg' class="w3-card" title='An old postcard of Berkeley Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Berkeley Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Edward II Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berkeley - Berkeley Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 September (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered on orders of his wife Queen Isabella (her fate is documented elsewhere in this database), the anniversary of the King's death is marked by his screams echoing around the castle. The skin of an animal was once on display near the entrance; a local legend said it belonged to a giant toad discovered in the dungeon, although it was more likely to belong to a seal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jenner</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berkeley - Edward Jenner Museum<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> May 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> BBC photographer Chris Sandys caught a strange humanoid shape on film while creating a panoramic photo within the museum. Some believe the image to show Jenner himself, while others believe the image to be an illusion caused by shadow and light.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Seaweed</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Berkeley - The Common<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> Seaweed rained down on this street during a storm. It was speculated that the seaweed was picked up by a weather spout from Clevedon Beach in Somerset, although media outlets did not say if the seaweed was accompanied by any other beach debris, such as sand or marine animals.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coffin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bibury - Saltway Barn Barrow<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A golden coffin is said to be buried in this long barrow.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Three Skeletons</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Birdlip - Barrow Wake, Crickley Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 1879<br>
              <span class="w3-border-bottom">Further Comments:</span> Three skeletons, two male and one female, were discovered here in 1879. Objects found with the bodies included a silver brooch and a mirror, the value of which led some to speculate the grave belonged to Boudica.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moved Church</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bisley - Church Piece<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Church still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The church once stood at Church Piece until someone (Devil or angel) moved the building to where it now stands. As a side note, a Roman structure was found at Church Piece, and parts of a Roman altar found in the church walls.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bisley - Money Trump<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people passing the trump reported seeing several phantom headless men. Treasure is said to be hidden here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Financial Conversation</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bitton - Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness recalled how he stood outside the vestry door, listening to two men talking about money. When he knocked and walked in, the conversation stopped, and the room was empty.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bourton-On-The-Water - St Lawrence's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 10 March 2014, 22:20h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two brothers photographing the church by night heard footsteps approaching them on the gravel. They moved aside to let the person pass, but it quickly became apparent no one was there. The brothers could then make out the faint outline of a hooded figure coming closer to where they stood, vanishing when six feet away. The footsteps also ceased, and the brothers were left in silence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banging</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cam - Private residence, Woodfield Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1969 - 1972<br>
              <span class="w3-border-bottom">Further Comments:</span> Initially, the family living here believed the bangs and crashes they heard throughout the house were created by their neighbours. One visitor heard crockery being smashed, although not a single broken item could be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man in Trilby</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cambridge - Private house, Bristol Road<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> After modernising the building, the owners became aware of the presence of an old man, who materialised before the mistress of the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Mourning Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Charfield - Charfield graveyard, memorial to those killed in the Charfield Station rail crash<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This woman in black, who covers her face with her hands, is widely believed to be the mother of two children killed in a rail disaster which occurred in 1929. Ten people, including the children, who were killed in the train crash could not be formally identified and are buried in a mass grave.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/glou11451.jpg' class="w3-card" title='Everyman Theatre, Cheltenham.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Everyman Theatre, Cheltenham.</small></span>                      <p><h4><span class="w3-border-bottom">Man in 1940s Suit</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cheltenham - Everyman Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> Walter Charnock, a builder working on the renovation of the theatre, encountered a figure wearing a 1940s suit leaning on a banister. Within thirty seconds, the figure vanished. Music is also said to be heard within the building when none should be playing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cheltenham - Former private school in the town, now flats<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940<br>
              <span class="w3-border-bottom">Further Comments:</span> A teacher and headmaster at the school, working late, both saw a phantom nun crossing the playground - they approached the figure to speak to it, but when their torch ran out of batteries, they ran away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Lady in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cheltenham - Garden Reach house, Pitville Circus Road (currently called St Annes?)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 1882-1892<br>
              <span class="w3-border-bottom">Further Comments:</span> During the original ten year spell, the woman in black was seen dozens of times, during both daylight and night-time hours. Those who could not see her often reported disembodied footsteps walking down corridors. The ghost may have returned briefly mid-twentieth century; a similar figure was seen by a man, and then again by his brother a couple of years later.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 193</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=193"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=7&totalRows_paradata=193"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
