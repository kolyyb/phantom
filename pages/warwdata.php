

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Warwickshire, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/westmidlands.html">West Midlands</a> > Warwickshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Warwickshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Raincoat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alcester - A435, road near Coughton Court<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple driving along past the court spotted a pale middle aged woman standing by the roadside wearing a beige raincoat. She stepped into the road before vanishing. The figure has also been seen pushing a bike; another motorist reported hitting her as she stepped out of nowhere and crashed his car as he spun after trying to avoid her. A villager first on the scene reassured the driver that he was one of several people to have encountered the ghost that month. Separately, a phantom coach has also been reported along the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pink Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alcester - Coughton Court, the Tapestry Bedroom area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Reportedly exorcised in the early 1900s, the pink lady, nor the sounds of her footsteps echoing down the nearby corridors, has been detected in recent years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Secret Tunnel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alcester - Lord Nelson public house (as of May 2019, closed)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local rumour said that this public house had a secret tunnel running to a nearby religious site. It was also said the place was haunted, although details are sketchy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alcester - Ragley Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman in white is said to appear at midnight, sit for a while, and move off to a nearby stream to drink.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Thrown Table</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alcester - Town Hall<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A table was reported to have been thrown at a visitor, and sounds heard from empty rooms.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tutting Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alcester - Whispering Witch shop<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> As well as the tutting lady, this show is reputedly haunted by something in the basement which pushes and pokes visitors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Alcocks-Arbour-hill.jpg' class="w3-card" title='The demonic cockerel of Warwickshire.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The demonic cockerel of Warwickshire.</small></span>                      <p><h4><span class="w3-border-bottom">Cockerel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alcocks Arbour - Area around the hill<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A demonic cockerel stands guard over a hidden treasure hoard within the hill - the last man to discover it was eaten by the bird.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Can</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alscot - Parkland and roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghost-like entity said to be part calf, part man, was said to haunt this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alveston - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Late Nineteenth Century<br>
              <span class="w3-border-bottom">Further Comments:</span> Charles Walton (the man murdered in Lower Quinton (see Quinton database entry for further information)) claimed to have seen a phantom black dog for several successive nights when he was younger - on the last night, instead of the dog, he saw a ghostly headless woman. He soon heard that his sister had died at roughly the same time as the last sighting.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alveston - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered alongside his peddler master, this glowing white hound was said to haunt the lanes close to Alveston.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">William Hirons</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alveston - Hiron's Hole, close to Littleham Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> After November 1820<br>
              <span class="w3-border-bottom">Further Comments:</span> William Hirons was murdered and left on the roadside, with his head concealed in a hole. His murderers were found and executed, but William was still heard to call out for help at night. The hole where William was found was said to be unfillable.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alveston - Mill lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> A father and son out camping close to the River Avon were woken at around 03:00h by singing. The voice sounded like plainchant and was just outside the tent. After a while, they opened the tent and the singing stopped - nothing could be seen, other than empty open fields surrounding them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pilot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ansty - Ansty Aerodrome<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996, 19:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A figure wearing a Second World War pilot's suit walked past one witness as he stood by a photocopier. The figure then passed through a pair of closed doors leading outside and disappeared in a field. The pilot is rumoured to be a Polish airman who hanged himself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smashing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ansty - Ansty Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1998 (crockery), other incidents not known<br>
              <span class="w3-border-bottom">Further Comments:</span> A guest in the hall reported hearing crockery being smashed in a nearby room, although on investigation, nothing could be found. Footsteps and voices without a discernible cause are also heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Duke of Suffolk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Astley - Astley Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Father of Lady Jane, Henry Grey, Duke of Suffolk, had his head lopped off and now haunts this building (headless, of course).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Willie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Astley - Church of St Mary the Virgin<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be the ghost of a monk, this phantom vanishes so quickly after being spotted that witnesses have problems describing what he looked like.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Farmer, Farmer, Farmer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atherstone - A34, between Atherstone and Alderminster<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a farmer haunts this area - he was killed after hitting a low tree branch while riding his horse at breakneck speed. Legend says if seen once, it can be guaranteed that the wraith will be seen two more times.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Teenage Face</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atherstone - Private house along Royal Meadow Drive<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1981<br>
              <span class="w3-border-bottom">Further Comments:</span> This house was said to be haunted by the ghost of a teenager who committed suicide. One occupant in the building watched the phantom face of an adolescent youth hover around their living room for several hours.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dead Farm Animals</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Austrey - General area<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> Attacks on animals, including a dead pregnant sheep which had been stripped to the bone, convinced many farmers in the area that a large cat prowled the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sheet Puller</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barford - Westham House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1961<br>
              <span class="w3-border-bottom">Further Comments:</span> A guest at the house, staying in the former maid's quarters, lay in his bed half asleep when he heard someone enter his room. The covers of his bed were whipped off and thrown across the room; the man quickly turned the light to see who had removed the covers, but no one was there.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Firelighters</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bearley - Private residence close to Snitterfield Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> While sitting in front of the fire, a couple watched as a pack of firelighters lifted into the air and silently passed in front of them, setting itself down nearby. No other activity was reported after this event.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bags of Gold</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bearley (was Bearly) - Windmill (current status not known)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a former miller would hold down a flagstone, under which several bags of gold were said to be hidden. The hoard was only said to be accessible at the stroke of midnight during a full moon, when the phantom miller would take his ghostly horses to a pond to water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedworth - Bedworth Road, aka Rabbit Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Seen 2005, 28 May 2012, 16 December 2012 (01:15h)<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman left traumatised after being attacked along the road later committed suicide. Her phantom is said to stand by the roadside, waiting for a lift. One witness who spotted her said she wore a long white dress, possibly a nightdress, and had long black matted hair. Another driver heading towards Astley in May 2012 said she spotted a figure on the roadside with her hands against her face which was holding a dreadful expression. Two more witnesses in December 2012 described a figure standing by the road with long black matted hair, although this time the figure was dressed in black.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nun.jpg' class="w3-card" title='An illustration of a nun holding a cross, by Wayne Lowden.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An illustration of a nun holding a cross, by Wayne Lowden.</small></span>                      <p><h4><span class="w3-border-bottom">Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedworth - Bellairs Avenue and Dowty Avenue<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> June 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking down Bellairs Avenue with his brother, this witness could see a nun walking towards them in the middle of the road. He commented on the figure to his brother, who could not see her. As a car drove up the road, she vanished. The same witness said he spotted the same figure a few weeks later in Dowty Avenue.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Short Swordsman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedworth - Coal Pit Fields, exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1950s / early 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> A young girl witnessed a man of short stature and dressed in Cavalier clothing carrying a sword. The phantom was surrounded by a green glow and beckoned the child to approach. Her reaction to this is undocumented.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 141</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=141"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=141"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/westmidlands.html">Return to West Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="View West Midlands records" style="width:100%" title="View West Midlands  records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>East Midlands</h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
