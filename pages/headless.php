



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Headless ghosts and folklore, and where to find them, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Headless Ghosts</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Headless Ghosts across the Lands</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth (Dyfed) - General area of Penparcau<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This headless dog was said to have once belonged to a young giant who ran so fast that he decapitated the dog by pulling too sharply on the leash.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Abington-Pig-Lane.jpg' class="w3-card" title='A phantom coach and horses.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom coach and horses.</small></span>                      <p><h4><span class="w3-border-bottom">Coach with Headless Driver</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abington (Northamptonshire) - Pig Lane (lane may no longer exist or has been renamed?)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Racked with guilt after deliberately running over his daughter's lover in 1780, this man now continues to drive his coach and horses down the lane, though he has lost his head since death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alveston (Warwickshire) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Late Nineteenth Century<br>
              <span class="w3-border-bottom">Further Comments:</span> Charles Walton (the man murdered in Lower Quinton (see Quinton database entry for further information)) claimed to have seen a phantom black dog for several successive nights when he was younger - on the last night, instead of the dog, he saw a ghostly headless woman. He soon heard that his sister had died at roughly the same time as the last sighting.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashbourne (Derbyshire) - Hanging bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wayne Anthony, in Haunted Derbyshire and the Peak District, writes that the bridge is said to be haunted by two ghosts. A headless figure is sometimes seen standing on the bridge, while another ghost is seen leaping off the edge into the River Henmore.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Twelve Pallbearers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford in the Water (Derbyshire) - Shady Lane, near Thornbridge Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> These twelve men carrying a coffin are said to be headless. The coffin is also empty, the space reserved for any witnesses to the spectral sight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Towndrow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashover (Derbyshire) - Church and churchyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1890<br>
              <span class="w3-border-bottom">Further Comments:</span> The headless entity spotted in this location was believed to be the Mrs Towndrow who was murdered in 1841 by her husband John shortly before he killed himself. A local legend states that of you thrice circumambulate an empty stone coffin found in the churchyard before sitting in it, you can hear the restless dead.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashwell (Hertfordshire) - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This shadow of a shambling headless person reportedly stalks the churchyard here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Duke of Suffolk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Astley (Warwickshire) - Astley Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Father of Lady Jane, Henry Grey, Duke of Suffolk, had his head lopped off and now haunts this building (headless, of course).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Highwayman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Atwick (Yorkshire) - Roads in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This decapitated spectral bandit remains lurking in the shadows of his old ambush area. One version of the story says he remains on his horse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Audley End (Essex) - Audley End Road & Chestnut Avenue.<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The coach departs the Lions Gate and is driven along the local streets until it reaches the B1383. As normal, the coach driver is said to be headless.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/wilt3609c.jpg' class="w3-card" title='Silbury Hill, Avebury.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Silbury Hill, Avebury.</small></span>                      <p><h4><span class="w3-border-bottom">Burial</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Avebury (Wiltshire) - Silbury Hill<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Hill still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The largest man made mound in Europe, no one can say for sure why the hill was built, though one group has alleged that it was the only way the builders knew of immobilising a great evil. The ghost of King Sil is also said to ride around the base of the mound on moonlit nights, which is likely to be connected to the belief that a horse and rider of solid gold are contained within. Finally, another ghost, this time headless, is said to haunt the base.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Thomas_Boleyn.jpg' class="w3-card" title='Thomas Boleyn, as pictured on his tomb.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Thomas Boleyn, as pictured on his tomb.</small></span>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylsham (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ballyfin-lake.jpg' class="w3-card" title='A ghostly figure standing in a lake.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly figure standing in a lake.</small></span>                      <p><h4><span class="w3-border-bottom">Headless Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyfin (County Laois) - Lake<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom man who haunted the area was banished into the lake by a local priest. At some point during the banishment, the ghost lost his head.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymena (County Antrim) - Road leading to the White Gates, Crebilly Road area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A robber escaping from a house rode into a thin piece of wire tightly pulled between the gateposts here, losing his head in the process. His decapitated shade is either seen or heard at Halloween, still on horseback.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Blunderhazard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barsham (Suffolk) - Between Barsham and Norwich<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once per year, just before Christmas, a ghostly member of the Blennerhassett family leaves the village in a coach pulled by headless horses. The phantom would travel to Hassett's Tower in Norwich before returning home before sunrise.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baschurch (Shropshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, but may have been heard 17 October 2009, around 22:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Having hanged himself, this headless gentleman is reported to still drive his horse and trap around the neighbourhood. One witness heard a horse passing close to her late at night, but nothing could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bawnboy (County Cavan) - Exact area not known<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Possibly twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> An area of the village was said to be haunted by a headless monk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf777bles.jpg' class="w3-card" title='Roos Hall, Beccles, Suffolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Roos Hall, Beccles, Suffolk.</small></span>                      <p><h4><span class="w3-border-bottom">Headless Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beccles (Suffolk) - Roos Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Christmas Eve is marked by the arrival of a coach driven by a headless horseman and pulled by headless horses, which travel down the driveway and vanish as they arrive by the front door. Close by, an oak known as Nelson's Tree is reputed to have been used as gallows and is haunted by a woman in white, although it is not clear whether she was one of the tree's victims or mourns the loss of a loved one. In the hall itself, on a wall within in a bedroom cupboard, there is the imprint of the Devil's hoof branded into solid brick. Finally, another tale says there is a window at the hall which always opens itself, even if locked shut.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beer (Devon) - Bovey House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> While the smell of lavender is often reported, without any apparent source, the headless ghost, dressed in blue silk, has not been observed for years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beetham (Cumbria) - Between village and Milnthrope, and surrounding countryside<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> It is said that if this large creature follows you, death is not far behind.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belaugh (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Eoghann a' Chin Bhig</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ben Buie, Isle of Mull (Argyll and Bute) - General area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Prior to disaster in the MacLean family<br>
              <span class="w3-border-bottom">Further Comments:</span> Eoghann, while living, tried to kill his father - he was decapitated for his efforts. Now he appears before personal disaster or a death in the family.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bentley (Suffolk) - Bridge on road linking Brantham and Bentley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once based at Dodnash Priory that once stood nearby, this monk now haunts the road between two villages. A single stone remains at the site of the priory, and it is said treasure is concealed underneath.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jocelyn Percy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beverley (Yorkshire) - Roads in the village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sir Jocelyn has been reported driving his coach and four headless horses around the streets of this otherwise quiet neighbourhood. Another version of the legend says he flies above the street, pausing briefly over an unnamed house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Laughing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bickleigh (Devon) - Fisherman's Cot public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The soft laughter of an invisible female has been heard around the bar, which is unrelated to the headless horseman who lurks outside.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 382</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=382"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=15&totalRows_paradata=382"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
