

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Staffordshire Ghosts, Folklore and Weird Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/westmidlands.html">West Midlands</a> > Staffordshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Staffordshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bushy Bearded Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abbots Bromley - Royal Oak Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This cloaked, bearded gentleman was seen lurking around the attic and one of the bathrooms. A few unearthly notes from a non-existent music box have been heard in the bar area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Madam Vernon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Adbaston - The Lea house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Madam Vernon's ghost returned soon after death, upset at how her family had split her wealth and land. The spirit caused much trouble, until clergymen banished the entity to a pond under the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Men</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barton under Needwood - Former RAF Tatenhill, now a business park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2008-2010<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness who works on the site has reported the smell of old aftershave in the corner of a warehouse which is only detectible at 20:30h, and the sounds of young men in disused buildings.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bilston-greyhound.jpg' class="w3-card" title='Ghostly woman on the stairs.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ghostly woman on the stairs.</small></span>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bilston - Greyhound and Punchbowl public house, was once Stow Heath Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Prior to becoming a public house, the staircase of the manor house was haunted by a woman in a white gown. While it is not clear whether the ghostly woman remains, more recently a baby has been heard crying and mild poltergeist activity reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hooded Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bobbington - Road heading towards Wombourne<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver and his passenger spotted a monk-like figure run across the road in front of them and disappear into an open field.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bradley-Webb-Stone.jpg' class="w3-card" title='The Devil failing to steal stones.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Devil failing to steal stones.</small></span>                      <p><h4><span class="w3-border-bottom">Dropped Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradley - Webb Stone, also known as the Wanderer Stone<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Stones still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The three stones here were taken from a church by the Devil to extend hell. They proved too heavy, and he dropped them where they now stand. Another legend says that if anyone tried to move the Webb Stone, farmland would become less productive, while a third story says that any girl who baked a cake on All Saint's Eve and left it on the stone would dream of their future husband.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grave Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradnop - Road behind Oxhay Farm<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The site where a Jacobite was murdered by a companion is now haunted by a phantom black dog.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hound with Pointed Ears</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brereton - Coal Pit Lane, and roads leading to Brereton<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1972, 1985<br>
              <span class="w3-border-bottom">Further Comments:</span> This large black dog, with pointed ears and glowing eyes, is said to haunt the area. In 1972, one witness watched a large ball of light crash into the ground, and the hound appeared soon after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crashing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brown Edge - Private residence, north of village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1992<br>
              <span class="w3-border-bottom">Further Comments:</span> One witness living in this residence heard a loud crashing sound, as of something falling down the staircase, although no other person in the house heard the noise. Previously, the occupants had heard footsteps on the staircase, and felt a malevolent presence with the sensation of being watched.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Statuette</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burslem - Leopard public house/hotel, Market Place (razed by fire January 2022)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> No one appears to know for sure what haunts this public house (some claim the entity was a murderer), but people report feeling a presence, and a horse statuette once flew off the mantelpiece.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Molly with Pail</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burslem - Town streets<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Feeling so guilty about her own sins committed during life, Molly returned as a ghost; balancing a milk pail on her head, she apologised to anyone she met. This restless spirit was finally exorcised by six holy men, three of whom died in the process. However, a shade wearing black and white was seen racing around near the churchyard in the 1970s, which may indicate Molly's return...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Molly</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burslem - Turk's Head public house and St John's Church<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1748<br>
              <span class="w3-border-bottom">Further Comments:</span> A witch during life, Molly Leigh's shade briefly came back from the dead the day she was buried and made its home in the local pub before being exorcised. Her grave is still visible in the churchyard (it faces north to south) and running around it three times is said to summon her spirit.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Outside Saint Chad's church, Hunter Street<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 April 2012, between 09:15h - 09:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two bell ringers standing outside the church waiting to ring for Sunday service heard the peeling of a set of eight bells. The next nearest church only had six bells, while the only two churches with eight bells both denied their bells were operating at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Henry Paget</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Sinai House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Sir Henry is one of many phantoms claimed to haunt this site. Other spooks include participants from the English Civil War and a white (or grey) female form that stands on the moat bridge. Another piece of folklore claims the Knights Templar concealed a large portion of their treasure somewhere on the site, including the Holy Grail.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Stapenhill Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 09 January 2016<br>
              <span class="w3-border-bottom">Further Comments:</span> Two witnesses walked past a pale young woman dressed in black. They turned to see if she was okay, but she had vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Station Street, on the pavement outside the former Bass site (currently Coors)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> This witness heard footsteps behind them, like someone walking in high heels. When they halted and turned around to see who was following them, the footsteps ceased, and no one could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Former Worker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Appleby public house (now closed)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Investigated by Spirit Team UK, this public house was declared haunted by a former employee dating from when the building was not a pub.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Bass museum (no longer open)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen by members of staff and visitors, a ghostly cat lurked around the ground floor and the recreation of an Edwardian bar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Branston Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 July 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver reported seeing a white figure pass over the middle of the bridge. The light from his headlights appeared to go through the figure, and once it reached the far side of the bridge, the figure vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Car</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Bretby Business Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A former security guard reported seeing car headlights coming down an exit road from this site several times, although they would always vanish without trace before reaching the gate. Sounds of running showers and voices were also heard coming from an empty building. One report said that a guard was approached by a young woman wearing a summer dress (even though it was winter) who said she was lost and wanted directions to Stapenhill, which the guard gave. The CCTV caught the guard speaking and gesturing, but of the woman there was no trace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Running</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Burton Mail newspaper offices<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> The sound of feet running down the main staircase was said to have been often heard as the building was being locked up at the end of each day's production. One witness also reported seeing the ghost of a printer and hearing doors slam at night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Grey Rags</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Chemist shop, exact location withheld<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman in this chemist 'screamed the shop down' after seeing a man in grey rags and dirty long hair who held his arms out and made guttural sounds which she did not understand. The figure vanished and the witness was given a chair and a glass of water by staff who waited for her to calm down.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Derelict site along Shobnall Street (now a housing estate)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Around July, 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> Two young boys slipped into this derelict site after a firm had closed and moved away. The first time they did so, they heard footsteps and had the feeling of being watched. In another building they heard machinery and hammering, although nothing could be seen. A few days later they entered the site again, although this time they watched a large man slipped through a gap in the fence who proceeded to shout at them - they fled the site. When they returned later, they found the gap in the fence was far too narrow for a man to have fitted through.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mr Dykes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - Harper Avenue<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1954<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking home one afternoon, a teenage lad said hello to Mr Dykes, a neighbour who stood leaning on his gate. The neighbour returned the greeting, addressing the lad by his first name, something which had never happened before. When the teenager reached his house, only two doors away from where the neighbour lived, his mother informed him that Mr Dykes had died that morning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Heavy Bell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent - King Edward Place, heading towards corner of Rangemore street<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2012, 23:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two bell ringers with first-hand experience of every bell in town heard an unfamiliar ringing coming from the direction of the Audi supermarket, the sound different to the nearby town hall clock and that of Saint Modwens, a church with bells that could be occasionally heard from the area. The supermarket was built on the site of the Holy Trinity church, which had a heavy bell that would have had a similar note depth to the sound the bell ringers heard.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 148</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=148"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=148"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/westmidlands.html">Return to West Midlands</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>West Midlands</h5>
   <a href="/regions/westmidlands.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/westmidlands2.jpg"                   alt="View West Midlands records" style="width:100%" title="View West Midlands  records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>East Midlands</h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
