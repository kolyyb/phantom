



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Environmental Hauntings, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Environmental Hauntings</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Environmental Hauntings and Manifestations</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bog-of-Allen.jpg' class="w3-card" title='Vaguely human shapes standing in the bog.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Vaguely human shapes standing in the bog.</small></span>                      <p><h4><span class="w3-border-bottom">Strange Mist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allen (County Kildare) - Bog of Allen<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Before a large part of this boggy land was drained, it was renowned for strange shapes, vaguely human in appearance, which appeared at night. One man who walked across the area encountered a strange cloud which enveloped him, filling the poor man with terror, before it drifted off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">House</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allington (Wiltshire) - Tan Hill<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> House seen 1962 or 1963, coffin seen around 1940, and previously in 1870s<br>
              <span class="w3-border-bottom">Further Comments:</span> A young girl walking on the hill came across a horse which appeared to be empty. She later discovered that the building had been demolished many years before her encounter. Twenty years previous, three shepherds watched a phantom funeral procession, complete with a horse-drawn hearse and figures carrying burning torches. Upon the coffin sat a golden crown. As a side note, the hill once had a carved white horse upon it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Clashing Blades</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashmore (Dorset) - Cranborne Chase<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October, late 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> A member of the Ghost Club and his partner reported being surrounded by the sounds of men fighting, sword clashing with sword, and the moans of the dying. There are also tales of a golden coffin and stolen treasure buried in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aughrim (County Galway) - Site of Battle of Aughrim<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The battle, fought in 1691, is said to have been the most brutal on UK soil, with up to seven thousand people killed. Folklore says that the screams of dying soldiers can still be heard in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Baginton-Manor-House.jpg' class="w3-card" title='A burning manor house.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A burning manor house.</small></span>                      <p><h4><span class="w3-border-bottom">Burning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Baginton (West Midlands) - Manor House (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1900s onwards?<br>
              <span class="w3-border-bottom">Further Comments:</span> This manor house was consumed by fire around the start of the twentieth century. Ten years after the fire, a witness reported seeing a house burning where the manor had stood, and again another witness reported the same occurrence a few years later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Island</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballycotton (County Cork) - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 7 July 1878<br>
              <span class="w3-border-bottom">Further Comments:</span> Local fishermen awoke to see a strange new island out to sea, covered with trees and rocks. As dozens climbed into their boats and set off towards it, the island faded from view.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Buildings</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balnakeil (Highland) - Balnakeil Beach<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 1980<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman taking a polaroid photograph of the beach was amazed to see two buildings and a shadowy post on the developed image - no buildings were in the frame as she pressed the camera shutter. An analysis of the image suggested that it was an under exposed image of a farm.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Beaulieu-location-unknown.jpg' class="w3-card" title='The vanishing lake of Beaulieu.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The vanishing lake of Beaulieu.</small></span>                      <p><h4><span class="w3-border-bottom">Lake with Sword in a Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beaulieu (Hampshire) - Exact location unknown, somewhere in the New Forest<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1951<br>
              <span class="w3-border-bottom">Further Comments:</span> A family driving in the New Forest while on holiday passed a mist covered lake, with a sword set in a large stone a few metres off the shoreline. The lake had disappeared when they drove back and, to the best of our knowledge, has never returned.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Line of Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bilsington (Kent) - Bilsington Priory<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly procession of monks is said to haunt this area, and the ruins of the priory return to their former pristine state. The wife of Joseph Conrad was one witness to this event.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Strange Trees</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackburn (Lancashire) - Billinge Wood - Crow Wood<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Current<br>
              <span class="w3-border-bottom">Further Comments:</span> Sounds of footsteps can be heard in the wood, even when no one is immediately visible. The trees are also said to dramatically change appearance.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Fair</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackdown Hills (Somerset) - Blagdon Hill<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1600s<br>
              <span class="w3-border-bottom">Further Comments:</span> A man riding through the hills saw a fair from the distance, though it faded from view as he approached. As he passed through the area, it is reported he 'felt' the event happening around him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cottage and Chopper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenavon (Gwent) - Blorenge Mountain<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1973<br>
              <span class="w3-border-bottom">Further Comments:</span> A young girl ran ahead of their family and came across a stone cottage with an older man chopping firewood outside. The man had white hair and long moustache. The girl briefly spoke to the old man, who spoke back to her. Something then made the girl uneasy, so she ran back to her family. When the family passed the area where man and cottage were, all that could be found were old foundation stones of a long-gone building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Georgian Red Brick Dwelling, Surrounded by a Beautiful Garden</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradfield St George (Suffolk) - North of the church<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1860, 1900 and October 1926<br>
              <span class="w3-border-bottom">Further Comments:</span> First seen in 1860, the Georgian house was said to look quite normal, until it dissolved into mist. It was also seen around 1900, though no recent sightings exist.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wall and Gates of a Grand Building</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradfield St George (Suffolk) - The lane from Little Welnetham which turns right at the church<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1926<br>
              <span class="w3-border-bottom">Further Comments:</span> Observed in 1926, the witnesses were new to the area and did not think much about the building until they returned later in the day and realised it had disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/sus11446.jpg' class="w3-card" title='The ruined Brighton Pier.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ruined Brighton Pier.</small></span>                      <p><h4><span class="w3-border-bottom">Victorians</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brighton (Sussex) - Pier<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January, early 2000s, around 23:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> While walking along the seafront, two sisters watched Victorian figures walking around and listened to the sound of music and laughter. The seafront was lit up as if daytime, and a young man and a boy walked out to sea along a rope bridge. As the sisters walked away, they realised the scene they had watched was impossible, and turning back they saw only the darkness over the sea. One sister later discovered that there was a chain bridge on the spot which led to the pier, but it was destroyed in 1896.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roadside Cottage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buckfastleigh (Devon) - Moorland around the area<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Three lost girls on the moor at night claimed to have come across a cottage. Peering through an un-curtained window, they could see an old man and woman sitting by a fire. Before they had a chance to move, cottage and contents vanished without warning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sounds of Fighting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burgh Castle (Norfolk) - Roman castle<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 April (fighting) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year the sounds of clashing swords and Roman and Saxon screaming can be heard in this area. Another ghost reportedly observed here during dark nights is a figure that plummets from the ramparts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent (Staffordshire) - Outside Saint Chad's church, Hunter Street<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 April 2012, between 09:15h - 09:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two bell ringers standing outside the church waiting to ring for Sunday service heard the peeling of a set of eight bells. The next nearest church only had six bells, while the only two churches with eight bells both denied their bells were operating at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Heavy Bell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton upon Trent (Staffordshire) - King Edward Place, heading towards corner of Rangemore street<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2012, 23:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two bell ringers with first-hand experience of every bell in town heard an unfamiliar ringing coming from the direction of the Audi supermarket, the sound different to the nearby town hall clock and that of Saint Modwens, a church with bells that could be occasionally heard from the area. The supermarket was built on the site of the Holy Trinity church, which had a heavy bell that would have had a similar note depth to the sound the bell ringers heard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">House of the Dead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Callow (Hereford & Worcester) - Callow Hill<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The building which infrequently materialises on the hill was demolished long ago and was the site of several chilling murders.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cork7901.jpg' class="w3-card" title='Currabinny Woods close to Cork.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Currabinny Woods close to Cork.</small></span>                      <p><h4><span class="w3-border-bottom">Thick Mist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carrigaline (County Cork) - Currabinny Woods<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> A group of friends in the woods at dusk reported a thick mist that quickly descended the area. Footsteps and other noises surrounded them as they ran towards the car park. As they ran clear of the woods, the mist lifted, and silence returned once more.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Chagford-location-unknown.jpg' class="w3-card" title='Can a house be a ghost?'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Can a house be a ghost?</small></span>                      <p><h4><span class="w3-border-bottom">Cottages</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chagford (Devon) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Every ten or twelve years, though date not known<br>
              <span class="w3-border-bottom">Further Comments:</span> Some believe that several ghostly cottages periodically appear in the village, though it is not known whether they were once 'real'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Butler & House</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cold Ashton (Gloucestershire) - Manor House<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1930s<br>
              <span class="w3-border-bottom">Further Comments:</span> A lost woman driver stopped at this building and asked the butler for directions, which he gave. However, she soon found out that the house had been empty for many years. She returned and found the building in a disused state, completely different from when she first called in.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sounds of Battle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cotgrave (Nottinghamshire) - Colston Gate, open road flanked by fields<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 10 February 1994, 02:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two friends were walking through the area on a cold frosty morning. There was a sudden gust of wind, followed by the sounds of shouting, horses galloping, and the clash of swords. Both witnesses were scared by their encounter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Cottage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dartmoor (Devon) - Islington area?<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s or 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> An OS surveyor working on the moor spotted a cottage with smoke rising from the chimney. When he went to investigate, no sign of the building could be found, although he did meet another person who had also spotted the cottage but was unable to find it.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 85</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=85"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=85"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
