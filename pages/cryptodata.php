



<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Sites relating to cryptozoology and cryptid encounters, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Cryptozoology</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Strange Creatures from around the Isle</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shaggy Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> A1075 (Norfolk) - Between Thetford and East Wretham, half a mile from the level crossing<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> June 1986, and 22 December 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> Driving towards Thetford from the direction of Watton, this witness spotted a large, long haired creature coloured greyish white. It had small ears, a long snout, large eyes, and stood on four legs. The witness drove past the beast three times in total, ensuring a good look at it; on the third pass the creature rose on its hind legs, taking an almost human form, and stood between six to eight feet tall. The driver quickly returned home, only returning two days later to look at the area again - the creature had gone, but there were tracks at the site suggesting something had been dragged. Another witness, a similar creature in December 2007, but said it was greyer in colour with black patches.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Earth-Hound.jpg' class="w3-card" title='An overdramatic imagining of an Earth Hound.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An overdramatic imagining of an Earth Hound.</small></span>                      <p><h4><span class="w3-border-bottom">Earth Hound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdeen (Aberdeenshire) - Churchyard, Mastrick<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1915<br>
              <span class="w3-border-bottom">Further Comments:</span> Earth hounds are said to be the size of a rat, but with a dog-like head and feet of a mole. They live in churchyards and eat the recently buried. A dead earth hound may have been ploughed up in the Mastrick churchyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Urisk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberfoyle (Stirling) - Goblin's Cave, north base of Ben Venue<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This cave and the surrounding area are said to be the meeting place of many Urisk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Genaprugwirion</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abersoch (Gwynedd) - Burrows in the area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown - twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Also known as a cenaprugwirion, this lizard is around thirty centimetres long with an orange head and large eyes. Karl Shuker has speculated that the creature is a tuatara, a living fossil found in New Zealand.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/magic_aberystwyth.jpg' class="w3-card" title='An old magic lantern slide of Aberystwyth.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old magic lantern slide of Aberystwyth.</small></span>                      <p><h4><span class="w3-border-bottom">Bathing Mermaid</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystwyth (Dyfed) - Cliffs near the town<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> July 1826<br>
              <span class="w3-border-bottom">Further Comments:</span> Twelve people watched a beautiful pale woman washing herself in the sea, with what appeared to be a black tail splashing around behind her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Scaled Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Achill Sound (County Mayo) - Glendarry Lake<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> One witness said that this lake monster looked like a dinosaur, with scales running down the back. Another viewer observed the monster on the lake bank - he described it as four metres in length, with a long neck and a sheep's head. It moved in little 'hops'. A photograph taken in 1968 of the monsters is unconvincing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Humped Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Achmore, Isle of Lewis (Outer Hebrides) - Loch Urabhal<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 July 1961<br>
              <span class="w3-border-bottom">Further Comments:</span> While fishing, two teachers watched a small headed creature with a single hump swim past their boat some thirty-five metres away. It surfaced three times before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nuckelavee</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All over isles (Orkney) - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A hybrid, sea living creature, the Nuckelavee was described as having the features of both a mighty horse and its rider. The rider was legless and appeared to grow straight out of the horse's back. Its head was ten times the size of a normal human head, possessing a very wide mouth that jutted out like a pig's snout, and a single red eye that blazed with flame. It was both hairless and skinless. Nuckelavee's breath was toxic and it was responsible for the 'Mortasheen', a fatal disease of cattle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Otter King</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All over the region (Outer Hebrides) - No exact location given<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Seventeenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Martin Martin recorded the tradition of a large otter with a white spot upon its breast. It was said that the king otter was always accompanied by seven normal otters. The pelt of the Otter King was said to offer magical properties to those who owned it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alloa (Clackmannanshire) - Inglewood Pond<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1975 and 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> Previously spotted in 1975 by two women, the 1997 witness reported seeing a strange horse with the legs of a dog prancing around the woods in broad daylight. The entity quickly vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bigfoot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arbroath (Angus) - Woodland in the area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Local press reported that Charmaine Frase joined the British Bigfoot Research team having, as a child, twice encountering a tall figure in woodland. The creature was said to be seven foot (2.13 metres) with wide shoulders and no neck.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drowned Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aros, Isle of Mull (Argyll and Bute) - Loch Frisa<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> While the specifics may vary from tale to tale, the loch is named as home to a water horse that drowned a local man known for his dissolute lifestyle.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadowy Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bagley Wood (Berkshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> May 2015<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking through the woods after dusk, a couple found themselves chased by a large shadowy quadruped that stood just over a metre tall. One of the witnesses said it did not move like a deer or a dog. The woods is also reputedly home to a phantom highwayman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Bala-lake.jpg' class="w3-card" title='An old postcard of Bala Lake in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Bala Lake in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Lost City</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bala (Gwynedd) - Bala Lake<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> City unknown, Teggie observed March 1995<br>
              <span class="w3-border-bottom">Further Comments:</span> The first town of Bala is said to be concealed under the waters of this lake. In addition, a monster seen in 1995 and nicknamed 'Teggie' by locals, was described as resembling a humped crocodile, although a story published in the same year claimed recent sightings of the creature were a hoax. Teggie sightings date back to the 1960s, and the creature may have first been encountered by two men boating in the lake.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stoned Monster</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballybay (County Monaghan) - Lough Major<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1911 or 1912<br>
              <span class="w3-border-bottom">Further Comments:</span> Three boys watched a weird water creature playing in the lake - they threw stones at the creature and ran when it approached them. They stated it had a hairy, horned head.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Great Fish</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballycotton (County Cork) - Somewhere between Ballycotton and Old Head of Kinsale<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa May 1906<br>
              <span class="w3-border-bottom">Further Comments:</span> A fishing vessel reported being attacked by a dark fish the size of a whale. The fish rammed the boat several times, only ceasing the assault when the captain ordered the engines to be killed; after half an hour, the fish disappeared.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Eel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballynahinch (County Galway) - Bridge near to Ballynahinch Castle<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1880s<br>
              <span class="w3-border-bottom">Further Comments:</span> A long eel-like creature remained stuck under this bridge for two days.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ballyronan-Lough-Neagh.jpg' class="w3-card" title='AI impression of a thick, black eel under the lough.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> AI impression of a thick, black eel under the lough.</small></span>                      <p><h4><span class="w3-border-bottom">Thick Eel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballyronan (County Derry / County Londonderry) - Lough Neagh<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1986<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people in a two man kayak quickly headed towards the shore after spotting a long black creature under the water with a body the thickness of two human legs. A local legend says that Lough Neagh is also home to fairies, who can be heard laughing, and that the lough was created when someone forgot that they had left the cover off a magic well.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Humanoid Beast</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balvaig (Argyll and Bute) - Area in and around sea, and Grador Rock<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Incredibly ugly to look at, this monster would sit for many hours on the rock. One man went too close, and he was later found with his flesh clawed off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Long Serpent</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Banff (Aberdeenshire) - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1896<br>
              <span class="w3-border-bottom">Further Comments:</span> Eight crewmen of a lugger reported seeing a sea creature three hundred feet (ninety metres) long, with three huge humps. The beast made as much noise as a steamer while moving.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shape Changers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bantry (County Cork) - Bay cliffs<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The mermaids said to live in this area would be capable of changing their shape as they came on to land. They would also be seen sitting on the rocks play harps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Snake-like Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmouth (Gwynedd) - Menai Straits<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1805<br>
              <span class="w3-border-bottom">Further Comments:</span> A ship was attacked by a sea serpent, which wrapped itself around the ships mast until the crew hit back - it fell back into the sea but followed the vessel for two more days before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Barmouth-gen.jpg' class="w3-card" title='An old postcard of Barmouth Bay in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Barmouth Bay in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Sea Monsters</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmouth (Gwynedd) - Waters of the bay<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century / early twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> Several sightings of various strange creatures have been over recent years, from the traditional sea serpent to a creature resembling a giant turtle with an egg shaped head and two spines jutting from its back. A blurred photograph taken in January 2016 that national press claimed was the monster was more than likely to have been a log.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shagfoal</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barnack (Northamptonshire) - General area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Similar to (or maybe identical to) a shuck, the Shagfoal is reported to resemble a large, black longhaired bear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Women with Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barra (Outer Hebrides) - Reef in Caolas Cumhan<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Crofter Colin Campbell raised his rifle to fire at what he thought was an otter eating a fish but stopped when he realised the creature was a mermaid holding a child. The creature dived into the sea after Campbell made a noise.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 473</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=473"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=18&totalRows_paradata=473"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>

<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
