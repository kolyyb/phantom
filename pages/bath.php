
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Bath Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southwest.html">South West England</a> > Bath</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Bath Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9510.jpg' class="w3-card" title='Henrietta Street, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Henrietta Street, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Robinson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - 20 Henrietta Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The disembodied footsteps occasionally heard on site are attributed to Admiral Robinson.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9184.jpg' class="w3-card" title='Pulteney Street, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Pulteney Street, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Howe</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - 71 Pulteney Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in his naval uniform, Admiral Howe has been observed and heard moving around his former home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9508b.jpg' class="w3-card" title='Bath Abbey, Somerset.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bath Abbey, Somerset.</small></span>                      <p><h4><span class="w3-border-bottom">Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Abbey<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 August 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Phantom monks are reputed to haunt this area. A photograph taken by Russ Cribb on Flickr purports to show a ghostly cowled figure standing in front of the abbey's main door.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some2988.jpg' class="w3-card" title='Abbey Church, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Abbey Church, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Naked Roman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Area around Abbey Church, and other areas within the town centre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The figure of a naked Roman soldier (how do you tell without a uniform to assist you?) has apparently been seen running around the centre of the town. On one occasion, a police officer gave chase, but the pursuit abruptly ended when the figure faded into the air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some2080b.jpg' class="w3-card" title='Bath Assembly Rooms, Somerset.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Bath Assembly Rooms, Somerset.</small></span>                      <p><h4><span class="w3-border-bottom">Admiral Phillip</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Assembly Rooms, Saville Row and 19 Bennett Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Phillip, the first governor of New South Wales, has been seen dozens of times over the past fifty or so years, always wearing the same black hat and long cloak.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9513b.jpg' class="w3-card" title='Royal Mineral Water Hospital, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Royal Mineral Water Hospital, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Nurse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Bath Royal Mineral Water Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to have committed suicide on the site, this grey phantom nurse has vanished as of late.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tiny the Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Beckford's Tower, Lansdown Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> William Beckford's pet dog, Tiny, was once buried by the tower, though the creature's ghost returned after the remains were moved. It is also said that William's ghost haunts the graveyard surrounding the tower.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bunty</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Beehive public house (currently Grappa's Bar)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A former Victorian worker at the pub, Bunty makes fleeting appearances around the hallway dressed in a blue/grey gown. Her presence was said to be non-threatening.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monkey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Brassknocker Hill and surrounding area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1979 / 1980<br>
              <span class="w3-border-bottom">Further Comments:</span> There was a brief flat of monkey sightings here, the creature said to stand between 3 - 4 feet high (90 - 120cm) and had bright rings around its eyes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Crown Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in a First World War uniform, this phantom would appear in a darkened corner sipping his drink. The haunting is said not to have lasted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some3240.jpg' class="w3-card" title='Crystal Palace pub, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Crystal Palace pub, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Crystal Palace public house, 10 Abbey Green<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure is reported to materialise when building work is carried out inside the property. The ghostly monk is said to be quite transparent.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Items</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Curfew Inn<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> The poltergeist at work in this pub plays around with beer casks, taps and pinches staff, and moves pool balls.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cellar Dweller</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Devonshire Arms, Wellsway<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> The basement of this public house is reportedly haunted by a girl killed on the nearby railway lines. One member of staff reported feeling invisible hands tugging at their clothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dueller</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Duelling ground, Victoria Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This former duelling site is said to be haunted by a sword handling Dueller. The whispers of the deceased fighters and strange mist are said to emerge from a nearby holly bush.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Festival Office, Linley House, Pierreont Place<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Staff and visitors to the building have heard footsteps coming from an empty room here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9650.jpg' class="w3-card" title='Francis Hotel, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Francis Hotel, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Housekeeper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Francis Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Maid unknown, other experiences 03/04 January 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> As the guide on the Bath tour bus will tell you, this hotel is haunted by a former housekeeper who, in a state of depression, hanged herself. Two guests at the hotel in 2013 spent a restless night after scratching and tapping from inside their room kept them awake, and a hot water bottle was swept off the table.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9183.jpg' class="w3-card" title='Gay Street, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Gay Street, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Gay Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> A man with his long hair tied back with a ribbon is said to haunt this street. He can only be seen by other men. Two houses along the road are said to be haunted, one by disembodied voices, though details of the other haunting are not forthcoming.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some4130c.jpg' class="w3-card" title='The city of Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The city of Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Bladud</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - General area<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 860BC<br>
              <span class="w3-border-bottom">Further Comments:</span> The city of Bath is said to have been constructed around waters that cured Prince Bladud of leprosy and his pigs of skin complaints.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some3122.jpg' class="w3-card" title='The city of Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The city of Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Raining Jellyfish</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - General area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> August 1894<br>
              <span class="w3-border-bottom">Further Comments:</span> Some parts of the town were covered in thousands of jellyfish that apparently fell from the sky.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some6621c.jpg' class="w3-card" title='Victoria Park, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Victoria Park, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">White Haired Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Gravel Walk, Victoria Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Sightings said to have been around 20:45h, several in 1976<br>
              <span class="w3-border-bottom">Further Comments:</span> This white haired man has appeared several times, only to suddenly vanish.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Misty Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Grosvenor Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The appearance of this female phantom is accompanied by a sharp drop in temperature. Footfalls are also reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9237b.jpg' class="w3-card" title='High medieval wall near the Theatre Royal, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> High medieval wall near the Theatre Royal, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Moaning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - High medieval wall near Theatre Royal<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local myth states that the long drop on the other side of this wall is where plague-infected bodies were thrown - the upset spirits are still heard complaining about their treatment.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some9511a.jpg' class="w3-card" title='Laura Place, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Laura Place, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Laura Place and connecting streets<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sound (never the sight) of horses and drawn carriages is said to linger here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banging</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Little Theatre Cinema<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> Paranormal investigators were called to this theatre after strange bangs and other unexplained sounds were heard around the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/some6846.jpg' class="w3-card" title='Beau Nash House, Bath.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Beau Nash House, Bath.</small></span>                      <p><h4><span class="w3-border-bottom">Juliana</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bath - Popjoy's Restaurant (Beau Nash House)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Juliana Popjoy, the mistress of eighteenth century fashionista and dandy Beau Nash, is reported to haunt this restaurant, as is another ghostly woman wearing clothing from the 1960s. They say the latter figure looks perfectly solid until she suddenly vanishes without trace.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 34</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=34"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=34"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southwest.html">Return to South West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
