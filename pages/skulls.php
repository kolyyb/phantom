
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Screaming Skulls - Folklore and Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Skulls</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Screaming Skulls and Other Stories</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman's Skull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appley Bridge (Lancashire) - Skull House<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This item is said to be impossible to remove from the building - it always teleports back!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chatty Fellows</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Balsham (Cambridgeshire) - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend tells of talking skulls found in this graveyard, though what became of them is not known.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bony Guardian</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bettiscombe (Dorset) - Bettiscombe Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Home to the skull of a slave, the object is said to scream if removed from the house. One former owner threw the skull into a pond, only to fish it out two days later after being kept awake at night by supernatural 'sounds'. The site is also home to phantom coach and horses, and/or a phantom funeral procession.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackpool (Lancashire) - Ripley's Odditorium<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> When the skull of a local girl was displayed here, several people reported seeing a spectral female figure standing by it. The skull's owner was convinced the item was cursed and died a week after finally giving it away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Upset Skull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bowland (Yorkshire) - Bowland Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Removing this macabre item from the hall results in a sudden increase in the family mortality rate. Hence, it stays.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footfalls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bradshaw (Lancashire) - Old Timberbottom Farm<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Loud footsteps can be heard traversing empty parts of the building - it may be the last remnants of the haunted skulls that once were on display here, since relocated to Turton Tower.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Cranium</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brougham (Cumbria) - Brougham Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The cursed skull created havoc whenever removed from the property, so a solution-focused individual finally walled it up in a room of the hall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cursed Skull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Browsholme (Lancashire) - Browsholme Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> 1850s<br>
              <span class="w3-border-bottom">Further Comments:</span> When the skull was last removed from the hall (as a joke), it was blamed for spontaneous fires and family deaths. The skull was quickly returned and has not left since.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anne</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton Agnes (Yorkshire) - Burton Agnes Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Seventeenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Falling in love with the hall, Anne decreed that upon death her head should be severed and stored within the house. Immediately after she was murdered, the house was plagued by crashes and bangs, which ceased when her head was removed from the coffin and brought inside. Her skull has since been bricked up to ensure it never accidentally leaves the homestead.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George Whewell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bury (Greater Manchester) - The Pack Horse Inn, Affetside<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> If removed from the public house, this discoloured skull which once belonged to executioner George Whewell is said to scream until returned.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf389.jpg' class="w3-card" title='Old Bury Gaol, Bury St Edmunds.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Old Bury Gaol, Bury St Edmunds.</small></span>                      <p><h4><span class="w3-border-bottom">Screaming Skull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bury St Edmunds (Suffolk) - Old Bury Gaol (possibly)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The skull of William Corder, infamous for the Red Barn Murder, was stored in the prison - it was thought to be possessed by evil, and strange sounds and spectral figures were seen around it. The skull was finally removed and buried in an unknown local country graveyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bolton-Turton-Towers.jpg' class="w3-card" title='Haunted skulls.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Haunted skulls.</small></span>                      <p><h4><span class="w3-border-bottom">Duellists</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chapeltown (Greater Manchester) - Turton Tower<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two skulls are currently stored in the Towers, that when stored in previous places, have been blamed for causing two ghostly men to appear and begin fighting. Poltergeist activity is also attached to the stories, and a woman apparently dressed in mourning costume haunts the staircase of the tower (and possibly an old path outside the tower). The area outside the tower is haunted by a phantom coach.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Theophilu Brome</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chilton Cantelo (Somerset) - Higher Farm<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says that when the skull of Theophilu Brome (or Broome) has been removed in the past, the house has been besieged by howls and wails. When the tomb of Theophilus Brome was opened in the local church, the skull was found to be missing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drink Obsessed</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Derby (Derbyshire) - O'lafferty's public house (once known as The George), Sadlergate<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> This entity, which enjoys switching the barrels off and causing general mischief, could be related to the thousand year old skull discovered in the basement.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drifting Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fawkham Green (Kent) - Pennis Lane<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Autumn (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing at dusk, the figure of a nun has been reported floating down this lane. Her skull is housed at Pennis Farm and it is said if the object is ever removed a great curse will befall the valley.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dafydd</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Halkyn Mountain (Clwyd) - Ffagnallt Hall Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The skull of Dafydd, a Welsh Prince, can be found here. The last time it was removed from the building, the person responsible was plagued by shadowy figures and screams - these stopped once the skull was returned.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/OI-118.gif' class="w3-card" title='An old woodcut of a moody-looking skull.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old woodcut of a moody-looking skull.</small></span>                      <p><h4><span class="w3-border-bottom">Unhappy Skull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Heathfield (Sussex) - Warbleton Priory Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The skull (or skulls - one version of the story has two) causes trouble whenever removed from the site - it is said that to prevent 'accidental removal', the skull is now part of the brickwork.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dafydd</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Holywell (Clwyd) - Flacgnallt Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> The skull of Dafydd Prince of Wales is housed in a box at the hall. A curse is said to befall the owners of the building if it is ever removed from the property.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Freda</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hull (Yorkshire) - Ye Old White Hart public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> It is said that if the Freda's skull located behind the bar of this pub is moved, the former owner returns and haunts the building until the cranium is replaced.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/230331-skull.jpg' class="w3-card" title='An old sketch of a skull (public domain).'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old sketch of a skull (public domain).</small></span>                      <p><h4><span class="w3-border-bottom">Reappearing Skulls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lake Windermere (Cumbria) - Calgarth Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Sixteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Belonging to a married farming couple, the skulls came back to haunt the person who framed them for a crime that resulted in their execution. No matter how they were destroyed, the grinning skulls always returned to the hall, until the death of the man they cursed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Talking Skulls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Long Melford (Suffolk) - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend speaks of a pair of skulls which once resided in the churchyard which would chatter away after dark.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walled Up Skull</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lund (Yorkshire) - Manor house<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown if still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Not much is known about the skull and its surrounding legend, other than to prevent the removal of the object, it was bricked up in the attic.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Francis Downes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Manchester (Greater Manchester) - Wardley Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This building is reported to have had a skull bricked up within its walls, to prevent it from accidentally being removed and condemning the house's occupiers to misfortune or death. The macabre relic is thought to belong to Edward Barlow, a Catholic executed by Protestants in 1640.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sister</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Peak Forest (Derbyshire) - Unnamed farmhouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two sisters once lived here and fell in love with the same man. One sister murdered the other, but before dying, she said that she would never rest in any grave. The dead sister's bones were stored in a container on the property and if moved, strange noises could be heard at night and the farm's cattle would die.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Sister</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sherston (Wiltshire) - Pinkney Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The house connected to the park is home to a skull of a woman - it is believed to belong to the ghost of a young girl who carries her head under her arm, killed by her own sister. Another legend connected to the park is the 'Elm-Ash' tree, a single tree comprising of both types of wood given in its name - it was said to have grown from the grave of a male suicide into which two wooden stakes were thrust to ensure he did not return.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 29</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=29"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=29"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Reports: People</h5>
     <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
