

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Monaghan Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Monaghan</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Monaghan Ghosts, Folklore and Paranormal Places</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stoned Monster</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballybay - Lough Major<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1911 or 1912<br>
              <span class="w3-border-bottom">Further Comments:</span> Three boys watched a weird water creature playing in the lake - they threw stones at the creature and ran when it approached them. They stated it had a hairy, horned head.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Famine Victims</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Carrickmacross - Workhouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This former workhouse is reportedly haunted by ghosts dating back from the Great Famine.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castleblayney - Lake<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 June 1889<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman sitting on the banks of this lake watched a ghostly figure in a tweed suit jump into this body of water. Just over a week later, a banker took his life at the same spot.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lamp Carrier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clones - Road between Clones and Scarva<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Lamp holder unknown, shawled woman circa 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> Travelling along the road carrying a candlelit lamp under one arm, this tall shadowy figure wearing a long cloak is thought to have once been murdered in the area. One witness walking through the area spotted an older woman wearing glasses and a shawl. The witness knew something was wrong but did not mention the encounter to anyone. A few years later a friend mentioned that they had driven along the road and thought they had hit a shawled woman but found no evidence of a collision.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Armed Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drumate Lough (also referred to as Dromate) - Waters of the lough<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> August 1944<br>
              <span class="w3-border-bottom">Further Comments:</span> A farmer claimed to have shot at a creature in this lough after it passed within two metres on his boat. The monster, described as being seven-foot (2.13 metres) in length, had two arms that had claws (or webbing). The creature dived under the surface.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Eringle-Truagh-Churchyard.jpg' class="w3-card" title='A ghostly woman ready to make a date you do not want to keep.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly woman ready to make a date you do not want to keep.</small></span>                      <p><h4><span class="w3-border-bottom">Date with Death</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eringle Truagh - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Picking on the last person to leave a funeral to have occurred here, this phantasm manifests as a beautiful woman in front of male witnesses and as a gorgeous man for female viewers. It acts as a real person, setting a date when it can next meet with the mortal, before vanishing into thin air. The date agreed upon is when the witness dies.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Norman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glaslough - Castle Leslie<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1910s<br>
              <span class="w3-border-bottom">Further Comments:</span> When Lady Marjorie's son died in World War I, his form appeared to the woman as she sat in bed. Mistaking the ghost for a real person, she tried to speak to Norman, only to watch him fade away. He has been seen since in the same room. The castle is also haunted by Lady Constance, who died in 1925.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Uncle Moreton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glaslough - Glaslough House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1927<br>
              <span class="w3-border-bottom">Further Comments:</span> Author Shane Leslie encountered his dead Uncle Moreton while visiting his family home. Moreton warned the man that his other uncle would not be impressed by a recent article Leslie had penned. It turned out to be the truth.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">In Want of Mary</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Monaghan - Emyvale Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> An ethereal old man walks along the road calling the name 'Mary'. It is believed that his wife Mary was robbed and murdered along the street sometime in the eighteenth century.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 9 of 9</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
