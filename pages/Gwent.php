

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Gwent, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/wales.html">Wales</a> > Gwent</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Gwent Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Aberbargoed-hospital.jpg' class="w3-card" title='A ghostly miner walks along a hospital corridor.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly miner walks along a hospital corridor.</small></span>                      <p><h4><span class="w3-border-bottom">Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberbargoed - Aberbargoed hospital (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> Some believed this phantom, said to have been observed by staff and patients, to be a miner who died in a colliery accident, while others thought it to be a former patient.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knocking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberbargoed - Private residence on the hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January 1907<br>
              <span class="w3-border-bottom">Further Comments:</span> A newly married couple were said to have fled their home after enduring tapping on the walls, disembodied footsteps on the staircase and tin boxes which moved themselves around. Cynics said that the sounds were probably caused by work in the local colliery.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hosea Pope</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberbeeg - A4046 to Ebbw Vale<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Policeman Hosea Pope was murdered while trying to arrest a man named James Wise. Pope's ghost has since been seen along this stretch of road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Caped Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberbeeg - Bridge by the side of the  river<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman walking along the river to her place of work passed by a man wearing a cape and tall hat. She did not pay much attention, but as she walked by, she suddenly felt cold and the hairs on the back of her neck stood up. She turned to face the man, but he had vanished. A few weeks later a close friend drowned at the exact spot of her encounter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hanbury Family Only</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergavenny - Coldbrook House (demolished 1954)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The state bedroom was said to be haunted by an entity which could only be perceived by a member of the Hanbury family. The house was also known for its 'bloodstained room', the pool of blood coming from either a duelling gambling man or a maid who rejected the advances of a butler.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Woman in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergavenny - Kings Arms public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> This figure was observed descending the staircase and passing through the lounge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergavenny - Llanvihangel Court<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This pallid woman is said to leave the house at midnight and walk into a nearby wood. The White Room is said to be haunted by a little man with green eyes. It is not known for sure whether these ghosts are connected to a series of dark stains on the staircases of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Friendly Character?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abergavenny - Skirrid Mountain Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> While one report described one ghost here only as 'friendly', it may be in the minority. The inn is a former courthouse and claims to have had 182 people executed by noose within. One woman at the inn reportedly developed marks around her neck after feeling something akin to an invisible rope being wrapped around the skin. There has also been a report of a Cromwellian soldier on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">John Jenkins</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abertillery - Barn near Abertillery House (no longer standing?)<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> John Jenkins hanged himself in the barn and was discovered by his sister. The woman's screams attracted the attention of a resident of Abertillery House, who looked in the direction of the barn and watched a 'resemblance of a man' violently rotating towards the river.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Abertridwr-footbridge.jpg' class="w3-card" title='A glowing paranormal entity.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A glowing paranormal entity.</small></span>                      <p><h4><span class="w3-border-bottom">Radiant Maiden</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abertridwr - Area around the Old Mill footbridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> At least one witness watched this glowing figure cross the bridge late one dark night. She has also been seen on the nearby football field, gliding towards the footbridge.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rapping</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abertridwr - Unidentified house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1906<br>
              <span class="w3-border-bottom">Further Comments:</span> A man named Craze lived in the property and was said to have experienced strange knocking and groans which were said to emerge from the kitchen floor. After newspapers reported the story, it would appear Craze was boycotted by other miners.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Funeral</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberystruth - Church Lane<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A small procession of fairies was reported travelling along the lane towards the church to bury one of their own. Another version of the story says the funeral was ghostly in nature, and when a living man reached out to help carry the coffin, everything vanished, with the man only left holding a horse's skull.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Bottle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bargoed - Private Villa<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s<br>
              <span class="w3-border-bottom">Further Comments:</span> The family here heard footsteps, and experienced lights switching on and off, electrical cables being tugged by an unseen force, and one member having a glass bottle thrown at them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burning Boy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedwas - Hill in Machen<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, likely pre-eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Travelling between Bedwas and Risca, a woman and man spotted a boy ahead of them. The boy put his head between his legs and turned into a ball of fire which proceeded to roll up the hill. Later in their journey, the couple came across a pillar of fire which scorched the woman's handkerchief.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Pigs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedwellty - Field known as Y Weirglodd Fawr Dafolog<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Six people watched a flock of fairies flying in the sky - two people said they resembled sheep, two people said greyhounds, and the other two said pigs.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sounds of Hunters</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedwellty - Mountain in the area, close to Ebbw Fawr<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Evan Thomas claimed to have scared away invisible fairy hunters by drawing his knife (it was said that fairies were afraid of knives and iron).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Whirling Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedwellty - Unidentified field<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> While passing a field here, Lewis Thomas witnessed a terrible, ghostly man whirling along on its hands and feet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Female Spook</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blackwood (Y Coed Duon) - Monkey Tree Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This female phantom is said to be most active in rooms 7, 8 and 9.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cottage and Chopper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenavon - Blorenge Mountain<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1973<br>
              <span class="w3-border-bottom">Further Comments:</span> A young girl ran ahead of their family and came across a stone cottage with an older man chopping firewood outside. The man had white hair and long moustache. The girl briefly spoke to the old man, who spoke back to her. Something then made the girl uneasy, so she ran back to her family. When the family passed the area where man and cottage were, all that could be found were old foundation stones of a long-gone building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smashing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenavon - Colliery<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1904<br>
              <span class="w3-border-bottom">Further Comments:</span> One newspaper reported that 'uncanny' sounds were unnerving the locals. Smashed and removed windows were also attributed to the ghostly entity.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey White Mist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenavon - Cycle track below Varteg cemetery and a field at Waun Afon<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Around 23:00h, June 2018<br>
              <span class="w3-border-bottom">Further Comments:</span> Two friends on 	the cycle track watched as a misty, humanoid shape without features drift downhill. The shape paused before continuing through a field, heading towards a metal bridge over the Afon Llwyd. One of the friends took a photograph which showed a mist hovering above the ground.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Talking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenavon - Former nursing home<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2013?<br>
              <span class="w3-border-bottom">Further Comments:</span> While checking out this site, investigators heard a piano and children playing in an empty area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Glass Smashing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blaenavon - Private residences along Darkfield Terrace (since renamed?)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1904<br>
              <span class="w3-border-bottom">Further Comments:</span> Several properties along this terrace had windows smashed or removed. Although some believed a human hand was behind the vandalism, others pointed to a supernatural explanation as the perpetrator had never been seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smoky Human Cloud</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blorenge - B4246<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 09 April 2016, 22:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver and their passenger spotted a human-sized, white/blue cloud moving along this road. The driver could not make out any specific details, although the passenger could make it out as a human-like figure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unseen Ghost</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brynmawr - House along Worcester Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 15 November (?) 1886<br>
              <span class="w3-border-bottom">Further Comments:</span> After a rumour travelled the area that a certain house was haunted, a large crowd gathered outside to see the phantom for themselves. No phantom manifested.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 140</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=140"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=140"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/wales.html">Return to Welsh Preserved Counties</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Scotland</h5>
   <a href="/regions/scotland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/secondlevel/Lothian.jpg"                   alt="View Scotland records" style="width:100%" title="View Scotland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
