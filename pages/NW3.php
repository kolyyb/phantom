
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="NW3 - Strange Places in London from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > NW3</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of London's NW3 District</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Boudica's Grave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Barrow known as Boadicea's Grave, Hampstead Heath<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Local folklore says that the barrow is Boudica's final resting site, although it could just be a Bronze Age burial mound belonging to someone else entirely.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Head</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Church Row, Hampstead<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, early morning<br>
              <span class="w3-border-bottom">Further Comments:</span> The shade of a servant girl carrying a carpet bag has been seen walking down this street, heading towards the church. Contained within the bag are the remains of a child's body, which the maid had killed, and she is en route to dispose of the pieces in the churchyard. The entity was also said to haunt an unnamed building along the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sighing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Church Walk, Hampstead<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Loud sighing could be heard throughout the house, believed to be the site where a mother murdered a young child.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Close to the corner of Finchley Road and Frognal<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Local folklore once placed a ghostly woman in white here, although the finer points of the entity are not forthcoming.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - East Heath Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This smiling old man, dressed in a brown overcoat, follows witnesses along the road, quickly fading into the air if challenged.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Fitzroy Park near junction with Millfield Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Spring 1975, around 22:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Four people in a parked car spotted a woman wearing a Victorian style dress glide past. Although dark outside, the woman was illuminated, and made no sound on the gravel track. The witnesses quickly drove away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stress Inducing Shade</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Gatehouse public house, Hampstead Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1947<br>
              <span class="w3-border-bottom">Further Comments:</span> The landlord of this pub apparently collapsed after seeing the spectre and retired soon after. The ghost is thought to be an old white-haired smuggler murdered on site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Turpin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Hampstead Heath<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Sighting of ghosts unknown, cryptid in 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be none other than Dick Turpin, this ghostly robber rides around the heath after dusk on his jet black horse. Another ghost on the common land is that of a nineteenth century Member of Parliament who swallowed poison nearby; his shade can be identified by its top hat. A third ghost was spotted by two boys out playing on the heath, who reported watching an old man silently digging a hole. When they returned to the spot the following day, the ground was undisturbed. In 2005 a man encountered a large, winged creature which tore off the tops of trees as it passed overhead.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - HG Wells' House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This famous character, author of The Time Machine and many others, reportedly still walks between the walls of his former home.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Waitress</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Holly Bush Inn, Holly Mount<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen and spoken to several times by customers who have given their orders to her, they later find out that a girl matching that description has never worked for the pub.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Old Bull & Bush public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This shade is thought to belong to a skeleton discovered hidden in the basement, dated around the end of the nineteenth century.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Priory, Church Row<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The builder of this property, Mr Thompson, was said to have bricked up one of his daughters within its walls, and her ghost haunted the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Knocking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Private house along Well Walk<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The staircase and a bedroom were said to be haunted. One guest reportedly fled the bedroom after hearing knocking, quiet footsteps and whispering. A few days later, having changed room, the guest watched a woman in blue ascend the staircase and disappear into the haunted room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Trotting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - The Spaniards Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Horses reported in the 1930s, other events in 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> Sounds of a horse moving around this area were frequently reported prior to the Second World War. More recently, one witness reported her dog taking bites at an invisible presence within the public house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Turpin's Restaurant<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1980<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a woman strangled by the fireplace was said to haunt the area where she died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Charlie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Unnamed flat, Adamson Road, Swiss Cottage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> When a brother and sister moved into this flat, the local authorities warned that 'Old Charlie' haunted the site. Furniture would move around, a plate of fish vanished without trace, and during a seance, a hunched figure appeared. The siblings left, and the next family to move in said they had experienced nothing untoward.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dog sized Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - West End Lane, area near the church<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> April 1992<br>
              <span class="w3-border-bottom">Further Comments:</span> Deep within London, the report of this large golden cat surprised many locals.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Girl Looking Through Window</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - William IV public house, Hampstead High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The outside of the pub is haunted by a girl who gazes in through the lower windows, while inside walks the shade of a doctor's wife murdered and buried in the cellar.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 18 of 18</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Greater London</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View Greater London records" style="width:100%" title="View Greater London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
