

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places on Scotland's Outer Islands, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Outer Hebrides</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Outer Hebrides Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Humped Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Achmore, Isle of Lewis - Loch Urabhal<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 July 1961<br>
              <span class="w3-border-bottom">Further Comments:</span> While fishing, two teachers watched a small headed creature with a single hump swim past their boat some thirty-five metres away. It surfaced three times before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Otter King</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> All over the region - No exact location given<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Seventeenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Martin Martin recorded the tradition of a large otter with a white spot upon its breast. It was said that the king otter was always accompanied by seven normal otters. The pelt of the Otter King was said to offer magical properties to those who owned it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Arnish-Loch-A858.jpg' class="w3-card" title='A ghostly figure against a surreal sky.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly figure against a surreal sky.</small></span>                      <p><h4><span class="w3-border-bottom">Executed Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arnish - Loch along A858 between town and Stornoway?<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Local tradition tells says that this man was executed in Stornoway for murdering a student who had accompanied him on a bird hunting trip. The ghost is said to haunt the spot of the killing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Women with Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barra - Reef in Caolas Cumhan<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Crofter Colin Campbell raised his rifle to fire at what he thought was an otter eating a fish but stopped when he realised the creature was a mermaid holding a child. The creature dived into the sea after Campbell made a noise.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Foretelling War</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barra - Spring at Kilbarry (location may no longer exist)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Pieces of peat would float in this spring when peace reigned, but in times of war, the water would contain flakes of blood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Seafarers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barra - Unidentified house, Earsary<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre 1955<br>
              <span class="w3-border-bottom">Further Comments:</span> A house built on the site where the bodies of two mariners were washed ashore remained empty for many years; whenever someone moved in, they left a few days later, although the exact details of the manifestations are lacking.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blobster</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Benbecula - Shores<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990<br>
              <span class="w3-border-bottom">Further Comments:</span> A dead four metre long creature was washed up on the beach, with diamond shaped fins sitting along its back - the creature could not be identified.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sluagh</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Benbecula - Unknown<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Sluagh are a fairy host that travel across the sky to the land of the ever young, Tir-nan-Og. A couple of hunting hounds belonging to the Sluagh are said to have landed in Benbecula briefly before heading off on their travels.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unseen Tarbh-uisge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Borerary, St Kilda - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> During one dark and stormy evening one of several men sheltering in a hut wished that he had a fat ox. No sooner had he wished for one, there could be heard a bull just outside. However, all in the hut were too terrified to look and see what had appeared. It was only next morning when they ventured out, they found plenty of cloven hoof track but no sign of what had left them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Third Brother</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bragar - Old mill (may no longer exist)<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two brothers worked in the mill. One brother would work while the other slept. One day, the working brother watched a figure, who he recognised as another one of his brothers who had recently died, walk over to the sleeping brother and kiss him before disappearing. The brother who received the kiss died a few days later.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mysterious Stones</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Callanish, Isle of Lewis - Callanish standing stones<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> It is still argued to what extent this stone circle represents the heavenly bodies above, and what its ultimate purpose was. One legend says that the thirteen stones were giants who refused to become Christians - they were petrified for the sin.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Each Uisge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cnoc-na-Beist - Loch a Mhuileinn<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local girl had a narrow escape after encountering a water horse which had taken the form of young man by the edge of the loch, only realising his true nature after spotting sand and mud in his hair. She fled home and told her brother, who killed it with his sword the following morning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wish Granter</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eilean Anabuich - Coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local man caught a mermaid on the rocks, only releasing the creature after she agreed to grant three wishes. One of the wishes ensured the man became a skilled doctor, another granted the gift of prophecy, but the third wish, to become a great singer, apparently failed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Keepers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eilean Mor - Lighthouse<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1900<br>
              <span class="w3-border-bottom">Further Comments:</span> The three keepers who vanished from the lighthouse left their log behind, which reported they had been rocked by storms over a five day period. However, the surrounding towns and villages reported that the weather had been calm.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gallan Whale</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gallan Head, Isle of Lewis - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1690<br>
              <span class="w3-border-bottom">Further Comments:</span> Martin Martin reported that a large mystery whale terrorised the fishing boats of Gallan Head. It would deliberately sink the boats before returning to eat the crew.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cro Sith</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Great Bernera - Coast<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Dun and hornless fairy cattle were once thought to live along the coast of Bernera and under the nearby waters.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Long Necked Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Canna - Off the coast, and around surrounding islands<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> June 1808<br>
              <span class="w3-border-bottom">Further Comments:</span> This creature, mistaken for a rock when first observed, was seen at various times by up to thirteen fishing boat crews. It was described as between 70 and 80 foot in length (21 - 24 metres) and possessing a long neck which it kept underwater whilst swimming.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Glowing Balls</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Lewis - Area around Sandwick<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The lights that float around the area normally announce approaching death for a local. Some say the light belongs to an Irish merchant robbed and murdered on the island. One legend says that a witness who spoke to the ghost of the merchant was awarded a fairy dog's tooth that enabled the holder to cure ills and punish the greedy.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Serpent</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Lewis - Broad Bay area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa February 1830<br>
              <span class="w3-border-bottom">Further Comments:</span> A sea serpent spent a couple of weeks at this location, playing in the water. The creature, described as between 18 and 24 metres in length, had a mane like a horse and appeared white in colour.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Feolagan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Lewis - Kebock Head<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown (traditional)<br>
              <span class="w3-border-bottom">Further Comments:</span> The Feolagan was a small mouse-sized creature (or fairy) that would paralyse sheep by walking across the animal's back. The sheep would die unless the Feolagan was forced retrace its steps, or, if many sheep were affected, by capturing the Feolagan in a jar filled with salt and using the grains to cure the sheep.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Searrach Uisge</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Lewis - Loch Suainbhal<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1856 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Resembling a capsized boat, this creature has been reported swimming around for one and a half centuries. Locals say lambs were once offered annually to the creature.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bumpy Beast</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Lewis - Sea off the north of the island<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1882<br>
              <span class="w3-border-bottom">Further Comments:</span> A German ship 15 kilometres off the coast reported a sea serpent around 40 metres in length, several bumps protruding from the water, along the creatures back. Sea serpents have also been reported at the southern side of the island.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Potato Eater</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Lewis - Unknown<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> A crofter noticed his potato store was raided each night. One morning he discovered the tooth of a Cu Sith stuck in a potato - looks like this Cu Sith liked a meal of spuds!</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jumping Peat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Lewis - Unknown cottage on west side of island<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa February 1938<br>
              <span class="w3-border-bottom">Further Comments:</span> Mrs MacLeod, with her two grandchildren experienced a very brief poltergeist outbreak, during which a small piece of peat jumped from the floor and landed in her cup of tea. Crockery scattered itself around the floor, tins thrown into the bedroom, and another piece of peat struck MacLeod in the face.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Isle_of_Skye.jpg' class="w3-card" title='An old photograph of the Isle of Skye.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old photograph of the Isle of Skye.</small></span>                      <p><h4><span class="w3-border-bottom">Fillan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Isle of Skye - No exact location documented<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1690<br>
              <span class="w3-border-bottom">Further Comments:</span> Martin Martin documented a small worm that lived between the flesh and skin, known as either a Fiollan or Fillan. It was about 2.5 centimetres in length, as wide as a goose quill, and it had little legs either side of the body that enabled it to crawl around under the skin. Once there, it would cause tumours and abscesses.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 45</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=45"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=45"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
