
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="EC1 - A List of London Ghosts and Strangeness from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > EC1</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of London's EC1 District</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon1495.jpg' class="w3-card" title='Cock Lane at night, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Cock Lane at night, London.</small></span>                      <p><h4><span class="w3-border-bottom">The Cock-Lane Ghost</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - 20 Cock Lane (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> October 1762<br>
              <span class="w3-border-bottom">Further Comments:</span> No longer standing, this building was plagued by an outbreak of internationally famous poltergeist activity that centred itself around a young child in the household, Elizabeth Parsons.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burning Martyrs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Area around the Martyrs' Memorial, Smithfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The location where a total of 277 Protestants were once burnt alive for their beliefs, some have reported hearing their cries and screams late at night. Less fortunate people have also reported the smell of burning flesh.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Duke</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Charterhouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Duke of Norfolk, who spent much of his time and money here, lost his head after upsetting Queen Elizabeth and now ascends the staircase of the great hall. Outside the buildings, a monk has been seen flitting between the courtyards of Washhouse and Masters.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anne Naylor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Chick Lane (later renamed West Street), demolished 1940s/50s<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> Anne Naylor was murdered by a mother and daughter after she tried to escape their employment. Anne's sister was also murdered when she tried to investigate Anne's disappearance. The bodies were dumped in Chick Lane; soon the area was awash with tales of a pale white woman who had started to haunt the area. The daughter eventually confessed to the crimes, and she and her mother were executed in 1768.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anne Naylor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Farringdon Underground Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The odd screams that have been heard in this area are attributed to Naylor, thirteen year old girl murdered on this site in 1758. She is now referred to as 'the Screaming Spectre'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Person</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Holborn Library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> An archivist using a mirror in the third floor bathroom glimpsed a person walk behind her and turned to find nobody there. An art curator visiting the then-unused fourth floor was violently pushed in the shoulder by an unseen presence, and a cleaner working in the basement ran from it in terror, refusing to say what she had seen or return to the library. Strange lights have also been witnessed in the reserve stock room and doors open and slam by themselves.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - House of Detention, Sans Walk<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The apparition of a lady in a Victorian dress has been reported walking around the building (what is left of it), while phantom footsteps and the soft crying of a lost child have also been reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Grimaldi.jpg' class="w3-card" title='Joseph Grimaldi (public domain).'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Joseph Grimaldi (public domain).</small></span>                      <p><h4><span class="w3-border-bottom">Grimaldi the Clown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Sadler's Wells Theatre, Rosebury Avenue<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once seen sitting in one of the boxes (which no longer exist), Joseph Grimaldi is still seen wearing his clown makeup, almost 200 years since his death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horned Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Smithfield<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1600s<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom figure was said to have danced over meat stalls while angry butchers and meat merchants tried to stab at it, striking only thin air.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rahere</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - St Bartholomew's church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 July, 07:00h (Rahere) (reoccurring), others early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This pious monk, who doubled as a jester in Henry I's court, is thought to be the man who built the church and has been reported standing by the altar. He quickly disappears if seen. Another figure was observed in the pulpit by a former Rector, who said the man wore clothing from the Reformation period. A different witness spotted a woman in a white dress, while others have claimed shuffling footsteps were heard around the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pinkie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - St Bartholomew's Hospital, West Smithfield<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Grace Ward (once known as Pinkie's Ward?) is reputedly home to a nurse in old fashioned dress. Some say that she killed herself after administering an overdose to a patient, while other stories say she was murdered by an insane patient in the lift (after which, the lift would never work correctly). Another ghostly nurse haunts Bedford Fenwick Ward, and is said to be comforting in nature. Another story, or another version of first story, says that the theatre lift in the King George the Fifth block is haunted by Jasmine, who smells strongly of floral perfume.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Figure in Photo</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - St Botolph's Church, Bishopsgate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1982<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing in a photograph taken by Mr Chris Brackley, the image bore up to intense scrutiny. Mr Brackley was later contacted by a builder who recognised the face of one that he had seen in a coffin in the church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Haired Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Sutton Arms public house, Carthusian Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> This smiling redheaded entity materialises only for a couple of seconds at a time, before vanishing as quickly as he appears. He has been described as elderly, dressed in old fashioned clothing, and wearing a large grin on his face.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burning</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - The Elms, Smithfield (located outside Church of St Bartholomew the Great)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Hundreds of people were executed by being burnt alive outside the church during the reign of Henry VIII. One version of the ghost story says that screams and the crackling of fire is said to be heard, and the smell of burning flesh occasionally detected, while another (less dramatic) version has the haunting consist of groans and shuffling footsteps.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Viaduct Tavern<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One of the ghosts which haunts this pub goes by the name of Robert. He may be responsible for the poltergeist actions that occur within this building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Landlord</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> EC1 - Ye Old Red Cow, Cloth Fair<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1981<br>
              <span class="w3-border-bottom">Further Comments:</span> Believed to be a former landlord (though the jury is out on his name), this phantom figure would sit upstairs and look down at the patrons for a short time after his death..</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 16 of 16</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Greater London</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View Greater London records" style="width:100%" title="View Greater London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
