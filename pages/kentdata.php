
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Kent, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/southeast.html">South East England</a> > Kent</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Kent Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Force</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Adisham - 3 Church Lane<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1976<br>
              <span class="w3-border-bottom">Further Comments:</span> Nicknamed 'The Force' by one of the residents of this home, tables would lift themselves up, boiling water would come out of the floor, and the electric meter would operate even when power to the building was cut off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Adisham-Bossington-Road.jpg' class="w3-card" title='A shadowy horse that cannot be found.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A shadowy horse that cannot be found.</small></span>                      <p><h4><span class="w3-border-bottom">Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Adisham - Bossington Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A legend says that a shadow of a horse has been spotted under the bridge, but upon investigation, there is nothing there.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bad Poker Night</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aldington - The Walnut Tree Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Home to a gang of smugglers, a poker game they played while waiting for 'the signal' ended with one gang member slitting the throat of another - the ghostly footfalls that can now be heard are that of the murderer taking the body to the well to be disposed of.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Loud Shouts</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appledore - Old house used by Soldiers during Second World War<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> The soldiers staying here during the Second World War were all killed in action. After the war, it was said they could be heard shouting and screaming in the part of the house where they lived.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Walking in the Attic</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appledore - Unnamed council house on the village heath<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1975-1984<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of something moving around the attic have been reported in this property, while strange bangs were heard coming from the stairs. Whispering could often be heard in the smallest bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Golden Idol</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ash - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Hunted for by many a treasure seeker, still yet to be discovered, a large statue made from solid gold is supposed to be hidden in the lands here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Body in Attic</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford - An old Manor House near the railway<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown - pre twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a murdered man returned to his lover and directed her to find his body in the loft in this old house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford - Ball Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> New Year's Eve, Midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom horse drawn carriage is said to travel along this lane before either crashing into a tree or falling into a pond.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jet Black Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford - Close to railway line, Ham Street<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> A man spotted an Alsatian sized cat near his home. The creature was jet black and had a long tail. The witness had recently lost his own pet cat and speculated that it had been eaten by the panther.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Amorous Foxes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford - Dering Woods<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Always<br>
              <span class="w3-border-bottom">Further Comments:</span> The Woodland Trust denied the woodland was haunted by a gypsy who accidentally burnt herself to death or a headmaster who committed suicide, but rather by the sounds of amorous foxes. The Trust also highlighted that they had spent six thousand pounds on clearing up the damage caused by ghost hunters (or by people using ghost hunting as an excuse to be idiots).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swimming Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford - Eastwell Park and Manor House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 20 June - Horseman seen (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding towards the park house, this phantom horseman veers off at the last minute and enters the nearby lake. A white lady haunts the house itself, seen by porters on the night shift. In the seventeenth century, the Earl of Winchelsea cut down several oak trees, bestowing a curse on his family which took the life of his wife and son shortly after.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gliding Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford - Repton Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom white lady here was reportedly seen by soldiers using the building during the Second World War and was thought to be the murdered wife of a former owner. Within the manor she would ascend the staircase holding a candle, while one person who observed her outside said she had no feet and had drifted across a nearby field. The kitchen area was also reputedly haunted by a ghostly monk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Orb</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ashford - SMP Large Format (printing firm), Montpelier Business Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> CCTV caught what appeared to be a white ball of light moving around a workshop, although it could be something small close to the camera lens. Staff have also reported the feeling of being watched and had tools moved around the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burning Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylesford - Area around the White Horse Stone<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A horse and rider, both bathed in flame, are said to haunt this area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rider with Large Hat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bearsted - Road leading to Pilgrim's Way<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This man on horseback is described as wearing a large hat and possessing silver spurs. Some stories say that various people have engaged the man in conversation, not realising he was a ghost until he vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rider</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beckenham - Field along Pickhurst Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1926<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly figure riding a white horse was spotted galloping across this field.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Unhappy Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beckenham - Lazy Toad public house, and road outside<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing 1980s style clothing, one witness spotted the unhappy-looking teenager before she suddenly disappeared outside the pub. There appeared to be some debate in the media whether the ghost was somehow connected to the pub or actually just a misidentified real person.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Haired Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beckenham - Private residence along Abbey Park, Beckenham<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October 2007, early morning<br>
              <span class="w3-border-bottom">Further Comments:</span> One occupant of the house woke in the early hours to see an elderly lady standing in the corner of the bedroom. The figure had long grey hair which curled at the end, and wore a grey dressing gown. The occupant pulled the duvet over their head and hid. Other people in the property reported an unsettling feeling during the night.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Turpin in Green Velvet</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bickley - The Chequers public house, Southborough Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says Dick Turpin's ghost was seen in an upstairs room sitting at a desk, writing with a quill. Another story says a woman in eighteenth century garb also haunts the upper part of the building. The pub hit headlines in September 2013 after it was reported a phantom French Soldier called Barnard scared away two bar staff who were staying overnight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bicknor - Bicknor Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Echoing footfalls have been reported around the church and a large, strange animal seen that vanished when approached.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hooded Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bidborough - St Lawrence Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> July 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> Two visitors to the churchyard were alarmed by the appearance of three hooded figures floating down the path reasonably quickly. The figures possessed a faint glow, and there was also a pale hue under the hoods where their faces should have been. The witnesses ran from the churchyard but stayed outside the grounds for several minutes to see whether the figures would reappear. They did not.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Biddenden - Small wood in area<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Between Christmas and Twelfth Night, pre-1930s<br>
              <span class="w3-border-bottom">Further Comments:</span> A white figure haunted the woodland and became most active over winter. The worried daughter of a farmer who had been terrified by the ghostly figure sought to help her father by recruiting Jasper Maskelyne to talk to the ghost and try to put it to rest. Jasper stood in the woodland at night; as the white figure approached, Jasper cocked a shotgun loaded with salt and fired. The ghost was revealed to be a farm labourer.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/a_spitfire1.jpg' class="w3-card" title='Photograph of a spitfire flying overhead.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Photograph of a spitfire flying overhead.</small></span>                      <p><h4><span class="w3-border-bottom">Spitfire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Biggin Hill - Biggin Hill Airport & village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 January (spitfire) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Observed flying overhead, and sometimes heard, a phantom Spitfire haunts the skies of Biggin Hill - January would appear to have become its favourite haunting month. Airmen dressed in trench coats have been reported in the village; they stop people and ask directions before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tailed Bigfoot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bilington - Field near the town<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1961<br>
              <span class="w3-border-bottom">Further Comments:</span> Two girls walking from school reported seeing a large hairy figure running into nearby woods, complete with an unmistakeable tail.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Line of Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bilsington - Bilsington Priory<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly procession of monks is said to haunt this area, and the ruins of the priory return to their former pristine state. The wife of Joseph Conrad was one witness to this event.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 333</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=333"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=13&totalRows_paradata=333"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/southeast.html">Return to South East England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>South East</h5>
   <a href="/regions/southeast.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/southeast.jpg"                   alt="View South East records" style="width:100%" title="View South East records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
