


<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of hell gates and short cuts, from the Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/reports/reports-type.html">Reports: Browse Type</a> > Hell Gates</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>A List of Hell Gates & Holes from around the Isles</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Albury-Halls-Garden-Pond.jpg' class="w3-card" title='Entrances to Hell take many forms.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Entrances to Hell take many forms.</small></span>                      <p><h4><span class="w3-border-bottom">Bottomless Pit</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Albury (Hertfordshire) - Halls Garden Pond near the church<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This small body of water is said to be a one way ticket to Hell for anyone foolish enough to try to swim to the bottom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hell Gate</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymena (County Antrim) - Dundermot Mound<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Dundermot Mound is reported to be the location of a secret entrance to hell - occasionally it opens, releasing demons who take any witnesses straight to the underworld.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Thomas Boleyn Drives Headless Horses</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Breckles (Norfolk) - Breckles Hall<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The father of Anne Boleyn is said to be cursed with driving his coach over forty (or eleven) Norfolk bridges. One of the passing points is Breckles Hall, where the spectre once scared a poacher to death (another version states that the poacher was taken by the Devil because of his constant law breaking). Locals say that anyone who views the coach is dragged down to hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/misty-1a.jpg' class="w3-card" title='A illustration by Wayne Lowden showing several indistinct figures standing in the mist.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A illustration by Wayne Lowden showing several indistinct figures standing in the mist.</small></span>                      <p><h4><span class="w3-border-bottom">Misty Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burrill (Yorkshire) - Hell Hole Wood<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple walking from Thornton Watlass spotted a few misty figures moving around the wood. It is said that the Devil would use the woods as a hiding place.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/DEVILS-CHIMNEY-CHELTENHAM.jpg' class="w3-card" title='An old postcard showing the Devil&#039;s Chimney of Leckhampton Hill.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing the Devil&#039;s Chimney of Leckhampton Hill.</small></span>                      <p><h4><span class="w3-border-bottom">Devil's Chimney</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cheltenham (Gloucestershire) - Leckhampton Hill<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> This limestone outcrop is said to lead straight to hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hobgoblin Builders</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gifford (Lothian) - Yester Castle (ruins)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present, although only subterranean 'Goblin Hall remains'<br>
              <span class="w3-border-bottom">Further Comments:</span> Warlock Sir Hugo de Giffard is said to have made a pact with the Devil, and in return had an army of Hobgoblins build this castle for him. A staircase in the hall was filled with earth as it was believed it led straight to hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Urisk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glen Lyon (Argyll and Bute) - Inbhirinneoin (Burn)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Evidence that shows this part fairy, part human hybrid still to be in the area occasionally turns up. A barrow known locally as the 'Mound of the Dead' is thought to be an entrance to Hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/corn6423c.jpg' class="w3-card" title='The Hell&#039;s Mouth, Gwithian.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The Hell&#039;s Mouth, Gwithian.</small></span>                      <p><h4><span class="w3-border-bottom">Suicidal Screams</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gwithian (Cornwall) - Hell's Mouth<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Prior to a storm<br>
              <span class="w3-border-bottom">Further Comments:</span> A man once killed himself by leaping off the cliffs at Gwithian after he found his home burnt to the ground and his sister missing. As stormy weather approaches, the man's screams can once more be heard as they bounce off the steep cliff sides at the area known as Hell's Mouth.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Escape Route</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lundy Bay (Cornwall) - Lundy Hole<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Hole still present<br>
              <span class="w3-border-bottom">Further Comments:</span> While combing her hair nearby, Saint Menfre was confronted by the Devil. She threw her comb at him, striking with such force he dug Lundy Hole in his haste to return to hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Marston-Moretaine-DevilsStone.jpg' class="w3-card" title='Gaming with the Devil is risky.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Gaming with the Devil is risky.</small></span>                      <p><h4><span class="w3-border-bottom">Vanishing Leapfroggers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Marston Moretaine (Bedfordshire) - Devil's Stone, also known as Devil's Jump Stone<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A stone marks the spot where the Devil played a game of leapfrog with three local lads - when they jumped over his back, a hole to hell opened, and they were never seen again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/a_devil.jpg' class="w3-card" title='A photograph of a statue depicting the Devil.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A photograph of a statue depicting the Devil.</small></span>                      <p><h4><span class="w3-border-bottom">Laird Beardie</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Moonzie, Fife (Fife) - Lordscairnie Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 December, Midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Beardie played cards against the Devil and lost - the fateful game is now replayed once a year, though it is warned that anyone watching the game will also travel to hell with the Laird at the end of the match.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hell Hole</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Sheffield (Yorkshire) - Somewhere between Neepsend Bicarage and Rutland Road<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1880s<br>
              <span class="w3-border-bottom">Further Comments:</span> An entrance to the lower world was said to be located somewhere in Sheffield.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dando</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Germans (Cornwall) - Erth Hill, near the rivers of Tiddy and Lynher<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa  fourteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Dando was a monk and a bit of a rogue - he enjoyed hunting on a Sunday. One Sabbath he was out hunting for hare when the Devil appeared and grabbed all the game Dando had hanging from his horse. Old Nick ran, Dando in hot pursuit - unfortunately, the monk lost his balance on the bank of the Tiddy and fell in. The waters turned to steam, and Dando was taken to hell.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Torbay-Daddyhole.jpg' class="w3-card" title='Satan takes a murderer.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Satan takes a murderer.</small></span>                      <p><h4><span class="w3-border-bottom">A Woman Scorned</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Torbay (Devon) - Daddyhole (or Daddy Hole) Plain<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Scorned by a knight, a woman invited the warrior and his new love to meet here. The woman murdered the couple, and as a storm developed, the Devil appeared and took the murderess into a nearby hole (presumably to hell).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Marshy Bells</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Tunstall (Norfolk) - Church, and marsh known as Bell Hole (and Hell Hole)<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil once appeared in the village, stealing the church bells and sinking them into the nearby marsh. Bubbles in the marsh were said to be the result of the bells continuing to sink into the bottomless pit.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hellsmouth</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wellington (Somerset) - Park Farm<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> A muddy pool located near the farm was thought to be an entrance to hell, from where ghosts and ghouls could come and go as they pleased.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 16 of 16</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/reports/reports-type.html">Return to Reports: Types</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
      <div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
