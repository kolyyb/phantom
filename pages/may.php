
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in May">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > May</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>May - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Killarney-Lough-Leane.jpg' class="w3-card" title='The phantom horse and rider emerge from the lake.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The phantom horse and rider emerge from the lake.</small></span>                      <p><h4><span class="w3-border-bottom">O'Donoghue</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killarney (County Kerry) - Lough Leane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (O'Donoghue, every 7 years) (reoccurring), others on misty nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A warrior by the name of O'Donoghue is said to emerge from the waters of the lake on the back of a white horse once every seven years. Another story has an unidentified figure walking a hound appear on misty nights.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing Nuns</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Oxford (Oxfordshire) - Godstow Nunnery, Wolvercote<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> As dawn breaks on the first of May, phantom singing can be heard coming from the empty chapel. The phantom of Rosamund Clifford has also been reported here, though she is not so fussy as when she should appear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Viking Funeral</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> South Walsham (Norfolk) - South Walsham Broad<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Reports here claim that a spectral Viking long boat appears, burning in the middle of the water.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Secret Door</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Brecon Beacons (Powys) - Llyn Cwm Llwch (somewhere around the area)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year, a secret door opens here which will take mortals to the fairy realm. The door was once always open, but after a man stole a fairy flower, access was restricted. The area is also home to an old woman who uses music to lure the weak willed into the lake waters where they drown - once she has killed nine hundred people, she will regain her youth and become mortal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">White Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> River Wharfe (Yorkshire) - Wharfedale's Strid<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Together with a fairy, this pale horse rises from the dangerous waters once a year. It has been known to drown people who come too close.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/york3790a.jpg' class="w3-card" title='The River Ouse running through York.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The River Ouse running through York.</small></span>                      <p><h4><span class="w3-border-bottom">Pale Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> York (Yorkshire) - Edge of the River Ouse<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring) (legend), ghost unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be the shade of a drowned person, this figure drifts around the banks of the river. Another piece of folklore says that a person drops five white pebbles into the river when the Minster clock chimes one in the morning during the first of May, they see their past, present and future.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fighting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Loch Ashie (Highland) - Shores around, and to the moors east of, Loch Ashie and Loch Duntelchaig<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring), last seen 1870/71, heard 1999<br>
              <span class="w3-border-bottom">Further Comments:</span> Accompanied by silence, a battle has been seen raging once a year on the muddy banks of this loch. It was said to have been last heard during the summer of 1999.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wine</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Culloden (Highland) - St Mary's Well<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring), or first Sunday in May<br>
              <span class="w3-border-bottom">Further Comments:</span> This holy well would be visited to heal the sick on the first Sunday in May - a piece of cloth would be dipped in the water before being hung from the branch of a nearby tree. One myth says that on Mayday the water turned to wine.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Healing Well</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Mullingar (County Westmeath) - St Bridget's Well, Ash Road<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 May - 09 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> It was once believed that drinking from the well for these nine consecutive days in May would result in any disease being cured.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Nick in Waiting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burgh St Peter (Norfolk) - Church<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 May reoccurring (skeleton appears)<br>
              <span class="w3-border-bottom">Further Comments:</span> The builder of this church is said to have sold his soul to finish the construction. The Devil is said to still wait for the soul due to him, while the old man's shade lurks protected within the church grounds. There is one report that says the builder now takes the form of a clothed skeletal figure and appears once per year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Charging Horseman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Prestbury (Gloucestershire) - Shaw Green Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 04 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Killed by a Lancastrian arrow in the 1400's, this glowing messenger is doomed to carry his cargo forevermore. Another story says he was decapitated by a thin wire that was placed across the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hell Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Helston (Cornwall) - Angel Hotel, Coinagehall Street<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 08 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil dropped his stone while fighting with St Michael here, and is said to return once a year to view it (the rock now forms part of the building). The hotel also has a resident ghost named Nelly.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mediaeval Party</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Salthouse (Norfolk) - Salthouse Pool<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A re-enactment of a mediaeval (or possibly Roman) feast occurs late at night; the phantom scene includes singing, dancing, and small bonfires. Some sources state that the event happens on the third Tuesday in May.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Devil Takes His Due</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ormesby (Norfolk) - Exact location unknown<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 15 May (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The Devil is said to appear and burn down an old mill, containing a miller, his daughter, and a visitor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Wroxham (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Anne Boleyn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blickling (Norfolk) - Blickling Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> To mark the anniversary of her death, a coach pulled by four headless horses pulls up on the driveway, with a decapitated Anne on a seat, her head in her lap. After Anne's headless white ghost arrives, it is said then to climb out, before inspecting each room of the Hall (the place of her birth and childhood). On the same date, Sir Thomas Boleyn is said to drive a team of headless horses in this area, cursed to cross twelve Norfolk bridges (including those at Aylsham, Coltishall and Wroxham).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Thomas Boleyn</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Braydeston (Norfolk) - Bay's Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This bridge is said to be one of several which Sir Thomas passes over in his coach on the anniversary of his daughter's death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Thomas</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burgh (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven (or twelve, or forty) bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Buxton (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sir Thomas</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hautbois (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Horstead (Norfolk) - Meyton bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven (or twelve) bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Oxnead (Norfolk) - Oxnead Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Thomas_Boleyn.jpg' class="w3-card" title='Thomas Boleyn, as pictured on his tomb.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Thomas Boleyn, as pictured on his tomb.</small></span>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aylsham (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Belaugh (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Phantom Coach</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coltishall (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 19 May (reoccurring) (coach)<br>
              <span class="w3-border-bottom">Further Comments:</span> A ghostly coach and four passes over this bridge once a year. It is said to be driven by a headless Sir Thomas Boleyn and is one of eleven bridges that he passes over on the night of his daughter Anne's execution. A headless Black Shuck is also reputed to cross this bridge once per night.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 45</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=45"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=45"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
