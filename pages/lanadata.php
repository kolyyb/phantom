

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Places in Lanarkshire, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/scotland.html">Scotland</a> > Lanarkshire</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Lanarkshire Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Airdrie-lovers-leap.jpg' class="w3-card" title='Do not underestimate the ghostly lepus.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Do not underestimate the ghostly lepus.</small></span>                      <p><h4><span class="w3-border-bottom">White Hare</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Airdrie - Area which was once known as Lover's Leap, on the road towards Faskine<br>
              <span class="w3-border-bottom">Type:</span> Curse<br>
              <span class="w3-border-bottom">Date / Time:</span> Eighteenth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> The manifestation of a white hare would warn the Kerr family of impending doom. The site known as Lover's Leap marked the spot where a Kerr woman was found dead (along with her lover) shortly after this magic white hare was spotted.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Prints</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Airdrie - Easter Dunsyston Farm<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> September 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> The family at the farm reported finding two cat-like paw prints in fresh mud, fifteen centimetres in length.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mowing Devil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Airdrie - Unidentified farmer's field, North Burn<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A witch by the name of Maggie Ramsey was said to have summoned the devil to assist her in a mowing contest, although he remained invisible to anyone without second sight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">David?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Blantyre - The David Livingstone Centre<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> July 1982<br>
              <span class="w3-border-bottom">Further Comments:</span> With small items vanishing without trace, only to reappear several months later, it is no wonder that staff here believed the building to have a resident spirit. Some nicknamed the entity Old Davy, though it is unclear whether the ghost was that of David Livingstone who once worked here. In July 1982 three witnesses watched an apparition walk past them and vanish.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Monks</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Braidwood - Wooded area between Braidwood and Crossford, near St Oswald's Chapel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A monastery once stood near these woods, and even though it has long since vanished, the bells can be occasionally heard ringing out and a ghostly monk walks between the trees.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Chapelhall-Area-former-coal.jpg' class="w3-card" title='A phantom coal miner.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom coal miner.</small></span>                      <p><h4><span class="w3-border-bottom">Miners</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chapelhall - Area around former coal mine<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A local story says that if you stand in the same place where miners loitered, you could see their ghosts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bishop Cameron</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Chryston - Bedlay Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Just as likely to be heard stomping around as he is to be seen, the Bishop's ghost is said to date back almost seven hundred years, from when he was discovered floating face down in a large pond that was on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Benevolent Voices</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coatbridge - Private house near Showcase Leisure Park<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 2005 - 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> Scratching, loud banging, disembodied footsteps and flashing lights were experienced in this home, as well as strange unidentifiable ringing during the night. Voices emerged from children's toys which tried to warn of an upcoming event. One child reported seeing the apparition of tall old woman, while another would talk to something other family members could not see.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Couple</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Coulter - Near Windgate House, also known as The Vaults<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1920s<br>
              <span class="w3-border-bottom">Further Comments:</span> A farmer taking his sheep home late one night stumbled across a man and woman wearing Victorian clothing just outside Windgate House. The farmer bid them a 'Good evening', upon which the figures vanished. The following morning, the man discovered his hair had turned completely white.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Singing Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crawford - Old Post Horn Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Child unknown, man seen 1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> This young infant was run over by a coach and four outside the inn but is still heard today. The owner of the hotel during the 1980s watched a man wearing a brown cloak pass the window during a snowstorm - he went outside to investigate, but the man had vanished, and the snow was undisturbed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Roman Soldiers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crawford - Watling Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Cut off at the knees, ten ghostly Roman soldiers are said to walk along the street.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Possibly Mary</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crossford - Craignethan Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mary unknown, other activity circa 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> The headless woman reported in the castle grounds could be Mary Queen of Scots, though her relationship with the building appears to be minimal. Workmen on the site claimed to have seen a figure in the ruins and heard voices. A visitor alone in the caponier during the 1990s could hear a woman singing, although another visitor nearby failed to hear anything. Another story says that a Historic Scotland work crew could hear the sounds of a banquet emerging from the hall but could see no one there.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gardener</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Crossford - Valley International Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> Workers at the park have seen wheelbarrows moving on their own accord, and strange misty figures walking around the grounds. One man watched a plate stop and start spinning in the kitchen, with no one else present.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wailing Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> East Kilbride - Hospital grounds along Eaglesham Road<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 04 March 2005, around 00:30h<br>
              <span class="w3-border-bottom">Further Comments:</span> A pair of friends walking along the road had a strange, uncontrollable compulsion to lift the cover of a storm drain. The sound of a woman's sobbing could be heard from the hole, which quickly became a wail. When they spotted a glowing mist coming towards them from the drain, the friends regained control of their bodies and fled from the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jenny</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> East Kilbride - Jenny Cameron's grave<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> A well respected rebel from the Jacobite movement, a ball of light was often seen floating around her grave at dusk during the mid-twentieth century.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> East Kilbride - Mains Castle, Lymekilns<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Throttled by her husband during a domestic row (he was convinced she was having an affair), the lady has insisted on returning several times after her murder.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hall Keeper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> East Kilbride - Village Theatre (was the Public Hall)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of one of the men once charged with maintaining the building still carries on with his work many years after shuffling off his mortal coil.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Eddlewood - Disused (private) land between Eddlewood and Quarter<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness who played here as a child reported that a woman in a white or grey dress would walk down a path and vanish as she crossed a bridge. Wailing and sobbing would be heard in the area after dusk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">He Who Listens</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Fenwick - Kiwi Lodge Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-to-late-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Helen Blanchfield watched a man watching her as she played on the piano. She tried to talk to him, but he vanished.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Septimus Pitt</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Mitchell Library<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Although Pitt the phantom is the creation of Scottish poet Brian Whittingham, staff at the library have reported icy chills on the second floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Multi-storey car park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2007 or 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> While talking to her friend by her car on the fourth level, one driver watched a young woman walk across the car park, jump up onto the edge before jumping off. The driver was extremely upset, and asked her friend to check the young woman was okay - but there was no body and no evidence anyone had fallen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Museum of Transport<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> A museum security guard reported encountering several spooky occurrences during his time working here, including the screams of children, balls of blue light and sightings of a headless woman.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Disappearing Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Private flat, Woodlands area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A few spooky occurrences were reported as happening over a twenty-five year period, including a lady who would vanish, screaming, and loud banging.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadow with No Hand</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Private House, Duke Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Several unexplainable events have occurred in this building, including the sight of a shadow that approached the witness with its arms in the air (it was noticed that the shadow had a hand missing) and a phantom dog that played with children on site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Scratcher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glasgow - Radio Clyde, Studio Three<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This radio station is home to an entity that has left deep scratch marks on the arms of one guest and is blamed for any technical failure.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 86</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=86"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=3&totalRows_paradata=86"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/scotland.html">Return to Scotland Main Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland Records" style="width:100%" title="View View Republic of Ireland Records">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/wales.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
