

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="County Down Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northernireland.html">Northern Ireland</a> > County Down</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Down Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hairy Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Annesborough - A25 towards Castlewellan<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 October 2016, 21:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver watched a four foot high hairy figure dart across the road and disappear behind a bush. The driver looked behind the bush as they drove past but was unable to see anything there.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Orange Globes</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bangor - Skies around the area<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 May 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> Several media companies and air traffic control were contacted after witnesses watched three orange globes fly across the night sky. The objects were visible for around five minutes before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Victorian Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bangor - St Mary's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2006/7<br>
              <span class="w3-border-bottom">Further Comments:</span> A site worker walking up the main path to the left of the church spotted a grey lady in Victorian clothing twenty-five metres away. The witness shouted to her that the gates were closed and that she would not be able to pass through. The lady ignored the worker and continued walking, passing behind the witness's work van where she disappeared. The worker searched the area; there was no way the lady could leave without passing or being seen by the worker.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dullahan</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bryansford - Hill between Bryansford and Moneyscalp<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Riding headless on a black horse, the Dullahan are said to be bearers of ill-omen. The entity carries his severed, decaying head in his right hand. Wherever the Dullahan stops, it is said that a mortal will shortly die on that spot.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Landlady Neill</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Donaghadee - Grace Neill's public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Though only appearing once, dressed in her Victorian garb, Grace Neill's ghost is more likely to create mild poltergeist effects, such as switching on and off electrical equipment and moving bottles and glasses around the bar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Missing Sheep</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dromara - Farms around Dromara and Hillsborough; Hill Road, Ballynahinch Road and Mullaghdrin Road areas<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> August / September 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> A large cat was held accountable for mauling and killing sheep in this area. Police issued a warning for anyone who spotted a large cat to contact them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lord Tyrone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dromore - Gillhall Estate area (original building (Gillhall) destroyed by fire in 1969)<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Tyrone in 1693, other entities mid twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of Lord Tyrone visited Lady Beresford shortly after his death - he made several predictions which were to become true before touching her wrist to prove she was not dreaming. This touch withered her wrist, and she wore a piece of black cloth over the wound for the rest of her days. It is worth noting that the same story is told about a house in Portlaw. Before the building became uninhabitable during the twentieth century, many people claimed to have been driven from the area because of its ghosts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Glowing Ball</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drumbeg - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 16 March 2008, 22:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A glowing ball of light has been seen dancing around the headstones in the newer part of the graveyard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Drumbeg-Upper-Dunmurry-Lane.jpg' class="w3-card" title='A strange-looking figure standing on the road.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A strange-looking figure standing on the road.</small></span>                      <p><h4><span class="w3-border-bottom">Older Woman with Child</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Drumbeg - Upper Dunmurry Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> A withered lady carries a young child in her arms. One story says she was the wife of a highwayman hanged nearby, after which she killed herself and her child.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Muirdris</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dundrum - Dundrum Bay, was known as Lough Rudraige<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> When Fergus mac Leti, a king of Ulster, first encountered the monster known as Muirdris, his face was left permanently contorted in terror. To prevent the king from finding out, all mirrors were banned in his company. Seven years later, Fergus discovered the truth and returned to Dundrum Bay to kill the monster. After a battle which lasted two days, the king killed the beast but himself died soon after from exhaustion.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bottled Spirit</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Finnis - Dead sycamore tree in the village (no longer standing), near the Dree Hill Road bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century (tree fell in July 2009)<br>
              <span class="w3-border-bottom">Further Comments:</span> The bridge was once home to a malevolent spirit, until a brave village priest exorcised the ghost, trapping it in a bottle which was placed in the sycamore tree. It is said that no villager will touch the tree, and locals will prevent visitors to the area from doing so. The tree fell during a storm in July 2009, but an isolated patch of grass marks the spot where it once stood.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Countess of Clanbrassil</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killyleagh - Killyleagh Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once renowned for her charm and leadership abilities (she negotiated a pardon for her husband from Cromwell), the Countess is now said to walk in the old hall.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Radiant Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Killyleagh - Unknown house in the area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Every seven years<br>
              <span class="w3-border-bottom">Further Comments:</span> Murdered by her own mother, the glowing ghost of this little girl returns to the scene of the crime once every seven years.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kircubbin - Nun's Quarter<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s?<br>
              <span class="w3-border-bottom">Further Comments:</span> A floating, headless nun was spotted by a group of friends - they ran, shocked at what they had witnessed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Young Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Lisburn - M1 between Lisburn and Hillsborough<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s onwards?<br>
              <span class="w3-border-bottom">Further Comments:</span> This motorway is reported to be haunted by a young woman who hitchhikes - like the urban myth, she vanishes shortly after accepting a lift.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Gollum</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Mourne Mountains - Slieve Binnian<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 06 January 2019<br>
              <span class="w3-border-bottom">Further Comments:</span> A couple out walking on top of this mountain photographed a strange looking entity in the mist, which resembled Gollum (or a sitting hiker temporarily hunched over). Other ghostly entities are said to haunt the area, although little more is known about them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dead Foot</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newry - Ballybot area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A local legend told of 'Dead Foot', the ghost of a man who had lost his leg in the Queen Street Mill and could be heard passing by doors. Locals would rush outside hoping to catch a glimpse of the entity, but Dead Foot could never be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chase</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newry - Ballyholland area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1860s<br>
              <span class="w3-border-bottom">Further Comments:</span> A young girl found herself pursued by an entity only she could see. The exact details are not known, but it appears the ghost was banished, and nothing disturbed the girl again.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Newry-Lough-House.jpg' class="w3-card" title='A phantom boat drifts along the water.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom boat drifts along the water.</small></span>                      <p><h4><span class="w3-border-bottom">Lough Keeper</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Newry - Lough House Number 6, and disused Forsythe's Canal<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Local folklore says this run down house is haunted by a former lough keeper and his wife. The nearby canal is home to a phantom boat and other spirits with connections to the house.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Strange Figures</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portaferry - Cooley Wells<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s / 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Several reports of phantom voices and visions of ghostly figures exist based around the area of this set of ancient wells.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Three 'Ghosts'</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Portaferry - Innisfallen Farm (likely no longer exists)<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This farmhouse had a reputation as being so haunted that no-one could stay there; the owner moved to England, leaving the land to be used freely by a local farmer. The owner promised fifty pounds to anyone who would stay there and banish the ghosts, and the offer was taken up by a constable. During his first night, the constable was confronted by three ghostly figures. In best policing form, he drew his truncheon and set upon the entities, who quickly revealed themselves to be the local farmer and two sons. The constable was paid and became the landlord of the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Warrenpoint-co-Down.jpg' class="w3-card" title='An old postcard of Warrenpoint in County Down.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Warrenpoint in County Down.</small></span>                      <p><h4><span class="w3-border-bottom">Lord Blaney</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Warrenpoint - Carlingford Lough<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1916<br>
              <span class="w3-border-bottom">Further Comments:</span> A Steam-Packet which travelled between Warrenpoint and Liverpool, the Lord Blaney broke in two when it hit a sandbank, sinking with most of its crew and travellers. The ship is said to reappear when a naval disaster is close to hand, such as in November 1916 when the Connemara and the Retriever collided and sank.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 22 of 22</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northernireland.html">Return to Northern Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
