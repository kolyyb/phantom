
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="London Ghosts, Folklore and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > NW1 - NW11 Districts</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>London (NW1 - NW11) Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Candles</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - 33 Harewood Square (no longer standing)<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1851<br>
              <span class="w3-border-bottom">Further Comments:</span> Watched by a couple as they lay in bed, two 'candle flames' hovered over them. When the male witness grabbed the flames, they turned to liquid, running over the bed and fading to nothing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Trotting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - Camden Market (Stables Market section)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Traders have reported the sounds of horses trotting on the cobbles, and occasionally cries for help from long forgotten accidents.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nw1_3613.jpg' class="w3-card" title='Platform 10 of King&#039;s Cross Station, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Platform 10 of King&#039;s Cross Station, London.</small></span>                      <p><h4><span class="w3-border-bottom">Boudica's Grave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - King's Cross Station, Platform 10<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present?<br>
              <span class="w3-border-bottom">Further Comments:</span> The final resting ground of the warrior queen is reported to be under this busy platform.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blonde Women</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - Kings Cross Tube Station, escalator from Victoria to the Circle Line<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> August 2001, late one Tuesday night<br>
              <span class="w3-border-bottom">Further Comments:</span> A late night passenger, one of the last from the tube, was travelling up an escalator when he heard shrill female voices behind and below. The witness turned around, and even though nothing could be seen, the escalator shuddered as if people were running up it. Then two women appeared, brushed past the witness, then suddenly vanished and the shuddering ceased. The two women were described as both blonde, young and in jeans, with one in a bright yellow puffer gilet jacket.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Distressed Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - King's Cross underground station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1998<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness spotted a woman in her twenties with long brown hair, wearing jeans and t-shirt. The figure was kneeling at the side of the corridor with her arms outstretched, and appeared distressed and crying. Someone walking in the opposite direction then walked through the woman. The witness said that upon reflection, it was like watching a repeating piece of film.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/nw115199.jpg' class="w3-card" title='Inside London Zoo.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Inside London Zoo.</small></span>                      <p><h4><span class="w3-border-bottom">The Whistler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - London Zoo, particularly the aquarium area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2002<br>
              <span class="w3-border-bottom">Further Comments:</span> The phantom whistling that unnerves employees is thought to belong to a tuneful engineer from the early nineteenth century.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/hitler.jpg' class="w3-card" title='The wax Hitler in Madame Tussaud&#039;s Waxworks.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The wax Hitler in Madame Tussaud&#039;s Waxworks.</small></span>                      <p><h4><span class="w3-border-bottom">Chamber of Horrors</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - Madame Tussaud's Waxworks, Marylebone Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This area is reportedly haunted by the ghosts of a few of the wax models. Other bizarre stories come from the waxworks, including a tale about how the hair on Adolph Hitler's wax head started to grow.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Jogger</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - Outer Circle, Regent Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 November 2007<br>
              <span class="w3-border-bottom">Further Comments:</span> Even though rain had cleared all the evening joggers from the area, two friends standing on the Outer Circle heard the distinct sounds of someone jog by them, the runner remaining invisible.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lion</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - Regent's Park<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1930s (glowing lion), August 2010 (glowing figure)<br>
              <span class="w3-border-bottom">Further Comments:</span> A zookeeper and his granddaughter came across the glowing apparition of a lion while out walking in the park at evening. The creature faded away as they approached it. The keeper knew that one of the lions at his zoo was ill, and when he went into work the following day, was told the sick lion had died the previous evening - at the time of the sighting. More recently, a glowing green figure was observed by a witness in the park after midnight and had actually caused the witness to flee; another person with the witness was unable to see anything. The witness returned to the scene the following day to try to understand what they had seen and realised the figure first manifested in a bush and had moved at a very fast pace.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Chairs</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - St Pancras Railway Station<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> July 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Several workers at the Eurostar terminal have reported poltergeist activity in the corridors that run parallel with Pancras Road. Doors have been opened and closed and chairs refuse to budge when people try to push them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Alarm</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - The New Diorama Theatre, Camden<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2021<br>
              <span class="w3-border-bottom">Further Comments:</span> Duty managers at the theatre have reported the door of the disability-access toilet locking itself, and the alarm being triggered, when the facility is unoccupied.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Robert Neville</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 - Volunteer public house, Baker Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in the clothes he wore four hundred years previous, Neville now lurks in the cellar of this public house, occasionally materialising so that he is not forgotten.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bruno Hauptmann</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW1 (possible) - Bakerloo Line<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 1983<br>
              <span class="w3-border-bottom">Further Comments:</span> A photograph of a ghostly figure taken by Karen Collett while travelling on the Bakerloo Line later transpired to be the waxwork figure of Bruno Hauptmann, although it is still unclear how Hauptmann's form (with added bolts of lightning) managed to manifest on the photograph.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fat Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW10 - St Mary's Church, Neasden<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This jovial looking monk has been seen standing on the site of an old well in the garden of the vicarage. He sometimes pops across to the church, which is also haunted by a priest that plays with the doors.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Will Writer</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW11 - Golders Lodge (now demolished), Golders Green Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Golders Lodge was bequeathed (c1810) to a certain person on the condition that the author of the will's body 'remained above ground'. The new tenant is said to have kept the body of the former owner hidden away in the loft for a considerable period. Since the Lodge was demolished, the ghost of that first occupier has haunted the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Banana</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW2 - Above Wembley Stadium<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 15 and 17 March 1985<br>
              <span class="w3-border-bottom">Further Comments:</span> A flying banana shaped object was seen in the sky over the stadium a couple of times. It was described as bright and made up of several different colours.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Scareship</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW2 - Dollis Hill<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> February 1932<br>
              <span class="w3-border-bottom">Further Comments:</span> A possible early ufo encounter. A married couple spotted two men wearing caps working on a cylinder-shaped craft perched on the ground. As the married couple approached for a better look, the men disappeared, and the craft took off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Boudica's Grave</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Barrow known as Boadicea's Grave, Hampstead Heath<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> Local folklore says that the barrow is Boudica's final resting site, although it could just be a Bronze Age burial mound belonging to someone else entirely.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Head</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Church Row, Hampstead<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown, early morning<br>
              <span class="w3-border-bottom">Further Comments:</span> The shade of a servant girl carrying a carpet bag has been seen walking down this street, heading towards the church. Contained within the bag are the remains of a child's body, which the maid had killed, and she is en route to dispose of the pieces in the churchyard. The entity was also said to haunt an unnamed building along the road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sighing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Church Walk, Hampstead<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Loud sighing could be heard throughout the house, believed to be the site where a mother murdered a young child.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Close to the corner of Finchley Road and Frognal<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Local folklore once placed a ghostly woman in white here, although the finer points of the entity are not forthcoming.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - East Heath Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This smiling old man, dressed in a brown overcoat, follows witnesses along the road, quickly fading into the air if challenged.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Victorian Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Fitzroy Park near junction with Millfield Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Spring 1975, around 22:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Four people in a parked car spotted a woman wearing a Victorian style dress glide past. Although dark outside, the woman was illuminated, and made no sound on the gravel track. The witnesses quickly drove away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stress Inducing Shade</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Gatehouse public house, Hampstead Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1947<br>
              <span class="w3-border-bottom">Further Comments:</span> The landlord of this pub apparently collapsed after seeing the spectre and retired soon after. The ghost is thought to be an old white-haired smuggler murdered on site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Turpin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> NW3 - Hampstead Heath<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Sighting of ghosts unknown, cryptid in 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> Thought to be none other than Dick Turpin, this ghostly robber rides around the heath after dusk on his jet black horse. Another ghost on the common land is that of a nineteenth century Member of Parliament who swallowed poison nearby; his shade can be identified by its top hat. A third ghost was spotted by two boys out playing on the heath, who reported watching an old man silently digging a hole. When they returned to the spot the following day, the ground was undisturbed. In 2005 a man encountered a large, winged creature which tore off the tops of trees as it passed overhead.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 54</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=54"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=2&totalRows_paradata=54"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Main London Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Reports: People</h5>
     <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View tReports: People">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2024</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
