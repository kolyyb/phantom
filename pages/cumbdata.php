
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Cumbria Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/northwest.html">North West</a> > Cumbria</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Cumbria Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Emma</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aira Force - Waters of the fall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Waiting for her true love to return from knightly duties, Emma waited by the river every day, often falling asleep there. When her hero eventually came home and found Emma sleeping, he awoke her - startled, Emma fell into the water and drowned. Her white spectre may still haunt the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Black Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allithwaite - Outskirts of village, off the main road<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Walking along the side of the main road out of village, this witness watched a large black panther-like cat in one of the fields, heading towards a nearby patch of woodland. The witness could compare the height of the creatures to nearby cows and estimated the cat to be three foot (ninety centimetres) high at the shoulder. After a minute, the creature walked into the trees.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nanny or Witch</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Allonby - Crookhurst Farm<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> The site is said to be haunted by a nanny and a couple of children who died in a fire on the site. Some say the nanny may have been a witch, and her body never recovered from the ruins.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mrs Pattinson's Portrait</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Alston - Angel Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2020s<br>
              <span class="w3-border-bottom">Further Comments:</span> Local press reported that spooky events would break out whenever the portrait of former landlady Mrs Pattinson was moved, including disembodied singing and small items thrown around.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Watcher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ambleside - Clock Tower<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people, their experiences separated by thirteen years, have reported a strange feeling and mild poltergeist activity around the toilets.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/cum6248.jpg' class="w3-card" title='Kirkstone Pass Inn, Ambleside, Cumbria.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Kirkstone Pass Inn, Ambleside, Cumbria.</small></span>                      <p><h4><span class="w3-border-bottom">Coach Fatality</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ambleside - Kirkstone Pass Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A young boy hit and killed by a coach and four has found it difficult to leave this mortal world. A woman, named by some as Ruth Ray, froze to death after being caught in a freak snowstorm. The child she carried in her arms survived, and Ruth now remains to warn others against venturing out during cold winter nights. Finally, a seventeenth century coachman lurks around the bar.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Wordsworth</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ambleside - Rydal Mount<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wordsworth lived at Rydal Mount until his death in 1850 - his spirit is reported to remain here, although whether in metaphor or metaphysics is unclear.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-56.jpg' class="w3-card" title='An old postcard of Appleby Castle in Cumbria.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Appleby Castle in Cumbria.</small></span>                      <p><h4><span class="w3-border-bottom">Lady Anne Clifford</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appleby - Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The castle may have been haunted by Lady Anne Clifford, though no recent reports exist.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Appleby-Unidentified-stone.jpg' class="w3-card" title='A phantom shepherd escapes.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom shepherd escapes.</small></span>                      <p><h4><span class="w3-border-bottom">Shepherd</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Appleby - Unidentified stone in the village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a shepherd once haunted a home in the village, but someone banished the entity to a nearby large stone. Many years later, a party being held close to the stone disturbed the shepherd, who floated out of the rock and into the nearby river.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Relocation</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Arlecdon - Church<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The church should have been constructed in a different location, but every night the previous day's building work would be found deconstructed. The workmen eventually gave up and built the church where it now stands.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady in Brown</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Armathwaite - Fox & Pheasant Inn<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> A previous landlady and her friend observed a woman wearing old fashioned brown clothing walk through the closed front door. The ghost is said to be a woman who lost both her children outside the inn in a coach accident. A highwayman is also reputed to haunt the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Armathwaite-Tarn-Watheling.jpg' class="w3-card" title='Sir Gawain in the mist.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Sir Gawain in the mist.</small></span>                      <p><h4><span class="w3-border-bottom">Gawain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Armathwaite - Tarn Watheling<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> One Arthurian story places Sir Gawain here, jousting with a knight from Scotland to see if he is worthy to sit at the Round Table.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ringing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Armboth Fell - Thirlmere Lake<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The echoes of phantom bells have been heard coming from beneath the waters here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">May Marye</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Askerton - Askerton Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Once murdered by a lover, now May extracts her revenge by jumping on the backs of passing horses and terrifying their riders. It is reported she once engaged one rider in conversation, making him swear on his life that the topic of discussion would never be revealed to anyone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lord Lonsdale</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Askham - Area around St Peter's Church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Driving a phantom coach, the ghost of Lord Lonsdale passes through this location.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shadowy Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Askham - Punchbowl public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Although the landlady of this public house had never seen anything, a few of her staff were said to have encountered a shadowy man, with no discernible features.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Strangling</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aspatria - Gill House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1940s<br>
              <span class="w3-border-bottom">Further Comments:</span> While being used by the Woman's Land Army during the Second World War, many of the women reported seeing mysterious shadows, hearing weird sounds, and one even reported feeling an invisible presence throttling her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aspatria - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> Following a death in the house, every night for a two week period the occupant heard someone walking from the front door to the kitchen and cupboards being opened.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pony with Coffin</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bakerstead - Road passing the village from Burnmoor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom pony with a coffin strapped to its back is doomed to make the same journey over and over. Both pony and pack were lost during a funeral procession that became caught in a freak snap snowstorm.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Former Worker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barrow - BAE shipyard<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2005 & 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A shadowy figure, loud bangs, and a crane which automated itself were all manifestations of what many employees believed to be a former member of the workforce who had killed himself.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barrow-In-Furness - Clive Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 2008<br>
              <span class="w3-border-bottom">Further Comments:</span> Driving along Clive Street on a dark rainy night, a driver and his daughter spotted a male figure crossing the road in a diagonal path from left to right. As the driver slowed to let him cross, the figure just vanished. The driver's daughter yelled 'Stop the car, dad, that man just disappeared'. The driver stopped and climbed out the car, but the man was nowhere to be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Flying Kettle</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barrow-in-Furness - Station House, Roose (aka Roosecote)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> December 1930<br>
              <span class="w3-border-bottom">Further Comments:</span> Stationmaster J Jackson and his family moved from the property having experienced a poltergeist outbreak which included destruction of crockery, a flying kettle and a levitating tea-table. A surveyor who investigated the site claimed the poltergeist to be vibrations from passing trains.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tiny Green People</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bassenthwaite - Piel Wyke<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Two children digging on the mound said they unearthed a small cottage with a slate roof, though when they returned a short time later, the construction had vanished. A few days later their father saw two small people in green standing on the hill - they vanished into it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Hairy Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beckermet - Nursery Woods<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 25 January 1998, most recently 1 October 2005<br>
              <span class="w3-border-bottom">Further Comments:</span> A man walking his dog reported seeing a man like creature, just over two metres tall and covered in ginger hair, drinking from a pond in the woods.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Pterosaur?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beckermet - Nursery Woods<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> January 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> A witness reported a large pterosaur-like creature flying above Nursery woods.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 240</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=240"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=9&totalRows_paradata=240"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/northwest.html">Return to North West England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>North East & Yorkshire</h5>
   <a href="/regions/northeastandyorks.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/secondlevel/yorkshire.jpg"                   alt="View North East & Yorkshire records" style="width:100%" title="View North East & Yorkshire records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Wales </h5>
     <a href="/regions/wales.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/secondlevel/southgla.jpg"                   alt="View Wales records" style="width:100%" title="View Wales records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
