
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in April">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > April</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>April - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Viking Raiders</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ludham (Norfolk) - Ludham Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 02 April (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This raiding party appears late at night and heads towards the bridge, blowing hunting horns and cracking whips.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Burge's Pooling Blood</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Acle (Norfolk) - Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 07 April (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Josiah Burge was murdered by a man seeking revenge for the death of his sister. Josiah's blood is now reported to pool at the location on the day of the fatal attack. The stories state that Burge's ghost appeared only once - returning to kill his murderer.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bleeding Stone</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hinckley (Leicestershire) - Churchyard, grave of Richard Smith<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 April (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year this gravestone is reported to leak blood - Richard was murdered by an army recruiting sergeant in 1727. An old folklore book says that the effect was caused by red sandstone being washed away by the rain. The church itself is home to phantom footsteps, believed to belong to a monk.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Charlotte Dymond</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bodmin Moor (Cornwall) - Roughtor (aka Rough Tor), near a memorial stone to Charlotte<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 April (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> While Charlotte's boyfriend was hanged for her murder, it is widely believed he was innocent; this could be the reason why Charlotte has returned, dressed in a gown and silk bonnet.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George Marsh</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bolton (Greater Manchester) - Smithills Hall (aka Smithells Hall)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 April (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Tried and executed for heresy, Marsh is remembered as his footprint (created when he stamped his foot) in the stone floor turns bloody each April 24. His shade is also reported to haunt the Green Room, where he was interrogated, though that is not date dependent. A priest is also reported to haunt the vicinity, as is a photogenic grey lady and a cat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Not Quite Dead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Whittlesford (Cambridgeshire) - Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 April (St Mark's Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> All those who will be buried in the churchyard over the next twelve months appear here and lay in the correct place, before being absorbed into the ground.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">A Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmby Moor (Yorkshire) - Private garden, was known in nineteenth century as the Quakers' Burial Ground<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 April (St Mark's Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A nineteenth century piece of folklore says that by turning around seven times in this garden on St Mark's Eve would result in a man appearing just behind you...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Future Dead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burton (Lincolnshire) - Church<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 April (St Mark's Eve) (reoccurring), 1634<br>
              <span class="w3-border-bottom">Further Comments:</span> This church is another site where two men are said to have stayed awake until midnight on St Mark's Eve to see if the legend about the manifestation of apparitions of those due to die over the forthcoming twelve months was true. It was.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Neighbours</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Haxey (Lincolnshire) - Church<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 April (St Mark's Eve) (reoccurring), but year of event unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two men camped out by the porch to see if the legend were true; whether the phantoms of people due to die over the following twelve months would appear upon this night. After one man fell asleep, the other watched as ghostly manifestations of neighbours he recognised, including the man sleeping next to him, walked through the porch. Sure enough, over the following year, everyone he spotted died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Future Dead</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Scarborough (Yorkshire) - St Mary's Churchyard<br>
              <span class="w3-border-bottom">Type:</span> Manifestation of the Living<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 April (St Marks Eve) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> All those who will die between this date and next year's St Mark's Eve appear in this graveyard on the stroke of midnight and walk into the church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Kate, her Lover and her Father</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Poundstock (Cornwall) - Penfound Manor<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 26 April (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Kate and her lover, John, had decided to elope before her father married her off to someone else. The pair were caught as they tried to run away; the father and John engaged in a sword fight, managing to kill each other, as well as Kate who had tried to break the scuffle up. This meaningless battle is now re-enacted once a year at midnight.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sounds of Fighting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burgh Castle (Norfolk) - Roman castle<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 27 April (fighting) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year the sounds of clashing swords and Roman and Saxon screaming can be heard in this area. Another ghost reportedly observed here during dark nights is a figure that plummets from the ramparts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Dancing Statues</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Burrough Green (Cambridgeshire) - School and village green<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 April (evening) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year the two statues standing above the door of the school come alive and dance on the village green - sometimes their footsteps are visible in the grass.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">HMS Gladiator</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Colwell Bay (Isle of Wight) - Off the coast<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Every ten years since April 1908, last seen Spring 1958<br>
              <span class="w3-border-bottom">Further Comments:</span> An islander who spotted an old ship on misty morning claimed it was HMS Gladiator, which sank in 1908. Some say that the ship is appears every ten years, though it is over half a century since the last report.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rainaldus the Chaplain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Runwell (Essex) - Our Lady of the Running Well parish church<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> January - April (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Rainaldus is said to have practiced black magic, and that the Devil came for him (with the north door of the church still showing the scorch marks created by the Devil's hands). Now the ghost of a monk with a scarred face, dressed in black, haunts the area; and the old Chaplain is blamed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-146.jpg' class="w3-card" title='An old postcard of Rochester Castle.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Rochester Castle.</small></span>                      <p><h4><span class="w3-border-bottom">Lady Blanche de Warenne</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Rochester (Kent) - Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: Blanche on Good Friday or 04 April, Dickens on 24 December (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A bystander in a battle for the castle, the Lady Blanche was killed when an arrow entered her heart - her shade has been observed still staggering with the shaft protruding from her chest. It is also believed that Charles Dickens's ghost haunts the moat; he has been seen both here and at the Corn Exchange clock.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 16 of 16</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5></h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
