
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="W1 - Spooky London. Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/greaterlondon.html">Greater London</a> > W1</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ghosts, Folklore and Forteana of London's W1 District</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/w1_4771.jpg' class="w3-card" title='Dunraven Street, North Row, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Dunraven Street, North Row, London.</small></span>                      <p><h4><span class="w3-border-bottom">Lily Langtry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - 19 Dunraven Street, North Row (formally 17 Norfolk Street, original house demolished)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Lily (aka Lillie) is not alone in this haunting - victims of hangings, Cavalier soldiers and a couple of headless men are also reported to have also been seen here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sarah Siddons</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - 228 Baker Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The former home of the great eighteenth century actress, she is now seen as she passes through walls on the first floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/lon2506.jpg' class="w3-card" title='George Frederic Handel&#039;s House, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> George Frederic Handel&#039;s House, London.</small></span>                      <p><h4><span class="w3-border-bottom">Unknown Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - 25 Brook Street, George Frederic Handel's House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2001<br>
              <span class="w3-border-bottom">Further Comments:</span> Manifesting in Handel's former home, this woman's presence was also marked by the smell of perfume. The building next door may also be haunted by the same ghost - Jimi Hendrix reportedly saw her there during the 1960s. An exorcism in July 2001 should have put an end to her activities.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Shapeless Horror</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - 50 Berkeley Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost that once resided here has been blamed for two deaths in the house - one man was found dead of 'shock', the other jumped through an upstairs window to escape the 'thing' that chased him. Another story tells that the ghost was laid to rest when a ghost hunter fired a silver bullet at the spirit as it appeared and lunged at him.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - 53 Berkeley Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen glazing out of the window, it is thought that this seventeenth century gentleman's daughter eloped, and he waits for her return.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Voices</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Absolute Radio, No.1 Golden Square<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s / 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> Reputedly built on a plague victim burial site, this station's basement studio is said to be haunted by children's voices.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">George Payne</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Albemarle Street<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1960s<br>
              <span class="w3-border-bottom">Further Comments:</span> Two friends returning from a heavy nights drinking spotted their old friend George walking towards them. They assumed that George was on leave, as he was in the army, but when the friends approached, they could see the soldier's face to be grey and dead. They stopped and let George continue walking away. A few weeks later the pair found out that George had died fighting in New Zealand.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cromwell</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Apsley House, aka the Wellington museum<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> 1832<br>
              <span class="w3-border-bottom">Further Comments:</span> Appearing only once, the ghost of Cromwell was seen by Wellington - the spectre warned the Iron Duke to let the 1832 Reform Bill through parliament. Wisely enough, the Iron Duke complied.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Naked Corpse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Argyle Rooms, Regent Street<br>
              <span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 26 December 1832<br>
              <span class="w3-border-bottom">Further Comments:</span> A female witness went into hysterics after she reportedly saw the naked body of a famous man, partly covered by cloth, lying on the floor. Later in the day, the man in question was discovered drowned in the Thames, wrapped around a piece of sail.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Naked Body</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Argyll Rooms (no longer standing), Argyll Street<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1828<br>
              <span class="w3-border-bottom">Further Comments:</span> A visitor to the playhouse was mortified to see a phantom body on the floor close to her seat. The body was mostly naked, partly covered by a cloak, and the visitor recognised the body as a friend. The following day news arrived that the friend had drowned in Southampton, around the time of the encounter.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/w15336.jpg' class="w3-card" title='Baker Street, London.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Baker Street, London.</small></span>                      <p><h4><span class="w3-border-bottom">Glowing Dachshund</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Baker Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1900s<br>
              <span class="w3-border-bottom">Further Comments:</span> A pet belonging to two women, the dachshund disappeared one day, though its phantom manifested several times along the road for a period of weeks afterwards.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footprints</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Baker's Street to St John's Wood, northbound tunnel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s?<br>
              <span class="w3-border-bottom">Further Comments:</span> Bill, an underground track walker, sat down for a break while patrolling the line. He reported disembodied footprints which crunched down in the ballast and appeared before him. The footsteps went straight past him and stopped ten metres from his position. When he finished his rounds, one of his colleagues said that other people had also encountered the footsteps, and they belonged to a workman killed in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lord Nelson</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Brewer Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Prior to his final battle, Nelson reportedly visited an undertaker along this road and ordered his coffin. Nelson's shade has been seen looking into a window that once belonged to the business.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">London Beer Flood Victim</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Dominion Theatre, Tottenham Court Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twenty-first century<br>
              <span class="w3-border-bottom">Further Comments:</span> The theatre was built partly on the site where the Meux and Company Brewery stood. An accident at the brewery in 1814 released 1.5 million litres of beer that destroyed homes and killed several people. It has been reported that a teenage victim haunts the theatre and can occasionally be heard giggling.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Admiral Tryon</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Eaton Place, Belgravia<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 22 June 1893<br>
              <span class="w3-border-bottom">Further Comments:</span> The solid looking form of Admiral Tryon appeared in front of several of his wife's friends at his home, at the exact moment he was going down with his ship in the Mediterranean.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sergeant</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Former Vine Street Police Station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1969<br>
              <span class="w3-border-bottom">Further Comments:</span> This officer of the law, who killed himself in a cell in the nineteenth century, keeps an eye on workers at the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tobacco Tosser</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Fribourg & Treyer Tobacconist (no longer trading), Burlington Arcade<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1955<br>
              <span class="w3-border-bottom">Further Comments:</span> Press carried the story that the shop had acquired a poltergeist after stock was found disturbed, damaged, and thrown to the floor.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lost Children</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Georgian House Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> These children make infrequent appearances, as does the ghostly figure of a man.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/astrologerof19ca.jpg' class="w3-card" title='Lord Lyttleton encountering a ghost, from the book The Astrologer of the Nineteenth Century.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Lord Lyttleton encountering a ghost, from the book The Astrologer of the Nineteenth Century.</small></span>                      <p><h4><span class="w3-border-bottom">Mrs Amphlett</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Hill Street, Mayfair<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 November 1779<br>
              <span class="w3-border-bottom">Further Comments:</span> In his home along this street, Lord Lyttleton woke and saw a woman in white standing by his bed (named by some as being a Mrs Amphlett) who warned him he would be dead within three days. At 11pm on the third day, he dropped dead into his butler's arms at Pitt Place, in Epsom. The illustration is Lyttleton's encounter from the publication 'The Astrologer of the Nineteenth Century'.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Suicide</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Hotel Piccadilly, Room 537, Denman Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost in this room was thought to have been a traveller who slit his wrists in the bathroom. Bumps and banging have woken those who have spent the night here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Sally's Tree</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Hyde Park<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A tree in the park is rumoured to be haunted by Black Sally, murdered as she slept underneath the canopy. The tree no longer stands, though the rumours of ghosts continue. A clump of trees close to Lancaster Gate were also said to be haunted, this time by the groans and cries of a cancer-ridden man named Sammy. Finally, an elm tree was considered 'evil' and none of the local homeless population would spend a night close to it - the tree was said to whisper during the night, trying to convince anyone sleeping nearby to take their own lives.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Face</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Hyde Park Corner underground station<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1978, early morning<br>
              <span class="w3-border-bottom">Further Comments:</span> Two members of staff were alarmed when they discovered an escalator working, though the electrical connection had been disabled. Walking into a room, one man reported hitting a spot so cold that his breath was visible, while the other worker collapsed in a state of shock. The latter claimed to have seen a strange face, and left the station before his shift ended, never to return.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Eyed Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - John Snow public house, Broadwick Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This man with red eyes looks as if he is in terrible pain and sits alone in the furthest corner of the bar. Just outside the pub, a phantom water pump was seen several times, though it has vanished since a real one was installed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man Waiting</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Kingly Court, Beak Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Dressed in eighteenth century clothing, this figure walks around in a circle, whistling to himself (and badly, according to one account).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Footpads</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> W1 - Lansdowne Passage (now called Lansdowne Row)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The shadowy figures reported here are those of criminals who made full use of the old passageways to escape from the law in the nineteenth century.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 47</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=47"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=47"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/greaterlondon.html">Return to Greater London</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View Greater London records" style="width:100%" title="View Greater London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Type </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/reports.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
