

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Ghosts, Folklore and Strange Welsh Places, from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/wales.html">Wales</a> > Gwynedd</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Gwynedd Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdaron - St Mary's well<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s - July 2019, evening hours<br>
              <span class="w3-border-bottom">Further Comments:</span> From this spot screaming and chanting can be heard at dusk, thought to be coming from the nearby Bardsey island.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hoof</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdovey - Carn March Arthur (three miles northeast of Aberdovey)<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Still present<br>
              <span class="w3-border-bottom">Further Comments:</span> The hoof prints from King Arthur's horse can still be seen imprinted in the rock here.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Aberdovey.jpg' class="w3-card" title='An old postcard showing Aberdovey in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard showing Aberdovey in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Lost Town</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdovey - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Swallowed by the sea, church bells belonging to a town lost off the coast here can still be heard on quiet evenings. They say that at low tides one can see sunken tree trunks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crockery Throwing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdovey - Penhelyg House<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1845<br>
              <span class="w3-border-bottom">Further Comments:</span> This noisy spirit became active at night, throwing around crockery, though never breaking it. The activity ceased the day after a visitor to the house read from a bible.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Swallowed Sounds</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aberdyfi - Off coast<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Calm nights<br>
              <span class="w3-border-bottom">Further Comments:</span> The bells of a church which was swallowed by the sea can sometimes be heard when the weather is right.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Children Playing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abersoch - Beach carpark<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Between 22:00h - 01:00h, last heard 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of children playing and laughing have been heard in the carpark, although when people have checked on the children's welfare, no one can be found.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Genaprugwirion</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Abersoch - Burrows in the area<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown - twentieth century?<br>
              <span class="w3-border-bottom">Further Comments:</span> Also known as a cenaprugwirion, this lizard is around thirty centimetres long with an orange head and large eyes. Karl Shuker has speculated that the creature is a tuatara, a living fossil found in New Zealand.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Greyhound</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Amlwch - Stone circle between village & St Ellian Church<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom hound was said to haunt this prehistoric site - it beat up a local preacher a couple of times before moving on to a further plane of existence.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Run and Shout</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anglesey - RAF Valley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970<br>
              <span class="w3-border-bottom">Further Comments:</span> Having just locked the main hangar, the key orderly heard a shout and the sound of running from within. He unlocked the hangar, but no one was could be seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Black Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anglesey - Road towards Wylfa<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> October 2011<br>
              <span class="w3-border-bottom">Further Comments:</span> While driving to the power station, this witness spotted a black cat larger than an Alsatian. The creature had a long bushy tail and sat in a crouched position. The witness watched the creature for three or four seconds and was slightly unnerved by it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ivy</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anglesey - Ruins of Onions public house, Red Wharf Bay<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Ivy Nettleton was murdered by her husband who unsuccessfully tried to conceal her body on the nearby beach. The flitting white figure seen within the ruins of the pub is said to be Ivy's troubled shade.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Panther-Like Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Anglesey - Several areas over the island, including Llandona Beach<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> April 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> A large black cat, thought to be either a puma or panther, has been blamed for attacking horses and stealing foals.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Bala-lake.jpg' class="w3-card" title='An old postcard of Bala Lake in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Bala Lake in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Lost City</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bala - Bala Lake<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> City unknown, Teggie observed March 1995<br>
              <span class="w3-border-bottom">Further Comments:</span> The first town of Bala is said to be concealed under the waters of this lake. In addition, a monster seen in 1995 and nicknamed 'Teggie' by locals, was described as resembling a humped crocodile, although a story published in the same year claimed recent sightings of the creature were a hoax. Teggie sightings date back to the 1960s, and the creature may have first been encountered by two men boating in the lake.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Blodeuwedd</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bala - Castell Gronw<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Blodeuwedd and her lover failed to kill her husband, and as a result he cursed his wife to take the form of an owl. She still flies around the castle, calling out to her other man. Her lover was killed when a spear was thrown into his chest. The man held aloft a stone to try to protect himself, but the weapon pieced it - a holed stone is still in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Watch Your Glass</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bala - White Lion Royal Hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> An entity in the bar is said to knock over unwatched glasses. One story says that at some point in the murky past, a fire at the hotel killed a couple of people who now haunt their former room.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Strange Bird</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bangor - Faenol Estate<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly bird contains the spirit of man executed after stealing wood from the land around the estate. The bird's song is heard by people who are considering stealing something as a warning against their actions.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bangor - New County Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1950s?<br>
              <span class="w3-border-bottom">Further Comments:</span> A possible urban myth says that staff once found a dead man sitting on a chair in the balcony. His ghost is said to remain in the theatre.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Elderly Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bangor - Ysbyty Gwynedd (hospital)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing a brown dressing gown, this elderly man was seen stepping into lift 3. The lift doors had just closed as the witness reached the lift, but they pressed the call button anyway and the doors reopened - the old man had vanished. A phantom ward sister has also been reported on site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Glass Tower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bardsey Island - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> As well as the final resting place of King Arthur, Merlin is said to have kept the Thirteen Treasures of Britain within his glass tower that stood on Bardsey Island. These treasures were: 
1) Dyrnwyn - the sword's blade would burst into flame when wielded by royalty, 
2) The Hamper of Gwyddno Garanhir - the hamper would multiply its contents by five-score, 
3) The Horn of Bran - this drinking beaker would provide whatever the user wanted to whet their lips, 
4) The Chariot of Morgan the Wealthy - extremely fast magical travel, 
5) The Halter of Clyno Eiddyn - any horse dreamt of would appear haltered in the morning, 
6) The Knife of Llawfronedd the Horseman - would carve the meat at a meal for twenty four men, 
7) The Cauldron of Diwrnach - would only boil water for a brave man, 
8) The Whetstone of Tudwal Tudglyd - when used to sharpen the sword of a hero, any strike made by the weapon would be fatal, 
9) The Coat of Padarn Redcoat - would only fit anyone of noble blood, 
10) The Crock of Rhygenydd - would serve any food the user wanted, 
11) The Dish of Rhygenydd - would serve any food the user desired, 
12) The Chessboard of Gwenddolau - would play itself once set up, &
13) The Mantle of Arthur - would make the wearer invisible.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">All Saints</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bardsey Island - General area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Also known as the Island of a Thousand Saints due to the large number of monks buried here, the shoreline spirits reported to appear are believed to be just some of those holy men.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/postcard-Barmouth-gen2.jpg' class="w3-card" title='An old postcard of Barmouth in Wales.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Barmouth in Wales.</small></span>                      <p><h4><span class="w3-border-bottom">Darting Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmouth - Bay area<br>
              <span class="w3-border-bottom">Type:</span> Crisis Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Small lights would dance across the surface of the water just before an accident which involved a drowning.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/1Cader-Idris.jpg' class="w3-card" title='An old sketch of Cader Idris.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old sketch of Cader Idris.</small></span>                      <p><h4><span class="w3-border-bottom">Idris</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmouth - Cader Idris (aka Caldair Idris)<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Idris was said to be a wise giant, who may have had links with King Arthur. People would avoid the area at night, convinced that fairies would drive them mad. Another legend says that the site was the hunting ground of Gwyn ap Nudd (Ruler of the Otherworld) who searched the area with a pack of Cwn Annwn, looking for souls to take back home. Finally. Strange lights are said to appear around the summit for the first few days of a new year.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Hymns</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmouth - Dragon Theatre<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> This theatre has been home to ghost hunts in the past and is reputed to be home to various entities which include a young girl singing hymns, a ghostly cat, and a figure who closes the stage curtains.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horse and Carriage</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmouth - Farchynys Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Midnight (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Moving towards the hall at midnight, the history of this ghostly horse and carriage is unknown.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Snake-like Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Barmouth - Menai Straits<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1805<br>
              <span class="w3-border-bottom">Further Comments:</span> A ship was attacked by a sea serpent, which wrapped itself around the ships mast until the crew hit back - it fell back into the sea but followed the vessel for two more days before disappearing.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 134</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=134"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=5&totalRows_paradata=134"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/wales.html">Return to Welsh Preserved Counties</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Scotland</h5>
   <a href="/regions/scotland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/secondlevel/Lothian.jpg"                   alt="View Scotland records" style="width:100%" title="View Scotland records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East of England </h5>
     <a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastengland.jpg"                   alt="View East of England records" style="width:100%" title="View East of England records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
