
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Ipswich Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

 <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/eastengland.html">East of England</a> > Ipswich</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Ipswich Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - A12 bypass near Ipswich<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> Part of a rash of sightings of ABCs in and around the Ipswich area during 1996, and that have continued well into the 2000s.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Smoky Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - A12 heading towards Orwell Bridge<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 2013<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver watched a pillar of smoke rise and form the shape of a young man. The driver believed that the figure was someone who had died nearby.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Nocturnal Light</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - A12 towards Woodbridge, after Orwell Bridge<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> A family driving home one night watched as a bright light manoeuvred around the sky, before disappearing vertically into the clouds at a fantastic speed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man in Grey</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Belvedere Road / Cemetery Lane junction<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 18 December 2012, 08:50h<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver braked as a man in dark grey clothing and a trilby-like hat stepped out in front of their car. The driver then realised the man had vanished, with no means of passing without the driver seeing. The figure may have also been spotted by a dog walker, who reported a man in old fashioned clothing who tipped his hat before disappearing.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf13414.jpg' class="w3-card" title='Birkfield Drive, Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Birkfield Drive, Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">Large Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Birkfield Drive<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> A man walking home late had encountered a handful of police searching the edge of a small, wooded area. He asked the closest police officer if everything was okay, only to be told that they had had a report of a large black cat in this location and were searching for it.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Glass Pusher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Black Horse Pub<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> February 2023<br>
              <span class="w3-border-bottom">Further Comments:</span> CCTV caught a glass moving across the table before falling to the floor. The pub is reputedly haunted, and local stories say a smuggling tunnel once ran from the cellar to a nearby church.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf818iich.jpg' class="w3-card" title='Christchurch Mansion, Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Christchurch Mansion, Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">Sweeping Female Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Christchurch Mansion<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Summer 1995, and prior<br>
              <span class="w3-border-bottom">Further Comments:</span> Pictures on the walls have been seen turning themselves, while a female witness with her daughter watched a Victorian woman walk by them, passing through a closed glass door. A much older report stated that the mansion was haunted by a maid with two children, who laughed and danced around the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Teapot Smasher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Coffee shop along St Margaret's Street (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1950<br>
              <span class="w3-border-bottom">Further Comments:</span> Over the period of a week, eleven members of staff witnessed cups, plates, and a teapot smashed by an unseen hand. On one occasion, a police officer watched a cup roll down the staircase. A C Henning, famed for his involvement in the Borley Rectory case, came to investigate (his conclusion is not known).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf13656.jpg' class="w3-card" title='Princes Street, Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Princes Street, Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">Walking</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Community building (no longer operating), Princes Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010s<br>
              <span class="w3-border-bottom">Further Comments:</span> Staff working late at night in the building would occasionally hear footsteps and whistling from empty parts of the building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Staring Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Crown Hotel, 346 Felixstowe Road (no longer operating)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> A face of a woman looks down from the window in bedroom 2, even though the room is empty. Mild poltergeist activities have also been reported, the piano has been heard playing itself, and a crashing sound, as of a wardrobe falling over, also reported.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf5565a.jpg' class="w3-card" title='Eagle Street, Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Eagle Street, Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">Robert</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Eagle Street - undisclosed house<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 14 December 1952 (date of exorcism)<br>
              <span class="w3-border-bottom">Further Comments:</span> The local vicar of St Michael's was called in to exorcise this entity which threw items through the air and scattered an unknown powder around rooms. The entity briefly paused its antics after the service, but started again a few days later. The family would appear in court a month later for non-payment of rent, the landlord claiming the haunting started just after he had issued a summons against the occupants.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf6960.jpg' class="w3-card" title='Foxhall Road, Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Foxhall Road, Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">A Very Black Cat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - East Ipswich (Felix Road, Foxhall Road areas)<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 21 August 2006<br>
              <span class="w3-border-bottom">Further Comments:</span> The local paper reported that two people had seen a large cat, 'very black' in colour, in separate incidents. The first was along Felix Road at around 03:30h, followed by the Foxhall Road sighting two hours later. One witness said that the cat was not as big as a panther but was much larger than a normal cat.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Crying Baby</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Flat along Foxhall Road<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> February 2003-July 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> A medium who came to cleanse the flat failed to do so and the tenants finally moved out. They had reported poltergeist-like activity, loud bangs and footsteps, the muffled sounds of a baby crying, and black and grey shadowy figures.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Poltergeist</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Former Lakeland Plastics building<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> Believed to be the ghost of Mother Lakeland, a wise woman/witch who once lived in the area, this entity has been blamed for moving displays around the shop. As a side note, contained within the shop is a fifteenth century chapel that remained undiscovered for many years. It is now open to the public.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf6227.jpg' class="w3-card" title='Gippeswyk Hall, Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Gippeswyk Hall, Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Gippeswyk Hall (currently occupied by Red Rose Chain)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nights of the full moon (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly white lady has been seen by several witnesses in the upper part of the hall. One person reported a strange mist emerging from the chimney, though no fires were burning within.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf6274.jpg' class="w3-card" title='Great White Horse Hotel, Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Great White Horse Hotel, Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">Grey Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Great White Horse Hotel (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This ghostly woman who has been observed walking through a wall is said to have died in a fire on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf6855.jpg' class="w3-card" title='Ipswich Film Theatre, Suffolk.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Ipswich Film Theatre, Suffolk.</small></span>                      <p><h4><span class="w3-border-bottom">Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Ipswich Film Theatre (Cinema), Town Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Late twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Several staff at this building reported a shadowy figure walking around the rear corridors near the protection booths.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf12767.jpg' class="w3-card" title='Isaacs public house on the docks, Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Isaacs public house on the docks, Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">Footsteps</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Isaacs public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 December 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> While alone in the gents' toilets, one man heard the door open and close and heavy booted footsteps approach. Then, after hearing a loud sigh in his ear, the witness turned but to see who had come in, but realised he was alone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Paws</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Kelly Road<br>
              <span class="w3-border-bottom">Type:</span> ABC<br>
              <span class="w3-border-bottom">Date / Time:</span> 1996<br>
              <span class="w3-border-bottom">Further Comments:</span> Part of a rash of sightings of ABCs in and around the Ipswich area during 1996. The cat was nicknamed 'Paws' by the local press and a reward was posted for evidence. In the 2000s a witness came forwards to say that the 'cat' was a black Labrador which would run freely around the area, and most locals were aware of this at the time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf10566.jpg' class="w3-card" title='London Road in Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> London Road in Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">Kangaroo</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - London Road<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 12 September 2011, around 16:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A driver and passenger reported seeing a young kangaroo travelling along a busy Ipswich road.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Heartbeat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - McGinty's public house (currently Halberd Inn)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Heart from twentieth century, girl in July 2004<br>
              <span class="w3-border-bottom">Further Comments:</span> If you place your ear on one of the walls of this Irish theme pub, one can hear the faint beating of a human heart. Maybe it was the same entity which once turned the all the fuses off at a junction box, saving the landlord from electrocution. In 2004 a visitor staying at the pub awoke to see a little girl in the room, even though the only other person there was the publican.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf14821.jpg' class="w3-card" title='The home of an Ipswich poltergeist.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The home of an Ipswich poltergeist.</small></span>                      <p><h4><span class="w3-border-bottom">Plate Smasher</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Olde Tudor Café (no longer operating)<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa November 1950<br>
              <span class="w3-border-bottom">Further Comments:</span> Eleven members of staff witnessed the strange smashing of cups, plates and a teapot, thrown through the air by hands unseen. A C Henning, of Borley fame, was called in to investigate.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Spinning Lights</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Orwell Bridge and the A12 towards Felixstowe, and Nacton Road<br>
              <span class="w3-border-bottom">Type:</span> UFO<br>
              <span class="w3-border-bottom">Date / Time:</span> 26 August 2006, around 04:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> Two dull, spinning lights followed a teenager driving across the bridge for several miles. The lights always remained in front of the car, turning at the same time as the driver. Several other witnesses reported seeing similar lights around the Nacton Road area at roughly the same time.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/suf6922.jpg' class="w3-card" title='Pipers Vale, Ipswich.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Pipers Vale, Ipswich.</small></span>                      <p><h4><span class="w3-border-bottom">Pale Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Pipers Vale, aka The Lairs<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s, 2000s<br>
              <span class="w3-border-bottom">Further Comments:</span> This pallid person spotted standing around the area could be the phantom of one of many suicides which have occurred on the nearby Orwell Bridge. At least one witness has heard an invisible horse.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Teenager</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ipswich - Pipers' Court<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1990s<br>
              <span class="w3-border-bottom">Further Comments:</span> This building is said to be haunted by the ghost of a teenager who died within. He appears on CCTV cameras but is not visible in the real world.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 42</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=42"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=42"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
    <button class="w3-button w3-block h4 w3-border"><a href="/regions/eastengland.html">Return to East of England</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> East Midlands </h5>
     <a href="/regions/eastmidlands.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/eastmidlands.jpg"                   alt="View East Midlands records" style="width:100%" title="View East Midlands records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>

<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>Â© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
