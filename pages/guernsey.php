

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A List of Guernsey Ghosts and Mysteries from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/otherregions.html">Other Regions</a> > Guernsey</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>Guernsey Ghosts, Folklore and Forteana</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Headless Figure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castel - Fauxquets Valley<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> December (reoccurring), between 22:00h - 00:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> A tall headless figure is said to haunt the valley during the winter months.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Castel-Les-Mourains.jpg' class="w3-card" title='A phantom woman trapped in a wardrobe.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom woman trapped in a wardrobe.</small></span>                      <p><h4><span class="w3-border-bottom">Angry Wife</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castel - House known as Les Mourains (may no longer stand)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Possibly mid-to-late-eighteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A woman who lived here died after giving birth to two sons, although she had time to make her husband promise not to remarry. Her husband failed to keep the promise, and shortly after his second marriage, his first wife came home post-mortem. She was seen by the children's nanny and the sound of her silk dress heard along the corridors. The family locked up the drawing room as it was the ghost's favourite place to manifest. Eventually the rector of the parish banished the ghost to a cupboard which was then boarded up, although sometimes the former wife could be heard behind the planks.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Funeral Procession</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castel - Mont d'Aval road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Slowly walking along carrying a coffin, this group of phantoms appear to have lost their heads.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Coffin Carriers</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castel - Moulin de Haut (lane)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Around Christmas (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> To spot the phantom funeral procession is said to bring bad luck or death (or both!).</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fairy Treasure</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castel - Rocky outcrop in the parish of Ste Marie du Castel<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This rocky pile was once said to be home to fairies and witches, and it concealed a massive wealth of treasure within.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Castel-Rue-du-Dos-dAne.jpg' class="w3-card" title='Phantom holding a lantern.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Phantom holding a lantern.</small></span>                      <p><h4><span class="w3-border-bottom">Zigzagging Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Castel - Rue du Dos d'Ane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Pre-1933<br>
              <span class="w3-border-bottom">Further Comments:</span> Said to have been seen on more than one occasion, this road was the haunt of an old woman who would zigzag along holding an old lantern. She would vanish next to a field.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tchen Bodu</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clos du Valle - General area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> This black hound, known locally as tchen Bodu, is seen as a death omen for either the witness or someone close to them.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Germans</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Forest - Le Bourg (road)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-to-late-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A line of soldiers wearing grey uniforms walking along this road are said to be occupying German forces from the Second World War.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Drowned</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> La Braye du Valle - Stone bridges known as Le Pont Colliche and Le Pont St Michel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> La Braye du Valle was once a tidal channel, crossable only by using two stone bridges. Over the years, a handful of people have slipped from the bridges and drowned, leaving their ghosts to haunt the area. Le Pont St Michel was additionally haunted by a 'feu bellenger', or Will-o-the-Wisp, and the cries of someone in distress.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Saint-Martin-Profond-Camp.jpg' class="w3-card" title='A ghostly hare.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly hare.</small></span>                      <p><h4><span class="w3-border-bottom">Hare</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Saint Martin - Area around Profond Camp<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Weather dependent: stormy nights<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom white hare was said to manifest when storms rolled through the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Man with Goat</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Saint Martin - Blanche Pierre Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Leading a white goat with a rope, this phantom man has not been reported for quite a while.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Person</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Saint Martin - Corner of Les Maindoneaux and The Hermitage<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Little was known about this phantom until adjustments were made to the road and a coffin discovered containing large human bones. When properly buried the ghost never returned.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-12.jpg' class="w3-card" title='An old postcard of Saint Martin in Guernsey.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Saint Martin in Guernsey.</small></span>                      <p><h4><span class="w3-border-bottom">Sounds of Killing</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Saint Martin - La Petite Port<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The sounds of wailing and crying, thought to be linked to a past murder, would cause people to avoid this area at night, until a man armed with a bible spent the night on the beach. He would not say what happened, other than reassure people the sounds would never return.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Funeral</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Saint Martin - Lanes around the area of Le Hurel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> All members of this ghostly funeral procession were said to be headless.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Matthew</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Saint Martin - Sausmarez Manor pond<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghostly figure wearing a long grey coat was named as Matthew De Sausmarez, a former owner of the manor. The road running alongside the manor was another area haunted by a black dog.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">German Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Andrews - German Military Underground Hospital and Ammunition Store<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 2008 (Photograph taken)<br>
              <span class="w3-border-bottom">Further Comments:</span> Several phantoms are said to haunt this former German building, including a ghostly soldier and a lady. Disembodied voices have also been reported (and recorded). In 2008 a photograph was taken which some believe show a spirit, although it is more likely to be a result of a long exposure and camera shake.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Stone Thrower</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Andrews - Homes on west side of Four Cabot<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> 1951<br>
              <span class="w3-border-bottom">Further Comments:</span> Residents were forced to cover their west-facing windows with meshed chicken wire after windows were broken by stones. A police investigation found nothing, and some people blamed a poltergeist.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mr Blondel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Peter in the Wood - Church at St Pierre du Bois and rectory<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Mr Blondel left instructions that on his deathbed the church bell should be tolled to show his passing. However, when Blondel died the bell remained silent, so his ghost took to climbing the church tower and throwing crockery around in the rectory kitchen. Thirteen members of clergy gathered and locked the ghost in a cupboard.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Vanishing Lady</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Peter Port - Amherst Stores (no longer operating), Amherst Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 1970s-1980s<br>
              <span class="w3-border-bottom">Further Comments:</span> A cashier in this store during the 1970s watched a woman in old fashioned clothing walk in and move from sight down an aisle. After fifteen minutes, the cashier grew concerned and tried to find the woman, but she had vanished with no possible way to leave the store. During the 1980s, wailing or howling started to be heard in the store, but only to the same group of people, and only when they were together.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Three Horsemen</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Peter Port - Colborne Road<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1900<br>
              <span class="w3-border-bottom">Further Comments:</span> Three figures riding white horses were said to gallop loudly down the hill.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Peter Port - Elizabeth College<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Local legend says this college is home to a phantom Nazi soldier and, perhaps more tongue in cheek, to a student accidently locked in the library over the summer holidays who starved to death.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Floating Shadow</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Peter Port - Flat in the vicinity of Berthelot Street and Lefebvre Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early 1972-1973<br>
              <span class="w3-border-bottom">Further Comments:</span> Over a period of nine months, a shadowy entity had manifested six times to one witness. The entity would hover over the witness and touch her right arm before vanishing. Two visiting mediums had said that the entity was either a sailor or pirate who had once lived on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Peter Port - Former garden at the corner of Berthelot Street and Lefebvre Street (no longer present)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> This phantom older woman, wearing old fashioned clothing was observed close to the former garden by a nearby house owner, whose own home in turn was haunted by a tall, cloaked man.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Peter Port - Hauteville House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Early nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> Prior to Victor Hugo moving into the property, this house was empty due to the belief that it was haunted by the ghost of a woman who committed suicide.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> St Peter Port - Havilland Vale<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa late nineteenth century<br>
              <span class="w3-border-bottom">Further Comments:</span> A man decided to follow a female phantom known locally for walking through the area at night. The ghost would stop to wring her hands before vanishing. The man marked the spot where she disappeared and returned the following day. Digging at the spot with friends, they discovered the skeleton of a baby. When the bones were buried in a churchyard, the ghost never returned.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 45</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=45"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=45"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/otherregions.html">Return to Other Regions</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Greater London</h5>
   <a href="/regions/greaterlondon.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/london.jpg"                   alt="View London records" style="width:100%" title="View London records">
  </div></a>
  <p></p>
</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5> Reports: Types </h5>
     <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report2.jpg"                   alt="View Reports: Type records" style="width:100%" title="View Reports: Type records">
  </div></a>
  <p></p>

</div>

</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
