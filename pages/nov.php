
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="A list of ghosts and strangeness from the Paranormal Database said to occur in November">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/calendar/Pages/calendar.html">Calendar</a> > November</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>November - Paranormal Database Records</h3>

 

                      
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Ghosts of the Future</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Middleton (Greater Manchester) - St Leonard's Church<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 November (All Souls' Day) (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The souls of all those who will die by the following All Souls' Day walk through the church at midnight. A former member of the clergy is also reported to haunt the church, though his appearance is not date related.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Saint Juthware</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Halstock (Dorset) - Judith Hill (aka Abbots Hill), and area near the Quiet Woman Inn (now a guest house)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 01 November, 01:00h (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The headless phantom of Saint Juthware is reported to walk towards the church with her head in her hands. She was beheaded by her step-brother, tricked into doing so by his mother. The inn was also haunted by a female shadow which vanished as soon as it was seen.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Constantia Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> N17 (Greater London) - Bruce Castle, Tottenham<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 03 November (Coleraine seen) (reoccurring), party seen in 1971<br>
              <span class="w3-border-bottom">Further Comments:</span> The Lady Coleraine is seen once a year, screaming as she jumps from a balcony trying to escape the possessive nature of her husband. A couple walking past one night witnessed a party occurring on site, complete with people dressed in eighteenth century garb - they realised something was strange, as there was no sound, and the figures appeared to glide...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Turn it Over</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Shebbear (Devon) - Devil's Boulder<br>
              <span class="w3-border-bottom">Type:</span> Legend - Old Nick<br>
              <span class="w3-border-bottom">Date / Time:</span> 05 November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year the villagers turn a large stone over that stands by an oak. It is said that if they fail to do so, disaster and ill-fortune will strike the region - one legend says the last time the village forgot to rotate the stone was just before the Second World War began. Some believe the Devil himself is trapped under the stone.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mr Baker</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Melbury Bubb (Dorset) - Dirt track along Bubdown Hill,  known as Murderers Lane?<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 10 November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Mr Baker was a farmer murdered on the hill in 1694. His ghost returns on the anniversary of the crime, possibly driving a horse and carriage.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Marching Troops</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Horley (Surrey) - Thunderfield Castle and Haroldslea Drive area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 11 November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A phantom bell begins to toll as the sun sets here, slowly growing louder until midnight comes and a small army of men pass through the area. The castle is said to have been a resting place for King Harold's men as they marched to Hastings.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Nun</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Outer London: Stanmore (Greater London) - Royal National Orthopaedic Hospital<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 13 November (reoccurring), although could have last been seen 30 November 2009<br>
              <span class="w3-border-bottom">Further Comments:</span> Built on the site of a nunnery, the hospital is now visited by a gliding figure that is thought to come from the former building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cries</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Pyecombe (Sussex) - General area of village<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 23 November (St Clement's night) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Cries for help are said to come from the area where the village smith was once based.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Old Coles</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Leigh (Hereford & Worcester) - Road passing through village, leading to river<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 24 November, St Catherine's Eve (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Long since exorcised, the phantom of Old Coles would drive his coach through the village and use the local river to cool his horses.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sleeping Sentry</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Kilkenny (County Kilkenny) - Foulksrath Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 29 November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A guard at the castle once fell asleep while on duty and was thrown from the battlements by his superior officer as punishment. Once a year, his footsteps are heard as he walks around to make amends for his inappropriate actions. A ghostly woman can also be heard as she moves around on a staircase, accompanied by the smell of wildflowers.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sunk Church</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Romford (Essex) - Old Church Road<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> 30 November (St Andrew's Day (reoccurring))<br>
              <span class="w3-border-bottom">Further Comments:</span> After the church sank into the ground, the only 'evidence' of its existence is the phantom ringing of the bells.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Lady Wintour</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Huddington (Hereford & Worcester) - Huddington Court, and road outside<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 January (reoccurring), road encounter in November (year unknown)<br>
              <span class="w3-border-bottom">Further Comments:</span> One story says that this decapitated shade that appears once a year still mourns the passing of her husband Robert Wintour, executed on 31 January 1606 for playing a part in the Gunpowder Plot. One November evening, a couple driving past the court spotted what they believed to be a person riding a bike, but as they passed, realised the figure to be a woman in a cloak who vanished. The couple stopped at a nearby pub, and being visibly shaken, the landlord guessed that they had encountered Lady Wintour, who would glide down the lane.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Bedworth-Newdigate-Pit.jpg' class="w3-card" title='A phantom miner down the pit.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A phantom miner down the pit.</small></span>                      <p><h4><span class="w3-border-bottom">Cloth Cap</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Bedworth (Warwickshire) - Newdigate Pit (no longer operational)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November (normally) (reoccurring), 1930s onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a former miner who died when the cable of a lift snapped, sending him falling to his death, 'Cloth Cap' was occasionally spotted down the mine. One witness spotted the miner wearing a long black coat with the collar raised.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Sister Barbara</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Marham (Norfolk) - Vinegar Hill<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> The sister would pay men to pretend to rob travellers, before 'saving' them, and receiving rewards that she used to pamper herself. She was reported by a group of monks and was subsequently bricked up alive behind a wall. She now walks the hill, gliding silently along.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mr Goddard</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Whitestone (Warwickshire) - Private house along Gypsy Lane<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A former owner of a house along here, Mr Goddard's ghost returned after the division of his garden and a new property built on the site. He makes his presence known by opening locked doors, moving items around rooms, and shorting out electrical items.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Duel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Youlgreave (Derbyshire) - Youlgreave Hall<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November (unknown night) (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Once a year a ghostly Cavalier and a phantom Roundhead meet and fight. It is not known who wins.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Cheriton</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Folkestone (Kent) - Enbrook Manor<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> November / December (?) 1917<br>
              <span class="w3-border-bottom">Further Comments:</span> Poltergeist activity broke out around the site of an excavation, with one man hit multiple times by rocks. Another witness claimed to have seen stones rise and fall from the ground. A hammer, chairs and a pickaxe were all seen to move unaided. Sir Arthur Conan Doyle visited the area but did not offer an explanation to the events.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Dilton-Marsh-High-Street.jpg' class="w3-card" title='The ghost with a wide brimmed hat.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> The ghost with a wide brimmed hat.</small></span>                      <p><h4><span class="w3-border-bottom">Man in Black</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Dilton Marsh (Wiltshire) - High Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October / November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Wearing long black leather boots and coat with a wide brimmed hat, this phantom man is said to swagger towards the Prince of Wales pub but vanishes just prior to arriving.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Chimes without Wind</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Beenham (Berkshire) - Private residence<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October and November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> One resident reported that around this time of year strange things would happen - wind chimes in the bathroom would be vigorously shaken, lights flick on, and a young man has been spotted in a former bedroom.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Grey Monk</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Rochester (Kent) - Cooper's Arms public house, St Margaret's Street<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Once a year in November, late at night (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Bricked up alive for a long forgotten sin, this misty monk emerges from the wall behind the bar in this ancient building that once was a priory.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/oldpostcard-140.jpg' class="w3-card" title='An old postcard of Hampton Court Palace.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Hampton Court Palace.</small></span>                      <p><h4><span class="w3-border-bottom">Henry's Wives</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Hampton Court (Surrey) - Hampton Court Palace<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: 12 October (reoccurring), 4 November (reoccurring), and other (see text)<br>
              <span class="w3-border-bottom">Further Comments:</span> Jane Seymour returns every 12 October (the date she gave birth to her son), while Anne Boleyn, dressed in blue, is said to walk the passageways of the palace. A silently screaming Catherine Howard has been watched moving towards the chapel (on 4 November), where she begged Henry VIII to save her life. Howard's disembodied hand was spotted and sketched by an artist working in Guard Watching Chamber in 1900. A photograph taken in December 2003 reportedly showing a ghostly hooded figure opening a fire escape has been dismissed as a hoax. In 1989 a figure spotted wearing a Victorian nightshirt vanished in front of a witness.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Giant and Bloody Rain</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Somerleyton (Suffolk) - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: 17 July & 19 November (Reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Somerleyton was once the hunting area of a giant, who is said to briefly appear on the evening of 17 July. The giant once stumbled across a plot to murder him, so struck first and killed the would-be assassin. The assassin's blood is said to rain down from the sky on 19 November.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Fire</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Warminster (Wiltshire) - Cley Hill<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: 31 October & 02 November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Several witnesses have reported seeing large burning fires on the hills, with figures dancing around them, talking and singing loudly in an unrecognised language. When investigated, there is never any evidence of the fire or figures. A legend says the hill was created by the Devil, who threw down a pile of dirt on the spot after being led to believe that it would take him several years to reach Devizes.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Red Head</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Stockton (Warwickshire) - Blue Lias public house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: between March and November (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> Seen walking through the pub after opening hours, this red headed male is thought to have been killed long ago after being discovered in bed with another man's wife.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Healing Waters</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Loch Monar (Highland) - Waters of the loch<br>
              <span class="w3-border-bottom">Type:</span> Legend<br>
              <span class="w3-border-bottom">Date / Time:</span> Varies: first Mondays of February, May, August and November<br>
              <span class="w3-border-bottom">Further Comments:</span> If entered on certain days of the year, the waters of this Loch would heal the sick on the proviso that the Loch was entered three times at midnight and the water then sipped, before a coin was thrown in. The sick would then have to leave the site before the sun came up.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 27</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=27"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=27"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/calendar/Pages/calendar.html">Return to Main Calendar Page</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
  <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>

      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>

<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Type</h5>
   <a href="/reports/reports-type.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/report2.jpg"                   alt="Reports: Type" style="width:100%" title="View Reports: Type">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Reports: Places</h5>
   <a href="/reports/reports.htm"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/reports.jpg"                   alt="View Reports: Places" style="width:100%" title="View Reports: Places">
  </div></a>
  <p></p>
</div>
<div class="w3-border-left w3-border-top w3-left-align">
<h5>Reports: People</h5>
       <a href="/reports/reports-people.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/report3.jpg"                   alt="View Reports: People" style="width:100%" title="View Reports: People">
  </div></a>
  <p></p>

</div>



</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
