

<!DOCTYPE html>
<html>
<head>
<meta http-equiv='content-language' content='en-gb'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127447789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127447789-1');
</script>
<meta property="og:image" content="https://www.paranormaldatabase.com/forsocialmedia.jpg" />
<title>The Paranormal Database</title>
<meta name="description" content="Galway Folklore, Ghosts and Strange Places from The Paranormal Database">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../databasecss/w3.css"> 
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@200&display=swap" rel="stylesheet"> 
<style>
body, h1, h2, h3, h4, h5, h6  {
font-family: 'Roboto Mono', monospace;
}
</style>
</head>
<body class="w3-content" style="max-width:1200px">

  <!-- Side Navigation -->
  <nav class="w3-sidebar w3-bar-block w3-card w3-animate-top w3-center" style="display:none"

      id="mySidebar">
      <h2 class="w3-xlarge w3-text-theme">Navigation</h2>
<button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html" class="w3-bar-item w3-button">Home</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/ssl/contact001.html" class="w3-bar-item w3-button">Contribute</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/index.html#england" class="w3-bar-item w3-button">England</a> </button>
          <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/scotland.html" class="w3-bar-item w3-button">Scotland</a></button>
    <button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/wales.html" class="w3-bar-item w3-button">Wales</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/northernireland.html" class="w3-bar-item w3-button">Northern Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/regions/ireland.html" class="w3-bar-item w3-button">Republic of Ireland</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-type.html" class="w3-bar-item w3-button">Reports: Types</a> </button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports.htm" class="w3-bar-item w3-button">Reports: Places</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/reports/reports-people.html" class="w3-bar-item w3-button">Reports: People</a></button>
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/calendar/Pages/calendar.html" class="w3-bar-item w3-button">Calendar</a></button>  
<button class="w3-bar-item w3-button" onclick="w3_close()"><a href="/recent/index.php" class="w3-bar-item w3-button">Recent Additions</a></button>           
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-left-align ">
  <h1 class="w3-xxxlarge w3-animate-bottom">The Paranormal Database</h1>
    <h4>Examining  folkloric, paranormal &amp; cryptozoological locations  in the UK and beyond<b>
    </h4>

  </div>
</header>
<!-- Header END -->

  <div class="w3-panel w3-border-left w3-border-top">
  <h4><a href="/index.html">Home</a> > <a href="/regions/ireland.html">Republic of Ireland</a> > County Galway</h4>
  </div>

 <div class="w3-panel w3-border-left w3-border-top">
  <h3>County Galway Ghosts, Folklore and Paranormal Places</h3>
    
  <div class="w3-half">
                           <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Short Men in Green</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aran Island - Exact location not known<br>
              <span class="w3-border-bottom">Type:</span> Fairy<br>
              <span class="w3-border-bottom">Date / Time:</span> 1992<br>
              <span class="w3-border-bottom">Further Comments:</span> A fifteen year old boy spotted two figures sea fishing, each just over a metre tall, dressed in green and wearing brown shoes. The figures chattered to each other in Irish before jumping up and vanishing. One of the figures left a small pipe behind which the witness took, although it later disappeared when 'safely' locked away.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Mantelpiece</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Athenry - Dunsandle House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Mid-to-late-twentieth century<br>
              <span class="w3-border-bottom">Further Comments:</span> The ghost of a tall man attached itself to the mantelpiece which was in this house - when a stoneworker took the piece away, the ghost followed both it and him and began to haunt his workshop. Items were thrown around, and strange fiddling could be heard at night. It is unclear whether the new owner returned the mantelpiece or smashed it up.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Woman with a Lamp</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Athenry - Wooded area<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 July 2020, between 03:00h-04:00h<br>
              <span class="w3-border-bottom">Further Comments:</span> While playing Geocaching, two friends met an older lady with a lamp walking through the woods. Sharing the story once they arrived home, they discovered a family member had also met the older lady around 35 years previous. A legend says that a ghostly lady walks the area, looking for her son who had died.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Screaming</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Aughrim - Site of Battle of Aughrim<br>
              <span class="w3-border-bottom">Type:</span> Environmental Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The battle, fought in 1691, is said to have been the most brutal on UK soil, with up to seven thousand people killed. Folklore says that the screams of dying soldiers can still be heard in the area.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Ballinasloe-Rathpeak-House.jpg' class="w3-card" title='A ghostly young girl looks from a window.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> A ghostly young girl looks from a window.</small></span>                      <p><h4><span class="w3-border-bottom">Knocking Girl</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballinasloe - Rathpeak House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An unknown young girl has sometimes been seen standing by an upstairs window in the ruined house. There are reports of strange, loud bangs which drift from the empty building.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">First Irish Case?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballybane - Private residence, Clareview Park<br>
              <span class="w3-border-bottom">Type:</span> SHC<br>
              <span class="w3-border-bottom">Date / Time:</span> 22 December 2010<br>
              <span class="w3-border-bottom">Further Comments:</span> The BBC reported that Dr Ciaran McLoughlin, West Galway coroner, had recorded a verdict of spontaneous combustion in a case he had investigated. The body of Michael Faherty had been found in his living room, totally burnt, although damage to the rest of the room was minimal.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Trotting Dog</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballygar - Lane in the area<br>
              <span class="w3-border-bottom">Type:</span> Shuck<br>
              <span class="w3-border-bottom">Date / Time:</span> 1913<br>
              <span class="w3-border-bottom">Further Comments:</span> Potentially last seen by a cyclist, the black dog which haunted this area would vanish into thin air as witnesses looked on.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Bear-faced Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballymacward - Lane near Woodlawn railway station<br>
              <span class="w3-border-bottom">Type:</span> Other<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Two people walking home after leaving the station heard groaning coming from over a wall. One climbed up to have a look and observed a creature with a man's body but with the head of a bear. The witness tried to speak to the strange creature, but it told him 'don't speak to me' before wandering off.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Large Eel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Ballynahinch - Bridge near to Ballynahinch Castle<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1880s<br>
              <span class="w3-border-bottom">Further Comments:</span> A long eel-like creature remained stuck under this bridge for two days.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Christopher St George</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clarinbridge - Tyrone House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Christopher came from a strongly Protestant family but fell in love with and married a Catholic woman. His family immediately disowned him, his ghost a testament to his protests of being unfairly treated.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">The Poor</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clifden - Clifden Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Stories say the castle was a place for people to gather during the Great Famine, and those who died remained on the site.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Loathsome Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Clifden - Lough Fadda<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 1954 onwards<br>
              <span class="w3-border-bottom">Further Comments:</span> After spotting this strange water creature, one of the witnesses had nightmares for weeks after the event and refused to return alone to the lake. She said the creature was long and black, with smooth skin and a head like a shark.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Horned Eel</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Connemara - Lough Nahioon (also spelt Nahooin)<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 22 February 1968<br>
              <span class="w3-border-bottom">Further Comments:</span> A family watched this creature, almost four metres long, swimming around the lake. It was described as dark and long without hair or eyes, and possessed two horns at the top of the head.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Strangler</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Connemara - Renvyle House<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Circa 1917<br>
              <span class="w3-border-bottom">Further Comments:</span> This house is thought to be haunted by the ghost of a man who strangled himself with bare hands. Doors are reported to open and close unaided, while figures have been seen reflected in mirrors. One phantom on the site is named as 'Old Mrs Gogarty', and little is known about the ghostly man dressed in tweed.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/dunanore-castle.jpg' class="w3-card" title='Castle Dunanore (public domain).'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> Castle Dunanore (public domain).</small></span>                      <p><h4><span class="w3-border-bottom">Murdered Pirates</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Cummer - Castle Dunanore<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Sometimes one can see a ghostly pirate ship drop anchor just off the coast; the battle scarred sailors use smaller boats to row to the castle where they carry chests and sacks of gold and jewels into the ruined structure.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Soldier</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Feenish Island - Area around castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> An unknown amount of gold hidden somewhere in this area is guarded by a ghostly soldier.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/galway-bay.jpg' class="w3-card" title='An old postcard of Galway Bay.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An old postcard of Galway Bay.</small></span>                      <p><h4><span class="w3-border-bottom">Pig Fish</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galway - Bay<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> 28 June 1885<br>
              <span class="w3-border-bottom">Further Comments:</span> A sea monster caught by a fishing crew was seen by thousands of people. The creature was described as nine and a half feet (2.9 metres) long, weighed 136 kilograms and had silvery skin. The head had a long pig-like snout, cow-like eyes, and two rows of teeth, each tooth around two inches (5 centimetres) in length. The creature also had an underbelly of sharp thorns. The Dublin Museum later identified the creature as a bramble shark.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Tall Man</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galway - Newcastle area (possibly neighbourhood known as Granville)<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> November 1908<br>
              <span class="w3-border-bottom">Further Comments:</span> Two men walking in the evening encountered a dark human-like shape, some nine foot (2.74 metres) in height. The figure vanished as it moved closer, only to reappear further down the road. The following night, a party armed with sticks and pistols tried to find the entity. They succeeded, although as one man took aim with his revolver, he dropped the weapon and fainted. The entity vanished once again and never returned.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Moving Toys</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galway - Private residence, Corrib Park<br>
              <span class="w3-border-bottom">Type:</span> Poltergeist<br>
              <span class="w3-border-bottom">Date / Time:</span> May 1997<br>
              <span class="w3-border-bottom">Further Comments:</span> A haunting presence made itself known in this house shortly after the birth of a girl. Unexplained footsteps and crying could be heard, toys rearranged, items would fall from the walls, and a jug shattered after being thrown on the staircase. Although frightening, family members believed the entity was protecting the newborn. The family eventually moved out and a priest visited to bless the rooms.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">WB Yeats</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galway - Renvyle House hotel<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> Yeats treated this building as home away from home and is believed to still reside here. Several of the rooms are also haunted by the ghosts of children.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Third Floor Ghost</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galway - Subway sandwich shop<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> May 2017<br>
              <span class="w3-border-bottom">Further Comments:</span> This shop enjoyed a brief surge in online publicity when some news outlets showed a poster on display which stated, 'Ouija boards and seances are not allowed to be performed in this shop'. It was reported that the manager believed the third floor to be haunted and did not want anyone attracting more ghosts.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Claddagh Nun?</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galway - The Long Walk<br>
              <span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br>
              <span class="w3-border-bottom">Date / Time:</span> February 2012<br>
              <span class="w3-border-bottom">Further Comments:</span> An image from photographer Jonathan Curran showed what looked like a nun standing along the Long Walk at Claddagh, although the woman may just be a living person who popped up at the wrong time...</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Murdered Woman</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Galway - Unidentified house<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> October 1890<br>
              <span class="w3-border-bottom">Further Comments:</span> Thousands of people were reported to have stood outside this house after the ghost of a woman appeared in a window. She had been murdered by her husband fifteen years previous and had recently manifested to a couple walking to church, asking them to say a prayer for her.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p>                      <p><h4><span class="w3-border-bottom">Rhino-Like Creature</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Glinsk - Lough Dubh, aka Black's Lake, between Glinsk and Roscommon<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> March 1962<br>
              <span class="w3-border-bottom">Further Comments:</span> A cow-sized creature was hooked by a man and his son out fishing along the river - the monster emerged from the water after the younger of the two snagged it with his hook. The strange beast was described as having short stumpy legs, small ears, and a single horn protruding from its nose.</div></p>
              
              
          <div class="w3-border-left w3-border-top  w3-left-align">

             <p><img src='../../thumbs/Gort-Coole-Lake.jpg' class="w3-card" title='An Irish cryptid.'><br><span class="w3-text-grey"><i class="fa fa-camera-retro" aria-hidden="true"></i> <small> An Irish cryptid.</small></span>                      <p><h4><span class="w3-border-bottom">Water Horse</span></h4></p>
            <p><span class="w3-border-bottom">Location:</span> Gort - Coole Lake<br>
              <span class="w3-border-bottom">Type:</span> Cryptozoology<br>
              <span class="w3-border-bottom">Date / Time:</span> Unknown<br>
              <span class="w3-border-bottom">Further Comments:</span> The lake is said to be home of a mysterious water monster resembling a horse.</div></p>
              
              
        
  <!-- Still part of main list-->    
<div class="w3-center w3-border-top">
  <h4>Records 
            1 - 25 of 32</h4>

  
<div class="w3-row w3-border">

  <div class="w3-quarter w3-container">
    <h5></h5>
  </div>
  
  <div class="w3-quarter w3-container">
    <h5> </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=32"class="w3-button h4">next</a>  
            </h5>  
  </div>
  
  
  <div class="w3-quarter w3-container">
    <h5>            <a href="?pageNum_paradata=1&totalRows_paradata=32"class="w3-button h4">last</a> 
            </h5>  
  </div>
  
  <div class="w3-container">

<p>
  <button class="w3-button w3-block h4 w3-border"><a href="/regions/ireland.html">Return to the Republic of Ireland</a></button></p>

</div>
  </div>
  
</div>
   
  </div>

    <div class="w3-half">

    <div class="w3-half w3-container">
  
  <p></p>
  </div>
  
   <!-- Side menu -->
      <div class="w3-half w3-container w3-border-top w3-light-grey w3-border-left"> 
      <h5>Quick Links</h5>
      
      
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Contribute</h5>
     <a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/cont2.jpg"                   alt="Contribute to the Paranormal Database" style="width:100%" title="Contribute to the Paranormal Database">
  </div></a>
  <p></p>

</div>
      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Republic of Ireland</h5>
     <a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/roi.jpg"                   alt="View Republic of Ireland records" style="width:100%" title="View Republic of Ireland records">
  </div></a>
  <p></p>

</div>


      <div class="w3-border-left w3-border-top w3-left-align">
  <h5>Calendar</h5>
     <a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img src="/images/front/calendar.jpg"                   alt="View the Paranormal Database Calendar" style="width:100%" title="View the Paranormal Database Calendar">
  </div></a>
  <p></p>

</div>


<div class="w3-border-left w3-border-top w3-left-align ">
  <h5>Northern Ireland</h5>
   <a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image "> <img src="/images/front/ni.jpg"                   alt="View Northern Ireland records" style="width:100%" title="View Northern Ireland records">
  </div></a>
  <p></p>
</div>
</div>
</div>
</div>
</div>


<!-- Footer -->
<footer class="w3-container w3-theme-dark w3-padding-16 w3-left-align">
  <h3>© Paranormal Database 2023</h3>
  <p></p>
  <div style="position:relative;bottom:55px;" class="w3-tooltip w3-right">
    <span class="w3-text w3-theme-light w3-padding">Return to Top</span>    
    <a class="w3-text-white " href="#myHeader"><span class="w3-xlarge ">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>
<!-- Footer End -->
<!-- Script for Sidebar, Tabs, Accordions, Progress bars and slideshows -->
<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "50%";
  x.style.fontSize = "20px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}

// Tabs
function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}

var mybtn = document.getElementsByClassName("testbtn")[0];
mybtn.click();

// Accordions
function myAccFunc(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// Slideshows
var slideIndex = 1;

function plusDivs(n) {
  slideIndex = slideIndex + n;
  showDivs(slideIndex);
}

function showDivs(n) {
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

showDivs(1);

// Progress Bars
function move() {
  var elem = document.getElementById("myBar");   
  var width = 5;
  var id = setInterval(frame, 10);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

</body>
</html>
